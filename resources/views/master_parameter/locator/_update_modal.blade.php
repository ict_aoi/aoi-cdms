<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.updateLocator') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE Locator</span> <!-- title -->
                              </legend>
                              
                              <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">NAME:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control text-uppercase" name="locator_name_update" id="locator_name_update" placeholder="Locator" readonly>
                              	</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Role</label>
								<div class="col-lg-9">
									<select data-placeholder="Select a state" name="role_update" id="role_update" class="form-control select-search">
										<option value=""></option>
										@foreach ($roles as $item)
											<option value="{{$item->id}}">{{$item->display_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label class="col-sm-7 control-label text-semibold">Is Artwork ?</label>
										<div class="col-sm-5">
											<input type="checkbox" name="is_artwork_update" id="is_artwork_update">
										</div>
									</div>
									<div class="col-md-4">
										<label class="col-sm-7 control-label text-semibold">Is Cutting ?</label>
										<div class="col-sm-5">
											<input type="checkbox" name="is_cutting_update" id="is_cutting_update">
										</div>
									</div>
									<div class="col-md-4">
										<label class="col-sm-7 control-label text-semibold">Is Setting ?</label>
										<div class="col-sm-5">
											<input type="checkbox" name="is_setting_update" id="is_setting_update">
										</div>
									</div>
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->