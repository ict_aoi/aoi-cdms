@extends('layouts.app',['active' => 'upload_premintaan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Upload Permintaan Marker </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Preparation</li>
            <li class="active">Permintaan Marker</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form class="form-horizontal">
            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <span class="input-group-btn">  
                                <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                            </span>
                            <input type="text" class="form-control pickadate" name="date_cutting" placeholder="Masukkan Tanggal Cutting" id="date_cutting">
                            <span class="input-group-btn">
                                <button class="btn btn-primary legitRipple" type="button" id="btn-filter">Pilih</button>
                            </span>
                            <span class="input-group-btn">
                                <button class="btn btn-success legitRipple" type="button" id="btn-uploadpm"><span class="icon-file-upload2"></span> Upload</button>
                            </span>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cutting Date</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>Color</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h2>Upload Request Marker || PCD : </h2>
                    </div>
                    <div class="col-lg-5">
                        <h2 id="pcd"></h2>
                    </div>
                </div>
            </div>
            <form action="{{ route('preparation.uploadxls') }}" method="POST" id="form_upload" enctype="multipart/form-data">

                <div class="modal-body">

                    @csrf

                    <input type="text" name="date_cut" id="date_cut" hidden="" >

                    <div class="form-group">
                        <div class="row pd-l-20">
                            <div class="col-md-12">
                                <label class="display-block">File:</label>
                                <input type="file" class="file-styled" name="file_upload" id="file_upload" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                <span class="help-block">Accepted formats: xls, xlsx. Max file size 2Mb</span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-save"><i class="icon-file-upload2"></i> Upload</button>
                    </div>
                </div>
            </form>                               
        </div>
    </div>
</div>

<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="table-detail">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="150px;">Barcode Marker</th>
                                <th width="50px;">Cut</th>
                                <th width="50px;">Part</th>
                                <th>Ratio</th>
                                <th>Layer</th>
                                <th>Fabric Width</th>
                            </tr>
                        </thead>
                    </table>
                </div>                   
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script>
$(document).ready(function(){

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });
    
    var pcd =$('#date_cutting').val();
    var url = "{{route('preparation.ajaxGetPlan')}}";
    var tableL = $('#table-list').DataTable({
         processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
                 return $.extend({}, d, {
                     "date_cutting"         : $('#date_cutting').val(),
                 });
            }
        },
        columns: [
            {data: 'queu', name: 'queu'},
            {data: 'cutting_date', name: 'cutting_date'},
            {data: 'style', name: 'style'},
            {data: 'articleno', name: 'articleno'},
            {data: 'color', name: 'color'},
            {data: 'status', name:'status'}
        ]
    });

    $('#btn-filter').click(function(){
        event.preventDefault();
        var date_cutting    = $('#date_cutting').val();

        if (date_cutting=='') {
            $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url,
                data:{date_cutting:date_cutting},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    pcd = date_cutting;
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });

    $('#btn-uploadpm').click(function(){
       
        var cut_date = $('#date_cutting').val();

        if (cut_date=="") {
         
            $("#alert_warning").trigger("click", 'Filter Plan Cutting Date First !');
           return false;
        }else{
           $('#date_cut').val(cut_date);
           $('#pcd').text(cut_date);

            $('#modalUpload').modal('show');    
        }
            

    });

    $('#form_upload').submit(function(event) {
        event.preventDefault();

        //check po_number
        

        //
        var formData = new FormData($(this)[0]);
        var parameter = ['pcd'];
        var val = [$('#date_cut').val()];

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form_upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#modalUpload').modal('hide');
                loading_process();
            },
            success: function(response) {
                var data_response = response.data;
                if (data_response.status==200) {
                    myalert('success',data_response.output);
                }else{
                    myalert('danger',response);
                }
                $('#table-list').unblock();
                submitFilter(pcd);
            },
            error: function(response) {
                 $('#table-list').unblock();
                 myalert('danger',response.data);
                submitFilter(pcd);
               console.log(response);
            }
        });
    });
  
  function submitFilter(date){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url : "{{route('preparation.ajaxGetPlan')}}",
            data:{date_cutting:date},
            beforeSend:function(){
                loading_process();
            },
            success:function(response){
                $('#table-list').unblock();
                pcd = date_cutting;
                tableL.clear();
                tableL.draw();
            },
            error:function(response){
                myalert('error','eror');
            }
        });
    }
    
});

function detrat(e){
    var id_plan = $(e).data('id');
    
    tableDetail(id_plan);
       

        

       


    $('#modalDetail').modal('show');
}

function tableDetail(id_plan){
   
    console.log(id_plan);
    var tableD = $('#table-detail').DataTable({
             processing: true,
            destroy:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.ajaxGetDetRat') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "id_plan"         : id_plan,
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableD.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(1).css('max-width: 150px');
                $('td', row).eq(2).css('max-width: 50px');
                $('td', row).eq(3).css('max-width: 50px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id'},
                {data: 'cut', name: 'cut'},
                {data: 'part_no', name: 'part_no'},
                {data: 'ratio', name: 'ratio'},
                {data: 'layer', name: 'layer'},
                {data: 'fabric_width', name: 'fabric_width'}
            ]
        });
}

function delrat(e){
    var id_plan = $(e).data('id');
    console.log(id_plan);

     $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url : "{{route('preparation.delrat')}}",
        data:{id_plan:id_plan},
        beforeSend:function(){
            loading_process();
        },
        success:function(response){
            $('#table-list').unblock();
            myalert('success','Delete Success');
            $('#btn-filter').click();
            
        },
        error:function(response){
            myalert('error','eror');
        }
    });

}


function shoot(e){
    var barcode = $(e).data('barcode');
    var plan = $(e).data('plan');
    var part = $(e).data('part');

     $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url : "{{ route('preparation.shootFabricWidth') }}",
        data:{id_plan:plan,barcode_id:barcode,part_no:part},
        beforeSend:function(){
            $('#table-detail').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success:function(response){
           
            var data_response = response.data;
            if (data_response.status==200) {
                myalert('success',data_response.output);
            }else{
                myalert('danger',response);
            }
            $('#table-detail').unblock();
            tableDetail(plan);

        },
        error:function(response){
            myalert('error','eror');
        }
    });

}


</script>
@endsection
