<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataCuttingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_cuttings', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('documentno')->nullable();
            $table->string('style')->nullable();
            $table->string('job_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('customer')->nullable();
            $table->string('destination')->nullable();
            $table->string('product')->nullable();
            $table->integer('ordered_qty')->nullable();
            $table->string('part_no', 50)->nullable();
            $table->string('material')->nullable();
            $table->string('color_name')->nullable();
            $table->string('product_category')->nullable();
            $table->double('cons', 15, 8)->nullable();
            $table->double('fbc', 15, 8)->nullable();
            $table->date('cutting_date')->nullable();
            $table->string('uom')->nullable();
            $table->string('is_piping', 20)->nullable();
            $table->string('custno')->nullable();
            $table->date('statistical_date')->nullable();
            $table->date('lc_date')->nullable();
            $table->string('upc')->nullable();
            $table->string('color_code_raw_material')->nullable();
            $table->integer('width_size')->nullable();
            $table->string('code_category_raw_material')->nullable();
            $table->text('desc_category_raw_material')->nullable();
            $table->text('desc_produksi')->nullable();
            $table->timestamp('mo_updated')->nullable();
            $table->date('ts_lc_date')->nullable();
            $table->string('articleno')->nullable();
            $table->string('size_finish_good', 50)->nullable();
            $table->string('size_category', 10)->nullable();
            $table->string('color_finish_good')->nullable();
            $table->string('warehouse')->nullable();
            $table->text('desc_mo')->nullable();
            $table->string('status_ori')->nullable();
            $table->timestamp('mo_created')->nullable();
            $table->date('promised_date')->nullable();
            $table->string('season')->nullable();
            $table->string('article_name')->nullable();
            $table->string('garment_type')->nullable();
            $table->string('queu', 20)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_cuttings');
    }
}
