<?php

namespace App\Http\Controllers\Ppcm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\DockkPpcdetail;
use App\Models\DockkPpcheader;
use File;
use Excel;
use DB;
use Auth;
use DataTables;

class DockkController extends Controller
{
    public function index(){
        return view('ppcm.doc_kk');
    }

    public function UploadKK(Request $request){
        if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $data = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }else{
            return response()->json('Pilih Dokumen Dahulu !', 422);
        }
        foreach($data as $x => $y){
            if($y->factory == 'AOI 1' || $y->factory == 'AOI1'){
                $factory_id = 1;
            }elseif($y->factory_id == 'AOI 2' || $y->factory == 'AOI2'){
                $factory_id = 2;
            }else{
                $factory_id = 0;
            }
            if(strpos($y->style,'-TOP') > 1){
                $temp = explode('-',$y->style);
                $style = trim($temp[0]);
                $set_type = 'TOP';
            }elseif(strpos($y->style,'-BOT') > 1){
                $temp = explode('-',$y->style);
                $style = trim($temp[0]);
                $set_type = 'BOTTOM';
            }else{
                $style = $y->style;
                $set_type = 'Non';
            }
            $data_check = DB::table('doc_kk_ppc_header')
                ->where('kk_no','like','%'.$y->kk_no.'%')
                ->where('style', $style)
                ->where('set_type',$set_type)
                ->whereYear('created_at',Carbon::now()->year)
                ->whereNull('deleted_at')
                ->where('factory_id',$factory_id)
                ->get();
            if(count($data_check) == 0){
                try {
                    DB::begintransaction();
                    foreach ($data as $dx => $val) {
                        if($val->factory == 'AOI 1' || $val->factory == 'AOI1'){
                            $factory_id = 1;
                        }elseif($val->factory_id == 'AOI 2' || $val->factory == 'AOI2'){
                            $factory_id = 2;
                        }else{
                            $factory_id = 0;
                        }
                            if($val->factory == '' || $val->factory == null){
                                return response()->json('Factory Tidak Boleh Kosong !', 422);
                            }
                            if($val->season == '' || $val->season == null){
                                return response()->json('Season Tidak Boleh Kosong !', 422);
                            }
                            if($val->style == '' || $val->style == null){
                                return response()->json('Style Tidak Boleh Kosong !', 422);
                            }
                            if($val->panel == '' || $val->panel == null){
                                return response()->json('Nama Panel Tidak Boleh Kosong !', 422);
                            }
                            if($val->remark == '' || $val->remark == null){
                                return response()->json('Remark Tidak Boleh Kosong !', 422);
                            }
                            if($val->price_estimated == '' || $val->price_estimated == null){
                                return response()->json('Remark Tidak Boleh Kosong !', 422);
                            }
                            if($val->qty == '' || $val->qty == null){
                                return response()->json('Qty Tidak Boleh Kosong !', 422);
                            }
                            if($val->allowance == '' || $val->allowance == null){
                                return response()->json('Allowance Tidak Boleh Kosong !', 422);
                            }
                            if($val->price_ori == '' || $val->price_ori == null){
                                return response()->json('Harga Tidak Boleh Kosong !', 422);
                            }
                            if($val->currency_rate == '' || $val->currency_rate == null){
                                return response()->json('Currency Rate Tidak Boleh Kosong !', 422);
                            }
                            if($val->kk_no == '' || $val->kk_no == null){
                                return response()->json('No KK Tidak Boleh Kosong !', 422);
                            }
                            if($val->kk_type == '' || $val->kk_type == null){
                                return response()->json('KK Type Tidak Boleh Kosong !', 422);
                            }
                            if($val->document_type == '' || $val->document_type == null){
                                return response()->json('Document Type Tidak Boleh Kosong !', 422);
                            }
                            // if(strpos($val->style,'-') > 1){
                            //     $temp = explode('-',$val->style);
                            //     $style_check = $temp[1];
                            //     if(!in_array($style_check,['TOP','BOT'])){
                            //         return response()->json('Kesalahan Penulisan Style '.$temp[0], 422);
                            //     }
                            // }
            
                            if(strpos($val->style,'-TOP') > 1){
                                $temp = explode('-',$val->style);
                                $style = $temp[0];
                                $set_type = 'TOP';
                            }elseif(strpos($val->style,'-BOT') > 1){
                                $temp = explode('-',$val->style);
                                $style = $temp[0];
                                $set_type = 'BOTTOM';
                            }else{
                                $style = $val->style;
                                $set_type = 'Non';
                            }
                            if($set_type == 'BOTTOM'){
                                $type_id = '3';
                            }elseif($set_type == 'TOP'){
                                $type_id = '2';
                            }else{
                                $type_id = '1';
                            }

                            // $check_inline_style = DB::table('master_style_detail')
                            // ->where('style',$style)
                            // ->where('type_id',$type_id)
                            // ->first();
                            // $season = DB::table('master_seasons')->where('season_name',strtoupper(trim($val->season)))->first();
                            // if($season == null){
                            //     return response()->json('Kesalahan Penulisan Season '.$val->season.' !', 422);
                            // }else{
                            //     $check_style = DB::table('master_style_detail')
                            //     ->where('style',$style)
                            //     ->where('id',$season->id)->first();
                            //     if($check_style == null){
                            //         return response()->json('Master Style '.$style.'-'.$set_type.'Belum di Input di Sistem!', 422);
                            //     }elseif($check_style != null && $check_inline_style == null){
                            //         return response()->json('Kesalahan Pada Set Type Dari Style '.$style.'-'.$set_type, 422);
                            //     }
                            // }
            
                            $months = Carbon::now()->month;
                            $years = Carbon::now()->year;
                            $romans_code = $this->convertMonthtoRoman($months);

                            $data_exists = DB::table('doc_kk_ppc_detail')
                            ->where('kk_no',strtolower(trim($val->kk_no)))
                            ->where('season',strtoupper(trim($val->season)))
                            ->where('panel',strtolower(trim($val->panel)))
                            ->where('kk_type',strtolower(trim($val->kk_type)))
                            ->where('document_type',$val->document_type)
                            ->where('style',strtoupper(trim($style)))
                            ->where('set_type',$set_type)
                            ->where('document_no',strtoupper(trim($val->kk_no)).'/'.'PPIC-'.$val->factory.'/'.strtolower(trim($val->document_type)).'/'.$romans_code.'/'.$years)
                            ->where('factory_id',$val->factory_id)
                            ->where('document_type',strtolower(trim($val->document_type)))
                            ->exists();

                            if($data_exists){
                                return response()->json('Data pernah di upload !', 422);
                            }else{
                                DockkPpcdetail::FirstOrCreate([
                                'season'            => strtoupper(trim($val->season)),
                                'style'             => strtoupper(trim($style)),
                                'set_type'          => $set_type,
                                'panel'             => strtolower(trim($val->panel)),
                                'remark'            => strtolower(trim($val->remark)),
                                'qty'               => $val->qty,
                                'allowance'         => $val->allowance,
                                'qty_plus_allowance'=> $val->qty + ($val->qty*$val->allowance),
                                'price_ori'         => $val->price_ori,
                                'price_estimated'   => $val->price_estimated,
                                'currency_rate'     => $val->currency_rate,
                                'idr_price'         => $val->currency_rate*$val->price_estimated,
                                'total_amount'      => ($val->currency_rate*$val->price_estimated)*$val->qty,
                                'created_at'        => Carbon::now(),
                                'created_by'        => \Auth::user()->id,
                                'updated_at'        => null,
                                'kk_no'             => strtolower(trim($val->kk_no)),
                                'kk_type'           => strtolower(trim($val->kk_type)),
                                'factory_id'        => $factory_id,
                                'document_no'       => strtoupper(trim($val->kk_no)).'/'.'PPIC-'.$val->factory.'/'.strtolower(trim($val->document_type)).'/'.$romans_code.'/'.$years,
                                'document_type'     => strtolower(trim($val->document_type))
                                ]);
                            }

                            $data_detail = DB::table('doc_kk_ppc_detail')
                            ->select('factory_id','season','style','set_type','kk_no','kk_type','document_no','remark','document_type')
                            ->where('kk_no',strtolower(trim($val->kk_no)))
                            ->where('style',$style)
                            ->where('season',$val->season)
                            ->where('set_type',$set_type)
                            ->where('factory_id',$factory_id)
                            ->whereNull('deleted_at')
                            ->get();

                            foreach($data_detail as $zx => $y){
                                DockkPpcheader::FirstOrCreate([
                                    'season'        => $y->season,
                                    'style'         => $y->style,
                                    'set_type'      => $y->set_type,
                                    'remark'        => $y->remark,
                                    'factory_id'    => $y->factory_id,
                                    'kk_no'         => strtolower($y->kk_no),
                                    'kk_type'       => $y->kk_type,
                                    'document_type' => $y->document_type,
                                    'document_no'   => $y->document_no,
                                    'created_by'    => \Auth::user()->id,
                                    'created_at'    => Carbon::now()->format('Y-m-d'),
                                    'updated_at'    => null
                                ]);
                                $get_header_id = DockkPpcheader::where('style',$style)
                                ->where('season',$y->season)
                                ->where('set_type',$y->set_type)
                                ->where('factory_id',$y->factory_id)
                                ->where('document_no',$y->document_no)
                                ->first();
                                DockkPpcdetail::where('factory_id',$factory_id)
                                ->where('style',$y->style)
                                ->where('set_type',$y->set_type)
                                ->where('kk_no',strtolower($y->kk_no))
                                ->where('season',$y->season)
                                ->where('document_no',$y->document_no)
                                ->update([
                                    'doc_ppc_header_id'     => $get_header_id->id
                                ]);
                            }
                    }
                    $header_id_arr = DB::table('doc_kk_ppc_header')->where('kk_no',strtolower($y->kk_no))->whereYear('created_at',Carbon::now()->year)->pluck('id')->toArray();
                    $total_qty = DB::table('doc_kk_ppc_detail')->select(DB::raw('doc_ppc_header_id,sum(qty) as total_qty'))->whereIn('doc_ppc_header_id',$header_id_arr)->groupBy('doc_ppc_header_id')->get();
                    foreach($total_qty as $qtys => $ax){
                        DB::table('doc_kk_ppc_header')->where('id',$ax->doc_ppc_header_id)->update([
                            'total_qty'         => $ax->total_qty,
                            'balanced_quota'    => $ax->total_qty,
                            'quota_used'        => 0
                        ]);
                    }
                    DB::commit();
                } catch (Exception $er) {
                    DB::rollback();
                    $message = $er->getMessage();
                    ErrorHandler::db($message);
                }
                return view('ppcm.doc_kk');
            }else{
                return response()->json('style'.$style.'-'.$set_type.'#'.'KK-'.strtoupper($y->kk_no).' Sudah Pernah di Upload!', 422);
            }
        }
    }

    public function getDataDockk(Request $request){
        if(request()->ajax()) 
        {
            $temp = explode('-',$request->date_range);
            $from = Carbon::parse($temp[0])->format('Y-m-d');
            $to = Carbon::parse($temp[1])->format('Y-m-d');

            $data = DB::table('doc_kk_ppc_detail as dkp')
            ->join('users as u','u.id','=','dkp.created_by')
            ->whereDate('dkp.created_at','>=',$from)
            ->whereDate('dkp.created_at','<=',$to)
            ->where('dkp.factory_id',\Auth::user()->factory_id)
            ->whereNull('dkp.deleted_at')
            ->orderBy('dkp.created_at','desc')
            ->get();
            
            if($data == null){
                return response()->json('Data Tidak Ditemukan..', 422);
            }else{
                return DataTables::of($data)
                ->addColumn('style',function($data){
                    if($data->set_type == 'Non'){
                        return $data->style;
                    }else{
                        return $data->style.'-'.$data->set_type;
                    }
                })
                ->addColumn('remark',function($data){
                    return strtoupper($data->remark);
                })
                ->addColumn('panel',function($data){
                    return strtoupper($data->panel);
                })
                ->addColumn('qty',function($data){
                    return number_format($data->qty);
                })
                ->addColumn('qty_plus_allowance',function($data){
                    return number_format($data->qty_plus_allowance);
                })
                ->addColumn('allowance',function($data){
                    return number_format($data->allowance,2);
                })
                ->addColumn('price_estimated',function($data){
                    return number_format($data->price_estimated,2);
                })
                ->addColumn('total_amount',function($data){
                    return 'Rp. '.number_format($data->total_amount);
                })
                ->addColumn('kk_no_type',function($data){
                    return strtoupper($data->kk_no).' - '.strtoupper($data->kk_type);
                })
                ->addColumn('document_no',function($data){
                    return strtoupper($data->document_no);
                })
                ->addColumn('factory',function($data){
                    if($data->factory_id == 1){
                        return 'AOI 1';
                    }elseif($data->factory_id == 2){
                        return 'AOI 2';
                    }else{
                        return '-';
                    }
                })
                ->addColumn('user_name',function($data){
                    return strtoupper($data->name);
                })
            ->rawColumns(['style','qty','allowance','price_estimated','total_amount','kk_no_type','factory','remark','panel','qty_plus_allowance','document_no'])
            ->make(true);
            }
        }
    }

    public function editDataDockk(Request $request){
        if(request()->ajax()) 
        {
            $kk_no = $request->kk_no;
            
            $data = DB::table('doc_kk_ppc_header as dkp')
            ->select('dkp.id','dkp.season','dkp.style','dkp.set_type','dkp.remark','dkp.total_qty','dkp.kk_no','dkp.kk_type','dkp.factory_id','dkp.created_by','dkp.document_no','u.name')
            ->join('users as u','u.id','=','dkp.created_by')
            ->where('kk_no',$kk_no)
            ->where('dkp.factory_id',\Auth::user()->factory_id)
            ->whereNull('dkp.deleted_at')
            ->orderBy('dkp.created_at','asc')
            ->get();
            return DataTables::of($data)
            ->addColumn('style',function($data){
                if($data->set_type == 'Non'){
                    return $data->style;
                }else{
                    return $data->style.'-'.$data->set_type;
                }
            })
            ->addColumn('remark',function($data){
                return strtoupper($data->remark);
            })
            ->addColumn('total_qty',function($data){
                return number_format($data->total_qty);
            })
            ->addColumn('kk_no_type',function($data){
                return strtoupper($data->kk_no).' - '.strtoupper($data->kk_type);
            })
            ->addColumn('document_no',function($data){
                return strtoupper($data->document_no);
            })
            ->addColumn('factory',function($data){
                if($data->factory_id == 1){
                    return 'AOI 1';
                }elseif($data->factory_id == 2){
                    return 'AOI 2';
                }else{
                    return '-';
                }
            })
            ->addColumn('action', function($data){ 
                return view('_action', [
                    'edit_modal' => route('DocKK.editQty', $data->id),
                    'id' => $data->id,
                ]);
            })
            ->addColumn('user_name',function($data){
                return strtoupper($data->name);
            })
            ->rawColumns(['style','qty','allowance','price_estimated','total_amount','kk_no_type','factory','remark','panel','qty_plus_allowance','document_no','action'])
            ->make(true);
            
        }
    }

    public function editQty(Request $request){
        $id = $request->id;
        $data = DB::table('doc_kk_ppc_header')->select('id','total_qty')->where('id',$id)->first();
        return response()->json($data, 200);
    }

    public function updateQty(Request $request)
    {
        $data = $request->all();
        $qty = $data['qty'];
        $header_id = $data['header_id'];

        $cek_qty = DB::table('doc_kk_ppc_header')
                        ->where('id',$header_id)
                        ->first();  

        if ((int)$qty < $cek_qty->total_qty) {
            return response()->json('Anda Tidak Dapat Mengurangi Quota Booking!', 422);
        }
        try {
            DB::beginTransaction();
                DB::table('doc_kk_ppc_header')
                ->where('id',$header_id)
                ->update([
                    'total_qty' => $qty,
                    'updated_at' => Carbon::now(),
                    'updated_by' => \Auth::user()->id
                ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }    
    }

    private function convertMonthtoRoman($months)
    {
        switch ($months) {
            case 1:
                $roman = 'I';
                break;
            case 2:
                $roman = 'II';
                break;
            case 3:
                $roman = 'III';
                break;
            case 4:
                $roman = 'IV';
                break;
            case 5:
                $roman = 'V';
                break;
            case 6:
                $roman = 'VI';
                break;
            case 7:
                $roman = 'VII';
                break;
            case 8:
                $roman = 'VIII';
                break;
            case 9:
                $roman = 'IX';
                break;
            case 10:
                $roman = 'X';
                break;
            case 11:
                $roman = 'XI';
                break;
            case 12:
                $roman = 'XII';
            default:
                $roman = null;
        }
        $convert_month = $roman;

        return $convert_month;
    }
}
