@extends('layouts.app',['active' => 'switch_locator'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Edit Supply Line</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Edit Supply Line</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">Edit Supply Line</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
								<label class="control-label col-lg-2 text-bold">PO Buyer</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="po_buyer" id="po_buyer" placeholder="Masukkan PO Buyer..!" onkeydown="return (event.keyCode!=13);"/>
										</div>
									</div>
									<label class="control-label col-lg-2 text-bold">Cut Num</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="cut_num" id="cut_num" placeholder="Masukkan No Cut..!" onkeydown="return (event.keyCode!=13);"/>
										</div>
									</div>
								</div>
								<div class="pull-right">
									<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead>
										<tr>
											<th>No</th>
											<th>Season</th>
											<th>Style</th>
											<th>PO Buyer</th>
											<th>Article</th>
											<th>Cut</th>
											<th>Size</th>
											<th>Supply Date</th>
											<th>Supply Line</th>
											<th>Supply By</th>
											<th>Action</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-modal')
<div id="modal_edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('switch_locator.update') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
            <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                <div class="panel-body loader-area">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> UPDATE SUPPLY LINE</span>
                            </legend>
							<input type="hidden" class="form-control" id="barcode_id" name="barcode_id" readonly>
                        <div class="form-group">
                        <label class="control-label col-lg-2 text-bold">Line</label>
                            <div class="col-lg-10">
							<select class="form-control select-search" name="line_update" id="line_update" required>
								<option value=""></option>
								@foreach($line as $line)
									<option value="{{ $line->area_name }}">{{$line->area_name }}</option>
								@endforeach
							</select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Update <i class="icon-floppy-disk position-right"></i></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('page-js')
<script>
	//DATATABLES
    var url = "{{route('switch_locator.getDataSupply')}}";
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
		scrollX:true,
		pageLength:100,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
				return $.extend({}, d, {
					"po_buyer"		: $('#po_buyer').val(),
					"cut_num"		: $('#cut_num').val(),
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
            {data: 'style_set', name: 'style_set',searchable:false,visible:true,orderable:true},//2
            {data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//3
            {data: 'article', name: 'article',searchable:true,visible:true,orderable:true},//4
            {data: 'cut_num', name: 'cut_num',searchable:true,visible:true,orderable:true},//5
			{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//6
            {data: 'supply_date', name: 'supply_date',searchable:false,visible:true,orderable:true},//10
			{data: 'supply_line', name: 'supply_line',searchable:true,visible:true,orderable:true},//12
			{data: 'supply_by', name: 'supply_by',searchable:true,visible:true,orderable:true},//13
			{data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

	var tableL = $('#table-list').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				tableL.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				tableL.search("").draw();
			}
			return;
	});
	tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').click(function(){
        tableL.draw();
    });
	$('#form-update').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var parameter = ['line_update','barcode_id'];
        var val = [
			// $('#id_update').val(),
            $('#line_update').val(),
			$('#barcode_id').val(), 
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            $.unblockUI();
            },
            success: function(response) {
                myalert('success','Update Sukses');
				$('#modal_edit').modal('toggle');
				$('#line_update').val('').change();
				tableL.ajax.reload();
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        })
    });

	function edit(url){
	$.ajax({
		type: "get",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();
		}
		})
		.done(function (response) {
			var loc_dist = response.loc_dist.loc_dist;
			$('#barcode_id').val(response.barcode_id);
			$('#line_update').val(loc_dist).trigger('change');;
			$('#modal_edit').modal();
			console.log(response);
		});
	}
</script>
@endsection