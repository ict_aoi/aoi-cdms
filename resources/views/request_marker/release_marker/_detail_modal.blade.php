<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Release Marker</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="style_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="po_buyer_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>Part No</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="part_no_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>Item</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="item_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3">
								<span><strong>Factory</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="factory_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>PCD</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="pcd_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>LC</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="lc_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<span><strong>Balance</strong></span>
							</div>
							<div class="col-md-9">
								: <span id="balance_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr>
								<th>#</th>
								<th>Release At</th>
								<th>Release By</th>
								<th>Marker Prod</th>
								<th>Supply WHS (Yards)</th>
								<th>Actual Marker Prod (Yards)</th>
								<th>Balance (Yards)</th>
								<th>% Save / Overconsumption</th>
								<th>Remark</th>
								<th>Approved</th>
								<th>Approved At</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<hr />
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				{{-- <button type="submit" class="btn btn-primary">Upload File</button> --}}
			</div>
		</div>
	</div>
</div>
