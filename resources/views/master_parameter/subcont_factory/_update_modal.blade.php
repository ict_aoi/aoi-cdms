<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('subcont_factory.updateSubcontFactory') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
            <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                <div class="panel-body loader-area">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> UPDATE SUBCONT FACTORY</span> <!-- title -->
                            </legend>
                            <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

                        <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">Subcont Name:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="subcont_fact_name_update" id="subcont_fact_name_update" placeholder="Subcont Name">
                            </div>
                            <label class="col-lg-3 control-label text-semibold">Subcont Initial:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="subcont_initial_name_update" id="subcont_initial_name_update" placeholder="Subcont Initial">
                            </div>
                            <label class="col-lg-3 control-label text-semibold">Division:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="division_name_update" id="division_name_update" placeholder="Division">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Update <i class="icon-floppy-disk position-right"></i></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->