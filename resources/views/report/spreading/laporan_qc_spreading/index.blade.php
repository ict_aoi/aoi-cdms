@extends('layouts.app',['active' => 'report_qc_spreading'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - QC Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - QC Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="{{ route('reportSpreading.ajaxGetReportQcSpreading') }}" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Spreading</label>
					<div class="col-lg-10">

                        @csrf

                        <div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control daterange-basic" name="spread" placeholder="Masukkan Tanggal Spreading" id="spread" required="">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Download</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>

        <div class="form-horizontal">
            <fieldset class="content-group">
                {!!
                    Form::open([
                        'role'   => 'form',
                        'id'     => 'form',
                        'url'    => '',
                        'method' => 'get',
                        'class'  => 'form-horizontal',
                    ])
                !!}

                <div class="form-group">
                    <label class="control-label col-lg-9 text-bold">Weekly Report QC Spreading ({{$start}} s/d {{$end}})</label>
                    <div class="col-lg-3">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-warning col-md-12" id="btn-weekly">Download Weekly</button>
                            </span>
                        </div>
                    </div>
                </div>
			</fieldset>
        </div>
        {{-- <div class="col-md-6">
            <label class="text-bold">Weekly Report QC Spreading ({{$start}} s/d {{$end}})</label>
            <h4>Weekly Report QC Spreading ({{$start}} s/d {{$end}})</h4>
        </div>
        <div class="col-md-6">
            <span class="input-group-btn">
                <button class="btn btn-primary col-md-12 legitRipple" type="button" id="btn-weekly">Download Weekly</button>
            </span>
        </div> --}}
	</div>
</div>

{!! Form::hidden('url_download_weekly',route('reportSpreading.downloadWeekly'), array('id' => 'url_download_weekly')) !!}

@endsection


@section('page-js')
<script type="text/javascript">

    $(document).ready(function () {
        $('#btn-weekly').on('click', function() {
            var url = $("#url_download_weekly").val();
            $('#form').attr('action',url);
        });
    });

</script>
@endsection

