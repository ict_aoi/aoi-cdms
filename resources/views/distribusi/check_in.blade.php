@extends('layouts.scan',['active' => 'dashboard-scan'])

@section('page-css')
<style>
	.center-row {
		text-align: center !important;
	}
	.bg-red {
		background-color: red !important;
		color: white !important;
	}
	.bg-yellow {
		background-color: yellow !important;
	}
	.bg-green {
		background-color: green !important;
		color: white !important;
	}
	.bg-blue {
		background-color: blue !important;
		color: white !important;
	}
	.add-margin-right {
		margin-right: 20px;
	}

    .white-text {
        color: #ffffff !important;
        font-size: 30px;
    }

    .btn-scan {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .locator-title {
        margin-top: 10px;
        margin-bottom: 5px;

        text-size-adjust: 100pt;
    }

    .title-distribusi {
        padding-top: 0px !important;
    }

    .login-text{
        line-height: 36px;
        vertical-align:center !important;
    }

    .btn-custom-header {
        width: 80px;
    }


</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title text-center title-distribusi">
            <h1 class="white-text"><span class="text-semibold">SCAN LOCATOR {{$locator->locator_name}} IN</span> </h1>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('distribusi.menu',Request::segment(2))}}"> Menu</a></li>
            {{-- <li><a href="{{ route('distribusi.checkIn',Request::segment(2))}}"> {{ucwords(strtolower($locator->locator_name))}}</a></li> --}}
            <li class="active">Check In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<fieldset class="content-group">
    <div class="row text-center">
        <div class="col-md-4">
            <a href="{{ route('distribusi.menu',Request::segment(2))}}" class="btn btn-sm btn-info btn-custom-header pull-left"> BACK </a>
        </div>
        <div class="col-md-4 login-text">
            <label class="control-label col-md-12 col-xs-12">Anda login sebagai, <strong>{{ Auth::user()->name }}<strong></label>
        </div>
        <div class="col-md-4">
            <a href="#"
            {{-- <a href="{{ route('logout') }}" --}}
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            class="btn btn-sm btn-danger pull-right btn-custom-header">
            Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</fieldset>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                    <input type="hidden" id="locator_id" value="{{$locator->id}}" readonly="readonly" />
                    <input type="hidden" name="state" id="state" class="form-control" value="in">
                </div>
            </div>
            {{-- <br /> --}}
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_input" name="barcode_input" placeholder="#Scan Barcode Komponen" autofocus autocomplete="off" />
                </div>
                {{-- <div class="col-md-4">
                    <input type="text" value="Barcode Komponen" class="form-control bg-info" readonly="readonly" />
                </div> --}}
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <h5>Proses Terbaru</h5>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span class="btn btn-warning mr-20" id="reset_count">Reset</span>
                    <span class="btn border-slate text-slate-800 btn-flat legitRipple" id="scan_count">0</span>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Style | Article</th>
                        <th>Part - Komponen</th>
                        <th>PO</th>
                        <th>Size</th>
                        <th>Cut / Sticker No</th>
                        <th>Qty</th>
                        <th>Process</th>
                        <th>Status</th>
                        <th>Loc</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('distribusi.scanManualSet') }}" id="scanManualSetLink"></a>
<a href="{{ route('distribusi.deleteScanProcSet') }}" id="deleteScanProcSetLink"></a>
{{--<a href="{{route('distribusi.SetLocatorLine')}}" id="get_line_set_locator"></a>--}}
@endsection

@section('page-modal')
    @include('distribusi._modal_line_set')
	{{-- @include('distribusi._modal_line_set_new') --}}
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {
            $(".file-styled").uniform({
                fileButtonClass: 'action btn bg-warning'
            });

            $.extend( $.fn.dataTable.defaults, {
                stateSave: true,
                autoWidth: false,
                autoLength: false,
                processing: true,
                // serverSide: true, //sync
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                }
            });

            $('#lineSetModal').on('hidden.bs.modal', function(){
                var barcode_input = $('#barcode_id_set_locator').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: $('#deleteScanProcSetLink').attr('href'),
                    data: {
                        barcode_input: barcode_input,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                    },
                    success: function(response) {
                        $.unblockUI();
                        $('#barcode_id_set_locator').val('');
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('#barcode_input').val('').focus();
                    }
                });
            });

            $('#barcode_input').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var barcode_input = $(this).val();
                    var locator_id = $('#locator_id').val();
                    var state = $('#state').val();

                    validationInputBarcode(barcode_input, locator_id, state, function() {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: '/scan-distribusi/scan-component',
                            data: {
                                barcode_input: barcode_input,
                                locator_id: locator_id,
                                state: state,
                            },
                            beforeSend: function() {
                                $.blockUI({
                                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait',
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent',
                                    },
                                    theme: true,
                                    baseZ: 2000
                                });
                            },
                            success: function(response) {
                                $.unblockUI();
                                $('#barcode_input').val('').focus();
                                appendBarcode(response);
                            },
                            error: function(response) {
                                $.unblockUI();
                                if (response.status == 331) {
                                    $("#lineSetModal").modal();
                                    $('#barcode_id_set_locator').val(response.responseJSON.barcode_id);
                                    $('.select-locator-set').each(function() {
                                        var areaName = $(this).data('area_name');
                                        if (response.responseJSON.line && response.responseJSON.line.includes(areaName)) {
                                            $(this).addClass('bg-success');
                                        }
                                    });
                                    // lineSet(response.responseJSON);
                                }
                                if (response.status == 422) {
                                    $("#alert_warning").trigger("click",response.responseJSON);
                                }
                                $('#barcode_input').val('').focus();
                            }
                        });
                    });
                }
            });

            $('#reset_count').on('click', function() {
                $('#lastActivityTable > tbody').html('');
                $('#scan_count').text(0);
            });

            $('#lineSetModal').on('hidden.bs.modal', function(){
                var barcode_id = $('#barcode_id_set_locator').val();
                $('#barcode_input').val('').focus();
                forceDelTemp(barcode_id);
            });

            $('#lineSetModal').on('click', '.select-locator-set', function() {
                var id = $(this).data('id');
                var barcode_id = $('#barcode_id_set_locator').val();
                var area_name = $(this).data('area_name');

                if(!id) {
                    $("#alert_warning").trigger("click", "terjadi kesalahan saat parsing data, silakan reload halaman!");
                }

                if(!barcode_id) {
                    $("#alert_warning").trigger("click", "terjadi kesalahan saat parsing data, silakan reload halaman!");
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: $('#scanManualSetLink').attr('href'),
                    data: {
                        id: id,
                        barcode_id: barcode_id,
                        area_name: area_name,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                    },
                    success: function(response) {
                        $.unblockUI();
                        $('#lineSetModal').modal('hide');
                        appendBarcode(response);
                    },
                    error: function(response) {
                        $.unblockUI();
                        $('#lineSetModal').modal('hide');
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('#barcode_input').val('').focus();
                    }
                });
            })
        });

        function appendBarcode(data) {
            var scanCount = parseInt($('#scan_count').text()) + 1;
            $('#scan_count').text(scanCount);

            $('#lastActivityTable > tbody').prepend(
                $('<tr>').append(
                    $('<td>').text(data.barcode_id)
                ).append(
                    $('<td>').text(data.style+' | '+data.article)
                ).append(
                    $('<td>').text(data.part+' - '+data.komponen_name)
                ).append(
                    $('<td>').text(data.po_buyer)
                )
                .append(
                    $('<td>').text(data.size)
                ).append(
                    $('<td>').text(data.cut+' / '+data.sticker_no)
                ).append(
                    $('<td>').text(data.qty)
                ).append(
                    $('<td>').text(data.process)
                ).append(
                    $('<td>').text(data.status)
                ).append(
                    $('<td>').text(data.loc_dist)
                )
            )
        }

        function validationInputBarcode(barcode_input, locator_id, state, callbackFucntion) {
            if(!locator_id) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            if(!state) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            callbackFucntion(barcode_input, locator_id, state);
        }

        function forceDelTemp(barcode_id){
            $.ajax({
                type: 'get',
                url: "{{route('distribusi.forceDelTemp')}}",
                data: {
                    barcode_id: barcode_id,
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait',
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent',
                        },
                        theme: true,
                        baseZ: 2000
                    });
                },
                success: function(response) {
                    $.unblockUI();
                    // myalert('info','Nothing to Change!');
                },
                error: function(response) {
                    $.unblockUI();
                }
            });
        }

        // function lineSet(barcode_id){
            
        //     $.ajax({
        //             type: 'get',
        //             url: $('#get_line_set_locator').attr('href'),
        //             data: {
        //                 barcode_id: barcode_id,
        //             },
        //             beforeSend: function() {
        //                 $.blockUI({
        //                     message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
        //                     overlayCSS: {
        //                         backgroundColor: '#fff',
        //                         opacity: 0.8,
        //                         cursor: 'wait',
        //                     },
        //                     css: {
        //                         border: 0,
        //                         padding: 0,
        //                         backgroundColor: 'transparent',
        //                     },
        //                     theme: true,
        //                     baseZ: 2000
        //                 });
        //             },
        //             success: function(response) {
        //                 $.unblockUI();
        //                 $("#lineSetModal").modal();
        //                 $("#lineSetModal .modal-body").html(response);
        //                 $('#barcode_id_set_locator').val(barcode_id);
        //             },
        //             error: function(response) {
        //                 $.unblockUI();
        //             }
        //         });
        // }

    </script>
@endsection
