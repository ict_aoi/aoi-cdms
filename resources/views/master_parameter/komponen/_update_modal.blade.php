<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.updateKomponen') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE Locator</span> <!-- title -->
                              </legend>
                              
                              <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">NAME:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control text-uppercase" name="komponen_name_update" id="komponen_name_update" placeholder="Locator">
                              	</div>
							</div>
							
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Panel Size:</label>
								<div class="col-lg-9">
									<select data-placeholder="Select a State..." name="komponen_size_update" id="komponen_size_update" class="form-control select-search">
										<option value=""></option>
										@foreach ($panel_size as $item)  
											<option value="{{ $item }}">{{ $item }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Total:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" name="totalUpdate" id="totalUpdate" placeholder="Total" value="1" onkeypress="return isNumberKey(event)">
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->