<style type="text/css">
@page {
    margin: 20 20 0 20;
}

.barcode {
	line-height: 10px;
	padding: 10px;
	
}

.barcode > img {
    width: 220px;
    height:61px;
}

table {
  border-collapse: collapse;
}

table, td {
  border: 1px solid black;
}


</style>
<div class="row">
@if(isset($data))
	@php($chunk = array_chunk($data->toArray(), 3))
	@foreach($chunk as $key => $value)
	<table>
		<tr>

			@if(isset($value[0]) ? $value[0]->id_table : null )
			<td>
				<div class="barcode">
					<center><h3 style="margin-bottom: 5px;">SPREADING</h3></center>
					<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[0]->id_table, 'C128',2,35) }}" alt="barcode"   />
					<center><h2>{{ $value[0]->table_name }}</h2></center>
				</div>
			</td>
			@endif

			@if(isset($value[1]) ? $value[1]->id_table : null )
			<td>
				<div class="barcode">
					<center><h3 style="margin-bottom: 5px;">SPREADING</h3></center>
					<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[1]->id_table, 'C128',2,35) }}" alt="barcode"   />
					<center><h2>{{ $value[1]->table_name }}</h2></center>
				</div>
			</td>
			@endif

			@if(isset($value[2]) ? $value[2]->id_table : null )
			<td>
				<div class="barcode">
					<center><h3 style="margin-bottom: 5px;">SPREADING</h3></center>
					<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value[2]->id_table, 'C128',2,35) }}" alt="barcode"   />
					<center><h2>{{ $value[2]->table_name }}</h2></center>
				</div>
			</td>
			@endif
		</tr>
	</table>

	@if(($key + 1) % 3 == 0 && ($key + 1) != count($chunk))
    <div class="page-break"></div>
    @endif
    @endforeach

@endif
</div>