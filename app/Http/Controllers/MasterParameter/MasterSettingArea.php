<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterSetting;
use DB;
use Auth;
use Excel;
use DataTables;
use Carbon\Carbon;

class MasterSettingArea extends Controller
{
    public function index(){
    	return view('master_parameter/setting_area/index');
    }

    public function getData(){
    	$data = MasterSetting::where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->orderBy('created_at','desc');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return '<button class="btn btn-primary" data-id="'.$data->id.'" onclick="edit(this);"><span class="icon-quill2"></span></button> <button class="btn btn-danger" data-id="'.$data->id.'" onclick="deleteId(this);"><span class="icon-trash"></span></button>';
    						})
    						->rawColumns(['action'])
    						->make(true);
    }

    public function addArea(Request $request){
    	$newarea= trim($request->new_area);
    	try {
    		
    		$cek = MasterSetting::where('area_name',$newarea)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();
    	
    		if ($newarea!=null && $cek==null) {
                    db::begintransaction();
	    			$dtin = array(
	    				'area_name'=>$newarea,
	    				'factory_id'=>Auth::user()->factory_id,
	    				'created_at'=>carbon::now()
	    			);

	    			MasterSetting::FirstOrCreate($dtin);
                    db::commit();
	    			$data_response = [
                            'status' => 200,
                            'output' => 'Add Area Success'
                          ];
	    	}else{
	    		$data_response = [
                            'status' => 422,
                            'output' => 'Add Area Fieled, area is already'
                          ];
	    	}
	    	

	    	
    	} catch (Exception $ex) {
    		db::rollback();
	        $message = $ex->getMessage();
	                  \ErrorHandler($message);

	        $data_response = [
	                            'status' => 422,
	                            'output' => 'Add Area failed !!!'
	                          ];
    	}
    	return response()->json(['data' => $data_response]);
    }

    public function getDataEdit(Request $request){
    	$data = MasterSetting::where('id',$request->id)->whereNull('deleted_at')->first();

    	return response()->json(['area_name'=>$data->area_name]);
    }

    public function setEditData(Request $request){
    	$id = $request->id;
    	$name = trim($request->name);

    	try {
    		db::begintransaction();
    		if ($name!=null) {

    			MasterSetting::where('id',$id)->update(['area_name'=>$name]);
    			$data_response = [
                            'status' => 200,
                            'output' => 'Edit Area Success'
                          ];
    		}else{
    			$data_response = [
                            'status' => 422,
                            'output' => 'Edit Area Field !!!'
                          ];
    		}
    		db::commit();
    	} catch (Exception $ex) {
    		db::rollback();
	        $message = $ex->getMessage();
	                  \ErrorHandler($message);

	        $data_response = [
	                            'status' => 422,
	                            'output' => 'Edit Area failed !!!'
	                          ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function deleteArea(Request $request){
        $id = $request->id;


        try {
            db::begintransaction();
          

                MasterSetting::where('id',$id)->update(['deleted_at'=>carbon::now()]);
                $data_response = [
                            'status' => 200,
                            'output' => 'Delete Area Success'
                          ];
            
            db::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
                      \ErrorHandler($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Delete Area failed !!!'
                              ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
