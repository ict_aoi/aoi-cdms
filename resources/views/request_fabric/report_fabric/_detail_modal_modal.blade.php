<div id="detailDetailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailDetailTable">
						<thead>
							<tr>
								<th class="dt_col_hide">Id</th>
								<th>Cut</th>
								<th>Color</th>
								<th>Rasio</th>
								<th>Layer</th>
								<th>Marker</th>
								<th>Part No</th>
								<th>Lebar FB</th>
								<th>Alokasi FB</th>
								<th>Perimeter</th>
								<th>Panjang Marker</th>
								<th>Over Consum</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="planning_id_detail" id="planning_id_detail" />
				<input type="hidden" name="part_no_detail" id="part_no_detail" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="postData">Post Data</button>
			</div>
		</div>
	</div>
</div>