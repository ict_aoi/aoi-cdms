<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ReverseDetailPackinglist extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'reverse_detail_packinglist';
	protected $fillable = ['packinglist_no','barcode_id','poreference','season','style','set_type','article','size','komponen_name','cut_num','start_no','end_no','qty','factory_id','factory_order','created_at','created_by','updated_at','updated_by','pl_factory_order','color_name','pl_subsys_id','no_polybag','deleted_at','deleted_by','detail_id','reverse_from_factory'];
}
