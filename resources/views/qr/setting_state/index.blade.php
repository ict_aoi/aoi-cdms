@extends('layouts.app',['active' => 'dashboard'])

@section('page-css')
<style>

</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Set State</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Set State</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <button class="btn btn-primary" id="btn-set-endline-qc">SET ENDLINE QC</button>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script>
    $(document).ready(function(){
        $('#btn-set-endline-qc').on('click', function() {
            localStorage.setItem('qr_scan_state', 'endline-qc');
        });
    });
</script>
@endsection
