<form class="form-horizontal" action="{{ route('masterparam.addKomponenProcessDetail') }}" id="form-adds" enctype="multipart/form-data">
@csrf
<div class="row loader-area">
    <input type="text" class="form-control hidden" id="id_style_transactions" name="id_style_transactions" value="{{$id_style_transactions}}">
    <div class="form-group">
        <div class="col-sm-4">
            <label for="komponen_name_">Komponen:</label>
            <input type="text" id="komponen_name_" name="komponen_name_" class="form-control" readonly>
        </div>
        <div class="col-sm-6">
            <label for="process_name_">Process:</label>
            <input type="text" id="process_name_" name="process_name_" class="form-control" readonly>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <label for="id_detail_process">Detail Process:</label>
            <select class="form-control select2" name="id_detail_process" id="id_detail_process" required>
                <option value=""></option>
                @foreach($process_detail as $proc)
                    <option value="{{ $proc->id }}" data-gsd="{{$proc->gsd_name}}" data-artwork="{{$proc->is_artwork}}">{{ $proc->detail_process_name }}</option>
                @endforeach
            </select>
            <input type="hidden" name="is_artwork" id="is_artwork" class="form-control">
        </div>
        <div class="col-sm-4">
            <label for="gsd_name">GSD:</label>
            <textarea class="form-control" name="gsd_name" id="gsd_name" cols="30" rows="3" readonly></textarea>
        </div>
        <div class="col-sm-1">
            <label for="lc" id="lbl_lc">LC:</label>
            <input type="text" id="lc" name="lc" class="form-control number text-right" autocomplete="off">
        </div>
        <div class="col-sm-1">
            <label for="gasa" id="lbl_gasa">Gasa:</label>
            <input type="text" id="gasa" name="gasa" class="form-control number text-right" autocomplete="off">
        </div>
        <div class="col-sm-1">
            <label for="total" id="lbl_total">Total:</label>
            <input type="text" id="total" name="total" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off">
        </div>
        <div class="col-sm-2 pull-right">
            <label for=""></label>
            <div class="input-group-btn"><br>
                <button type="submit" class="btn btn-primary" id="btnAdds">Add & Save</button>
            </div>
        </div>
    </div>
</div>
</form>
Style: <span id="spStyle"></span>
<hr>
<div class="table-responsive">
    <table class="table datatable-save-state" id="table-list-komponen-process-detail_">
        <thead>
            <tr>
                <th>#</th>
                {{-- <th>Style</th> --}}
                <th>Komponen</th>
                <th>Process</th>
                <th>Detail Process</th>
                <th>LC</th>
                <th>Gasa</th>
                <th>Total</th>
                <th>Col.Report</th>
                <th width="10px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessDetailJson') }}" id="get_detail_process_2"></a>

<script type="text/javascript">
    $('.select2').select2({
        placeholder: "Please select a State",
        allowClear: true
    });

    $('.number').keypress(function (event) {
        var $this = $(this);
    
        if ((event.which != 44 || $this.val().indexOf(',') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                        (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which == 188) && (text.indexOf(',') == -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf(',')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf(',') + 3));
                }
            }, 1);
        }

       /* if ((text.indexOf(',') != -1) &&
                (text.substring(text.indexOf(',')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }*/
    });

    $('.number').bind("paste", function (e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        if ($.isNumeric(text)) {
            if ((text.substring(text.indexOf(',')).length > 3) && (text.indexOf(',') > -1)) {
                e.preventDefault();
                $(this).val(text.substring(0, text.indexOf(',') + 3));
            }
        } else {
            e.preventDefault();
        }
    });

    $('#id_detail_process').change(function(){
        var gsd = $('option:selected', this).attr('data-gsd');
        var is_artwork = $('option:selected', this).attr('data-artwork');
        
        if($(this).val() != ''){
            $('#gsd_name').val(gsd);
            $('#is_artwork').val(is_artwork);

            //gak jadi
        /*    $('#lc').removeAttr('readonly');
            $('#gasa').removeAttr('readonly');
            $('#total').removeAttr('readonly');

            //jika is artwork true input total
            if(is_artwork=='1'){
                $('.dvartwork').removeClass('hidden');
                $('.dvnonartwork').addClass('hidden');

            }else{
                $('.dvnonartwork').removeClass('hidden');
                $('.dvartwork').addClass('hidden');

            }*/
        }else{
            $('#gsd_name').val('');
            $('#is_artwork').val('');
            //gak jadi
         /*   $('#lc').attr('readonly', true);
            $('#gasa').attr('readonly', true);
            $('#total').attr('readonly', true);
            
            $('.dvnonartwork').removeClass('hidden');
            $('.dvartwork').addClass('hidden');*/
        }
    });

    var table_komponens = $('#table-list-komponen-process-detail_').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            url: $('#get_detail_process_2').attr('href'),
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "id": $('#id_style_transactions').val(),
                    "_token": $("input[name='csrf-token']").val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
           // {data: 'style', name: 'style'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'process_name', name: 'process_name'},
            {data: 'detail_process_name', name: 'detail_process_name'},
            {data: 'lc', name: 'lc'},
            {data: 'gasa', name: 'gasa'},
            {data: 'total', name: 'total'},
            {data: 'column_name', name: 'column_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });


    //add new
    $('#form-adds').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_style_transactions', 'id_detail_process', 'lc', 'gasa', 'total'];
        var val = [
            $('#id_style_transactions').val(), 
            $('#id_detail_process').val(),
            $('#lc').val(),
            $('#gasa').val(),
            $('#total').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-adds').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#id_detail_process').val(null).trigger('change');
                $('#lc').val('');
                $('#gasa').val('');
                $('#total').val('');
                table_komponens.draw();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    function deleteKomponenProcessDetail(e){
        event.preventDefault();
        var id = $(e).data('id');
        var process_name = $(e).data('processdetail');
        if(id == '') {
            return false;
        }
        bootbox.confirm("Are you sure delete "+process_name+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-komponen-process-detail/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": $("input[name='csrf-token']").val(),
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table_komponens.ajax.reload();
                    }
                });
            }
        });
    }
</script>
