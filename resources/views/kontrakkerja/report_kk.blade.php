@extends('layouts.app',['active' => 'report_kk'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Kontrak Kerja </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Master Kontrak Kerja</li>
            <li class="active">Laporan Kontrak Kerja</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <label><b>Nomor Kontrak Kerja</b></label>
            <div class="input-group">
                <input type="text" class="form-control" name="no_kk" id="no_kk" required="" placeholder="Nomor Kontrak Kerja">
                
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
                <center><label><b>Nomor Kontrak Kerja</b></label></center>
                <input type="text" name="txno_kk" id="txno_kk" class="form-control" readonly="">
            </div>
            <div class="col-lg-2">
                <center><label><b>Subcont Name</b></label></center>
                <input type="text" name="txsubcont" id="txsubcont" class="form-control" readonly="">
            </div>
            <div class="col-lg-2">
                <center><label><b>Total Qty</b></label></center>
                <input type="text" name="txqty" id="txqty" class="form-control" readonly="">
            </div>
            <div class="col-lg-2">
                <center><label><b>Create By</b></label></center>
                <input type="text" name="txuser" id="txuser" class="form-control" readonly="">
            </div>
            <div class="col-lg-2">
                <button style="margin-top: 25px;" type="button" class="btn btn-success hidden" id="btn-print"><span class="icon-printer2"></span> <b>Print</b></button>
            </div>
        </div>

        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>PO Buyer</th>
                            <th>Style</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('MasterKK.exportLaporanKK') }}" id="exportKK"></a>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('MasterKK.getDataKK') }}",
            data: function(d) {
                 return $.extend({}, d, {
                     "no_kk"         : $('#no_kk').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'po_number', name: 'po_number'},
            {data: 'style', name: 'style'}
        ]
    });

    tableL
    .on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').on('click',function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url : "{{ route('MasterKK.getHeadKK') }}",
                data: {
                    no_kk: $('#no_kk').val(),
                },
                success: function(response) {
                    var data_response = response.data;
                    if (data_response.status==200) {
                        tableL.clear();
                        tableL.draw();

                        $('#txno_kk').val(data_response.nomorKK);
                        $('#txsubcont').val(data_response.subcont);
                        $('#txqty').val(data_response.total);
                        $('#txuser').val(data_response.user);
                        if ($('#btn-print').hasClass('hidden')) {
                            $('#btn-print').removeClass('hidden');
                        }
                        
                    }else{
                        $("#alert_warning").trigger("click",data_response.output);
                        if ($('#btn-print').hasClass('hidden')==false) {
                            $('#btn-print').addClass('hidden');
                        }
                    }
                    
                    
                },
                error: function(response) {
                    var data_response = response.data;
                    $("#alert_warning").trigger("click",data_response.output);
                    $.unblockUI();
                }
            });
    });

    $('#btn-print').on('click',function(){
        window.location.href = $('#exportKK').attr('href')+'?no_kk='+$('#txno_kk').val();
    });
});
</script>
@endsection