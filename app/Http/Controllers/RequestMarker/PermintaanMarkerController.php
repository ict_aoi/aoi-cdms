<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\InfoSync;
use App\Models\DetailPlan;
use App\Models\CuttingPlan;
use App\Models\RatioMarker;
use App\Models\CombinePart;
use App\Models\CuttingMarker;
use App\Models\RatioDetailTemp;
use App\Models\CuttingMovement;
use App\Models\SuggestCutTemp;
use App\Models\Data\DataCutting;

class PermintaanMarkerController extends Controller
{
    // menu permintaan marker manual
    public function index()
    {
        RatioDetailTemp::where('user_id', \Auth::user()->id)
            ->where('deleted_at', null)
            ->delete();

        return view('request_marker.permintaan_marker.index');
    }

    // get datatable
    public function getData(Request $request)
    {
        $date_cutting = $request->date_cutting;

        $data = CuttingPlan::where('cutting_date',$date_cutting)
            ->where('factory_id',Auth::user()->factory_id)
            ->where('is_header', true)
            ->whereNull('deleted_at')
            ->orderBy('queu','ASC');

        return DataTables::of($data)
        ->editColumn('size_category', function($data) {
            if ($data->size_category == 'I') {
                return 'Inter';
            } elseif ($data->size_category == 'A') {
                return 'Asian';
            } elseif ($data->size_category == 'J') {
                return 'Japan';
            } else {
                return 'None';
            }
        })
        ->addColumn('poreference', function($data) {
            if($data->header_id != null && $data->is_header) {
                $get_plan_id = CuttingPlan::where('header_id', $data->id)
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->toArray();

                return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray());
            } else {
                return implode(', ', $data->po_details->pluck('po_buyer')->toArray());
            }
        })
        ->addColumn('status',function($data){
            $dt = DB::table('cutting_marker')
                ->join('ratio','ratio.id_marker','=','cutting_marker.barcode_id')
                ->select('ratio')
                ->where('id_plan',$data->id)
                ->whereNull('ratio.deleted_at')
                ->whereNull('cutting_marker.deleted_at')
                ->count();

            $po_array = $data->po_details->pluck('po_buyer')->toArray();

            $check_plan = DB::table('data_cuttings')
                ->where('plan_id', $data->id)
                ->whereIn('po_buyer', $po_array)
                ->get();

            if (count($check_plan) < 1) {
                return '<span class="badge bg-danger">Geser Plan</span>';
            } elseif ($dt == 0) {
                $badge = '<span class="badge bg-warning">Belum Diupload</span>';
            }else{
                $badge = '<span class="badge bg-success">Sudah Diupload</span>';
            }
            return $badge;
        })
        ->addColumn('check_fb', function($data) {
            $dt = DB::table('cutting_marker')
                ->join('ratio','ratio.id_marker','=','cutting_marker.barcode_id')
                ->select('ratio')
                ->where('id_plan',$data->id)
                ->whereNull('ratio.deleted_at')
                ->whereNull('cutting_marker.deleted_at')
                ->count();

            if($dt > 0) {
                return view('request_marker.upload_req_marker._action', [
                    'data'      => $data,
                    'detail'  => '#',
                    'check_fb'  => route('permintaanMarker.getCheckFB',$data->id),
                    // 'check_alokasi'  => route('permintaanMarker.getCheckAlokasi',$data->id),
                    'delete'  => '#',
                ]);
            } else {
                return view('request_marker.upload_req_marker._action', [
                    'data'      => $data,
                    'check_fb'  => route('permintaanMarker.getCheckFB',$data->id)
                ]);
            }
        })
        ->rawColumns(['status','check_fb'])
        ->make(true);
    }

    // get datatable detail
    public function getDataDetail(Request $request)
    {
        $id_plan = $request->id_plan;

        $data = DB::table('cutting_marker')
            ->where('id_plan',$id_plan)
            ->whereNull('deleted_at')
            ->orderBy('part_no')
            ->orderBy('cut');

        return DataTables::of($data)
        ->editColumn('ratio',function($data){
            $grt = DB::table('ns_trig_ratio')
                ->select('ratio')
                ->where('id_marker',$data->barcode_id)
                ->first();
            return $grt->ratio;
        })
        ->editColumn('fabric_width',function($data){
            if (is_null($data->fabric_width)) {
                return '<button class="btn bg-slate btn-shoot"  id="btn-shoot" data-barcode="'.$data->barcode_id.'" data-plan="'.$data->id_plan.'" data-part="'.$data->part_no.'" onclick="shoot(this);"><span class="icon-hammer2"></span></button>';
            } else {
                if ($data->bay_pass == true) {
                    return '<span class="label label-warning label-rounded"> '.$data->fabric_width.' </span>';
                } else {
                    return $data->fabric_width;
                }
            }
        })
        ->editColumn('cut',function($data){
            if($data->suggest_cut == null) {
                return $data->cut.' (-)';
            } else {
                return $data->cut.' ('.$data->suggest_cut.')';
            }
        })
        ->addColumn('remark',function($data){
            if($data->is_new == null) {
                return null;
            } else {
                return 'LAPORAN BARU';
            }
        })
        ->addColumn('action',function($data){
            return '<button class="btn btn-danger btn-delpartno" id="btn-delpartno"  data-barcode="'.$data->barcode_id.'"  data-plan="'.$data->id_plan.'" data-part="'.$data->part_no.'" onclick="delpartno(this);"><span class="icon-trash"></span></button>';
        })
        ->setRowAttr([
            'style'=>function($data){
                if ($data->remark==true) {
                    return  'background-color: #00e6ac;';
                }
            }
        ])
        ->rawColumns(['fabric_width','ratio','action'])
        ->make(true);
    }

    // upload excel
    public function uploadExcel(Request $request)
    {
        $pcd          = Carbon::createFromFormat('d F, Y', $request->date_cut)->format('Y-m-d');
        $problem      = '';
        $problem_data = array();
        $results      = array();
        if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
                // $datas = \Excel::selectSheetsByIndex(1)->load($path)->get();
            }
        }

        try {
            DB::begintransaction();
            foreach ($datax as $dx) {
                $queu = trim($dx->queu);
                $style  = trim($dx->style);
                $article = trim($dx->article);
                $part = trim($dx->part);
                $cut = trim($dx->cut);
                $layer = trim($dx->layer);
                $fabric_width = trim($dx->fabric_width);
                $item_id = trim($dx->item_id);
                $is_body = trim($dx->body);
                $set_type = trim($dx->set);
                $is_laporan_baru = trim($dx->laporan_baru);
                $ratio = str_replace(" ","", $dx->ratio);

                if($is_body == 1) {
                    $is_body = true;
                } else {
                    $is_body = false;
                }

                if($is_laporan_baru == 1) {
                    $is_laporan_baru = 'editreport';
                } else {
                    $is_laporan_baru = null;
                }

                if($set_type == '') {
                    $set_type = 1;
                } else {
                    $get_type = DB::table('types')
                        ->where('type_name', $set_type)
                        ->first();

                    if($get_type == null) {
                        $set_type = 1;
                    } else {
                        $set_type = $get_type->id;
                    }
                }

                if ($queu!=''&&$style!=''&&$article!=''&&$part!=''&&$cut!=''&&$layer!=''&&$ratio!=''&&$fabric_width!=''&&$item_id!='') {

                    $queue = explode('+', $queu);

                    if(count($queue)<2){
                        $queue = $queue[0];
                        $check_plan = CuttingPlan::where('queu', $queue)
                        ->whereNull('deleted_at')
                        ->where('cutting_date',$pcd)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->first();

                        if($check_plan->header_id != null && $check_plan->is_header) {
                            $get_plan_id = CuttingPlan::where('header_id', $check_plan->header_id)
                                ->whereNull('deleted_at')
                                ->pluck('id')
                                ->toArray();

                        } else {
                            $get_plan_id[] = $check_plan->id;
                        }

                        $part_check = explode('+', $part);

                        $item_check = DataCutting::whereIn('plan_id',$get_plan_id)
                        ->where('warehouse',Auth::user()->factory_id)
                        ->whereIn('part_no',$part_check)
                        ->select(DB::raw("distinct item_id"))
                        ->pluck('item_id')
                        ->toArray();

                        // $item_check = DataCutting::where('cutting_date',$pcd)->where('style','like', '%'.$style.'%')
                        // ->where('articleno',$article)
                        // ->where('warehouse',Auth::user()->factory_id)
                        // ->whereIn('part_no',$part_check)
                        // ->select(DB::raw("distinct item_id"))
                        // ->pluck('item_id')
                        // ->toArray();

                        if (in_array($item_id, $item_check)){
                            $qtyratio = $this->qty_ratio($ratio);

                            $queue_check = explode('+', $queu);

                            // jika queue lebih dari 1
                            if(count($queue_check) > 1) {
                                $id_plan_array = array();
                                $validate_check = 0;
                                foreach($queue_check as $queue) {
                                    // $check_plan = $this->check_plan($queue,$style,$pcd,$article);
                                    $check_plan =  DB::table('ns_detail_cutplan')
                                        ->where('cutting_date',$pcd)
                                        ->where('style',$style)
                                        ->where('articleno',$article)
                                        ->where('queu',$queue)
                                        ->get();

                                    if(count($check_plan) > 0) {
                                        $id_plan = $check_plan->first()->id_plan;
                                    } else {
                                        $id_plan = null;
                                        $check_plan = null;
                                    }

                                    $id_plan_array[] = $id_plan;

                                    $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                                    if ($check_plan == null || $check_CutMark != null) {
                                        $validate_check++;
                                    }
                                }

                                if($validate_check == 0) {

                                    $newid = $this->id_marker($pcd);
                                    $next_val = DB::select("select nextval('combine_part_id') as nxt");

                                    $ratio_temp = explode(",",$ratio);
                                    $ratio_temp = implode(', ', $ratio_temp);

                                    $dt_cutmark = array(
                                        'barcode_id'=>$newid,
                                        'id_plan'=>$id_plan,
                                        'part_no'=>$part,
                                        'cut'=>$cut,
                                        'layer'=>$layer,
                                        'qty_ratio'=>$qtyratio,
                                        'fabric_width'=>$fabric_width,
                                        'item_id'=>$item_id,
                                        'created_at'=>carbon::now(),
                                        'updated_at'=>carbon::now(),
                                        'combine_id'=>$next_val[0]->nxt,
                                        'is_body'=>$is_body,
                                        'is_new'=>$is_laporan_baru,
                                        'layer_plan'=>$layer,
                                        'set_type'=>$set_type,
                                        'ratio_print'=>$ratio_temp,
                                        'created_by'=>\Auth::user()->id
                                    );

                                    DB::table('cutting_marker')->insert($dt_cutmark);

                                    $opRatio = explode(",",$ratio);

                                    foreach ($opRatio as $or) {
                                        $qr = explode("|",$or);

                                        $dt_ratio = array(
                                            'id_marker'=>$newid,
                                            'size'=>$qr[0],
                                            'ratio'=>(int)$qr[1],
                                            'created_at'=>carbon::now(),
                                            'updated_at'=>carbon::now()
                                        );

                                        RatioMarker::FirstOrCreate($dt_ratio);
                                    }

                                    foreach($id_plan_array as $id_plan) {
                                        CombinePart::FirstOrCreate([
                                            'plan_id' => $id_plan,
                                            'combine_id' => $next_val[0]->nxt,
                                            'deleted_at' => null
                                        ]);
                                    }

                                    CuttingMovement::firstOrCreate([
                                        'barcode_id' => $newid,
                                        'process_from' => null,
                                        'status_from' => null,
                                        'process_to' => 'preparation',
                                        'status_to' => 'onprogress',
                                        'is_canceled' => false,
                                        'user_id' => \Auth::user()->id,
                                        'description' => 'barcoding',
                                        'ip_address' => $this->getIPClient(),
                                        'barcode_type' => 'spreading',
                                        'deleted_at' => null
                                    ]);
                                }

                            // jika queue cuma ada 1
                            } else {
                                // $check_plan = $this->check_plan($queue_check[0],$style,$pcd,$article);

                                $check_plan =  DB::table('ns_detail_cutplan')
                                    ->where('cutting_date',$pcd)
                                    ->where('style',$style)
                                    ->where('articleno',$article)
                                    ->where('queu',$queue_check[0])
                                    ->where('factory_id',\Auth::user()->factory_id)
                                    ->get();

                                if(count($check_plan) > 0) {
                                    $id_plan = $check_plan->first()->id_plan;
                                } else {
                                    $id_plan = null;
                                    $check_plan = null;
                                }

                                $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                                if ($check_plan!=null && $check_CutMark==null) {
                                    $newid = $this->id_marker($pcd);

                                    $ratio_temp = explode(",",$ratio);
                                    $ratio_temp = implode(', ', $ratio_temp);

                                    // $items = DB::table('ns_master_items')->where('item_id',$item_id)->first();
                                    $dt_cutmark = array(
                                        'barcode_id'=>$newid,
                                        'id_plan'=>$id_plan,
                                        'part_no'=>$part,
                                        'cut'=>$cut,
                                        'layer'=>$layer,
                                        'layer_plan'=>$layer,
                                        'qty_ratio'=>$qtyratio,
                                        'fabric_width'=>$fabric_width,
                                        'item_id'=>$item_id,
                                        'created_at'=>carbon::now(),
                                        'updated_at'=>carbon::now(),
                                        'is_body'=>$is_body,
                                        'is_new'=>$is_laporan_baru,
                                        'set_type'=>$set_type,
                                        'ratio_print'=>$ratio_temp,
                                        'created_by'=>\Auth::user()->id
                                    );

                                    DB::table('cutting_marker')->insert($dt_cutmark);

                                    $opRatio = explode(",",$ratio);

                                    foreach ($opRatio as $or) {
                                        $qr = explode("|",$or);

                                        $dt_ratio = array(
                                            'id_marker'=>$newid,
                                            'size'=>$qr[0],
                                            'ratio'=>(int)$qr[1],
                                            'created_at'=>carbon::now(),
                                            'updated_at'=>carbon::now()
                                        );

                                        RatioMarker::FirstOrCreate($dt_ratio);
                                    }

                                    CuttingMovement::firstOrCreate([
                                        'barcode_id' => $newid,
                                        'process_from' => null,
                                        'status_from' => null,
                                        'process_to' => 'preparation',
                                        'status_to' => 'onprogress',
                                        'is_canceled' => false,
                                        'user_id' => \Auth::user()->id,
                                        'description' => 'barcoding',
                                        'ip_address' => $this->getIPClient(),
                                        'barcode_type' => 'spreading',
                                        'deleted_at' => null
                                    ]);
                                }
                            }
                            $error_info[] = 0;
                        }
                        else{
                            $error_info[] = 1;
                            if(in_array($queu.','.$part, $problem_data)){

                            }else{
                                $problem_data[] = $queu.','.$part;

                                $problem .= 'queue:'.$queu .' - part: ' . $part . '. ';
                            }
                        }
                    }
                    else{
                        $qtyratio = $this->qty_ratio($ratio);

                        $queue_check = explode('+', $queu);

                        // jika queue lebih dari 1
                        if(count($queue_check) > 1) {
                            $id_plan_array = array();
                            $validate_check = 0;
                            foreach($queue_check as $queue) {
                                // $check_plan = $this->check_plan($queue,$style,$pcd,$article);
                                $check_plan =  DB::table('ns_detail_cutplan')
                                    ->where('cutting_date',$pcd)
                                    ->where('style',$style)
                                    ->where('articleno',$article)
                                    ->where('queu',$queue)
                                    ->get();

                                if(count($check_plan) > 0) {
                                    $id_plan = $check_plan->first()->id_plan;
                                } else {
                                    $id_plan = null;
                                    $check_plan = null;
                                }

                                $id_plan_array[] = $id_plan;

                                $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                                if ($check_plan == null || $check_CutMark != null) {
                                    $validate_check++;
                                }
                            }

                            if($validate_check == 0) {

                                $newid = $this->id_marker($pcd);
                                $next_val = DB::select("select nextval('combine_part_id') as nxt");

                                $ratio_temp = explode(",",$ratio);
                                $ratio_temp = implode(', ', $ratio_temp);

                                $dt_cutmark = array(
                                    'barcode_id'=>$newid,
                                    'id_plan'=>$id_plan,
                                    'part_no'=>$part,
                                    'cut'=>$cut,
                                    'layer'=>$layer,
                                    'qty_ratio'=>$qtyratio,
                                    'fabric_width'=>$fabric_width,
                                    'item_id'=>$item_id,
                                    'created_at'=>carbon::now(),
                                    'updated_at'=>carbon::now(),
                                    'combine_id'=>$next_val[0]->nxt,
                                    'is_body'=>$is_body,
                                    'is_new'=>$is_laporan_baru,
                                    'layer_plan'=>$layer,
                                    'set_type'=>$set_type,
                                    'ratio_print'=>$ratio_temp,
                                    'created_by'=>\Auth::user()->id
                                );

                                DB::table('cutting_marker')->insert($dt_cutmark);

                                $opRatio = explode(",",$ratio);

                                foreach ($opRatio as $or) {
                                    $qr = explode("|",$or);

                                    $dt_ratio = array(
                                        'id_marker'=>$newid,
                                        'size'=>$qr[0],
                                        'ratio'=>(int)$qr[1],
                                        'created_at'=>carbon::now(),
                                        'updated_at'=>carbon::now()
                                    );

                                    RatioMarker::FirstOrCreate($dt_ratio);
                                }

                                foreach($id_plan_array as $id_plan) {
                                    CombinePart::FirstOrCreate([
                                        'plan_id' => $id_plan,
                                        'combine_id' => $next_val[0]->nxt,
                                        'deleted_at' => null
                                    ]);
                                }

                                CuttingMovement::firstOrCreate([
                                    'barcode_id' => $newid,
                                    'process_from' => null,
                                    'status_from' => null,
                                    'process_to' => 'preparation',
                                    'status_to' => 'onprogress',
                                    'is_canceled' => false,
                                    'user_id' => \Auth::user()->id,
                                    'description' => 'barcoding',
                                    'ip_address' => $this->getIPClient(),
                                    'barcode_type' => 'spreading',
                                    'deleted_at' => null
                                ]);
                            }

                        // jika queue cuma ada 1
                        } else {
                            // $check_plan = $this->check_plan($queue_check[0],$style,$pcd,$article);

                            $check_plan =  DB::table('ns_detail_cutplan')
                                ->where('cutting_date',$pcd)
                                ->where('style',$style)
                                ->where('articleno',$article)
                                ->where('queu',$queue_check[0])
                                ->get();

                            if(count($check_plan) > 0) {
                                $id_plan = $check_plan->first()->id_plan;
                            } else {
                                $id_plan = null;
                                $check_plan = null;
                            }

                            $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                            if ($check_plan!=null && $check_CutMark==null) {
                                $newid = $this->id_marker($pcd);

                                $ratio_temp = explode(",",$ratio);
                                $ratio_temp = implode(', ', $ratio_temp);

                                // $items = DB::table('ns_master_items')->where('item_id',$item_id)->first();
                                $dt_cutmark = array(
                                    'barcode_id'=>$newid,
                                    'id_plan'=>$id_plan,
                                    'part_no'=>$part,
                                    'cut'=>$cut,
                                    'layer'=>$layer,
                                    'layer_plan'=>$layer,
                                    'qty_ratio'=>$qtyratio,
                                    'fabric_width'=>$fabric_width,
                                    'item_id'=>$item_id,
                                    'created_at'=>carbon::now(),
                                    'updated_at'=>carbon::now(),
                                    'is_body'=>$is_body,
                                    'is_new'=>$is_laporan_baru,
                                    'set_type'=>$set_type,
                                    'ratio_print'=>$ratio_temp,
                                    'created_by'=>\Auth::user()->id
                                );

                                DB::table('cutting_marker')->insert($dt_cutmark);

                                $opRatio = explode(",",$ratio);

                                foreach ($opRatio as $or) {
                                    $qr = explode("|",$or);

                                    $dt_ratio = array(
                                        'id_marker'=>$newid,
                                        'size'=>$qr[0],
                                        'ratio'=>(int)$qr[1],
                                        'created_at'=>carbon::now(),
                                        'updated_at'=>carbon::now()
                                    );

                                    RatioMarker::FirstOrCreate($dt_ratio);
                                }

                                CuttingMovement::firstOrCreate([
                                    'barcode_id' => $newid,
                                    'process_from' => null,
                                    'status_from' => null,
                                    'process_to' => 'preparation',
                                    'status_to' => 'onprogress',
                                    'is_canceled' => false,
                                    'user_id' => \Auth::user()->id,
                                    'description' => 'barcoding',
                                    'ip_address' => $this->getIPClient(),
                                    'barcode_type' => 'spreading',
                                    'deleted_at' => null
                                ]);
                            }
                        }
                        $error_info[] = 0;
                    }
                }

            }

            DB::commit();
            if(in_array(1,$error_info)){
                $data_response = [
                    'status' => 200,
                    'output' => 'upload request marker success, tapi terdapat beda item_id pada '.$problem
                ];
            }
            else {
                $data_response = [
                    'status' => 200,
                    'output' => 'upload request marker success'
                ];
            }
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'upload request marker failed '.$message
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function getCheckAlokasi($id)
    {
        if(request()->ajax())
        {
            RatioDetailTemp::where('plan_id', $id)
                ->where('user_id', \Auth::user()->id)
                ->delete();

            $new_data = CuttingMarker::where('id_plan', $id)
                ->where('deleted_at', null)
                ->get();

            $last_po_buyer = '';

            // foreach marker
            foreach($new_data as $get_data_marker) {

                try {
                    DB::begintransaction();

                    $queue_list = array();
                    $plan_id_list = array();

                    if($get_data_marker->combine_id == null) {
                        $queue_list[] = $get_data_marker->cutting_plan->queu;
                        $plan_id_list[] = $get_data_marker->cutting_plan->id;
                    } else {
                        $plan_id_list = CombinePart::where('combine_id', $get_data_marker->combine_id)
                            ->where('deleted_at', null)
                            ->pluck('plan_id')
                            ->toArray();

                        foreach($plan_id_list as $planid) {
                            $get_data_plan = CuttingPlan::where('id', $planid)
                                ->where('deleted_at', null)
                                ->first();

                            $queue_list[] = $get_data_plan->queu;
                        }
                    }

                    // get informasi part in marker
                    $part_no = explode('+', $get_data_marker->part_no);

                    // set value sisa menjadi 0
                    $sisa = 0;

                    // get qty per po per part no per size -> planning
                    $get_list_po = DB::table('jaz_detail_size_per_part_2')
                        ->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))
                        ->whereIn('plan_id', $plan_id_list)
                        ->where('part_no', $part_no[0])
                        ->orderBy('deliv_date', 'asc')
                        ->orderBy('qty', 'asc')
                        ->groupBy('po_buyer','deliv_date','part_no')
                        ->get();

                    // foreach ratio size per marker
                    foreach($get_data_marker->rasio_markers as $b) {

                        $qty_marker_size = $get_data_marker->layer * $b->ratio;

                        foreach($get_list_po as $c) {
                            if($qty_marker_size > 0) {
                                $get_po_plot = RatioDetailTemp::where('plan_id', $get_data_marker->cutting_plan->id)
                                    ->where('size', $b->size)
                                    ->where('part_no', $c->part_no)
                                    ->where('po_buyer', $c->po_buyer)
                                    ->where('deleted_at', null)
                                    ->sum('qty');

                                $get_po_plan = DB::table('jaz_detail_size_per_part_2')
                                    ->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))
                                    ->whereIn('plan_id', $plan_id_list)
                                    ->where('part_no', $part_no[0])
                                    ->where('po_buyer', $c->po_buyer)
                                    ->where('size_finish_good', $b->size)
                                    ->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')
                                    ->first();

                                if($get_po_plan != null) {
                                    if($get_po_plot < $get_po_plan->qty) {
                                        if(($get_po_plan->qty - $get_po_plot) < $qty_marker_size) {
                                            RatioDetailTemp::FirstOrCreate([
                                                'barcode_id' => $get_data_marker->barcode_id,
                                                'plan_id' => $get_data_marker->cutting_plan->id,
                                                'ratio_id' => $b->id,
                                                'po_buyer' => $c->po_buyer,
                                                'size' => $b->size,
                                                'qty' => $get_po_plan->qty - $get_po_plot,
                                                'is_sisa' => 'f',
                                                'part_no' => $c->part_no,
                                                'user_id' => \Auth::user()->id,
                                                'factory_id' => \Auth::user()->factory_id,
                                            ]);

                                            $qty_marker_size = $qty_marker_size - ($get_po_plan->qty - $get_po_plot);

                                            $last_ratio_id = $b->id;
                                            $last_po_buyer = $c->po_buyer;
                                            $last_size = $b->size;
                                            $last_part_no = $c->part_no;
                                        } else {
                                            RatioDetailTemp::FirstOrCreate([
                                                'barcode_id' => $get_data_marker->barcode_id,
                                                'plan_id' => $get_data_marker->cutting_plan->id,
                                                'ratio_id' => $b->id,
                                                'po_buyer' => $c->po_buyer,
                                                'size' => $b->size,
                                                'qty' => $qty_marker_size,
                                                'is_sisa' => 'f',
                                                'part_no' => $c->part_no,
                                                'user_id' => \Auth::user()->id,
                                                'factory_id' => \Auth::user()->factory_id,
                                            ]);

                                            $qty_marker_size = 0;
                                        }
                                    } else {
                                        $last_po_buyer = $c->po_buyer;
                                    }
                                }
                            } else {
                                break;
                            }
                        }

                        if($qty_marker_size > 0) {
                            RatioDetailTemp::FirstOrCreate([
                                'barcode_id' => $get_data_marker->barcode_id,
                                'plan_id' => $get_data_marker->cutting_plan->id,
                                'ratio_id' => $b->id,
                                'po_buyer' => $last_po_buyer,
                                'size' => $b->size,
                                'qty' => $qty_marker_size,
                                'is_sisa' => 't',
                                'part_no' => $part_no[0],
                                'user_id' => \Auth::user()->id,
                                'factory_id' => \Auth::user()->factory_id,
                            ]);
                        }
                    }

                    DB::commit();
                }
                catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            $return_data = array();
            $return_data_temp = array();

            $get_header = DB::table('jaz_ratio_header_temp')
                ->where('plan_id', $id)
                ->where('user_id', \Auth::user()->id)
                ->get();

            $get_cut = CuttingMarker::select('cut')
                ->where('id_plan', $id)
                ->where('deleted_at', null)
                ->groupBy('cut')
                ->orderBy('cut', 'asc')
                ->pluck('cut')
                ->toArray();

            array_push($return_data_temp, 'PO Buyer', 'Part No', 'Size', 'Qty Order', 'Qty Total');

            foreach($get_cut as $cut) {
                $return_data_temp[] = 'Cut '.$cut;
            }

            $return_data_temp[] = 'Balance';

            $return_data[] = $return_data_temp;

            foreach($get_header as $header) {
                $return_data_temp = array();
                $return_data_cut_temp = array();
                $qty_total = 0;
                $detail_plan = CuttingPlan::where('id', $header->plan_id)
                    ->where('deleted_at', null)
                    ->first();

                $part_no = explode('+', $header->part_no);

                $qty_order = DB::table('jaz_detail_size_per_part_2')
                    ->where('plan_id', $detail_plan->id)
                    ->where('part_no', $part_no[0])
                    ->where('factory_id', $detail_plan->factory_id)
                    ->where('po_buyer', $header->po_buyer)
                    ->where('size_finish_good', $header->size)
                    ->first()->qty;

                array_push($return_data_temp, $header->po_buyer, $header->part_no, $header->size, $qty_order);
                foreach($get_cut as $cut) {
                    $get_detail = DB::table('jaz_ratio_detail_temp')
                        ->where('plan_id', $id)
                        ->where('user_id', \Auth::user()->id)
                        ->where('po_buyer', $header->po_buyer)
                        ->where('size', $header->size)
                        ->where('part_no', $header->part_no)
                        ->where('cut', $cut)
                        ->first();

                    if($get_detail != null) {
                        $return_data_cut_temp[] = $get_detail->qty;
                        $qty_total = $qty_total + $get_detail->qty;
                    } else {
                        $return_data_cut_temp[] = '';
                    }
                }
                $balance = $qty_total - $qty_order;
                $return_data_temp[] = $qty_total;
                $return_data_cut_temp[] = $balance;
                $return_result = array_merge($return_data_temp, $return_data_cut_temp);
                $return_data[] = $return_result;
            }

            $parts = CuttingMarker::select('part_no')
                ->where('id_plan', $id)
                ->groupBy('part_no')
                ->pluck('part_no')
                ->toArray();

            foreach($parts as $pr) {
                $list_marker = array();

                // get informasi part in marker
                $part_ = explode('+', $pr);

                // get qty per po per part no per size -> planning
                $get_list_po_ = DB::table('jaz_detail_size_per_part_2')
                    ->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))
                    ->where('plan_id', $id)
                    ->where('part_no', $part_[0])
                    ->orderBy('deliv_date', 'asc')
                    ->orderBy('qty', 'asc')
                    ->groupBy('po_buyer','deliv_date','part_no')
                    ->get();

                foreach($get_list_po_ as $bb) {
                    $get_suggest_cut = RatioDetailTemp::selectRaw('plan_id, barcode_id, sum(qty) as qty')
                        ->where('plan_id', $id)
                        ->where('part_no', $part_[0])
                        ->where('po_buyer', $bb->po_buyer)
                        ->whereNull('deleted_at')
                        ->groupBy('plan_id', 'barcode_id','po_buyer')
                        ->orderBy('qty', 'desc')
                        ->get();

                    foreach($get_suggest_cut as $cc) {
                        if(!in_array($cc->barcode_id, $list_marker)) {
                            $list_marker[] = $cc->barcode_id;
                        }
                    }
                }

                $suggest_no = 1;

                foreach($list_marker as $id_marker_) {
                    CuttingMarker::where('barcode_id', $id_marker_)->whereNull('deleted_at')->update([
                        'suggest_cut' => $suggest_no
                    ]);
                    $suggest_no++;
                }
            }

            return response()->json($return_data,200);
        }
    }

    public function deleteCheckAlokasi(Request $request)
    {
        if(request()->ajax())
        {
            RatioDetailTemp::where('plan_id', $request->id)
                ->where('user_id', \Auth::user()->id)
                ->whereNull('deleted_at')->delete();

            return response()->json('Delete data berhasil',200);
        }
    }

    public function getSizeBalance(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;

            if($planning_id != NULL) {
                $check_plan = CuttingPlan::where('id', $planning_id)
                    ->whereNull('deleted_at')
                    ->where('is_header', true)
                    ->first();

                if($check_plan == null) {
                    return response()->json('terjadi kesalahan data',422);
                } else {
                    if($check_plan->is_combine) {
                        $planning_ids = CuttingPlan::where('header_id', $planning_id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();
                        $part_no = CuttingMarker::select('part_no')
                            ->whereIn('id_plan', $planning_ids)
                            ->where('deleted_at', null)
                            ->groupBy('part_no')
                            ->orderBy('part_no')
                            ->pluck('part_no')
                            ->toArray();

                        foreach($part_no as $aa) {

                            $data_temp = array();
                            $data_rasio_size = array();
                            $data_final = array();
                            $data = array();
                            $data_final = array();

                            $data_marker = CuttingMarker::whereIn('id_plan', $planning_ids)
                                ->where('deleted_at', null)
                                ->where('part_no', $aa)
                                ->get();

                            $data_per_size = DB::table('jaz_detail_size_per_po_2')
                                ->select(DB::raw("size_finish_good, sum(qty) as qty"))
                                ->whereIn('plan_id', $planning_ids)
                                ->where('qty','>','0')
                                ->groupBy('size_finish_good')
                                ->orderByRaw('LENGTH(size_finish_good) asc')
                                ->orderBy('size_finish_good', 'asc')
                                ->get();

                            $data_temp[0][0] = 'Size';
                            $data_temp[1][0] = 'Total Kebutuhan';
                            $data_temp[2][0] = 'Total Marker';

                            for($i1 = 0; $i1 < 34; $i1++) {
                                if(isset($data_per_size[$i1])) {
                                    $data_temp[0][$i1 + 1] = $data_per_size[$i1]->size_finish_good;
                                    $data_temp[1][$i1 + 1] = $data_per_size[$i1]->qty;
                                } else {
                                    $data_temp[0][$i1 + 1] = null;
                                    $data_temp[1][$i1 + 1] = null;
                                }
                            }

                            foreach($data_marker as $a) {
                                foreach($a->rasio_markers as $b) {
                                    $data_rasio_size[] = [
                                        'size' => $b->size,
                                        'rasio' => $b->ratio * $a->layer,
                                    ];
                                }
                            }

                            for($i2 = 1; $i2 < 34; $i2++) {
                                $jumlah_qty = 0;
                                foreach($data_rasio_size as $a) {
                                    if($data_temp[0][$i2] != null) {
                                        if($a['size'] == $data_temp[0][$i2]) {
                                            $jumlah_qty += $a['rasio'];
                                        }
                                    }
                                }
                                if($jumlah_qty == 0) {
                                    $jumlah_qty = null;
                                }
                                $data_temp[2][$i2] = $jumlah_qty;
                            }

                            for($i3 = 0; $i3 < 3; $i3++) {
                                if($i3 == 2) {
                                    for($i4 = 0; $i4 < 34; $i4++) {
                                        if($i4 == 0) {
                                            $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                        } else {
                                            if($data_temp[$i3][$i4] != null) {
                                                if($data_temp[$i3][$i4] > $data_temp[$i3-1][$i4]) {
                                                    $data_temp[$i3][$i4] = '<div class="bg-danger">'.$data_temp[$i3][$i4].'</div>';
                                                } elseif($data_temp[$i3][$i4] < $data_temp[$i3-1][$i4]) {
                                                    $data_temp[$i3][$i4] = '<div class="bg-orange-300">'.$data_temp[$i3][$i4].'</div>';
                                                } else {
                                                    $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                                }
                                            } else {
                                                $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                            }
                                        }
                                    }
                                } else {
                                    for($i5 = 0; $i5 < 34; $i5++) {
                                        $data_temp[$i3][$i5] = $data_temp[$i3][$i5];
                                    }
                                }

                                $data[] = [
                                    'column1' => $data_temp[$i3][0],
                                    'column2' => $data_temp[$i3][1],
                                    'column3' => $data_temp[$i3][2],
                                    'column4' => $data_temp[$i3][3],
                                    'column5' => $data_temp[$i3][4],
                                    'column6' => $data_temp[$i3][5],
                                    'column7' => $data_temp[$i3][6],
                                    'column8' => $data_temp[$i3][7],
                                    'column9' => $data_temp[$i3][8],
                                    'column10' => $data_temp[$i3][9],
                                    'column11' => $data_temp[$i3][10],
                                    'column12' => $data_temp[$i3][11],
                                    'column13' => $data_temp[$i3][12],
                                    'column14' => $data_temp[$i3][13],
                                    'column15' => $data_temp[$i3][14],
                                    'column16' => $data_temp[$i3][15],
                                    'column17' => $data_temp[$i3][16],
                                    'column18' => $data_temp[$i3][17],
                                    'column19' => $data_temp[$i3][18],
                                    'column20' => $data_temp[$i3][19],
                                    'column21' => $data_temp[$i3][20],
                                    'column22' => $data_temp[$i3][21],
                                    'column23' => $data_temp[$i3][22],
                                    'column24' => $data_temp[$i3][23],
                                    'column25' => $data_temp[$i3][24],
                                    'column26' => $data_temp[$i3][25],
                                    'column27' => $data_temp[$i3][26],
                                    'column28' => $data_temp[$i3][27],
                                    'column29' => $data_temp[$i3][28],
                                    'column30' => $data_temp[$i3][29],
                                    'column31' => $data_temp[$i3][30],
                                    'column32' => $data_temp[$i3][31],
                                    'column33' => $data_temp[$i3][32],
                                    'column34' => $data_temp[$i3][33],
                                ];
                            }

                            $data_final[] = $aa;

                            $data_final[] = $data;

                            $data_final_fix[] = $data_final;
                        }
                    } else {
                        $part_no = CuttingMarker::select('part_no')
                            ->where('id_plan', $planning_id)
                            ->where('deleted_at', null)
                            ->groupBy('part_no')
                            ->orderBy('part_no')
                            ->pluck('part_no')
                            ->toArray();

                        foreach($part_no as $aa) {

                            $data_temp = array();
                            $data_rasio_size = array();
                            $data_final = array();
                            $data = array();
                            $data_final = array();

                            $data_marker = CuttingMarker::where('id_plan', $planning_id)
                                ->where('deleted_at', null)
                                ->where('part_no', $aa)
                                ->get();

                            $data_per_size = DB::table('jaz_detail_size_per_po_2')
                                ->select(DB::raw("size_finish_good, sum(qty) as qty"))
                                ->where('plan_id', $planning_id)
                                ->where('qty','>','0')
                                ->groupBy('size_finish_good')
                                ->orderByRaw('LENGTH(size_finish_good) asc')
                                ->orderBy('size_finish_good', 'asc')
                                ->get();

                            $data_temp[0][0] = 'Size';
                            $data_temp[1][0] = 'Total Kebutuhan';
                            $data_temp[2][0] = 'Total Marker';

                            for($i1 = 0; $i1 < 34; $i1++) {
                                if(isset($data_per_size[$i1])) {
                                    $data_temp[0][$i1 + 1] = $data_per_size[$i1]->size_finish_good;
                                    $data_temp[1][$i1 + 1] = $data_per_size[$i1]->qty;
                                } else {
                                    $data_temp[0][$i1 + 1] = null;
                                    $data_temp[1][$i1 + 1] = null;
                                }
                            }

                            foreach($data_marker as $a) {
                                foreach($a->rasio_markers as $b) {
                                    $data_rasio_size[] = [
                                        'size' => $b->size,
                                        'rasio' => $b->ratio * $a->layer,
                                    ];
                                }
                            }

                            for($i2 = 1; $i2 < 34; $i2++) {
                                $jumlah_qty = 0;
                                foreach($data_rasio_size as $a) {
                                    if($data_temp[0][$i2] != null) {
                                        if($a['size'] == $data_temp[0][$i2]) {
                                            $jumlah_qty += $a['rasio'];
                                        }
                                    }
                                }
                                if($jumlah_qty == 0) {
                                    $jumlah_qty = null;
                                }
                                $data_temp[2][$i2] = $jumlah_qty;
                            }

                            for($i3 = 0; $i3 < 3; $i3++) {
                                if($i3 == 2) {
                                    for($i4 = 0; $i4 < 34; $i4++) {
                                        if($i4 == 0) {
                                            $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                        } else {
                                            if($data_temp[$i3][$i4] != null) {
                                                if($data_temp[$i3][$i4] > $data_temp[$i3-1][$i4]) {
                                                    $data_temp[$i3][$i4] = '<div class="bg-danger">'.$data_temp[$i3][$i4].'</div>';
                                                } elseif($data_temp[$i3][$i4] < $data_temp[$i3-1][$i4]) {
                                                    $data_temp[$i3][$i4] = '<div class="bg-orange-300">'.$data_temp[$i3][$i4].'</div>';
                                                } else {
                                                    $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                                }
                                            } else {
                                                $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                            }
                                        }
                                    }
                                } else {
                                    for($i5 = 0; $i5 < 34; $i5++) {
                                        $data_temp[$i3][$i5] = $data_temp[$i3][$i5];
                                    }
                                }

                                $data[] = [
                                    'column1' => $data_temp[$i3][0],
                                    'column2' => $data_temp[$i3][1],
                                    'column3' => $data_temp[$i3][2],
                                    'column4' => $data_temp[$i3][3],
                                    'column5' => $data_temp[$i3][4],
                                    'column6' => $data_temp[$i3][5],
                                    'column7' => $data_temp[$i3][6],
                                    'column8' => $data_temp[$i3][7],
                                    'column9' => $data_temp[$i3][8],
                                    'column10' => $data_temp[$i3][9],
                                    'column11' => $data_temp[$i3][10],
                                    'column12' => $data_temp[$i3][11],
                                    'column13' => $data_temp[$i3][12],
                                    'column14' => $data_temp[$i3][13],
                                    'column15' => $data_temp[$i3][14],
                                    'column16' => $data_temp[$i3][15],
                                    'column17' => $data_temp[$i3][16],
                                    'column18' => $data_temp[$i3][17],
                                    'column19' => $data_temp[$i3][18],
                                    'column20' => $data_temp[$i3][19],
                                    'column21' => $data_temp[$i3][20],
                                    'column22' => $data_temp[$i3][21],
                                    'column23' => $data_temp[$i3][22],
                                    'column24' => $data_temp[$i3][23],
                                    'column25' => $data_temp[$i3][24],
                                    'column26' => $data_temp[$i3][25],
                                    'column27' => $data_temp[$i3][26],
                                    'column28' => $data_temp[$i3][27],
                                    'column29' => $data_temp[$i3][28],
                                    'column30' => $data_temp[$i3][29],
                                    'column31' => $data_temp[$i3][30],
                                    'column32' => $data_temp[$i3][31],
                                    'column33' => $data_temp[$i3][32],
                                    'column34' => $data_temp[$i3][33],
                                ];
                            }

                            $data_final[] = $aa;

                            $data_final[] = $data;

                            $data_final_fix[] = $data_final;
                        }
                    }
                }
            }
            return response()->json(['data' => $data_final_fix],200);
        }
    }

    public function getCheckFB($id)
    {
        $data = array();
        $part_no = array();
        $cutting_date_ = array();

        $get_plan = CuttingPlan::where('id', $id)->first();

        if($get_plan == null) {
            return response()->json('plan id tidak ditemukan',200);
        } else {

            if($get_plan->header_id != null && $get_plan->is_header) {
                $get_plan_id = CuttingPlan::where('header_id', $get_plan->header_id)
                    ->pluck('id')
                    ->toArray();

                $get_data_temp = DB::table('jaz_detail_size_per_part_2')
                    ->select('part_no', 'cutting_date')
                    ->whereIn('plan_id', $get_plan_id)
                    ->groupBy('part_no', 'cutting_date')
                    ->orderBy('part_no', 'asc')
                    ->get();

            } else {
                $get_plan_id = array($id);
                $get_data_temp = DB::table('jaz_detail_size_per_part_2')
                    ->select('part_no', 'cutting_date')
                    ->where('plan_id', $get_plan->id)
                    ->groupBy('part_no', 'cutting_date')
                    ->orderBy('part_no', 'asc')
                    ->get();
            }

            foreach($get_data_temp as $aa) {
                if(!in_array($aa->part_no, $part_no)) {
                    $part_no[] = $aa->part_no;
                }
                if(!in_array($aa->cutting_date, $cutting_date_)) {
                    $cutting_date_[] = $aa->cutting_date;
                }
            }

            $po_buyer = DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray();

            $style = $get_plan->top_bot != null ? $get_plan->style.'-'.$get_plan->top_bot : $get_plan->style;

            foreach($part_no as $a) {
                $data_temp = array();
                $data_lebar = array();
                $data_lebar_temp = array();

                // $get_data_fb = DB::connection('wms_live')
                //     ->table('integration_whs_to_cutting')
                //     ->select(DB::raw("actual_width, part_no, sum(qty_prepared) as qty_prepared"))
                //     ->whereIn('planning_date', $cutting_date_)
                //     ->where('style', $style)
                //     ->where('article_no', $get_plan->articleno)
                //     ->whereIn('po_buyer', $po_buyer)
                //     ->where('part_no', $a)
                //     ->groupBy('part_no', 'actual_width')
                //     ->get();

                $article        = $get_plan->articleno;
                $po             = "'".implode("','",$po_buyer)."'";
                $cut_date       = "'".implode("','",$cutting_date_)."'";
                $get_data_fb = DB::connection('wms_live_new')->select(DB::raw("SELECT actual_width,part_no,sum(qty_prepared) as qty_prepared FROM get_data_lebar_f(array[$cut_date]) WHERE style='$style' AND article_no ='$article' AND part_no = '$a' AND po_buyer IN($po) GROUP BY actual_width,part_no"));

                $sum_fb_prepared = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM get_data_lebar_f(array[$cut_date]) WHERE style='$style' AND article_no ='$article' AND part_no = '$a' AND po_buyer IN($po)"));

                $get_data_uom_csi = DB::table('data_cuttings')
                    ->whereIn('plan_id', $get_plan_id)
                    ->where('part_no', $a)
                    ->first()->uom;

                $get_data_csi = DB::table('data_cuttings')
                    ->whereIn('plan_id', $get_plan_id)
                    ->where('part_no', $a)
                    ->sum('fbc');

                if($get_data_uom_csi == 'M') {
                    $get_data_csi = $get_data_csi * 1.0936;
                }

                foreach($get_data_fb as $c) {
                    $data_lebar_temp = array();

                    $data_lebar_temp = [
                        'width' => $c->actual_width,
                        'qty' => $c->qty_prepared,
                    ];

                    $data_lebar[] = $data_lebar_temp;
                }

                // $blc_qty = $get_data_fb->sum('qty_prepared') - $get_data_csi;
                $blc_qty = $sum_fb_prepared[0]->qty_prepared - $get_data_csi;

                if($blc_qty > 0) {
                    $blc_qty = '<div class="bg-success">'.$blc_qty.'</div>';
                } elseif($blc_qty < 0) {
                    $blc_qty = '<div class="bg-danger">'.$blc_qty.'</div>';
                } else {
                    $blc_qty = $blc_qty;
                }

                $data_qty = [
                    'csi_qty' => $get_data_csi,
                    'whs_qty' => $sum_fb_prepared[0]->qty_prepared,
                    'blc_qty' => $blc_qty,
                ];

                $data_temp['part_no'] = $a;
                $data_temp['qty'] = $data_qty;
                $data_temp['lebar'] = $data_lebar;

                $data[] = $data_temp;
            }

            $obj = new StdClass();
            $obj->data = $data;

            return response()->json($obj,200);
        }
    }

    public function deleteRatio(Request $request)
    {
        $id_plan=$request->id_plan;
        $gcutmark = DB::table('cutting_marker')
        ->where('id_plan',$id_plan)
        ->whereNull('deleted_at')
        ->whereNull('perimeter')
        ->get();
        if(count($gcutmark) > 0){
            try {
                DB::begintransaction();
                    foreach ($gcutmark as $gcm) {
                        DB::table('cutting_marker')->where('barcode_id',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                        DB::table('ratio')->where('id_marker',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                        DB::table('ratio_detail')->where('barcode_id',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                    }
                    $data_response = [
                        'status' => 200,
                        'output' => 'Deleted Success'
                    ];
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                \ErrorHandler($message);
                $data_response = [
                    'status' => 422,
                    'output' => 'Deleted Failed '.$message
                ];
            }
            return response()->json(['data' => $data_response]);
        }else{
            $data_response = [
                'status' => 422,
                'output' => 'PERIMETER SUDAH TERISI / PLANNING DIHAPUS DARI SISTEM!'
            ];
            return response()->json(['data' => $data_response]);
        }
    }

    public function deletePartNo(Request $request)
    {
        $id_plan = $request->id_plan;
        $part_no = $request->part_no;
        $barcode = $request->barcode;

        try {
            DB::beginTransaction();
            $cek = DB::table('cutting_marker')
                ->where('barcode_id',$barcode)
                ->whereNull('perimeter')
                ->whereNull('marker_length')
                ->count();

            if ($cek > 0) {
                DB::table('cutting_marker')->where('barcode_id',$barcode)->whereNull('deleted_at')->update(['deleted_at'=>Carbon::now()]);
                DB::table('ratio')->where('id_marker',$barcode)->update(['deleted_at'=>Carbon::now()]);
                DB::table('ratio_detail')->where('barcode_id',$barcode)->update(['deleted_at'=>Carbon::now()]);
            } else {
                return response()->json('Deleted Field, marker have perimeter and marker length, please contact pola marker team!',422);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
        }

        return response()->json($data_response,200);
    }

    // function private
    private function check_CutMark($id_plan,$part,$cut)
    {
        $chek = db::table('cutting_marker')
            ->where('id_plan',$id_plan)
            ->where('part_no',$part)
            ->where('cut',$cut)
            ->whereNull('deleted_at')
            ->first();

        return $chek;
    }

    private function qty_ratio($ratio)
    {
        $erat = explode(",", $ratio);
        $qty = 0;

        if(count($erat) > 0) {
            foreach ($erat as $e) {
                if($e != null && $e != '') {
                    $qtyrat = explode("|", $e);
                    $qty = $qty+(int)$qtyrat[1];
                }
            }
        }

        return $qty;
    }

    private function id_marker($pcd)
    {
        $id_row_new = '';

        $data_cutting_all = DB::table('cutting_marker')
            ->where('barcode_id', 'like', 'MK-'.Carbon::now()->format('ymd').'%')
            ->orderBy('barcode_id', 'desc')
            ->get();

        if(count($data_cutting_all) > 0) {
            $id_row_new = substr($data_cutting_all->first()->barcode_id, 3, 12) + 1;
            $id_row_new = 'MK-'.$id_row_new;
        } else {
            $id_row_new = 'MK-'.Carbon::now()->format('ymd').'000001';
        }

        return $id_row_new;
   }

   private function getIPClient()
   {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
