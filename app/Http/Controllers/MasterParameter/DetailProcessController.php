<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class DetailProcessController extends Controller
{
    //MASTER DETAIL PROCESS
    public function masterDetailProcess()
    {
        $process = DB::table('master_process')
                        ->whereNull('deleted_at')
                        ->get();

        return view('master_parameter.detail_process.index', compact('process'));
    }

    public function ajaxGetDataMasterDetailProcess(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_detail_process')
                        ->select('master_detail_process.id', 'master_detail_process.detail_process_name', 'master_detail_process.gsd_name', 'master_detail_process.process_id', 'master_process.process_name', 'master_detail_process.id_column_report', 'master_column_report_rnd.column_name')
                        ->leftjoin('master_process', 'master_process.id', '=', 'master_detail_process.process_id')
                        ->leftjoin('master_column_report_rnd', 'master_column_report_rnd.id', '=', 'master_detail_process.id_column_report')
                        ->wherenull('master_detail_process.deleted_at')
                        ->wherenull('master_process.deleted_at')
                        ->orderBy('master_detail_process.created_at', 'desc')
                        ->get();

            //datatables
            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editDetailProcess', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteDetailProcess', $data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //add new master detail process
    public function addDetailProcess(Request $request)
    {

        $this->validate($request, [
            'detail_process_name' => 'required',
            'gsd_name' => 'required'
        ]);

        $detail_process_name = trim($request->detail_process_name);
        $gsd_name = trim($request->gsd_name);
        $process_id = $request->process_id;
        $id_column_report = $request->id_column_report;
        $id_column_report2 = $request->id_column_report2;

        $data = array([
            'detail_process_name' => $detail_process_name,
            'gsd_name' => $gsd_name,
            'process_id' => $process_id,
            'id_column_report' => $id_column_report,
            'id_column_report2' => $id_column_report2,
            'created_at' => Carbon::now()
        ]);

        // cek detail process exists
        $cekifexist = DB::table('master_detail_process')
                        ->where('detail_process_name', $detail_process_name)
                        ->where('gsd_name', $gsd_name)
                        ->where('process_id', $process_id)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master Detail Process already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table detail process
            DB::table('master_detail_process')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master detail process
    public function editDetailProcess($id)
    {
        $master_detail_process = DB::table('master_detail_process')
                        ->where('id', $id)
                        ->first();

        return response()->json($master_detail_process, 200);
    }

    //update master detail process
    public function updateDetailProcess(Request $request)
    {
        $data = $request->all();

        $detail_process_name = trim($data['detail_process_name_update']);
        $gsd_name = trim($data['gsd_name_update']);
        $process_id = $data['process_id_update'];
        $id_column_report = $data['id_column_report_update'];
        $id_column_report2 = $data['id_column_report2_update'];

        // cek exists
        // $cekifexist = DB::table('master_detail_process')
        //                 ->where('detail_process_name', $detail_process_name)
        //                 ->whereNull('deleted_at')
        //                 ->exists();
        
        // if ($cekifexist) {
        //     return response()->json('Master detail process already exists..!', 422);
        // }

        try {
            DB::beginTransaction();

                $update = DB::table('master_detail_process')
                                ->where('id', $data['id_update'])
                                ->update([
                                    'detail_process_name' => $detail_process_name,
                                    'gsd_name' => $gsd_name,
                                    'process_id' => $process_id,
                                    'id_column_report' => $id_column_report,
                                    'id_column_report2' => $id_column_report2,
                                    'updated_at' => Carbon::now()
                                ]);

                if ($update) {
                    // cek detail process exists
                    $cekifexist = DB::table('master_detail_process')
                                    ->where('detail_process_name', $detail_process_name)
                                    ->where('gsd_name', $gsd_name)
                                    ->where('process_id', $process_id)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cekifexist > 1) {
                        throw new \Exception('Master Detail Process already exists..!');
                    }
                }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('master_parameter.detail_process.index');

    }

    //delete master detail process
    public function deleteDetailProcess(Request $request)
    {
        $id = $request->id;
        DB::table('master_detail_process')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('master_parameter.detail_process.index');
    }

    //END OF MASTER DETAIL PROCESS
}
