@extends('layouts.app',['active' => 'masterDefectName'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Defect Name</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Defect Name</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group">
            <div class="row">
                <form class="form-horizontal" action="{{ route('masterparam.downloadMasterdefectname') }}" method="GET">
                    @csrf
                    <lable type="button" class="btn btn-xs btn-primary add_new">Add Defect Name <span class="icon-plus2"></span></lable>
                    <button type="submit" class="btn btn-success legitRipple pull-right" id="download_master">Export Excel <span class=" icon-file-excel"></span></button>
                </form>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr class="bg-teal">
                            <th style="width:10px;">NO</th>
                            <th>Defect Name (EN)</th>
                            <th>Defect Name (ID)</th>
                            <th>Is Repair</th>
                            <th>Is Reject</th>
                            <th>Cutting</th>
                            <th>Fuse</th>
                            <th>HE</th>
                            <th>Template</th>
                            <th>Bobok</th>
                            <th>Bonding</th>
                            <th>Pad Print</th>
                            <th>Join Label</th>
                            <th>PPA 1</th>
                            <th>PPA 2</th>
                            <th>Embro</th>
                            <th>Print</th>
                            <th style="width:10px;">ACTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('masterparam.getDataDefectName') }}" id="get_data"></a>
@endsection

@section('page-modal')
    @include('master_parameter.defect_name._add_modal')
    @include('master_parameter.defect_name._update_modal')
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            scrollY:true,
            scrollX:true,
            processing: true,
            serverSide: true,
            deferRender:true,
            pageLength:50,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
    
        var url = $('#get_data').attr('href');
        var table = $('#table-list').DataTable({
            ajax: url,
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false  },
                {data: 'nama_defect', name: 'nama_defect'},
                {data: 'nama_defect_eng', name: 'nama_defect_eng'},
                {data: 'is_repair', name: 'is_repair'},
                {data: 'is_reject', name: 'is_reject'},
                {data: 'cutting', name: 'cutting'},
                {data: 'fuse', name: 'fuse'},
                {data: 'heat_transfer', name: 'heat_transfer'},
                {data: 'template', name: 'template'},
                {data: 'bobok', name: 'bobok'},
                {data: 'bonding', name: 'bonding'},
                {data: 'pad', name: 'pad'},
                {data: 'join_label', name: 'join_label'},
                {data: 'ppa1', name: 'ppa1'},
                {data: 'ppa2', name: 'ppa2'},
                {data: 'embro', name: 'embro'},
                {data: 'printing', name: 'printing'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        var table = $('#table-list').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    table.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    table.search("").draw();
                }
                return;
        });
        table.on('preDraw',function(){
            Pace.start();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        })
        .on('draw.dt',function(){
            Pace.stop();
            $.unblockUI();
        });
        //end of datatables
    
        //add
        $('.add_new').on('click',function(){
            $('#modal_add_new_').modal('show')
        });
        //end of add
    
        $('#modal_add_new_').on('shown.bs.modal', function(){
            $('input[name=defect_name_id]').focus();
        });

        $('#modal_edit_').on('shown.bs.modal', function(){
            $('input[name=defect_name_id_update]').focus();
        });

        $('#modal_edit_').on('hidden.bs.modal', function(){
            $('#form-update').trigger("reset");
            $('#id_update').val(null).trigger('change');
            $('#defect_name_id_update').val('');
            $('#defect_name_en_update').val('');
        });
    
        //delete
        $("#table-list").on("click", ".delete", function() {
            event.preventDefault();
            var id = $(this).data('id');
            if(id == 'kosong') {
                return false;
            }
            var token = $(this).data("token");
            bootbox.confirm("Are you sure delete this row ?", function (result) {
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url: "master-defect-name/delete/"+id,
                        type: "GET",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        beforeSend: function () {
                            $('.loader-area').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        },
                        complete: function () {
                            $(".loader-area").unblock();
                        },
                        success: function () {
                            myalert('success','Deleted Successfully');
                            table.ajax.reload();
                        },
                        error: function(response) {
                            myalert('error','NOT GOOD');
                        }
                    });
                }
            });
        });
        //end of delete
    
        //add new
        $('#form-add').submit(function(event) {
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : "POST",
                url : $('#form-add').attr('action'),
                data: $('#form-add').serialize(),
                beforeSend: function () {
                    $('.loader-area').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                complete: function () {
                    $('.loader-area').unblock();
                },
                success: function(response) {
                    $('.loader-area').unblock();
                    $('#modal_add_new_').modal('hide');
                    myalert('success','DEFECT NAME SAVED!');
                    table.draw();
                },
                error: function(response) {
                    $('.loader-area').unblock();
                    $('#modal_add_new_').modal('hide');
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            })
        });

        //update
    $('#form-update').submit(function(event) {
        event.preventDefault();
        if($('#is_repair_update').is(':checked')){
            var is_repair_update = true; 
        }else{
            var is_repair_update = false; 
        }
        if($('#is_reject_update').is(':checked')){
            var is_reject_update = true; 
        }else{
            var is_reject_update = false; 
        }
        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'defect_name_id_update', 'defect_name_en_update','locator_update','is_repair_update','is_reject_update'];
        var val = [
            $('#id_update').val(), 
            $('#defect_name_id_update').val(),
            $('#defect_name_en_update').val(),
            $('#locator_update').val(),
            is_repair_update,
            is_reject_update
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                $.unblockUI();
                myalert('success','Data Updated');
                $('#modal_edit_').modal('hide');
                table.draw();
            },
            error: function(response) {
                $.unblockUI();
                myalert('error',response['responseJSON']);
            }
        })
    });

    $('#modal_edit_').on('hidden.bs.modal', function(){
        sesEditDone();
    });

});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        },error:function(response){
            $.unblockUI();
            if (response.status == 422){
                $("#alert_warning").trigger("click",response.responseJSON);
            }
            if (response.status == 500){
                $("#alert_warning").trigger("click",response.responseJSON.message);
            }
        }
    })
    .done(function (response) {
        var id = response.id;
        var defect_name_id = response.nama_defect;
        var defect_name_en = response.nama_defect_eng;
        var locator = response.locator;
        var is_repair = response.is_repair;
        var is_reject = response.is_reject;
        $('#id_update').val(id);
        $('#defect_name_id_update').val(defect_name_id);
        $('#defect_name_en_update').val(defect_name_en);
        if(locator != null && locator != ''){
            $.each(locator.split(","), function(i, v){
                $('#locator_update option[value='+ v +']').prop('selected', true).trigger('change');
            });
        }

        if(is_repair){
            $('#is_repair_update').prop('checked', true);
        }else{
            $('#is_repair_update').prop('checked', false);
        }
        if(is_reject){
            $('#is_reject_update').prop('checked', true);
        }else{
            $('#is_reject_update').prop('checked', false);
        }
        $('#modal_edit_').modal();
    });
}

function sesEditDone(){
    var user_id = "{{Auth::user()->id}}";
    $.ajax({
        url: "{{route('masterparam.deleteEditSession')}}",
        type: "GET",
        data: {
            user_id: user_id,
        },
        beforeSend: function () {
            $('.loader-area').block({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        },
        complete: function () {
            $(".loader-area").unblock();
        },
        success: function () {
            $(".loader-area").unblock();
            myalert('success','Update Successfully!');
        },
        error: function(response) {
            $(".loader-area").unblock();
            myalert('error','TERJADI KESALAHAN HARAP REFRESH HALAMAN INI');
        }
    });
}

</script>
@endsection




