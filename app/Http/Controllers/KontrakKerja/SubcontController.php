<?php

namespace App\Http\Controllers\KontrakKerja;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Excel;
use Carbon\Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use DataTables;

use App\Models\PackingSubTemp;
use App\Models\PackinglistSubcont;


class SubcontController extends Controller
{
    public function scanOut(){
    	$pl_sub = DB::table('packinglist_subcont')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->orderBy('created_at','desc')->get();
    	return view('kontrakkerja/scan_subcont/scanout')->with('pl_sub',$pl_sub);
    }

    public function ajaxScanOut(Request $request){
    	$barcode = $request->barcode;
    	$nopl = $request->nopl;
    	$nobag = $request->nobag;

    	$cekbun = $this->_cek_bundle($barcode,$nopl);
    	$cekpl = $this->_cek_packinglist($barcode);
    	if ($cekbun['permission']==true) {
    		if ($cekpl==true) {
    			try {
	    			db::begintransaction();

	    			$datain = array(
	    				'id_packinglist'=>$nopl,
	    				'no_polybag'=>$nobag,
	    				'bundle_id'=>$barcode,
	    				'qty'=>$cekbun['qty'],
	    				'user_id'=>Auth::user()->id,
	    				'factory_id'=>Auth::user()->factory_id
	    			);
	    				PackingSubTemp::FirstOrCreate($datain);
	    			db::commit();
	    			$data_response = [
	                            'status' => 200,
	                            'output' => 'scan bundle success'
	                          ];
	    		} catch (Exception $ex) {
	    			db::rollback();
		            $message = $ex->getMessage();
		            \ErrorHandler($message);

		            $data_response = [
		                                'status' => 422,
		                                'output' => 'scan bundle gagal !!!'
		                              ];
	    		}
    		}else{
    			$data_response = [
	                                'status' => 422,
	                                'output' => 'scan bundle failed, bundel sudah pernah discan !!!'
	                              ];
    		}
    		
    	}else{
    		$data_response = [
	                                'status' => 422,
	                                'output' => 'scan bundle failed, PO buyer tidak ada di packinglist !!!'
	                              ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function getDataTemp(){
    	$data = PackingSubTemp::where('factory_id',Auth::user()->factory_id)->where('user_id',Auth::user()->id)->whereNull('deleted_at');

    	return DataTables::of($data)
    						->addColumn('action',function($data){
    							return '<button class="btn btn-danger" data-id="'.$data->id.'" onclick="delbarcode(this);"><span class="icon-trash"></span></button>';
    						})
    						->make(true);
    }

    public function deleteBundleTmp(Request $request){
    	$id = $request->id;

    	try {
    		db::begintransaction();
 					PackingSubTemp::where('id',$id)->delete();
    		db::commit();
    			$data_response = [
	                            'status' => 200,
	                            'output' => 'delete bundle success'
	                          ];
    	} catch (Exception $ex) {
    		db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'detele bundle gagal !!!'
                              ];
    	}

    	return response()->json(['data'=>$data_response]);
    }

    public function submitPackinglist(Request $request){
    	$data = PackingSubTemp::where('user_id',Auth::user()->id)->where('factory_id',Auth::user()->factory_id)->get();

        $ip_address = $request->ip();
        $m_locator = DB::table('master_locator')->where('is_artwork',true)->whereNull('deleted_at')->first();

        try {
            db::begintransaction();
                foreach ($data as $dt) {
                    $dt_sub = array(
                        'id_packinglist'=>$dt->id_packinglist,
                        'no_polybag'=>$dt->no_polybag,
                        'bundle_id'=>$dt->bundle_id,
                        'qty'=>$dt->qty,
                        'user_id'=>$dt->user_id,
                        'factory_id'=>$dt->factory_id,
                        'created_at'=>$dt->created_at,
                        'updated_at'=>$dt->updated_at
                    );

                    PackinglistSubcont::FirstOrCreate($dt_sub);
                    PackingSubTemp::where('id',$dt->id)->delete();
                    $this->bundel_move($dt->bundle_id,'IN '.$m_locator->locator_name,$ip_address,$m_locator->id,'in');
                }
            db::commit();
             $data_response = [
                                'status' => 200,
                                'output' => 'submit success . . .'
                              ];
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
            $data_response = [
                                'status' => 422,
                                'output' => 'Submit gagal !!!'
                              ];
        }

    	return response()->json(['data'=>$data_response]);
    }

    public function getDataPl(Request $request){
        $nopl = $request->nopl;
        $prosname = array();
        $dt_pl = DB::table('packinglist_subcont')->where('no_packinglist',$nopl)->Where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();
        $arrpros = explode(", ",$dt_pl->process);
        $dt_pros = DB::table('master_process')->WhereIn('id',$arrpros)->whereNull('deleted_at')->get();
        foreach ($dt_pros as $dt) {
            $prosname[]=$dt->process_name;
        }

        $data_response = array(
            'nopl'=>$dt_pl->no_packinglist,
            'nokk'=>$dt_pl->no_kk,
            'style'=>$dt_pl->style,
            'po_buyer'=>$dt_pl->po_buyer,
            'subcont'=>$dt_pl->subcont,
            'process'=>implode(", ",$prosname)
        );

        return response()->json(['data'=>$data_response]);
        
    }

    private function _cek_bundle($barcode,$nopl){
    	$barcode = DB::table('bundle_detail')
    					->join('bundle_header','bundle_detail.bundle_header_id','=','bundle_header.id')
    					->where('bundle_detail.barcode_id',$barcode)
    					->where('bundle_header.factory_id',Auth::user()->factory_id)
    					->select('bundle_header.poreference','bundle_detail.qty')->first();
        
                        $packing = DB::table('packinglist_subcont')->where('no_packinglist',$nopl)->where('factory_id', Auth::user()->factory_id)->where('po_buyer','LIKE','%'.$barcode->poreference.'%')->exists();
                        
                        // dd($barcode, $nopl);

    	$allow = array(
    		'permission'=>$packing,
    		'qty'=>$barcode->qty
    	);

    	return $allow;
    }

    private function _cek_packinglist($barcode){
    	$cektemp = DB::table('detail_packinglist_subtemp')->where('bundle_id',$barcode)->whereNull('deleted_at')->exists();
    	$cekpldet = DB::table('detail_packinglist_subcont')->where('bundle_id',$barcode)->whereNull('deleted_at')->exists();

    	if ($cektemp==false && $cekpldet==false) {
    		$permission = true;
    	}else{
    		$permission = false;
    	}

    	return $permission;
    }

    private function bundel_move($barcode,$desc,$ip_address,$locator,$status){
        $cek_move = DistribusiMovement::where('barcode_id',$barcode)->orderBy('created_at','desc')->first();
        $dt_in = array(
            'barcode_id'=>$barcode,
            'locator_from'=>$cek_move->locator_to,
            'status_from'=>$cek_move->status_to,
            'locator_to'=>$locator,
            'status_to'=>$status,
            'user_id'=>Auth::user()->id,
            'description'=>$desc,
            'ip_address'=>$ip_address,
            'loc_dist'=>'-'
        );
        DistribusiMovement::FirstOrCreate($dt_in);

    }
}
