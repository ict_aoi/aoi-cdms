<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TempScanCutting extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_scan_cuttings';

	protected $fillable = ['barcode_marker', 'barcode_table', 'created_at', 'updated_at', 'factory_id', 'deleted_at','scan_cutting_id', 'start_at'];

	public function marker()
    {
        return $this->belongsTo('App\Models\CuttingMarker', 'barcode_marker', 'barcode_id');
    }
}
