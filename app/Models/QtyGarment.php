<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class QtyGarment extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'qty_garment';
	protected $fillable = ['id_plan','size','qty','deleted_at','created_at','updated_at'];
}
