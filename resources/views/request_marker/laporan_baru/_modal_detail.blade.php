<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Spreading Report Input - <span id="detail_view_barcode_marker">Data Empty!</span> <strong><span id="detail_view_notes"></span></strong></span>
				</legend>
			</div>
            <br />
            <div class="modal-header">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="headerTable">
						<tbody>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td><span id="detail_view_style">Data Empty!</span></td>
                                <td><strong>Cut</strong></td>
                                <td><span id="detail_view_cut">Data Empty!</span></td>
                                <td><strong>Item</strong></td>
                                <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Deliv</strong></td>
                                <td><span id="detail_view_deliv">Data Empty!</span></td>
                                <td><strong>Marker Width (inch)</strong></td>
                                <td><span id="detail_view_width">Data Empty!</span></td>
                                <td rowspan="2"><strong>Color</strong></td>
                                <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Article</strong></td>
                                <td><span id="detail_view_article">Data Empty!</span></td>
                                <td><strong>Marker Length (inch)</strong></td>
                                <td><span id="detail_view_length">Data Empty!</span></td>
                                <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Marker Pcs</strong></td>
                                <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                                <td><strong>Total Length (+allow)</strong></td>
                                <td><span id="detail_view_allow">Data Empty!</span></td>
                                <td><strong>Part</strong></td>
                                <td><span id="detail_view_part">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Total Grmt</strong></td>
                                <td><span id="detail_view_total_garment">Data Empty!</span></td>
                                <td><strong>layer</strong></td>
                                <td><span id="detail_view_layer">Data Empty!</span></td>
                                <td><strong>Plan</strong></td>
                                <td><span id="detail_view_plan">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Po Buyer</strong></td>
                                <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                            </tr>
                        </tbody>
					</table>
				</div>
            </div>
            <div class="modal-body">
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
		</div>
	</div>
</div>