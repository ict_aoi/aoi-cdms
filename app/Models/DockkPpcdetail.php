<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DockkPpcdetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'doc_kk_ppc_detail';
	protected $fillable = ['id','season','style','set_type','panel','remark','qty','allowance','qty_plus_allowance','price_ori','price_estimated','currency_rate','idr_price','total_amount','kk_no','kk_type','factory_id','created_by','created_at','updated_by','updated_at','deleted_at','deleted_by','document_no','document_type','doc_ppc_header_id'];
}
