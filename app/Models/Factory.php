<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    protected $table = 'master_factory';
	protected $fillable = ['factory_name','factory_alias','deleted_at','status'];

    public function users()
    {
        return $this->hasMany('App\Models\User', 'factory_id', 'id');
    }
    
    public function cutting_plan()
    {
        return $this->hasMany('App\Models\CuttingPlan', 'factory_id', 'id');
    }

    public function scan_cutting()
    {
        return $this->hasMany('App\Models\ScanCutting', 'factory_id', 'id');
    }

    public function release_marker()
    {
        return $this->hasMany('App\Models\MarkerReleaseHeader', 'factory_id', 'id');
    }
}
