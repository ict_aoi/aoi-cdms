<!-- MODAL ADD -->
<div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.addDefectName') }}" id="form-add">
            <div class="modal-content">
				<div class="modal-body">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="panel-body loader-area">
						<fieldset>
							<legend class="text-semibold">
								<i class="icon-file-text2 position-left"></i>
								<span id="title"> ADD DEFECT NAME</span> <!-- title -->
							</legend>

							<div class="form-group">
								<label class="col-lg-4 control-label text-semibold">DEFECT NAME (ID):</label>
								<div class="col-lg-8">
									<input type="text" class="form-control text-uppercase" name="defect_name_id" placeholder="Name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-4 control-label text-semibold">DEFECT NAME (EN):</label>
								<div class="col-lg-8">
									<input type="text" class="form-control text-uppercase" name="defect_name_en" placeholder="Name" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-4 control-label text-semibold">LOCATOR:</label>
								<div class="col-lg-8" style="background-color:#abf6f7">
									<select multiple data-placeholder="Select a locator" class="form-control select-search" name="locator[]" id="locator" required style="padding-bottom:4px">
										<option value=""></option>
										@foreach($locator as $x)
											<option value="{{ $x->id }}">{{ $x->process_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="row text-center">
									<div class="col-md-4">
										<label class="col-sm-7 control-label text-semibold">Is Repair</label>
										<div class="col-sm-5" style="padding-top:9px;padding-bottom:9px">
											<input type="checkbox" name="is_repair" id="is_repair">
										</div>
									</div>
									<div class="col-md-4">
										<label class="col-sm-7 control-label text-semibold">Is Reject</label>
										<div class="col-sm-5" style="padding-top:9px;padding-bottom:9px">
											<input type="checkbox" name="is_reject" id="is_reject">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
							</div>
						</fieldset>
					</div>
				</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD -->