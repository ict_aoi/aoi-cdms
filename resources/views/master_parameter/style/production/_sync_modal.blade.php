<!-- MODAL -->
<div id="modal_sync_" class="modal fade" role="dialog">
    <div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title-style"><i class="position-left"></i>Choose Style</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <hr>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-semibold">Season:</label>
                                <div class="col-sm-9">
                                    <select data-placeholder="Select a State..." name="season_sync" id="season_sync" class="form-control select-search" required>
                                        <option value=""></option>
                                        @foreach ($seasons as $item)  
                                            <option value="{{ $item->id }}">{{ $item->season_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
					</div>
                  	<div class="panel-body loader-area">
						<fieldset>
                            <input type="hidden" id="style_sync" name="style_sync">
                            <div id="listing" class="col-lg-12">
                                <!-- template pilihan -->
                            </div>
                        </fieldset>
                  	</div>
              	</div>
            </div>
    </div>
</div>
<!-- /MODAL -->