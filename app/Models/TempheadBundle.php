<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TempheadBundle extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_head_bundle';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['id','season','style','set_type','cut_num','poreference','article','size','qty','que_no','input_time','user_nik','factory_id','deleted_at','deleted_by','updated_by','ed_pcd','created_by'];
}
