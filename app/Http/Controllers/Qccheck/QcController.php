<?php

namespace App\Http\Controllers\Qccheck;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use StdClass;
use File;
use Illuminate\Support\Facades\Storage;
use Excel;
use App\Models\CuttingMarker;
use App\Models\MasterDefectEndline;

use App\Models\ScanCutting;
use App\Models\QcCheck;
use App\Models\QcBundle;
use App\Models\DetailQcBundle;

class QcController extends Controller
{
    public function index(){
            $barcode_id = DB::table('scan_cuttings')->select('barcode_marker')->where('state','done')->whereNull('deleted_at')->get();
            return view('qccheck/qccheck')->with('barcode',$barcode_id);
    }

    public function getData(Request $req){
            $barcode_marker = $req->barcode_id;

            if (trim($barcode_marker)!=null) {


                $data = DB::table('cutting_marker')->join('ratio','ratio.id_marker','=','cutting_marker.barcode_id')
                                        ->join('scan_cuttings','scan_cuttings.barcode_marker','=','cutting_marker.barcode_id')
                                        ->where('cutting_marker.barcode_id',$barcode_marker)
                                        ->where('scan_cuttings.factory_id',Auth::user()->factory_id)
                                        ->where('scan_cuttings.state','done')
                                        ->whereNull('ratio.deleted_at')
                                        ->whereNull('cutting_marker.deleted_at')
                                        ->whereNull('scan_cuttings.deleted_at')
                                        ->orderBy('ratio.size')
                                        ->select('ratio.size','ratio.ratio','cutting_marker.barcode_id','cutting_marker.layer');


        }else{
            $data = array();
        }

        return DataTables::of($data)
        ->addColumn('action',function($data){
            return '<a href="#" data-barcode="'.$data->barcode_id.'" data-size="'.$data->size.'" class="btn btn-primary btn-detail" id="btn-detail"><span class="icon-list"></span></a> ';
        })
        ->addColumn('qty',function($data){
            return $data->ratio * $data->layer;
        })
        ->rawColumns(['action','qty'])
        ->make(true);
    }

    public function indexQR($id){
        $data_id = $id;
        return view('qccheck.qr.qccheck', compact('data_id'));
    }

    public function getHeader(Request $req){
        $barcode_id = $req->barcode_id;

        $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->whereNull('deleted_at')->first();
        $part_no = explode('+', $data_marker->part_no);

        $get_data_cutting = DB::table('data_cuttings')->select('style')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no)->groupBy('style')->get();

        $style_check = explode('-', $get_data_cutting->first()->style);

        $set_type = 'Non';

        if(count($style_check) > 1) {
            if($style_check[1] == 'TOP') {
                $set_type = 'TOP';
            } elseif ($style_check[1] == 'BOT') {
                $set_type = 'BOTTOM';
            }
        }

        if(count($get_data_cutting) == 1) {
            $style = $get_data_cutting->first()->style;
        } else {
            $style = $style_check[0];
        }

        $dhead = DB::table('cutting_marker')
                    ->join('cutting_plans2','cutting_plans2.id','=','cutting_marker.id_plan')
                    ->join('ns_detail_cutplan','ns_detail_cutplan.id_plan','=','cutting_plans2.id')
                    ->join('fabric_useds','fabric_useds.barcode_marker','=','cutting_marker.barcode_id')
                    ->where('cutting_marker.barcode_id',$barcode_id)
                    ->whereNull('cutting_marker.deleted_at')
                    ->whereNull('cutting_plans2.deleted_at')
                    ->whereNull('fabric_useds.deleted_at')
                    ->groupby('cutting_marker.cut','cutting_marker.color','cutting_plans2.style','ns_detail_cutplan.po_buyer','fabric_useds.po_supplier','fabric_useds.item_code','fabric_useds.color')
                    ->select('cutting_marker.cut','cutting_marker.color','cutting_plans2.style','ns_detail_cutplan.po_buyer','fabric_useds.po_supplier','fabric_useds.item_code','fabric_useds.color')
                    ->first();

        return response()->json(['data'=>$dhead, 'style_replace' => $style]);
    }

    public function getDetail(Request $req){
        $style = $req->style;
        $size = $req->size;
        $barcode_id = $req->barcode;

        $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->whereNull('deleted_at')->first();
        $part_no = explode('+', $data_marker->part_no);

        $get_data_cutting = DB::table('data_cuttings')->select('style')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no)->groupBy('style')->get();

        $style_check = explode('-', $get_data_cutting->first()->style);

        $set_type = 'Non';

        if(count($style_check) > 1) {
            if($style_check[1] == 'TOP') {
            $set_type = 'TOP';
            } elseif ($style_check[1] == 'BOT') {
            $set_type = 'BOTTOM';
            }
        }

        $data = DB::table('ns_v_qc_cutting')
                        ->where('barcode_id',$barcode_id)
                        ->where('style',$style)
                        ->where('m_size',$size);

        if(count($get_data_cutting) == 1) {
            $data->where('set_type', $set_type);
        }

        return DataTables::of($data)
        ->addColumn('id',function($data){
            return '<label>'.$data->komponen.'</label>';
        })
        ->editColumn('komponen_name',function($data){
            return $data->komponen_name;
        })
        ->editColumn('qty',function($data){

            return '<input type="number" min=0 id="qty" class="form-control" value="'.$data->qty.'">';
        })
        ->addColumn('table',function($data){
            $table = DB::table('bundle_table')->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->get();
            $op = '';
            $sel = "";

            foreach ($table as $tb) {
                if ($tb->id_table==$data->bundle_table) {
                $sel = "selected";
                }else{
                $sel="";
                }

                $op = $op.' <option value="'.$tb->id_table.'" '.$sel.'>'.$tb->name.'</option>';
            }

            return '<select class="form-control">
                    <option value="" >--Choose Table--</option>
                    '.$op.'
            </select>';
        })
        ->editColumn('bowing',function($data){

            return '<input type="number" min=0 id="bowing" class="form-control" value="'.$data->bowing.'">';
        })
        ->editColumn('sliding',function($data){

            return '<input type="number" min=0 id="sliding" class="form-control" value="'.$data->sliding.'">';
        })
        ->editColumn('pilling',function($data){

            return '<input type="number" min=0 id="pilling" class="form-control" value="'.$data->pilling.'">';
        })
        ->editColumn('snag',function($data){

            return '<input type="number" min=0 id="snag" class="form-control" value="'.$data->snag.'">';
        })
        ->editColumn('siult',function($data){

            return '<input type="number" min=0 id="siult" class="form-control" value="'.$data->siult.'">';
        })
        ->editColumn('crease_mark',function($data){
            return '<input type="number" min=0 id="crease_mark" class="form-control" value="'.$data->crease_mark.'">';
            // return '<input type="number" min=0 id="crease_mark" class="form-control" value="'.$data->crease_mark.'">';
        })
        ->editColumn('foresign_yam',function($data){

            return '<input type="number" min=0 id="foresign_yam" class="form-control" value="'.$data->foresign_yam.'">';
        })
        ->editColumn('mising_yam',function($data){

            return '<input type="number" min=0 id="mising_yam" class="form-control" value="'.$data->mising_yam.'">';
        })
        ->editColumn('shide_bar',function($data){

            return '<input type="number" min=0 id="shide_bar" class="form-control" value="'.$data->shide_bar.'">';
        })
        ->editColumn('exposure',function($data){

            return '<input type="number" min=0 id="exposure" class="form-control" value="'.$data->exposure.'">';
        })
        ->editColumn('hole',function($data){

            return '<input type="number" min=0 id="hole" class="form-control" value="'.$data->hole.'">';
        })
        ->editColumn('dirly_mark',function($data){

            return '<input type="number" min=0 id="dirly_mark" class="form-control" value="'.$data->dirly_mark.'">';
        })
        ->editColumn('stain',function($data){

            return '<input type="number" min=0 id="stain" class="form-control" value="'.$data->stain.'">';
        })
        ->editColumn('printing_defact',function($data){

            return '<input type="number" min=0 id="printing_defact" class="form-control" value="'.$data->printing_defact.'">';
        })
        ->editColumn('streak_line',function($data){

            return '<input type="number" min=0 id="streak_line" class="form-control" value="'.$data->streak_line.'">';
        })
        ->editColumn('dye_spot',function($data){

            return '<input type="number" min=0 id="dye_spot" class="form-control" value="'.$data->dye_spot.'">';
        })
        ->editColumn('shrinkage',function($data){

            return '<input type="number" min=0 id="shrinkage" class="form-control" value="'.$data->shrinkage.'">';
        })
        ->editColumn('slide_cutting',function($data){

            return '<input type="number" min=0 id="slide_cutting" class="form-control" value="'.$data->slide_cutting.'">';
        })
        ->editColumn('wrong_fice',function($data){

            return '<input type="number" min=0 id="wrong_fice" class="form-control" value="'.$data->wrong_fice.'">';
        })
        ->editColumn('tag_pin',function($data){

            return '<input type="number" min=0 id="tag_pin" class="form-control" value="'.$data->tag_pin.'">';
        })
        ->editColumn('poor_cutting',function($data){

            return '<input type="number" min=0 id="poor_cutting" class="form-control" value="'.$data->poor_cutting.'">';
        })
        ->editColumn('needle_hole',function($data){

            return '<input type="number" min=0 id="needle_hole" class="form-control" value="'.$data->needle_hole.'">';
        })
        ->editColumn('fabric_join',function($data){

            return '<input type="number" min=0 id="fabric_join" class="form-control" value="'.$data->fabric_join.'">';
        })
        ->editColumn('snaging_cut',function($data){

            return '<input type="number" min=0 id="snaging_cut" class="form-control" value="'.$data->snaging_cut.'">';
        })
        ->editColumn('tag_pin',function($data){

            return '<input type="number" id="tag_pin" class="form-control" value="'.$data->tag_pin.'">';
        })
        ->editColumn('wrong_size',function($data){

            return '<input type="number" min=0 id="wrong_size" class="form-control" value="'.$data->wrong_size.'">';
        })
        ->editColumn('remark',function($data){

            return '<input type="text"  id="remark" class="form-control" value="'.$data->remark.'">';
        })
        // ->addColumn('upload',function($data){
        //    return '<input type="file" class="file-styled" name="file_upload" id="file_upload" >';
        // })
        ->rawColumns(['id','komponen_name','size','qty','table','bowing','sliding','pilling','snag','siult','crease_mark','foresign_yam','mising_yam','shide_bar','exposure','hole','dirly_mark','stain','printing_defact','streak_line','dye_spot','shrinkage','slide_cutting','wrong_fice','tag_pin','poor_cutting','needle_hole','fabric_join','snaging_cut','wrong_size','remark'])
        ->make(true);
    }

    public function saveQc(Request $req){
        $barcode = $req->barcode;
        $size = $req->size;
        $data = $req->data;

        try {
            db::begintransaction();

                foreach ($data as $dt) {

                $cek = QcCheck::where('barcode_id',$barcode)->where('size',$size)
                                ->where('komponen_id',$dt[0])->whereNull('deleted_at')->first();
                $total_defect = $dt[4]+$dt[5]+$dt[6]+$dt[7]+$dt[8]+$dt[9]+$dt[10]+$dt[11]+$dt[12]+$dt[13]+$dt[14]+$dt[15]+$dt[16]+$dt[17]+$dt[18]+$dt[19]+$dt[20]+$dt[21]+$dt[22]+$dt[23]+$dt[24]+$dt[25]+$dt[26]+$dt[27]+$dt[28];
                if ($total_defect>0) {
                    $is_approve=false;
                }else{
                    $is_approve=true;
                }
                if ($cek==null && $dt[3]!=null) {

                        $dt_ar = array(
                            'barcode_id'=>$barcode,
                            'komponen_id'=>$dt[0],
                            'size'=>$size,
                            'qty'=>$dt[2],
                            'bundle_table'=>$dt[3],
                            'bowing'=>$dt[4],
                            'sliding'=>$dt[5],
                            'pilling'=>$dt[6],
                            'snag'=>$dt[7],
                            'siult'=>$dt[8],
                            'crease_mark'=>$dt[9],
                            'foresign_yam'=>$dt[10],
                            'mising_yam'=>$dt[11],
                            'shide_bar'=>$dt[12],
                            'exposure'=>$dt[13],
                            'hole'=>$dt[14],
                            'dirly_mark'=>$dt[15],
                            'stain'=>$dt[16],
                            'printing_defact'=>$dt[17],
                            'streak_line'=>$dt[18],
                            'dye_spot'=>$dt[19],
                            'shrinkage'=>$dt[20],
                            'slide_cutting'=>$dt[21],
                            'wrong_fice'=>$dt[22],
                            'tag_pin'=>$dt[23],
                            'poor_cutting'=>$dt[24],
                            'needle_hole'=>$dt[25],
                            'fabric_join'=>$dt[26],
                            'snaging_cut'=>$dt[27],
                            'wrong_size'=>$dt[28],
                            'remark'=>$dt[29],
                            'created_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'total_defect'=>$total_defect,
                            'is_approve'=>$is_approve
                        );
                        Qccheck::FirstOrCreate($dt_ar);

                }else if ($cek!=null && $dt[3]!=null){

                        $dt_up = array(
                            'qty'=>$dt[2],
                            'bundle_table'=>$dt[3],
                            'bowing'=>$dt[4],
                            'sliding'=>$dt[5],
                            'pilling'=>$dt[6],
                            'snag'=>$dt[7],
                            'siult'=>$dt[8],
                            'crease_mark'=>$dt[9],
                            'foresign_yam'=>$dt[10],
                            'mising_yam'=>$dt[11],
                            'shide_bar'=>$dt[12],
                            'exposure'=>$dt[13],
                            'hole'=>$dt[14],
                            'dirly_mark'=>$dt[15],
                            'stain'=>$dt[16],
                            'printing_defact'=>$dt[17],
                            'streak_line'=>$dt[18],
                            'dye_spot'=>$dt[19],
                            'shrinkage'=>$dt[20],
                            'slide_cutting'=>$dt[21],
                            'wrong_fice'=>$dt[22],
                            'tag_pin'=>$dt[23],
                            'poor_cutting'=>$dt[24],
                            'needle_hole'=>$dt[25],
                            'fabric_join'=>$dt[26],
                            'snaging_cut'=>$dt[27],
                            'wrong_size'=>$dt[28],
                            'remark'=>$dt[29],
                            'updated_at'=>Carbon::now(),
                            'user_id'=>Auth::user()->id,
                            'total_defect'=>$total_defect,
                            'is_approve'=>$is_approve
                        );
                        Qccheck::where('id',$cek->id)->update($dt_up);

                }


                }
            db::commit();
            $data_response = [
                                'status' => 200,
                                'output' => 'Save Success . . .'
                            ];
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Save failed, Table require || '.$message
                            ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function approveQcCheck(){
        $table = DB::table('qc_cutting')
                        ->join('bundle_table','qc_cutting.bundle_table','=','bundle_table.id_table')
                        ->where('qc_cutting.is_approve',false)
                        ->where('bundle_table.factory_id',Auth::user()->factory_id)
                        ->whereNull('qc_cutting.deleted_at')
                        ->select('bundle_table.name','qc_cutting.bundle_table')
                        ->groupby('bundle_table.name','qc_cutting.bundle_table')
                        ->get();

        return view('qccheck/approve_qc_check')->with('table',$table);
    }

    public function getBarcodeMarker(Request $request){
            $barcode = Qccheck::where('bundle_table',$request->id_table)
                                ->where('is_approve',false)
                                ->whereNull('deleted_at')
                                ->groupby('bundle_table','is_approve','barcode_id')
                                ->select('bundle_table','is_approve','barcode_id')
                                ->get();
            return response()->json(['barcode'=>$barcode],200);
    }

    public function getDataQc(Request $request){
        $barcode_id = $request->barcode_id;
        $table_id = $request->table_id;

        $data = DB::table('ns_approve_qc_check')
                    ->where('bundle_table',$table_id)
                    ->where('barcode_id',$barcode_id)
                    // ->where('is_approve',false)
                    ->orderBy('komponen_id','asc')
                    ->orderBy('size','asc');

        return DataTables::of($data)
        ->editColumn('komponen_id',function($data){
            $kom = DB::table('master_komponen')->where('id',$data->komponen_id)->first();
            return $kom->komponen_name;
        })
        ->addColumn('qty_cut',function($data){
            $ratio = DB::table('ratio')
                    ->join('cutting_marker','cutting_marker.barcode_id','=','ratio.id_marker')
                    ->where('ratio.id_marker',$data->barcode_id)
                    ->where('ratio.size',$data->size)
                    ->whereNull('ratio.deleted_at')
                    ->whereNull('cutting_marker.deleted_at')
                    ->select('cutting_marker.layer','ratio.ratio')
                    ->first();
            return $ratio->ratio * $ratio->layer;
        })
        ->addColumn('action',function($data){
            if ($data->is_approve==false) {
                return '<button class="btn btn-success" id="btn-approve" data-barcode="'.$data->barcode_id.'" data-table="'.$data->bundle_table.'" data-size="'.$data->size.'" data-komponen="'.$data->komponen_id.'" onclick="approve(this);"><span class="icon-thumbs-up3"></span></button>';
            }else{
                return '';
            }
        })
        ->rawColumns(['komponen_id','qty_cut','action'])
        ->make(true);
    }

    public function setApproveQc(Request $request){
        $barcode = $request->barcode;
        $table = $request->table;
        $size = $request->size;
        $komponen = $request->komponen;

        try {
        DB::begintransaction();
                $setapv = array(
                    'is_approve'=>true,
                    'updated_at'=>Carbon::now()
                );
                Qccheck::where('bundle_table',$table)->where('barcode_id',$barcode)->where('komponen_id',$komponen)->where('size',$size)->update($setapv);
        DB::commit();
        $data_response = [
                        'status' => 200,
                        'output' => 'Approve Success . . .'
                        ];
        } catch (Exception $ex) {
        DB::rollback();
        $message = $ex->getMessage();
        \ErrorHandler($message);

        $data_response = [
                    'status' => 200,
                    'output' => 'Approve Field . . .'
                    ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function qcBundle(){

        $qc_table = DB::table('bundle_table')
        ->where('factory_id',Auth::user()->factory_id)
        ->orderBy('id_table')
        ->pluck('name', 'id_table')->all();
        $operator = DB::table('master_operator_name')->whereNull('deleted_at')->where('factory_id',Auth::user()->factory_id)->get();
        $ror = ['','repair','reject'];
        // $supplier = DB::table('master_subcont')->whereNull('deleted_at')->where('factory_id',\Auth::user()->factory_id)->get();

        // $locator = Locator::where('is_qc_inspection',true)
        // ->pluck('locator_name', 'id')->all();

        // $list_locator = array_merge($qc_table, $locator);

        // $qc_defect = MasterDefectEndline::get();
        // $qc_defect = MasterDefectEndline::pluck('nama_defect', 'id')->all();
        // return view('qccheck/qcapprove', compact('id','found','check_barcode','check_qc'));
        // return view('qccheck/qcbundle', compact('id','found','check_barcode','qc_table','qc_defect'));
        return view('qccheck.qcbundle', compact(['qc_table','operator','ror']));
    }


    public function qcApprove(){

        // $qc_table = DB::table('bundle_table')
        // ->where('factory_id',Auth::user()->factory_id)
        // ->pluck('name', 'id_table')->all();

        $qc_table = DB::table('bundle_table')
        ->where('factory_id',Auth::user()->factory_id)
        ->pluck('name', 'id_table')->all();


        return view('qccheck/qcapprove',compact('qc_table'));
    }

    public function getBundle(Request $request){
        $id = $request->barcode_id;
        $meja_qc = $request->meja_qc;
        $operator = $request->operator;
        $user = \Auth::user()->factory_id;
        $barcode_cdms = DB::table('bundle_detail')->where('barcode_id',$id)->first();
        $barcode_sds = DB::table('bundle_detail')->where('sds_barcode',$id)->first();
        $locator_id = DB::table('bundle_table')->where('id_table',$meja_qc)->first();
        $barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode',$id)->first();
        if($barcode_cdms != null){
            $check_barcode = DB::table('bundle_detail as bd')
                    ->select('bh.style','vms.type_name','bh.poreference','bh.article','bh.size','vms.part','bd.komponen_name','vms.process_name','bh.cut_num','bd.qty','bd.start_no','bd.end_no','vms.process')
                    ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                    ->leftJoin('v_master_style as vms','vms.id','=','bd.style_detail_id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bd.barcode_id',$barcode_cdms->barcode_id)
                    ->first();
        }elseif($barcode_sds != null){
            $check_barcode = DB::table('bundle_detail as bd')
                ->select('bh.style','vms.type_name','bh.poreference','bh.article','bh.size','vms.part','bd.komponen_name','vms.process_name','bh.cut_num','bd.qty','bd.start_no','bd.end_no','vms.process')
                ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                ->leftJoin('v_master_style as vms','vms.id','=','bd.style_detail_id')
                ->where('bh.factory_id',\Auth::user()->factory_id)
                ->where('bd.sds_barcode',$barcode_sds->sds_barcode)
                ->first();
        }elseif($barcode_sds_only != null){
            $check_barcode = DB::table('sds_detail_movements as sdm')
                ->select('shm.style','vms.type_name','shm.poreference','shm.article','shm.size','vms.part','sdm.komponen_name','vms.process_name','shm.cut_num','sdm.qty','sdm.start_no','sdm.end_no','vms.process')
                ->leftJoin('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')
                ->leftJoin('v_master_style as vms','vms.id','=','sdm.style_id')
                ->where('sdm.sds_barcode',$barcode_sds_only->sds_barcode)
                ->where('shm.factory_id',\Auth::user()->factory_id)
                ->first();
        }else{
            return response()->json(['status' => 422, 'responseJSON' => 'Bundle Tidak Ditemukan! #1']);
        }

        if($check_barcode){
            $cek_locator = DB::table('bundle_table')->where('id_table',$meja_qc)->where('factory_id',\Auth::user()->factory_id)->whereNull('deleted_at')->first();
            if($cek_locator->locator_id == 3){
                $cek_process = explode(',',$check_barcode->process);
                $cek_process_id = DB::table('master_process')->whereIn('id',$cek_process)->pluck('id')->toArray();
                $process_id_qc = $cek_locator->name == 'PRINTING' ? 2 : 6;
                if(!in_array($process_id_qc,$cek_process_id)){
                    return response()->json('Kesalahan Pemilihan Meja!',422);
                }
            }
            if($barcode_sds != null){
                $barcode_id = $barcode_sds->barcode_id;
            }else{
                $barcode_id = $id;
            }
            $check_qc = QcBundle::where('barcode_id',$barcode_id)->where('locator_id',$locator_id->locator_id)->first();

            if($check_qc){
                return response()->json(['status' => 422, 'responseJSON' => 'Bundle Sudah Diinput Defect!']);
            }
            if($barcode_sds != null){
                if($locator_id->locator_id != 3){
                    $supplier_id = null;
                }else{
                    $supplier_cdms = DB::table('detail_packinglist_distribusi as dps')
                                    ->select('pd.subcont_id')
                                    ->Join('packinglist_distribusi as pd','pd.no_packinglist','=','dps.id_packinglist')
                                    ->where('dps.bundle_id',$barcode_sds->barcode_id)
                                    ->first();
                    if($supplier_cdms == null){
                        $check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->first();
                        $supplier_temp = DB::connection('sds_live')->table('packing_list_header_alt as plh')
                            ->leftJoin('bundle_info_new as bi','bi.packing_list_number','=','plh.packing_list_number')
                            ->leftJoin('art_pk_target as apt','apt.target_id','=','plh.division')
                            ->where('bi.barcode_id',$barcode_sds->sds_barcode)
                            ->first();
                            if($supplier_temp == null){
                                return response()->json('PACKINGLIST BELUM DIBUAT!',422);
                            }
                        $supplier_id_c = $this->subcontConverter($supplier_temp->subcont_id);
                    }else{
                        $supplier_id = $supplier_cdms->subcont_id;
                    }
                }
            }elseif($barcode_cdms != null){
                if($locator_id->locator_id != 3){
                    $supplier_id = null;
                }else{
                    $supplier_cdms = DB::table('detail_packinglist_distribusi as dps')
                                    ->select('pd.subcont_id')
                                    ->Join('packinglist_distribusi as pd','pd.no_packinglist','=','dps.id_packinglist')
                                    ->where('dps.bundle_id',$barcode_cdms->barcode_id)
                                    ->first();
                    if($supplier_cdms == null){
                        $supplier_temp = DB::connection('sds_live')->table('packing_list_header_alt as plh')
                            ->leftJoin('bundle_info_new as bi','bi.packing_list_number','=','plh.packing_list_number')
                            ->leftJoin('art_pk_target as apt','apt.target_id','=','plh.division')
                            ->where('bi.barcode_id',$barcode_cdms->sds_barcode)
                            ->first();
                        if($supplier_temp == null){
                            return response()->json('PACKINGLIST BELUM DIBUAT!',422);
                        }
                        $supplier_id_c = $this->subcontConverter($supplier_temp->subcont_id);
                    }else{
                        $supplier_id = $supplier_cdms->subcont_id;
                    }
                }
            }else{
                if($locator_id->locator_id != 3){
                    $supplier_id = null;
                }else{
                    $supplier_cdms = DB::table('detail_packinglist_distribusi as dps')
                                    ->select('pd.subcont_id')
                                    ->Join('packinglist_distribusi as pd','pd.no_packinglist','=','dps.id_packinglist')
                                    ->where('dps.bundle_id',$id)
                                    ->first();

                    if($supplier_cdms == null){
                        $supplier_temp = DB::connection('sds_live')->table('packing_list_header_alt as plh')
                            ->leftJoin('bundle_info_new as bi','bi.packing_list_number','=','plh.packing_list_number')
                            ->leftJoin('art_pk_target as apt','apt.target_id','=','plh.division')
                            ->where('bi.barcode_id',$id)
                            ->first();
                        if($supplier_temp == null){
                            return response()->json('PACKINGLIST BELUM DIBUAT!',422);
                        }
                        $supplier_id_c = $this->subcontConverter($supplier_temp->subcont_id);
                    }else{
                        $supplier_id = $supplier_cdms->subcont_id;
                    }
                }
            }
            if($locator_id->locator_id == 3){
                if($supplier_cdms == null){
                    $supplier = DB::table('master_subcont')->where('initials',$supplier_id_c)->where('division',$supplier_temp->target_name)->first();
                }else{
                    $supplier = DB::table('master_subcont')->where('id',$supplier_id)->first();
                }
            }else{
                $supplier = null;
            }

            $response = [
                'id'            => $id,
                'check_barcode' => $check_barcode,
                'meja_qc'       => $meja_qc,
                'supplier'      => $supplier,
                'operator'      => $operator,
                'locator_id'    => $locator_id->locator_id
            ];
            return response()->json(['status' => 200, 'data' => $response]);
        }
        else{
            return response()->json(['status' => 422, 'responseJSON' => 'Bundle Tidak Ditemukan! #2']);
        }
    }

    // public function getLocatorId(Request $request){
    //     if($request->ajax()){
    //         $meja = $request->meja;
    //         if($meja != null){
    //             $locator_id = DB::table('bundle_table')->where('id_table',$meja)->where('factory_id',Auth::user()->factory_id)->first();
    //             $response = [
    //                 'locator_id'      => $locator_id,
    //             ];
    //             return response()->json(['status' => 200, 'data' => $response]);
    //         }else{
    //             $response = [
    //                 'locator_id'      => '',
    //             ];
    //             return response()->json(['status' => 200, 'data' => $response]);
    //         }
    //     }
    // }

    public function getBundleApprove($id){
        $check_barcode = DB::table('bundle_line_valid_new')
        ->where('barcode_id',$id)
        ->where('processing_fact',Auth::user()->factory_id)
        ->first();

        if($check_barcode){

            $check_qc = QcBundle::where('barcode_id',$id)->first();

            $response = [
                'id'            => $id,
                'check_barcode' => $check_barcode,
                'check_qc'      => $check_qc
            ];
            return response()->json(['status' => 200, 'data' => $response]);
        }
        else{
            return response()->json(['status' => 422, 'responseJSON' => 'Bundle Tidak Ditemukan!']);
        }


        // if($check_qc){
        //     return view('qccheck/qcapprove', compact('id','found','check_barcode','check_qc'));
        // }
        // else{
        //     return view('qccheck/qcbundle', compact('id','found','check_barcode','qc_table','qc_defect'));
        // }
    }

    public function simpanQc(Request $req){
        $barcode_id      = $req->barcode_id;
        $bundle_table_id = $req->bundle_table_id;
        $supplier_id     = (int)$req->supplier_id;
        $defectlist      = json_decode($req->defectlist);
        $status_komponen = $req->status_komponen;
        $operator        = $req->operator;

        $barcode_sds = DB::table('bundle_detail')->where('sds_barcode', $barcode_id)->first();
        $locator_id = DB::table('bundle_table')->where('id_table',$bundle_table_id)->first();
        $barcode_cdms = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
        $barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode', $barcode_id)->first();
        if($operator != null || $operator != ''){
            $op_name = DB::table('master_operator_name')->where('nik',$operator)->whereNull('deleted_at')->first()->name;
        }else{
            $op_name = null;
        }


        if($barcode_sds != null){
            if(QcBundle::where('barcode_id',$barcode_sds->barcode_id)->where('bundle_table_id',$bundle_table_id)->exists()){
                $data_response = [
                    'status' => 422,
                    'output' => 'Bundle Sudah diInput !'
                ];
                return response()->json(['data' => $data_response]);
            }else{
                try {
                    db::begintransaction();

                    $total_defect = 0;
                    if($locator_id->locator_id == 10){
                        $marker = DB::table('bundle_detail as bd')
                        ->select('bh.barcode_marker')
                        ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                        ->where('bd.barcode_id',$barcode_sds->barcode_id)->first();
                        $qty_cek = DB::table('bundle_header')->where('barcode_marker',$marker->barcode_marker)->sum('qty');
                    }elseif($locator_id->locator_id == 3){
                        $qty_cek = 0;
                    }else{
                        $qty_cek = $barcode_sds->qty;
                    }

                    if ($status_komponen == 'ok') {

                        $qcbundle = QcBundle::firstorCreate([
                            'barcode_id'      => $barcode_sds->barcode_id,
                            'bundle_table_id' => $bundle_table_id,
                            'status'          => 'ok',
                            'created_by'      => Auth::user()->id,
                            'created_at'      => Carbon::now(),
                            'updated_at'      => Carbon::now(),
                            'defect'          => $total_defect,
                            'locator_id'      => $locator_id->locator_id,
                            'subcont_id'      => $supplier_id,
                            'factory_id'      => \Auth::user()->factory_id,
                            'total_cek'       => $qty_cek,
                            'operator_name'   => $op_name
                        ]);

                        DetailQcBundle::firstOrCreate([
                            'qc_bundle_id' => $qcbundle->id,
                            'qty'          => 0,
                            'defect_id'    => 0,
                            'defect_name'  => 0,
                            'is_repair'    => true,
                            'created_at'   => Carbon::now(),
                            'updated_at'   => Carbon::now(),
                            'confirmed_by' => Auth::user()->id,
                            'confirmed_at' => Carbon::now(),
                            'repaired_by'  => Auth::user()->id,
                            'repaired_at'  => Carbon::now(),
                            'measurement_marker' => 0,
                            'position'      => null
                        ]);

                    }else {

                        // cek defect list ada atau tidak
                        if(count($defectlist) <=0 ){
                            return response()->json('defect not found', 422);
                        }

                        $qcbundle = QcBundle::firstorCreate([
                            'barcode_id'      => $barcode_sds->barcode_id,
                            'bundle_table_id' => $bundle_table_id,
                            'status'          => 'not approved',
                            'created_by'      => Auth::user()->id,
                            'created_at'      => Carbon::now(),
                            'updated_at'      => Carbon::now(),
                            'locator_id'      => $locator_id->locator_id,
                            'subcont_id'      => $supplier_id,
                            'factory_id'      => \Auth::user()->factory_id,
                            'total_cek'       => $qty_cek,
                            'operator_name'   => $op_name
                        ]);
                        // return response()->json('success', 200);
                        foreach ($defectlist as $dl) {

                            if(DetailQcBundle::where('qc_bundle_id',$qcbundle->id)->where('defect_id', $dl->id_defect)->exists()){

                            }
                            else{

                                // if($bundle_table_id == '1'){
                                //     // FUSE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->fuse == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '2'){
                                //     // HE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->heat_transfer == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '4'){
                                //     // PAD PRINT
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->pad == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '6'){
                                //     // PPA1
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->ppa1 == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '7'){
                                //     // PPA2
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->ppa2 == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '8'){
                                //     // BONDING
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->bonding == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '9'){
                                //     // BOBOK
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->bobok == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '14'){
                                //     // TEMPLATE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->template == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '15'){
                                //     // JOIN LABEL
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->join_label == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else{
                                //     // CUTTING
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->cutting == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }

                                if($dl->ror == 'repair'){
                                    $is_repair = true;
                                }else{
                                    $is_repair = false;
                                }

                                // $defect = explode(',',$dl->id_defect);

                                // $reject = 0;
                                // foreach ($defect as $key => $value) {
                                //     $cek = MasterDefectEndline::where('id',$value)->first();
                                //     if($cek->is_repair == false){
                                //         $reject++;
                                //     }
                                // }

                                // if($reject==0){
                                //     $is_repair = true;
                                // }
                                // else{
                                //     $is_repair = false;
                                // }

                                DetailQcBundle::firstOrCreate([
                                    'qc_bundle_id' => $qcbundle->id,
                                    'qty'          => $dl->qty,
                                    'defect_id'    => $dl->id_defect,
                                    'defect_name'  => $dl->nama_defect,
                                    'is_repair'    => $is_repair,
                                    'created_at'   => Carbon::now(),
                                    'updated_at'   => Carbon::now(),
                                    'repair_or_reject' => $dl->ror
                                ]);

                                $total_defect = $total_defect + $dl->qty;
                                // return response()->json('success', 200);
                            }
                        }

                        $qcbundle->defect = $total_defect;
                        $qcbundle->save();
                    }


                    db::commit();
                    $data_response = [
                        'status' => 200,
                        'output' => 'Save Success'
                    ];
                } catch (Exception $ex) {
                    db::rollback();
                    $message = $ex->getMessage();
                    \ErrorHandler($message);

                    $data_response = [
                        'status' => 422,
                        'output' => 'Save failed, Table require || '.$message
                    ];
                }
            }
        }else{
            if(QcBundle::where('barcode_id',$barcode_id)->where('bundle_table_id',$bundle_table_id)->exists()){
                $data_response = [
                    'status' => 422,
                    'output' => 'Bundle Sudah diInput !'
                ];
                return response()->json(['data' => $data_response]);
            }else{
                try {
                    if($locator_id->locator_id == 10){
                        if($barcode_cdms != null && $barcode_sds_only == null){
                            $marker = DB::table('bundle_detail as bd')
                            ->select('bh.barcode_marker')
                            ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                            ->where('bd.barcode_id',$barcode_id)->first();
                            $qty_cek = DB::table('bundle_header')->where('barcode_marker',$marker->barcode_marker)->sum('qty');
                        }elseif($barcode_cdms == null && $barcode_sds_only != null){
                            $qty_cek = 0;
                        }
                    }elseif($locator_id->locator_id == 3){
                        $qty_cek = 0;
                    }else{
                        if($barcode_cdms != null && $barcode_sds_only == null){
                            $qty_cek = $barcode_cdms->qty;
                        }elseif($barcode_cdms == null && $barcode_sds_only != null){
                            $qty_cek = $barcode_sds_only->qty;
                        }
                    }
                    db::begintransaction();

                    $total_defect = 0;

                    if ($status_komponen == 'ok') {

                        $qcbundle = QcBundle::firstorCreate([
                            'barcode_id'      => $barcode_id,
                            'bundle_table_id' => $bundle_table_id,
                            'status'          => 'ok',
                            'created_by'      => Auth::user()->id,
                            'created_at'      => Carbon::now(),
                            'updated_at'      => Carbon::now(),
                            'defect'          => $total_defect,
                            'locator_id'      => $locator_id->locator_id,
                            'subcont_id'      => $supplier_id,
                            'factory_id'      => \Auth::user()->factory_id,
                            'total_cek'       => $qty_cek,
                            'operator_name'   => $op_name
                        ]);

                        DetailQcBundle::firstOrCreate([
                            'qc_bundle_id' => $qcbundle->id,
                            'qty'          => 0,
                            'defect_id'    => 0,
                            'defect_name'  => 0,
                            'is_repair'    => false,
                            'created_at'   => Carbon::now(),
                            'updated_at'   => Carbon::now(),
                            'confirmed_by' => Auth::user()->id,
                            'confirmed_at' => Carbon::now(),
                            'repaired_by'  => Auth::user()->id,
                            'repaired_at'  => Carbon::now(),
                            'measurement_marker'    => 0,
                            'position'      => null
                        ]);

                    }else {

                        // cek defect list ada atau tidak
                        if(count($defectlist) <=0 ){
                            return response()->json('defect not found', 422);
                        }

                        $qcbundle = QcBundle::firstorCreate([
                            'barcode_id'      => $barcode_id,
                            'bundle_table_id' => $bundle_table_id,
                            'status'          => 'not approved',
                            'created_by'      => Auth::user()->id,
                            'created_at'      => Carbon::now(),
                            'updated_at'      => Carbon::now(),
                            'locator_id'      => $locator_id->locator_id,
                            'subcont_id'      => $supplier_id,
                            'factory_id'      => \Auth::user()->factory_id,
                            'total_cek'       => $qty_cek,
                            'operator_name'   => $op_name,
                        ]);
                        foreach ($defectlist as $dl) {
                            if(DetailQcBundle::where('qc_bundle_id',$qcbundle->id)->where('defect_id', $dl->id_defect)->exists()){

                            }
                            else{
                                // if($bundle_table_id == '1'){
                                //     // FUSE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->fuse == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '2'){
                                //     // HE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->heat_transfer == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '4'){
                                //     // PAD PRINT
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->pad == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '6'){
                                //     // PPA1
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->ppa1 == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '7'){
                                //     // PPA2
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->ppa2 == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '8'){
                                //     // BONDING
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->bonding == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '9'){
                                //     // BOBOK
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->bobok == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '14'){
                                //     // TEMPLATE
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->template == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else if($bundle_table_id == '15'){
                                //     // JOIN LABEL
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->join_label == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                // else{
                                //     // CUTTING
                                //     $cek = MasterDefectEndline::where('id',$dl->id_defect)->first();
                                //     if($cek->cutting == 'repair'){
                                //         $is_repair = true;
                                //     }
                                //     else{
                                //         $is_repair = false;
                                //     }
                                // }
                                if($dl->ror == 'repair'){
                                    $is_repair = true;
                                }else{
                                    $is_repair = false;
                                }

                                // $defect = explode(',',$dl->id_defect);

                                // $reject = 0;
                                // foreach ($defect as $key => $value) {
                                //     $cek = MasterDefectEndline::where('id',$value)->first();
                                //     if($cek->is_repair == false){
                                //         $reject++;
                                //     }
                                // }

                                // if($reject==0){
                                //     $is_repair = true;
                                // }
                                // else{
                                //     $is_repair = false;
                                // }

                                DetailQcBundle::firstOrCreate([
                                    'qc_bundle_id' => $qcbundle->id,
                                    'qty'          => $dl->qty,
                                    'defect_id'    => $dl->id_defect,
                                    'defect_name'  => $dl->nama_defect,
                                    'is_repair'    => $is_repair,
                                    'created_at'   => Carbon::now(),
                                    'updated_at'   => Carbon::now(),
                                    'repair_or_reject' => $dl->ror,
                                    'measurement_marker' => (float)$dl->measurement == 0 ? 0 : (float)$dl->measurement,
                                    'position' => $dl->position == '' ? null : $dl->position
                                    // 'qty_random'    => (int)$dl->qty_random
                                ]);

                                $reject_check = DetailQcBundle::where('qc_bundle_id',$qcbundle->id)->pluck('repair_or_reject')->toArray();
                                $reject_res   = in_array('reject',$reject_check) == true ? 'reject' : 'repair';
                                // $qty_random_avg = DetailQcBundle::where('qc_bundle_id',$qcbundle->id)->avg('qty_random');

                                QcBundle::where('id',$qcbundle->id)->update([
                                    'repair_or_reject'      => $reject_res
                                    // 'qty_random'            => (int)$qty_random_avg
                                ]);

                                $total_defect = $total_defect + $dl->qty;
                            }
                        }

                        $qcbundle->defect = $total_defect;
                        $qcbundle->save();
                    }
                    db::commit();
                    $data_response = [
                        'status' => 200,
                        'output' => 'Save Success'
                    ];
                } catch (Exception $ex) {
                    db::rollback();
                    $message = $ex->getMessage();
                    \ErrorHandler($message);

                    $data_response = [
                        'status' => 422,
                        'output' => 'Save failed, Table require || '.$message
                    ];
                }
            }
        }

        return response()->json(['data' => $data_response]);
    }

    public function qcReject()
    {
        // $qc_table = DB::table('bundle_table')
        // ->where('factory_id',Auth::user()->factory_id)
        // ->pluck('name', 'name')->all();

        $qc_table = DB::table('bundle_table')
        ->where('factory_id',Auth::user()->factory_id)
        ->pluck('name', 'name')->all();

        // $locator = Locator::where('is_qc_inspection',true)
        // ->pluck('locator_name', 'locator_name')->all();

        // $list_locator = array_merge($qc_table, $locator);

        return view('qccheck.qcreject',compact('qc_table'));
    }

    public function getQcReject(Request $request)
    {
        $filter  = $request->filter;
        $meja    = $request->filter_meja;
        $factory = \Auth::user()->factory_id;

        if($meja == null){
            if($filter == 'belum'){
                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                and confirmed_at is null and repaired_at is null
                GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
            else if($filter == 'sudah'){

                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                 defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                 and confirmed_at is null and repaired_at is not null
                 GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
            else{
                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                 defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                 and confirmed_at is not null and repaired_at is not null
                 GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
        }
        else{
            if($filter == 'belum'){
                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                and confirmed_at is null and repaired_at is null and name = '$meja'
                GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
            else if($filter == 'sudah'){

                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                 defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                 and confirmed_at is null and repaired_at is not null and name = '$meja'
                 GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
            else{
                $data = DB::select(db::raw("SELECT qc_bundle_id as id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at,
                 defect FROM get_list_qc_reject('".$factory."') WHERE is_repair is false
                 and confirmed_at is not null and repaired_at is not null and name = '$meja'
                 GROUP BY qc_bundle_id, barcode_id, style, set_type, article, poreference, cut_num, size, component_name, name, user_qc, user_approve, user_reject, created_at, confirmed_at, repaired_at, defect"));
            }
        }

        return datatables::of($data)
            ->editColumn('style', function ($data) {
                return  $data->set_type == '0' ? $data->style : $data->style.'-'.$data->set_type;
            })
            ->addColumn('action', function ($data) {
                if($data->repaired_at){
                    return 'Sudah Ganti Reject';
                }
                else{
                    // return '<button class="btn btn-success btn-detail btn-icon"  id="btn-detail" data-id="'.$data->id.'" onclick="detail(this);"><span class="icon-checkmark"></span></button>';
                    return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
                }
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function gantiReject(Request $req){
        $detail_qc_id      = json_decode($req->id);

        $data = DetailQcBundle::whereIn('qc_bundle_id',$detail_qc_id)->whereNull('repaired_at')->get();
        // if(count($data) == count($detail_qc_id)){
            try {
                db::begintransaction();

                foreach ($data as $key => $value) {
                    $data_detail = array(
                        'repaired_by' => Auth::user()->id,
                        'repaired_at' => Carbon::now()
                    );
                    DetailQcBundle::where('id',$value->id)->update($data_detail);

                }

                db::commit();
                $data_response = [
                    'status' => 200,
                    'output' => 'Save Success'
                ];
            } catch (Exception $ex) {
                db::rollback();
                $message = $ex->getMessage();
                \ErrorHandler($message);

                $data_response = [
                    'status' => 422,
                    'output' => 'Save Failed, Something Wrong || '.$message
                ];
            }
        // }
        // else{
        //     $data_response = [
        //         'status' => 422,
        //         'output' => 'Terjadi Kesalahan, Refresh Dahulu !'
        //     ];
        // }

        return response()->json(['data' => $data_response]);
    }

    public function getQcDefect(Request $request)
    {
        // $barcode_id = $request->barcode_id;
        $filter  = $request->filter;
        $meja    = $request->meja;
        $factory = \Auth::user()->factory_id;

        if($meja == null){
            if($filter == 'belum'){
                $data = DB::table('hs_qc_reject')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->whereNull('confirmed_at')
                    ->orderBy('name')
                    ->orderBy('barcode_id')
                    ->orderBy('qty');
            }
            else{
                $data = DB::table('hs_qc_reject')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->whereNotNull('confirmed_at')
                    ->orderBy('name')
                    ->orderBy('barcode_id')
                    ->orderBy('qty');
            }
        }
        else{
            if($filter == 'belum'){
                $data = DB::table('hs_qc_reject')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('bundle_table_id',$meja)
                    ->whereNull('confirmed_at')
                    ->orderBy('name')
                    ->orderBy('barcode_id')
                    ->orderBy('qty');
            }
            else{
                $data = DB::table('hs_qc_reject')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('bundle_table_id',$meja)
                    ->whereNotNull('confirmed_at')
                    ->orderBy('name')
                    ->orderBy('barcode_id')
                    ->orderBy('qty');
            }
        }


        return datatables::of($data)
            ->addColumn('action', function ($data) {
                if($data->is_repair){
                    if($data->confirmed_at){
                        return 'Sudah Diapprove';
                    }
                    else{
                        return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
                    }
                }
                else{
                    if($data->repaired_at){
                        if($data->confirmed_at){
                            return 'Sudah Diapprove';
                        }
                        else{
                            return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
                        }
                    }
                    else{
                        return 'Belum Diganti Reject';
                    }
                }

                // $defect = explode(',',$data->defect_id);

                // $reject = 0;
                // foreach ($defect as $key => $value) {
                //     $cek = MasterDefectEndline::where('id',$value)->first();
                //     if($cek->is_repair == false){
                //         $reject++;
                //     }
                // }

                // if($reject==0){
                //     return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
                // }
                // else{
                //     if($data->repaired_at){
                //         if($data->confirmed_at){
                //             return 'Sudah Diapprove';
                //         }
                //         else{
                //             return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
                //         }
                //     }
                //     else{
                //         return 'Belum Diganti Reject';
                //     }
                // }
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function approveReject(Request $req){
        $detail_qc_id      = json_decode($req->id);
        $qc_bundle_id      = $req->qc_bundle_id;

        $data = DetailQcBundle::whereIn('id',$detail_qc_id)->whereNull('confirmed_at')->get();

        if(count($data) == count($detail_qc_id)){
            try {
                db::begintransaction();

                foreach ($detail_qc_id as $key => $value) {
                    $data_detail = array(
                        'confirmed_by' => Auth::user()->id,
                        'confirmed_at' => Carbon::now()
                    );
                    DetailQcBundle::where('id',$value)->update($data_detail);

                }

                $check_qc = DetailQcBundle::where('qc_bundle_id',$qc_bundle_id)->whereNull('confirmed_at')->count();

                if($check_qc==0){
                    $data_header = array(
                        'status' => 'approved',
                    );
                    QcBundle::where('id',$qc_bundle_id)->update($data_header);
                }

                db::commit();
                $data_response = [
                    'status' => 200,
                    'output' => 'Save Success'
                ];
            } catch (Exception $ex) {
                db::rollback();
                $message = $ex->getMessage();
                \ErrorHandler($message);

                $data_response = [
                    'status' => 422,
                    'output' => 'Save Failed, Something Wrong || '.$message
                ];
            }
        }
        else{
            $data_response = [
                'status' => 422,
                'output' => 'Terjadi Kesalahan, Refresh Dahulu !'
            ];
        }

        return response()->json(['data' => $data_response]);
    }


    public function getDefectList($id){

        if($id != null){

            //if($id == '1'){
            //    // FUSE
            //    $list_defect = MasterDefectEndline::whereNotNull('fuse')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '2'){
            //    // HE
            //    $list_defect = MasterDefectEndline::whereNotNull('heat_transfer')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '4'){
            //    // PAD PRINT
            //    $list_defect = MasterDefectEndline::whereNotNull('pad')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '6'){
            //    // PPA1
            //    $list_defect = MasterDefectEndline::whereNotNull('ppa1')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '7'){
            //    // PPA2
            //    $list_defect = MasterDefectEndline::whereNotNull('ppa2')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '8'){
            //    // BONDING
            //    $list_defect = MasterDefectEndline::whereNotNull('bonding')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '9'){
            //    // BOBOK
            //    $list_defect = MasterDefectEndline::whereNotNull('bobok')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '14'){
            //    // TEMPLATE
            //    $list_defect = MasterDefectEndline::whereNotNull('template')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else if($id == '15'){
            //    // JOIN LABEL
            //    $list_defect = MasterDefectEndline::whereNotNull('join_label')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}
            //else{
            //    // CUTTING
            //    $list_defect = MasterDefectEndline::whereNotNull('cutting')
            //    ->orderBy('nama_defect')
            //    ->get();
            //}

            ///////////////////////////////
            //
            $data_bundle_table = DB::table('bundle_table')->where('id_table', $id)->first();
            //

            if($data_bundle_table->name == 'FUSE'){
                // FUSE
                $list_defect = MasterDefectEndline::whereNotNull('fuse')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'HE'){
                // HE
                $list_defect = MasterDefectEndline::whereNotNull('heat_transfer')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'PAD PRINT'){
                // PAD PRINT
                $list_defect = MasterDefectEndline::whereNotNull('pad')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'PPA1'){
                // PPA1
                $list_defect = MasterDefectEndline::whereNotNull('ppa1')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'PPA2'){
                // PPA2
                $list_defect = MasterDefectEndline::whereNotNull('ppa2')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'BONDING'){
                // BONDING
                $list_defect = MasterDefectEndline::whereNotNull('bonding')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'BOBOK'){
                // BOBOK
                $list_defect = MasterDefectEndline::whereNotNull('bobok')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'TEMPLATE'){
                // TEMPLATE
                $list_defect = MasterDefectEndline::whereNotNull('template')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'JOIN LABEL'){
                // JOIN LABEL
                $list_defect = MasterDefectEndline::whereNotNull('join_label')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'EMBRO'){
                // EMBRO
                $list_defect = MasterDefectEndline::whereNotNull('embro')
                ->orderBy('nama_defect')
                ->get();
            }
            else if($data_bundle_table->name == 'PRINTING'){
                // PRINTING
                $list_defect = MasterDefectEndline::whereNotNull('printing')
                ->orderBy('nama_defect')
                ->get();
            }
            else{
                // CUTTING
                $list_defect = MasterDefectEndline::whereNotNull('cutting')
                ->orderBy('nama_defect')
                ->get();
            }

            $locator_id = DB::table('bundle_table')->where('id_table',$id)->where('factory_id',Auth::user()->factory_id)->first();
            //////////////////////////////

            $response = [
                'id'            => $id,
                'list_defect'   => $list_defect,
                'locator_id'    => $locator_id->locator_id
            ];

            return response()->json(['status' => 200, 'data' => $response]);
        }
        else{
            return response()->json(['status' => 422, 'responseJSON' => 'Locator Tidak Ditemukan!']);
        }
    }

    public function reviewQcV4(Request $request)
    {
        // $locator = DB::table('master_locator')->whereNull('deleted_at')->get();
        $locator = DB::table('bundle_table')->whereNull('deleted_at')->where('factory_id',Auth::user()->factory_id)->orderBy('name')->get();
        return view('qccheck.reviewqcv4',compact('locator'));
    }

    public function getDataReviewQcV4(Request $request)
    {
        if(request()->ajax())
        {
            $qc_date = explode('-', preg_replace('/\s+/', '', $request->qc_date));
            $from = Carbon::createFromFormat('m/d/Y', $qc_date[0])->format('Y-m-d');
            $to = Carbon::createFromFormat('m/d/Y', $qc_date[1])->format('Y-m-d');
            $locator = $request->locator;
            if($qc_date != NULL && $locator != null){
                $user = Auth::user()->factory_id;
                $locator_arr = "'".implode("','",$locator)."'";

                // $data = DB::select("SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,bd.bundle_header_id,dqb.defect_name,bh.season,bh.STYLE,vms.type_name,bh.poreference,bh.cutting_date,bh.article,bh.color_name,bh.cut_num,bh.SIZE,bh.job,bd.qty,bd.komponen_name,bd.start_no,bd.end_no,qb.status,qb.created_by,ml.locator_name,ms.subcont_name,qb.created_at
                // FROM
                // qc_bundles qb
                // LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                // LEFT JOIN bundle_detail bd ON bd.barcode_id = qb.barcode_id
                // LEFT JOIN bundle_header bh ON bh.ID = bd.bundle_header_id
                // LEFT JOIN master_style_detail msd ON msd.ID = bd.style_detail_id
                // LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                // LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                // LEFT JOIN master_locator ml on ml.id = qb.locator_id
                // LEFT JOIN v_master_style vms on vms.id = bd.style_detail_id
                // WHERE
                // qb.deleted_at IS NULL
                // AND dqb.deleted_at IS NULL
                // -- AND NULLIF ( msd.process, '' ) IS NOT NULL
                // -- AND bt.is_secondary = TRUE
                // AND date(qb.created_at) BETWEEN '$from' AND '$to'
                // AND bh.factory_id = '$user'
                // AND qb.locator_id IN($locator_arr)
                // UNION ALL
                // SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,sdm.sds_header_id,dqb.defect_name,shm.season,shm.style,vms.type_name,shm.poreference,shm.cutting_date,shm.article,shm.color_name,shm.cut_num,shm.size,concat(shm.style,'::',shm.poreference,'::',shm.article) as job,sdm.qty,sdm.komponen_name,sdm.start_no,sdm.end_no,qb.status,qb.created_by,ml.locator_name,ms.subcont_name,qb.created_at
                // FROM
                // qc_bundles qb
                // LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                // LEFT JOIN sds_detail_movements sdm ON sdm.sds_barcode = qb.barcode_id
                // LEFT JOIN sds_header_movements shm ON shm.id = sdm.sds_header_id
                // LEFT JOIN master_style_detail msd ON msd.ID = sdm.style_id
                // LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                // LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                // LEFT JOIN master_locator ml on ml.id = qb.locator_id
                // LEFT JOIN v_master_style vms on vms.id = sdm.style_id
                // WHERE
                // qb.deleted_at IS NULL
                // AND dqb.deleted_at IS NULL
                // -- AND NULLIF ( msd.process, '' ) IS NOT NULL
                // -- AND bt.is_secondary = TRUE
                // AND date(qb.created_at) BETWEEN '$from' AND '$to'
                // AND shm.factory_id = '$user'
                // AND qb.locator_id IN ($locator_arr)");
                $data = DB::select("SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,bd.bundle_header_id,dqb.defect_name,bh.season,bh.STYLE,vms.type_name,bh.poreference,bh.cutting_date,bh.article,bh.color_name,bh.cut_num,bh.SIZE,bh.job,bd.qty,bd.komponen_name,bd.start_no,bd.end_no,qb.status,qb.created_by,ml.locator_name,bt.name as process_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name,bh.factory_id
                FROM
                qc_bundles qb
                LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                LEFT JOIN bundle_detail bd ON bd.barcode_id = qb.barcode_id
                LEFT JOIN bundle_header bh ON bh.ID = bd.bundle_header_id
                LEFT JOIN master_style_detail msd ON msd.ID = bd.style_detail_id
                LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                LEFT JOIN master_locator ml on ml.id = qb.locator_id
                LEFT JOIN v_master_style vms on vms.id = bd.style_detail_id
                WHERE
                qb.deleted_at IS NULL
                AND dqb.deleted_at IS NULL
                AND NULLIF ( msd.process, '' ) IS NOT NULL
                AND date(qb.created_at) BETWEEN '$from' AND '$to'
                AND bh.factory_id = '$user'
                AND bt.id_table IN ($locator_arr)
                UNION ALL
                SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,sdm.sds_header_id,dqb.defect_name,shm.season,shm.style,vms.type_name,shm.poreference,shm.cutting_date,shm.article,shm.color_name,shm.cut_num,shm.size,concat(shm.style,'::',shm.poreference,'::',shm.article) as job,sdm.qty,sdm.komponen_name,sdm.start_no,sdm.end_no,qb.status,qb.created_by,ml.locator_name,bt.name as process_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name,shm.factory_id
                FROM
                qc_bundles qb
                LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                LEFT JOIN sds_detail_movements sdm ON sdm.sds_barcode = qb.barcode_id
                LEFT JOIN sds_header_movements shm ON shm.id = sdm.sds_header_id
                LEFT JOIN master_style_detail msd ON msd.ID = sdm.style_id
                LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                LEFT JOIN master_locator ml on ml.id = qb.locator_id
                LEFT JOIN v_master_style vms on vms.id = sdm.style_id
                WHERE
                qb.deleted_at IS NULL
                AND dqb.deleted_at IS NULL
                AND NULLIF ( msd.process, '' ) IS NOT NULL
                AND date(qb.created_at) BETWEEN '$from' AND '$to'
                AND shm.factory_id = '$user'
                AND bt.id_table IN ($locator_arr)");
            }else{
                $data = [];
            }
                return datatables()->of($data)
                    ->addColumn('cut_sticker',function ($data)
                        {
                            return $data->cut_num.' | '.$data->start_no.'-'.$data->end_no;
                        })
                    ->addColumn('style',function ($data)
                        {
                            if($data->type_name == 'Non'){
                                $style = $data->style;
                            }else{
                                $style = $data->style.'-'.$data->type_name;
                            }
                            return $style;
                        })
                    ->editColumn('defect_name',function($data){
                        if($data->defect_name == '0'){
                            return '';
                        }else{
                            return $data->defect_name;
                        }
                    })
                    ->addColumn('qc_name',function($data){
                        $user = DB::table('users')->where('id',$data->created_by)->first();
                        return strtoupper($user->name);
                    })
                    ->addColumn('date_check',function($data){
                        return Carbon::parse($data->created_at)->format('d-m-Y');
                    })
                    ->rawColumns(['qc_name','defect_name'])
                    ->make(true);
        }else{
            $data = array();
            return datatables()->of($data)
            ->rawColumns([])
            ->make(true);
        }
    }

    public function downloadReviewQcV4(Request $request)
    {
        $qc_date = explode('-', preg_replace('/\s+/', '', $request->qc_date));
        $from = Carbon::createFromFormat('m/d/Y', $qc_date[0])->format('Y-m-d');
        $to = Carbon::createFromFormat('m/d/Y', $qc_date[1])->format('Y-m-d');
        $locator = $request->locator;
        $locator_arr = "'".implode("','",$locator)."'";
        $factory_id = \Auth::user()->factory_id;
        $user = (int)$factory_id;


        // $data = DB::select("SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,bd.bundle_header_id,dqb.defect_name,bh.season,bh.STYLE,vms.type_name,bh.poreference,bh.cutting_date,bh.article,bh.color_name,bh.cut_num,bh.SIZE,bh.job,bd.qty,bd.komponen_name,bd.start_no,bd.end_no,qb.status,qb.created_by,ml.locator_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name
        // FROM
        // qc_bundles qb
        // LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
        // LEFT JOIN bundle_detail bd ON bd.barcode_id = qb.barcode_id
        // LEFT JOIN bundle_header bh ON bh.ID = bd.bundle_header_id
        // LEFT JOIN master_style_detail msd ON msd.ID = bd.style_detail_id
        // LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
        // LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
        // LEFT JOIN master_locator ml on ml.id = qb.locator_id
        // LEFT JOIN v_master_style vms on vms.id = bd.style_detail_id
        // WHERE
        // qb.deleted_at IS NULL
        // AND dqb.deleted_at IS NULL
        // AND NULLIF ( msd.process, '' ) IS NOT NULL
        // AND date(qb.created_at) BETWEEN '$from' AND '$to'
        // AND bh.factory_id = '$user'
        // AND qb.locator_id IN($locator_arr)
        // UNION ALL
        // SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,sdm.sds_header_id,dqb.defect_name,shm.season,shm.style,vms.type_name,shm.poreference,shm.cutting_date,shm.article,shm.color_name,shm.cut_num,shm.size,concat(shm.style,'::',shm.poreference,'::',shm.article) as job,sdm.qty,sdm.komponen_name,sdm.start_no,sdm.end_no,qb.status,qb.created_by,ml.locator_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name
        // FROM
        // qc_bundles qb
        // LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
        // LEFT JOIN sds_detail_movements sdm ON sdm.sds_barcode = qb.barcode_id
        // LEFT JOIN sds_header_movements shm ON shm.id = sdm.sds_header_id
        // LEFT JOIN master_style_detail msd ON msd.ID = sdm.style_id
        // LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
        // LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
        // LEFT JOIN master_locator ml on ml.id = qb.locator_id
        // LEFT JOIN v_master_style vms on vms.id = sdm.style_id
        // WHERE
        // qb.deleted_at IS NULL
        // AND dqb.deleted_at IS NULL
        // AND NULLIF ( msd.process, '' ) IS NOT NULL
        // AND date(qb.created_at) BETWEEN '$from' AND '$to'
        // AND shm.factory_id = '$user'
        // AND qb.locator_id IN ($locator_arr)");
        $data = DB::select("SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,bd.bundle_header_id,dqb.defect_name,bh.season,bh.STYLE,vms.type_name,bh.poreference,bh.cutting_date,bh.article,bh.color_name,bh.cut_num,bh.SIZE,bh.job,bd.qty,bd.komponen_name,bd.start_no,bd.end_no,qb.status,qb.created_by,ml.locator_name,bt.name as process_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name,bh.factory_id
                FROM
                qc_bundles qb
                LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                LEFT JOIN bundle_detail bd ON bd.barcode_id = qb.barcode_id
                LEFT JOIN bundle_header bh ON bh.ID = bd.bundle_header_id
                LEFT JOIN master_style_detail msd ON msd.ID = bd.style_detail_id
                LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                LEFT JOIN master_locator ml on ml.id = qb.locator_id
                LEFT JOIN v_master_style vms on vms.id = bd.style_detail_id
                WHERE
                qb.deleted_at IS NULL
                AND dqb.deleted_at IS NULL
                AND NULLIF ( msd.process, '' ) IS NOT NULL
                AND date(qb.created_at) BETWEEN '$from' AND '$to'
                AND bh.factory_id = '$user'
                AND bt.id_table IN ($locator_arr)
                UNION ALL
                SELECT qb.id,qb.barcode_id,dqb.qty AS qty_reject,sdm.sds_header_id,dqb.defect_name,shm.season,shm.style,vms.type_name,shm.poreference,shm.cutting_date,shm.article,shm.color_name,shm.cut_num,shm.size,concat(shm.style,'::',shm.poreference,'::',shm.article) as job,sdm.qty,sdm.komponen_name,sdm.start_no,sdm.end_no,qb.status,qb.created_by,ml.locator_name,bt.name as process_name,ms.subcont_name,qb.created_at,dqb.repair_or_reject,qb.operator_name,shm.factory_id
                FROM
                qc_bundles qb
                LEFT JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id
                LEFT JOIN sds_detail_movements sdm ON sdm.sds_barcode = qb.barcode_id
                LEFT JOIN sds_header_movements shm ON shm.id = sdm.sds_header_id
                LEFT JOIN master_style_detail msd ON msd.ID = sdm.style_id
                LEFT JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id
                LEFT JOIN master_subcont ms on ms.id = qb.subcont_id
                LEFT JOIN master_locator ml on ml.id = qb.locator_id
                LEFT JOIN v_master_style vms on vms.id = sdm.style_id
                WHERE
                qb.deleted_at IS NULL
                AND dqb.deleted_at IS NULL
                AND NULLIF ( msd.process, '' ) IS NOT NULL
                AND date(qb.created_at) BETWEEN '$from' AND '$to'
                AND shm.factory_id = '$user'
                AND bt.id_table IN ($locator_arr)");

        if(count($data) < 1){
            return response()->json('Data tidak ditemukan.', 422);
        }else{
            $filename = "Report_Review_QC_"."AOI_".\Auth::user()->factory_id."_".$from.'#'.$to;
            return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        // 'I' => '@',
                        'J' => '@',
                        'K' => '@',
                        'L' => '@',
                        'M' => '@',
                        'N' => '@',
                        'O' => '@',
                        'P' => '@',
                        'Q' => '@',
                        'R' => '@',
                        'S' => '@',
                        'T' => '@',
                        'U' => '@',
                        'V' => '@',
                    ));
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','SEASON');
                    $sheet->setCellValue('C1','DEFECT NAME');
                    $sheet->setCellValue('D1','STYLE');
                    $sheet->setCellValue('E1','PO BUYER');
                    $sheet->setCellValue('F1','ARTICLE');
                    $sheet->setCellValue('G1','COLOR');
                    $sheet->setCellValue('H1','CUT | STICKER');
                    $sheet->setCellValue('I1','BARCODE BUNDLE');
                    $sheet->setCellValue('J1','SIZE');
                    $sheet->setCellValue('K1','KOMPONEN');
                    $sheet->setCellValue('L1','CUTTING DATE');
                    $sheet->setCellValue('M1','QTY BUNDLE');
                    $sheet->setCellValue('N1','QTY DEFECT');
                    $sheet->setCellValue('O1','JOB');
                    $sheet->setCellValue('P1','QC NAME');
                    $sheet->setCellValue('Q1','LOCATOR');
                    $sheet->setCellValue('R1','PROCESS NAME');
                    $sheet->setCellValue('S1','SUPPLIER');
                    $sheet->setCellValue('T1','DATE CHECK');
                    $sheet->setCellValue('U1','REPAIR / REJECT');
                    $sheet->setCellValue('V1','OPERATOR NAME');
                    $sheet->setCellValue('W1','DESTINATION');

                    $i = 1;
                    foreach ($data as $key => $vl) {
                        $cut_sticker = $vl->cut_num.' | '.$vl->start_no.'-'.$vl->end_no;
                        $style = $vl->type_name == 'Non' ? $vl->style : $vl->style.'-'.$vl->type_name;
                        $cut_date = DB::table('data_cuttings')->whereNull('deleted_at')->where('po_buyer',$vl->poreference)->first();
                        $destination = DB::table('data_cuttings')->whereNull('deleted_at')->where('po_buyer',$vl->poreference)->orWhere('job_no','like','%'.$vl->poreference.'%')->first();
                        $cut_date = $cut_date == null ? 'REROUTE-PO' : $cut_date->cutting_date;
                        $qc_name = DB::table('users')->where('id',$vl->created_by)->first();

                        $rw = $i+1;
                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,$vl->season);
                        $sheet->setCellValue('C'.$rw,$vl->defect_name);
                        $sheet->setCellValue('D'.$rw,$style);
                        $sheet->setCellValue('E'.$rw,$vl->poreference);
                        $sheet->setCellValue('F'.$rw,$vl->article);
                        $sheet->setCellValue('G'.$rw,$vl->color_name);
                        $sheet->setCellValue('H'.$rw,$cut_sticker);
                        $sheet->setCellValue('I'.$rw,"'".$vl->barcode_id);
                        $sheet->setCellValue('J'.$rw,$vl->size);
                        $sheet->setCellValue('K'.$rw,$vl->komponen_name);
                        $sheet->setCellValue('L'.$rw,$cut_date);
                        $sheet->setCellValue('M'.$rw,$vl->qty);
                        $sheet->setCellValue('N'.$rw,$vl->qty_reject);
                        $sheet->setCellValue('O'.$rw,$vl->job);
                        $sheet->setCellValue('P'.$rw,$qc_name->name);
                        $sheet->setCellValue('Q'.$rw,$vl->locator_name);
                        $sheet->setCellValue('R'.$rw,$vl->process_name);
                        $sheet->setCellValue('S'.$rw,$vl->subcont_name);
                        $sheet->setCellValue('T'.$rw,Carbon::parse($vl->created_at)->format('d-M-Y H:i:s'));
                        $sheet->setCellValue('U'.$rw,$vl->repair_or_reject);
                        $sheet->setCellValue('V'.$rw,$vl->operator_name);
                        $sheet->setCellValue('W'.$rw,$destination == null ? '-' : $destination->destination);
                        $i++;
                    }
                });
                $excel->setActiveSheetIndex(0);
            })->export('xlsx');
        }
    }

    private function subcontConverter($supplier_temp)
    {
        switch ($supplier_temp) {
            case 1:
                $subcont = 'KM80';
                break;
            case 11:
                $subcont = 'KUS';
                break;
            case 16:
                $subcont = 'AJMA';
                break;
            case 29:
                $subcont = 'DKS';
                break;
            case 23:
                $subcont = 'HN PRINT';
                break;
            case 21:
                $subcont = 'KSM';
                break;
            case 24:
                $subcont = 'UPN';
                break;
            case 12:
                $subcont = 'MG';
                break;
            case 14:
                $subcont = 'ACK';
                break;
            case 10:
                $subcont = 'JK';
                break;
            case 15:
                $subcont = 'IBM';
                break;
            case 25:
                $subcont = 'OPI';
                break;
            case 27:
                $subcont = 'BJY';
                break;
            case 4:
                $subcont = 'JK';
                break;
            case 5:
                $subcont = 'DKS';
                break;
            case 8:
                $subcont = 'UPR';
                break;
            case 9:
                $subcont = 'BJY1';
                break;
            case 6:
                $subcont = 'AN';
                break;
            case 31:
                $subcont = 'ABJ';
                break;
            case 3:
                $subcont = 'KUS';
                break;
            case 32:
                $subcont = 'KUS';
                break;
            case 17:
                $subcont = 'KSM';
                break;
            default:
                $subcont = null;
        }
        $convert_subcont = $subcont;

        return $convert_subcont;
    }
}
