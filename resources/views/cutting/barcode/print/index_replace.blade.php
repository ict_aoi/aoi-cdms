<style type="text/css">

    @page {
        margin: 10 20 0 20;
    }

    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }

    .print-friendly {
        width: 50%;
        float:left;
        line-height: 0px;
        padding-top: 8px;
        font-size: 14px;
        border-right: thick solid #000;
        border-bottom: thick solid #000;
        border-top: thick solid #000;
        border-width: 2px;
        margin-right: 1em;
        page-break-inside: avoid;

    }

    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }

    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        /* page-break-inside:auto;
        page-break-after:auto; */

        padding: 6.4px;
    }
    .barcode2 {
        float: left;
        margin-bottom: 5px;
        line-height: 16px;
        position:absolute;
        top: -18.5em;
        padding-left: 2px;
        font-family: 'impact';
    }
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -4em;
        padding-left: 200px;
        //margin-left: 230px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }

    .img_barcode {
        display: block;
        padding: 0px;
    }

    .img_barcode > img {
        width: 130px;
        height: 30px;
    }
    /////
    .img_barcode2 {
        display: block;
        padding: 0px;
    }

    .img_barcode2 > img {
        width: 180px;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .area_barcode {
        width: 25%;
    }

    p{
        line-height: 0.5;
    }

    .verticalLine {
        border-right: thick solid #ff0000;
    }

    //
    div {
        margin:0;
        padding:0;
    }
    /*table {
        border-collapse:collapse;
        margin:0;
        padding:0;
    }*/

    table.outer {
        border:1px dotted #333;
    }
    table,table tr {
        padding:0;
        margin:0;
    }
    table tr td {
        /*border:1px solid #000;*/
        padding:0;
        margin:0;
    }
    td.main {
        padding:0;
        margin:0;
        width:100mm;/*100mm;*/
        height:74mm;/*74mm;*/
    }
    tr.bold td {
        font-weight:bold;
    }

    td.col-1 {
        width:8mm;
    }
    td.col-2 {
        width:23mm;
    }
    td.col-3 {
        width:20mm;
    }
    td.col-4 {
        width:15mm;
    }
    td.col-5 {
        width:34mm;
    }

    td.box-1 {
        width:10mm;
    }
    td.box-2 {
        width:10mm;
    }
    td.box-3 {
        width:8mm;
    }
    td.box-4 {
        width:76mm;
    }

    .smallfont {
        font-size:0.9em;
    }
    .evensmallerfont {
        font-size:0.6em;
    }

    .center {
        text-align:center;
    }
    .left {
        text-align:left;
    }
    .right {
        text-align:right;
    }

    .container { 
        width: 400px; float: left; 
    }
    td.bc-cell {
        position:relative;
        padding:2mm;
    }

    tr.box, tr td.box {
        font-weight:bold;
    }
    td.box-title {
        font-size:1.5em;
    }
    td.small-font {
        font-size:0.9em;
    }
    td.med-large-font {
        font-size:1.5em;
    }
    td.med-large-font-2 {
        font-size:1.3em;

    }
    table.box-table {
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
    }
    table.box-table, table.box-table tr.content td{
        border:1px solid #333;
    }
    table.box-table tr.content td {
        padding:0 0.5em;
    }
    table.box-table tr.tr-title {
        border:1px solid #333;
        vertical-align: top;
    }
    table.box-table tr.box-header {
        vertical-align: bottom;
    }
    table.box-table tr td.bot-cel {
        border-bottom: 1pt solid #333;
    }
</style>
    @foreach($data_detail_bundle as $key => $value)
        @php
            $proname = $value->process_name != null ? ' ('.$value->process_name.')' : '';

            $komponen_name = $value->komponen_name;
            $color_name = $value->color_name;

            if(strlen($value->komponen_name.$proname)>30){
                $spn = '<span style="font-size: 9px;">'.$value->komponen_name.'/'.$value->process_name;
            }else{
                $spn = '<span style="font-size: 14px;">'.$value->komponen_name.'/'.$value->process_name;
            }

            if(strlen($value->color_name)>30){
                if(strlen($value->color_name)>45){
                    $spn_color = '<span style="font-size: 10px;">'.substr($value->color_name,0,45).' ..';
                }else{
                    $spn_color = '<span style="font-size: 10px;">'.$value->color_name;
                }
            }else{
                $spn_color = '<span style="font-size: 14px;">'.$value->color_name;
            }

            $type_name = $value->type_name != 'Non' ? $value->type_name : '';
        @endphp
            <table class="print-friendly" style="width: 52.6%; padding-left: 1px;margin-left:-15px;padding-right: 5px;margin-right:13px;margin-bottom:10px;border:solid 1px">
                <tr>
                    <td style="width:4rem;"></td>
                    <td></td>
                    <td align="right"><b style="padding-right: 20px;">{{ \Carbon\Carbon::parse($value->cutting_date)->format('d F') }}</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    @php
                        if($value->is_recycle == 'Y'){
                            echo '<td align="right"><b style="padding-right: 20px;">RECYC/'.$value->factory_alias.'</b></td>';
                        }
                        else{
                            echo '<td align="right"><b style="padding-right: 20px;">'.$value->factory_alias.'</b></td>';
                        }
                    @endphp
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width:4rem;"><b>CUT</b></td>
                    <td style="width: 4rem;" align=""><b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value->cut_num }}</b></b></td>
                    <td align="right"></td>
                </tr>
                <tr>
                    <td style="width:4rem;"><b>DELV</b></td>
                    <td style="width: 5.5rem;" align="left"><b>: {{ \Carbon\Carbon::parse($value->statistical_date)->format('d-M-y') }}</b></td>
                </tr>
                <tr>
                    <td><b>KOMPONEN</b></td>
                    <td colspan="2"><b>: @php echo $spn; @endphp </b></td>
                </tr>
                <tr>
                    <td><b>PART</b></td>
                    <td style="width: 5.5rem;"><b>: {{ $value->part }}</b></td>

                </tr>
                <tr>
                    <td>JOB</td>
                    <td colspan="2">: {{ $value->job }}</td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $value->style.$type_name }} ({{$value->season}})</td>
                </tr>
                <tr>
                    <td><b>PO/SO</b></td>
                    <td colspan="2"><b>: {{ $value->poreference.'/'.$value->destination }}</b></td>
                </tr>
                <tr>
                    <td>ARTICLE</td>
                    <td colspan="2">: {{ $value->article }}</td>
                </tr>
                <tr>
                    <td>COLL</td>
                    <td colspan="2">: @php echo $spn_color; @endphp</td>
                </tr>
                <tr>
                    <td>SIZE</td>
                    <td colspan="2">: {{ $value->size }}</td>
                </tr>
                <tr>
                    <td>QTY</td>
                    <td>: {{ $value->qty }}</td>
                    <td>Qty Total: {{ $value->qty_total }}</td>
                </tr>
                <tr>
                    <td>NO STICKER</td>
                    <td>: {{ $value->start_no.' - '.$value->end_no }}</td>
                    <td>Qty Reject: {{ $value->qty_reject }}</td>
                </tr>
                {{--@if ($value->sds_barcode != null)--}}
                <tr>
                    <td colspan="3">
                        {{--<td>QTY Total</td>
                        <td>: {{ $value->qty_total }}</td>--}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="barcode2">
                            <div class="img_barcode2">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>
            </table>
            @if(($key + 1) % 2 == 0 )
            <hr style="clear: left; margin: -0.1em; border-width: 0px; margin-left: -300%;  margin-right: -300%;" />
            @endif
    @endforeach
