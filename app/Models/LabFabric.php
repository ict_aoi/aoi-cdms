<?php
namespace App\Models;
use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class LabFabric extends Model
{
    use Uuids;
    protected $connection   = 'lab';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $table        = 'master_trf_testings';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','update_at','deleted_at'];
    protected $fillable     = ['no_trf','buyer','lab_location','nik','asal_specimen','category_specimen','type_specimen','test_required','previous_trf','date_information_remark', 'category','part_of_specimen','is_pobuyer','is_mo','size', 'style','article_no','color','created_at','updated_at','return_test_sample','last_status','deleted_at','testing_method_id','verified_lab_date','verified_lab_by','date_information','reject_by','ishandover','status_handover','platform','item','orderno','destination','factory_id','remark','t2_result','description','is_po_supplier','barcode_supplier','nomor_roll','batch_number','is_repetation','supplier_name'];
}
