<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_summary', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_number', 100)->nullable();
            $table->string('plan_ref', 100)->nullable();
            $table->string('style', 200)->nullable();
            $table->dateTime('dateordered')->nullable();
            $table->dateTime('datepromised')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->date('psd')->nullable();
            $table->date('podd')->nullable();
            $table->date('pcd')->nullable();
            $table->boolean('is_cancel_order')->default(false);
            $table->integer('factory_id')->nullable();
            $table->string('city_name',200)->nullable();
            $table->integer('c_order_id');
            $table->text('buyer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_summary');
    }
}
