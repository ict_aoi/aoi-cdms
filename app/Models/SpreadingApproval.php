<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;


class SpreadingApproval extends Model
{
   	use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'spreading_approval';
	protected $fillable = ['status','description','user_id','id_marker','created_at','updated_at','deleted_at','apv_type'];
}
