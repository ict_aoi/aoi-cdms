<style type="text/css">
    @page {
        margin-top : 5px;
        margin-bottom: 0px;
        padding-bottom:0px;
        font-family: 'sans-serif';
        margin-left: 10px;
        margin-right: 20px;
    }

    .font-family{
        font-family : 'sans-serif';
    }
    
    /*.tbhead{
        border-collapse: collapse;
        width: 100%;
        
    }
    
    .tbhead, th,tr,td{
        height: 25px;
        border: 1px white;
    }*/
    
    .aoi{
        position: absolute;
        align-content: center;
    }
    
    /*.kepada{
        text-decoration: underline;
    }*/
    
    .tbdetail{
        margin-top: 17px;
        font-family: 'Calibri';
        border-collapse: collapse;
        width: 100%;
        font-size: 12px;
        
    }
    
    .tbdetail th,tr,td{
        border: 1px solid black;
    }
    
    .tbdetail td{
        height: 8px;
    }
    
    * {
      box-sizing: border-box;
    }
    
    .column {
      float: left;
      width: 33.33%;
      /*padding: 5px;*/
      height: 130px; 
    }
    
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    
    .txfoot{
        font-size: 12px;
    }
    
    .colhead {
      float: right;
      width: 33.33%;
      /*padding: 5px;*/
      height: 50px; 
    }
    
    .colhead2 {
      float: left;
      width: 40.33%;
      /*padding: 5px;*/
      height: 50px; 
    }

    .logo{
        padding-top:0px;
        margin-top:0px;
        padding-bottom:10px;
        text-align:left;
    }

    .header_{
        margin-top:1px;
        margin-bottom:1px;
        padding-top:1px;
        padding-bottom:1px;
    }

    .no_mp{
        margin:0px;
        padding:0px;
        text-align:center;
    }

    .huruf-center {
        text-align: center;
    }

    .tbdetail tr {
        line-height: 27px;
    }
</style>    
    <div class="row">
        <div class="colhead">
            <div class="aoi">
            </div>
        </div>
        <div class="colhead">
            <center>
                <h3>PACKING LIST</h3>
            </center>
        </div>
        <div class="colhead">
        <div class="logo">
            <img src="{{url('images/general/logo_aoi.png')}}" style="width:50%;text-align:left">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="colhead2">
            <label style="font-weight:bold">Packing List No : {{ $no_packinglist }}</label>
            <br>
        </div>
        <div class="colhead2">
            <br />
            <label style="font-weight:bold">KK : {{ strtoupper($kk) }}</label>
        </div>
        <div class="colhead2">
            <label>Semarang, {{$created_at}}</label>
            <br />
            <label>
                Kepada Yth :
            </label>
            <br />
            <label style="text-decoration: underline;">{{ $proses.'-'. $factory  }}</label>
        </div>
    </div>
    <!-- <div class="row">
        <div class="colhead2">
            <center><h4>EMBRO - AOI2</h4></center>
        </div>
    </div> -->
    <div class="row">
        <table class="tbdetail">
            <tr class="font-family">
                <th class="header_" rowspan="2" width="15%"><center>STYLE</center></th>
                <th class="header_" rowspan="2" width="8%"><center>SCAN</center></th>
                <th class="header_" rowspan="2" width="10%"><center>PO</center></th>
                <th class="header_" rowspan="2" width="7%"><center>ART</center></th>
                <th class="header_" rowspan="2"><center>COLOR</center></th>
                <th class="header_" rowspan="2" width="7.5%"><center>BAGIAN</center></th>
                <!-- <th rowspan="2" width="6%"><center>POLI BAG</center></th> -->
                <th colspan="6" width="25%"><center>SIZE</center></th>
                <th class="header_" rowspan="2" width="6%"><center>TOTAL</center></th>
                <th class="header_" rowspan="2" width="6%"><center>REMARK</center></th>
            </tr>
            <tr class="font-family">
                @foreach($sizes as $size)
                    @if($size === 0)
                        <th class="huruf-center header_">&nbsp;</th>
                    @else
                        <th class="huruf-center header_">{{ $size }}</th>
                    @endif
                @endforeach
            </tr>

            @foreach($data_details as $key => $detail)
                @php
                    $komponen = explode(' ', $detail->komponen_name);
                    $count_komponen = count($komponen);
                    if($count_komponen == 1) {
                        $komponen_view = $detail->komponen_name;
                    } else {
                        $komp_array = array();
                        foreach($komponen as $komp) {
                            $komp_array[] = substr(preg_replace('/[AEIOUCNTY]/', '', $komp),0,3);
                        }
                        $komponen_view = implode(' ', $komp_array);
                    }
                    $color_name = explode(' ',$detail->color_name);
                    $count_color = count($color_name);
                    if($count_color == 1){
                        $color_name_view = $detail->color_name;
                    }else{
                        $color_arr = array();
                        foreach($color_name as $col) {
                            $color_arr[] = substr(preg_replace('/[AEIOUYS0-9]/', '', $col),0,3);
                        }
                        $color_name_view = implode(' ', $color_arr);
                    }
                @endphp
                <tr class="font-family">
                    <td class="no_mp">{{ $style_set }}</td>
                    <td class="no_mp">{{ Carbon\Carbon::parse($detail->created_at)->format('d/m/y') }}</td>
                    <td class="no_mp">{{ $detail->poreference }}</td>
                    <td class="no_mp">{{ $detail->article }}</td>
                    <td class="no_mp">{{ $color_name_view}}</td>
                    <td class="huruf-center">{{ $komponen_view }}</td>
                    <!-- <td class="huruf-center">{{ $detail->no_polybag }}</td> -->
                    @foreach($sizes as $size)
                        @if($size !== 0)
                            @php
                                $is_exist_size = 0;
                            @endphp
                            @foreach($data_size_details[$key]['data'] as $qty)
                                @if($qty->size == $size) {
                                    <td class="huruf-center">{{ $qty->qty }}</td>
                                    @php
                                        $is_exist_size++;   
                                    @endphp
                                @endif
                            @endforeach
                            @if($is_exist_size == 0)    
                                <td class="huruf-center"></td>
                            @endif
                        @else
                            <td class="huruf-center"></td>
                        @endif
                    @endforeach
                    <td class="huruf-center">{{ $total_qty_all[$key] }}</td>
                    <td class="huruf-center">{{ $remark }}</td>
                </tr>
            @endforeach
            @if(count($data_details) < 9)
                @php
                    $selisih = 8 - count($data_details);
                @endphp
                @for($i = 0; $i < $selisih; $i++)
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endfor
            @endif
        </table>
        <label style="font-size: 10px;font-style:italic"><b>BARANG-BARANG YANG SUDAH DITERIMA HARAP DIPERIKSA, KLAIM DALAM 1 HARI</b></label>
    </div>
    
    <div class="row" style="height:10px">
        <div class="column">
            <center>
                <label class="txfoot">Yang Menerima</label>
                <br>
                <br>
                <label>( .................. )</label>
            </center>
        </div>
    
        <div class="column">
            <center>
                <label class="txfoot">Pengirim Barang</label>
                <br>
                <br>
                <label>( .................. )</label>
            </center>
        </div>
    
        <div class="column">
            <center>
                <label class="txfoot">Hormat Kami,</label>
                <br>
                <br>	
                <label>( .................. )</label>
            </center>
        </div>
    </div>
    