<?php

namespace App\Models\Spreading;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class FabricUsed extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'fabric_useds';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['barcode_marker', 'barcode_fabric', 'no_roll', 'lot', 'qty_fabric', 'actual_width',
    'suplai_layer', 'actual_layer', 'suplai_sisa', 'actual_sisa', 'akumulasi_layer', 'reject', 'sambungan', 'actual_use',
     'remark', 'kualitas_gelaran', 'deleted_at', 'barcode_table', 'user_id', 'created_spreading', 'factory_id', 'po_supplier',
     'supplier_name', 'item_code', 'color', 'sambungan_end', 'item_id','spreading_report_temp_id'];

    public function cutting_marker()
    {
        return $this->belongsTo('App\Models\CuttingMarker', 'barcode_marker', 'barcode_id');
    }
}
