<div id="updatePackinglistModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Update Packinglist</span>
                </legend>
                <input type="text" class="form-control" name="kk_id_update" id="kk_id_update" placeholder="" readonly>
			</div>
            <form method="POST" action="{{ route('packinglistDistribusi.update') }}" accept-charset="UTF-8" id="form_update">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">PO Buyer</label>
                                <div class="col-lg-12">
                                    <select multiple data-placeholder="Select PO Buyer..." class="form-control select-search" name="po_buyer_insert_update[]" id="po_buyer_insert_update" required>
                                        <option value=""></option>
                                        @foreach($po_buyers as $po_buyer)
                                            <option value="{{ $po_buyer->po_buyer }}">{{ $po_buyer->po_buyer }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Subcont Tujuan</label>
                                <div class="col-lg-12">
                                    <select data-placeholder="Select a subcont..." class="form-control select-search" name="subcont_insert_update" id="subcont_insert_update" required>
                                        <option value=""></option>
                                            @foreach($subconts as $subcont)
                                                <option value="{{ $subcont->id }}">{{ $subcont->initials }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Packinglist No</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="no_packinglist_insert_update" id="no_packinglist_insert_update" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Kontrak Kerja No</label>
                                <div class="col-lg-12">
                                    <select data-placeholder="Select KK No..." class="form-control select-search" name="no_kk_insert_update" id="no_kk_insert_update" required>
                                        <option value=""></option>
                                            @foreach($kk_no as $kk_no)
                                                <option value="{{ $kk_no->kk_no }}">{{ $kk_no->kk_no }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Locator</label>
                                <div class="col-lg-12">
                                    <select multiple data-placeholder="Select a locator..." class="form-control select-search" name="locator_insert_update[]" id="locator_insert_update" required>
                                        <option value=""></option>
                                        @foreach($locators as $locator)
                                            <option value="{{ $locator->id }}">{{ $locator->locator_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Remark</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="remark_insert_update" id="remark_insert_update">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-lg-12 text-semibold">Style</label>
                                    <div class="col-lg-12">
                                        <select data-placeholder="Select a Style" class="form-control select-search" name="style_update" id="style_update" required>
                                            <option value=""></option>
                                            @foreach($style as $style)
                                                <option value="{{ $style->style }}">{{ $style->style }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-update-packinglist">Update</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
            </form>
		</div>
	</div>
</div>