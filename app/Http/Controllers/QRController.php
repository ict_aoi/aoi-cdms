<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QRController extends Controller
{
    // QR Code
    public function scanBarcode($id)
    {
        $id = $id;
        return view('qr.scan.index', compact('id'));
    }

    // QR Code Setting

    public function settingState()
    {
        return view('qr.setting_state.index');
    }
}
