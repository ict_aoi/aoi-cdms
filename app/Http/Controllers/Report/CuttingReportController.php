<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CuttingPlan;
use DB;
use Carbon\Carbon;
use App\Models\CuttingMarker;
use App\Models\RatioMarker;
use App\Models\DetailPlan;
use Excel;
use Auth;
use DataTables;

class CuttingReportController extends Controller
{
    // Report Downtime Cutting

    public function indexDowntime()
    {
        return view('report.cutting.downtime.index');
    }

    public function downloadDowntime(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);
        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );
        $cutting_date = $request->cutting_date;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_downtime_cutting')->whereBetween('created_cutting',[$range['from'],$range['to']])->whereNotNull('category_name')->where('factory_id', \Auth::user()->factory_id)->orderBy('created_cutting', 'asc');
        } else {
            $data = DB::table('jaz_report_downtime_cutting')->whereBetween('created_cutting',[$range['from'],$range['to']])->whereNotNull('category_name')->orderBy('created_cutting', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Downtime_Cutting_'.$range['from'].'-'.$range['to'];

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL CUTTING',
                        'OPERATOR',
                        'KATEGORI',
                        'MASALAH',
                        'DOWNTIME (MENIT)',
                        'PROSES',
                        'FACTORY'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->created_cutting,
                                $row->operator,
                                $row->category_name,
                                $row->keterangan,
                                $row->downtime,
                                $row->process,
                                'AOI '.$row->factory_id,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    // Report Perimeter Cutting

    public function indexPerimeter()
    {
        return view('report.cutting.perimeter.index');
    }

    public function downloadPerimeter(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $from = Carbon::parse($date_range[0]);
        $from->hour(6);
        $from->minute(0);
        $from->second(0);
        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(5);
        $to->minute(59);
        $to->second(59);

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_perimeter')->whereBetween('end_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('start_time')->whereNotNull('end_time')->where('factory_id', \Auth::user()->factory_id)->orderBy('end_time', 'asc');
        } else {
            $data = DB::table('jaz_report_perimeter')->whereBetween('end_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('end_time')->whereNotNull('start_time')->orderBy('end_time', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if($date_range[0] == $date_range[1]) {
                $filename = 'Laporan_Perimeter_'.$from->format('d-m-Y');
            } else {
                $filename = 'Laporan_Perimeter_'.$from->format('d-m-Y').'-'.$to->subDays(1)->format('d-m-Y');
            }

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL CUTTING',
                        'OPERATOR',
                        'TOTAL MP',
                        'WORKING HOUR (DEFAULT)',
                        'PERIMETER (INCH)',
                        'PERIMETER (METER)',
                        'LAMA CUTTING (MENIT)',
                        'MEJA'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->end_time,
                                $row->operator,
                                $row->total_mp,
                                $row->working_hour,
                                $row->perimeter_in_inch,
                                $row->perimeter_in_meter,
                                Carbon::parse($row->start_time)->diffInMinutes($row->end_time),
                                $row->table_name
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    // Report Scan Cutting

    public function indexCutting()
    {
        return view('report.cutting.cutting.index');
    }

    public function downloadCutting_old(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $from = Carbon::parse($date_range[0]);
        $from->hour(6);
        $from->minute(0);
        $from->second(0);
        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(5);
        $to->minute(59);
        $to->second(59);

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_cutting')->whereBetween('end_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('start_time')->whereNotNull('end_time')->where('factory_id', \Auth::user()->factory_id)->orderBy('end_time', 'asc');
        } else {
            $data = DB::table('jaz_report_cutting')->whereBetween('end_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('end_time')->whereNotNull('start_time')->orderBy('end_time', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if($date_range[0] == $date_range[1]) {
                $filename = 'Laporan_Cutting_'.$from->format('d-m-Y');
            } else {
                $filename = 'Laporan_Cutting_'.$from->format('d-m-Y').'-'.$to->subDays(1)->format('d-m-Y');
            }

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'ID MARKER',
                        'STYLE',
                        'MEJA',
                        'NAMA CUTTER',
                        'PART NO',
                        'CUT NO',
                        'START TIME',
                        'END TIME'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->barcode_marker,
                                $row->style,
                                $row->table_name,
                                $row->operator,
                                $row->part_no,
                                $row->cut,
                                $row->start_time,
                                $row->end_time,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadCutting(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $from = Carbon::parse($date_range[0]);
        $from->hour(00);
        $from->minute(00);
        $from->second(00);
        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(00);
        $to->minute(00);
        $to->second(00);

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('hs_report_cutting')->whereBetween('start_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('start_time')->whereNotNull('end_time')->where('factory_id', \Auth::user()->factory_id)->orderBy('end_time', 'asc');
        } else {
            $data = DB::table('hs_report_cutting')->whereBetween('start_time',[$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->whereNotNull('end_time')->whereNotNull('start_time')->orderBy('end_time', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if($date_range[0] == $date_range[1]) {
                $filename = 'Laporan_Cutting_'.$from->format('d-m-Y');
            } else {
                $filename = 'Laporan_Cutting_'.$from->format('d-m-Y').'-'.$to->subDays(1)->format('d-m-Y');
            }

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'ID MARKER',
                        'MEJA',
                        'STYLE',
                        'ARTICLE',
                        'PART NO',
                        'CUT NO',
                        'PO BUYER',
                        'LAYER',
                        'PERIMETER (INCH)',
                        'PANJANG MARKER',
                        'QTY RATIO',
                        'RATIO',
                        'START TIME',
                        'END TIME',
                        'DOWNTIME',
                        'KETERANGAN',
                        'NAMA CUTTER',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            if($row->header_id != null && $row->is_header) {
                                $get_plan_id = CuttingPlan::where('header_id', $row->plan_id)
                                    ->whereNull('deleted_at')
                                    ->pluck('id')
                                    ->toArray();

                                $po =  implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                                ->whereNull('deleted_at')
                                ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                                ->pluck('po_buyer')->toArray());
                            } else {
                                $po = implode(', ', DetailPlan::where('id_plan', $row->plan_id)
                                ->whereNull('deleted_at')
                                ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                                ->pluck('po_buyer')->toArray());
                            }

                            $data_excel = array(
                                $row->barcode_marker,
                                $row->table_name,
                                $row->style,
                                $row->articleno,
                                $row->part_no,
                                $row->cut,
                                $po,
                                $row->layer,
                                $row->perimeter,
                                $row->marker_length,
                                $row->qty_ratio,
                                $row->ratio_print,
                                $row->start_time,
                                $row->end_time,
                                $row->downtime,
                                $row->keterangan,
                            );

                            $name = DB::table('scan_cutting_operators')->where('scan_cutting_id',$row->id)
                            ->whereNull('deleted_at')->pluck('name')->toArray();
                            foreach ($name as $key => $v) {
                                array_push($data_excel, $v);
                            }

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }
    public function getDataBundleOutput(Request $request){
        if ($request->ajax()){
            $cut_date = explode('-', preg_replace('/\s+/', '', $request->actual_cutting));
            $from = Carbon::createFromFormat('m/d/Y H:i:s', $cut_date[0].' 00:00:00')->format('Y-m-d H:i:s');
            $to = Carbon::createFromFormat('m/d/Y H:i:s', $cut_date[1].' 24:00:00')->format('Y-m-d H:i:s');

            $data = DB::table('dd_bundle_output')
            ->where('factory_id',\Auth::user()->factory_id)
            ->whereBetween('scan_out',[$from,$to])
            ->get();

            return DataTables::of($data)
            
            ->editColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->editColumn('cut_sticker',function($data){
                $cut_sticker = $data->cut_num.' | '.$data->sticker;
                return $cut_sticker;
            })
            ->editColumn('factory_id',function ($data)
            {
                if($data->factory_id == 1){
                    $factory_id = 'AOI 1';
                }else{
                    $factory_id = 'AOI 2';
                }
                return $factory_id;
            })
            ->rawColumns(['style','factory_id','cut_sticker'])
            ->make(true);
        }else{
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function downloadBundleOutput(Request $request)
    {
        $cut_date = explode('-', preg_replace('/\s+/', '', $request->actual_cutting));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $cut_date[0].' 00:00:00')->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $cut_date[1].' 24:00:00')->format('Y-m-d H:i:s');

        $data = DB::table('dd_bundle_output')
            ->where('factory_id',\Auth::user()->factory_id)
            ->whereBetween('scan_out',[$from,$to])
            ->get();

        if($data == null)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Bundle_Output AOI ".\Auth::user()->factory_id.'-'.$from." sampai ".$to;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                            'H' => '@',
                            'I' => '@',
                            'J' => '@',
                            'K' => '@',
                            'L' => '@',
                            'M' => '@',
                            'N' => '@',
                            'O' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','SEASON');
                        $sheet->setCellValue('C1','STYLE');
                        $sheet->setCellValue('D1','PO BUYER');
                        $sheet->setCellValue('E1','ARTICLE');
                        $sheet->setCellValue('F1','CUT | STICKER');
                        $sheet->setCellValue('G1','SIZE');
                        $sheet->setCellValue('H1','KOMPONEN');
                        $sheet->setCellValue('I1','PART NUMBER');
                        $sheet->setCellValue('J1','CUTTING DATE');
                        $sheet->setCellValue('K1','QTY');
                        $sheet->setCellValue('L1','MEJA BUNDLE');
                        $sheet->setCellValue('M1','BUNDLE OPERATOR');
                        $sheet->setCellValue('N1','FACTORY');
                        $sheet->setCellValue('O1','SCAN AT');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->season);
                            $sheet->setCellValue('C'.$rw,$vl->style);
                            $sheet->setCellValue('D'.$rw,$vl->poreference);
                            $sheet->setCellValue('E'.$rw,$vl->article);
                            $sheet->setCellValue('F'.$rw,$vl->cut_num.' | '.$vl->sticker);
                            $sheet->setCellValue('G'.$rw,$vl->size);
                            $sheet->setCellValue('H'.$rw,$vl->komponen_name);
                            $sheet->setCellValue('I'.$rw,$vl->part);
                            $sheet->setCellValue('J'.$rw,$vl->cutting_date);
                            $sheet->setCellValue('K'.$rw,$vl->qty);
                            $sheet->setCellValue('L'.$rw,$vl->bundle_table);
                            $sheet->setCellValue('M'.$rw,$vl->name);
                            $sheet->setCellValue('N'.$rw,'AOI'.$vl->factory_id);
                            $sheet->setCellValue('O'.$rw,$vl->scan_out);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }
}
