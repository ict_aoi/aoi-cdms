<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterSetting extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_setting';
	protected $fillable = ['area_name','factory_id','order_name','created_at','updated_at','deleted_at'];
}
