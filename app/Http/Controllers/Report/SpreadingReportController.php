<?php

namespace App\Http\Controllers\Report;

use DB;
use Auth;
use File;
use Excel;
use Config;
use Carbon\Carbon;
use App\Models\DetailPlan;
use App\Models\RatioMarker;
use App\Models\ScanCutting;
use App\Models\CuttingPlan;
use Illuminate\Http\Request;
use App\Models\CuttingMarker;
use App\Models\CuttingMovement;
use App\Models\Data\DataCutting;
use App\Http\Controllers\Controller;
use App\Models\Spreading\FabricUsed;
use Illuminate\Support\Facades\Storage;

class SpreadingReportController extends Controller
{

    // Report Rekonsiliasi

    public function indexRekonsiliasi()
    {
        return view('report.spreading.laporan_rekonsiliasi.index');
    }

    public function downloadRekonsiliasi(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);
        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );
        $cutting_date = $request->cutting_date;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_rekonsiliasi')->whereBetween('finish_spread_date',[$range['from'],$range['to']])->where('factory_id', \Auth::user()->factory_id)->orderBy('finish_spread_date', 'asc');
        } else {
            $data = DB::table('jaz_report_rekonsiliasi')->whereBetween('finish_spread_date',[$range['from'],$range['to']])->orderBy('finish_spread_date', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Rekonsiliasi_'.$range['from'].'-'.$range['to'];

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'PLAN CUT',
                        'FINISH SPREADING',
                        'FTY',
                        'STYLE',
                        'PO BUYER',
                        'DELIVERY',
                        'ART',
                        'PART',
                        'ITEM FAB',
                        'COLOR FAB',
                        'CUT',
                        'SIZE',
                        'RASIO',
                        'QTY LAYER',
                        'QTY PCS',
                        'FABRIC WIDTH',
                        'MARKER WIDTH',
                        'PANJANG MARKER (inch)',
                        'ALLW',
                        'TOTAL',
                        'NEED FAB (dari marker)',
                        'YY FAB / PC',
                        'YY CSI',
                        'Need Fab CSI',
                        'OVERCONSUMPT',
                        'OPERATOR',
                        'MEJA',
                        'GROUP LEADER',
                        'QC',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->cutting_date,
                                $row->finish_spread_date,
                                $row->factory,
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-m-Y'),
                                $row->articleno,
                                $row->part_no,
                                $row->item_code,
                                $row->color_name,
                                $row->cut,
                                $row->size,
                                $row->ratio,
                                $row->qty_layer,
                                $row->qty_pcs,
                                $row->fabric_width,
                                $row->marker_width,
                                $row->marker_length,
                                $row->allow,
                                $row->total,
                                $row->need_fabric,
                                $row->yy_need,
                                $row->yy_csi,
                                $row->fab_csi_need,
                                $row->overconsum,
                                $row->operator,
                                $row->meja,
                                $row->gl,
                                $row->qc,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadRekonsiliasiPO(Request $request)
    {
        $this->validate($request, [
            'po_buyer' => 'required',
        ]);

        $po_buyer = $request->po_buyer;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_rekonsiliasi')->where('po_buyer', 'like', '%'.$po_buyer.'%')->where('factory_id', \Auth::user()->factory_id)->orderBy('finish_spread_date', 'asc');
        } else {
            $data = DB::table('jaz_report_rekonsiliasi')->where('po_buyer', 'like', '%'.$po_buyer.'%')->orderBy('finish_spread_date', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Rekonsiliasi_'.$po_buyer;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'PLAN CUT',
                        'FINISH SPREADING',
                        'FTY',
                        'STYLE',
                        'PO BUYER',
                        'DELIVERY',
                        'ART',
                        'PART',
                        'ITEM FAB',
                        'COLOR FAB',
                        'CUT',
                        'SIZE',
                        'RASIO',
                        'QTY LAYER',
                        'QTY PCS',
                        'FABRIC WIDTH',
                        'MARKER WIDTH',
                        'PANJANG MARKER (inch)',
                        'ALLW',
                        'TOTAL',
                        'NEED FAB (dari marker)',
                        'YY FAB / PC',
                        'YY CSI',
                        'Need Fab CSI',
                        'OVERCONSUMPT',
                        'OPERATOR',
                        'MEJA',
                        'GROUP LEADER',
                        'QC',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->cutting_date,
                                $row->finish_spread_date,
                                $row->factory,
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-m-Y'),
                                $row->articleno,
                                $row->part_no,
                                $row->item_code,
                                $row->color_name,
                                $row->cut,
                                $row->size,
                                $row->ratio,
                                $row->qty_layer,
                                $row->qty_pcs,
                                $row->fabric_width,
                                $row->marker_width,
                                $row->marker_length,
                                $row->allow,
                                $row->total,
                                $row->need_fabric,
                                $row->yy_need,
                                $row->yy_csi,
                                $row->fab_csi_need,
                                $row->overconsum,
                                $row->operator,
                                $row->meja,
                                $row->gl,
                                $row->qc,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    // Report Fabric Per Roll

    public function indexFabricRoll()
    {
        return view('report.spreading.laporan_fabric_roll.index');
    }

    public function downloadFabricRoll(Request $request)
    {
        ini_set('max_execution_time', 0);
        //0 set to unlimitted execution
        $this->validate($request, [
            'date' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date));
        $from = Carbon::parse($date_range[0]);
        $from->hour(6);
        $from->minute(0);
        $from->second(0);
        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(5);
        $to->minute(59);
        $to->second(59);

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_fabric_per_roll#1')->whereBetween('created_spreading', [$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->where('factory_id', \Auth::user()->factory_id)->orderBy('created_spreading', 'asc');
        } else {
            $data = DB::table('jaz_report_fabric_per_roll#1')->whereBetween('created_spreading', [$from->format('Y-m-d H:i:s'),$to->format('Y-m-d H:i:s')])->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if($date_range[0] == $date_range[1]) {
                $filename = 'Laporan_Fabric_Per_roll_'.$from->format('d-m-Y');
            } else {
                $filename = 'Laporan_Fabric_Per_roll_'.$from->format('d-m-Y').'-'.$to->subDays(1)->format('d-m-Y');
            }

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL PLAN',
                        'STYLE',
                        'ORDER NO/PO BUYER',
                        'DELIV',
                        'ARTICLE',
                        'COLOR DESC',
                        'PART NO',
                        'ITEM FABRIC',
                        'NO CUT',
                        'LOT',
                        'NO ROLL',
                        'PO SUPPLIER',
                        'SUPPLIER',
                        'PANJANG FABRIC',
                        'LEBAR FABRIC ACTUAL',
                        'MARKER LENGTH (YDS)',
                        'MARKER LENGTH (INC)',
                        'MARKER LENGTH (ALLW)',
                        'WIDTH MARKER',
                        'TOTAL MARKER(YDS)',
                        'YY',
                        'SIZE',
                        'QTY MARKER',
                        'QTY LAYER ACTUAL',
                        'QTY CUT (PCS)',
                        'NEED FABRIC',
                        'BALANCE',
                        'SAMBUNGAN AWAL',
                        'SAMBUNGAN AKHIR',
                        'ACTUAL SISA',
                        'REJECT',
                        'SHORT',
                        'CATATAN',
                        'OPERATOR',
                        'GL',
                        'QC',
                        'SPREAD TIME',
                        'STATUS',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-M'),
                                $row->articleno,
                                $row->color,
                                $row->part_no,
                                $row->item_code,
                                $row->cut,
                                $row->lot,
                                $row->no_roll,
                                $row->po_supplier,
                                $row->supplier_name,
                                $row->qty_fabric,
                                $row->actual_width_fabric,
                                $row->marker_length_yard,
                                $row->marker_length_inch,
                                $row->marker_allow,
                                $row->marker_width,
                                $row->total_marker_yard,
                                $row->yy,
                                $row->size,
                                $row->qty_marker,
                                $row->qty_layer,
                                $row->qty_cut,
                                $row->fabric_need,
                                $row->balance,
                                $row->sambungan,
                                $row->sambungan_end,
                                $row->actual_sisa,
                                $row->reject,
                                $row->short_roll,
                                $row->catatan,
                                $row->operator,
                                $row->gl,
                                $row->qc,
                                Carbon::parse($row->created_spreading)->format('Y-m-d H:i:s'),
                                $row->status,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadFabricRollPOBuyer(Request $request)
    {
        $this->validate($request, [
            'po_buyer' => 'required',
        ]);

        $po_buyer = $request->po_buyer;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('po_buyer', $po_buyer)->where('factory_id', \Auth::user()->factory_id)->orderBy('created_spreading', 'asc');
        } else {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('po_buyer', $po_buyer)->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Fabric_Per_roll_'.$po_buyer;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL PLAN',
                        'STYLE',
                        'ORDER NO/PO BUYER',
                        'DELIV',
                        'ARTICLE',
                        'COLOR DESC',
                        'PART NO',
                        'ITEM FABRIC',
                        'NO CUT',
                        'LOT',
                        'NO ROLL',
                        'PO SUPPLIER',
                        'SUPPLIER',
                        'PANJANG FABRIC',
                        'LEBAR FABRIC ACTUAL',
                        'MARKER LENGTH (YDS)',
                        'MARKER LENGTH (INC)',
                        'MARKER LENGTH (ALLW)',
                        'WIDTH MARKER',
                        'TOTAL MARKER(YDS)',
                        'YY',
                        'SIZE',
                        'QTY MARKER',
                        'QTY LAYER ACTUAL',
                        'QTY CUT (PCS)',
                        'NEED FABRIC',
                        'BALANCE',
                        'SAMBUNGAN AWAL',
                        'SAMBUNGAN AKHIR',
                        'ACTUAL SISA',
                        'REJECT',
                        'SHORT',
                        'CATATAN',
                        'OPERATOR',
                        'GL',
                        'QC',
                        'SPREAD TIME',
                        'STATUS',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-M'),
                                $row->articleno,
                                $row->color,
                                $row->part_no,
                                $row->item_code,
                                $row->cut,
                                $row->lot,
                                $row->no_roll,
                                $row->po_supplier,
                                $row->supplier_name,
                                $row->qty_fabric,
                                $row->actual_width_fabric,
                                $row->marker_length_yard,
                                $row->marker_length_inch,
                                $row->marker_allow,
                                $row->marker_width,
                                $row->total_marker_yard,
                                $row->yy,
                                $row->size,
                                $row->qty_marker,
                                $row->qty_layer,
                                $row->qty_cut,
                                $row->fabric_need,
                                $row->balance,
                                $row->sambungan,
                                $row->sambungan_end,
                                $row->actual_sisa,
                                $row->reject,
                                $row->short_roll,
                                $row->catatan,
                                $row->operator,
                                $row->gl,
                                $row->qc,
                                Carbon::parse($row->created_spreading)->format('Y-m-d H:i:s'),
                                $row->status,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadFabricRollPOSupplier(Request $request)
    {
        $this->validate($request, [
            'po_supplier' => 'required',
        ]);

        $po_supplier = $request->po_supplier;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('po_supplier', $po_supplier)->where('factory_id', \Auth::user()->factory_id)->orderBy('created_spreading', 'asc');
        } else {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('po_supplier',$po_supplier)->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Fabric_Per_roll_'.$po_supplier;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL PLAN',
                        'STYLE',
                        'ORDER NO/PO BUYER',
                        'DELIV',
                        'ARTICLE',
                        'COLOR DESC',
                        'PART NO',
                        'ITEM FABRIC',
                        'NO CUT',
                        'LOT',
                        'NO ROLL',
                        'PO SUPPLIER',
                        'SUPPLIER',
                        'PANJANG FABRIC',
                        'LEBAR FABRIC ACTUAL',
                        'MARKER LENGTH (YDS)',
                        'MARKER LENGTH (INC)',
                        'MARKER LENGTH (ALLW)',
                        'WIDTH MARKER',
                        'TOTAL MARKER(YDS)',
                        'YY',
                        'SIZE',
                        'QTY MARKER',
                        'QTY LAYER ACTUAL',
                        'QTY CUT (PCS)',
                        'NEED FABRIC',
                        'BALANCE',
                        'SAMBUNGAN AWAL',
                        'SAMBUNGAN AKHIR',
                        'ACTUAL SISA',
                        'REJECT',
                        'SHORT',
                        'CATATAN',
                        'OPERATOR',
                        'GL',
                        'QC',
                        'SPREAD TIME',
                        'STATUS',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-M'),
                                $row->articleno,
                                $row->color,
                                $row->part_no,
                                $row->item_code,
                                $row->cut,
                                $row->lot,
                                $row->no_roll,
                                $row->po_supplier,
                                $row->supplier_name,
                                $row->qty_fabric,
                                $row->actual_width_fabric,
                                $row->marker_length_yard,
                                $row->marker_length_inch,
                                $row->marker_allow,
                                $row->marker_width,
                                $row->total_marker_yard,
                                $row->yy,
                                $row->size,
                                $row->qty_marker,
                                $row->qty_layer,
                                $row->qty_cut,
                                $row->fabric_need,
                                $row->balance,
                                $row->sambungan,
                                $row->sambungan_end,
                                $row->actual_sisa,
                                $row->reject,
                                $row->short_roll,
                                $row->catatan,
                                $row->operator,
                                $row->gl,
                                $row->qc,
                                Carbon::parse($row->created_spreading)->format('Y-m-d H:i:s'),
                                $row->status,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadFabricRollItemCode(Request $request)
    {
        $this->validate($request, [
            'item_code' => 'required',
        ]);

        $item_code = $request->item_code;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('item_code', $item_code)->where('factory_id', \Auth::user()->factory_id)->orderBy('created_spreading', 'asc');
        } else {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('item_code',$item_code)->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Fabric_Per_roll_'.$item_code;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL PLAN',
                        'STYLE',
                        'ORDER NO/PO BUYER',
                        'DELIV',
                        'ARTICLE',
                        'COLOR DESC',
                        'PART NO',
                        'ITEM FABRIC',
                        'NO CUT',
                        'LOT',
                        'NO ROLL',
                        'PO SUPPLIER',
                        'SUPPLIER',
                        'PANJANG FABRIC',
                        'LEBAR FABRIC ACTUAL',
                        'MARKER LENGTH (YDS)',
                        'MARKER LENGTH (INC)',
                        'MARKER LENGTH (ALLW)',
                        'WIDTH MARKER',
                        'TOTAL MARKER(YDS)',
                        'YY',
                        'SIZE',
                        'QTY MARKER',
                        'QTY LAYER ACTUAL',
                        'QTY CUT (PCS)',
                        'NEED FABRIC',
                        'BALANCE',
                        'SAMBUNGAN AWAL',
                        'SAMBUNGAN AKHIR',
                        'ACTUAL SISA',
                        'REJECT',
                        'SHORT',
                        'CATATAN',
                        'OPERATOR',
                        'GL',
                        'QC',
                        'SPREAD TIME',
                        'STATUS',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-M'),
                                $row->articleno,
                                $row->color,
                                $row->part_no,
                                $row->item_code,
                                $row->cut,
                                $row->lot,
                                $row->no_roll,
                                $row->po_supplier,
                                $row->supplier_name,
                                $row->qty_fabric,
                                $row->actual_width_fabric,
                                $row->marker_length_yard,
                                $row->marker_length_inch,
                                $row->marker_allow,
                                $row->marker_width,
                                $row->total_marker_yard,
                                $row->yy,
                                $row->size,
                                $row->qty_marker,
                                $row->qty_layer,
                                $row->qty_cut,
                                $row->fabric_need,
                                $row->balance,
                                $row->sambungan,
                                $row->sambungan_end,
                                $row->actual_sisa,
                                $row->reject,
                                $row->short_roll,
                                $row->catatan,
                                $row->operator,
                                $row->gl,
                                $row->qc,
                                Carbon::parse($row->created_spreading)->format('Y-m-d H:i:s'),
                                $row->status,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function getDownloadPerRoll(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('poreference', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                        ->whereNull('deleted_at')
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details()
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    }
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('action', function($data) {
                    return '<a href="'.route('reportSpreading.downloadFabricRollPlan', $data->id).'" target="_blank" class="btn btn-primary btn-icon"><i class="icon-download"></i></a>';
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function downloadFabricRollPlan($id)
    {
        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('plan_id', $id)->where('factory_id', \Auth::user()->factory_id)->orderBy('part_no', 'asc')->orderBy('cut', 'asc')->orderBy('created_spreading', 'asc');
        } else {
            $data = DB::table('jaz_report_fabric_per_roll#1')->where('plan_id',$id)->orderBy('part_no', 'asc')->orderBy('cut', 'asc')->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Fabric_Per_roll_Plan_'.$id;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL PLAN',
                        'STYLE',
                        'ORDER NO/PO BUYER',
                        'DELIV',
                        'ARTICLE',
                        'COLOR DESC',
                        'PART NO',
                        'ITEM FABRIC',
                        'NO CUT',
                        'LOT',
                        'NO ROLL',
                        'PO SUPPLIER',
                        'SUPPLIER',
                        'PANJANG FABRIC',
                        'LEBAR FABRIC ACTUAL',
                        'MARKER LENGTH (YDS)',
                        'MARKER LENGTH (INC)',
                        'MARKER LENGTH (ALLW)',
                        'WIDTH MARKER',
                        'TOTAL MARKER(YDS)',
                        'YY',
                        'SIZE',
                        'QTY MARKER',
                        'QTY LAYER ACTUAL',
                        'QTY CUT (PCS)',
                        'NEED FABRIC',
                        'BALANCE',
                        'SAMBUNGAN AWAL',
                        'SAMBUNGAN AKHIR',
                        'ACTUAL SISA',
                        'REJECT',
                        'SHORT',
                        'CATATAN',
                        'OPERATOR',
                        'GL',
                        'QC',
                        'SPREAD TIME',
                        'STATUS',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->style,
                                $row->po_buyer,
                                Carbon::parse($row->deliv_date)->format('d-M'),
                                $row->articleno,
                                $row->color,
                                $row->part_no,
                                $row->item_code,
                                $row->cut,
                                $row->lot,
                                $row->no_roll,
                                $row->po_supplier,
                                $row->supplier_name,
                                $row->qty_fabric,
                                $row->actual_width_fabric,
                                $row->marker_length_yard,
                                $row->marker_length_inch,
                                $row->marker_allow,
                                $row->marker_width,
                                $row->total_marker_yard,
                                $row->yy,
                                $row->size,
                                $row->qty_marker,
                                $row->qty_layer,
                                $row->qty_cut,
                                $row->fabric_need,
                                $row->balance,
                                $row->sambungan,
                                $row->sambungan_end,
                                $row->actual_sisa,
                                $row->reject,
                                $row->short_roll,
                                $row->catatan,
                                $row->operator,
                                $row->gl,
                                $row->qc,
                                Carbon::parse($row->created_spreading)->format('Y-m-d H:i:s'),
                                $row->status,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    // Report Data Potong

    public function indexDataPotong()
    {
        return view('report.spreading.laporan_data_potong.index');
    }

    public function downloadDataPotong(Request $request)
    {
        $this->validate($request, [
            'date_range' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );

        if(\Auth::user()->factory_id == 0 && \Auth::user()->factory->status == 'internal') {
            $get_data = DB::table('hs_report_data_potong_partial')->whereBetween('cutting_date',[$range['from'],$range['to']])->get();
        } elseif(\Auth::user()->factory_id > 0 && \Auth::user()->factory->status == 'internal') {
            $get_data = DB::table('hs_report_data_potong_partial')->whereBetween('cutting_date',[$range['from'],$range['to']])->where('factory_alias', \Auth::user()->factory->factory_alias)->get();
        } else {
            return response()->json('Data tidak ditemukan.', 422);
        }

        if(count($get_data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {

            $get_max_cutting = DB::table('hs_report_data_potong_partial')->whereBetween('cutting_date',[$range['from'],$range['to']])->orderBy('cut_total', 'desc')->first()->cut_total;

            $filename = 'Laporan_Data_potong_'.$range['from'].'-'.$range['to'];

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($get_data,$get_max_cutting,$i) {
                $excel->sheet('report', function($sheet) use($get_data, $get_max_cutting,$i) {
                    $header = array('FACTORY', 'PLAN CUT', 'FINISH SPREADING', 'STYLE', 'PO', 'PART', 'DELV', 'ART', 'COLOR', 'SIZE', 'QTY');

                    for($i = 0; $i < $get_max_cutting; $i++) {
                        $number = $i + 1;
                        array_push($header, 'Cut '.$number.' (need)', 'Cut '.$number.' (actual)');
                    }

                    array_push($header, 'BALANCE');

                    $sheet->appendRow($header);

                    foreach ($get_data as $key => $row)
                    {
                        if($row->header_id != null && $row->is_header) {
                            $get_plan_id = CuttingPlan::where('header_id', $row->id)
                                ->whereNull('deleted_at')
                                ->pluck('id')
                                ->toArray();
                        } else {
                            $get_plan_id[] = $row->id;
                        }

                        $part_no = explode('+', $row->part_no);

                        $data_cut = DataCutting::whereIn('plan_id',$get_plan_id)
                        ->where('warehouse',Auth::user()->factory_id)
                        ->where('part_no',$part_no[0])
                        ->select(DB::raw("articleno, color_name, po_buyer, size_finish_good, part_no, warehouse, min(statistical_date) as deliv_date, sum(ordered_qty) as qty"))
                        ->groupBy('articleno','color_name','po_buyer','size_finish_good','part_no','warehouse')
                        ->get();

                        foreach ($data_cut as $key => $value) {
                            $data_excel = array(
                                $row->factory_alias,
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->finish_spreading,
                                $row->style,
                                $value->po_buyer,
                                $row->part_no,
                                Carbon::parse($value->deliv_date)->format('d-m-Y'),
                                $value->articleno,
                                $value->color_name,
                                $value->size_finish_good,
                                $value->qty,
                            );

                            for($max = 1; $max <= $get_max_cutting; $max++) {
                                $get_marker = CuttingMarker::where('id_plan', $row->id)
                                ->where('part_no', $row->part_no)
                                ->where('cut', $max)->where('deleted_at', null)->first();

                                if($get_marker != null) {
                                    $part_no = explode('+', $get_marker->part_no);

                                    $get_qty_cut = DB::table('ratio_detail')
                                    ->where('barcode_id', $get_marker->barcode_id)
                                    ->where('size', $value->size_finish_good)
                                    ->where('po_buyer', $value->po_buyer)
                                    ->whereIn('part_no', $part_no)
                                    ->where('deleted_at', null)->get();

                                    if($get_qty_cut->sum('qty') > 0) {
                                        array_push($data_excel, $get_qty_cut->sum('qty'), null);
                                    } else {
                                        array_push($data_excel, null, null);
                                    }
                                } else {
                                    array_push($data_excel, null, null);
                                }
                            }

                            $row_number = $key + 2;
                            $max_column = 11 + ($get_max_cutting * 2);

                            $balance = '=(SUM(L'.$row_number.':'.$this->getNameFromNumber($max_column).$row_number.')-K'.$row_number.')';
                            array_push($data_excel,$balance);
                            $sheet->appendRow($data_excel);
                        }
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function downloadDataPotongPO(Request $request)
    {
        $this->validate($request, [
            'po_buyer' => 'required',
        ]);

        $po_buyer = $request->po_buyer;

        // if(\Auth::user()->factory_id == 0 && \Auth::user()->factory->status == 'internal') {
        //     $get_data = DB::table('hs_report_data_potong_partial')->where('po_buyer', $po_buyer)->get();
        // } elseif(\Auth::user()->factory_id > 0 && \Auth::user()->factory->status == 'internal') {
        //     $get_data = DB::table('hs_report_data_potong_partial')->where('po_buyer', $po_buyer)->where('factory_alias', \Auth::user()->factory->factory_alias)->get();
        // } else {
        //     return response()->json('Data tidak ditemukan.', 422);
        // }

        // $get_data = DB::select(DB::raw())

        $get_data = DB::select(DB::raw("SELECT cutting_plans2.factory_id, cutting_plans2.id, cutting_plans2.header_id, cutting_plans2.is_header, cutting_date, style, articleno, cutting_plans2.color, ratio_detail.po_buyer, cutting_marker.part_no, max(cutting_marker.cut) as cut from ratio_detail
        LEFT JOIN cutting_marker on cutting_marker.barcode_id = ratio_detail.barcode_id
        LEFT JOIN cutting_plans2 on cutting_plans2.id = ratio_detail.plan_id
        where po_buyer = '$po_buyer' and ratio_detail.deleted_at is null
        and cutting_marker.deleted_at is null and cutting_plans2. deleted_at is null
        GROUP BY  cutting_plans2.factory_id, cutting_plans2.id, cutting_plans2.header_id, cutting_plans2.is_header, cutting_date, style, articleno, cutting_plans2.color, ratio_detail.po_buyer, cutting_marker.part_no
        ORDER BY cutting_marker.part_no
        "));

        if(count($get_data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {

            $get_max_cutting = DB::select(DB::raw("SELECT cutting_plans2.factory_id, cutting_plans2.id, cutting_plans2.header_id, cutting_plans2.is_header, cutting_date, style, articleno, cutting_plans2.color, ratio_detail.po_buyer, cutting_marker.part_no, max(cutting_marker.cut) as cut from ratio_detail
            LEFT JOIN cutting_marker on cutting_marker.barcode_id = ratio_detail.barcode_id
            LEFT JOIN cutting_plans2 on cutting_plans2.id = ratio_detail.plan_id
            where po_buyer = '$po_buyer' and ratio_detail.deleted_at is null
            and cutting_marker.deleted_at is null and cutting_plans2. deleted_at is null
            GROUP BY  cutting_plans2.factory_id, cutting_plans2.id, cutting_plans2.header_id, cutting_plans2.is_header, cutting_date, style, articleno, cutting_plans2.color, ratio_detail.po_buyer, cutting_marker.part_no
            order by cut desc
            limit 1
            "))[0]->cut;

            $filename = 'Laporan_Data_potong_'.$po_buyer;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($get_data,$get_max_cutting,$i) {
                $excel->sheet('report', function($sheet) use($get_data, $get_max_cutting,$i) {
                    $header = array('FACTORY', 'PLAN CUT', 'FINISH SPREADING', 'STYLE', 'PO', 'PART', 'DELV', 'ART', 'COLOR', 'SIZE', 'QTY');

                    for($i = 0; $i < $get_max_cutting; $i++) {
                        $number = $i + 1;
                        array_push($header, 'Cut '.$number.' (need)', 'Cut '.$number.' (actual)');
                    }

                    array_push($header, 'BALANCE');

                    $sheet->appendRow($header);

                    foreach ($get_data as $key => $row)
                    {
                        $part_no = explode('+', $row->part_no);

                        $data_cut = DataCutting::where('plan_id',$row->id)
                        ->where('warehouse',Auth::user()->factory_id)
                        ->where('part_no',$part_no[0])
                        ->select(DB::raw("style, articleno, color_name, po_buyer, size_finish_good, part_no, warehouse, min(statistical_date) as deilv_date, sum(ordered_qty) as qty"))
                        ->groupBy('style','articleno','color_name','po_buyer','size_finish_good','part_no','warehouse')
                        ->get();

                        foreach ($data_cut as $key => $value) {

                            $data_excel = array(
                                $row->factory_id == 1 ? 'AOI1' : 'AOI2',
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                '',
                                $row->style,
                                $value->po_buyer,
                                $row->part_no,
                                Carbon::parse($value->deliv_date)->format('d-m-Y'),
                                $value->articleno,
                                $value->color_name,
                                $value->size_finish_good,
                                $value->qty,
                            );

                            for($max = 1; $max <= $get_max_cutting; $max++) {
                                $get_marker = CuttingMarker::where('id_plan', $row->id)
                                ->where('part_no', $row->part_no
                                )->where('cut', $max)->where('deleted_at', null)->first();

                                if($get_marker != null) {

                                    $get_qty_cut = DB::table('ratio_detail')
                                    ->where('barcode_id', $get_marker->barcode_id)
                                    ->where('size', $value->size_finish_good)
                                    ->where('po_buyer', $value->po_buyer)
                                    ->whereIn('part_no', $part_no)
                                    ->where('deleted_at', null)->get();

                                    if($get_marker->count() > 0) {
                                        if($get_qty_cut->sum('qty') > 0) {
                                            array_push($data_excel, $get_qty_cut->sum('qty'), null);
                                        } else {
                                            array_push($data_excel, null, null);
                                        }
                                    } else {
                                        array_push($data_excel, null, null);
                                    }
                                } else {
                                    array_push($data_excel, null, null);
                                }
                            }

                            $row_number = $key + 2;
                            $max_column = 11 + ($get_max_cutting * 2);

                            $balance = '=(SUM(L'.$row_number.':'.$this->getNameFromNumber($max_column).$row_number.')-K'.$row_number.')';
                            array_push($data_excel,$balance);
                            $sheet->appendRow($data_excel);
                        }
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function getDownloadDataPotong(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('poreference', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                        ->whereNull('deleted_at')
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details()
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    }

                    // $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    // return implode(', ', $po_array);
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('action', function($data) {
                    return '<a href="'.route('reportSpreading.downloadDataPotongPlan', $data->id).'" target="_blank" class="btn btn-primary btn-icon"><i class="icon-download"></i></a>';
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function downloadDataPotongPlan($id)
    {
        ini_set('max_execution_time', 0);
        //0 set to unlimitted execution
        $get_plan = CuttingPlan::where('id', $id)
            ->where('deleted_at', null)
            ->first();

        if(\Auth::user()->factory_id == 0 && \Auth::user()->factory->status == 'internal') {
            $get_data = DB::table('hs_report_data_potong_partial')
                ->where('queu', $get_plan->queu)
                ->where('cutting_date', $get_plan->cutting_date)
                ->get();

        } elseif(\Auth::user()->factory_id > 0 && \Auth::user()->factory->status == 'internal') {
            $get_data = DB::table('hs_report_data_potong_partial')
                ->where('queu', $get_plan->queu)
                ->where('cutting_date', $get_plan->cutting_date)
                ->where('factory_alias', \Auth::user()->factory->factory_alias)
                ->get();

        } else {
            return response()->json('Data tidak ditemukan.', 422);
        }

        if(count($get_data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {

            $get_max_cutting = DB::table('hs_report_data_potong_partial')->where('queu', $get_plan->queu)->where('factory_alias', \Auth::user()->factory->factory_alias)->where('cutting_date', $get_plan->cutting_date)->orderBy('cut_total', 'desc')->first()->cut_total;

            $filename = 'Laporan_Data_potong_'.$get_plan->cutting_date.'_Queue_'.$get_plan->queu;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($get_data,$get_max_cutting,$i) {
                $excel->sheet('report', function($sheet) use($get_data, $get_max_cutting,$i) {
                    $header = array('FACTORY', 'PLAN CUT', 'FINISH SPREADING', 'STYLE', 'PO', 'PART', 'DELV', 'ART', 'COLOR', 'SIZE', 'QTY');

                    for($i = 0; $i < $get_max_cutting; $i++) {
                        $number = $i + 1;
                        $preparation_header = 'Preparation (CUT '.$number.')';
                        $spreading_header = 'Spreading (CUT '.$number.')';
                        $cutting_header = 'Cutting (CUT '.$number.')';
                        $bundling_header = 'Bundling (CUT '.$number.')';
                        array_push($header, $preparation_header, $spreading_header, $cutting_header, $bundling_header);
                    }

                    array_push($header, 'Preparation (BALANCE)', 'Spreading (BALANCE)', 'Cutting (BALANCE)', 'Bundling (BALANCE)');

                    $sheet->appendRow($header);

                    foreach ($get_data as $key => $row)
                    {
                        if($row->header_id != null && $row->is_header) {
                            $get_plan_id = CuttingPlan::where('header_id', $row->id)
                                ->whereNull('deleted_at')
                                ->pluck('id')
                                ->toArray();
                        } else {
                            $get_plan_id[] = $row->id;
                        }

                        $part_no = explode('+', $row->part_no);

                        $data_cut = DataCutting::whereIn('plan_id',$get_plan_id)
                        ->where('warehouse',Auth::user()->factory_id)
                        ->where('part_no',$part_no[0])
                        ->select(DB::raw("articleno, color_name, po_buyer, size_finish_good, warehouse, min(statistical_date) as deliv_date, sum(ordered_qty) as qty"))
                        ->groupBy('articleno','color_name','po_buyer','size_finish_good','warehouse')
                        ->get();

                        foreach ($data_cut as $key => $value) {
                            $data_excel = array(
                                $row->factory_alias,
                                Carbon::parse($row->cutting_date)->format('d-M'),
                                $row->finish_spreading,
                                $row->style,
                                $value->po_buyer,
                                $row->part_no,
                                Carbon::parse($value->deliv_date)->format('d-m-Y'),
                                $value->articleno,
                                $value->color_name,
                                $value->size_finish_good,
                                $value->qty,
                            );

                            for($max = 1; $max <= $get_max_cutting; $max++) {
                                $get_marker = CuttingMarker::where('id_plan', $row->id)
                                ->where('part_no', $row->part_no)
                                ->where('cut', $max)->where('deleted_at', null)->first();

                                if($get_marker != null) {

                                    $get_qty_cut = DB::table('ratio_detail')
                                    ->where('barcode_id', $get_marker->barcode_id)
                                    ->where('size', $value->size_finish_good)
                                    ->where('po_buyer', $value->po_buyer)
                                    ->whereIn('part_no', $part_no)
                                    ->where('deleted_at', null)->get();

                                    if($get_qty_cut->sum('qty') > 0) {
                                        array_push($data_excel, $get_qty_cut->sum('qty'));
                                        $spreading_check = FabricUsed::where('barcode_marker', $get_marker->barcode_id)->whereNull('deleted_at')->get();
                                        if(count($spreading_check) > 0) {
                                            array_push($data_excel, $get_qty_cut->sum('qty'));
                                            $cutting_check = ScanCutting::where('barcode_marker', $get_marker->barcode_id)->where('state', 'done')->whereNull('deleted_at')->get();
                                            if(count($cutting_check) > 0) {
                                                array_push($data_excel, $get_qty_cut->sum('qty'), null);
                                            } else {
                                                array_push($data_excel, null, null);
                                            }
                                        } else {
                                            array_push($data_excel, null, null, null);
                                        }
                                    } else {
                                        array_push($data_excel, null, null, null, null);
                                    }
                                } else {
                                    array_push($data_excel, null, null, null, null);
                                }
                            }
                            $row_number = $key + 2;
                            $max_column = 11 + ($get_max_cutting * 4);

                            $column_preparation = array();
                            $column_spreading = array();
                            $column_cutting = array();
                            $column_bundling = array();

                            for($i = 0; $i < $get_max_cutting; $i++) {
                                $ratio_preparation = 11 + ($i * 4) + 1;
                                $column_preparation[] = $this->getNameFromNumber($ratio_preparation).$row_number;
                                $ratio_spreading = 11 + ($i * 4) + 2;
                                $column_spreading[] = $this->getNameFromNumber($ratio_spreading).$row_number;
                                $ratio_cutting = 11 + ($i * 4) + 3;
                                $column_cutting[] = $this->getNameFromNumber($ratio_cutting).$row_number;
                                $ratio_bundling = 11 + ($i * 4) + 4;
                                $column_bundling[] = $this->getNameFromNumber($ratio_bundling).$row_number;
                            }

                            $preparation_view = implode(',',$column_preparation);
                            $spreading_view = implode(',',$column_spreading);
                            $cutting_view = implode(',',$column_cutting);
                            $bundling_view = implode(',',$column_bundling);

                            $preparation_balance = '=(SUM('.$preparation_view.')-K'.$row_number.')';
                            $spreading_balance = '=(SUM('.$spreading_view.')-K'.$row_number.')';
                            $cutting_balance = '=(SUM('.$cutting_view.')-K'.$row_number.')';
                            $bundling_balance = '=(SUM('.$bundling_view.')-K'.$row_number.')';

                            array_push($data_excel,$preparation_balance, $spreading_balance, $cutting_balance, $bundling_balance);
                            $sheet->appendRow($data_excel);
                        }
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function reportQcSpreading()
    {
        $monday = Carbon::now()->startOfWeek();
        $start  = $monday->copy()->subDays(7)->format('d-m-Y');
        $end    = $monday->copy()->subDays(1)->format('d-m-Y');
        // dd($monday, $start, $end);
        return view('report/spreading/laporan_qc_spreading/index', compact('start','end'));
    }

    public function ajaxGetReportQcSpreading(Request $request)
    {
        $date_range = explode('-', preg_replace('/\s+/', '', $request->spread));
        // $range = array(
        //     'from' => date_format(date_create($date_range[0]), 'Y-m-d 00:00:00'),
        //     'to' => date_format(date_create($date_range[1]), 'Y-m-d 00:00:00')
        // );

        $from = Carbon::parse($date_range[0]);
        $from->hour(0);
        $from->minute(0);
        $from->second(0);
        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(0);
        $to->minute(0);
        $to->second(0);

        $f = $from->format('Y-m-d H:i:s');
        $t = $to->format('Y-m-d H:i:s');

        // $from    = $range['from'];
        // $to      = $range['to'];
        $factory = \Auth::user()->factory_id;

        $data = DB::select(db::raw("SELECT * FROM get_report_qc_spreading('".$f."','".$t."',$factory)"));

        $filename = 'Report_QC_spreading_'.$f.'-'.$t;
        if (count($data)>0) {
            $i=1;
            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        '#',
                        'PLAN CUT',
                        'SCAN ROLL SPREADING',
                        'STYLE',
                        'PO BUYER',
                        'ART',
                        'PART',
                        'ITEM FABRIC',
                        'COLOR FAB',
                        'CUT',
                        'BARCODE MARKER',
                        'SIZE',
                        'RATIO',
                        'QTY LAYER(supplai)',
                        'QTY LAYER (aktual)',
                        'SAMBUNGAN AWAL (yard)',
                        'SAMBUNGAN AKHIR (yard)',
                        'QTY PCS',
                        'SUPPLIER FABRIC',
                        'OPERATOR', 'MEJA',
                        'GROUP LEADER',
                        'QC',
                        'NO ROLL',
                        'LOT',
                        'PANJANG SUPLAI',
                        'PANJANG AKTUAL',
                        'LEBAR SUPLAI',
                        'LEBAR AKTUAL',
                        'POIN FIR',
                        'BAU / ODOR',
                        'PANJANG',
                        'LEBAR',
                        'TINGGI GELARAN',
                        'PANJANG MARKER + ALLOWANCE',
                        'AKTUAL PANJANG',
                        'AKTUAL LEBAR',
                        'PANJANG JEBLOS',
                        'LEBAR JEBLOS',
                        'TIDAK RAPI',
                        'TERLALU KENCANG',
                        'TERLALU KENDUR',
                        'SAMBUNGAN',
                        'TOTAL',
                        'REMARK'
                    ));

                    foreach ($data as $dt)
                    {
                        $get_plan_id = array();
                        if($dt->header_id != null && $dt->is_header) {
                            $get_plan_id = CuttingPlan::where('header_id', $dt->plan_id)
                                ->whereNull('deleted_at')
                                ->pluck('id')
                                ->toArray();
                        } else {
                            $get_plan_id[] = $dt->plan_id;
                        }

                        $po = implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                        ->whereNull('deleted_at')
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());


                        $get_fir_point = DB::connection('wms_live')->table('material_checks')->where('barcode_preparation', $dt->barcode_fabric)->first();
                        // $item = DB::connection('wms_live')
                        //     ->table('items')
                        //     ->where('item_id', $dt->item_id)
                        //     ->first();

                        // $ratio = DB::table('ratio')
                        //     ->select(DB::raw("string_agg(size,'/') as size"),DB::raw("SUM(ratio) as qty"))
                        //     ->where('id_marker',$dt->barcode_marker)
                        //     ->groupby('id_marker')
                        //     ->first();

                        // $operator = DB::table('users')
                        //     ->where('id',$dt->user_id)
                        //     ->first();

                        // $table = DB::table('master_table')
                        //     ->where('id_table',$dt->barcode_table)
                        //     ->first();

                        // $gl = DB::table('spreading_approval')
                        //     ->select('users.name')
                        //     ->join('users','spreading_approval.user_id','=','users.id')
                        //     ->where('id_marker',$dt->barcode_marker)
                        //     ->where('apv_type','GL Spreading')
                        //     ->first();

                        // $qc = DB::table('spreading_approval')
                        //     ->select('users.name')
                        //     ->join('users','spreading_approval.user_id','=','users.id')
                        //     ->where('id_marker',$dt->barcode_marker)
                        //     ->where('apv_type','QC Spreading')
                        //     ->first();

                        // $marspr = DB::table('marker_spreading')
                        //     ->where('id_marker',$dt->barcode_marker)
                        //     ->first();

                        $data_excel = array(
                            $i++,
                            $dt->cutting_date,
                            $dt->created_spreading,
                            $dt->style,
                            $po,
                            $dt->articleno,
                            $dt->part_no,
                            $dt->upc,
                            $dt->color_name,
                            $dt->cut,
                            $dt->barcode_marker,
                            $dt->ratio_print,
                            $dt->qty_ratio,
                            $dt->suplai_layer,
                            $dt->actual_layer,
                            $dt->sambungan,
                            $dt->sambungan_end,
                            ($dt->actual_layer * $dt->qty_ratio),
                            '-',
                            isset($dt->operator) ? $dt->operator :'--',
                            $dt->table_name,
                            isset($dt->gl_name) ? $dt->gl_name :'--',
                            isset($dt->qc_name) ? $dt->qc_name :'--',
                            $dt->no_roll,
                            $dt->lot,
                            $dt->qty_fabric,
                            ($dt->actual_use + $dt->remark),
                            $dt->actual_width,
                            $dt->actual_width,
                            $get_fir_point == null ? '' : $get_fir_point->total_yds,
                            $dt->bau,
                            $dt->marker_length,
                            ($dt->fabric_width-0.5),
                            isset($dt->spreading_high_qc) ? $dt->spreading_high_qc :'--',
                            ($dt->marker_length + 0.75),
                            isset($dt->actual_length_qc) ? $dt->actual_length_qc : '--',
                            isset($dt->actual_width_qc) ? $dt->actual_width_qc : '--',
                            $dt->l_jeblos,
                            $dt->w_jeblos,
                            $dt->rapi,
                            $dt->kencang,
                            $dt->kendur,
                            $dt->sambungan_qc,
                            ($dt->l_jeblos+$dt->w_jeblos+$dt->rapi+$dt->kencang+$dt->kendur+$dt->sambungan_qc),
                            isset($dt->remark_qc) ? $dt->remark_qc :'--',
                        );
                        $sheet->appendRow($data_excel);
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }else{
            return view('report/spreading/laporan_qc_spreading/index');
        }

    }

    // Report Downtime Spreading

    public function indexDowntime()
    {
        return view('report.spreading.downtime.index');
    }

    public function downloadDowntime(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);
        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to' => date_format(date_create($date_range[1]), 'Y-m-d')
        );
        $cutting_date = $request->cutting_date;

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('jaz_report_downtime_spreading')
                ->whereBetween('created_spreading',[$range['from'],$range['to']])
                ->whereNotNull('category_name')
                ->where('factory_id', \Auth::user()->factory_id)
                ->orderBy('created_spreading', 'asc');

        } else {
            $data = DB::table('jaz_report_downtime_spreading')
                ->whereBetween('created_spreading',[$range['from'],$range['to']])
                ->whereNotNull('category_name')
                ->orderBy('created_spreading', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Downtime_Spreading_'.$range['from'].'-'.$range['to'];

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TGL SPREADING',
                        'OPERATOR',
                        'KATEGORI',
                        'MASALAH',
                        'DOWNTIME (MENIT)',
                        'PROSES',
                        'FACTORY'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->created_spreading,
                                $row->operator,
                                $row->category_name,
                                $row->keterangan,
                                $row->downtime,
                                $row->process,
                                'AOI '.$row->factory_id,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    private function getNameFromNumber($num)
    {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }


    //report serah terima markers
    public function indexSerahTerima()
    {
        return view('report.marker.index');
    }

    public function downloadSerahTerima(Request $request)
    {
        $this->validate($request, [
            'date_range' => 'required',
        ]);

        $date_range = explode('-', preg_replace('/\s+/', '', $request->date_range));
        $range = array(
            'from' => date_format(date_create($date_range[0]), 'Y-m-d'),
            'to'   => date_format(date_create($date_range[1]), 'Y-m-d')
        );
        $from = $range['from'];
        $to   = $range['to'];

        if(\Auth::user()->factory_id == 0 && \Auth::user()->factory->status == 'internal') {
            $data = DB::select(db::raw("SELECT * FROM get_report_serah_terima('$from','$to')"));
        } elseif(\Auth::user()->factory_id > 0 && \Auth::user()->factory->status == 'internal') {
            $factory = \Auth::user()->factory_id;
            $data    = DB::select(db::raw("SELECT * FROM get_report_serah_terima('$from', '$to') where factory_id = '$factory'"));
        } else {
            return response()->json('Data tidak ditemukan.', 422);
        }

        if(count($data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {

            $filename = 'Laporan_Data_Serah_Terima_'.$range['from'].'-'.$range['to'];

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data,$i) {
                $excel->sheet('report', function($sheet) use($data,$i) {
                    $header = array('FACTORY', 'PLAN CUT', 'COMBINE DATE', 'STYLE', 'ARTICLE', 'BARCODE MARKER', 'PART',
                    'LAYER PLAN', 'LAYER AKTUAL', 'LAPORAN BARU', 'RATIO', 'MARKER DIBUAT', 'MARKER DIBUAT OLEH', 'MARKER CANCEL',
                    'MARKER DIUPLOAD', 'MARKER DIUPLOAD OLEH', 'MARKER DITERIMA', 'MARKER DITERIMA OLEH');


                    $sheet->appendRow($header);

                    foreach ($data as $key => $row)
                    {
                        if($row->is_combine == true){
                            $get_combine_plan = CuttingPlan::where('header_id', $row->id)
                                ->whereNull('deleted_at')
                                ->orderBy('cutting_date')
                                ->pluck('cutting_date')
                                ->toArray();
                            $combine_plan = rtrim(implode(', ',$get_combine_plan),', ');
                        } else {
                            $combine_plan = '-';
                        }

                        $data_excel = array(
                            $row->factory_id == '1' ? 'AOI1' : 'AOI2',
                            $row->cutting_date,
                            $combine_plan,
                            $row->style,
                            $row->articleno,
                            $row->barcode_id,
                            $row->part_no,
                            $row->layer_plan,
                            $row->layer,
                            $row->is_new == null ? '-' : $row->is_new,
                            $row->ratio_print,
                            $row->marker_dibuat,
                            $row->nik_dibuat_oleh == null ? '' : $row->nik_dibuat_oleh .' - '.$row->nama_dibuat_oleh,
                            $row->marker_dihapus,
                            $row->marker_diupload,
                            $row->nik_diupload_oleh == null ? '' :$row->nik_diupload_oleh .' - '.$row->nama_diupload_oleh,
                            $row->marker_diterima,
                            $row->nik_diterima_oleh == null ? '' :$row->nik_diterima_oleh .' - '.$row->nama_diterima_oleh,
                        );

                        $sheet->appendRow($data_excel);
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function getSerahTerima(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('poreference', function($data) {
                    $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    return implode(', ', $po_array);
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('action', function($data) {
                    return '<a href="'.route('reportSpreading.downloadSerahTerimaPlan', $data->id).'" target="_blank" class="btn btn-primary btn-icon"><i class="icon-download"></i></a>';
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function downloadSerahTerimaPlan($id)
    {
        $get_plan = CuttingPlan::where('id', $id)
            ->where('deleted_at', null)
            ->first();

        if(\Auth::user()->factory_id == 0 && \Auth::user()->factory->status == 'internal') {
            $data = DB::select(db::raw("SELECT * FROM get_report_serah_terima('$get_plan->cutting_date','$get_plan->cutting_date') where id = '$id'"));
        } elseif(\Auth::user()->factory_id > 0 && \Auth::user()->factory->status == 'internal') {
            $factory = \Auth::user()->factory_id;
            $data    = DB::select(db::raw("SELECT * FROM get_report_serah_terima('$get_plan->cutting_date','$get_plan->cutting_date') where id = '$id' and factory_id = '$factory'"));
        } else {
            return response()->json('Data tidak ditemukan.', 422);
        }

        if(count($data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {

            $filename = 'Laporan_Data_Serah_Terima_'.$get_plan->cutting_date.'-'.$get_plan->style.'-'.$get_plan->articleno;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data,$i) {
                $excel->sheet('report', function($sheet) use($data,$i) {
                    $header = array('FACTORY', 'PLAN CUT', 'COMBINE DATE', 'STYLE', 'ARTICLE', 'BARCODE MARKER', 'PART',
                    'LAYER PLAN', 'LAYER AKTUAL', 'LAPORAN BARU', 'RATIO', 'MARKER DIBUAT', 'MARKER DIBUAT OLEH', 'MARKER CANCEL',
                    'MARKER DIUPLOAD', 'MARKER DIUPLOAD OLEH', 'MARKER DITERIMA', 'MARKER DITERIMA OLEH');


                    $sheet->appendRow($header);

                    foreach ($data as $key => $row)
                    {
                        if($row->is_combine == true){
                            $get_combine_plan = CuttingPlan::where('header_id', $row->id)
                                ->whereNull('deleted_at')
                                ->orderBy('cutting_date')
                                ->pluck('cutting_date')
                                ->toArray();
                            $combine_plan = rtrim(implode(', ',$get_combine_plan),', ');
                        } else {
                            $combine_plan = '-';
                        }

                        $data_excel = array(
                            $row->factory_id == '1' ? 'AOI1' : 'AOI2',
                            $row->cutting_date,
                            $combine_plan,
                            $row->style,
                            $row->articleno,
                            $row->barcode_id,
                            $row->part_no,
                            $row->layer_plan,
                            $row->layer,
                            $row->is_new == null ? '-' : $row->is_new,
                            $row->ratio_print,
                            $row->marker_dibuat,
                            $row->nik_dibuat_oleh == null ? '' : $row->nik_dibuat_oleh .' - '.$row->nama_dibuat_oleh,
                            $row->marker_dihapus,
                            $row->marker_diupload,
                            $row->nik_diupload_oleh == null ? '' :$row->nik_diupload_oleh .' - '.$row->nama_diupload_oleh,
                            $row->marker_diterima,
                            $row->nik_diterima_oleh == null ? '' :$row->nik_diterima_oleh .' - '.$row->nama_diterima_oleh,
                        );

                        $sheet->appendRow($data_excel);
                    }
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }


    public function indexOutput()
    {
        return view('report.spreading.output.index');
    }


    public function downloadOutput(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $factory = \Auth::user()->factory_id;

        $date_range = explode('-', preg_replace('/\s+/', '', $request->cutting_date));

        $from = Carbon::parse($date_range[0]);
        $from->hour(00);
        $from->minute(00);
        $from->second(00);
        $f = $from->format('Y-m-d H:i:s');

        $to = Carbon::parse($date_range[1])->addDays(1);
        $to->hour(00);
        $to->minute(00);
        $to->second(00);
        $t = $to->format('Y-m-d H:i:s');

        if(\Auth::user()->factory_id > 0) {
            $data = DB::table('hs_report_output_spreading')
            ->whereBetween('date',[$f,$t])
            ->where('factory_id', \Auth::user()->factory_id)
            ->orderBy('date', 'asc')
            ->orderBy('factory_id', 'asc')
            ->orderBy('shift', 'asc');
            // ->orderBy('table_name', 'asc');
        } else {
            $data = DB::table('hs_report_output_spreading')
            ->whereBetween('date',[$f,$t])
            ->orderBy('date', 'asc')
            ->orderBy('factory_id', 'asc')
            ->orderBy('shift', 'asc');
            // ->orderBy('table_name', 'asc');
        }

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if($date_range[0] == $date_range[1]) {
                $filename = 'Laporan_Output_Spreading_'.$f;
            } else {
                $filename = 'Laporan_Output_Spreading_'.$f.'-'.$t;
            }

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'TANGGAL',
                        'FACTORY',
                        'MEJA',
                        'SHIFT  ',
                        'SPREADING (YARD)',
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            $data_excel = array(
                                $row->date,
                                ($row->factory_id == '1') ? 'AOI 1' : 'AOI 2',
                                $row->table_name,
                                $row->shift,
                                $row->used,
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    static function weeklyReportQcSpreading($factory_id)
    {
        $location = Config::get('storage.qc');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);

        $start    = Carbon::now()->subDays(7)->format('Y-m-d');
        $end      = Carbon::now()->format('Y-m-d');
        $real_end = Carbon::now()->subDays(1)->format('Y-m-d');

        $data = DB::select(db::raw("SELECT * FROM get_report_qc_spreading('".$start."','".$end."',$factory_id)"));

        $filename = 'Report_QC_Spreading_AOI'.$factory_id.'';
        $i=1;
        return \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    '#',
                    'PLAN CUT',
                    'SCAN ROLL SPREADING',
                    'STYLE',
                    'PO BUYER',
                    'ART',
                    'PART',
                    'ITEM FABRIC',
                    'COLOR FAB',
                    'CUT',
                    'BARCODE MARKER',
                    'SIZE',
                    'RATIO',
                    'QTY LAYER(supplai)',
                    'QTY LAYER (aktual)',
                    'SAMBUNGAN AWAL (yard)',
                    'SAMBUNGAN AKHIR (yard)',
                    'QTY PCS',
                    'SUPPLIER FABRIC',
                    'OPERATOR', 'MEJA',
                    'GROUP LEADER',
                    'QC',
                    'NO ROLL',
                    'LOT',
                    'PANJANG SUPLAI',
                    'PANJANG AKTUAL',
                    'LEBAR SUPLAI',
                    'LEBAR AKTUAL',
                    'POIN FIR',
                    'BAU / ODOR',
                    'PANJANG',
                    'LEBAR',
                    'TINGGI GELARAN',
                    'PANJANG MARKER + ALLOWANCE',
                    'AKTUAL PANJANG',
                    'AKTUAL LEBAR',
                    'PANJANG JEBLOS',
                    'LEBAR JEBLOS',
                    'TIDAK RAPI',
                    'TERLALU KENCANG',
                    'TERLALU KENDUR',
                    'SAMBUNGAN',
                    'TOTAL',
                    'REMARK'
                ));

                foreach ($data as $dt)
                {
                    $get_plan_id = array();
                    if($dt->header_id != null && $dt->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $dt->plan_id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();
                    } else {
                        $get_plan_id[] = $dt->plan_id;
                    }

                    $po = implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                    ->whereNull('deleted_at')
                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                    ->pluck('po_buyer')->toArray());


                    $get_fir_point = DB::connection('wms_live')->table('material_checks')->where('barcode_preparation', $dt->barcode_fabric)->first();

                    $data_excel = array(
                        $i++,
                        $dt->cutting_date,
                        $dt->created_spreading,
                        $dt->style,
                        $po,
                        $dt->articleno,
                        $dt->part_no,
                        $dt->upc,
                        $dt->color_name,
                        $dt->cut,
                        $dt->barcode_marker,
                        $dt->ratio_print,
                        $dt->qty_ratio,
                        $dt->suplai_layer,
                        $dt->actual_layer,
                        $dt->sambungan,
                        $dt->sambungan_end,
                        ($dt->actual_layer * $dt->qty_ratio),
                        '-',
                        isset($dt->operator) ? $dt->operator :'--',
                        $dt->table_name,
                        isset($dt->gl_name) ? $dt->gl_name :'--',
                        isset($dt->qc_name) ? $dt->qc_name :'--',
                        $dt->no_roll,
                        $dt->lot,
                        $dt->qty_fabric,
                        ($dt->actual_use + $dt->remark),
                        $dt->actual_width,
                        $dt->actual_width,
                        $get_fir_point == null ? '' : $get_fir_point->total_yds,
                        $dt->bau,
                        $dt->marker_length,
                        ($dt->fabric_width-0.5),
                        isset($dt->spreading_high_qc) ? $dt->spreading_high_qc :'--',
                        ($dt->marker_length + 0.75),
                        isset($dt->actual_length_qc) ? $dt->actual_length_qc : '--',
                        isset($dt->actual_width_qc) ? $dt->actual_width_qc : '--',
                        $dt->l_jeblos,
                        $dt->w_jeblos,
                        $dt->rapi,
                        $dt->kencang,
                        $dt->kendur,
                        $dt->sambungan_qc,
                        ($dt->l_jeblos+$dt->w_jeblos+$dt->rapi+$dt->kencang+$dt->kendur+$dt->sambungan_qc),
                        isset($dt->remark_qc) ? $dt->remark_qc :'--',
                    );
                    $sheet->appendRow($data_excel);
                }
            });
        })->save('xlsx', $location);

    }

    public function downloadWeekly(Request $request){
        $factory_id  = \Auth::user()->factory_id;

        if ($factory_id == '1') {
            $filename   = "Report_QC_Spreading_AOI1.xlsx";
        }else{
            $filename   = "Report_QC_Spreading_AOI2.xlsx";
        }

        $file       = Config::get('storage.qc') . '/' . $filename;

        if(!file_exists($file)) return 'File not found.';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
    //TAMBAHAN REPORT SPREADING RESUM & DETAIL 14 DESEMBER 2021
    public function downloadSpreadingdaytable(Request $request)
    {
        // $spreading_date = explode('-', preg_replace('/\s+/', '', $request->spreading_date));
        // $from = Carbon::createFromFormat('m/d/Y', $spreading_date[0])->format('Y-m-d');        
        // $to = Carbon::createFromFormat('m/d/Y', $spreading_date[1])->format('Y-m-d');
        $spreading_date = explode('-', preg_replace('/\s+/', '', $request->spreading_date));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $spreading_date[0].' 07:00:00')->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $spreading_date[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');

        $data = DB::table('dd_report_spreading_per_day')
        ->select(DB::raw('created_at,barcode_table,count(barcode_marker) as total_marker,sum(actual_spreading) as actual_spreading,name,factory_id'))
        ->whereBetween('created_at', [$from, $to])
        ->where('factory_id', \Auth::user()->factory_id)
        ->groupBy('created_at','barcode_table','name','factory_id')
        ->orderBy('created_at')
        ->get();
        if($data == null)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Report_Spreading_per_Day_table".$from." sampai ".$to;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','TANGGAL SPREADING');
                        $sheet->setCellValue('C1','MEJA');
                        $sheet->setCellValue('D1','TOTAL MARKER');
                        $sheet->setCellValue('E1','ACTUAL SPREADING (Yds)');
                        $sheet->setCellValue('F1','OPERATOR');
                        $sheet->setCellValue('G1','FACTORY');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->created_at);
                            $sheet->setCellValue('C'.$rw,$vl->barcode_table);
                            $sheet->setCellValue('D'.$rw,$vl->total_marker);
                            $sheet->setCellValue('E'.$rw,$vl->actual_spreading);
                            $sheet->setCellValue('F'.$rw,$vl->name);
                            $sheet->setCellValue('G'.$rw,'AOI '.$vl->factory_id);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }
    public function downloadSpreadingdetailtable(Request $request)
    {
        // $spreading_date = explode('-', preg_replace('/\s+/', '', $request->spreading_date_x));
        // $from = Carbon::createFromFormat('m/d/Y', $spreading_date[0])->format('Y-m-d');        
        // $to = Carbon::createFromFormat('m/d/Y', $spreading_date[1])->format('Y-m-d');
        $scan_date = explode('-', preg_replace('/\s+/', '', $request->spreading_date_x));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[0].' 07:00:00')->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');
        // dd($to);

        $data = DB::table('dd_report_spreading_per_day')
        ->whereBetween('created_at', [$from, $to])
        ->where('factory_id', \Auth::user()->factory_id)
        ->orderBy('created_at','barcode_marker')
        ->get();
        if($data == null)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Report_Spreading_per_Day_table".$from." sampai ".$to;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','TANGGAL SPREADING');
                        $sheet->setCellValue('C1','MEJA');
                        $sheet->setCellValue('D1','MARKER');
                        $sheet->setCellValue('E1','STYLE');
                        $sheet->setCellValue('F1','ACTUAL SPREADING');
                        $sheet->setCellValue('G1','OPERATOR');
                        $sheet->setCellValue('H1','FACTORY');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->created_at);
                            $sheet->setCellValue('C'.$rw,$vl->barcode_table);
                            $sheet->setCellValue('D'.$rw,$vl->barcode_marker);
                            if($vl->set_type == null || $vl->set_type == 'Non'){
                                $sheet->setCellValue('E'.$rw,$vl->style);
                            }else{
                                $sheet->setCellValue('E'.$rw,$vl->style.'-'.$vl->set_type);
                            }
                            $sheet->setCellValue('F'.$rw,$vl->actual_spreading);
                            $sheet->setCellValue('G'.$rw,strtoupper($vl->name));
                            $sheet->setCellValue('H'.$rw,'AOI '.$vl->factory_id);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }
}
