<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.updateDetailProcess') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE DETAIL PROCESS</span> <!-- title -->
                              </legend>
                              
                            <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">DETAIL PROCESS NAME:</label>
                              	<div class="col-lg-9">
									  <textarea class="form-control" name="detail_process_name_update" id="detail_process_name_update" cols="30" rows="10" readonly required></textarea>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">GSD NAME:</label>
                              	<div class="col-lg-9">
									  <textarea class="form-control" name="gsd_name_update" id="gsd_name_update" cols="30" rows="10" required></textarea>
                              	</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Process:</label>
								<div class="col-lg-9">
									<select data-placeholder="Select a state" name="process_id_update" id="process_id_update" class="form-control select-search">
										<option value=""></option>
										@foreach ($process as $item)
											<option value="{{$item->id}}">{{$item->process_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Column Report RnD:</label>
								<div class="col-lg-9">
									<input type="hidden" name="column_report_edit" id="column_report_edit">
									<select class="form-control select2 select-column-name" name="id_column_report_update" id="id_column_report_update">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="form-group hidden" id="dvColumnReport2Update">
								<label class="col-lg-3 control-label text-semibold">Column Report RnD Type 2:</label>
								<div class="col-lg-9">
									<input type="hidden" name="column_report_edit2" id="column_report_edit2">
									<select class="form-control select2 select-column-name2" name="id_column_report2_update" id="id_column_report2_update">
										<option value=""></option>
									</select>
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->