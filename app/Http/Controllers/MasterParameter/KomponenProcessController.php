<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class KomponenProcessController extends Controller
{
    //MASTER KOMPONEN PLROCESS
    public function masterKomponenProcess()
    {
        $process = DB::table('master_process')
                    ->whereNull('deleted_at')
                    ->get();

        $komponen = DB::table('master_komponen')
                    ->whereNull('deleted_at')
                    ->get();

        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        $seasons = DB::table('master_seasons')
                    ->whereNull('deleted_at')
                    ->get();

        $styles = DB::table('master_style')
                    ->select('style')
                    ->groupBy('style')
                    ->whereNull('deleted_at')
                    ->get();

        $column_report = DB::table('master_column_report_rnd')
                            ->get();

        return view('master_parameter.komponen_process.index', compact('process', 'komponen', 'types', 'styles', 'seasons', 'column_report'));
    }

    public function ajaxGetDataMasterKomponenProcess(Request $request)
    {
        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'style') {

                if(empty($request->style)) {
                   $style = 'null';
                }
                else {
                   $style = $request->style;
                }

               $params = DB::table('master_style_detail_ie')
                            ->select('master_style_detail_ie.id', 'master_style_detail_ie.style', 'master_style_detail_ie.season', 'master_seasons.season_name', 'master_style_detail_ie.komponen', 'master_komponen.komponen_name', 'master_style_detail_ie.type_id', 'types.type_name')
                            ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                            ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                            ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                            ->where('style', $style)
                            ->whereNull('master_style_detail_ie.deleted_at')
                            ->whereNull('master_seasons.deleted_at')
                            ->whereNull('master_komponen.deleted_at')
                            ->whereNull('types.deleted_at')
                            ->get();
            }
           //  
            else {
               
            }

            $data = json_decode(json_encode($params), true);

            return Datatables::of($data)
                    ->editColumn('komponen_name', function($data){
                        $komponen_name = trim($data['komponen_name']);

                        return '<a onclick="detailKomponen(this)" data-id="'.$data['id'].'" data-komponen="'.$data['komponen'].'">'.$komponen_name.'</a>';
                    })
                    ->rawColumns(['komponen_name'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                   ->editColumn('style', function($data) {
                       return null;
                   })
                   ->editColumn('season_name', function($data) {
                       return null;
                   })
                   ->editColumn('komponen_name', function($data) {
                       return null;
                   })
                   ->editColumn('type_name', function($data) {
                       return null;
                   })
                   ->rawColumns(['komponen_name'])
                   ->make(true);
        }
    }

    //add new
    public function addKomponenProcess(Request $request)
    {

        $this->validate($request, [
            'id_style_detail' => 'required',
            'process_id' => 'required'
        ]);

        $cekifexist = DB::table('master_style_transactions')
                        ->where('id_style_detail', $request->id_style_detail)
                        ->where('process_id', $request->process_id)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('komponen and process already exists..!', 422);
        }


        $data = array();

        $det['id_style_detail'] = $request->id_style_detail;
        $det['process_id'] = $request->process_id;
        $det['created_at'] = Carbon::now();

        $data[] = $det;

        try {
            DB::begintransaction();

            //insert table master style transactions

            DB::table('master_style_transactions')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    
    //add new detail
    public function addKomponenProcessDetail(Request $request)
    {

        $this->validate($request, [
            'id_style_transactions' => 'required',
            'id_detail_process' => 'required'
        ]);

        $lc = $request->lc ? str_replace(',','.',$request->lc) : null;
        $gasa = $request->gasa ? str_replace(',','.',$request->gasa) : null;
        $total = $request->total ? (int)$request->total : null;

        $id_column_report = $request->id_column_report;
        $remark_rnd = $request->remark_rnd;

        $data = array();

        $det['id_style_transactions'] = $request->id_style_transactions;
        $det['id_detail_process'] = $request->id_detail_process;
        $det['lc'] = $lc;
        $det['gasa'] = $gasa;
        $det['total'] = $total;
        $det['remark_rnd'] = $remark_rnd;
        $det['created_at'] = Carbon::now();

        $data[] = $det;

        try {
            DB::begintransaction();

            $cekifexist = DB::table('master_style_transactions_detail')
                        ->where('id_style_transactions', $request->id_style_transactions)
                        ->where('id_detail_process', $request->id_detail_process)
                        ->whereNull('deleted_at')
                        ->exists();
        
            if ($cekifexist) {
                // return response()->json('komponen and process detail already exists..!', 422);
                //update table master style transactions
                DB::table('master_style_transactions_detail')
                        ->where('id_style_transactions', $request->id_style_transactions)
                        ->where('id_detail_process', $request->id_detail_process)
                        ->whereNull('deleted_at')
                        ->update([
                            'lc' => $lc,
                            'gasa' => $gasa,
                            'total' => $total,
                            'remark_rnd' => $remark_rnd,
                            'updated_at' => Carbon::now()
                        ]);
            }else{

                //insert table master style transactions
                DB::table('master_style_transactions_detail')->insert($data);
            }


            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    // //edit master style
    // public function editStyle(Request $request)
    // {
    //     $style = $request->style;
    //     $komponen = $request->komponen;

    //     $data = DB::table('v_master_style_ie')
    //                 ->where('style', $style)
    //                 ->where('komponen', $komponen)
    //                 ->first();

    //     return response()->json($data, 200);
    // }

    // //update master style
    // public function updateStyle(Request $request)
    // {
    //     $data = $request->all();

    //     try {
    //         DB::beginTransaction();

    //             DB::table('master_style')
    //                 ->where('style', $data['styleUpdate'])
    //                 ->update([
    //                     'updated_at' => Carbon::now()
    //                 ]);
                
    //             DB::table('master_style_detail_ie')
    //                 ->where('style', $data['styleUpdate'])
    //                 ->where('komponen', $data['komponenUpdate'])
    //                 ->update([
    //                     'season' => $data['seasonUpdate'],
    //                     // 'process' => $data['process_id_update'] != 'null' ? $data['process_id_update'] : null,
    //                     'type_id' => $data['type_id_update'],
    //                     'updated_at' => Carbon::now(),
    //                     'user_id' => Auth::user()->id
    //                 ]);

    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollback();
    //         $message = $e->getMessage();
    //         ErrorHandler::db($message);
    //     }

    //     // return view('cutting.master.index');
    // }

    //delete master komponen process
    public function deleteKomponenProcess(Request $request)
    {
        $id = $request->id;

        try {
            DB::beginTransaction();

            DB::table('master_style_transactions')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }
    //delete master komponen process detail
    public function deleteKomponenProcessDetail(Request $request)
    {
        $id = $request->id;
        if ($request->_method == "DELETE") {
            try {
                DB::beginTransaction();
    
                DB::table('master_style_transactions_detail')
                    ->where('id', $id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
    
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }elseif ($request->_method == "UPDATE") {
            $data = DB::table('master_style_transactions')
                        ->select('master_style_transactions_detail.id', 'master_style_transactions.id_style_detail', 'master_style_transactions.process_id', 'master_style_detail_ie.season', 'master_style_detail_ie.komponen', 'master_style_detail_ie.type_id', 'master_process.process_name', 'master_komponen.komponen_name', 'master_seasons.season_name', 'types.type_name', 'master_style_transactions_detail.id_style_transactions', 'master_style_transactions_detail.id_detail_process', 'master_style_transactions_detail.ts', 'master_style_transactions_detail.lc', 'master_style_transactions_detail.gasa', 'master_style_transactions_detail.total', 'master_detail_process.detail_process_name', 'master_detail_process.gsd_name', 'master_style_transactions_detail.id_column_report', 'master_column_report_rnd.column_name', 'master_style_transactions_detail.remark_rnd')
                        ->join('master_style_transactions_detail', 'master_style_transactions_detail.id_style_transactions', '=', 'master_style_transactions.id')
                        ->join('master_detail_process', 'master_detail_process.id', '=', 'master_style_transactions_detail.id_detail_process')
                        ->join('master_style_detail_ie', 'master_style_detail_ie.id', '=', 'master_style_transactions.id_style_detail')
                        ->join('master_process', 'master_process.id', 'master_style_transactions.process_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                        ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                        ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                        ->leftjoin('master_column_report_rnd', 'master_column_report_rnd.id', '=', 'master_style_transactions_detail.id_column_report')
                        ->where('master_style_transactions_detail.id', $id)
                        ->whereNull('master_style_transactions.deleted_at')
                        ->whereNull('master_style_transactions_detail.deleted_at')
                        ->whereNull('master_detail_process.deleted_at')
                        ->whereNull('master_style_detail_ie.deleted_at')
                        ->whereNull('master_process.deleted_at')
                        ->whereNull('master_komponen.deleted_at')
                        ->whereNull('master_seasons.deleted_at')
                        ->whereNull('types.deleted_at')
                        ->first();
        
            return response()->json($data);
        }

    }

    //END OF MASTER KOMPONEN 
    
    public function ajaxGetDataKomponenProcess(Request $request)
    {
        $id_style_detail = $request->id;

        $data['transactions'] = DB::table('master_style_transactions')
                                ->select('master_style_transactions.id', 'master_style_transactions.id_style_detail', 'master_style_transactions.process_id', 'master_style_detail_ie.season', 'master_style_detail_ie.komponen', 'master_style_detail_ie.type_id', 'master_process.process_name', 'master_komponen.komponen_name', 'master_seasons.season_name', 'types.type_name')
                                ->join('master_style_detail_ie', 'master_style_detail_ie.id', '=', 'master_style_transactions.id_style_detail')
                                ->join('master_process', 'master_process.id', 'master_style_transactions.process_id')
                                ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                                ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                                ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                                ->where('master_style_transactions.id_style_detail', $id_style_detail)
                                ->whereNull('master_style_transactions.deleted_at')
                                ->whereNull('master_style_detail_ie.deleted_at')
                                ->whereNull('master_process.deleted_at')
                                ->whereNull('master_komponen.deleted_at')
                                ->whereNull('master_seasons.deleted_at')
                                ->whereNull('types.deleted_at')
                                ->get();

        $data['details'] = DB::table('master_style_detail_ie')
                                ->select('master_style_detail_ie.id', 'master_style_detail_ie.style', 'master_style_detail_ie.season', 'master_seasons.season_name', 'master_style_detail_ie.komponen', 'master_komponen.komponen_name', 'master_style_detail_ie.type_id', 'types.type_name')
                                ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                                ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                                ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                                ->where('master_style_detail_ie.id', $id_style_detail)
                                ->whereNull('master_style_detail_ie.deleted_at')
                                ->whereNull('master_seasons.deleted_at')
                                ->whereNull('master_komponen.deleted_at')
                                ->whereNull('types.deleted_at')
                                ->first();

            
        $data['process'] = DB::table('master_process')
                            ->whereNull('deleted_at')
                            ->get();

        $data['komponen'] = DB::table('master_komponen')
                            ->whereNull('deleted_at')
                            ->get();

        $data['types'] = DB::table('types')
                        ->whereNull('deleted_at')
                        ->get();

        $data['seasons'] = DB::table('master_seasons')
                    ->whereNull('deleted_at')
                    ->get();

        $data['column_report'] = DB::table('master_column_report_rnd')
                                    ->get();

        // return view('master_parameter.komponen_process.list_process', compact('data', 'details', 'process', 'komponen', 'types', 'seasons'));
        return response()->json($data);
    }

    public function ajaxGetDataKomponenProcessJson(Request $request)
    {
        if ($request->id != null) {

            $id_style_detail = $request->id;
    
            $params = DB::table('master_style_transactions')
                        ->select('master_style_transactions.id', 'master_style_transactions.id_style_detail', 'master_style_transactions.process_id', 'master_style_detail_ie.style','master_style_detail_ie.season', 'master_style_detail_ie.komponen', 'master_style_detail_ie.type_id', 'master_process.process_name', 'master_komponen.komponen_name', 'master_seasons.season_name', 'types.type_name')
                        ->join('master_style_detail_ie', 'master_style_detail_ie.id', '=', 'master_style_transactions.id_style_detail')
                        ->join('master_process', 'master_process.id', 'master_style_transactions.process_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                        ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                        ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                        ->where('master_style_transactions.id_style_detail', $id_style_detail)
                        ->whereNull('master_style_transactions.deleted_at')
                        ->whereNull('master_style_detail_ie.deleted_at')
                        ->whereNull('master_process.deleted_at')
                        ->whereNull('master_komponen.deleted_at')
                        ->whereNull('master_seasons.deleted_at')
                        ->whereNull('types.deleted_at')
                        ->get();
    
            $data = json_decode(json_encode($params), true);
    
            return Datatables::of($data)
                    ->editColumn('process_name', function($data){
                        $process_name = trim($data['process_name']);
    
                        return '<a onclick="detailKomponenProcess(this)" data-id="'.$data['id'].'" data-komponen="'.$data['komponen_name'].'" data-process="'.$data['process_name'].'" data-processid="'.$data['process_id'].'" data-style="'.$data['style'].'" >'.$process_name.'</a>';
                    })
                    ->addColumn('action', function($data){
    
                        return '<a class="btn btn-danger" onclick="deleteKomponenProcess(this)" data-id="'.$data['id'].'" data-process="'.$data['process_name'].'">Delete</a>';
                    })
                    ->rawColumns(['action', 'process_name'])
                    ->make(true);
        }else{
            $data = array();
            return Datatables::of($data)
                    ->editColumn('style', function($data){
                        return null;
                    })
                    ->editColumn('komponen_name', function($data){
                        return null;
                    })
                    ->editColumn('process_name', function($data){
                        return null;
                    })
                    ->addColumn('action', function($data){
    
                        return null;
                    })
                    ->rawColumns(['action', 'process_name'])
                    ->make(true);
        }
            
    }
    
    
    public function ajaxGetDataKomponenProcessDetail(Request $request)
    {
        $data['id_style_transactions'] = $request->id;

        $data['transactions'] = DB::table('master_style_transactions')
                                ->select('master_style_transactions.id', 'master_style_transactions.id_style_detail', 'master_style_transactions.process_id', 'master_style_detail_ie.season', 'master_style_detail_ie.komponen', 'master_style_detail_ie.type_id', 'master_process.process_name', 'master_komponen.komponen_name', 'master_seasons.season_name', 'types.type_name', 'master_style_transactions_detail.id_style_transactions', 'master_style_transactions_detail.id_detail_process', 'master_style_transactions_detail.ts', 'master_detail_process.detail_process_name', 'master_detail_process.gsd_name')
                                ->join('master_style_transactions_detail', 'master_style_transactions_detail.id_style_transactions', '=', 'master_style_transactions.id')
                                ->join('master_detail_process', 'master_detail_process.id', '=', 'master_style_transactions_detail.id_detail_process')
                                ->join('master_style_detail_ie', 'master_style_detail_ie.id', '=', 'master_style_transactions.id_style_detail')
                                ->join('master_process', 'master_process.id', 'master_style_transactions.process_id')
                                ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                                ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                                ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                                ->where('master_style_transactions_detail.id_style_transactions', $request->id)
                                ->whereNull('master_style_transactions.deleted_at')
                                ->whereNull('master_style_transactions_detail.deleted_at')
                                ->whereNull('master_detail_process.deleted_at')
                                ->whereNull('master_style_detail_ie.deleted_at')
                                ->whereNull('master_process.deleted_at')
                                ->whereNull('master_komponen.deleted_at')
                                ->whereNull('master_seasons.deleted_at')
                                ->whereNull('types.deleted_at')
                                ->get();

        $data['process_detail'] = DB::table('master_detail_process')
                                ->where('process_id', $request->process_id)
                                ->whereNull('deleted_at')
                                ->get();

        if ($request->process_id == \Config::get('constants.PRINTING_TYPE')) {
       
            $data['column_reports'] = DB::table('master_column_report_rnd')
                                    ->where('process_id', $request->process_id)
                                    ->where('type_report', 1)
                                    ->get();
    
            $data['column_reports2'] = DB::table('master_column_report_rnd')
                                    ->where('process_id', $request->process_id)
                                    ->where('type_report', 2)
                                    ->get();

            $data['is_printing'] = true;
        }else{

            $data['column_reports'] = DB::table('master_column_report_rnd')
                                    ->where('process_id', $request->process_id)
                                    ->get();
    
            $data['column_reports2'] = DB::table('master_column_report_rnd')
                                    ->where('process_id', $request->process_id)
                                    ->get();

            $data['is_printing'] = false;
        }

        if ($request->process_id == \Config::get('constants.HE_TYPE')) {
            $data['is_he'] = true;
        }else{
            $data['is_he'] = false;
        }

        $data['remark_rnd'] = \Config::get('constants.remark_rnd');

        // return view('master_parameter.komponen_process.list_process_detail', compact('data', 'process_detail', 'id_style_transactions'));
        return response()->json($data, 200);
    }

    public function ajaxGetDataKomponenProcessDetailJson(Request $request)
    {
        if ($request->id != null) {

            $id_style_transactions = $request->id;
    
            $params = DB::table('master_style_transactions')
                        ->select('master_style_transactions_detail.id', 'master_style_transactions.id_style_detail', 'master_style_transactions.process_id', 'master_style_detail_ie.season', 'master_style_detail_ie.komponen', 'master_style_detail_ie.type_id', 'master_process.process_name', 'master_komponen.komponen_name', 'master_seasons.season_name', 'types.type_name', 'master_style_transactions_detail.id_style_transactions', 'master_style_transactions_detail.id_detail_process', 'master_style_transactions_detail.ts', 'master_style_transactions_detail.lc', 'master_style_transactions_detail.gasa', 'master_style_transactions_detail.total', 'master_detail_process.detail_process_name', 'master_detail_process.gsd_name', 'master_style_transactions_detail.id_column_report', 'master_column_report_rnd.column_name', 'master_style_transactions_detail.remark_rnd')
                        ->join('master_style_transactions_detail', 'master_style_transactions_detail.id_style_transactions', '=', 'master_style_transactions.id')
                        ->join('master_detail_process', 'master_detail_process.id', '=', 'master_style_transactions_detail.id_detail_process')
                        ->join('master_style_detail_ie', 'master_style_detail_ie.id', '=', 'master_style_transactions.id_style_detail')
                        ->join('master_process', 'master_process.id', 'master_style_transactions.process_id')
                        ->join('master_komponen', 'master_komponen.id', '=', 'master_style_detail_ie.komponen')
                        ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail_ie.season')
                        ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                        ->leftjoin('master_column_report_rnd', 'master_column_report_rnd.id', '=', 'master_style_transactions_detail.id_column_report')
                        ->where('master_style_transactions_detail.id_style_transactions', $id_style_transactions)
                        ->whereNull('master_style_transactions.deleted_at')
                        ->whereNull('master_style_transactions_detail.deleted_at')
                        ->whereNull('master_detail_process.deleted_at')
                        ->whereNull('master_style_detail_ie.deleted_at')
                        ->whereNull('master_process.deleted_at')
                        ->whereNull('master_komponen.deleted_at')
                        ->whereNull('master_seasons.deleted_at')
                        ->whereNull('types.deleted_at')
                        ->get();
        
                $data = json_decode(json_encode($params), true);
        
                return Datatables::of($data)
                        ->editColumn('detail_process_name', function($data){
                            $str = '<a onclick="editKomponenProcessDetail(this)" data-id="'.$data['id'].'" data-processdetail="'.$data['detail_process_name'].'">'.$data['detail_process_name'].'</a>';
    
                            return $str;
                        })
                        ->editColumn('lc', function($data){
                            $str = $data['lc'] ? str_replace('.',',',$data['lc']) : null;
    
                            return $str;
                        })
                        ->editColumn('gasa', function($data){
                            $str = $data['gasa'] ? str_replace('.',',',$data['gasa']) : null;
    
                            return $str;
                        })
                        ->addColumn('action', function($data){
    
                            return '<a class="btn btn-danger" onclick="deleteKomponenProcessDetail(this)" data-id="'.$data['id'].'" data-processdetail="'.$data['detail_process_name'].'">Delete</a>';
                        })
                        ->rawColumns(['action', 'detail_process_name'])
                        ->make(true);
        }else {
            $data = array();
            return Datatables::of($data)
                        ->editColumn('komponen_name', function($data){
    
                            return null;
                        })
                        ->editColumn('process_name', function($data){
    
                            return null;
                        })
                        ->editColumn('detail_process_name', function($data){
    
                            return null;
                        })
                        ->editColumn('lc', function($data){
    
                            return null;
                        })
                        ->editColumn('gasa', function($data){
    
                            return null;
                        })
                        ->editColumn('total', function($data){
    
                            return null;
                        })
                        ->addColumn('action', function($data){
    
                            return null;
                        })
                        ->rawColumns(['action', 'detail_process_name'])
                        ->make(true);
        }    
    }

    // get json style
    public function ajaxGetDataStyle(Request $request)
    {
        $data = DB::table('master_style')
                    ->select(DB::raw('style as id, style as text'))
                    ->groupBy('style')
                    ->where('style', 'like', '%'.$request->term.'%')
                    ->whereNull('deleted_at')
                    ->get();

        return response()->json($data, 200);
    }

    // listing
    public function listingKomponenProcess(Request $request)
    {
        $data = DB::table('master_detail_process')
                    ->where('id', $request->id)
                    ->whereNull('deleted_at')
                    ->first();

        return response()->json($data, 200);

    }

}
