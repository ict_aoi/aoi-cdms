<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.updateDefectName') }}" method="post" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
				<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
						<div class="panel-body loader-area">
							<fieldset>
								<legend class="text-semibold">
									<i class="icon-file-text2 position-left"></i>
									<span id="title"> UPDATE DEFECT NAME</span> <!-- title -->
									</legend>
									<input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>
									<div class="form-group">
										<label class="col-lg-4 control-label text-semibold">DEFECT NAME (ID):</label>
										<div class="col-lg-8">
											<input type="text" class="form-control text-uppercase" name="defect_name_id_update" id="defect_name_id_update" placeholder="Name" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label text-semibold">DEFECT NAME (EN):</label>
										<div class="col-lg-8">
											<input type="text" class="form-control text-uppercase" name="defect_name_en_update" id="defect_name_en_update" placeholder="Name" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label text-semibold">LOCATOR:</label>
										<div class="col-lg-8" style="background-color:#abf6f7">
											<select multiple data-placeholder="Select a locator" class="form-control select-search" name="locator_update[]" id="locator_update" required style="padding-bottom:4px">
												<option value=""></option>
												@foreach($locator as $x)
													<option value="{{ $x->id }}">{{ $x->process_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="row text-center">
											<div class="col-md-4">
												<label class="col-sm-7 control-label text-semibold">Is Repair</label>
												<div class="col-sm-5" style="padding-top:9px;padding-bottom:9px">
													<input type="checkbox" name="is_repair_update" id="is_repair_update">
												</div>
											</div>
											<div class="col-md-4">
												<label class="col-sm-7 control-label text-semibold">Is Reject</label>
												<div class="col-sm-5" style="padding-top:9px;padding-bottom:9px">
													<input type="checkbox" name="is_reject_update" id="is_reject_update">
												</div>
											</div>
										</div>
									</div>
								
								<div class="form-group text-center">
									<button type="submit" class="btn btn-success">UPDATE <i class="icon-floppy-disk position-right"></i></button>
									<button type="button" class="btn btn-default" data-dismiss="modal">CLOSE <i class="icon-reload-alt position-right"></i></button>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
<!-- /MODAL EDIT -->