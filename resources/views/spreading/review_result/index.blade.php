@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-css')
<style>
    .set-width {
        width: 50px;
    }
    .set-hidden {
        display: none;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Review Hasil Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Review Hasil Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-body">
        <form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
                    <label class="control-label col-lg-2 text-bold">Tanggal Aktual Cutting</label>
					<div class="col-lg-10">
                        <div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control" name="cutting_date" placeholder="Masukkan Tanggal Aktual Cutting" id="cutting_date">
							<span class="input-group-btn">
                                <button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
        <button type="button" class="btn btn-success pull-right" id="btn_download_data">Download</button>
	</div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="markerDataTable">
                <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">Plan</th>
                        <th rowspan="2">Style</th>
                        <th rowspan="2">Article</th>
                        <th rowspan="2">Marker ID</th>
                        <th rowspan="2">Part No</th>
                        <th rowspan="2">Cut</th>
                        <th rowspan="2">Pemakaian Fabric</th>
                        <th colspan="2"><center>Approval</center></th>
                        <th rowspan="2">Table</th>
                        <th rowspan="2">Operator</th>
                        <th rowspan="2">View</th>
                    </tr>
                    <tr>
                        <th>GL</th>
                        <th>QC</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('reviewSpreadingResult.downloadGenerate') }}" id="generateDownloadLink"></a>
@endsection

@section('page-modal')
	@include('spreading.review_result._result_modal')
    @include('spreading.review_result._revisi_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/review_spreading_result.js') }}"></script>
@endsection

