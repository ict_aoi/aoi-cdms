@extends('layouts.app', ['active' => 'report_fabric_prepare'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Preparation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li><a href="{{ route('reportFabricPrepare.index') }}">Daily Material Preparation</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Po Supllier <b>{{ $material_preparation->document_no }}</b></p>
				<p>Supplier Name <b>{{ $material_preparation->supplier_name }}</b></p>
				<p>Warehouse Preparation <b>{{ ($material_preparation->warehouse_id == '1000001' ? 'Warehouse fabric AOI 1' : 'Warehouse fabric AOI 2') }}</b></p>
				<p>Planning Date <b>{{ ($material_preparation->planning_date) ? $material_preparation->planning_date : $material_preparation->planning_date }}</b></p>
				<p>Article No <b>{{ $material_preparation->article_no }}</b></p>
				<p>Style <b>{{ $material_preparation->_style }}</b></p>
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12">
				<p>Piping : @if($material_preparation->is_piping) <span class="label label-info">Piping</span>
									@else <span class="label label-default">Non Piping</span>
									@endif</p>
				<p>Item Code :  <b>{{ $material_preparation->item_code }}</b></p>
				<p>Item Code Source :  <b>{{ $material_preparation->item_code_source }}</b></p>
				<p>Total Preparation :  <b>{{ number_format($material_preparation->total_reserved_qty, 4, '.', ',') }} ({{ $material_preparation->uom }})</b></p>
				<p>Total Relax :  <b><span id='text_total_qty_rilex'>{{ number_format($material_preparation->total_qty_rilex, 4, '.', ',') }} </span>({{ $material_preparation->uom }})</b></p>

			</div>
		</div>

		{{-- {!!
			Form::open(array(
				'class' 	=> 'heading-form',
				'role' 		=> 'form',
				'url' 		=> route('fabricReportDailyMaterialPreparation.exportDetail',$material_preparation->id),
				'method' 	=> 'get',
				'target' 	=> '_blank'
			))
		!!}
		<button type="submit" class="btn btn-default col-xs-12">Export Detail All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!} --}}
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Move Preparation   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_detail_material_preparation_table">
					<thead>
						<tr>
							<th>ID</th>
							{{-- <th>#</th> --}}
							<th>Preparation Date</th>
							<th>Preparation By</th>
							<th>Movement Out Date</th>
							<th>Barcode</th>
							<th>No Roll</th>
							<th>Batch Number</th>
							<th>Begin Width</th>
							<th>Middle Width</th>
							<th>End Width</th>
							<th>Actual Width</th>
							<th>Actual Length</th>
							<th>Actual Lot</th>
							<th>Qty Prepare (In Yard)</th>
							{{-- <th>Action</th> --}}
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th></th>
							{{-- <th></th> --}}
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							{{-- <th></th> --}}
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('wms_version',$wms_version, array('id' => 'wms_version')) !!}
    {!! Form::hidden('material_preparation','[]', array('id' => 'material_preparation')) !!}
    {!! Form::hidden('page','detail', array('id' => 'page')) !!}
	{!! Form::hidden('url_fabric_report_daily_material_preparation_detail',route('reportFabricPrepare.dataDetailPrepare',[$material_preparation->id,$wms_version]), array('id' => 'url_fabric_report_daily_material_preparation_detail')) !!}



@endsection

@section('page-js')
	<script>
        $(function()
        {
            material_preparation 				= JSON.parse($('#material_preparation').val());
			var wms_version				= $('#wms_version').val();

            var url_fabric_report_daily_material_preparation_detail = $('#url_fabric_report_daily_material_preparation_detail').val();
            $('#daily_detail_material_preparation_table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                ordering: false,
                serverSide: true,
                pageLength:-1,
                ajax: {
                    type: 'GET',
					data:{
						wms_version:wms_version	
					},
                    url: url_fabric_report_daily_material_preparation_detail
                },
                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // computing column Total of the complete result
                    var total = api
                        .column( 13 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column( 12 ).footer() ).html('Total');
                    $( api.column( 13 ).footer() ).html(total.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                },
                columns: [
                    {data: 'id', name: 'id',visible:false},//0
                    {data: 'created_at', name: 'created_at',searchable:false},//1
                    {data: 'name', name: 'name',searchable:false},//2
                    {data: 'movement_out_date', name: 'movement_out_date',searchable:false},//3
                    {data: 'barcode', name: 'barcode'},//4
                    {data: 'nomor_roll', name: 'nomor_roll'},//5
                    {data: 'batch_number', name: 'batch_number',orderable:false,searchable:false},//6
                    {data: 'begin_width', name: 'begin_width',orderable:false,searchable:false},//7
                    {data: 'middle_width', name: 'middle_width',orderable:false,searchable:false},//8
                    {data: 'end_width', name: 'end_width',orderable:false,searchable:false},//9
                    {data: 'actual_width', name: 'actual_width',orderable:false,searchable:false},//10
                    {data: 'actual_length', name: 'actual_length',orderable:false,searchable:false},//11
                    {data: 'load_actual', name: 'load_actual',orderable:false,searchable:false},//12
                    {data: 'reserved_qty', name: 'reserved_qty',orderable:false,searchable:false},
                ]
            });

            var dtabl = $('#daily_detail_material_preparation_table').dataTable().api();
            $(".dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtabl.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtabl.search("").draw();
                    }
                    return;
            });
			dtabl.on('preDraw',function(){
            Pace.start();
				$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				})
			.on('draw.dt',function(){
				Pace.stop();
				$.unblockUI();
			});
			dtabl.draw();

    });
    </script>
@endsection
