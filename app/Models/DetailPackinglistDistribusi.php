<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DetailPackinglistDistribusi extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_packinglist_distribusi';
	protected $fillable = ['id_packinglist','no_polybag','bundle_id','qty','user_id','factory_id','created_at','updated_at','deleted_at','style','article','poreference','komponen_name','color_name','set_type'];

	protected $dates = ['created_at','updated_at'];
}