<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class KomponenController extends Controller
{
    //MASTER KOMPONEN
    public function masterKomponen()
    {
        $panel_size = \Config::get('constants.panel_size');

        return view('master_parameter.komponen.index', compact('panel_size'));
    }

    public function ajaxGetDataMasterKomponen(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_komponen')
                ->wherenull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();
            //datatables

            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editKomponen', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteKomponen', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new master komponen
    public function addKomponen(Request $request)
    {

        $this->validate($request, [
            'komponen_name' => 'required'
        ]);

        $komponen_name = trim(strtoupper($request->komponen_name));

        $data = array([
            'komponen_name' => $komponen_name,
            'komponen_size' => $request->komponen_size,
            'total' => $request->total,
            'created_at' => Carbon::now()
        ]);

        // cek komponen exists
        $cekifexist = DB::table('master_komponen')
                        ->where('komponen_name', $komponen_name)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master komponen already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table
            DB::table('master_komponen')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master komponen
    public function editKomponen($id)
    {
        $master_komponen = DB::table('master_komponen')
                        ->where('id', $id)
                        ->first();

        return response()->json($master_komponen, 200);
    }

    //update master komponen
    public function updateKomponen(Request $request)
    {
        $data = $request->all();

        $komponen_name = trim(strtoupper($data['komponen_name_update']));

        // cek exists
        // $cekifexist = DB::table('master_komponen')
        //                 ->where('komponen_name', $komponen_name)
        //                 ->whereNull('deleted_at')
        //                 ->exists();
        
        // if ($cekifexist) {
        //     return response()->json('Master komponen already exists..!', 422);
        // }
       try {
            DB::beginTransaction();

                $update = DB::table('master_komponen')
                            ->where('id', $data['id_update'])
                            ->update([
                                'komponen_name' => $komponen_name,
                                'komponen_size' => $data['komponen_size_update'],
                                'total' => $data['totalUpdate'],
                                'updated_at' => Carbon::now()
                            ]);

                // cek double
                if ($update) {
                    // cek komponen
                    $cekcount = DB::table('master_komponen')
                                    ->where('komponen_name', $komponen_name)
                                    ->whereNull('deleted_at')
                                    ->count();

                    if ($cekcount > 1) {
                            return response()->json('error duplikat komponen..!', 422);
                            // throw new \Exception("error duplikat komponen..!");
                    }
                }

            DB::commit();
       } catch (Exception $e) {
           DB::rollback();
           $message = $e->getMessage();
           ErrorHandler::db($message);
       } 

        // return view('master_parameter.komponen.index');

    }

    //delete master komponen
    public function deleteKomponen(Request $request)
    {
        $id = $request->id;
        DB::table('master_komponen')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('master_parameter.komponen.index');
    }

    //END OF MASTER KOMPONEN
}
