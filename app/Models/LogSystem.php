<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class LogSystem extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'log_system';
	protected $fillable = ['menu','user_id','desc','factory_id','deleted_at','type', 'created_at'];
}
