<?php

namespace App\Http\Controllers\Qccheck;

use DB;
use StdClass;
use Carbon\Carbon;
use App\Models\ScanCutting;
use Illuminate\Http\Request;
use App\Models\Spreading\FabricUsed;
use App\Http\Controllers\Controller;

class InfoFIRController extends Controller
{
    private $wms_connection;

    public function __construct()
    {
        $this->wms_connection = DB::connection('wms_live');
    }

    public function index()
    {
        $barcodes = ScanCutting::where('state', 'done')->whereNull('deleted_at')->get();
        return view('qccheck.info_fir.index', compact('barcodes'));
    }

    public function indexQR($id)
    {
        $data_id = $id;
        return view('qccheck.qr.info_fir.index', compact('data_id'));
    }

    public function getData(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_marker = $request->barcode_marker;

            if($barcode_marker != null) {
                $data = FabricUsed::where('barcode_marker', $barcode_marker)->orderBy('akumulasi_layer', 'asc');
                return datatables()->of($data)
                ->addColumn('action', function($data) {
                    $get_status = DB::connection('wms_live')->table('material_checks')->where('barcode_preparation', $data->barcode_fabric)->first();
                    if($get_status == null) {
                        return view('qccheck.info_fir._action', [
                            'model' => $data
                        ]);
                    } else {
                        return view('qccheck.info_fir._action', [
                            'model' => $data,
                            'detail'  => route('infoFIR.detail', $data->barcode_fabric)
                        ]);
                    }
                })
                ->editColumn('qty_fabric', function($data) {
                    return round($data->qty_fabric, 2);
                })
                ->addColumn('fabric_inspect', function($data) {
                    $get_status = DB::connection('wms_live')->table('material_checks')->where('barcode_preparation', $data->barcode_fabric)->first();
                    if($get_status != null) {
                        return '<span class="label bg-success">Yes</span>';
                    } else {
                        return '<span class="label bg-danger">No</span>';
                    }
                })
                ->rawColumns(['action','fabric_inspect'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detail($id)
    {
        $data = $this->wms_connection->table('material_checks')->where('barcode_preparation', $id)->first();
        if($data != null) {
            $obj = new StdClass();
            $obj->date_of = Carbon::parse($data->created_at)->format('d/M/Y H:i:s');
            $obj->nomor_roll = $data->nomor_roll;
            $obj->batch_number = $data->batch_number;
            $obj->actual_lot = null;
            $obj->kg = $data->kg;
            $obj->sticker_yard = $data->qty_on_barcode;
            $obj->width_on_barcode = $data->actual_width;
            $obj->yard_actual = $data->actual_length;
            $obj->yard_diff = $data->different_yard;
            $obj->cuttable_width = $data->actual_width;
            $obj->defect_a = $data->jumlah_defect_a;
            $obj->defect_b = $data->jumlah_defect_b;
            $obj->defect_c = $data->jumlah_defect_c;
            $obj->defect_d = $data->jumlah_defect_d;
            $obj->defect_e = $data->jumlah_defect_e;
            $obj->defect_f = $data->jumlah_defect_f;
            $obj->defect_g = $data->jumlah_defect_g;
            $obj->defect_h = $data->jumlah_defect_h;
            $obj->defect_i = $data->jumlah_defect_i;
            $obj->defect_j = $data->jumlah_defect_j;
            $obj->defect_k = $data->jumlah_defect_k;
            $obj->defect_l = $data->jumlah_defect_l;
            $obj->defect_m = $data->jumlah_defect_m;
            $obj->defect_n = $data->jumlah_defect_n;
            $obj->defect_o = $data->jumlah_defect_o;
            $obj->defect_p = $data->jumlah_defect_p;
            $obj->defect_q = $data->jumlah_defect_q;
            $obj->defect_r = $data->jumlah_defect_r;
            $obj->defect_s = $data->jumlah_defect_s;
            $obj->defect_t = $data->jumlah_defect_t;
            $obj->linier_point = $data->total_linear_point;
            $obj->sq = $data->total_yds;
            $obj->formula_1 = $data->total_formula_1;
            $obj->formula_2 = $data->total_formula_2;
            $obj->created_by = $data->user_id;
            $obj->comfirm = $data->confirm_user_id.' '.$data->confirm_date;
            $obj->final_result = $data->final_result;
            $obj->note = $data->note;
            return response()->json($obj,200);
        } else {
            return response()->json('Data tidak ada!',422);
        }
    }
}
