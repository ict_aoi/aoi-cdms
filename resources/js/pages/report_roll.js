$(document).ready(function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/report-roll/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: true, orderable: true, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po', name: 'po',searchable:true},
            {data: 'size_category', name: 'size_category'},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/report-roll/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#jumlah_pengurangan').keyup(function(){
        var pengurangan = $('#jumlah_pengurangan').val();
        var qty = $('#qty_reduce_view').text();
        var sisa_pengurangan = qty - pengurangan;
        $("#sisa_pengurangan").val(sisa_pengurangan);
    });

    $('#partModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        //$('#file_marker').val(null);
    });

    $('#btn_reduce').on('click', function() {
        var barcode = $('#barcode_reduce_view').text();
        var jumlah_pengurangan = $('#jumlah_pengurangan').val();
        var sisa_pengurangan = $('#sisa_pengurangan').val();
        var qty_fabric = $('#qty_reduce_view').text();

        if(!jumlah_pengurangan){
            $("#alert_warning").trigger("click", 'jumlah pengurangan masih belum diisi!');
            return false;
        }

        if(jumlah_pengurangan <= 0){
            $("#alert_warning").trigger("click", 'jumlah pengurangan harus lebih dari 0!');
            return false;
        }

        if(sisa_pengurangan < 0){
            $("#alert_warning").trigger("click", 'sisa pengurangan tidak boleh kurang dari 0!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : '/report-roll/reduce-pengurangan',
            data: {
                barcode: barcode,
                jumlah_pengurangan: jumlah_pengurangan,
                sisa_pengurangan: sisa_pengurangan,
                qty_fabric: qty_fabric,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $("#alert_success").trigger("click", response);
                $('#reduceModal').modal('hide');
                detailModal($('#url_detail').val());
                $('#jumlah_pengurangan').val('');
                $('#sisa_pengurangan').val('');
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    })

    $("#detailTable").on("click", ".cancelReceived", function() {
        event.preventDefault();
        var id = $(this).data('id');
        if(id == 'kosong') {
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure to cancel this Roll ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/report-roll/cancel-received/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "method" : "DELETE",
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        $.unblockUI();
                        myalert('success','Deleted Successfully');
                        get_data_modal_table();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if(response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });

    $('#reduceModal').on('hidden.bs.modal', function(){
        detailModal($('#url_detail').val());
        $('#jumlah_pengurangan').val('');
        $('#sisa_pengurangan').val('');
    });

    $('#btn_accepted').on('click', function() {
        var data_barcode = Array();
        var cutting_date = $('#cutting_date_detail').val();
        var style = $('#style_detail').val();
        var articleno = $('#articleno_detail').val();
        var po_buyer = $('#po_buyer_detail').val();

        $('#detailTable tbody tr').each(function(i, v) {
            data_barcode[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('#accepted_check').length > 0) {
                    if($(this).find('input').is(":checked") == true) {
                        data_barcode[i][ii] = 1;
                    } else {
                        data_barcode[i][ii] = 0;
                    }
                } else {
                    data_barcode[i][ii] = $(this).text();
                }
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : '/report-roll/accept-rolls',
            data: {
                data_barcode: data_barcode,
                cutting_date: cutting_date,
                style: style,
                articleno: articleno,
                po_buyer: po_buyer,
            },
            beforeSend: function() {
                // $('#planningTable').block({
                //     message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                //     overlayCSS: {
                //         backgroundColor: '#fff',
                //         opacity: 0.8,
                //         cursor: 'wait'
                //     },
                //     css: {
                //         border: 0,
                //         padding: '10px 15px',
                //         color: '#fff',
                //         width: 'auto',
                //         '-webkit-border-radius': 2,
                //         '-moz-border-radius': 2,
                //         backgroundColor: '#333'
                //     }
                // });
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    },
                    theme:true,
                    baseZ:2000
                });
            },
            success: function(response) {
                // $('#planningTable').unblock();
                $.unblockUI();
                $('#cutting_date_detail').val(response.cutting_date);
                $('#style_detail').val(response.style);
                $('#articleno_detail').val(response.articleno);
                $('#po_buyer_detail').val(JSON.stringify(response.po_buyer));
                $('#cutting_date_detail_view').text(response.cutting_date);
                $('#style_detail_view').text(response.style);
                $('#articleno_detail_view').text(response.articleno);
                $('#po_buyer_detail_view').text(response.po_buyer.join(', '));
                $('#detailModal').modal();
                get_data_modal_table();
                $("#alert_success").trigger("click", 'Fabrik Berhasil Diterima!');
            },
            error: function(response) {
                // $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        // var planning_id_detail = $('#planning_id_detail').val();
        // var part_no_detail = $('#part_no_detail').val();
        // removePageAccess(planning_id_detail, part_no_detail);
        // $('#form_filter_date').submit();
        $('#partModal').modal();
        get_part_modal_table()
    });

});


function partModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#url_part').val(response.url);
            $('#state_id').val(response.id);
            $('#cutting_date_part').val(response.cutting_date);
            $('#style_part').val(response.style);
            $('#articleno_part').val(response.articleno);
            $('#po_buyer_part').val(JSON.stringify(response.po_buyer));
            $('#cutting_date_part_view').text(response.cutting_date);
            $('#style_part_view').text(response.style);
            $('#articleno_part_view').text(response.articleno);
            $('#po_buyer_part_view').text(response.po_buyer.join(', '));
            $('#partModal').modal();
            get_part_modal_table();
        }
    });
}

function get_part_modal_table() {
    var table1 = $('#partTable').DataTable({
        destroy: true,
        ajax: {
            url: '/report-roll/data-part',
            data: {
                cutting_date: $('#cutting_date_detail').val(),
                style: $('#style_detail').val(),
                articleno: $('#articleno_detail').val(),
                po_buyer: $('#po_buyer_detail').val(),
                state_id: $('#state_id').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        order: [
            [ 0, 'asc' ]
        ],
        columns: [
            {data: 'part_no', name: 'part_no', searchable:true, visible:true, orderable:false},
            {data: 'action', name: 'action', searchable:true, visible:true, orderable:false},
        ],
    });

    return table1;
}

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#url_detail').val(response.url);
            $('#state_id').val(response.id);
            $('#cutting_date_detail').val(response.cutting_date);
            $('#style_detail').val(response.style);
            $('#part_no_detail').val(response.part_no);
            $('#articleno_detail').val(response.articleno);
            $('#po_buyer_detail').val(JSON.stringify(response.po_buyer));
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#style_detail_view').text(response.style);
            $('#part_no_detail_view').text(response.part_no);
            $('#articleno_detail_view').text(response.articleno);
            $('#qty_csi_detail_view').text(response.data_csi);
            $('#qty_ssp_detail_view').text(response.data_ssp);
            $('#po_buyer_detail_view').text(response.po_buyer.join(', '));
            // $('#detailModal').modal();
            $('#partModal').modal('toggle');
            $('#detailModal').modal('toggle');
            get_data_modal_table();
        },
        error: function(response) {
            $('#partModal').unblock();
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
        }
    });
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        ajax: {
            url: '/report-roll/data-detail',
            data: {
                cutting_date: $('#cutting_date_detail').val(),
                style: $('#style_detail').val(),
                articleno: $('#articleno_detail').val(),
                po_buyer: $('#po_buyer_detail').val(),
                part_no: $('#part_no_detail').val(),
                state_id: $('#state_id').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        order: [
            [ 2, 'asc' ]
        ],
        columns: [
            {data: 'barcode', name: 'barcode', searchable:true, visible:true, orderable:false},//0
            {data: 'nomor_roll', name: 'nomor_roll', searchable:true, visible:true, orderable:false},//1
            {data: 'item_code', name: 'item_code', searchable:true, visible:true, orderable:true},//2
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},//3
            {data: 'actual_lot', name: 'actual_lot', searchable:true, visible:true, orderable:false},//4
            {data: 'actual_width', name: 'actual_width', searchable:true, visible:true, orderable:false},//5
            {data: 'document_no', name: 'document_no', searchable:true, visible:true, orderable:false},//6
            {data: 'qty_prepared', name: 'qty_prepared', searchable:true, visible:true, orderable:false},//7
            {data: 'qty_sisa', name: 'qty_sisa', searchable:true, visible:true, orderable:false},//8
            {data: 'status_relax', name: 'status_relax', searchable:true, visible:true, orderable:false},//8
            {data: 'machine', name: 'machine', searchable:true, visible:true, orderable:false},//8
            {data: 'action', name: 'action', searchable:true, visible:true, orderable:false},//9
            {data: 'reduce', name: 'reduce',searchable:false,orderable:false},//10
            {data: 'user_id', name: 'user_id', searchable:true, visible:true, orderable:false},//11
            {data: 'cancel_recv', name: 'cancel_recv', searchable:true, visible:true, orderable:false},//11
        ],
        //Footer CallBack digunakan untuk menjumlahkan nilai pada 1 kolom tertentu
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Update footer
            $( api.column( 7 ).footer() ).html(
               total
            );
        }
    });

    return table1;
}

function reduceModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $('#detailModal').modal('hide');
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#cutting_date_reduce_view').text(response.cutting_date);
            $('#style_reduce_view').text(response.style);
            $('#articleno_reduce_view').text(response.articleno);
            $('#po_buyer_reduce_view').text(response.po_buyer.join(', '));
            $('#barcode_reduce_view').text(response.barcode);
            $('#no_roll_reduce_view').text(response.no_roll);
            $('#lot_reduce_view').text(response.lot);
            $('#qty_reduce_view').text(response.qty);
            $('#material_reduce_view').text(response.material);
            $('#color_reduce_view').text(response.color);
            $('#width_reduce_view').text(response.width);
            $('#reduceModal').modal();
        }
    });
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}
