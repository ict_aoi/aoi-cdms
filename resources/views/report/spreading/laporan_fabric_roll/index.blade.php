@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan Fabric Per Roll</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan Fabric Per Roll</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">By Date</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">By PO Buyer</a></li>
				<li class=""><a href="#basic-tab3" data-toggle="tab" class="legitRipple" aria-expanded="false">By PO Supplier</a></li>
				<li class=""><a href="#basic-tab4" data-toggle="tab" class="legitRipple" aria-expanded="false">By Material/Item Code</a></li>
				<li class=""><a href="#basic-tab5" data-toggle="tab" class="legitRipple" aria-expanded="false">By Per Plan</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadFabricRoll') }}" id="form_filter_date" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="date" placeholder="Masukkan Tanggal Cutting" id="date" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>

				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadFabricRollPOBuyer') }}" id="form_filter_po_buyer" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">PO Buyer</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<input type="text" class="form-control" name="po_buyer" placeholder="Masukkan PO Buyer" id="po_buyer" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>

				<div class="tab-pane" id="basic-tab3">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadFabricRollPOSupplier') }}" id="form_filter_po_supplier" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">PO Supplier</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<input type="text" class="form-control" name="po_supplier" placeholder="Masukkan PO Supplier" id="po_buyer" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>

				<div class="tab-pane" id="basic-tab4">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadFabricRollItemCode') }}" id="form_filter_item_code" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Material/Item Code</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<input type="text" class="form-control" name="item_code" placeholder="Masukkan Material/Item Code" id="item_code" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>

				<div class="tab-pane" id="basic-tab5">
					<form class="form-horizontal" action="#" id="form_filter_date_cutting" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Plan Cutting</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control pickadate" name="date_plan" placeholder="Masukkan Tanggal Plan" id="date_plan" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Select</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>

					<div class="table-responsive">
						<table class="table table-basic table-condensed" id="planningTable">
							<thead>
								<tr>
									<th>Queue</th>
									<th>Style</th>
									<th>Season</th>
									<th>Article</th>
									<th>PO Buyer</th>
									<th>Color</th>
									<th>Size Type</th>
									<th>Factory</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('reportSpreading.getDownloadPerRoll') }}" id="getDataPerRollLink"></a>
@endsection

@section('page-js')
<script>
	$(document).ready( function () {
		$(".file-styled").uniform({
			fileButtonClass: 'action btn bg-warning'
		});
	
		$.extend( $.fn.dataTable.defaults, {
			stateSave: true,
			autoWidth: false,
			autoLength: false,
			processing: true,
			// serverSide: true, //sync
			dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
			language: {
				search: '<span>Filter:</span> _INPUT_',
				searchPlaceholder: 'Type to filter...',
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
			}
		});
	
		var table = $('#planningTable').DataTable({
			ajax: {
				type: 'get',
				url: $('#getDataPerRollLink').attr('href'),
				data: {
					cutting_date: $('#date_plan').val(),
				},
			},
			columnDefs: [
				{
					className: 'dt-center'
				},
			],
			columns: [
				{data: 'queu', name: 'queu', orderable: false, searchable: false},
				{data: 'style', name: 'style',searchable:true,orderable:true},
				{data: 'season', name: 'season',searchable:true,orderable:true},
				{data: 'articleno', name: 'articleno',searchable:true,orderable:true},
				{data: 'poreference', name: 'poreference',searchable:true,orderable:true},
				{data: 'color', name: 'color',searchable:true,orderable:true},
				{data: 'size_category', name: 'size_category',searchable:true,orderable:true},
				{data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
				{data: 'action', name: 'action',searchable:false,orderable:false},
			],
		});
		
		$('#planningTable').on('click','.ignore-click', function() {
			return false;
		});
		
		$('#form_filter_date_cutting').submit(function(event){
			event.preventDefault();
			var cutting_date = $('#date_plan').val();
	
			if(!cutting_date){
				$("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
				return false;
			}
	
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
	
			$.ajax({
				type: 'get',
				url : $('#getDataPerRollLink').attr('href'),
				data: {
					cutting_date: cutting_date,
				},
				beforeSend: function() {
					$('#planningTable').block({
						message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: '10px 15px',
							color: '#fff',
							width: 'auto',
							'-webkit-border-radius': 2,
							'-moz-border-radius': 2,
							backgroundColor: '#333'
						}
					});
				},
				success: function(response) {
					$('#planningTable').unblock();
	
					table.clear();
					table.rows.add(response['data']);
					table.draw();
				},
				error: function(response) {
					$('#planningTable').unblock();
					$.unblockUI();
					if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
				}
			});
		});
	});
</script>
@endsection