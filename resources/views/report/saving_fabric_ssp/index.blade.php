@extends('layouts.app',['active' => 'report_saving_fabric'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Balance Fabric (SSP)</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Balance Fabric (SSP)</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="row form-group" id="form-filter">
			<label><b>Choose Date</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary" id="btn-export">Export</button>
                </div>
            </div>
		</div>
	</div>
</div>
<a href="{{ route('savingfabricssp.exportDataSaving') }}" id="exportData"></a>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
	var url_export = $('#exportData').attr('href');
	$('#btn-export').on('click',function(){
		 window.location.href = url_export+'?date='+$('#date_range').val();
	});

	var url_export_new = $('#exportDataNew').attr('href');
    $('#btn-export-new').on('click',function(){
        window.location.href = url_export_new+'?date='+$('#date_range').val();
    });
});
</script>
@endsection
