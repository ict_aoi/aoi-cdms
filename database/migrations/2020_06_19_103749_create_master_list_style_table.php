<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterListStyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_list_style', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('season')->nullable();
            $table->string('style_standart', 200)->nullable();
            $table->string('style', 200)->nullable();
            $table->string('product_type', 200)->nullable();
            $table->string('product_category', 200)->nullable();
            $table->string('bisnis_unit', 200)->nullable();
            $table->string('main_material', 200)->nullable();
            $table->text('desc_material')->nullable();
            $table->string('fabric_category', 100)->nullable();
            $table->string('fabric_group', 100)->nullable();
            $table->string('difficulty_level', 100)->nullable();
            $table->integer('mp')->nullable();
            $table->integer('total')->nullable();
            $table->string('status_style', 100)->nullable();
            $table->float('tsme')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_list_style');
    }
}
