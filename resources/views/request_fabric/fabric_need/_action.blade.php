@if (isset($download))
    <a href='#' onclick="unduh('{!! $download !!}')"  class="btn btn-primary btn-icon"><i class="icon-folder-download3"></i></a>
@endif
@if (isset($delete))
    <a href='#' onclick="hapus('{!! $delete !!}')"  class="btn btn-danger btn-icon"><i class="icon-close2"></i></a>
@endif
@if (isset($detail))
    <a href="#" onclick="detailModal('{!! $detail !!}')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>
@endif
@if (isset($detailDetail))
    <a href="#" onclick="detailDetailModal('{!! $detailDetail !!}')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>
@endif