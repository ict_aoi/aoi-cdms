<form class="form-horizontal" action="{{ route('masterparam.addKomponenProcess') }}" id="form-add" enctype="multipart/form-data">
@csrf
<div class="row loader-area">
    <input type="text" class="form-control hidden" id="id_style_detail" name="id_style_detail" value="{{$details->id}}">
    <div class="form-group">
        <div class="col-sm-5">
            <label for="komponen_name">Komponen:</label>
            <input type="text" id="komponen_name" name="komponen_name" class="form-control" value="{{$details->komponen_name}}">
        </div>
        <div class="col-sm-5">
            <label for="process_id">Process:</label>
            <select class="form-control select2" name="process_id" id="process_id" required>
                <option value=""></option>
                @foreach($process as $proc)
                    <option value="{{ $proc->id }}">{{ $proc->process_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2 pull-right">
            <label for="">Action</label>
            <div class="input-group-btn"><br>
                <button type="submit" class="btn btn-primary" id="btnAdd">Add & Save</button>
            </div>
        </div>
    </div>
</div>
</form>
<span>Style: {{$details->style}}</span>
<hr>
<div class="table-responsive">
    <table class="table datatable-save-state" id="table-list-komponen-process">
        <thead>
            <tr>
                <th>#</th>
                <th>Style</th>
                <th>Komponen</th>
                <th>Process</th>
                <th width="10px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessJson') }}" id="get_detail_process_"></a>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessDetail') }}" id="get_detail_process_detail"></a>
{{--  <a href="{{ route('masterparam.ajaxGetDataStyle') }}" id="get_data_style_"></a>  --}}

@include('master_parameter.komponen_process._list_process_modal')

<script type="text/javascript">
    var ids = $('#id_style_detail').val();
    var url = $('#get_detail_process_').attr('href');

    $('.select2').select2({
        placeholder: "Please select a State",
        allowClear: true
    });
    
    $('button[data-number=2]').click(function(){
        $('#modal_detail_process_').modal('hide');
    });

    var table_komponen = $('#table-list-komponen-process').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            url: url,
            type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "id": $('#id_style_detail').val(),
                    "_token": $("input[name='csrf-token']").val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'process_name', name: 'process_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_style_detail', 'process_id'];
        var val = [
            $('#id_style_detail').val(), 
            $('#process_id').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#process_id').val('');
                table_komponen.draw();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    function deleteKomponenProcess(e){
        event.preventDefault();
        var id = $(e).data('id');
        var process_name = $(e).data('process');
        if(id == '') {
            return false;
        }
        bootbox.confirm("Are you sure delete "+process_name+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-komponen-process/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": $("input[name='csrf-token']").val(),
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table_komponen.ajax.reload();
                    }
                });
            }
        });
    }

    function detailKomponenProcess(e){
        var id = $(e).data('id');
        var komponen_name = $(e).data('komponen');
        var process_name = $(e).data('process');
        var style = $(e).data('style');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_process_detail').attr('href'),
            data: {'id': id, '_token': _token},
            beforeSend: function(){
                $('#modal_detail_process_ > .modal-content').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function() {
                $('#modal_detail_process_').modal('show');
            },
            success: function(response) {
                console.log(response);
                $('#modal_detail_process_ > .modal-content').unblock();
                $('#list_process').html(response);
                $('#komponen_name_').val(komponen_name);
                $('#process_name_').val(process_name);
                $('#spStyle').text(style);
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
</script>
