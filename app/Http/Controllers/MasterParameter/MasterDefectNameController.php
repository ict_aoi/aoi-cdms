<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DefectName;
use DB;
use Auth;
use Uuid;
use DataTables;
use Carbon\Carbon;

class MasterDefectNameController extends Controller
{
    //MASTER
    public function index(Request $request)
    {
        DB::table('temp_edit_defect_endlines')->where('edit_by',Auth::user()->id)->delete();
        $locator = DB::table('master_process')->whereNull('deleted_at')->OrderBy('process_name','ASC')->get();
        return view('master_parameter.defect_name.index',compact('locator'));
    }

    public function deleteEditSession(Request $request){
        $user_id = $request['user_id'];
        DB::table('temp_edit_defect_endlines')->where('edit_by',$user_id)->delete();
    }

    public function getDataDefectName(Request $request)
    {
        if ($request->ajax()){
            $data = DefectName::wherenull('deleted_at')
                    ->orderBy('updated_at','DESC')
                    ->get();

            return DataTables::of($data)
                ->editColumn('is_repair',function($data){
                    if($data->is_repair == true || $data->is_repair == 1){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('is_reject',function($data){
                    if($data->is_reject == 1 || $data->is_reject == true){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('cutting',function($data){
                    if($data->cutting == 'reject' || $data->cutting == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('fuse',function($data){
                    if($data->fuse == 'reject' || $data->fuse == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('heat_transfer',function($data){
                    if($data->heat_transfer == 'reject' || $data->heat_transfer == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('template',function($data){
                    if($data->template == 'reject' || $data->template == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('bobok',function($data){
                    if($data->bobok == 'reject' || $data->bobok == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('bonding',function($data){
                    if($data->bonding == 'reject' || $data->bonding == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('pad',function($data){
                    if($data->pad == 'reject' || $data->pad == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('join_label',function($data){
                    if($data->join_label == 'reject' || $data->join_label == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('ppa1',function($data){
                    if($data->ppa1 == 'reject' || $data->ppa1 == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('ppa2',function($data){
                    if($data->ppa2 == 'reject' || $data->ppa2 == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('printing',function($data){
                    if($data->printing == 'reject' || $data->printing == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->editColumn('embro',function($data){
                    if($data->embro == 'reject' || $data->embro == 'repair'){
                        return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                    }else{
                        return '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }
                })
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editDefectName', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteDefectName', $data->id)
                    ]);
                })
                ->rawColumns(['action','is_repair','is_reject','cutting','fuse','heat_transfer','template','bobok','bonding','pad','join_label','ppa1','ppa2','printing','embro'])
                ->make(true);
        }
    }

    //add new master
    public function addDefectName(Request $request)
    {
        $defect_name_id = trim(strtolower($request->defect_name_id));
        $defect_name_en = trim(strtolower($request->defect_name_en));
        $is_reject = $request->is_reject == 'on' ? true : false;
        $is_repair = $request->is_repair == 'on' ? true : false;
        $locator = $request->locator_update;
        
        $cek = DefectName::where('nama_defect',$defect_name_id)->where('nama_defect_eng',$defect_name_en)->whereNull('deleted_at')->exists();
        if($cek) {
            return response()->json('Nama Defect Sudah Ada..!', 422);
        }
        try {
            DB::begintransaction();
                DefectName::FirstOrCreate([
                    'nama_defect'       => $defect_name_id,
                    'nama_defect_eng'   => $defect_name_en,
                    'is_repair'         => $is_repair,
                    'is_reject'         => $is_reject,
                    'created_by'        => Auth::user()->id,
                    'locator'           => implode(',',$locator)
                ]);
                foreach($locator as $locator){
                    $convert_column = $this->convertColumn($locator);
                    DefectName::where('nama_defect',$defect_name_id)->where('nama_defect_eng',$defect_name_en)->update([
                        $convert_column['column_name'] => 'reject'
                    ]);
                }
            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master
    public function editDefectName($id)
    {
        $get_edited = DB::table('temp_edit_defect_endlines')->where('defect_id',$id)->first();
        if($get_edited != null){
            if(Auth::user()->id != $get_edited->edit_by){
                $user = DB::table('users')->where('id',$get_edited->edit_by)->first();
                return response()->json('Defect Name Masih Dalam Proses Edit Oleh '.strtoupper($user->name).' AOI '.$user->factory_id,422);
            }
        }else{
            $data = DB::table('master_defect_endlines')->where('id', $id)->first();
            try {
                DB::beginTransaction();
                    DB::table('temp_edit_defect_endlines')->insert([
                        'id'            => Uuid::generate()->string,
                        'defect_id'     => $id,
                        'locator_id'       => $data->locator,
                        'edit_by'       => Auth::user()->id,
                        'created_at'    => Carbon::now()
                    ]);
                DB::commit();
                return response()->json($data, 200);
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
                return response()->json($message,500);
            }   
        }
    }

    //update master
    public function updateDefectName(Request $request)
    {
        $data = $request->all();
        
        $defect_name_id   = trim(strtolower($data['defect_name_id_update']));
        $defect_name_en   = trim(strtolower($data['defect_name_en_update']));
        $locator          = $data['locator_update'];
        $is_repair        = $data['is_repair_update'];
        $is_reject        = $data['is_reject_update'];
        
        try {
            DB::beginTransaction();
                DefectName::where('id', $data['id_update'])
                ->update([
                    'nama_defect' => $defect_name_id,
                    'nama_defect_eng' => $defect_name_en,
                    'updated_by' => \Auth::user()->id,
                    'updated_at' => Carbon::now(),
                    'is_repair' => $is_repair,
                    'is_reject' => $is_reject,
                    'locator'   => $locator
                ]);

                $get_last_locator = DB::table('temp_edit_defect_endlines')->where('defect_id',$data['id_update'])->first();
                if($get_last_locator->locator_id != null){
                    $locator_arr = array_filter(explode(',',$get_last_locator->locator_id));
                    $locator_id = array_filter(explode(',',$locator));
                    foreach($locator_arr as $x){
                        if(in_array($x,$locator_id)){
                            foreach($locator_id as $x => $locator){
                                $convert_column = $this->convertColumn((int)$locator);
                                DefectName::where('id',$data['id_update'])->update([
                                    $convert_column['column_name'] => 'reject'
                                ]);
                            }
                        }else{
                            $locator = $x;
                            $convert_column = $this->convertColumn((int)$locator);
                            DefectName::where('id',$data['id_update'])->update([
                                $convert_column['column_name'] => null
                            ]);
                        }
                    }
                }
                $get_last_locator = DB::table('temp_edit_defect_endlines')->where('defect_id',$data['id_update'])->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }    
    }

    //delete master
    public function deleteDefectName(Request $request)
    {
        $id = $request->id;
                DefectName::where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now(),
                    'deleted_by' => Auth::user()->id
                ]);
    }

    public function downloadMasterdefectname(Request $request)
    {
        $data = DefectName::whereNull('deleted_at')->OrderBy('nama_defect','ASC')->get();

        if(count($data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Master_defect_endlines';
            $i = 1;
            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'DEFECT NAME ( ID )',
                        'DEFECT NAME ( EN )',
                        'IS REPAIR',
                        'IS REJECT',
                        'LOCATOR',
                        'CUTTING',
                        'FUSE',
                        'HE',
                        'TEMPLATE',
                        'BOBOK',
                        'BONDING',
                        'PAD PRINT',
                        'JOIN LABEL',
                        'PPA 1',
                        'PPA 2',
                        'EMBRO',
                        'PRINT'
                    ));

                    foreach ($data as $row){
                        $locator_id = explode(',',(int)$row->locator);
                        $process_name = DB::table('master_process')->whereIn('id',$locator_id)->pluck('process_name')->toArray();
                        $process_name_2 = implode(',',$process_name);
                        $data_excel = array(
                            strtoupper(trim($row->nama_defect)),
                            strtoupper(trim($row->nama_defect_eng)),
                            $row->is_repair,
                            $row->is_reject,
                            $process_name_2,
                            $row->cutting == null ? '' : 'TRUE',
                            $row->fuse == null ? '' : 'TRUE',
                            $row->heat_transfer == null ? '' : 'TRUE',
                            $row->template == null ? '' : 'TRUE',
                            $row->bobok == null ? '' : 'TRUE',
                            $row->bonding == null ? '' : 'TRUE',
                            $row->pad == null ? '' : 'TRUE',
                            $row->join_label == null ? '' : 'TRUE',
                            $row->ppa1 == null ? '' : 'TRUE',
                            $row->ppa2 == null ? '' : 'TRUE',
                            $row->embro == null ? '' : 'TRUE',
                            $row->printing == null ? '' : 'TRUE',
                        );

                        $sheet->appendRow($data_excel);
                    }
                });
            })->download('xlsx'); 
        }
    }

    private function convertColumn($locator)
    {
        switch ($locator) {
            case '1':
                $column_name = 'template';
                break;
            case '4':
                $column_name = 'heat_transfer';
                break;
            case '16':
                $column_name = 'bobok';
                break;
            case '9':
                $column_name = 'fuse';
                break;
            case '10':
                $column_name = 'bonding';
                break;
            case '3':
                $column_name = 'pad';
                break;
            case '18':
                $column_name = 'join_label';
                break;
            case '5':
                $column_name = 'ppa1';
                break;
            case '12':
                $column_name = 'ppa2';
                break;
            case '10':
                $column_name = 'cutting';
                break;
            case '6':
                $column_name = 'embro';
                break;
            case '2':
                $column_name = 'printing';
            default:
                $column_name = null;
        }
        $convert_column = [
            'column_name'     => $column_name,
        ];
        return $convert_column;

    }
}
