@extends('layouts.app',['active' => 'reportRnd'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan RnD</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan RnD</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
        <form action="{{action('Report\RndController@index')}}" method="post" id="form-tab">
            @csrf
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active" data-value="all"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">All</a></li>
                    <li class="" data-value="season"><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">By Season</a></li>
                </ul>

                <div class="tab-content">
                        <input type="hidden" class="form-control" id="tab_active" name="tab_active" value="all">
                        <div class="tab-pane active" id="basic-tab1">
                                <fieldset class="content-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-primary" id="btn-export">Export RnD</button>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>

                        <div class="tab-pane" id="basic-tab2">
                                <fieldset class="content-group">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2 text-bold">Season</label>
                                        <div class="col-lg-10">
                                            
                                            <div class="input-group">
                                                
                                                <select data-placeholder="Select a State..." name="season" id="season" class="form-control select-search">
                                                    <option value=""></option>
                                                    @foreach($master_seasons as $list)
                                                        <option value="{{ $list->id }}">{{ $list->season_name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary legitRipple" type="submit">Export RnD</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>
                </div>
            </div>
        </form>
        
	</div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var url_export = $('#exportData').attr('href');
	/*$('#btn-export').on('click',function(){
		
		 window.location.href = url_export+'?date='+$('#date_range').val();
    });*/

    $('.nav-tabs').on('shown.bs.tab', function (e) {
        var tab_active = $(".nav-tabs .active").attr("data-value");

        $('#tab_active').val(tab_active).trigger('change');
    });
    
});
</script>
@endsection