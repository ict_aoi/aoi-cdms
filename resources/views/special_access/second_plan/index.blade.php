@extends('layouts.app',['active' => 'second_plan'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }

    .change-bg-header {
        background-color: aqua;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Second Plan</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Second Plan</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_marker" name="barcode_marker" placeholder="#Scan Barcode Marker" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="panel-body">
        <div class="panel-heading">
            <h5>Barcode <span id="detail_view_barcode_marker">-</span></h5>
        </div>
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="headerTable">
                <tbody>
                    <tr>
                        <td><strong>Style</strong></td>
                        <td><span id="detail_view_style">Data Empty!</span></td>
                        <td><strong>Cut</strong></td>
                        <td><span id="detail_view_cut">Data Empty!</span></td>
                        <td><strong>Item</strong></td>
                        <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Deliv</strong></td>
                        <td><span id="detail_view_deliv">Data Empty!</span></td>
                        <td><strong>Marker Width (inch)</strong></td>
                        <td><span id="detail_view_width">Data Empty!</span></td>
                        <td rowspan="2"><strong>Color</strong></td>
                        <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Article</strong></td>
                        <td><span id="detail_view_article">Data Empty!</span></td>
                        <td><strong>Marker Length (inch)</strong></td>
                        <td><strong>layer</strong></td>
                        <td><span id="detail_view_length">Data Empty!</span></td>
                        <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Marker Pcs</strong></td>
                        <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                        <td><strong>Total Length (+allow)</strong></td>
                        <td><span id="detail_view_allow">Data Empty!</span></td>
                        <td><strong>Part</strong></td>
                        <td><span id="detail_view_part">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Total Grmt</strong></td>
                        <td><span id="detail_view_total_garment">Data Empty!</span></td>
                        <td><span id="detail_view_layer">Data Empty!</span></td>
                        <td><strong>Plan</strong></td>
                        <td><span id="detail_view_plan">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Po Buyer</strong></td>
                        <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                        <td><strong>Ratio</strong></td>
                        <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Second Plan Add</h5>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <form class="form-horizontal" action="{{ route('secondPlan.add') }}" id="form_filter_date" method="POST">
                    <fieldset class="content-group">

                        @csrf

                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-btn">	
                                        <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                                    </span>
                                    <input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary legitRipple" type="submit">Add</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="col-md-6">
                <fieldset class="content-group">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-btn">	
                                    <button class="btn btn-default btn-icon legitRipple" type="button">Second Plan</button>
                                </span>
                                <input type="text" class="form-control" name="detail_view_second_plan" placeholder="Tidak ada" id="detail_view_second_plan" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-danger legitRipple" id="delete_button">Del</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('secondPlan.scanBarcode') }}" id="scanBarcodeLink"></a>
<a href="{{ route('secondPlan.delete') }}" id="deletePlanLink"></a>
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {
            $('.input-new').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                
                    var barcode_id = $(this).val();
                    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url: $('#scanBarcodeLink').attr('href'),
                        data: {
                            barcode_id: barcode_id,
                        },
                        beforeSend: function() {
                            $('.panel').block({
                                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: '10px 15px',
                                    color: '#fff',
                                    width: 'auto',
                                    '-webkit-border-radius': 2,
                                    '-moz-border-radius': 2,
                                    backgroundColor: '#333'
                                }
                            });
                        },
                        success: function(response) {
                            $('.panel').unblock();
                            $('#detail_view_barcode_marker').text(response.data.barcode_id);
                            $('#detail_view_style').text(response.style);
                            $('#detail_view_cut').text(response.data.cut);
                            $('#detail_view_layer').text(response.data.layer);
                            $('#detail_view_length').text(response.data.marker_length);
                            $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                            $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                            $('#detail_view_part').text(response.data.part_no);
                            $('#detail_view_plan').text(response.plan);
                            $('#detail_view_color_name').text(response.color_name);
                            $('#detail_view_color_code').text(response.color_code);
                            $('#detail_view_item').text(response.item_code);
                            $('#detail_view_deliv').text(response.stat_date);
                            $('#detail_view_width').text(response.marker_width);
                            $('#detail_view_article').text(response.article);
                            $('#detail_view_marker_pcs').text(response.ratio);
                            $('#detail_view_total_garment').text(response.total_garment);
                            $('#detail_view_ratio_size').text(response.ratio_size);
                            $('#detail_view_po_buyer').text(response.po_buyer);
                            $('#detail_view_second_plan').val(response.second_plan);
                            $('#spreading_status').text(response.spreading_status);
                            $('#spreading_date').text(response.spreading_date);
                            $('#spreading_by').text(response.spreading_by);
                            $('#spreading_table').text(response.spreading_table);
                            $('#approve_gl_status').text(response.approve_gl_status);
                            $('#approve_gl_date').text(response.approve_gl_date);
                            $('#approve_gl_by').text(response.approve_gl_by);
                            $('#approve_qc_status').text(response.approve_qc_status);
                            $('#approve_qc_date').text(response.approve_qc_date);
                            $('#approve_qc_by').text(response.approve_qc_by);
                            $('#cutting_status').text(response.cutting_status);
                            $('#cutting_date').text(response.cutting_date);
                            $('#cutting_by').text(response.cutting_by);
                            $('#cutting_table').text(response.cutting_table);
                            $('#bundling_status').text(response.bundling_status);
                            $('#bundling_date').text(response.bundling_date);
                            $('#bundling_by').text(response.bundling_by);
                            $('#bundling_table').text(response.bundling_table);
                            $('#barcode_marker').val('');
                            $('#barcode_marker').focus();
                        },
                        error: function(response) {
                            $('.panel').unblock();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });

            $('#form_filter_date').submit(function (event){
                event.preventDefault();
                var barcode_id = $('#detail_view_barcode_marker').text();
                var cutting_date = $('#cutting_date').val();
                
                if(!cutting_date){
                    $("#alert_warning").trigger("click", 'second plan wajib diisi');
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: $('#form_filter_date').attr('action'),
                    data: {
                        barcode_id: barcode_id,
                        cutting_date: cutting_date
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('#detail_view_second_plan').val(response.second_plan);
                        $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });

            $('#delete_button').on('click', function (){
                var barcode_id = $('#detail_view_barcode_marker').text();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                    type: "POST",
                    url: $('#deletePlanLink').attr('href'),
                    data: {
                        barcode_id: barcode_id
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('#detail_view_second_plan').val(response.second_plan);
                        $("#alert_success").trigger("click", 'Data Berhasil dihapus');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });
        });
    </script>
@endsection