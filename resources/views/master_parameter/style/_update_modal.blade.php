<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.updateStyle') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE STYLE</span> <!-- title -->
                              </legend>
                              
                              {{--  <input type="hidden" class="form-control" name="komponenIdUpdate" id="komponenIdUpdate" placeholder="" readonly>  --}}
                              <input type="hidden" class="form-control" name="idUpdate" id="idUpdate" placeholder="" readonly>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Style:</label>
                              	<div class="col-lg-9">
                                  	<input type="text" class="form-control" name="styleUpdate" id="styleUpdate" placeholder="Style" readonly>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Season:</label>
                              	<div class="col-lg-9">
									{{-- <select data-placeholder="Select a State..." class="form-control select-search" name="seasonUpdate" id="seasonUpdate">
										<option value=""></option>
										@foreach($seasons as $season)
											<option value="{{ $season->id }}">{{ $season->season_name }}</option>
										@endforeach
									</select> --}}
									<input type="hidden" name="seasonUpdate" id="seasonUpdate" class="form-control">
									<input type="text" name="season_name_update" id="season_name_update" class="form-control" readonly>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Komponen:</label>
                              	<div class="col-lg-9">
									  {{--  <input type="text" class="form-control" name="komponenNameUpdate" id="komponenNameUpdate" placeholder="" readonly>  --}}
									  <select data-placeholder="Select a State..." class="form-control select-search" name="komponenIdUpdate" id="komponenIdUpdate" required>
										<option value=""></option>
										  @foreach($komponen as $komp)
											  <option value="{{ $komp->id }}">{{ $komp->komponen_name }}</option>
										  @endforeach
									  </select>
                              	</div>
                          	</div>
                          	<div class="form-group hidden">
                              	<label class="col-lg-3 control-label text-semibold">Komponen:</label>
                              	<div class="col-lg-9">
									  <select data-placeholder="Select a State..." class="form-control select-search" name="komponenUpdate" id="komponenUpdate">
										@foreach($komponen as $komp)
											<option value="{{ $komp->id }}">{{ $komp->komponen_name }}</option>
										@endforeach
									</select>
                              	</div>
                          	</div>
							<div class="form-group">
								<label class="col-lg-3 control-label">Type:</label>
								<div class="col-lg-9">
									<input type="hidden" name="type_id_update" id="type_id_update" class="form-control">
									<input type="text" name="type_name_update" id="type_name_update" class="form-control" readonly>
									{{--  <select class="form-control" name="type_id_update" id="type_id_update">
										@foreach($types as $type)
											<option value="{{ $type->id }}">{{ $type->type_name }}</option>
										@endforeach
									</select>  --}}
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->