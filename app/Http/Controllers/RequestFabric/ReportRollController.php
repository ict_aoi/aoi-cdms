<?php

namespace App\Http\Controllers\RequestFabric;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\User;
use App\Models\Spreading\FabricUsed;
use App\Models\DetailPlan;
use App\Models\RequestFabric\ReportRoll;
use App\Http\Controllers\Controller;
use App\Models\Spreading\SpreadingReportTemp;
use Excel;

class ReportRollController extends Controller
{
    public function index()
    {
        return view('request_fabric.report_roll.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {
                $data = CuttingPlan::orderBy('queu', 'asc')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('is_header',true)
                    ->where('cutting_date', $cutting_date)
                    ->where('deleted_at', null);

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->addColumn('po', function($data) {
                    $view = implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                    return $view;
                })
                ->addColumn('action', function($data) {
                    return view('request_fabric.report_roll._action', [
                        'model'      => $data,
                        'part'     => route('reportRoll.partModal',$data->id)
                    ]);
                })
                // ->setRowAttr([
                //     'style' => function($data) use($cutting_date)
                //     {
                //         //$po_buyer = $data->po_details->pluck('po_buyer')->toArray();

                //         $data_cut_plan = DetailPlan::select('po_buyer')
                //         ->where('id_plan', $data->id)
                //         ->where('deleted_at', null)
                //         ->pluck('po_buyer')
                //         ->toArray();

                //         $data_wms = DB::connection('wms_live_new')
                //                     ->table('integration_whs_to_cutting')
                //                     ->select('barcode')
                //                     ->where('planning_date', $cutting_date)
                //                     // ->where('style', $style)
                //                     ->where(function ($query) use($data) {
                //                         $query->where('style', $data->style);
                //                     })
                //                     ->where('article_no', $data->articleno)
                //                     ->whereIn('po_buyer', $data_cut_plan)
                //                     ->groupBy('barcode')
                //                     ->get();
                //         $bg = null;
                //         $count = 0;
                //         foreach ($data_wms as $key => $val) {
                //             $barcode = $val->barcode;
                //             $report_roll_exist = ReportRoll::where('barcode', $barcode)
                //             ->whereNull('deleted_at')
                //             ->exists();
                //             if ($report_roll_exist) {
                //                 $count++;
                //             }
                //         }
                //         if($count > 0 && $count == $data_wms->count()){
                //             return  'background-color: #74ff76;';
                //         }
                //     },
                // ])
                ->rawColumns(['action','status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->make(true);
            }
        }
    }



    public function partModal($id)
    {
        $detail = CuttingPlan::where('id', $id)
            ->whereNull('deleted_at')
            ->first();

        $obj = new StdClass();
        $obj->id = $detail->id;
        $obj->style = $detail->style;
        $obj->articleno = $detail->articleno;
        $obj->po_buyer = $detail->po_details->pluck('po_buyer')->toArray();
        $obj->cutting_date = $detail->cutting_date;
        $obj->url = route('reportRoll.partModal',$detail->id);
		return response()->json($obj,200);
    }


    public function dataPart(Request $request)
    {
        if(request()->ajax())
        {
            $style = $request->style;
            $id_plan = $request->state_id;
            $articleno = $request->articleno;
            $cutting_date = $request->cutting_date;
            $po_buyer = json_decode($request->po_buyer);

            if($id_plan != null) {

                $data = CuttingMarker::select('color', 'part_no', 'id_plan')
                    ->orderBy('part_no', 'asc')
                    ->where('id_plan', $id_plan)
                    ->where('deleted_at', null)
                    ->groupBy('color', 'part_no', 'id_plan');

                return datatables()->of($data)
                ->addColumn('action', function($data) {
                    return '<a href="#" onclick="detailModal(\''.route('reportRoll.detailModal',['id' => $data->id_plan, 'part_no' => $data->part_no]).'\')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>';
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('action', function($data) {
                    return '';
                })
                ->rawColumns(['action'])
                ->make(true);
            }
        }
    }


    public function dataDetail(Request $request)
    {
        if(request()->ajax())
        {
            $style         = $request->style;
            $state_id      = $request->state_id;
            $articleno     = $request->articleno;
            $cutting_date  = $request->cutting_date;
            $part_no       = $request->part_no;
            $part_no_array = explode('+', $part_no);
            $po_buyer      = json_decode($request->po_buyer);
            $po            = "'".implode("','",$po_buyer)."'";
            $part          = "'".implode("','",$part_no_array)."'";

            if($style != null && $articleno != null && $cutting_date != null && $po_buyer != null) {
                $data = DB::connection('wms_live_new')->select(DB::raw("SELECT planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$articleno' AND po_buyer IN($po) AND part_no IN($part) AND deleted_at IS NULL or style_non_so='$style' AND article_no ='$articleno' AND po_buyer IN($po) AND deleted_at IS NULL AND part_no IN($part) GROUP BY planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, last_status_movement ORDER BY item_code,article_no"));
                return datatables()->of($data)
                ->addColumn('qty_sisa', function($data) {
                    $fabric_use_check = FabricUsed::where('barcode_fabric', strtoupper($data->barcode))->orWhere('barcode_fabric',strtolower($data->barcode))->orderBy('created_spreading', 'desc')->get();
                    if(count($fabric_use_check) > 0) {
                        return $fabric_use_check->first()->actual_sisa;
                    } else {
                        return 'Belum digunakan';
                    }
                })
                ->addColumn('action', function($data) {
                    $report_roll = ReportRoll::where('barcode', strtoupper($data->barcode))->orWhere('barcode',strtolower($data->barcode))->get();
                    if(count($report_roll) > 0) {
                        return '<center>Diterima</center>';
                    }elseif($report_roll == null){
                        return '<center-</center>';
                    }else {
                        if($data->last_status_movement == 'relax') {
                            return '<center>Masih Prepare</center>';
                        } else {
                            if($data->actual_lot == 'CLOSING') {
                                return '<center-</center>';
                            } else {
                                return '<center><input type="checkbox" id="accepted_check" value="'.$data->barcode.'" /></center>';
                            }
                        }
                    }
                })
                ->addColumn('user_id', function($data){
                    $userid_roll = ReportRoll::where('barcode',strtolower($data->barcode))->orWhere('barcode',strtoupper($data->barcode))->first(['user_id']);
                    $username_roll = User::where('id',$userid_roll['user_id'])->first(['name']);
                        if($userid_roll['user_id'] == null){
                            return 'NO USER';
                        }else{
                        return $username_roll['name'];
                        }
                })
                ->editColumn('qty_prepared', function($data) {
                    return round($data->qty_prepared, 2);
                })
                ->addColumn('reduce', function($data) use($state_id) {
                    return view('request_fabric.report_roll._action', [
                        'model'      => $data,
                        'reduce'     => route('reportRoll.reduceModal', ['id' => $state_id, 'barcode' => $data->barcode])
                    ]);
                })
                ->addColumn('cancel_recv', function($data) {
                    $userid_roll = ReportRoll::where('barcode',strtoupper($data->barcode))->orWhere('barcode',strtolower($data->barcode))->first(['user_id']);
                    if($userid_roll == null){
                        return '<a class="btn btn-default btn-icon bg-warning"><li class="icon icon-warning22"></li></a>';
                    }else{
                        return view('request_fabric.report_roll._action', [
                            'model'         => $data,
                            'id'            => $data->barcode,
                            'cancelrcvd'    => route('reportRoll.cancelReceived', $data->barcode)
                        ]);
                    }
                })
                ->setRowAttr([
                    'style' => function($data)
                    {
                        $report_roll = ReportRoll::where('barcode', strtoupper($data->barcode))->orWhere('barcode',strtolower($data->barcode))->get();
                        if(count($report_roll) > 0) {
                            return  'background-color: #74ff76;';
                        } else {
                            if($data->last_status_movement == 'preparation') {
                                return  'background-color: #fffa99;';
                            } else {
                                if($data->actual_lot == 'CLOSING') {
                                    return  'background-color: #ffac91;';
                                }
                            }
                        }
                    },
                ])
                ->addColumn('status_relax',function($data){
                    $data_status = DB::connection('wms_live_new')->table('detail_material_planning_preparations')->select('status_relax')->where('barcode',strtolower($data->barcode))
                    ->whereNull('deleted_at')->first();
                    return ucwords($data_status->status_relax);
                })
                ->addColumn('machine',function($data){
                    $data_machine = DB::connection('wms_live_new')
                    ->table('detail_material_planning_preparations as dmpp')
                    ->Join('machine_preparations as mp','mp.id','=','dmpp.machine_preparation_id')
                    ->select('mp.name')
                    ->where('dmpp.barcode',strtolower($data->barcode))
                    ->whereNull('dmpp.deleted_at')
                    ->first();
                    return strtoupper($data_machine->name);
                })
                ->rawColumns(['action','reduce','cancel_recv','machine','status_relax'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
        return response()->json('Data Tidak Ditemukan',422);
    }

    public function detailModal($id, $part_no)
    {
        $detail = CuttingPlan::where('id', $id)
            ->whereNull('deleted_at')
            ->first();

        $po_buyer_arr  = $detail->po_details->pluck('po_buyer')->toArray();
        $part_no_array = explode('+', $part_no);
        $po            = "'" . implode("','",$po_buyer_arr) . "'";
        $part          = "'" . implode("','",$part_no_array) . "'";;
        
        // $cek_data = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode,STRING_AGG(wms_version,',') as wms_version FROM integration_to_cutting_by_planning_f('$detail->cutting_date') WHERE style='$detail->style' AND article_no ='$detail->articleno' AND po_buyer IN($po) AND part_no IN($part) GROUP BY barcode"));
        // foreach($cek_data as $xx){
        //     $contain_version = str_contains($xx->wms_version,'wmsv1,wmsv2' || $xx->wms_version,'wmsv2,wmsv1');
        //     if($contain_version){
        //         return response()->json('Terdapat Duplikat Barcode, Harap Hubungi ICT',422);
        //     }
        // }

        if($detail->header_id != null & $detail->is_header) {
            $get_plan_id = CuttingPlan::where('header_id', $id)
                ->whereNull('deleted_at')
                ->pluck('id')
                ->toArray();
        } else {
            $get_plan_id = array($id);
        }

        $data_marker_query = CuttingMarker::whereIn('id_plan', $get_plan_id)
            ->where('part_no', $part_no)
            ->where('deleted_at', null);

        $data_uom_consx = DB::table('data_cuttings')
            ->whereIn('plan_id', $get_plan_id)
            ->whereIn('part_no', $part_no_array)
            ->first();

        if($data_uom_consx == null){
            return response()->json('Data CSI Tidak Ditemukan!',422);
        }
        $data_uom_cons = $data_uom_consx->uom;
        //dd($data_uom_cons);

        $data_cons = DB::table('data_cuttings')
            ->whereIn('plan_id', $get_plan_id)
            ->whereIn('part_no', $part_no_array)
            ->sum('fbc');

        if($data_uom_cons == 'M') {
            $data_cons = (float) $data_cons * 1.0936;
        }
        $data_ssp = $data_marker_query->sum('need_total');

        $obj               = new StdClass();
        $obj->id           = $detail->id;
        $obj->style        = $detail->style;
        $obj->part_no      = $part_no;
        $obj->po_buyer     = $detail->po_details->pluck('po_buyer')->toArray();
        $obj->articleno    = $detail->articleno;
        $obj->cutting_date = $detail->cutting_date;
        $obj->data_csi     = number_format($data_cons, 2, '.', ',');
        $obj->data_ssp     = number_format($data_ssp, 2, '.', ',');
        $obj->url          = route('reportRoll.detailModal',['id' => $id, 'part_no' => $part_no]);
		return response()->json($obj,200);
    }

    public function reduceModal($id, $barcode)
    {
        $detail = CuttingPlan::where('id', $id)
            ->whereNull('deleted_at')
            ->first();

        // $fabric = DB::connection('wms_live_new')
        //     ->table('integration_whs_to_cutting')
        //     ->select(DB::raw("planning_date, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement"))
        //     ->where('barcode', $barcode)
        //     ->groupBy('planning_date', 'style', 'article_no', 'barcode', 'nomor_roll', 'item_code', 'color', 'actual_lot', 'actual_width', 'last_status_movement')
        //     ->first();
        $fabric = DB::connection('wms_live_new')->select(DB::raw("SELECT planning_date, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement from integration_to_cutting_f('$barcode') GROUP BY planning_date, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, last_status_movement LIMIT 1"));

        $check_fabric_used = FabricUsed::where('barcode_fabric', strtoupper($barcode))->orWhere('barcode_fabric',strtolower($barcode))->orderBy('created_spreading', 'desc')->first();

        if($check_fabric_used == null) {
            $fabric_qty = $fabric[0]->qty_prepared;
        } else {
            $fabric_qty = $check_fabric_used->actual_sisa;
        }
        $obj = new StdClass();
        $obj->id = $detail->id;
        $obj->style = $detail->style;
        $obj->articleno = $detail->articleno;
        $obj->po_buyer = $detail->po_details->pluck('po_buyer')->toArray();
        $obj->cutting_date = $detail->cutting_date;
        $obj->barcode = strtoupper($fabric[0]->barcode);
        $obj->width = $fabric[0]->actual_width;
        $obj->lot = $fabric[0]->actual_lot;
        $obj->qty = $fabric_qty;
        $obj->material = $fabric[0]->item_code;
        $obj->color = $fabric[0]->color;
        $obj->no_roll = $fabric[0]->nomor_roll;
		return response()->json($obj,200);
    }

    public function reducePengurangan(Request $request)
    {
        if(request()->ajax())
        {
            $barcode = $request->barcode;
            $qty_fabric = $request->qty_fabric;
            $jumlah_pengurangan = $request->jumlah_pengurangan;
            $sisa_pengurangan = $request->sisa_pengurangan;

            if($barcode != null) {
                // $get_barcode = DB::connection('wms_live_new')
                //     ->table('integration_whs_to_cutting')
                //     ->select(DB::raw("planning_date, supplier_name, document_no, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, item_id, sum(qty_prepared) as qty_prepared, last_status_movement"))
                //     ->where('barcode', $barcode)
                //     ->groupBy('planning_date', 'style', 'article_no', 'barcode', 'nomor_roll', 'item_code', 'color', 'actual_lot', 'actual_width', 'last_status_movement', 'document_no', 'supplier_name', 'item_id')
                //     ->first();

                $get_barcode = DB::connection('wms_live_new')->select(DB::raw("SELECT planning_date, supplier_name, document_no, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, item_id, sum(qty_prepared) as qty_prepared, last_status_movement from integration_to_cutting_f(lower('$barcode')) where barcode =upper('$barcode') GROUP BY planning_date, style, article_no, barcode, nomor_roll, item_code, color, actual_lot, actual_width, last_status_movement, document_no, supplier_name, item_id limit 1"));

                if(count($get_barcode) > 0) {
                    FabricUsed::FirstOrCreate([
                        'barcode_marker' => 'REDUCE_QTY',
                        'barcode_fabric' => $get_barcode[0]->barcode,
                        'no_roll' => $get_barcode[0]->nomor_roll,
                        'qty_fabric' => $qty_fabric,
                        'actual_width' => $get_barcode[0]->actual_width,
                        'lot' => $get_barcode[0]->actual_lot,
                        'suplai_layer' => 0,
                        'actual_layer' => 0,
                        'suplai_sisa' => 0,
                        'actual_sisa' => $sisa_pengurangan,
                        'akumulasi_layer' => 0,
                        'reject' => 0,
                        'sambungan' => 0,
                        'actual_use' => $jumlah_pengurangan,
                        'remark' => 0,
                        'kualitas_gelaran' => 'REDUCE QTY',
                        'barcode_table' => null,
                        'user_id' => \Auth::user()->id,
                        'created_spreading' => Carbon::now(),
                        'factory_id' => \Auth::user()->factory_id,
                        'po_supplier' => $get_barcode[0]->document_no,
                        'supplier_name' => $get_barcode[0]->supplier_name,
                        'item_code' => $get_barcode[0]->item_code,
                        'color' => $get_barcode[0]->color,
                        'sambungan_end' => 0,
                        'item_id' => $get_barcode[0]->item_id,
                    ]);

                    return response()->json('qty fabric berhasil dikurangi!',200);
                } else {
                    return response()->json('terjadi kesalahan sistem, Silakan contact ict!',442);
                }
            } else {
                return response()->json('terjadi kesalahan saat pengiriman data, silakan ulangi proses kembali!',442);
            }
        }
    }

    public function acceptRolls(Request $request)
    {
        if(request()->ajax())
        {
            $data_barcode = $request->data_barcode;
            $style        = $request->style;
            $articleno    = $request->articleno;
            $cutting_date = $request->cutting_date;
            $po_buyer     = json_decode($request->po_buyer);
            
            for($i = 0; $i < count($data_barcode); $i++) {
                if($data_barcode[$i][11] == 1) {
                    $barcode_checked = $data_barcode[$i][0];
                    $get_barcode = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode,nomor_roll,item_code,color,actual_lot,actual_width,sum(qty_prepared) as qty_prepared,planning_date,document_no,po_buyer,last_status_movement from integration_to_cutting_f('$barcode_checked') WHERE deleted_at IS NULL GROUP BY barcode,nomor_roll,item_code,color,actual_lot,actual_width,planning_date,document_no,po_buyer,last_status_movement"));
                    $qty = 0;
                    foreach($get_barcode as $key => $val){
                        if($val->last_status_movement != 'out'){
                            return response()->json('Fabric Belum Scan Out WHS!',422);
                        }
                        $po_buyer[] = $val->po_buyer;
                        if(isset($val->qty_prepared)){ 
                            $qty += $val->qty_prepared;
                        }
                    }
                    $po_buyers = implode(',',array_unique($po_buyer));
                    $qty_prepared  = $qty;
                    if(count($get_barcode) > 0) {
                        $data = $get_barcode[0];
                        try {
                            DB::beginTransaction();
                            ReportRoll::FirstOrCreate([
                                'barcode'           => $data->barcode,
                                'no_roll'           => $data->nomor_roll,
                                'material'          => $data->item_code,
                                'color'             => $data->color,
                                'actual_lot'        => $data->actual_lot,
                                'actual_width'      => $data->actual_width,
                                'qty_prepared'      => $qty_prepared,
                                'user_id'           => \Auth::user()->id,
                                'factory_id'        => \Auth::user()->factory_id,
                                'planning_cutting'  => $data->planning_date,
                                'po_supplier'       => $data->document_no,
                                'po_buyer'          => $po_buyers,
                            ]);
                            DB::connection('wms_lives')
                            ->table('detail_material_planning_preparations')
                            ->where('barcode', strtolower($data->barcode))
                            ->whereNotNull('material_out_date')
                            ->update([
                                'cutting_receive_date' => Carbon::now(),
                                'cutting_receive_name' => \Auth::user()->name
                            ]);
                            DB::commit();
                        } catch (Exception $e) {
                            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }
            return response()->json([
                'style'        => $style,
                'articleno'    => $articleno,
                'cutting_date' => $cutting_date,
                'po_buyer'     => $po_buyer
            ],200);
        }
    }

    public function cancelReceived(Request $request)
    {
        $barcode            = $request->id;
        $last_barcode       = ReportRoll::where('barcode',$barcode)->OrderBy('created_at','DESC')->first();
        $check_still_used   = SpreadingReportTemp::where('barcode_fabric',strtolower($barcode))->orWhere('barcode_fabric',$barcode)->where('factory_id',\Auth::user()->factory_id)->first();
        $check_is_used      = FabricUsed::where('barcode_fabric',strtolower($barcode))->orWhere('barcode_fabric',$barcode)->whereNull('deleted_at')->first();
        if($last_barcode == null){
            return response()->json('Barcode Tidak Ditemukan!',422);
        }elseif($check_still_used != null){
            return response()->json('Barcode Masih Spreading, Harap Batalkan Proses Spreading Dahulu!',422);
        }elseif($check_is_used != null){
            return response()->json('Barcode Sudah Spreading, Harap Reverse Proses Spreading Dahulu!',422);
        }else{
            try {
                DB::beginTransaction();
                DB::table('cancel_received_roll_fabric')->insert([
                    'id'                => $last_barcode->id,
                    'barcode'           => $last_barcode->barcode,
                    'no_roll'           => $last_barcode->no_roll,
                    'material'          => $last_barcode->material,
                    'color'             => $last_barcode->color,
                    'actual_lot'        => $last_barcode->actual_lot,
                    'actual_width'      => $last_barcode->actual_width,
                    'qty_prepared'      => $last_barcode->qty_prepared,
                    'created_at'        => $last_barcode->created_at,
                    'updated_at'        => $last_barcode->updated_at,
                    'deleted_at'        => Carbon::now(),
                    'user_id'           => $last_barcode->user_id,
                    'factory_id'        => $last_barcode->factory_id,
                    'planning_cutting'  => $last_barcode->planning_cutting,
                    'po_supplier'       => $last_barcode->po_supplier,
                    'po_buyer'          => $last_barcode->po_buyer,
                    'is_cancel'         => true,
                    'cancel_by'         => \Auth::user()->id,
                    'cancel_time'       => Carbon::now()
                ]);
                $update_cdms = ReportRoll::where('barcode',$last_barcode->barcode)
                ->where('factory_id',\Auth::user()->factory_id)
                ->delete();
                $update_wms = DB::connection('wms_lives')
                ->table('detail_material_planning_preparations')
                ->where('barcode', strtolower($barcode))
                ->orWhere('barcode',strtoupper($barcode))
                ->whereNotNull('material_out_date')
                ->whereNull('deleted_at')
                ->update([
                    'cutting_receive_date' => null,
                    'cutting_receive_name' => null
                ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    public function downloadExcel(Request $request){
        // dd($request->all());
        $cutting_date  = $request->cutting_date_detail;
        $part_no       = $request->part_no_detail;
        $style         = $request->style_detail;
        $articleno     = $request->articleno_detail;
        $part_no_array = explode('+', $part_no);
        $po_buyer      = json_decode($request->po_buyer_detail);
        $po            = "'".implode("','",$po_buyer)."'";
        $part          = "'".implode("','",$part_no_array)."'";
        $data          = DB::connection('wms_live_new')->select(DB::raw("SELECT planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$articleno' AND po_buyer IN($po) AND part_no IN($part) AND deleted_at IS NULL or style_non_so='$style' AND article_no ='$articleno' AND po_buyer IN($po) AND deleted_at IS NULL AND part_no IN($part) GROUP BY planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, last_status_movement ORDER BY item_code,article_no"));
        if(count($data) < 1){
            return response()->json('Data tidak ditemukan.', 422);
        }else{
            $filename = "Report_Roll_"."AOI_".\Auth::user()->factory_id."_".Carbon::parse($cutting_date)->format('d M, Y').' part '.$part_no.' Style '.$style.' Article '.$articleno;
            return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'J' => '@',
                        'K' => '@',
                        'L' => '@',
                        'M' => '@',
                    ));
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','Barcode');
                    $sheet->setCellValue('C1','No Roll');
                    $sheet->setCellValue('D1','Material');
                    $sheet->setCellValue('E1','Color');
                    $sheet->setCellValue('F1','LOT');
                    $sheet->setCellValue('G1','Width');
                    $sheet->setCellValue('H1','PO Supplier');
                    $sheet->setCellValue('I1','Qty (Yds)');
                    $sheet->setCellValue('J1','Balanced');
                    $sheet->setCellValue('K1','Status (WHS)');
                    $sheet->setCellValue('L1','Machine');
                    $sheet->setCellValue('M1','User');

                    $i = 1;
                    foreach ($data as $key => $vl) {
                        $balanced = FabricUsed::where('barcode_fabric', strtoupper($vl->barcode))->orWhere('barcode_fabric',strtolower($vl->barcode))->orderBy('created_spreading', 'desc')->get();
                        $status = DB::connection('wms_live_new')->table('detail_material_planning_preparations')->select('status_relax')->where('barcode',strtolower($vl->barcode))->whereNull('deleted_at')->first();
                        $machine = DB::connection('wms_live_new')
                        ->table('detail_material_planning_preparations as dmpp')
                        ->Join('machine_preparations as mp','mp.id','=','dmpp.machine_preparation_id')
                        ->select('mp.name')
                        ->where('dmpp.barcode',strtolower($vl->barcode))
                        ->whereNull('dmpp.deleted_at')
                        ->first();
                        $user_received = DB::table('report_roll as rr')->select('u.name')
                        ->join('users as u','u.id','=','rr.user_id')
                        ->where('rr.barcode',strtoupper($vl->barcode))
                        ->first();
                        $rw = $i+1;
                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,$vl->barcode);
                        $sheet->setCellValue('C'.$rw,$vl->nomor_roll);
                        $sheet->setCellValue('D'.$rw,$vl->item_code);
                        $sheet->setCellValue('E'.$rw,$vl->color);
                        $sheet->setCellValue('F'.$rw,$vl->actual_lot);
                        $sheet->setCellValue('G'.$rw,$vl->actual_width);
                        $sheet->setCellValue('H'.$rw,$vl->document_no);
                        $sheet->setCellValue('I'.$rw,$vl->qty_prepared);
                        $sheet->setCellValue('J'.$rw,count($balanced) > 0 ? $balanced->first()->actual_sisa : 0);
                        $sheet->setCellValue('K'.$rw,strtoupper($status->status_relax));
                        $sheet->setCellValue('L'.$rw,strtoupper($machine->name));
                        $sheet->setCellValue('M'.$rw,$user_received == null ? 'No User' : $user_received->name);
                        $i++;
                    }
                });
                $excel->setActiveSheetIndex(0);
            })->export('xlsx');
        }
    }
}
