$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/print-id-marker/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: false, orderable: false, searchable: false},
            {data: 'cutting_date', name: 'cutting_date',searchable:true,visible:true,orderable:false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:false,orderable:false},
            {data: 'last_updated_by', name: 'last_updated_by',searchable:false,orderable:false},
            {data: 'detail', name: 'detail',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/print-id-marker/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#btn_print_id').on('click', function() {
        var data = Array();
        $('#detailTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });

        var print_type = $("input[name='print_type']:checked").val();

        $.ajax({
            type: "post",
            url: '/print-id-marker/print-id',
            data: {
                data: data,
                planning_id: $('#planning_id').val(),
                print_type: print_type
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },
            success: function (response) {
                var data = response.data
                $.unblockUI();
                $('#detailModal').modal();
                get_data_modal_table();

                if(response.status == 'isset') {
                    $('#list_id_marker').val(JSON.stringify(data));
                    $('#print_type_').val(response.print_type);
                    $('#get_barcode').submit();
                    $('#list_id_marker').val('');
                    $('#print_type_').val('');
                    $("#check_all").prop('checked', false);
                    $('#detailModal').modal('hide');
                } else {
                    $("#alert_warning").trigger("click", response.message);
                    return false;
                }
            }
        });
    });

    $('#btn_realokasi').on('click', function() {
        var data = Array();
        $('#detailTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });
        var plan_id = $('#planning_id').val();

        $('#detailModal').modal('hide');

        bootbox.confirm("Apakah anda yakin akan realokasi PO untuk data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "post",
                    url: '/print-id-marker/realokasi',
                    data: {
                        plan_id: plan_id,
                        data: data
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function (response) {
                        $.unblockUI();
                        $('#planning_id').val(response.id);
                        $('#cutting_date_detail_view').text(response.cutting_date);
                        $('#style_detail_view').text(response.style);
                        $('#articleno_detail_view').text(response.articleno);
                        $('#detailModal').modal();
                        get_data_modal_table();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        if (response.status == 200) $("#alert_success").trigger("click",response.responseJSON);
                    },
                });
            }
        });
    })

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $("#check_all").prop('checked', false);
        //$('#file_marker').val(null);
    });

    $('#detailDetailModal').on('hidden.bs.modal', function(){
        var planning_id_detail = $('#planning_id_detail').val();
        $('#form_filter_date').submit();
        $('#planning_id').val(planning_id_detail);
        $('#detailModal').modal();
        get_data_modal_table()
    });

    var actual_marker = '#actual_marker';

    $('#detailDetailTable').on('change', actual_marker, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_marker = $row.find(actual_marker).val();
        var $alokasi_fb = $row.find('.alokasi_fb').text();
        var $over_consum = $alokasi_fb - $actual_marker;
        $row.find('#over_consum').val($over_consum);
        if ($over_consum >= 0) {
            $row.find('#over_consum').addClass('bg-success-300');
        } else {
            $row.find('#over_consum').addClass('bg-danger-300');
        }
    });
});

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id);
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#detailModal').modal();
            get_data_modal_table();
        }
    });
}

function approve(url,id_plan,part_no) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            id_plan: id_plan,
            part_no: part_no
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id_plan);
            $('#detailModal').modal();
            get_data_modal_table();
            $("#alert_success").trigger("click", 'Data Berhasil Disetujui!');
        }
    });
}

function reject(url) {
    console.log('function reject');
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/print-id-marker/data-marker',
            data: {
                planning_id: $('#planning_id').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: [0],
                className: 'dt_col_hide',
            },
        ],
        columns: [
            {data: 'barcode_id', name: 'barcode_id', searchable: false},
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'part_no', name: 'part_no', searchable:true, visible:true, orderable:false},
            {data: 'cut', name: 'cut', searchable: true, visible:true, orderable:false},
            {data: 'rasio', name: 'rasio', searchable:true, visible:true, orderable:false},
            {data: 'layer', name: 'layer', searchable:true, visible:true, orderable:false},
            {data: 'status', name: 'status', searchable:false, visible:true, orderable:false},
            {data: 'selector', name: 'selector', searchable:false, visible:true, orderable:false},
        ],
    });

    return table1;
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {

        var attr = checkboxes[i].hasAttribute('disabled');

        if (attr === false) {
            checkboxes[i].checked = source.checked;
        }
    }
}
