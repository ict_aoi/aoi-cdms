<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Actual Marker</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								<span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								<span id="style_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								<span id="articleno_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-8">
								<span id="po_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr>
								<th>Color</th>
								<th>Part No</th>
								<th>Total Cutting</th>
								<th>Status</th>
								<th>Detail</th>
								<th>Note</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<span><strong>Outstanding Part:</strong> <span id="outstanding_part_view">no data</span></span>
				<input type="hidden" name="planning_id" id="planning_id" />
			</div>

			<div class="modal-footer">
				<a class="btn btn-primary" id="btn-download-ratio" target="_blank">Download Ratio</a>
				<a class="btn btn-success" id="btn-download-report" target="_blank">Download Report</a>
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>