<?php

namespace App\Http\Controllers\ComponentMovement;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScanComponentSubcontInController extends Controller
{
    public function index()
    {
        $packinglists = DB::table('packinglist_subcont')->whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->where('status', 'draft')->get();
        return view('component_movement.scan_component_subcont_in.index', compact('packinglists'));
    }
}
