<?php

namespace App\Http\Controllers\Subcont;

use DB;
use Uuid;
use StdClass;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class ScanSubcontInController extends Controller
{
    public function index()
    {
        return view('subcont.scan_in.index');
    }

    public function dataComponent(Request $request)
    {
        if(request()->ajax())
        {
            $locator = $request->locator;
            $state = $request->state;
            $user = $request->user;

            if($locator != null && $state != null && $user != null) {
                $data = DB::table('temp_scan_subcontsys')
                ->where('created_by',$user)
                ->where('factory_id',\Auth::user()->factory_id)
                ->where('state',$state)
                ->where('locator',$locator)
                ->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->addColumn('action', function($data) {
                    return view('subcont.scan_in._action', [
                        'model'      => $data,
                        'hapus'     => route('scanSubcontIn.deleteComponent', $data->barcode_id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataScanInsubcont(Request $request)
    {
        if(request()->ajax())
        {
            $user = $request->user;

            if($user != null) {
                $data = DB::table('temp_scan_subcontsys')->where('created_by',$user)->where('factory_id',\Auth::user()->factory_id)->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == null || $data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataScanin(Request $request)
    {
        if(request()->ajax())
        {
            $user = $request->user;  

            $obj = new StdClass();
            $obj->user = $user;
            $obj->locator = '12';
            $obj->state = 'in';
            
            return response()->json($obj,200);
        }
    }
    
    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $state = 'in';
            $locator = '12';
            $user = \Auth::user()->id;
            $barcode_component = $request->barcode_component;

            if($user != null && $state != null && $barcode_component != null && $locator != null)
            {
                $bundle_detail = DB::table('bundle_detail')->where('barcode_id',$barcode_component)->first();
                $bundle_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                $barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                //$barcode_sds_check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();
                $process_scan_factory = DB::table('detail_packinglist_subtemp')->where('bundle_id',$barcode_component)->first();
                if($process_scan_factory == null){
                    if($bundle_detail != null && $bundle_detail->barcode_id == $barcode_component){
                        $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->where('locator_to',10)->where('status_to','out')->first();
                        $in_factory_order = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->where('locator_to',3)->where('status_to','in')->first();
                        $get_info_factory_order = DB::table('detail_packinglist_distribusi')->where('bundle_id',$barcode_component)->first();
                        if($out_cutting != null)
                        {
                            if($in_factory_order == null){
                                return response()->json('Bundle Belum Scan In Factory Asal!',422);
                            }
                            $part_num = DB::table('master_style_detail')->where('id',$bundle_detail->style_detail_id)->first();
                            $type_name = DB::table('types')->where('id',$part_num->type_id)->first();
                            $bundle_header = DB::table('bundle_header')->where('id',$bundle_detail->bundle_header_id)->first();
                            //$barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$bundle_detail->sds_barcode)->first();
                            if($part_num == null){
                                return response()->json('Part Number Dihapus Dari Sistem',422);
                            }
                            // if($barcode_sds->artwork_out != null){
                            //     return response()->json('Bundle Sudah Scan di SDS!',422);
                            // }
                            $get_process = explode(',', $part_num->process);

                            if($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }
        
                            $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                            if(!in_array(3, $get_locator_process)){
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }

                            if($bundle_header->set_type == 'Non'){
                                $style = $bundle_header->style;
                            }else{
                                $style = $bundle_header->style;
                            }

                            $component_check = DB::table('subcont_movements')->where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                            $subcont_factory = DB::table('master_factory')->where('id',\Auth::user()->factory_id)->first();
                            $current_scan = DB::table('temp_scan_subcontsys')->where('barcode_id',$barcode_component)->first();
            
                            if($current_scan == null){
                                if($component_check != null && $component_check->description == 'in '.$subcont_factory->factory_name){
                                    return response()->json('Barcode sudah melalui scan in '.$subcont_factory->factory_name,422);
                                }else{
                                    DB::table('temp_scan_subcontsys')->insert([
                                        'id'                => Uuid::generate()->string,
                                        'barcode_id'        => $barcode_component,
                                        'poreference'       => $bundle_header->poreference,
                                        'season'            => $bundle_header->season,
                                        'style'             => $bundle_header->style,
                                        'set_type'          => $type_name->type_name,
                                        'size'              => $bundle_header->size,
                                        'komponen_name'     => $bundle_detail->komponen_name,
                                        'cut_num'           => $bundle_header->cut_num,
                                        'start_no'          => $bundle_detail->start_no,
                                        'end_no'            => $bundle_detail->end_no,
                                        'factory_id'        => \Auth::user()->factory_id,
                                        'locator'           => $locator,
                                        'state'             => $state,
                                        'created_at'        => Carbon::now(),
                                        'created_by'        => $user,
                                        'factory_order'     => $bundle_header->factory_id,
                                        'article'           => $bundle_header->article,
                                        'part_num'          => $part_num->part,
                                        'qty'               => $bundle_detail->qty,
                                        'pl_factory_order'  => $get_info_factory_order->id_packinglist,
                                        'color_name'        => $bundle_header->color_name
                                    ]);

                                    //INSERT DATA IN TEMPORARY SCAN ARTWORK SDS
                                    // DB::connection('sds_live')->table('temp_scan_proc')->insert([
                                    //     'ap_id'         => $bundle_detail->sds_barcode,
                                    //     'proc'          => 'ART',
                                    //     'user_id'       => \Auth::user()->nik,
                                    //     'scan_time'     => Carbon::now(),
                                    //     'check_out'     => false,
                                    //     'factory'       => \Auth::user()->factory_id
                                    // ]);
                                }
                            }else{
                                return response()->json('Bundle Sedang menjalani Proses scan in '.$subcont_factory->factory_name,422);
                            }
                        }else{
                            return response()->json('Bundle Belum Scan Out Cutting!',422);
                        }
                    }
                    // elseif($bundle_detail == null && $barcode_sds_check != null && $bundle_sds_cdms != null && $barcode_component == $bundle_sds_cdms->sds_barcode){
                    //     $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$bundle_sds_cdms->barcode_id)->where('locator_to',10)->where('status_to','out')->first();
                    //     if($out_cutting != null)
                    //     {
                    //         $barcode_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                    //         $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_detail_id)->first();
                    //         $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                    //         $bundle_header_sds = DB::table('bundle_header')->where('id',$barcode_sds_cdms->bundle_header_id)->first();
                    //         $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                    //         if($part_num_sds == null){
                    //             return response()->json('Part Number Dihapus dari Sistem!',422);
                    //         }

                    //         if($barcode_sds->artwork_out != null){
                    //             return response()->json('Bundle Sudah Scan di SDS!',422);
                    //         }

                    //         $get_process = explode(',', $part_num_sds->process);

                    //         if($get_process[0] == null || $get_process[0] == '') {
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }
        
                    //         $get_locator_process = DB::table('master_process')
                    //         ->select('locator_id')
                    //         ->whereIn('id', $get_process)
                    //         ->groupBy('locator_id')
                    //         ->pluck('locator_id')->toArray();

                    //         if(!in_array(3, $get_locator_process)){
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }

                    //         if($bundle_header_sds->set_type == 'Non'){
                    //             $style = $bundle_header_sds->style;
                    //         }else{
                    //             $style = $bundle_header_sds->style;
                    //         }

                    //         $component_check = DistribusiMovement::where('barcode_id', $bundle_sds_cdms->barcode_id)->orderBy('created_at','desc')->first();
                    //         $current_scan = DB::table('temp_scan_out_artwork')->where('barcode_id',$bundle_sds_cdms->barcode_id)->first();

                    //         $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                    //         if(!$current_scan){
                    //             if($component_check != null && $component_check->description == 'out ARTWORK'){
                    //                 return response()->json('Barcode sudah melalui scan Out Artwork!',422);
                    //             }else{
                    //                 DB::table('temp_scan_out_artwork')->insert([
                    //                     'id'                => Uuid::generate()->string,
                    //                     'barcode_id'        => $barcode_sds_cdms->barcode_id,
                    //                     'poreference'       => $bundle_header_sds->poreference,
                    //                     'style'             => $bundle_header_sds->style,
                    //                     'set_type'          => $bundle_header_sds->set_type,
                    //                     'size'              => $bundle_header_sds->size,
                    //                     'article'           => $bundle_header_sds->article,
                    //                     'part_num'          => $part_num_sds->part,
                    //                     'komponen_name'     => $bundle_sds_cdms->komponen_name,
                    //                     'cut_num'           => $bundle_header_sds->cut_num,
                    //                     'start_no'          => $bundle_sds_cdms->start_no,
                    //                     'end_no'            => $bundle_sds_cdms->end_no,
                    //                     'qty'               => $bundle_sds_cdms->qty,
                    //                     'locator'           => $locator,
                    //                     'state'             => $state,
                    //                     'created_at'        => Carbon::now(),
                    //                     'created_by'        => $user,
                    //                     'factory_id'        => \Auth::user()->factory_id
                    //                 ]);

                    //                 //INSERT DATA IN TEMPORARY TABLE SCAN OUT ARTWORK
                    //                 // DB::connection('sds_live')->table('temp_scan_proc')->insert([
                    //                 //     'ap_id'         => $barcode_component,
                    //                 //     'proc'          => 'ART',
                    //                 //     'user_id'       => \Auth::user()->nik,
                    //                 //     'scan_time'     => Carbon::now(),
                    //                 //     'check_out'     => false,
                    //                 //     'factory'       => \Auth::user()->factory_id
                    //                 // ]);
                    //             }
                    //         }else{
                    //             return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                    //         }
                    //     }else{
                    //         return response()->json('Bundle Belum Scan Out Cutting!',422);
                    //     }
                    // }
                    // elseif($bundle_sds_cdms == null  && $barcode_sds_only != null)
                    // {
                    //     $out_cutting = DB::table('distribusi_movements')
                    //     ->where('barcode_id',$barcode_component)
                    //     ->where('locator_to',10)->where('status_to','out')->first();
                    //     if($out_cutting != null){
                    //         $barcode_sds_cdms = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                    //         $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_id)->first();
                    //         $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                    //         $bundle_header_sds = DB::table('sds_header_movements')->where('id',$barcode_sds_cdms->sds_header_id)->first();
                    //         $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                    //         if($part_num_sds == null){
                    //             return response()->json('Part Number Dihapus Dari Sistem!',422);
                    //         }

                    //         if($barcode_sds->artwork != null){
                    //             return response()->json('Bundle Sudah Scan di SDS!',422);
                    //         }

                    //         $get_process = explode(',', $part_num_sds->process);

                    //         if($get_process[0] == null || $get_process[0] == '') {
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }
        
                    //         $get_locator_process = DB::table('master_process')
                    //         ->select('locator_id')
                    //         ->whereIn('id', $get_process)
                    //         ->groupBy('locator_id')
                    //         ->pluck('locator_id')->toArray();

                    //         if(!in_array(3, $get_locator_process)){
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }

                    //         if($bundle_header_sds->set_type == 'Non'){
                    //             $style = $bundle_header_sds->style;
                    //         }else{
                    //             $style = $bundle_header_sds->style;
                    //         }

                    //         $component_check = DistribusiMovement::where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                    //         $current_scan = DB::table('temp_scan_out_artwork')->where('barcode_id',$barcode_component)->first();

                    //         $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                    //         if(!$temp_scan_proc){
                    //             if($component_check != null && $component_check->description == 'out ARTWORK'){
                    //                 return response()->json('Barcode sudah melalui scan Out Artwork!',422);
                    //             }else{
                    //                 DB::table('temp_scan_out_artwork')->insert([
                    //                     'id'                => Uuid::generate()->string,
                    //                     'barcode_id'        => $barcode_sds_cdms->sds_barcode,
                    //                     'poreference'       => $bundle_header_sds->poreference,
                    //                     'style'             => $bundle_header_sds->style,
                    //                     'set_type'          => $type_name_sds->type_name,
                    //                     'size'              => $bundle_header_sds->size,
                    //                     'article'           => $bundle_header_sds->article,
                    //                     'part_num'          => $part_num_sds->part,
                    //                     'komponen_name'     => $barcode_sds_cdms->komponen_name,
                    //                     'cut_num'           => $bundle_header_sds->cut_num,
                    //                     'start_no'          => $barcode_sds_cdms->start_no,
                    //                     'end_no'            => $barcode_sds_cdms->end_no,
                    //                     'qty'               => $barcode_sds_cdms->qty,
                    //                     'locator'           => $locator,
                    //                     'state'             => $state,
                    //                     'created_at'        => Carbon::now(),
                    //                     'created_by'        => $user,
                    //                     'factory_id'        => \Auth::user()->factory_id
                    //                 ]);

                    //                 DB::connection('sds_live')->table('temp_scan_proc')->insert([
                    //                     'ap_id'         => $barcode_component,
                    //                     'proc'          => 'ART',
                    //                     'user_id'       => \Auth::user()->nik,
                    //                     'scan_time'     => Carbon::now(),
                    //                     'check_out'     => false,
                    //                     'factory'       => \Auth::user()->factory_id
                    //                 ]);
                    //             }
                    //         }else{
                    //             return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                    //         }
                    //     }else{
                    //         return response()->json('Bundle Belum Scan Out Cutting!',422);
                    //     }
                    // }
                    else{
                        return response()->json('Barcode Tidak Ditemukan!',422);
                    }
                }else{
                    return response()->json('Bundle Masih di Proses di Factory Asal!',422);
                }
            }else{
                return response()->json('Harap refresh Halaman Ini!',422);
            }
        }
    }

    public function deleteComponent(Request $request, $id)
    {
        if(request()->ajax())
        {
            if($id != null) {
                $state = 'in';
                $locator = '12';
                $user = \Auth::user()->id;
                    DB::table('temp_scan_subcontsys')->where('barcode_id', $id)->delete();
                    //$barcode_sds_cdms = DB::table('bundle_detail')->where('barcode_id',$id)->first();
                    // if($barcode_sds_cdms != null){
                    //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds_cdms->sds_barcode)->update([
                    //         'artwork_out'       => null,
                    //         'artwork_out_pic'   => null
                    //     ]);
                    //     DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_sds_cdms->sds_barcode)->delete();
                    // }else{
                    //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$id)->update([
                    //         'artwork_out'       => null,
                    //         'artwork_out_pic'   => null
                    //     ]);
                    //     DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$id)->delete();
                    // }

                    $obj = new StdClass();
                    $obj->user = $user;
                    $obj->locator = $locator;
                    $obj->state = $state;

                    return response()->json($obj,200);
            } else {
                return response()->json('Terjadi Kesalahan Silahkan Refresh Halaman Ini!',422);
            }
        }
    }

    public function saveComponent(Request $request)
    {
        if(request()->ajax())
        {
            $user = $request->user;
            $state = $request->state;
            $locator =$request->locator;
            if($locator != null && $user != null && $state != null) {
                $data = DB::table('temp_scan_subcontsys')->where('created_by', $user)->get();
                $barcode_arr = DB::table('temp_scan_subcontsys')->select('barcode_id')->where('created_by',$user)->pluck('barcode_id')->toArray();
                $barcode_arr_sds = DB::table('bundle_detail')->whereIn('barcode_id',$barcode_arr)->pluck('sds_barcode')->toArray();
                $barcode_cdms_check = DB::table('bundle_detail')->whereIn('barcode_id',$barcode_arr)->get();
                // $barcode_sds_check = DB::connection('sds_live')->table('bundle_info_new')->whereIn('barcode_id',$barcode_arr_sds)->get();
                // $barcode_sds_arr = DB::connection('sds_live')->table('bundle_info_new')->whereIn('barcode_id',$barcode_arr_sds)->pluck('barcode_id')->toArray();
                // $barcode_sds_only = DB::connection('sds_live')->table('bundle_info_new')->whereIn('barcode_id',$barcode_arr)->pluck('barcode_id')->toArray();
                if(count($data) > 0) {
                    // if(count($barcode_cdms_check) > 0 && count($barcode_sds_check) > 0)
                    // {
                        try {
                            DB::beginTransaction();
                            $locator = DB::table('master_locator')->where('id', $locator)->first();
                            foreach($data as $x){
                                $last_locator = DB::table('temp_scan_subcontsys')->where('barcode_id',$x->barcode_id)->orderBy('created_at','desc')->first();
                                $subcont_factory = DB::table('master_factory')->where('id',$last_locator->factory_id)->first();
                                $last_locator_factor_order = DB::table('distribusi_movements')->where('barcode_id',$x->barcode_id)->orderBy('created_at','desc')->first();
                                $last_locator_to = $last_locator->factory_id == null ? null : $last_locator->factory_id;
                                $last_status_to = $last_locator_factor_order->status_to == null ? null : $last_locator_factor_order->status_to;

                                DB::table('subcont_movements')->insert([
                                    'id'            => Uuid::generate()->string,
                                    'barcode_id'    => $x->barcode_id,
                                    'factory_from'  => $x->factory_order,
                                    'status_from'   => $last_status_to,
                                    'factory_to'    => \Auth::user()->factory_id,
                                    'factory_order' => $x->factory_order,
                                    'status_to'     => $state,
                                    'description'   => $state.' '.$subcont_factory->factory_name,
                                    'user_id'       => \Auth::user()->id,
                                    'ip_address'    => $this->getIPClient(),
                                    'created_at'    => Carbon::now(),
                                    'updated_at'    => Carbon::now()
                                ]);
                                DB::table('bundle_detail')->where('barcode_id',$x->barcode_id)->update([
                                    'current_locator_id'    => $last_locator->factory_id,
                                    'current_status_to'     => $state,
                                    'current_description'   => $state.' '.$subcont_factory->factory_name,
                                    'updated_movement_at'   => Carbon::now(),
                                    'current_user_id'       => \Auth::user()->id,
    
                                ]);
                                DB::table('detail_transaction_subcontsys')->insert([
                                    'id'                => Uuid::generate()->string,
                                    'barcode_id'        => $x->barcode_id,
                                    'poreference'       => $x->poreference,
                                    'season'            => $x->season,
                                    'style'             => $x->style,
                                    'article'           => $x->article,
                                    'set_type'          => $x->set_type,
                                    'size'              => $x->size,
                                    'part_num'          => $x->part_num,
                                    'komponen_name'     => $x->komponen_name,
                                    'cut_num'           => $x->cut_num,
                                    'start_no'          => $x->start_no,
                                    'end_no'            => $x->end_no,
                                    'qty'               => $x->qty,
                                    'factory_id'        => $x->factory_id,
                                    'factory_order'     => $x->factory_order,
                                    'locator_from'      => $x->locator,
                                    'state_from'        => $x->state,
                                    'created_at'        => Carbon::now(),
                                    'created_by'        => \Auth::user()->id,
                                    'pl_factory_order'  => $x->pl_factory_order,
                                    'color_name'        => $x->color_name
                                ]);

                                // $barcode_sds = DB::table('bundle_detail')->where('barcode_id',$x->barcode_id)->first();
                                // if($barcode_sds == null){
                                //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$x->barcode_id)->update([
                                //         'location'              => 'ART',
                                //         'factory'               => 'AOI'.\Auth::user()->factory_id,
                                //         'artwork_out'           => Carbon::now(),
                                //         'artwork_out_pic'       => \Auth::user()->nik
                                //     ]);
                                // }else{
                                //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->update([
                                //         'location'              => 'ART',
                                //         'factory'               => 'AOI'.\Auth::user()->factory_id,
                                //         'artwork_out'           => Carbon::now(),
                                //         'artwork_out_pic'       => \Auth::user()->nik
                                //     ]);
                                // }
                            }

                            DB::table('temp_scan_subcontsys')->where('created_by', $user)->delete();
                            //DB::connection('sds_live')->table('temp_scan_proc')->whereIn('ap_id',$barcode_arr_sds)->delete();

                            DB::commit();
                        } catch (Exception $e) {
                            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    // }
                    // elseif(in_array($barcode_sds_arr,$barcode_arr_sds) == false)
                    // {
                    //     try {
                    //         DB::beginTransaction();
                    //         foreach($data as $x){
                    //             $last_locator = DistribusiMovement::where('barcode_id',$x->barcode_id)->orderBy('created_at','desc')->first();
                    //             $last_locator_to = $last_locator->locator_to == null ? null : $last_locator->locator_to;
                    //             $last_status_to = $last_locator->status_to == null ? null : $last_locator->status_to;
                    //             DistribusiMovement::FirstOrCreate([
                    //                 'barcode_id'    => $x->barcode_id,
                    //                 'locator_from'  => $last_locator_to,
                    //                 'status_from'   => $last_status_to,
                    //                 'locator_to'    => $locator,
                    //                 'status_to'     => $state,
                    //                 'ip_address'    => $this->getIPClient(),
                    //             ]);
                    //             DB::table('sds_detail_movements')->where('sds_barcode',$x->barcode_id)->update([
                    //                 'current_location_id'    => $locator,
                    //                 'current_status_to'     => $state,
                    //                 'current_description'   => $state.' '.$locator->locator_name,
                    //                 'updated_movement_at'   => Carbon::now(),
                    //                 'current_user_id'       => \Auth::user()->id,
                    //             ]);

                    //             DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$x->barcode_id)->update([
                    //                 'location'              => 'ART',
                    //                 'factory'               => 'AOI'.\Auth::user()->factory_id,
                    //                 'artwork_out'           => Carbon::now(),
                    //                 'artwork_out_pic'       => \Auth::user()->nik
                    //             ]);
                    //         }

                    //         DB::table('temp_scan_out_artwork')->where('created_by', $user)->delete();

                    //         DB::connection('sds_live')->table('temp_scan_proc')->whereIn('ap_id',$barcode_sds_only)->delete();
                    //         DB::commit();
                    //     } catch (Exception $e) {
                    //         DB::rollBack();
                    //         $message = $e->getMessage();
                    //         ErrorHandler::db($message);
                    //     }
                    //     return response()->json($obj,200);
                    // }
                    // else{
                    //     return response()->json('Terjadi Kesalahan Saat Menyimpan Data',422);
                    // }
                } else {
                    return response()->json('Harap Refresh Halaman Ini!',422);
                }
            } else {
                return response()->json('Harap Refresh Halaman Ini!',422);
            }
        }
    }

    private function getIPClient()
    {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
