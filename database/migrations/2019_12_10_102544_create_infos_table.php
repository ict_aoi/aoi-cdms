<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('type');
            $table->date('cutting_date');
            $table->string('style');
            $table->string('articleno');
            $table->string('po_buyer');
            $table->string('documentno');
            $table->text('desc')->nullable();
            $table->boolean('is_integrated')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
