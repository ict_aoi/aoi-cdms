<div id="uploadModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Download File Marker</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="style_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="articleno_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="uploadTable">
						<thead>
							<tr>
								<th>#</th>
								<th>File Name</th>
								<th>Uploaded By</th>
								<th>Uploaded At</th>
								<th>Received By</th>
								<th>Received At</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="upload_file_id" id="upload_file_id" />
				<input type="hidden" name="upload_cutting_date" id="upload_cutting_date" />
				<input type="hidden" name="upload_style" id="upload_style" />
				<input type="hidden" name="upload_articleno" id="upload_articleno" />
				<input type="hidden" name="upload_size_category" id="upload_size_category" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>