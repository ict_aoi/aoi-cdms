@extends('layouts.app',['active' => 'master_setting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Setting Area </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Master Setting</li>
            <li class="active">Setting Area</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            @csrf
            <div class="col-lg-1"></div>
            <div class="col-lg-1"><label style="padding-top: 20px;"><b>AREA NAME</b></label></div>
            <div class="col-lg-5"><input type="text" name="new_area" id="new_area" class="form-control" onkeyup="this.value = this.value.toUpperCase();"></div>
            <div class="col-lg-1"><button class="btn btn-default" id="save" name="save"><span class="icon-location4"></span> ADD AREA</button></div>
        </div>
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Area Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalEdit"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            
            <div class="modal-body">
                <div class="row form-group">
                    <center><h4>EDIT AREA</h4></center>
                </div>
                <div class="row form-group">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-1"><label style="padding-top: 20px;"><b>AREA NAME</b></label></div>
                    <div class="col-lg-5">
                        <input type="text" name="id" id="id" hidden=""> 
                        <input type="text" name="edit_name" id="edit_name" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                    </div>
                    <div class="col-lg-1">
                        <button type="button" class="btn btn-primary" onclick="saveedit(this);"><span class="icon-upload"></span> Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('Asetting.getData') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'area_name', name: 'area_name'},
            {data: 'action', name: 'action'}
        ]
    });

    $('#save').on('click',function(event){
        event.preventDefault();
        var new_area = $('#new_area').val();
        
        
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url : "{{ route('Asetting.addAread') }}",
            data:{new_area:new_area},
            beforeSend:function(){
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success:function(response){
                var data_response = response.data;
                if (data_response.status == 200) {
                    $("#alert_success").trigger("click", data_response.output);
                    
                }else{
                    $("#alert_warning").trigger("click",data_response.output);
                   
                }
                tableL.clear();
                tableL.draw();
                $.unblockUI();
                $('#new_area').val('');
            },
            error:function(response){
                var data_response = response.data;
                $("#alert_warning").trigger("click",data_response.output);
                $('#new_area').val('');
            }
        });
    });

    

});

function edit(e){
    var id = $(e).data('id');

    $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url : "{{ route('Asetting.getDataEdit') }}",
            data:{id:id},
            success:function(response){
                $('#edit_name').val(response.area_name);
                $('#id').val(id);
                $('#modalEdit').modal('show');
            },
            error:function(response){
                console.log(response);
            }
        });

}

function saveedit(e){
    var id = $('#id').val();
    var name = $('#edit_name').val();
    var tableL = $('#table-list').DataTable();
    $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url : "{{ route('Asetting.setEditData') }}",
            data:{id:id,name:name},
            beforeSend:function(){
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success:function(response){
                var data_response = response.data;
                if (data_response.status == 200) {
                    $("#alert_success").trigger("click", data_response.output);
                    
                }else{
                    $("#alert_warning").trigger("click",data_response.output);
                   
                }
                tableL.clear();
                tableL.draw();
                $.unblockUI();
                $('#modalEdit').modal('hide');
            },
            error:function(response){
                var data_response = response.data;
                $("#alert_warning").trigger("click",data_response.output);
                 tableL.clear();
                tableL.draw();
                $.unblockUI();
                $('#modalEdit').modal('hide');
            }
        });

}

function deleteId(e){
    var id = e.getAttribute('data-id');
    var tableL = $('#table-list').DataTable();
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url : "{{ route('Asetting.deleteArea') }}",
            data:{id:id},
            beforeSend:function(){
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success:function(response){
                var data_response = response.data;
                if (data_response.status == 200) {
                    $("#alert_success").trigger("click", data_response.output);
                    
                }else{
                    $("#alert_warning").trigger("click",data_response.output);
                   
                }
                tableL.clear();
                tableL.draw();
                $.unblockUI();
            },
            error:function(response){
                var data_response = response.data;
                $("#alert_warning").trigger("click",data_response.output);
                 tableL.clear();
                tableL.draw();
                $.unblockUI();
            }
        });
}
</script>
@endsection