@extends('layouts.app',['active' => 'report_qc_check'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan QC</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan QC</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="row form-group" id="form-filter">
			<label><b>Tanggal Laporan</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                <div class="input-group-btn">
                        <button type="button" class="btn btn-warning" id="btn-export-new">Export New</button>
                        <button type="button" class="btn btn-primary" id="btn-export">Export</button>
                </div>
            </div>
		</div>
	</div>
</div>
@if(\Auth::user()->factory_id == 2)
    <div class="panel panel-flat">
	<div class="panel-body">
		<div class="row form-group" id="form-filter">
			<label><b>Tanggal Planning Cutting</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control daterange-basic" name="date_range" id="date_range_x">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-danger" id="btn-download-new">Export</button>
                </div>
            </div>
		</div>
	</div>
</div>
@endif
<a href="{{ route('reportQc.exportQc') }}" id="exportData"></a>
<a href="{{ route('reportQc.exportQcCutting') }}" id="exportDataNew"></a>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
	var url_export = $('#exportData').attr('href');
	$('#btn-export').on('click',function(){
		 window.location.href = url_export+'?date='+$('#date_range').val();
	});

	var url_export_new = $('#exportDataNew').attr('href');
    $('#btn-export-new').on('click',function(){
        window.location.href = url_export_new+'?date='+$('#date_range').val();
    });
    var url_export_dd = $('#exportDatax').attr('href');
    $('#btn-download-new').on('click',function(){
        window.location.href = url_export_dd+'?date='+$('#date_range_x').val();
    });
});
</script>
@endsection
