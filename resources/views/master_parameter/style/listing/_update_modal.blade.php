<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="{{ route('masterparam.updateListStyle') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> UPDATE List Style</span> <!-- title -->
							</legend>
							
							<input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>
							
							  <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Style Standart:</label>
										<div class="col-lg-9">
											
											<input type="text" name="style_standart_update" id="style_standart_update" class="form-control" readonly>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Style:</label>
										<div class="col-lg-9">
											<input type="hidden" name="style_update" id="style_update" class="form-control" readonly required>
											<select data-placeholder="Select a State..." name="set_type_update" id="set_type_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($types as $item)  
													<option value="{{ $item->type_name }}">{{ $item->type_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Bisnis Unit:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="bisnis_unit_update" id="bisnis_unit_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($bisnis_unit as $item)  
													<option value="{{ $item->bisnis_unit_name }}">{{ $item->bisnis_unit_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Main Material:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="main_material_update" id="main_material_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($main_material as $item)  
													<option value="{{ $item->main_material_name }}">{{ $item->main_material_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Fabric Category:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="fabric_category_update" id="fabric_category_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($fabric_category as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Difficulty Level:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="difficulty_level_update" id="difficulty_level_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($difficulty_level as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Stripes:</label>
										<div class="col-lg-9">
											<select multiple data-placeholder="Select a State..." name="stripes_update" id="stripes_update" class="form-control select-search">
												@foreach ($stripes as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Status AD:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="status_ad_update" id="status_ad_update" class="form-control select-search">
												<option value=""></option>
												@foreach ($status_ad as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">aSC Priority:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="asc_priority_update" id="asc_priority_update" class="form-control select-search">
												<option value=""></option>
												@foreach ($asc_priority as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Update SPT Input:</label>
										<div class="col-lg-9">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" class="form-control pickadate" placeholder="Update SPT Input" id="update_spt_input_update" name="update_spt_input_update">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">season:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="season_update" id="season_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($seasons as $item)  
													<option value="{{ $item->id }}">{{ $item->season_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Product Type:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="product_type_update" id="product_type_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($product_type as $item)  
													<option value="{{ $item->product_type_name }}">{{ $item->product_type_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Product Category:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="product_category_update" id="product_category_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($product_category as $item)  
													<option value="{{ $item->product_category_name }}">{{ $item->product_category_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Desc Material:</label>
										<div class="col-lg-9">
											<textarea class="form-control" name="desc_material_update" id="desc_material_update" cols="30" rows="2"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Fabric Group:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="fabric_group_update" id="fabric_group_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($fabric_group as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Status Style:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="status_style_update" id="status_style_update" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($status_style as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">AD:</label>
										<div class="col-lg-9">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" class="form-control pickadate" placeholder="Update AD" id="ad_update" name="ad_update">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Engineering Fabric:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="engineering_fabric_update" id="engineering_fabric_update" class="form-control select-search">
												<option value=""></option>
												@foreach ($engineering_fabric as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Critical Process:</label>
										<div class="col-lg-9">
											<textarea class="form-control" name="critical_process_update" id="critical_process_update" cols="30" rows="2"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">MP:</label>
										<div class="col-lg-9">
											<input type="text" name="mp_update" id="mp_update" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Total Process:</label>
										<div class="col-lg-9">
											<input type="text" name="total_update" id="total_update" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">tsme:</label>
										<div class="col-lg-9">
											<input type="text" name="tsme_update" id="tsme_update" class="form-control text-right number2" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">tslc:</label>
										<div class="col-lg-9">
											<input type="text" name="tslc_update" id="tslc_update" class="form-control text-right number2" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Min Diameter Hemning:</label>
										<div class="col-lg-9">
											<input type="text" name="min_diameter_hemning_update" id="min_diameter_hemning_update" class="form-control text-right number2" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Max Diameter Hemning:</label>
										<div class="col-lg-9">
											<input type="text" name="max_diameter_hemning_update" id="max_diameter_hemning_update" class="form-control text-right number2" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->