<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Report Per Roll</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="style_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="articleno_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="po_buyer_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" width="100%" id="detailTable">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>No Roll</th>
                                <th>PO Supplier</th>
								<th>Material</th>
								<th>Color</th>
								<th>Actual LOT</th>
								<th>Actual Width</th>
								<th>Qty Prepared</th>
								<th>Qty Sisa</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="cutting_date_detail" id="cutting_date_detail" />
				<input type="hidden" name="style_detail" id="style_detail" />
				<input type="hidden" name="articleno_detail" id="articleno_detail" />
				<input type="hidden" name="po_buyer_detail" id="po_buyer_detail" value="[]" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
