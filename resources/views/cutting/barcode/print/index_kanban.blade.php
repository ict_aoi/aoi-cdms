<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }

    @media print {
        footer {page-break-after: always;}
    }

    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }

    .print-friendly {
        //height: 30%;
        width: 98.5%;
        line-height: 0.8px;
        padding-top: 6px;
        margin-left: -5px;
        //margin-right: -5px;
        font-size: 12px;
        border-right: thick solid #000;
        border-width: 2px;
        margin-right: 1em;
    }

    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }

    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 5.13px;
    }

    table.print-friendly2 tr td .content, table.print-friendly2 tr th .content, {
        page-break-inside: avoid;
    }

    .barcode2 {
        float: left;
        margin-bottom: 5px;
        line-height: 13px;
        position:absolute;
        top: -21.5em;
        /* SETTING PADDING BARCODE CDMS NILAI SEMAKIN KECIL SEMAKIN KE KIRI*/
        padding-left: 5px;
        font-family: 'impact';
    }

    .barcode {
        float: left;
        margin-bottom: 9px;
        line-height: 13px;
        position:absolute;
        top: -3.5em;
        padding-left: 220px;
        font-family: 'impact';
    }

    .img_barcode {
        display: block;
        padding: 1px;
    }

    .img_barcode > img {
        width: 130px;
        height: 25px;
    }
    .img_barcode2 {
        display: block;
        padding: 0px;
    }

    .img_barcode2 > img {
        width: 180px;
        height: 25px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 12px !important;
    }

    .area_barcode {
        width: 25%;
    }

    p{
        line-height: 0.5;
    }

    .left-half {
        position: absolute;
        left: 0px;
        padding-top: 15px;
        margin-left: -20px;
        width: 50%;
    }

    .right-half {
        position: absolute;
        padding-top: 15px;
        right: -10px;
        width: 50%;
    }

    //
    div {
        margin:0;
        padding:0;
    }
    /*table {
        border-collapse:collapse;
        margin:0;
        padding:0;
    }*/

    table.outer {
        border:1px dotted #333;
    }
    table,table tr {
        padding:0;
        margin:0;
    }
    table tr td {
        /*border:1px solid #000;*/
        padding:0;
        margin:0;
    }
    td.main {
        padding:0;
        margin:0;
        width:100mm;/*100mm;*/
        height:74mm;/*74mm;*/
    }
    tr.bold td {
        font-weight:bold;
    }

    td.col-1 {
        width:8mm;
    }
    td.col-2 {
        width:23mm;
    }
    td.col-3 {
        width:20mm;
    }
    td.col-4 {
        width:15mm;
    }
    td.col-5 {
        width:34mm;
    }

    td.box-1 {
        width:10mm;
    }
    td.box-2 {
        width:10mm;
    }
    td.box-3 {
        //width:8mm;
        width:0.5rem;
    }
    td.box-4 {
        width:76mm;
    }
    td.box-x {
        width:10px;
    }

    .smallfont {
        font-size:0.9em;
    }
    .evensmallerfont {
        font-size:0.6em;
    }

    .center {
        text-align:center;
    }
    .left {
        text-align:left;
    }
    .right {
        text-align:right;
    }

    td.bc-cell {
        position:relative;
        padding:2mm;
    }

    tr.box, tr td.box {
        font-weight:bold;
    }
    td.box-title {
        font-size:1.5em;
    }
    td.small-font {
        font-size:0.9em;
    }
    td.med-large-font {
        font-size:1.5em;
    }
    td.med-large-font-2 {
        font-size:1.3em;

    }
    table.box-table {
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
    }
    table.box-table, table.box-table tr.content td{
        border:1px solid #333;
    }
    table.box-table tr.content td {
        padding:0 0.5em;
    }
    table.box-table tr.tr-title {
        border:1px solid #333;
        vertical-align: top;
    }
    table.box-table tr.box-header {
        vertical-align: bottom;
    }
    table.box-table tr td.bot-cel {
        border-bottom: 1pt solid #333;
    }
</style>

    @if(isset($data))
        @if(count($data) == 1)
        @php
            $proname = $data[0]->process_name != null ? ' ('.$data[0]->process_name.')' : '';

            $komponen_name = $data[0]->komponen_name;
            $color_name = $data[0]->color_name;

            if(strlen($komponen_name.$proname)>30){
                $spn = '<span style="font-size: 10px;">'.$komponen_name.$proname;
            }else{
                $spn = '<span style="font-size: 14px;">'.$komponen_name.$proname;
            }

            if(strlen($color_name)>30){
                if(strlen($color_name)>45){
                    $spn_color = '<span style="font-size: 10px;">'.substr($color_name,0,45).' ..';
                }else{
                    $spn_color = '<span style="font-size: 10px;">'.$color_name;
                }
            }else{
                $spn_color = '<span style="font-size: 14px;">'.$color_name;
            }
        @endphp
        <table class="print-friendly" style="width: 24%; float: left; padding-left: -10px;">

            <tr>
                <td style="width: 5.2rem;">CUT</td>
                <td colspan="2">: {{ $data[0]->cut_num }}</td>
            </tr>
            <tr>
                <td>KOMPONEN</td>
                <td colspan="2">: {{ $spn }}</td>
            </tr>
            <tr>
                <td>PART</td>
                <td style="width: 4.5rem;">: {{ $data[0]->part }}</td>
                <td align="right"><b style="padding-right: 45px;">{{ $data[0]->total.'X' }}</b></td>
            </tr>
            <tr>
                <td>STYLE</td>
                <td colspan="2">: {{ $data[0]->style_edit }}</td>
            </tr>
            <tr>
                <td>SO</td>
                <td colspan="2">: @php echo $spn_color; @endphp</td>
            </tr>
            <tr>
                <td>COLL</td>
                <td colspan="2">: {{ $data[0]->color_edit }}</td>
            </tr>
            <tr>
                <td>ARTICLE</td>
                <td colspan="2">: {{ $data[0]->article_edit }}</td>
            </tr>
            <tr>
                <td>LOT</td>
                <td colspan="2">: {{ $data[0]->lot }}</td>
            </tr>
            <tr>
                <td>SIZE</td>
                <td colspan="2">: {{ $data[0]->size_edit }}</td>
            </tr>
            <tr>
                <td>QTY</td>
                <td colspan="2">: {{ $data[0]->qty }}</td>
            </tr>
            <tr>
                <td>NO STICKER</td>
                <td colspan="2">: {{ $data[0]->sticker_no }}</td>
            </tr>
            <tr>
                <td>TGL</td>
                <td>: {{ \Carbon\Carbon::parse($data[0]->cutting_date)->format('d/m') }}</td>
            </tr>
            <tr>
                <td>BUNDLE</td>
                <td>: {{ $data[0]->bundle }}</td>
            </tr>
            <tr>
                <td>QC</td>
                <td>: {{ $data[0]->qc }}</td>
            </tr>
            <tr>
                <td>CUTTER</td>
                <td>: {{ $data[0]->cutter }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="barcode">
                        <div class="img_barcode">
                            <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data[0]->barcode_id, 'C128') }}" alt="barcode"   />
                        </div>
                        <span class="barcode_number">{{ isset($data[0]) ? $data[0]->barcode_id : null }}</span >
                    </div>
                </td>
            </tr>
        </table>
        @else
            @foreach($data as $key => $value)
            @php
                $proname = $value->process_name != null ? ' ('.$value->process_name.')' : '';

                $komponen_name = $value->komponen_name;
                $color_name = $value->color_name;

                if(strlen($komponen_name.$proname)>30){
                    $spn = '<span style="font-size: 9px;">'.$komponen_name.$proname;
                }else{
                    $spn = '<span style="font-size: 14px;">'.$komponen_name.$proname;
                }

                if(strlen($color_name)>30){
                    if(strlen($color_name)>45){
                        $spn_color = '<span style="font-size: 10px;">'.substr($color_name,0,45).' ..';
                    }else{
                        $spn_color = '<span style="font-size: 10px;">'.$color_name;
                    }
                }else{
                    $spn_color = '<span style="font-size: 14px;">'.$color_name;
                }

                $type_name = $value->type_name != 'Non' ? $value->type_name : '';
            @endphp

            <table class="print-friendly" style="width: 25.6%;float: left; padding-left: -10px; padding-right: -10px;">

                <tr>
                    <td></td>
                    <td></td>
                    <!-- <td align="right"><b style="padding-right: 65px;">{{ \Carbon\Carbon::parse($data_plan->cutting_date)->format('d F') }}</b></td> -->
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    @php
                        if($value->is_recycle == 'Y'){
                            echo '<td align="right"><b style="padding-right: 65px;">RECYC/'.$value->factory_alias.'</b></td>';
                        }
                        else{
                            echo '<td align="right"><b style="padding-right: 65px;">'.$value->factory_alias.'</b></td>';
                        }
                    @endphp
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width:3rem;"><b>CUT</b></td>
                    <td style="width: 7.2rem;" align=""><b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value->cut_num }}</b></td>
                </tr>
                <tr>
                    <td style="width:3rem;"><b>DELV</b></td>
                    <td><b>: {{ \Carbon\Carbon::parse($value->statistical_date)->format('d-M-y') }}</b></td>
                </tr>
                <tr>
                    <td><b>KOMPONEN</b></td>
                    <td colspan="2"><b>: @php echo $spn; @endphp </b></td>
                </tr>
                <tr>
                    <td><b>PART</b></td>
                    <td style="width: 7.2rem;"><b>: {{ $value->part }}</b></td>

                </tr>
                <tr>
                    <td>JOB</td>
                    <td colspan="2">: {{ $value->job }}</td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $value->style.$type_name }} ({{$value->season}})</td>
                </tr>
                <tr>
                    <td><b>PO/SO</b></td>
                    <td colspan="2"><b>: {{ $value->poreference.'/'.$value->destination }}</b></td>
                </tr>
                <tr>
                    <td>ARTICLE</td>
                    <td colspan="2">: {{ $value->article }}</td>
                </tr>
                <tr>
                    <td>COLL</td>
                    <td colspan="2">: @php echo $spn_color; @endphp</td>
                </tr>
                <tr>
                    <td>SIZE</td>
                    <td colspan="2">: {{ $value->size }}</td>
                </tr>
                <tr>
                    <td>QTY</td>
                    <td colspan="2">: {{ $value->qty }}</td>
                </tr>
                <tr>
                    <td>NO STICKER</td>
                    <td colspan="2">: {{ $value->start_no.' - '.$value->end_no }}</td>
                </tr>
                <tr>
                    <td>StartDate</td>
                    <td colspan="2">: {{ \Carbon\Carbon::parse($value->created_at)->format('d F Y') }}</td>
                </tr>
                <tr>
                    <td>EndDate</td>
                    <td>: ........................</td>
                </tr>
                <tr>
                    <td>StartTime</td>
                    <td>: {{ \Carbon\Carbon::parse($value->created_at)->format('H:i') }}</td>
                </tr>
                @if ($value->sds_barcode != null)
                <tr>
                    <td colspan="3">
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->sds_barcode, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->sds_barcode : null }}</span >
                        </div>
                    </td>
                </tr>
                @else
                    <tr>
                        <td colspan="3">
                            <div class="barcode">
                                <div class="img_barcode">

                                </div>
                                <span class="barcode_number">{{ isset($value) ? $value->sds_barcode : null }}</span >
                            </div>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td colspan="3">
                        <div class="barcode2">
                            <div class="img_barcode2">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>
            </table>
            @if(($key + 1) % 4 == 0 )
            <hr style="clear: left; margin: -0.1em; border-width: 2px; margin-left: -300%;  margin-right: -300%;" />
            @endif

            @if($key == (count($data)-1))
                @if ($factories->komponen_on_bundle == 0)
                    <div class="page-break"></div>
                @else
                    <hr style="clear: left; margin: -0.1em; border-width: 2px; margin-left: -300%;  margin-right: -300%;" />
                @endif
            @endif

            @endforeach

            @php
                $count=0;
                $i=0;
                $num=0;
                $sisa_kosong = 4;
                $page_break = 8;
                $margin = ($count_komponen*15)+134;

                $pagenum = 1;

                if($factories->komponen_on_bundle == 0){
                    $pagenum = 2;
                }
            @endphp

            @foreach ($chunk_new as $key1 => $item)
                @php
                    if(($key1+1) % 2 == 0) {
                        $pss = 'right';
                    } else {
                        $pss = 'left';
                    }

                    $type_name = $list_sticker_new[$i]['type_name'] != 'Non' ? $list_sticker_new[$i]['type_name'] : '';
                @endphp
                <!-- CHECKLIST -->
                <div class="{{$pss}}-half">
                    <table class="box-table" style="border-collapse:collapse;margin:0;padding:0;">
                        <tr class='tr-title'>
                            <td colspan='5' class='box small-font'>PT. APPAREL ONE INDONESIA</td>
                            <td colspan='5' class='box box-title'>CHECKLIST KOMPONEN</td>
                        </tr>
                        <tr>
                            <td colspan='10'></td>
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>STYLE</td>
                            <td colspan='2' class='box med-large-font bot-cel' width="12rem">:{{$list_sticker_new[$i]['style'].$type_name}}</td>
                            <td></td>
                            <td colspan='4' class="box med-large-font bot-cel">No. Stiker : {{$list_sticker_new[$i]['start_no'].'-'.$list_sticker_new[$i]['end_no']}}</td>
                            <td></td>
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>COLOUR</td>
                            <td colspan='2' class='box {{ strlen(trim($list_sticker_new[$i]['color_name'])) > 26 ? NULL : "med-large-font"}} bot-cel'>{{$list_sticker_new[$i]['color_name']}}</td>
                            <td></td>
                            <td colspan='4' class="box med-large-font bot-cel">
                                @php
                                    if($list_sticker_new[$i]['is_recycle'] == 'Y'){
                                        echo '<b>RECYCLE</b>';
                                    }
                                    else{
                                        echo '';
                                    }
                                @endphp
                            </td>
                            <td></td>
                            {{-- <td colspan='6'></td> --}}
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>PO</td>
                            <td colspan='2' class='box med-large-font bot-cel'>{{$list_sticker_new[$i]['poreference']}}/{{$list_sticker_new[$i]['destination']}}</td>
                            <td colspan='6'></td>
                        </tr>
                        <tr class='center content'>
                            <td colspan='4'></td>
                            <td colspan='3' class='box med-large-font-2'>CUTTING</td>
                            <td colspan='3' class='box med-large-font-2'>DISTRIBUSI</td>
                        </tr>
                        <tr class='center content box'>
                            <td class='box-1'>SIZE</td>
                            <td class='box-2'>CUT</td>
                            <td class='' width="2rem">QTY</td>
                            <td class='' width="10rem">KOMPONEN</td>
                            <td class='box-3'>JML</td>
                            <td class='box-1'>STATUS</td>
                            <td class='box-2'>KET</td>
                            <td class='box-3'>JML</td>
                            <td class='box-1'>STATUS</td>
                            <td class='box-2'>KET</td>
                        </tr>
                        @php
                            $j=0;
                        @endphp
                        @foreach ($item as $key2 => $item2)
                            @php
                                $process_name=trim($item2['process_name']);
                                $komponen_name=trim($item2['komponen_name']);
                                if($process_name=='' || $process_name==null){
                                    $comp=$komponen_name;
                                }else{
                                    $comp=$komponen_name." (".$process_name.")";
                                }
                                if(strlen($comp) > 27){
                                    $cls='smallfont';
                                }else{
                                    $cls='';
                                }
                            @endphp

                            <tr class="content">
                                <td class='box-1 center'>{{$item2['size']}}</td>
                                <td class='box-2 center'>{{$item2['cut_num']}}</td>
                                <td class='box-3 center'>{{$item2['qty']}}</td>
                                <td class='box-4 {{$cls}}'>{{$comp}}</td>
                                <td class='box-3'></td>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                                <td class='box-3'></td>
                                <td class='box-1'>{{$key1}}</td>
                                <td class='box-2'>{{count($chunk_new)}}</td>
                            </tr>
                        @php
                            $j++;
                        @endphp
                        @endforeach
                    </table>
                </div>
                @php
                    $i++;
                    $num++;

                @endphp
                @if(($key1+1) % 2 == 0)
                <br style="clear: left; line-height: {{$margin}}px; border-width: 0.1px;" />
                @endif

                @if ($pagenum == 1 && ($key1+1) == $factories->komponen_on_bundle && ($key1 + 1) != count($chunk_new))
                    <div class="page-break"></div>
                    @php
                        $pagenum++;
                    @endphp
                @endif

                @if ($factories->komponen_on_bundle>0)
                    @if($pagenum > 1 && (($key1-$factories->komponen_on_bundle) + 1) == $factories->checklist_komponen_break && ($key1 + 1) != count($chunk_new))
                        <div class="page-break"></div>
                        @php
                            $pagenum++;
                        @endphp
                    @endif
                @else
                    @if($pagenum > 1 && (($key1 + 1) % $factories->checklist_komponen_break == 0) && ($key1 + 1) != count($chunk_new))
                        <div class="page-break"></div>
                        @php
                            $pagenum++;
                        @endphp
                    @endif

                @endif

            @endforeach
        @endif
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
</div>
