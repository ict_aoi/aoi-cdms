<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class BisnisUnitController extends Controller
{
    //MASTER
    public function index(Request $request)
    {
        return view('master_parameter.bisnis_unit.index');
    }

    public function ajaxGetDataMasterBisnisUnit(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_bisnis_unit')
                    ->wherenull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->get();

            //datatables
            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editBisnisUnit', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteBisnisUnit', $data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //add new master
    public function addBisnisUnit(Request $request)
    {

        $this->validate($request, [
            'bisnis_unit_name' => 'required'
        ]);

        $bisnis_unit_name = trim(strtoupper($request->bisnis_unit_name));

        $data = array([
            'bisnis_unit_name' => $bisnis_unit_name,
            'created_at' => Carbon::now()
        ]);

        // cek exists
        $cekifexist = DB::table('master_bisnis_unit')
                        ->where('bisnis_unit_name', $bisnis_unit_name)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master Bisnis Unit already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table
            DB::table('master_bisnis_unit')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master
    public function editBisnisUnit($id)
    {
        $data = DB::table('master_bisnis_unit')
                        ->where('master_bisnis_unit.id', $id)
                        ->first();

        return response()->json($data, 200);
    }

    //update master
    public function updateBisnisUnit(Request $request)
    {
        $data = $request->all();
        
        $bisnis_unit_name = trim(strtoupper($data['bisnis_unit_name_update']));

        // cek exists
        $cekifexist = DB::table('master_bisnis_unit')
                        ->where('bisnis_unit_name', $bisnis_unit_name)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master bisnis unit already exists..!', 422);
        }

        try {
            DB::beginTransaction();
                DB::table('master_bisnis_unit')
                ->where('id', $data['id_update'])
                ->update([
                    'bisnis_unit_name' => $bisnis_unit_name,
                    'updated_at' => Carbon::now()
                ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }    

        return view('master_parameter.bisnis_unit.index');

    }

    //delete master
    public function deleteBisnisUnit(Request $request)
    {
        $id = $request->id;
        DB::table('master_bisnis_unit')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('master_parameter.bisnis_unit.index');
    }

    //END OF MASTER
    
}
