@extends('layouts.app',['active' => 'upload_premintaan_manual'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Upload Permintaan Marker Manual </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Preparation</li>
            <li class="active">Permintaan Marker Manual</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form class="form-horizontal">
            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <span class="input-group-btn">  
                                <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                            </span>
                            <input type="text" class="form-control pickadate" name="date_cutting" placeholder="Masukkan Tanggal Cutting" id="date_cutting">
                            <span class="input-group-btn">
                                <button class="btn btn-primary legitRipple" type="button" id="btn-filter">Pilih</button>
                            </span>
                            <span class="input-group-btn">
                                <button class="btn btn-success legitRipple" type="button" id="btn-uploadpm"><span class="icon-file-upload2"></span> Upload</button>
                            </span>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>Queue</th>
                        <th>Cutting Date</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>Color</th>
                        <th>Size Ctg.</th>
                        <th>PO Buyer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h2>Upload Request Marker || PCD : </h2>
                    </div>
                    <div class="col-lg-5">
                        <h2 id="pcd"></h2>
                    </div>
                </div>
            </div>
            <form action="{{ route('preparation.uploadxlsmanual') }}" method="POST" id="form_upload" enctype="multipart/form-data">

                <div class="modal-body">

                    @csrf

                    <input type="text" name="date_cut" id="date_cut" hidden="" >

                    <div class="form-group">
                        <div class="row pd-l-20">
                            <div class="col-md-12">
                                <label class="display-block">File:</label>
                                <input type="file" class="file-styled" name="file_upload" id="file_upload" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                <span class="help-block">Accepted formats: xls, xlsx. Max file size 2Mb</span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn-save"><i class="icon-file-upload2"></i> Upload</button>
                    </div>
                </div>
            </form>                               
        </div>
    </div>
</div>

<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="table-detail">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="150px;">Barcode Marker</th>
                                <th width="50px;">Cut</th>
                                <th width="50px;">Part</th>
                                <th>Ratio</th>
                                <th>Layer</th>
                                <th>Fabric Width</th>
                                <th>Remark</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>                   
            </div>
            <div class="modal-body" id="sizeBalance">                 
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>

<!-- modal check fabric -->

<div id="modalCheckFB"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Qty Fabric Check</span>
				</legend>
			</div>
            <div class="modal-body" id="fabricCheck">                 
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>

<!-- modal check alokasi -->

<div id="modalCheckAlokasi"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Alokasi Pcs PO Buyer</span>
				</legend>
			</div>
            <div class="modal-body" id="alokasiCheck">                 
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
    <input type="hidden" id="plan_id_alokasi" />
</div>

<a href="{{ route('permintaanMarker.deleteCheckAlokasi') }}" id="deleteCheckAlokasiLink"></a>
@endsection

@section('page-js')
<script>
$(document).ready(function(){

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });
     
    var pcd =$('#date_cutting').val();
    var url = "{{route('preparation.ajaxGetPlan')}}";
    var tableL = $('#table-list').DataTable({
         processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
                 return $.extend({}, d, {
                     "date_cutting"         : $('#date_cutting').val(),
                 });
            }
        },
        columns: [
            {data: 'queu', name: 'queu', searchable:false, orderable:false},
            {data: 'cutting_date', name: 'cutting_date'},
            {data: 'style', name: 'style'},
            {data: 'articleno', name: 'articleno'},
            {data: 'color', name: 'color'},
            {data: 'size_category', name: 'size_category'},
            {data: 'poreference', name: 'poreference'},
            {data: 'status', name:'status', searchable:false, orderable:false},
            {data: 'check_fb', name:'check_fb', searchable:false, orderable:false}
        ]
    });

    $('#btn-filter').click(function(){
        event.preventDefault();
        var date_cutting    = $('#date_cutting').val();

        if (date_cutting=='') {
            $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url,
                data:{date_cutting:date_cutting},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    pcd = date_cutting;
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });

    $('#btn-uploadpm').click(function(){
       
        var cut_date = $('#date_cutting').val();

        if (cut_date=="") {
         
            $("#alert_warning").trigger("click", 'Filter Plan Cutting Date First !');
           return false;
        }else{
           $('#date_cut').val(cut_date);
           $('#pcd').text(cut_date);

            $('#modalUpload').modal('show');    
        }
            

    });

    $('#form_upload').submit(function(event) {
        event.preventDefault();

        //check po_number
        

        //
        var formData = new FormData($(this)[0]);
        var parameter = ['pcd'];
        var val = [$('#date_cut').val()];

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form_upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#modalUpload').modal('hide');
                loading_process();
            },
            success: function(response) {
                var data_response = response.data;
                if (data_response.status==200) {
                    myalert('success',data_response.output);
                }else{
                    myalert('danger',response);
                }
                $('#table-list').unblock();
                submitFilter(pcd);
            },
            error: function(response) {
                $('#table-list').unblock();
                 myalert('danger',response.data);
                  submitFilter(pcd);
               console.log(response);
            }
        });
    });

    $('#modalDetail').on('hidden.bs.modal', function(){
        $('#sizeBalance').html('');
    });

    $('#modalCheckFB').on('hidden.bs.modal', function(){
        $('#fabricCheck').html('');
    });

    $('#modalCheckAlokasi').on('hidden.bs.modal', function(){
        var id = $('#plan_id_alokasi').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: $('#deleteCheckAlokasiLink').attr('href'),
            data: {
                id: id
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
            }
        })
        .done(function(response) {
            $('#alokasiCheck').html('');
        });
    });
  
  function submitFilter(date){
        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url : "{{route('preparation.ajaxGetPlan')}}",
            data:{date_cutting:date},
            beforeSend:function(){
                loading_process();
            },
            success:function(response){
                $('#table-list').unblock();
                pcd = date_cutting;
                tableL.clear();
                tableL.draw();
            },
            error:function(response){
                myalert('error','eror');
            }
        });
    }
    
});

function detrat(e){
    var id_plan = $(e).data('id');
    
    tableDetail(id_plan);
    get_size_balance(id_plan);

    $('#modalDetail').modal('show');
}

function tableDetail(id_plan){
   
    console.log(id_plan);
    var tableD = $('#table-detail').DataTable({
             processing: true,
            destroy:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.ajaxGetDetRat') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "id_plan"         : id_plan,
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableD.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(1).css('max-width: 150px');
                $('td', row).eq(2).css('max-width: 50px');
                $('td', row).eq(3).css('max-width: 50px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id'},
                {data: 'cut', name: 'cut'},
                {data: 'part_no', name: 'part_no'},
                {data: 'ratio', name: 'ratio'},
                {data: 'layer', name: 'layer'},
                {data: 'fabric_width', name: 'fabric_width'},
                {data: 'remark', name: 'remark'},
                {data: 'action', name: 'action'},
            ]
        });
}

function delrat(e){
    var id_plan = $(e).data('id');
    console.log(id_plan);

     $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url : "{{route('preparation.delrat')}}",
        data:{id_plan:id_plan},
        beforeSend:function(){
            loading_process();
        },
        success:function(response){
            $('#table-list').unblock();
            var data_response = response.data;
                       
            if (data_response.status == 200) {
                $("#alert_success").trigger("click", data_response.output);
                
            }else{
                $("#alert_warning").trigger("click",data_response.output);
               
            }
            $('#btn-filter').click();
            
        },
        error:function(response){
            var data_response = response.data;
            $("#alert_warning").trigger("click",data_response.output);
        }
    });

}


function shoot(e){
    var barcode = $(e).data('barcode');
    var plan = $(e).data('plan');
    var part = $(e).data('part');

     $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url : "{{ route('preparation.shootFabricWidth') }}",
        data:{id_plan:plan,barcode_id:barcode,part_no:part},
        beforeSend:function(){
            $('#table-detail').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success:function(response){
           
            var data_response = response.data;
            if (data_response.status==200) {
                myalert('success',data_response.output);
            }else{
                myalert('danger',response);
            }
            $('#table-detail').unblock();
            tableDetail(plan);

        },
        error:function(response){
            myalert('error','eror');
        }
    });

}

function delpartno(e){

    var plan = $(e).data('plan');
    var part = $(e).data('part');
    var barcode = $(e).data('barcode');

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url : "{{ route('preparation.delpartno') }}",
        data:{
            id_plan: plan,
            part_no: part,
            barcode: barcode
        },
        beforeSend:function(){
            $('#table-detail').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success:function(response){
            $("#alert_success").trigger("click", 'Data deleted successfully!');
            $('#table-detail').unblock();
            tableDetail(plan);
        },
        error:function(response){
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            $('#table-detail').unblock();
            tableDetail(plan);
        }
    });

}

function get_size_balance(planning_id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: '/preparation/get-size-balance',
        data: {
            planning_id: planning_id,
        },
        beforeSend: function() {
            $('#table-detail').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function (response) {
            $('#table-detail').unblock();
            append_size_balance(response.data)
        },
        error: function(response) {
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
        }
    });
}

function getCheckFB(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        }
    })
    .done(function(response) {
        console.log(response);
        append_fabric_check(response.data)
        $('#modalCheckFB').modal();
    });
}

function getCheckAlokasi(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        }
    })
    .done(function(response) {
        append_alokasi_check(response);
        $('#modalCheckAlokasi').modal();
        $('#plan_id_alokasi').val(response.id)
    });
}

function append_size_balance(data) {
    console.log(data);
    $.each(data, function(index1, value1) {
        $('#sizeBalance').append($('<h5>').text('Part '+value1[0]));
        $('#sizeBalance').append($('<div>').addClass('table-responsive').append($('<table>').addClass('table table-basic').addClass('table-condensed').attr('id', 'sizeBalanceTable'+index1).append($('<tbody>'))));
        $.each(value1[1], function(index2, value2) {
            $('#sizeBalanceTable'+index1+' > tbody').append($('<tr>')
                .append($('<td>').html(value2.column1))
                .append($('<td>').html(value2.column2))
                .append($('<td>').html(value2.column3))
                .append($('<td>').html(value2.column4))
                .append($('<td>').html(value2.column5))
                .append($('<td>').html(value2.column6))
                .append($('<td>').html(value2.column7))
                .append($('<td>').html(value2.column8))
                .append($('<td>').html(value2.column9))
                .append($('<td>').html(value2.column10))
                .append($('<td>').html(value2.column11))
                .append($('<td>').html(value2.column12))
                .append($('<td>').html(value2.column13))
                .append($('<td>').html(value2.column14))
                .append($('<td>').html(value2.column15))
                .append($('<td>').html(value2.column16)));
        });
        console.log('end');
    });
}

function append_fabric_check(data) {
    console.log(data);
    $.each(data, function(index1, value1) {
        $('#fabricCheck').append($('<h5>').text('Part '+value1.part_no));

        $('#fabricCheck')
            .append($('<div>').addClass('table-responsive')
                .append($('<table>').addClass('table table-basic').addClass('table-condensed').attr('id', 'fabricWidthTable'+index1)
                    .append($('<thead>')
                        .append($('<tr>')
                            .append($('<th>').text('FABRIC WIDTH'))
                            .append($('<th>').text('FABRIC QTY'))
                        )
                    )
                    .append($('<tbody>'))
                )
            );
        
        $.each(value1.lebar, function(index2, value2) {
            $('#fabricWidthTable'+index1+' > tbody')
                .append($('<tr>')
                    .append($('<td>').html(value2.width))
                    .append($('<td>').html(value2.qty))
                );
        });

        $('#fabricCheck').append($('<br>'));

        $('#fabricCheck')
            .append($('<div>').addClass('table-responsive')
                .append($('<table>').addClass('table table-basic').addClass('table-condensed').attr('id', 'fabricCheckTable'+index1)
                    .append($('<thead>')
                        .append($('<tr>')
                            .append($('<th>').text('QTY CSI'))
                            .append($('<th>').text('QTY WHS PREPARED'))
                            .append($('<th>').text('QTY BALANCE'))
                        )
                    )
                    .append($('<tbody>')
                        .append($('<tr>')
                            .append($('<td>').html(value1.qty.csi_qty))
                            .append($('<td>').html(value1.qty.whs_qty))
                            .append($('<td>').html(value1.qty.blc_qty))
                        )    
                    )
                )
            );
        
        $('#fabricCheck').append($('<br>')).append($('<br>'));
    });
}

function append_alokasi_check(data) {
    $('#alokasiCheck').append($('<div>').addClass('table-responsive')
        .append($('<table>').addClass('table table-basic table-condensed').attr('id', 'alokasiTable')
            .append($('<thead>')
                .append($('<tr>'))
            )
            .append($('<tbody>'))
        )
    );

    $.each(data[0], function(index, value) {
        $('#alokasiTable > thead > tr').append($('<th>').text(value))
    });

    $.each(data, function(index, value) {
        if(value[0] != 'PO Buyer') {
            $('#alokasiTable > tbody').append($('<tr>')
                .append($('<td>').text(value[0]))
                .append($('<td>').text(value[1]))
                .append($('<td>').text(value[2]))
            );

            $.each(value, function(index1, value1) {
                if(index1 != 0 && index1 != 1 && index1 != 2) {
                    $('#alokasiTable tr:last').append($('<td>').text(value1));
                }
            });
        }
    });
}
</script>
@endsection
