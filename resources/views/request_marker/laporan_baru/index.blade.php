@extends('layouts.app',['active' => 'laporan_baru'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
    .margin-left-10 {
        margin-right: 10px;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Laporan Baru</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Laporan Baru</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="laporanBaruTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Plan</th>
                        <th>Barcode</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>Size Type</th>
                        <th>Part No</th>
                        <th>Cut</th>
                        <th>Detail</th>
                        <th>Print</th>
                        <th><input type="checkbox" onclick="toggle(this)" id="check_all"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="panel-body">
        <input type="hidden" name="planning_id" id="planning_id" />
        <div class="form-group">
            <label class="display-block text-semibold">Choose One</label>
            <label class="radio-inline">
                <input type="radio" name="print_type" value="single" checked="checked">
                Single
            </label>

            <label class="radio-inline">
                <input type="radio" name="print_type" value="double">
                Double
            </label>
        </div>
    </div>
    <div class="panel-body">
		<button class="btn btn-primary pull-right" id="btn_print_id">Cetak Laporan</button>
		<button class="btn btn-success pull-right margin-left-10" id="btn_change_ratio">Ganti Ratio</button>
	</div>
</div>

<form action="{{ route('laporanBaru.printIdPdf') }}" method="post" target="_blank" id="get_barcode">
	@csrf
    <input id="list_id_marker" name="list_id_marker" type="hidden" value="[]">
    <input id="print_type_" name="print_type_" type="hidden">
	<div class="" style="">
		<button type="submit" class="btn hidden">Print</button>
	</div>
</form>

@endsection

@section('page-modal')
	@include('request_marker.laporan_baru._modal_detail')
@endsection

@section('page-js')
    <script src="{{ mix('js/laporan_baru.js') }}"></script>
@endsection
