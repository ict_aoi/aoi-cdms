@extends('layouts.app',['active' => 'cuttingplan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Preparation </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Preparation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-css')
<style>
    /* .add-margin-left {
        margin-left: 20px;
    } */
</style>
@endsection

@section('page-content')
 
@include('includes.notif_mo_update')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_by_lcdate">
            <label><b>Choose Date</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="date" class="form-control pickadate" name="date_cutting" id="date_cutting">
                
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <!-- <div class="col-md-1">
                <button class="btn btn-danger" id="btnSync"><i class="icon-git-compare"></i> Sync</button>
            </div> -->
                <button class="btn btn-success add-margin-left" id="sync_plan"><i class="icon-spinner9"></i> Sync | 2 Prev - 3 Next</button>
                <button class="btn btn-success add-margin-left" id="sync_plan7"><i class="icon-spinner9"></i> Sync | 2 Prev - 5 Next</button>
            <div class="pull-right">
                <form action="{{ route('preparation.downloadPrioritas') }}" method="POST">
                    @csrf
                    <input type="hidden" name="date_cutting_download" id="date_cutting_download">
                    <button type="submit" class="btn btn-primary" id="download_prioritas"><i class="icon-download"></i> Download</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Style</th>
                        <th>PO Buyer</th>
                        <th>Article</th>
                        <th>Size Ctgy</th>
                        <th>Color</th>
                        <th>Queu</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-1" >
                        <label><b>PCD</b></label>
                        <input type="text" name="pcd_md" id="pcd_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Style</b></label>
                        <input type="text" name="style_md" id="style_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <input style="margin-top: 28px;" type="text" name="top_bot_md" id="top_bot_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Article</b></label>
                        <input type="text" name="article_md" id="article_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Size Ctg</b></label>
                        <input type="text" name="size_md" id="size_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-3">
                        <label><b>Color</b></label>
                        <input type="text" name="color_md" id="color_md" class="form-control" readonly="">
                        <input type="text" name="mo_md" id="mo_md" class="hidden">
                        <input type="text" name="material_md" id="material_md" class="hidden">
                        <input type="text" name="color_code_md" id="color_code_md" class="hidden">
                        <input type="text" name="season_md" id="season_md" class="hidden">
                        <input type="text" name="gtb_md" id="gtb_md" class="hidden">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Queue</b></label>
                        <input type="text" name="queue_md" id="queue_md" class="form-control" placeholder="Queue">
                    </div>
                    <div class="col-lg-1">
                        <!-- <label style="color: white; padding-top: 10px;"><b>button</b></label> -->
                        <button class="btn btn-primary btn-xs" id="btn-save" style="margin-top: 30px;"><span class="icon-stack2"></span></button>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-list-detail">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Po Buyer</th>
                                    <th>Queu</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                </div>             
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')

<script src="{{ mix('js/notif_mo_update.js') }}"></script>

<script>
$(document).ready(function(){

    var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{route('preparation.ajaxGetPlaning')}}",
                data: function(d) {
                     return $.extend({}, d, {
                         "date_cutting"         : $('#date_cutting').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(2).css('max-width','130px');
                $('td', row).eq(4).css('max-width','80px');
                $('td', row).eq(5).css('max-width','100px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style'},
                {data: 'po_buyer', name: 'po_buyer'},
                {data: 'articleno', name: 'articleno'},
                {data: 'size_category', name: 'size_category'},
                {data: 'color_name', name: 'color_name'},
                {data: 'queu', name: 'queu',sortable: false, orderable: false, searchable: false},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

    $('#btn-filter').click(function() {
       
        event.preventDefault();
        var date_cutting    = $('#date_cutting').val();
       
        
        if (date_cutting=='') {
            $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : "{{route('preparation.ajaxGetPlaning')}}",
                data:{date_cutting:date_cutting},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();

                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
       
        
    });

    $('#table-list').on('click','.btn-delqueu',function(){
        var date = $(this).data('pcd');
        var style = $(this).data('style');
        var articleno = $(this).data('article');
        var size = $(this).data('size');
        var queu = $(this).data('queu');
        var top_bot = $(this).data('tb');

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url : "{{ route('preparation.deletedAll') }}",
                data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,top_bot:top_bot},
                beforeSend:function(){
                    loading_process();
                },            
                success:function(response){
                    
                    $("#alert_success").trigger("click", 'delete success');
                    $('#table-list').unblock();
                    $('#btn-filter').click();
                },
                eror:function(response){
                    $("#alert_warning").trigger("click",response);
                    
                    return false;
                    $('#btn-filter').click();
                }

            });

    });

    $('#table-list').on('blur','.queu_unfilled', function(){

        var date = $(this).data('date');
        var style = $(this).data('style');
        var articleno = $(this).data('article');
        var color = $(this).data('color');
        var size = $(this).data('size');
        var mo_created = $(this).data('mo_created');
        var material = $(this).data('material');
        var color_code = $(this).data('color_code');
        var top_bot = $(this).data('tb');
        var gtop_bot = $(this).data('gtb');
        var season = $(this).data('season');
        var queu = $(this).val();
        if (queu!="" || queu.trim()!="") {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : "{{ route('preparation.allIn') }}",
                data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,mo_created:mo_created,color:color,material:material,color_code:color_code,top_bot:top_bot,season:season,gtopbot:gtop_bot},
                beforeSend:function(){
                    loading_process();
                },            
                success:function(response){
                    var data_response = response.data;
                       
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);
                        
                    }else{
                        $("#alert_warning").trigger("click",data_response.output);
                       
                    }
                    $('#queu_'+style).html(response);
                    $('#btn-filter').click();
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                    $('#btn-filter').click();
                }

            });
        }
    });

    $('#table-list').on('blur','.edqueu_unfilled', function(){
        var date = $(this).data('date');
        var id = $(this).data('id');
        var queu = $(this).val();
        if (queu!="" || queu.trim()!="") {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : "{{ route('preparation.edQueu') }}",
                data: {id:id,queu:queu,date:date},
                beforeSend:function(){
                    loading_process();
                },            
                success:function(response){
                    var data_response = response.data;
                       
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);
                        
                    }else{
                        $("#alert_warning").trigger("click",data_response.output);
                       
                    }
                    $('#edqueu_'+id).html(response);
                    $('#btn-filter').click();
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                    $('#btn-filter').click();
                }

            });
        }
    });

 
    $('#btnSync').on('click', function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url : "{{ route('preparation.sync') }}", 
            beforeSend:function(){
                loading_process();
            },          
            success:function(response){
                $('#table-list').unblock();
                console.log(response);
                myalert('success',response);
            },
            eror:function(response){
                myalert('danger',response);
               
            }

        });
    });


    $('#table-list').on('click','.btn-detail',function(){
        var pcd =  $(this).data('pcd');
        var style = $(this).data('style');
        var article =  $(this).data('article');
        var color = $(this).data('color');
        var queu = $(this).data('queu');
        var ncolor = color.replace(/-/g," ");
        var size_category = $(this).data('size');
        var mo_update = $(this).data('mo');
        var material = $(this).data('material');
        var color_code = String($(this).data('color_code'));
        var top_bot = $(this).data('tb');
        var season = $(this).data('season');
        var gtopbot = $(this).data('gtb');
       
        var ncolor_code = color_code.replace(/-/g," ");
        $('#pcd_md').val(pcd);
        $('#style_md').val(style);
        $('#article_md').val(article);
        $('#color_md').val(ncolor);
        $('#size_md').val(size_category);
        $('#mo_md').val(mo_update);
        $('#material_md').val(material);
        $('#color_code_md').val(ncolor_code);
        $('#top_bot_md').val(top_bot);
        $('#season_md').val(season);
        $('#gtb_md').val(gtopbot);

        $('#modalDetail').modal('show');

        var tableLD = $('#table-list-detail').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            destroy:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.getPO') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "cutting_date"         : pcd,
                         "style" : style,
                         "articleno" : article,
                         "size_category": size_category,
                         "color": ncolor,
                         "queu": queu,
                         "mo_update":mo_update,
                         "top_bot":top_bot
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                // var info = tableL.page.info();
                // var value = index+1+info.start;
                // $('td', row).eq(0).html(value);
                $('td',row).eq(0).css('max-widht: 50px;')
                $('td',row).eq(2).css('max-widht: 150px;')
            },
            columns: [
                // {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox',sortable: false, orderable: false, searchable: false},
                {data: 'po_buyer', name: 'po_buyer'},
                {data: 'queu', name: 'queu'}
            ]
        });
    });
 

    $('#table-list-detail').on('blur','.queup_unfilled', function(){
        var tableLD = $('#table-list-detail').DataTable();
        var date = $(this).data('date');
        var style = $(this).data('style');
        var articleno = $(this).data('article');
        var color = $(this).data('color');
        var size = $(this).data('size');
        var pobuyer = $(this).data('pobuyer');
        var mo_created = $(this).data('mo_update');
        var queu = $(this).val();
        if (queu!="" || queu.trim()!="") {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : "{{ route('preparation.partialIn') }}",
                data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,mo_created:mo_created,color:color,pobuyer:pobuyer},
                beforeSend:function(){
                   $('#table-list-detail').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },            
                success:function(response){
                    $('#table-list-detail').unblock();
                    $('#queup_'+style).html(response);
                    tableLD.clear();
                    tableLD.draw();
                    
                },
                eror:function(response){
                    console.log(response);
                    return false;
                  
                }

            });
        }
    });


    $('#btn-save').on('click',function(){
            var data = [];

            var queue = $('#queue_md').val();
            var date = $('#pcd_md').val();
            var style = $('#style_md').val();
            var color = $('#color_md').val();
            var article = $('#article_md').val();
            var sizectg = $('#size_md').val();
            var mo_created = $('#mo_md').val();
            var material = $('#material_md').val();
            var color_code = $('#color_code_md').val();
            var top_bot = $('#top_bot_md').val();
            var season = $('#season_md').val();
            var gtopbot = $('#gtb_md').val();


            $('.setpo:checked').each(function(){
                data.push({
                    pobuyer :$(this).val(),
                    stat_date :$(this).data('stat'),
                    qty:$(this).data('qty')
                });
            });

            if (data.length > 0) {

                if ($('#queue_md').val()=='') {
                    $("#alert_warning").trigger("click", 'Queu cannot empty');
                    
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                });
                $.ajax({
                    type:'POST',
                    url : "{{ route('preparation.partialIn') }}",
                    data:{data:data,date:date,style:style,article:article,sizectg:sizectg,queue:queue,mo_created:mo_created,color:color,material:material,color_code:color_code,top_bot:top_bot,season:season,gtopbot:gtopbot},
                    beforeSend:function(){
                        loading_md();
                    },
                    success:function(response){

                        var data_response = response.data;
                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);
                            
                        }else{
                            $("#alert_warning").trigger("click", data_response.output);
                        }
                       $('#modalDetail').modal('hide');
                       $('#btn-filter').click();
                       $('#queue_md').val('');
                    },
                    error:function(response){
                       myalert('error', response);
                       console.log(response);
                    }
                });
            }else{
                myalert('warning','Select mo first');
            }
    });

    $('#table-list').on('click','.btn-tb',function(){
        var pcd =  $(this).data('pcd');
        var style = $(this).data('style');
        var article =  $(this).data('article');
        var size  = $(this).data('size');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
                type:'post',
                url : "{{ route('preparation.splitTopBot') }}",
                data: {pcd:pcd,style:style,article:article,size_category:size},
                beforeSend:function(){
                    loading_process();
                },            
                success:function(response){
                    var data_response = response.data;
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);
                       
                    }else{
                        $("#alert_warning").trigger("click", data_response.output);
                        
                    }
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                    
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                    
                }

            });
        

    });



    $('#table-list').on('click','.btn-marge',function(){
        var pcd =  $(this).data('pcd');
        var style = $(this).data('style');
        var article =  $(this).data('article');
        var size  = $(this).data('size');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
                type:'post',
                url : "{{ route('preparation.margeTopBot') }}",
                data: {pcd:pcd,style:style,article:article,size_category:size},
                beforeSend:function(){
                    loading_process();
                },            
                success:function(response){
                    var data_response = response.data;
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);
                        
                    }else{
                        $("#alert_warning").trigger("click", data_response.output);
                       
                    }
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                    
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                    
                }

            });
        

    });

    $('#sync_plan').on('click', function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url : "{{ route('preparation.dailySync') }}", 
            beforeSend:function(){
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },          
            success:function(response){
                $.unblockUI();
                console.log(response);
                myalert('success',response);
            },
            eror:function(response){
                myalert('danger',response);
               
            }

        });
    });

    $('#sync_plan7').on('click', function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url : "{{ route('preparation.dailySync7') }}", 
            beforeSend:function(){
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },          
            success:function(response){
                $.unblockUI();
                console.log(response);
                myalert('success',response);
            },
            eror:function(response){
                myalert('danger',response);
               
            }

        });
    });

    $('#date_cutting').change(function() {
        var d = new Date($(this).val());
        $('#date_cutting_download').val($(this).val());
    });  


});



function delplan(e){
    var _token = $("input[name='_token]").val();
    var pcd = e.getAttribute('data-date');
    var style = e.getAttribute('data-style');
    var article = e.getAttribute('data-article');
    var queu = e.getAttribute('data-queu');
    var po_buyer = e.getAttribute('data-po');
     var tableLD = $('#table-list-detail').DataTable();
    bootbox.confirm("Are you sure delete this ?",
        function(result){

            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                        url:"{{ route('preparation.delpart') }}",
                        type: "POST",
                        data:{
                            "pcd": pcd,
                            "style": style,
                            "article": article,
                            "queu": queu,
                            "po_buyer":po_buyer,                           
                            "_token": _token,
                        },
                        success: function(response){    
                            $("#alert_success").trigger("click", 'Success Deleted');
                           
                            tableLD.clear();
                            tableLD.draw();
                        },
                        error: function(response) {
                            console.log("error");
                        }
                    });

            }
        });
}




    




</script>
@endsection
