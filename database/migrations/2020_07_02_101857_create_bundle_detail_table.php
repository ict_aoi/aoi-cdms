<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundle_detail', function (Blueprint $table) {
            $table->string('barcode_id', 100)->primary();
            $table->char('bundle_header_id', 36)->nullable();
            $table->integer('style_detail_id')->nullable();
            $table->string('komponen_name', 100)->nullable();
            $table->integer('start_no')->nullable();
            $table->integer('end_no')->nullable();
            $table->integer('qty')->nullable();
            $table->string('location')->nullable();
            $table->integer('factory_id')->nullable();
            $table->string('sds_barcode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_detail');
    }
}
