<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PackinglistDistribusi extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'packinglist_distribusi';
	protected $fillable = [
		'no_packinglist',
		'kk_id',
		'no_kk',
		'style',
		'po_buyer',
		'subcont',
		'remark',
		'created_by',
		'created_at',
		'factory_id',
		'updated_by',
		'updated_at',
		'deleted_by',
		'deleted_at',
		'process',
		'document_type',
		'pabean_no',
		'aju_no',
		'aju_date',
		'user_id_exim',
		'updated_at_exim',
		'status',
		'subcont_id',
		'released_by',
		'released_at',
		'is_completed',
		'packinglist_type',
		'is_completed',
		'set_type',
		'document_no',
		'print_at',
		'printed_by'
	];

	protected $dates = ['created_at','updated_at'];
}

