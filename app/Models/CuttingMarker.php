<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CuttingMarker extends Model
{
    protected $dates = ['created_at', 'updated_at'];
	protected $table = 'cutting_marker';
    protected $fillable = ['barcode_id','id_plan','part_no','cut','color','layer','fabric_width','fab_cons','perimeter','marker_length','over_consum','approved','deleted_at','qty_cut','item_id','bay_pass','is_new','user_updated_id','ratio_updated_at','combine_id','is_body','downtime','keterangan','downtime_category_id','old_marker','desc','layer_plan','set_type', 'second_plan', 'retio_print', 'po_print', 'suggest_cut','noted'];
    
    public function cutting_plan()
    {
        return $this->belongsTo('App\Models\CuttingPlan', 'id_plan', 'id');
    }

    public function rasio_markers()
    {
        return $this->hasMany('App\Models\RasioMarker', 'id_marker', 'barcode_id')->orderByRaw('length(size) asc, size asc');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_updated_id', 'id');
    }

    public function scan_cutting()
    {
        return $this->hasOne('App\Models\ScanCutting', 'barcode_marker', 'barcode_id');
    }

    public function fabric_used()
    {
        return $this->hasOne('App\Models\Spreading\FabricUsed', 'barcode_marker', 'barcode_id');
    }
}
