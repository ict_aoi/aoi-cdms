@extends('layouts.app',['active' => 'reportPpa2'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan KITT</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan KITT</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
        <form action="{{action('Report\KittController@ppa2')}}" method="post" id="form-tab">
            @csrf
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active" data-value="ppa1"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">PPA</a></li>
                    <li class="" data-value="ppa2"><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">PPA 2</a></li>
                </ul>

                <div class="tab-content">
                        <div class="form-group">
                            <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="all">All</label>
                            <label class="radio-inline"><input type="radio" name="radio_status" value="season">Filter by Season</label>
                        </div>
                        <hr>
                        <fieldset class="content-group hidden" id="dvSeason">
                            <div class="form-group">
                                <label class="control-label col-md-1 text-bold">Season</label>
                                <div class="col-md-6">
                                    <select data-placeholder="Select a State..." name="season" id="season" class="form-control select-search">
                                        <option value=""></option>
                                        @foreach($master_seasons as $list)
                                            <option value="{{ $list->id }}">{{ $list->season_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <input type="hidden" class="form-control" id="tab_active" name="tab_active" value="ppa1">
                        <div class="tab-pane active" id="basic-tab1">
                                <fieldset class="content-group">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-primary" id="btn-export">Export KITT PPA</button>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>

                        <div class="tab-pane" id="basic-tab2">
                            <fieldset class="content-group">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">Export KITT PPA2</button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                </div>
            </div>
        </form>
        
	</div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var url_export = $('#exportData').attr('href');

    $('.nav-tabs').on('shown.bs.tab', function (e) {
        var tab_active = $(".nav-tabs .active").attr("data-value");

        $('#tab_active').val(tab_active).trigger('change');
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'all') {
            $('#dvSeason').addClass('hidden');
        }
        else if (this.value == 'season') {
            $('#dvSeason').removeClass('hidden');
        }

    });
    
});
</script>
@endsection