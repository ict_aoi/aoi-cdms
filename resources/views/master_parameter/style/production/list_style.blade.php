<div class="table-responsive">
    <table class="table" id="table-style">
        <thead>
            <tr>
                <th>#</th>
                <th>Season</th>
                <th>Style</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 0;
            @endphp
            @foreach($data as $key => $val)
            @php
                $no++;
            @endphp
                <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $val->season_name }}</td>
                    <td>{{ $val->style }}</td>
                    <td>
                        <button type="button" id="choose-style"
                                data-season="{{ $val->season }}"
                                data-seasonname="{{ $val->season_name }}"
                                data-style="{{ $val->style }}"
                                class="btn btn-default">
                                SELECT
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    </div>
    <script type="text/javascript">
        var tables = $('#table-style').DataTable({  
            "lengthChange": false,
            "pageLength": 5,
            "columnDefs": [{
                "targets" : [0],
                "visible" : false,
                "searchable" : false
            }]
        });
    </script>
    