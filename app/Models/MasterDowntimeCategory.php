<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDowntimeCategory extends Model
{
	protected $table = 'master_downtime_category';
	protected $fillable = ['category_name','deleted_at'];
}
