<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;

class SavingfabricController extends Controller
{
    public function index(){
    	return view('report.saving_fabric_ssp.index');
    }

    public function exportDataSaving(Request $req){
    	$date_range = explode('-', preg_replace('/\s+/', '',$req->date));
    	$from = date_format(date_create($date_range[0]),'Y-m-d');
    	$to = date_format(date_create($date_range[1]),'Y-m-d');
    	$factory = Auth::user()->factory_id;
    	$data = DB::table('jaz_balance_marker')
                        ->select('jaz_balance_marker.cutting_date','jaz_balance_marker.season','jaz_balance_marker.style','jaz_balance_marker.po_buyer','jaz_balance_marker.articleno','jaz_balance_marker.item_code','jaz_balance_marker.part_no','jaz_balance_marker.factory_id','jaz_balance_marker.supply_whs','jaz_balance_marker.actual_marker_prod','jaz_balance_marker.balance','users.name')
                        ->leftJoin('users','users.id','=','jaz_balance_marker.release_by')
    					->whereBetween('jaz_balance_marker.cutting_date',[$from,$to])
    					->where('jaz_balance_marker.factory_id',$factory)
    					->get();
    	$filename = "AOI".$factory."_Report_Saving_Fabric_".$from."-".$to;
    	return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){

                    $sheet->setCellValue('A1','Cutting Plan');
                    $sheet->setCellValue('B1','Season');
                    $sheet->setCellValue('C1','Style');
					$sheet->setCellValue('D1','PO Buyer');
					$sheet->setCellValue('E1','Article No');
					$sheet->setCellValue('F1','Item Code');
					$sheet->setCellValue('G1','Part No');
					$sheet->setCellValue('H1','Factory');
					$sheet->setCellValue('I1','Supply Warehouse');
					$sheet->setCellValue('J1','Actual Production');
					$sheet->setCellValue('K1','Balance');
					$sheet->setCellValue('L1','Relased By');
					$i = 1;
					foreach ($data as $key => $vl) {
						$rw = $i+1;
						$sheet->setCellValue('A'.$rw,$vl->cutting_date);
						$sheet->setCellValue('B'.$rw,$vl->season);
						$sheet->setCellValue('C'.$rw,$vl->style);
						$sheet->setCellValue('D'.$rw,$vl->po_buyer);
						$sheet->setCellValue('E'.$rw,$vl->articleno);
						$sheet->setCellValue('F'.$rw,$vl->item_code);
						$sheet->setCellValue('G'.$rw,$vl->part_no);
						$sheet->setCellValue('H'.$rw,"AOI ".$vl->factory_id);
						$sheet->setCellValue('I'.$rw,$vl->supply_whs);
						$sheet->setCellValue('J'.$rw,$vl->actual_marker_prod);
						$sheet->setCellValue('K'.$rw,$vl->balance);
						$sheet->setCellValue('L'.$rw,$vl->name);
						$i++;
					}
                });
                $excel->setActiveSheetIndex(0);
          })->export('xlsx');
    }
}
