<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($print))
                @if($model->status == 'draft')

                @else
                    <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
                    <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
                @endif
            @endif
            @if (isset($delete))
                    
            @endif
            @if (isset($edit))
                @if($model->status == 'draft')
                    <li><a href="#" onclick="edit('{!! $edit !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
                    <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
                @endif
            @endif
            @if (isset($release))
                @if($model->status == 'draft')
                    <li><a href="#" onclick="release('{!! $release !!}')" ><i class="icon-magic-wand"></i> Release</a></li>
                @endif
            @endif
        </ul>
    </li>
</ul>
