@extends('layouts.app',['active' => 'dashboard'])

@section('page-css')
<style>
    .hilang {
        display: none;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">QC Endline QR Code</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">QC Endline QR Code</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <a href="{{ route('infoFIR.indexQR', $id) }}" class="btn btn-primary hilang" id="btn-choose-check-fir">Check FIR</a>
            <a href="{{ route('Qcheck.indexQR', $id) }}" class="btn btn-primary hilang" id="btn-choose-input-endline-qc">Input Hasil QC Endline</a>
        </div>
        <input type="hidden" name="data_id" id="data_id" value="{{ $id }}" />
    </div>
</div>
@endsection

@section('page-js')
<script>
    $(document).ready(function(){
        var state_qr = localStorage.getItem('qr_scan_state');
        if(state_qr == 'endline-qc') {
            $('#btn-choose-check-fir').removeClass('hilang');
            $('#btn-choose-input-endline-qc').removeClass('hilang');
        }
    });
</script>
@endsection
