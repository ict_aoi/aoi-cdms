<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'erp_live' => [
            'driver'   => 'pgsql',
            'host'     => '192.168.51.123',
            'port'     => '5432',
            'database' => 'aimsdbc',
            'username' => 'adempiere',
            'password' => 'Becarefulwithme',
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'adempiere',
            'sslmode'  => 'prefer',
        ],

        'wms_live' => [
            'driver'         => 'pgsql',
            'host'           => '192.168.51.22',
            'port'           => '5432',
            'database'       => 'wmsacc',
            'username'       => 'dev',
            'password'       => 'Pass@123',
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
        ],

        'line_live' => [
            'driver'         => 'pgsql',
            'host'           => '192.168.51.8',
            'port'           => '5432',
            'database'       => 'line_system',
            'username'       => 'postgres',
            'password'       => 'Becarefulwithme',
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
        ],

        'sds_live' => [
            'driver'         => 'pgsql',
            'host'           => '192.168.51.32',
            'port'           => '5432',
            'database'       => 'smart_dist',
            'username'       => 'smartdist',
            'password'       => 'Pass@1234',
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
        ],

        'sds_dev' => [
            'driver'         => 'pgsql',
            'host'           => '192.168.51.35',
            'port'           => '5432',
            'database'       => 'smart_dist2',
            'username'       => 'smartdist',
            'password'       => 'Pass@1234',
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => 'public',
            'sslmode'        => 'prefer',
        ],

        'mysql' => [
            'driver'         => 'mysql',
            'host'           => env('DB_HOST', '127.0.0.1'),
            'port'           => env('DB_PORT', '3306'),
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'unix_socket'    => env('DB_SOCKET', ''),
            'charset'        => 'utf8mb4',
            'collation'      => 'utf8mb4_unicode_ci',
            'prefix'         => '',
            'prefix_indexes' => true,
            'strict'         => true,
            'engine'         => null,
        ],

        'pgsql' => [
            'driver'         => 'pgsql',
            'host'           => env('DB_HOST', '127.0.0.1'),
            'port'           => env('DB_PORT', '5432'),
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
            'schema'         => env('DB_SCHEMA', 'public'),
            'sslmode'        => 'prefer',
        ],

        'sqlsrv' => [
            'driver'         => 'sqlsrv',
            'host'           => env('DB_HOST', 'localhost'),
            'port'           => env('DB_PORT', '1433'),
            'database'       => env('DB_DATABASE', 'forge'),
            'username'       => env('DB_USERNAME', 'forge'),
            'password'       => env('DB_PASSWORD', ''),
            'charset'        => 'utf8',
            'prefix'         => '',
            'prefix_indexes' => true,
        ],

        'lab' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.15',
            'port' => '5432',
            'database' => 'labdev',
            'username' => 'dev',
            'password' => 'Pass@123',
            'charset' => 'utf8',
            'prefix' => '',
            'sslmode' => 'prefer',
            'schema' => 'public'
        ],
        'absence_aoi' => [
            'driver' => 'pgsql',
            'host' => '192.168.15.56',
            'port' => '5432',
            'database' => 'absensi_aoi',
            'username' => 'absensi',
            'password' => 'Absensi12345',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' =>'public',
            'sslmode' => 'prefer',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];
