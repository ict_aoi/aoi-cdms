<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MarkerDetailPO extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'ratio_detail';
    protected $fillable = ['barcode_id','plan_id','ratio_id','po_buyer','size','qty','deleted_at','is_sisa','part_no'];
    
    public function rasio_marker()
    {
        return $this->belongsTo('App\Models\RasioMarker', 'ratio_id', 'id');
    }
}
