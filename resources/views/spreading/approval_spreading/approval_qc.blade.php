@extends('layouts.app',['active' => 'approval_spreading_gl'])
<!-- @section('page-css')
<style type="text/css">
.hidden_id{
    display: none;
}
</style>
@endsection -->
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Approval Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Approval Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-horizontal">
            <fieldset class="content-group">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-bold">Spreading Date</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                            </span>
                            <input type="date" class="form-control pickadate" name="spread" id="spread">

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
        	<table class="table table-basic table-condensed" id="table-list" width="100%">
        		<thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">Plan</th>
                        <th rowspan="2">Style</th>
                        <th rowspan="2">Article</th>
                        <th rowspan="2">Marker ID</th>
                        <th rowspan="2">Part No</th>
                        <th rowspan="2">Cut</th>
                        <th rowspan="2">Pemakaian Fabric</th>
                        <th colspan="2"><center>Approval</center></th>
                        <th rowspan="2">Table</th>
                        <th rowspan="2">Operator</th>
                        <th rowspan="2">Action</th>
                    </tr>
                    <tr>
                        <th>GL</th>
                        <th>QC</th>
                    </tr>
        		</thead>
        	</table>
        </div>
    </div>
</div>
@endsection


@section('page-modal')
<div id="modalApprove" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Spreading Approval</span>
				</legend>
			</div>
			<div class="modal-body">
                <div class="row form-group">
                	<div class="table-responsive">
						<table class="table table-basic table-condensed">
                            <tr>
                                <td><strong>Plan</strong></td>
                                <td>: <span id="plan">Data Empty!</span></td>
                                <td><strong>Marker Code</strong></td>
                                <td>: <span id="barcode_head">Data Empty!</span></td>
                                <td><strong>Operator</strong></td>
                                <td>: <span id="operator">Data Empty!</span></td>
                                <td><strong>Table</strong></td>
                                <td>: <span id="table">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td>: <span id="style">Data Empty!</span></td>
                                <td><strong>Part No</strong></td>
                                <td>: <span id="part">Data Empty!</span></td>
                                <td><strong>Qty Layer</strong></td>
                                <td>: <span id="qty">Data Empty!</span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><strong>Color</strong></td>
                                <td>: <span id="color">Data Empty!</span></td>
                                <td><strong>Item</strong></td>
                                <td>: <span id="item">Data Empty!</span></td>
                                <td><strong>Marker Length</strong></td>
                                <td>: <span id="m_length">Data Empty!</span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><strong>PO Buyer</strong></td>
                                <td colspan="3">: <span id="po">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td>: <span id="ratio">Data Empty!</span></td>
                                <td><strong>Marker Width</strong></td>
                                <td>: <span id="m_width">Data Empty!</span></td>
                                {{-- <td></td>
                                <td></td> --}}
                            </tr>
                            <tr>
                                <td><strong>Actual Length</strong></td>
                                <td><input type="number" name="a_length" id="a_length" class="form-control" placeholder="marker length"></td>
                                <td><strong>Actual Width</strong></td>
                                <td><input type="number" name="a_width" id="a_width" class="form-control" placeholder="marker width"></td>
                                <td><strong>Spreading High</strong></td>
                                <td><input type="text" name="s_high" id="s_high" class="form-control" placeholder="spreading high"></td>
                                <td><strong>Remark</strong></td>
                                <td>
                                    <input type="text" name="remark_note" id="remark_note" class="form-control" placeholder="Remark">
                                    <input type="text" name="barcode_m" id="barcode_m" class="form-control hidden">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row form-group">
                	<div class="table-responsive">
						<table class="table table-basic table-condensed" id="inputTable">
							<thead>
                                <tr>
                                    <th>ID</th>
                                    <th>No. Roll</th>
                                    <th>Panjang Roll</th>
                                    <th>Lebar Roll</th>
                                    <th>Lot</th>
                                    <th>Point Fir</th>
                                    <th>Short Roll</th>
                                    <th>Bau Odor</th>
                                    <th>Panjang Aktual</th>
                                    <th>Lebar Aktual</th>
                                    <th>Sambungan Awal</th>
                                    <th>Sambungan Akhir</th>
                                    <th>Panjang Jeblos</th>
                                    <th>Lebar Jeblos</th>
                                    <th>Tidak Rapi</th>
                                    <th>Terlalu Kencang</th>
                                    <th>Terlalu Kendur</th>
                                    <th>Sambungan</th>
                                </tr>
	                        </thead>
						</table>
					</div>
                </div>
			</div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" id="btn-close">Close</button>
                <button type="button" class="btn bg-orange" id="btn-save">Save</button>
                <button type="button" class="btn btn-success" id="btn-approve" data-status="approve"  onclick="setStatus(this);">Approve</button>
                <button type="button" class="btn btn-warning" id="btn-hold" data-status="escalation" onclick="setStatus(this);">Eskalasi</button>

            </div>
		</div>
	</div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    // var commit = $('#is_commit').val();
    // if(commit == 1){
    //     $('#btn-approve').removeClass('hidden');
    //     $('#btn-save').addClass('hidden');
    // }else{
    //     $('#btn-save').removeClass('hidden');
    //     $('#btn-approve').addClass('hidden');
    // }

    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        scrollX: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('ApprovalSpreadingQC.getApprovalSpreading') }}",
            data: function(d) {
                 return $.extend({}, d, {
                     "spread"         : $('#spread').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'cutting_date', name: 'cutting_date'},
            {data: 'style', name: 'style'},
            {data: 'articleno', name: 'articleno'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'part_no', name: 'part_no'},
            {data: 'cut', name: 'cut'},
            {data: 'actual_use', name: 'actual_use'},
            {data: 'gl_status', name: 'gl_status'},
            {data: 'qc_status', name: 'qc_status'},
            {data: 'table_name', name: 'table_name'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
    });

    $('#btn-filter').on('click',function(){
        var spread = $('#spread').val();
        if (spread.trim()!='') {
            $.ajaxSetup({
                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
            });
            $.ajax({
                type:'get',
                url : "{{ route('ApprovalSpreadingQC.getApprovalSpreading') }}",
                data:{spread:spread},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                   myalert('error', response);
                   console.log(response);
                }
            });
        }else{
            myalert('eror','Select Planning Date First');
        }
    });

    $('#btn-close').on('click',function(){
        $('#btn-approve').removeClass('hidden');
            $('#btn-hold').removeClass('hidden');
            $('#modalApprove').modal('hide');
    });


    $('#btn-save').on('click',function(){
        var data = Array();

        $('#inputTable tbody tr').each(function(i,v){
            data[i]=Array();
            $(this).children('td').each(function(ii,vv){
                    if (ii==6 || ii==0 || ii==7) {
                        data[i][ii]=$(this).find('input').val();
                    }
                    if (ii>=12) {
                        if ($(this).find('#checkbox').is(':checked')) {
                                data[i][ii]=1;
                        }else{
                                data[i][ii]=0;
                        }
                    }
            });


        });

        var barcode = $('#barcode_m').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url : "{{ route('ApprovalSpreadingQC.ajaxSaveQcTemp') }}",
            data:{data:data,barcode:barcode},
            beforeSend:function(){
                $('#modalApprove').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success:function(response){
                $('#modalApprove').unblock();
                var data_response = response.data;
                if (data_response.status == 200) {
                    myalert('success', data_response.output);
                }else{
                    myalert('error', data_response.output);
                }
                // $('#btn-approve').removeClass('hidden');
                // $('#btn-hold').removeClass('hidden');
                $('#modalApprove').modal('hide');
                $('#table-list').DataTable().ajax.reload();

            },
            error:function(response){
            myalert('error', response);
            console.log(response);
            }
        });

    });
});

function detail(e) {
    var barcode = e.getAttribute('data-id');
    var tableI = $('#inputTable').DataTable();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'get',
        url : "{{ route('ApprovalSpreadingQC.ajaxGetSpreadingApv') }}",
        data:{barcode:barcode},
        beforeSend:function(){
           $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
        },
        success:function(response){

            var data = response.data;

            $('#plan').text(data.plan);
            $('#style').text(data.style);
            $('#article').text(data.article);
            $('#part').text(data.part);
            $('#color').text(data.color);
            $('#m_width').text(data.marker_width);
            $('#m_length').text(data.marker_length);
            $('#btn-approve').attr('data-id',data.barcode_id);
            $('#btn-hold').attr('data-id',data.barcode_id);
            $('#barcode_head').text(data.barcode_id);
            $('#operator').text(data.name);
            $('#table').text(data.table);
            $('#po').text(data.po);
            $('#ratio').text(data.ratio);
            $('#item').text(data.item_code);
            $('#qty').text(data.qty_ratio);
            $('#barcode_m').val(data.barcode_id);
            $('#a_length').val(data.a_length);
            $('#a_width').val(data.a_width);
            $('#s_high').val(data.s_high);
            $('#remark_note').val(data.remark_note);
            if (data.qc_status=='approve') {
                $('#btn-approve').addClass('hidden');
                $('#btn-hold').addClass('hidden');
                $('#btn-save').addClass('hidden');
            }else if(data.qc_status=='escalation'){
                $('#btn-hold').addClass('hidden');
            }
            else{
                if(data.is_commit == 1){
                    $('#btn-approve').removeClass('hidden');
                    $('#btn-save').addClass('hidden');
                    $('#btn-hold').removeClass('hidden');
                }else{
                    $('#btn-save').removeClass('hidden');
                    $('#btn-approve').addClass('hidden');
                    $('#btn-hold').addClass('hidden');
                }
            }

            $.unblockUI();
            $('#modalApprove').modal('show');

            tableI.clear();
            tableFabricSpreading(data.barcode_id);

        },
        error:function(response){
           myalert('error', response);
           console.log(response);
        }
    });


}

function tableFabricSpreading(barcode_id){

    $('#inputTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            type: 'get',
            url: "{{ route('ApprovalSpreadingQC.ajaxFabricQcSpreading') }}",
            data: {
                barcode: barcode_id,
            }
        },
        columnDefs: [
            {
                className: 'dt-center',
            },
            {
                  className: 'text-center',
            },
        ],
        columns: [
            {data: 'id', name: 'id', searchable:true, visible:true, orderable:false},
            {data: 'no_roll', name: 'no_roll', searchable:true, visible:true, orderable:false},
            {data: 'qty_fabric', name: 'qty_fabric', searchable:true, visible:true, orderable:false},
            {data: 'actual_width', name: 'actual_width', searchable:true, visible:true, orderable:false},
            {data: 'lot', name: 'lot', searchable:true, visible:true, orderable:false},
            {data: 'point_fir', name: 'point_fir', searchable:true, visible:true, orderable:false},
            {data: 'short_roll', name: 'short_roll', searchable:true, visible:true, orderable:false},
            {data: 'bau', name: 'bau', searchable:true, visible:true, orderable:false},
            {data: 'l_actual', name: 'suplai_sisa', searchable:true, visible:true, orderable:false},
            {data: 'w_actual', name: 'actual_sisa', searchable:true, visible:true, orderable:false},
            {data: 'sambungan', name: 'sambungan', searchable:true, visible:true, orderable:false},
            {data: 'sambungan_end', name: 'sambungan_end', searchable:true, visible:true, orderable:false},
            {data: 'l_jeblos', name: 'akumulasi_layer', searchable:true, visible:true, orderable:false},
            {data: 'w_jeblos', name: 'reject', searchable:true, visible:true, orderable:false},
            {data: 'rapi', name: 'rapi', searchable:true, visible:true, orderable:false},
            {data: 'kencang', name: 'kencang', searchable:true, visible:true, orderable:false},
            {data: 'kendur', name: 'kendur', searchable:true, visible:true, orderable:false},
            {data: 'sambungan_qc', name: 'sambungan_qc', searchable:true, visible:true, orderable:false},
        ]
    });
}

function setStatus(e){
    var data = Array();


    $('#inputTable tbody tr').each(function(i,v){
        data[i]=Array();
        $(this).children('td').each(function(ii,vv){
                if (ii==6 || ii==0 || ii==7) {
                    data[i][ii]=$(this).find('input').val();
                }
                if (ii>=12) {
                   if ($(this).find('#checkbox').is(':checked')) {
                        data[i][ii]=1;
                   }else{
                        data[i][ii]=0;
                   }
                }
        });


    });

    var a_length = $('#a_length').val();
    var a_width = $('#a_width').val();
    var s_high = $('#s_high').val();
    var remark_note = $('#remark_note').val();
    var barcode = $('#barcode_m').val();
    var status = e.getAttribute('data-status');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'post',
        url : "{{ route('ApprovalSpreadingQC.ajaxApproveQc') }}",
        data:{data:data,a_length:a_length,a_width:a_width,s_high:s_high,remark_note,remark_note,barcode:barcode,status:status},
        beforeSend:function(){
           $('#modalApprove').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
        },
        success:function(response){
            $('#modalApprove').unblock();
            var data_response = response.data;
            if (data_response.status == 200) {
                myalert('success', data_response.output);
            }else{
                myalert('error', data_response.output);
            }
            $('#btn-approve').removeClass('hidden');
            $('#btn-hold').removeClass('hidden');
            $('#modalApprove').modal('hide');
            $('#table-list').DataTable().ajax.reload();

        },
        error:function(response){
           myalert('error', response);
           console.log(response);
        }
    });

}


</script>
@endsection
