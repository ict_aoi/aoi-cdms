<?php

namespace App\Http\Controllers\Adibooster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Config;
use StdClass;
use File;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Models\Data\DataCutting;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\CuttingMarker;
use App\Models\MarkerReleaseHeader;
use App\Models\MarkerReleaseDetail;
use App\Models\Data\DataSewing;
class DashboardController extends Controller
{
    public function preparation($factory){
         $today = Carbon::now()->format('Y-m-d');

    	$tomorrow = CuttingPlan::select('cutting_date')
    						->where('cutting_date','>',$today)
    						->groupBy('cutting_date')
    						->orderBy('cutting_date','asc')
    						->first();
    	return view('adibooster/preparation')->with('factory_id',$factory)
    											->with('today_show',$this->convert_hari($today).', '.Carbon::parse($today)->format('d-m-Y'))
    											->with('tomorrow_show',$this->convert_hari($tomorrow->cutting_date).', '.Carbon::parse($tomorrow->cutting_date)->format('d-m-Y'))
    											->with('today',$today)
    											->with('tomorrow',$tomorrow->cutting_date);
    }

    private function convert_hari($date) {
        $day = Carbon::parse($date)->format('w');

        if($day == '0') {
            return 'Minggu';
        } elseif($day == '1') {
            return 'Senin';
        } elseif($day == '2') {
            return 'Selasa';
        } elseif($day == '3') {
            return 'Rabu';
        } elseif($day == '4') {
            return 'Kamis';
        } elseif($day == '5') {
            return 'Jumat';
        } elseif($day == '6') {
            return 'Sabtu';
        }
    }

    public function today(Request $request){
    	$date          = $request->today;
    	$factory_id    = $request->factory_id;
    	$data          = array();
    	$data_planning = DB::table('hs_cutting_plan_new_update_2')->orderBy('queu', 'asc')->where('cutting_date', $date)->where('factory_id', $factory_id)->get();

        foreach($data_planning as $a) {

            $marker            = '';
            $fabric            = '';
            $over_consum_check = 0;

            // Set size category
            if($a->size_category == 'J') {
                $size_ctg = 'Japan';
            } elseif($a->size_category == 'A') {
                $size_ctg = 'Asian';
            } else {
                $size_ctg = 'Inter';
            }

            if($a->queu === null){

                $po_buyer_array = DB::table('data_cuttings')->select('po_buyer')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                $po_buyer          = "'" . implode("','",$po_buyer_array) . "'";;
                $marker         = 'Not Request Yet';

                $data_uom_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->first()->uom;
                $data_cons     = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->sum('fbc');

                if($data_uom_cons == 'M') {
                    $data_cons = $data_cons * 1.0936;
                }

                $total_need_qty       = 0;
                $data_fabric_query    = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_by_planning_f('$a->cutting_date') WHERE style='$a->style' AND article_no ='$a->articleno' AND po_buyer IN($po_buyer)"));

                if(count($data_fabric_prepared) == 0){
                    $fabric = 'Not Prepare Yet';
                }
                else{
                    $data_fabric_prepared = $data_fabric_query[0]->qty_prepared;
                    $data_balance         = $data_fabric_prepared - $data_cons;
                    $balance_over_consum  = $data_fabric_prepared - $total_need_qty;

                    if($data_balance < 0) {
                        $percentage = ($data_fabric_prepared / $data_cons)*100;
                        // $fabric = 'On Progress '.number_format($percentage,2).'%';
                        if(number_format($percentage,2) >= 100){
                            $fabric = 'Done';
                        }
                        else{
                            $fabric = 'On Progress '.number_format($percentage,2).'%';
                        }
                        // $fabric = 'On Progress';
                    } else {
                        if($balance_over_consum < 0) {
                            // $fabric = 'Waiting For MM Approval';
                            $fabric = 'Done';
                        } else {
                           $fabric = 'Done';
                        }
                    }
                }

                $data[] = [
                    'queu'       => $a->queu,
                    'style'      => $a->style." - ".$a->articleno." - ".$size_ctg,
                    'color_name' => substr($a->color_name, 0, 12),
                    'marker'     => $marker,
                    'fabric'     => $fabric,
                ];

            }
            else{
                if($a->queu == '0'){

                }
                else{
                    $data_check = CuttingPlan::where('id', $a->plan_id)->where('deleted_at', null)->first();

                    if($data_check->header_id != null && $data_check->is_header) {
                        $get_cutting_date = CuttingPlan::where('header_id', $data_check->id)
                            ->whereNull('deleted_at')
                            ->pluck('cutting_date')
                            ->toArray();

                        $get_plan_id = CuttingPlan::where('header_id', $data_check->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        $po_buyer_array = DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray();
                    }else{
                        $get_cutting_date[] = $data_check->cutting_date;

                        $po_buyer_array = $data_check->po_details->pluck('po_buyer')->toArray();
                    }

                    // set status marker
                    $data_uom_cons = DB::table('data_cuttings')->where('plan_id', $data_check->id)->first()->uom;
                    $data_cons     = DB::table('data_cuttings')->where('plan_id', $data_check->id)->sum('fbc');

                    if($data_uom_cons == 'M') {
                        $data_cons = $data_cons  * 1.0936;
                    }

                    $data_marker_all      = CuttingMarker::where('id_plan', $data_check->id)->where('deleted_at', null)->get();

                    if(count($data_marker_all) == 0){
                        $marker = 'Not Request Yet';
                    }
                    else{

                        $part_marker = CuttingMarker::where('id_plan',$data_check->id)->where('deleted_at', null)->distinct('part_no')->pluck('part_no')->toArray();
                        $part_csi    = DataCutting::where('plan_id',$data_check->id)->where('deleted_at', null)->distinct('part_no')->count('part_no');

                        $part = array();
                        foreach($part_marker as $value){
                            $part_no = explode('+', $value);
                            foreach($part_no as $x){
                                array_push($part, $x);
                            }
                        }

                        // dd($part_csi, count($part), $part_marker);
                        if($part_csi != count($part)){
                            $marker = 'On Progress';
                        }
                        else{

                            $data_marker          = CuttingMarker::where('id_plan', $data_check->id)->whereNull('marker_length')->whereNull('perimeter')->where('deleted_at', null)->count();
                            if($data_marker > 0){
                                $marker = 'On Progress';
                            }
                            else{
                                $marker = 'Done';
                            }
                        }

                    }

                    // set status fabric
                    //COUNT PART ON WMS PREPARED
                    $cutting_date = "'".implode("','",$get_cutting_date)."'";
                    $style = $a->style;
                    $article = $a->articleno;
                    $po = "'".implode("','",$po_buyer_array)."'";

                    $roll_part = DB::connection('wms_live_new')->select(DB::raw("SELECT count(part_no) as part_no FROM get_data_lebar_f(array[$cutting_date]) WHERE style='$style' AND article_no ='$article' AND po_buyer in($po)"));

                    //COUNT PART ON CSI
                    $part_count = DB::table('data_cuttings')->select('part_no')
                    ->where('cutting_date', $a->cutting_date)
                    ->where('style', 'like', '%'.$a->style.'%')
                    ->where('articleno', $a->articleno)
                    ->groupBy('part_no')->pluck('part_no')
                    ->flatten()
                    ->unique()
                    ->count();

                    $data_fabric_query = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM get_data_lebar_f(array[$cutting_date]) WHERE style='$style' AND article_no ='$article' AND po_buyer in($po)"));

                    $roll_part_c = count($roll_part) == 0 ? 0 : $roll_part[0]->part_no;
                    if(count($data_fabric_query) == 0){
                        $fabric = 'Not Prepare Yet';
                    }else{
                        $data_fabric_prepared = $data_fabric_query[0]->qty_prepared;
                        $total_need_qty      = CuttingMarker::where('id_plan', $data_check->id)->where('deleted_at')->sum('need_total');
                        $data_balance        = $data_fabric_prepared - $data_cons;
                        $balance_over_consum = $data_fabric_prepared - $total_need_qty;

                            if($data_balance < 0) {
                                if($total_need_qty != null || $total_need_qty != 0){
                                    $percentage = ($data_fabric_prepared / $total_need_qty)*100;
                                    // dd($data_fabric_prepared, $total_need_qty, $percentage);
                                }
                                else{
                                    $percentage = ($data_fabric_prepared / $data_cons)*100;
                                }

                                if(number_format($percentage,2) >= 100){
                                    $fabric = 'Done';
                                }
                                else{
                                    $fabric = 'On Progress '.number_format($percentage,2).'%';
                                }
                            } else {
                                if($roll_part_c < $part_count){
                                    $fabric = 'Missing Part';
                                }else{
                                if($balance_over_consum < 0) {
                                    // $fabric = 'Waiting For MM Approval';
                                    $fabric = 'Done';
                                } else {
                                   $fabric = 'Done';
                                }
                            }
                            }

                    }

                    $data[] = [
                        'queu'       => $a->queu,
                        'style'      => $a->style." - ".$a->articleno." - ".$size_ctg,
                        'color_name' => substr($a->color_name, 0, 12),
                        'marker'     => $marker,
                        'fabric'     => $fabric,
                    ];
                }

            }
        }
        return response()->json($data,200);
    }

    public function tomorrow(Request $request){
    	$date          = $request->tomorrow;
    	$factory_id    = $request->factory_id;
    	$data          = array();
    	$data_planning = DB::table('hs_cutting_plan_new_update_2')->orderBy('queu', 'asc')->where('cutting_date', $date)->where('factory_id', $factory_id)->get();

        foreach($data_planning as $a) {

            $marker            = '';
            $fabric            = '';
            $over_consum_check = 0;

            // Set size category
            if($a->size_category == 'J') {
                $size_ctg = 'Japan';
            } elseif($a->size_category == 'A') {
                $size_ctg = 'Asian';
            } else {
                $size_ctg = 'Inter';
            }

            if($a->queu === null){

                $po_buyer_array = DB::table('data_cuttings')->select('po_buyer')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                $po_buyer          = "'" . implode("','",$po_buyer_array) . "'";;
                $marker         = 'Not Request Yet';

                $data_uom_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->first()->uom;
                $data_cons     = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->where('size_category', $a->size_category)->sum('fbc');

                if($data_uom_cons == 'M') {
                    $data_cons = $data_cons * 1.0936;
                }

                $total_need_qty       = 0;
                $data_fabric_query    = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$a->cutting_date') WHERE style='$a->style' AND article_no ='$a->articleno' AND po_buyer IN($po_buyer)"));
                $data_fabric_prepared = count($data_fabric_query) == 0 ? 0 :$data_fabric_query[0]->qty_prepared;

                if($data_fabric_prepared == null || $data_fabric_prepared == 0){
                    $fabric = 'Not Prepare Yet';
                }
                else{

                    $data_balance         = $data_fabric_prepared - $data_cons;
                    $balance_over_consum  = $data_fabric_prepared - $total_need_qty;

                    if($data_balance < 0) {
                        $percentage = ($data_fabric_prepared / $data_cons)*100;
                        // $fabric = 'On Progress '.number_format($percentage,2).'%';
                        if(number_format($percentage,2) >= 100){
                            $fabric = 'Done';
                        }
                        else{
                            $fabric = 'On Progress '.number_format($percentage,2).'%';
                        }
                    } else {
                        if($balance_over_consum < 0) {
                            // $fabric = 'Waiting For MM Approval';
                            $fabric = 'Done';
                        } else {
                            $fabric = 'Done';
                        }
                    }
                }

                $data[] = [
                    'queu'       => $a->queu,
                    'style'      => $a->style." - ".$a->articleno." - ".$size_ctg,
                    'color_name' => substr($a->color_name, 0, 12),
                    'marker'     => $marker,
                    'fabric'     => $fabric,
                ];

            }
            else{
                if($a->queu == '0'){

                }
                else{
                    $data_check = CuttingPlan::where('id', $a->plan_id)->where('deleted_at', null)->first();

                    if($data_check->header_id != null && $data_check->is_header) {
                        $get_cutting_date = CuttingPlan::where('header_id', $data_check->id)
                            ->whereNull('deleted_at')
                            ->pluck('cutting_date')
                            ->toArray();

                        $get_plan_id = CuttingPlan::where('header_id', $data_check->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        $po_buyer_array = DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray();
                    }else{
                        $get_cutting_date[] = $data_check->cutting_date;

                        $po_buyer_array = $data_check->po_details->pluck('po_buyer')->toArray();
                    }
                    $po_buyer          = "'" . implode("','",$po_buyer_array) . "'";;

                    // set status marker
                    $data_uom_cons = DB::table('data_cuttings')->where('plan_id', $data_check->id)->first()->uom;
                    $data_cons     = DB::table('data_cuttings')->where('plan_id', $data_check->id)->sum('fbc');

                    if($data_uom_cons == 'M') {
                        $data_cons = $data_cons  * 1.0936;
                    }

                    $data_marker_all      = CuttingMarker::where('id_plan', $data_check->id)->where('deleted_at', null)->get();

                    if(count($data_marker_all) == 0){
                        $marker = 'Not Request Yet';
                    }
                    else{
                        $part_marker = CuttingMarker::where('id_plan',$data_check->id)->where('deleted_at', null)->distinct('part_no')->pluck('part_no')->toArray();
                        $part_csi    = DataCutting::where('plan_id',$data_check->id)->where('deleted_at', null)->distinct('part_no')->count('part_no');

                        $part = array();
                        foreach($part_marker as $value){
                            $part_no = explode('+', $value);
                            foreach($part_no as $x){
                                array_push($part, $x);
                            }
                        }

                        // dd($part_csi, count($part), $part_marker);
                        if($part_csi != count($part)){
                            $marker = 'On Progress';
                        }
                        else{
                            $data_marker          = CuttingMarker::where('id_plan', $data_check->id)->whereNull('marker_length')->whereNull('perimeter')->where('deleted_at', null)->count();
                            if($data_marker > 0){
                                $marker = 'On Progress';
                            }
                            else{
                                $marker = 'Done';
                            }
                        }

                    }

                    // set status fabric
                    //COUNT PART WMS PREPARED
                    // $roll_part   = DB::connection('wms_live_new')->table('integration_whs_to_cutting')
                    // ->select('part_no')
                    // ->whereIn('planning_date', $get_cutting_date)
                    // ->where('style', $a->style)
                    // ->where('article_no', $a->articleno)
                    // ->whereIn('po_buyer', $po_buyer_array)
                    // ->distinct()
                    // ->count('part_no');

                    $cutting_date = "'".implode("','",$get_cutting_date)."'";
                    $style = $a->style;
                    $article = $a->articleno;
                    $po = "'".implode("','",$po_buyer_array)."'";

                    $roll_part = DB::connection('wms_live_new')->select(DB::raw("SELECT count(part_no) as part_no FROM get_data_lebar_f(array[$cutting_date]) WHERE style='$style' AND article_no ='$article' AND po_buyer in($po)"));
                    //COUNT PART ON CSI
                    $part_count = DB::table('data_cuttings')->select('part_no')
                    ->where('cutting_date', $a->cutting_date)
                    ->where('style', 'like', '%'.$a->style.'%')
                    ->where('articleno', $a->articleno)
                    ->groupBy('part_no')->pluck('part_no')
                    ->flatten()
                    ->unique()
                    ->count();

                    // $data_fabric_query    = DB::connection('wms_live_new')->table('integration_whs_to_cutting')->whereIn('planning_date', $get_cutting_date)->where('style', $a->style)->where('article_no', $a->articleno)->whereIn('po_buyer', $po_buyer_array);

                    $data_fabric_query = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM get_data_lebar_f(array[$cutting_date]) WHERE style='$style' AND article_no ='$article' AND po_buyer in($po)"));


                    if(count($data_fabric_query) == 0){
                        $fabric = 'Not Prepare Yet';
                    }
                    else{
                        $data_fabric_prepared = $data_fabric_query[0]->qty_prepared;
                        $total_need_qty      = CuttingMarker::where('id_plan', $data_check->id)->where('deleted_at')->sum('need_total');
                        $data_balance        = $data_fabric_prepared - $data_cons;
                        $balance_over_consum = $data_fabric_prepared - $total_need_qty;

                        if($data_balance < 0) {
                            if($total_need_qty != null || $total_need_qty != 0){
                                $percentage = ($data_fabric_prepared / $total_need_qty)*100;
                            }
                            else{
                                $percentage = ($data_fabric_prepared / $data_cons)*100;
                            }
                            if(number_format($percentage,2) >= 100){
                                $fabric = 'Done';
                            }
                            else{
                                $fabric = 'On Progress '.number_format($percentage,2).'%';
                            }
                            // $fabric = 'On Progress';
                        } else {
                            if($roll_part < $part_count){
                                $fabric = 'Missing Part';
                            }else{
                            if($balance_over_consum < 0) {
                                // $fabric = 'Waiting For MM Approval';
                                $fabric = 'Done';
                            } else {
                                $fabric = 'Done';
                            }
                        }
                        }
                    }

                    $data[] = [
                        'queu'       => $a->queu,
                        'style'      => $a->style." - ".$a->articleno." - ".$size_ctg,
                        'color_name' => substr($a->color_name, 0, 12),
                        'marker'     => $marker,
                        'fabric'     => $fabric,
                    ];
                }

            }
        }
        return response()->json($data,200);
    }


   //  public function spreading($factory){
   //  	$now = Carbon::now()->format('Y-m-d');
   //  	$getdata = DB::table('v_ns_dash_spreading')->where('factory_id',$factory)->where('date_spreading',$now)->get();
   //  	$dash = [];
   //  	foreach ($getdata as $gd) {
   //  		$data ='{name: "'.$gd->table_name.'",y:'.$gd->spreading.',drilldown:"'.$gd->table_name.'"},';
			// $dash[]=$data;

   //  	}


   //  	return view('adibooster/spreading',['factory_id'=>$factory,'dash'=>$dash]);
   //  }

    public function spreading($factory){
        $sprd  = [];
        $trgt  = [];
        $trgt2 = [];
        $shift = '';
        // $table = DB::table('master_table')->select('table_name','id_table')->where('factory_id',$factory)->whereNull('deleted_at')->orderBy('id_table','asc')->get();
        // $now = Carbon::now();
        // $timenow = date("H:i:s",strtotime(date($now)));

        $current_time = Carbon::now();

        $get_shift = DB::table('master_shifts')->where('factory_id', $factory)->where('deleted_at', null)->where('shift', '1')->first();

        if($get_shift->start_time <= Carbon::now()->format('H:i:s')) {
            if($get_shift->end_time >= Carbon::now()->format('H:i:s')) {
                $get_time = DB::table('master_shifts')->where('factory_id', $factory)->where('deleted_at', null)->where('shift', '1')->first();
            } else {
                $get_time = DB::table('master_shifts')->where('factory_id', $factory)->where('deleted_at', null)->where('shift', '2')->first();
            }
        } else {
            $get_time = DB::table('master_shifts')->where('factory_id', $factory)->where('deleted_at', null)->where('shift', '2')->first();
        }

        if($get_time->shift == '1') {
            $table = DB::table('master_table')
                ->join('ns_target_daily','master_table.id_table','ns_target_daily.id_table')
                ->whereDate('ns_target_daily.date_spreading',$current_time)
                ->where('ns_target_daily.factory_id',$factory)
                ->where('ns_target_daily.shift', $get_time->shift)
                ->groupBy('master_table.id_table')
                ->groupBy('master_table.table_name')
                ->select('master_table.table_name','master_table.id_table')
                ->get();
        } else {
            if(Carbon::now()->format('H:i:s') <= $get_time->start_time) {
                $table = DB::table('master_table')
                    ->join('ns_target_daily','master_table.id_table','ns_target_daily.id_table')

                    ->whereBetween('ns_target_daily.date_spreading', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)])
                    // ->whereBetween('ns_target_daily.date_spreading', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)->subDay(1)])
                    ->where('ns_target_daily.factory_id',$factory)
                    ->where('ns_target_daily.shift', $get_time->shift)
                    ->groupBy('master_table.id_table')
                    ->groupBy('master_table.table_name')
                    ->select('master_table.table_name','master_table.id_table')
                    ->get();
            } else {
                $table = DB::table('master_table')
                    ->join('ns_target_daily','master_table.id_table','ns_target_daily.id_table')
                    // ->whereBetween('ns_target_daily.date_spreading', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)->addDay(1)])
                    ->whereBetween('ns_target_daily.date_spreading', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)->addDay(1)])
                    ->where('ns_target_daily.factory_id',$factory)
                    ->where('ns_target_daily.shift', $get_time->shift)
                    ->groupBy('master_table.id_table')
                    ->groupBy('master_table.table_name')
                    ->select('master_table.table_name','master_table.id_table')
                    ->get();
            }
        }

        foreach ($table as $tb) {
            if($get_time->shift == '1') {
                $spr = DB::table('jaz_union_spreading_fabric')
                            ->where('id_table', $tb->id_table)
                            ->where('factory_id', $factory)
                            ->whereBetween('dates',[Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)])->sum('actual_use');
            } else {
                if($current_time->toTimeString() <= $get_time->start_time) {
                    $spr = DB::table('jaz_union_spreading_fabric')
                                ->where('id_table', $tb->id_table)
                                ->where('factory_id', $factory)
                                ->whereBetween('dates', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)])->sum('actual_use');
                } else {
                    $spr = DB::table('jaz_union_spreading_fabric')
                                ->where('id_table', $tb->id_table)
                                ->where('factory_id', $factory)
                                ->whereBetween('dates', [Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time), Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->end_time)->addDay(1)])->sum('actual_use');
                }
            }

            if ($spr != null) {
                $sprd[] = round($spr, 2);
            }else{
                $sprd[] = 0;
            }

            //cek istirahat
            if($get_time->shift == '1') {
                //masih istirahat
                if(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time) < Carbon::now() && Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) > Carbon::now()) {
                    $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time))->format('%H');
                //selesai istirahat
                } elseif (Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) <= Carbon::now()) {
                    $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::now()->subMinutes($get_time->break_duration))->format('%H');
                } else {
                    $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::now())->format('%H');
                }
            }
            else{
                if($current_time->toTimeString() <= $get_time->start_time) {
                    if(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time) < Carbon::now() && Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) > Carbon::now()) {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1)->diff(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time))->format('%H');
                    //selesai istirahat
                    } elseif (Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) <= Carbon::now()) {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1)->diff(Carbon::now()->subMinutes($get_time->break_duration))->format('%H');
                    } else {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->subDay(1)->diff(Carbon::now())->format('%H');
                    }
                }
                else{
                    if(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time) < Carbon::now() && Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) > Carbon::now()) {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time))->format('%H');
                    //selesai istirahat
                    } elseif (Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->break_time)->addMinute($get_time->break_duration) <= Carbon::now()) {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::now()->subMinutes($get_time->break_duration))->format('%H');
                    } else {
                        $selisih = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_time->start_time)->diff(Carbon::now())->format('%H');
                    }
                }

            }

            $selisih2 = $selisih;
            $selisih  = $selisih + 1;

            // data target

            if($get_time->shift == '1') {
                $trg = DB::table('ns_target_daily')->where('id_table',$tb->id_table)->where('factory_id', $factory)->whereDate('date_spreading', $current_time)->where('shift',$get_time->shift)->first();
                if ($trg!=null) {
                    $trgt[]  = $trg->target * $selisih;
                    $trgt2[] = $trg->target * $selisih2;
                }else{
                    $trgt[]  = 0;
                    $trgt2[] = 0;
                }
            } else {
                if($current_time->toTimeString() <= $get_time->start_time) {
                    $trg = DB::table('ns_target_daily')->where('id_table',$tb->id_table)->where('factory_id', $factory)->whereDate('date_spreading', Carbon::now()->subDay(1))->where('shift',$get_time->shift)->first();
                    if ($trg!=null) {
                        $trgt[]  = $trg->target * $selisih;
                        $trgt2[] = $trg->target * $selisih2;
                    }else{
                        $trgt[]  = 0;
                        $trgt2[] = 0;
                    }
                } else {
                    $trg = DB::table('ns_target_daily')->where('id_table',$tb->id_table)->where('factory_id', $factory)->whereDate('date_spreading', $current_time)->first();
                    if ($trg!=null) {
                        $trgt[]  = $trg->target * $selisih;
                        $trgt2[] = $trg->target * $selisih2;
                    }else{
                        $trgt[]  = 0;
                        $trgt2[] = 0;
                    }
                }
            }


        }
        return view('adibooster/spreading2',[
            'factory_id' => $factory,
            'table'      => $table,
            'spread'     => $sprd,
            'target'     => $trgt,
            'target2'    => $trgt2,
            'shift'      => $get_time->shift]
        );
    }

    // dashboard bundling

    public function bundling($factory_id)
    {
        return view('adibooster.bundling', compact('factory_id'));
    }

    public function getDataCutting(Request $request)
    {
        if(request()->ajax())
        {
            $factory_id = $request->factory_id;

            // condition
            // 1 = shift 1
            // 2 = shift 1 lembur
            // 3 = jeda shift 1 / shift 1 lembur dengan shift 2
            // 4 = shift 2
            // 5 = shift 2 lembur

            $condition = 0;

            $current_time = Carbon::now();

            $get_shift1 = DB::connection('sds_live')->table('master_shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '1')->first();

            $get_start_shift1_ = explode(':', $get_shift1->start_time);

            $limit_cut_off = Carbon::now();
            $limit_cut_off->hour = $get_start_shift1_[0];
            $limit_cut_off->minute = $get_start_shift1_[1];
            $limit_cut_off->second = $get_start_shift1_[2];

            // condition time

            // jika waktu dari jam 00 - mulai shift 1
            if(Carbon::now() <= $limit_cut_off) {
                $condition = 2;
                $start_time_shift1 = Carbon::parse(Carbon::now()->subDays(1)->format('Y-m-d').' '.$get_shift1->start_time)->format('Y-m-d H:i:s');
                $end_time_shift1 = Carbon::parse(Carbon::now()->subDays(1)->format('Y-m-d').' '.$get_shift1->end_time)->format('Y-m-d H:i:s');
                $break_time_shift1 = Carbon::parse(Carbon::now()->subDays(1)->format('Y-m-d').' '.$get_shift1->break_time)->format('Y-m-d H:i:s');

                $get_shift2 = DB::connection('sds_live')->table('master_shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '2')->first();

                // jika ada shift 2
                if($get_shift2) {

                    $start_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->start_time)->subDays(1)->format('Y-m-d H:i:s');
                    $end_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->end_time)->format('Y-m-d H:i:s');

                    // jika break time ikut hari ini
                    if($get_shift2->break_time > '07:00:00') {
                        $break_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->break_time)->format('Y-m-d H:i:s');

                        // jika shift 2 sudah berakhir
                        if(Carbon::now() > $end_time_shift2) {
                            $get_shift2_lembur = DB::connection('sds_live')->table('shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '2')->where('actual_date', Carbon::parse($start_time_shift1)->format('Y-m-d'))->first();

                            // jika ada lembur shift 2
                            if($get_shift2_lembur) {

                                // jika waktu sekarang kurang adalah jam lembur
                                if(Carbon::now() < Carbon::parse($end_time_shift2)->addHours($get_shift2_lembur->injury)) {
                                    $condition = 5;

                                    $man_power_shift1 = $get_shift1->man_power;
                                    $man_power_shift1_lembur = $get_shift1->man_power_injury;
                                    $man_power_shift2 = $get_shift2->man_power;
                                    $man_power_shift2_lembur = $get_shift2->man_power_injury;

                                    $pengurangan = 60 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));

                                    $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinute(60))->format('%H');
                                    $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute(60));

                                // jika waktu sekarang sudah selesai lembur shift 1
                                } else {
                                    $get_shift2 = DB::connection('sds_live')->table('master_shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '2')->first();

                                    // jika ada shift 2
                                    if($get_shift2) {

                                        $start_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->start_time)->format('Y-m-d H:i:s');
                                        $end_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->end_time)->format('Y-m-d H:i:s');
                                        $break_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->break_time)->format('Y-m-d H:i:s');

                                        // jika belum mulai shift 2
                                        if(Carbon::now() < $start_time_shift2) {
                                            $condition = 0;

                                        // jika sudah mulai shift 2
                                        } else {
                                            $condition = 3;

                                            $man_power_shift1 = $get_shift1->man_power;
                                            $man_power_shift1_lembur = $get_shift1_lembur->man_power_injury;
                                            $man_power_shift2 = $get_shift2->man_power;

                                            if(Carbon::now() >= $break_time_shift2 && Carbon::now() < Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                                $condition = 0;
                                            } elseif(Carbon::now() >= Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                                $pengurangan = 120 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                                            } else {
                                                $pengurangan = 60 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                                            }

                                            $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinutes($pengurangan))->format('%H');
                                            $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute($pengurangan));
                                        }

                                    // jika tidak ada shift 2
                                    } else {
                                        $condition = 0;
                                    }
                                }
                            // jika tidak ada lembur
                            } else {
                                $condition = 0;
                            }

                        // jika shift 2 belum berakhir
                        } else {

                            $condition = 4;

                            $man_power_shift1 = $get_shift1->man_power;
                            $man_power_shift1_lembur = $get_shift1_lembur->man_power_injury;
                            $man_power_shift2 = $get_shift2->man_power;

                            if(Carbon::now() >= $break_time_shift2 && Carbon::now() < Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                $condition = 0;
                            } elseif(Carbon::now() >= Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                $pengurangan = 120 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                            } else {
                                $pengurangan = 60 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                            }

                            $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinutes($pengurangan))->format('%H');
                            $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute($pengurangan));
                        }
                    }

                // jika tidak ada shift 2
                } else {
                    $condition = 0;
                }

            // jika waktu dari mulai shift 1
            } else {
                $start_time_shift1 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift1->start_time)->format('Y-m-d H:i:s');
                $end_time_shift1 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift1->end_time)->format('Y-m-d H:i:s');
                $break_time_shift1 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift1->break_time)->format('Y-m-d H:i:s');

                // jika waktu dari mulai shift 1 - selesai shift 1
                if(Carbon::now() >= $start_time_shift1 && Carbon::now() <= $end_time_shift1) {
                    $condition = 1;

                    $man_power_shift1 = $get_shift1->man_power;

                    if($break_time_shift1 < Carbon::now() && $break_time_shift1 > Carbon::now()) {
                        $selisih = Carbon::parse($start_time_shift1)->diff($break_time_shift1)->format('%H');
                        $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes($break_time_shift1);
                    } elseif (Carbon::parse($break_time_shift1)->addMinute(60) <= Carbon::now()) {
                        $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinute(60))->format('%H');
                        $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute(60));
                    } else {
                        $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now())->format('%H');
                        $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now());
                    }

                // jika waktu dari selesai shift 1
                } else {
                    $get_shift1_lembur = DB::connection('sds_live')->table('shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '1')->where('actual_date', Carbon::parse($start_time_shift1)->format('Y-m-d'))->first();

                    // jika ada shift 1 lembur dan waktu dari selesai shift 1
                    if($get_shift1_lembur) {

                        // jika waktu sekarang kurang adalah jam lembur
                        if(Carbon::now() < Carbon::parse($end_time_shift1)->addHours($get_shift1_lembur->injury)) {
                            $condition = 2;

                            $man_power_shift1 = $get_shift1->man_power;
                            $man_power_shift1_lembur = $get_shift1->man_power_injury;

                            $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinute(60))->format('%H');
                            $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute(60));

                        // jika waktu sekarang sudah selesai lembur shift 1
                        } else {
                            $get_shift2 = DB::connection('sds_live')->table('master_shift_cutting')->where('factory_id', $factory_id)->where('deleted_at', null)->where('shift', '2')->first();

                            // jika ada shift 2
                            if($get_shift2) {

                                $start_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->start_time)->format('Y-m-d H:i:s');
                                $end_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->end_time)->format('Y-m-d H:i:s');
                                $break_time_shift2 = Carbon::parse(Carbon::now()->format('Y-m-d').' '.$get_shift2->break_time)->format('Y-m-d H:i:s');

                                // jika belum mulai shift 2
                                if(Carbon::now() < $start_time_shift2) {
                                    $condition = 0;

                                // jika sudah mulai shift 2
                                } else {
                                    $condition = 3;

                                    $man_power_shift1 = $get_shift1->man_power;
                                    $man_power_shift1_lembur = $get_shift1_lembur->man_power_injury;
                                    $man_power_shift2 = $get_shift2->man_power;

                                    if(Carbon::now() >= $break_time_shift2 && Carbon::now() < Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                        $condition = 0;
                                    } elseif(Carbon::now() >= Carbon::parse($break_time_shift2)->addMinutes(60)) {
                                        $pengurangan = 120 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                                    } else {
                                        $pengurangan = 60 + (Carbon::parse($end_time_shift1)->diffInMinutes($start_time_shift2) - Carbon::parse($end_time_shift1)->diffInMinutes(Carbon::now()->addHours($get_shift1_lembur->injury)));
                                    }

                                    $selisih = Carbon::parse($start_time_shift1)->diff(Carbon::now()->subMinutes($pengurangan))->format('%H');
                                    $selisih_minute = Carbon::parse($start_time_shift1)->diffInMinutes(Carbon::now()->subMinute($pengurangan));
                                }

                            // jika tidak ada shift 2
                            } else {
                                $condition = 0;
                            }
                        }

                    // jika tidak ada shift 1 lembur
                    } else {

                    }
                }
            }

            // end condition time

            $selisih = $selisih + 1;

            $total_pcs_plan = DB::table('jaz_for_output_cutting_dashboard')->where('factory_id', $factory_id)->where('cutting_date', Carbon::parse($current_time)->format('Y-m-d'))->sum('qty');

            $actual_current_hours = DB::connection('sds_live')->table('out_cutting_pcs')->where('processing_fact', $factory_id)->whereBetween('last_scan_at', [Carbon::parse(Carbon::now()->format('Y-m-d H').':00:00'), Carbon::parse(Carbon::now()->format('Y-m-d H').':59:59')])->sum('qty');

            $get_qty_and_eff_hours_all = DB::connection('sds_live')->table('eff_cutting')->select(DB::raw("processing_fact, sum(qty) as qty, sum(eff_cut) as eff_cut"))->where('processing_fact', $factory_id)->whereBetween('last_scan_at', [Carbon::parse(Carbon::now()->format('Y-m-d').' 06:00:01'), Carbon::parse(Carbon::now()->addDays(1)->format('Y-m-d').' 06:00:00')])->groupBy('processing_fact')->get();

            $actual_current_hours_all = $get_qty_and_eff_hours_all->first()->qty;

            $target_current_hours = ceil($total_pcs_plan / 16);

            if($condition == 1) {
                $target_current_hours_all = $target_current_hours * $selisih;
                $actual_eff = ceil($get_qty_and_eff_hours_all->first()->eff_cut * 100 / ($man_power_shift1 * $selisih_minute));
                $man_power = $man_power_shift1;
            } elseif ($condition == 2) {

            }

            $current_time = $this->convert_hari($current_time).', '.Carbon::parse($current_time)->format('d-m-Y');

            $target_eff = 85;


            $response['current_time'] = $current_time;
            $response['actual_eff'] = $actual_eff;
            $response['target_eff'] = $target_eff;
            $response['man_power'] = $man_power;
            $response['total_pcs_plan'] = $total_pcs_plan;
            $response['target_current_hours'] = $target_current_hours;
            $response['target_current_hours_all'] = $target_current_hours_all;
            $response['actual_current_hours'] = $actual_current_hours;
            $response['actual_current_hours_all'] = $actual_current_hours_all;
            $response['current_hours'] = 'Jam '.Carbon::now()->format('H').':00 - '.Carbon::now()->addHour()->format('H').':00';
            $response['current_hours_all'] = 'Jam '.Carbon::parse($start_time_shift1)->format('H').':00'.' - '.Carbon::now()->addHour()->format('H').':00';
            $response['last_update'] = Carbon::now()->format('d-m-Y H:i:s');

            return response()->json(['status' => 200, 'data' => $response]);
        }
    }

    // end dashboard bundling

    // DASHBOARD SETTING AREA

    public function settingArea($factory_id){

        // deklarasi variable
        $component_ready_1 = array();
        $component_unready_1 = array();
        $component_ready_2 = array();
        $component_unready_2 = array();

        // get line
        $get_lines = DB::connection('line_live')->table('daily_view')->select('line_name', 'present_sewer', 'no_urut')->whereDate('create_date', Carbon::now()->format('Y-m-d'))->where('factory_id', $factory_id)->groupBy('line_name', 'present_sewer', 'no_urut')->orderBy('no_urut', 'asc')->get();

        // split line
        if($get_lines->count() % 2 == 0) {
            $total_line_1 = $get_lines->count() / 2;
            $total_line_2 = $get_lines->count() / 2;
        } else {
            $total_line_1 = ceil($get_lines->count() / 2);
            $total_line_2 = $get_lines->count() - $total_line_1;
        }

        // line pertama
        $lines_1 = DB::connection('line_live')->table('daily_view')->select('line_name', 'present_sewer', 'no_urut')->whereDate('create_date', Carbon::now()->format('Y-m-d'))->where('factory_id', $factory_id)->groupBy('line_name', 'present_sewer', 'no_urut')->orderBy('no_urut', 'asc')->skip(0)->take($total_line_1)->get();

        foreach($lines_1 as $line) {

            // set location
            $location = explode(' ',$line->line_name);
            $location = 'L.'.$location[1];

            // get component ready
            $component_ready = DB::connection('sds_live')->table('component_ready')->where('location', $location)->where('processing_fact', $factory_id)->where('is_ready', true)->get();
            if($component_ready->count() > 0) {
                $total_component_ready = 0;
                foreach($component_ready as $ready) {

                    if($ready->gsd_smv == null) {
                        $total_gsd = 0;
                    } else {
                        $total_gsd = $ready->total_smv;
                    }

                    $total_component_ready = $total_component_ready + $total_gsd;
                }

                $component_ready_1[] = number_format($total_component_ready / ($line->present_sewer * 480 * 0.74), 1, '.', '');
            } else {
                $component_ready_1[] = number_format(0, 1, '.', '');
            }

            // get component unready
            $component_unready = DB::connection('sds_live')->table('component_ready')->where('location', $location)->where('processing_fact', $factory_id)->where('is_ready', false)->get();
            if($component_unready->count() > 0) {
                $total_component_unready = 0;
                foreach($component_unready as $unready) {

                    if($unready->gsd_smv == null) {
                        $total_gsd = 0;
                    } else {
                        $total_gsd = $unready->total_smv;
                    }

                    $total_component_unready = $total_component_unready + $total_gsd;
                }

                $component_unready_1[] = number_format($total_component_unready / ($line->present_sewer * 480 * 0.74), 1, '.', '');
            } else {
                $component_unready_1[] = number_format(0, 1, '.', '');
            }
        }

        $lines_2 = DB::connection('line_live')->table('daily_view')->select('line_name', 'present_sewer', 'no_urut')->whereDate('create_date', Carbon::now()->format('Y-m-d'))->where('factory_id', $factory_id)->groupBy('line_name', 'present_sewer', 'no_urut')->orderBy('no_urut', 'asc')->skip($total_line_1)->take($total_line_2)->get();

        foreach($lines_2 as $line) {

            // set location
            $location = explode(' ',$line->line_name);
            $location = 'L.'.$location[1];

            // get component ready
            $component_ready = DB::connection('sds_live')->table('component_ready')->where('location', $location)->where('processing_fact', $factory_id)->where('is_ready', true)->get();
            if($component_ready->count() > 0) {
                $total_component_ready = 0;
                foreach($component_ready as $ready) {

                    if($ready->gsd_smv == null) {
                        $total_gsd = 0;
                    } else {
                        $total_gsd = $ready->total_smv;
                    }

                    $total_component_ready = $total_component_ready + $total_gsd;
                }

                $component_ready_2[] = number_format($total_component_ready / ($line->present_sewer * 480 * 0.74), 1, '.', '');
            } else {
                $component_ready_2[] = number_format(0, 1, '.', '');
            }

            // get component unready
            $component_unready = DB::connection('sds_live')->table('component_ready')->where('location', $location)->where('processing_fact', $factory_id)->where('is_ready', false)->get();
            if($component_unready->count() > 0) {
                $total_component_unready = 0;
                foreach($component_unready as $unready) {

                    if($unready->gsd_smv == null) {
                        $total_gsd = 0;
                    } else {
                        $total_gsd = $unready->total_smv;
                    }

                    $total_component_unready = $total_component_unready + $total_gsd;
                }

                $component_unready_2[] = number_format($total_component_unready / ($line->present_sewer * 480 * 0.74), 1, '.', '');
            } else {
                $component_unready_2[] = number_format(0, 1, '.', '');
            }
        }

        return view('adibooster/setting_area', compact('factory_id', 'lines_1', 'lines_2', 'component_ready_1', 'component_unready_1', 'component_ready_2', 'component_unready_2'));
    }

    public function auto($factory_id)
    {
        $today = Carbon::now()->format('Y-m-d');
        $mp = 8;
        if($factory_id == 2){
            $target_output = 8500;
        }else{
            $target_output = 0;
        }

        // $data_today = DB::table('dd_daily_output_auto')->where('factory_id',$factory_id)->where('auto_date',$today)->get();
        // dd($data_today);
        $data = DB::select(DB::raw("SELECT
        xz.season,
        xz.style_set,
        sum(xz.ordered_qty) as ordered_qty,
        sum(xz.output_auto) as output_auto
        FROM
        (SELECT
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty,
        sum(z.output_auto) as output_auto
        FROM
        (SELECT
        y.season,
        y.poreference,
        CASE WHEN y.set_type = 'Non' then y.style when y.set_type='BOTTOM' then concat(y.style,'-','BOT') else concat(y.style,'-',y.set_type) end as style_set,
        y.size,
        y.article,
        sum(qty) as output_auto
        FROM
        (SELECT
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty
        FROM
        (SELECT DISTINCT
        dm.barcode_id
        FROM distribusi_movements dm
        where dm.locator_to='5' and dm.status_to='out' and date(created_at) = '$today')x
        JOIN dd_bundle_locator_merge dbl on dbl.barcode_id = x.barcode_id
        where dbl.factory_id='$factory_id'
        GROUP BY
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty)y
        GROUP BY
        y.season,
        y.poreference,
        y.style,
        y.set_type,
        y.size,
        y.article)z
        JOIN dd_order_qty ddo on ddo.po_buyer = z.poreference and ddo.style = z.style_set and ddo.articleno = z.article
        GROUP BY
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty)xz
        GROUP BY
        xz.season,
        xz.style_set
        "));

        $actual_output_day = DB::select(DB::raw("SELECT
        sum(xz.output_auto) as output_auto
        FROM
        (SELECT
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty,
        sum(z.output_auto) as output_auto
        FROM
        (SELECT
        y.season,
        y.poreference,
        CASE WHEN y.set_type = 'Non' then y.style when y.set_type='BOTTOM' then concat(y.style,'-','BOT') else concat(y.style,'-',y.set_type) end as style_set,
        y.size,
        y.article,
        sum(qty) as output_auto
        FROM
        (SELECT
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty
        FROM
        (SELECT DISTINCT
        dm.barcode_id
        FROM distribusi_movements dm
        where dm.locator_to='5' and dm.status_to='out' and date(created_at) = '$today')x
        JOIN dd_bundle_locator_merge dbl on dbl.barcode_id = x.barcode_id
        where dbl.factory_id='$factory_id'
        GROUP BY
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty)y
        GROUP BY
        y.season,
        y.poreference,
        y.style,
        y.set_type,
        y.size,
        y.article)z
        JOIN dd_order_qty ddo on ddo.po_buyer = z.poreference and ddo.style = z.style_set and ddo.articleno = z.article
        GROUP BY
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty)xz
        "));
        $output = $actual_output_day[0];
        return view('adibooster.output_auto', compact(['factory_id','today','data','output','mp','target_output']));
    }

    public function print($factory_id)
    {
        // $today = Carbon::now()->format('Y-m-d');
        $today = Carbon::now()->format('Y-m-d');
        $mp = 157;

        // $data_today = DB::table('dd_daily_output_auto')->where('factory_id',$factory_id)->where('auto_date',$today)->get();
        // dd($data_today);
        $data = DB::select(DB::raw("SELECT
        xz.season,
        xz.style_set,
        sum(xz.ordered_qty) as ordered_qty,
        sum(xz.output_auto) as output_auto
        FROM
        (SELECT
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty,
        sum(z.output_auto) as output_auto
        FROM
        (SELECT
        y.season,
        y.poreference,
        CASE WHEN y.set_type = 'Non' then y.style when y.set_type='BOTTOM' then concat(y.style,'-','BOT') else concat(y.style,'-',y.set_type) end as style_set,
        y.size,
        y.article,
        sum(qty) as output_auto
        FROM
        (SELECT
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty
        FROM
        (SELECT DISTINCT
        dm.barcode_id
        FROM distribusi_movements dm
        where dm.locator_to='3' and dm.status_to='out' and date(created_at) = '$today')x
        JOIN dd_bundle_locator_merge dbl on dbl.barcode_id = x.barcode_id
        where dbl.factory_id='$factory_id'
        GROUP BY
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty)y
        GROUP BY
        y.season,
        y.poreference,
        y.style,
        y.set_type,
        y.size,
        y.article)z
        JOIN dd_order_qty ddo on ddo.po_buyer = z.poreference and ddo.style = z.style_set and ddo.articleno = z.article
        GROUP BY
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty)xz
        GROUP BY
        xz.season,
        xz.style_set
        "));

        $actual_output_day = DB::select(DB::raw("SELECT
        sum(xz.output_auto) as output_auto
        FROM
        (SELECT
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty,
        sum(z.output_auto) as output_auto
        FROM
        (SELECT
        y.season,
        y.poreference,
        CASE WHEN y.set_type = 'Non' then y.style when y.set_type='BOTTOM' then concat(y.style,'-','BOT') else concat(y.style,'-',y.set_type) end as style_set,
        y.size,
        y.article,
        sum(qty) as output_auto
        FROM
        (SELECT
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty
        FROM
        (SELECT DISTINCT
        dm.barcode_id
        FROM distribusi_movements dm
        where dm.locator_to='3' and dm.status_to='out' and date(created_at) = '$today')x
        JOIN dd_bundle_locator_merge dbl on dbl.barcode_id = x.barcode_id
        where dbl.factory_id='$factory_id'
        GROUP BY
        dbl.poreference,
        dbl.season,
        dbl.style,
        dbl.set_type,
        dbl.size,
        dbl.article,
        dbl.cut_num,
        dbl.start_no,
        dbl.end_no,
        dbl.qty)y
        GROUP BY
        y.season,
        y.poreference,
        y.style,
        y.set_type,
        y.size,
        y.article)z
        JOIN dd_order_qty ddo on ddo.po_buyer = z.poreference and ddo.style = z.style_set and ddo.articleno = z.article
        GROUP BY
        z.season,
        z.poreference,
        z.style_set,
        z.article,
        ddo.ordered_qty)xz
        "));
        $output = $actual_output_day[0];
        return view('adibooster.output_print', compact(['factory_id','today','data','output','mp']));
    }

   // END DASHBOARD SETTING AREA
    public function priority($factory_id){
    $today = Carbon::now()->format('Y-m-d');
    // $today = '2022-06-11';
    $data = DataSewing::select('po_buyer','style','articleno')->where('start_sewing',$today)->where('factory_id',$factory_id)->distinct()->get();
    return view('adibooster.priority_sewing', compact(['factory_id','today','data']));
    }

    public function getDataPriority(Request $request){
        if(request()->ajax())
        {
            $start_sewing = $request->start_sewing;
            $factory_id = $request->factory_id;
            $po_arr_temp = DataSewing::select('po_buyer')->where('start_sewing',$start_sewing)->where('factory_id',$factory_id)->distinct()->pluck('po_buyer')->toArray();
            $po_arr = "'".implode("','",$po_arr_temp)."'";
            // $data = DB::select(DB::raw("SELECT zz.poreference,zz.style,zz.season,zz.article,
            // COALESCE(SUM(zz.ordered_qty),SUM(zz.qty_bundle)) AS ordered_qty,
            // SUM(zz.qty_bundle) AS qty_bundle,
            // SUM(zz.qty_cut) AS qty_cut,
            // SUM(zz.qty_fuse) AS qty_fuse,
            // SUM(zz.qty_ppa1) AS qty_ppa1,
            // SUM(zz.qty_ppa2) AS qty_ppa2,
            // SUM(zz.qty_pad) AS qty_pad,
            // SUM(zz.qty_he) AS qty_he,
            // SUM(zz.qty_auto) AS qty_auto,
            // SUM(zz.qty_artwork) AS qty_artwork,
            // SUM(zz.qty_setting) AS qty_setting,
            // SUM(zz.qty_supply) AS qty_supply
            // FROM(SELECT xz.poreference,xz.style,xz.season,xz.article,xz.size,
            // MAX(xz.ordered_qty) AS ordered_qty,
            // SUM(xz.qty_bundle) AS qty_bundle,
            // SUM(xz.qty_cut) AS qty_cut,
            // SUM(xz.qty_fuse) AS qty_fuse,
            // SUM(xz.qty_ppa1) AS qty_ppa1,
            // SUM(xz.qty_ppa2) AS qty_ppa2,
            // SUM(xz.qty_pad) AS qty_pad,
            // SUM(xz.qty_he) AS qty_he,
            // SUM(xz.qty_auto) AS qty_auto,
            // SUM(xz.qty_artwork) AS qty_artwork,
            // CASE WHEN max(xz.qty_cut) < max(xz.qty_setting) THEN max(xz.qty_cut) ELSE max(xz.qty_setting) END AS qty_setting,
            // MAX(xz.qty_supply) as qty_supply
            // FROM(SELECT z.poreference,z.style,z.season,z.article,z.size,dor.ordered_qty,
            // SUM(z.qty_bundle) as qty_bundle,
            // CASE WHEN z.act_set_garment = z.c_set_g THEN SUM(z.qty_cut) ELSE 0 END AS qty_cut,
            // SUM(z.qty_fuse) AS qty_fuse,
            // SUM(z.qty_ppa1) AS qty_ppa1,
            // SUM(z.qty_ppa2) AS qty_ppa2,
            // SUM(z.qty_pad) AS qty_pad,
            // SUM(z.qty_he) AS qty_he,
            // SUM(z.qty_auto) AS qty_auto,
            // SUM(z.qty_artwork) AS qty_artwork,
            // SUM(z.qty_setting) AS qty_setting,
            // SUM(z.qty_supply) AS qty_supply
            // FROM(SELECT y.poreference,y.season,
            // CASE WHEN y.type_name = 'TOP' THEN concat(y.style,'-TOP') WHEN y.type_name = 'BOTTOM' THEN concat(y.style,'-BOT') ELSE y.style END AS style,y.article,y.size,y.cut_num,y.start_no,y.end_no,
            // COUNT(y.part_z) AS act_set_garment,set_g.c_set_g,
            // MAX(y.qty_bundle) AS qty_bundle,
            // CASE WHEN MIN(y.qty_cut) = 0 THEN 0 ELSE MAX(y.qty_cut) END AS qty_cut,
            // MAX(y.qty_fuse) AS qty_fuse,
            // MAX(y.qty_ppa1) AS qty_ppa1,
            // MAX(y.qty_ppa2) AS qty_ppa2,
            // MAX(y.qty_pad) AS qty_pad,
            // MAX(y.qty_he) qty_he,
            // MAX(y.qty_auto) AS qty_auto,
            // MAX(y.qty_artwork) AS qty_artwork,
            // MAX(y.qty_setting) AS qty_setting,
            // MAX(y.qty_supply) AS qty_supply
            // FROM(SELECT x.barcode_id,x.poreference,x.season,x.style,x.type_name,x.article,x.size,x.komponen_name,x.process,x.process_name,x.cut_num,x.start_no,x.end_no,
            // CASE WHEN vms.style IS NOT NULL THEN 1 ELSE NULL END AS part_z,
            // MAX(x.qty) as qty_bundle,
            // MAX(x.qty_cut) as qty_cut,
            // CASE WHEN MAX(x.qty_fuse) IS NULL AND x.process_name LIKE '%FUSE%' THEN 0 ELSE MAX(x.qty_fuse)END AS qty_fuse,
            // CASE WHEN MAX(x.qty_ppa1) IS NULL AND x.process_name LIKE '%PPA%' THEN 0 ELSE MAX(x.qty_ppa1)END AS qty_ppa1,
            // CASE WHEN MAX(x.qty_ppa2) IS NULL AND x.process_name LIKE '%PPA2%' THEN 0 ELSE MAX(x.qty_ppa2) END AS qty_ppa2,
            // CASE WHEN MAX(x.qty_pad) IS NULL AND x.process_name LIKE '%PAD%' THEN 0 ELSE MAX(x.qty_pad) END AS qty_pad,
            // CASE WHEN MAX(x.qty_he) IS NULL AND x.process_name LIKE '%HE%' THEN 0 ELSE MAX(x.qty_he)END AS qty_he,
            // CASE WHEN x.process_name LIKE '%TEMPLATE%' OR x.process_name LIKE '%AUTO%' OR x.process_name LIKE '%BOBOK%' OR x.process_name LIKE '%AMS%' AND MAX(x.qty_auto) IS NOT NULL THEN MAX(x.qty_auto) ELSE null END AS qty_auto,
            // CASE WHEN x.process_name LIKE '%EMBRO%' or x.process_name LIKE '%PRINT%' and MAX(x.qty_artwork) IS NOT NULL THEN MAX(x.qty_artwork) ELSE null END AS qty_artwork,
            // CASE WHEN MAX(x.qty_setting) IS NULL THEN 0 ELSE MAX(x.qty_setting)END AS qty_setting,
            // CASE WHEN MAX(x.qty_supply) IS NULL THEN 0 ELSE MAX(x.qty_supply) END AS qty_supply
            // FROM(SELECT bd.barcode_id,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,bd.komponen_name,vms.process,vms.process_name,bh.cut_num,bd.start_no,bd.end_no,bd.qty,
            // COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN bd.qty END,0) AS qty_cut,
            // CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN bd.qty END AS qty_fuse,
            // CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN bd.qty END AS qty_ppa1,
            // CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN bd.qty END AS qty_ppa2,
            // CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN bd.qty END AS qty_pad,
            // CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN bd.qty END AS qty_he,
            // CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN bd.qty END AS qty_auto,
            // CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN bd.qty END AS qty_artwork,
            // CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN bd.qty END AS qty_setting,
            // CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN bd.qty END AS qty_supply
            // FROM bundle_detail bd
            // JOIN bundle_header bh on bh.id = bd.bundle_header_id
            // JOIN v_master_style vms on vms.id = bd.style_detail_id
            // LEFT JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
            // WHERE bh.poreference IN(SELECT DISTINCT bh.poreference
            // FROM
            // bundle_header bh
            // JOIN bundle_detail bd on bd.bundle_header_id = bh.id
            // JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
            // WHERE bh.poreference in($po_arr)
            // UNION
            // SELECT DISTINCT shm.poreference
            // FROM
            // sds_header_movements shm
            // JOIN sds_detail_movements sdm on sdm.sds_header_id = shm.id
            // JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
            // WHERE shm.poreference in($po_arr))
            // UNION
            // SELECT sdm.sds_barcode as barcode_id,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,sdm.komponen_name,vms.process,vms.process_name,shm.cut_num,sdm.start_no,sdm.end_no,sdm.qty,
            // COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN sdm.qty END,0) AS qty_cut,
            // CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN sdm.qty END AS qty_fuse,
            // CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN sdm.qty END AS qty_ppa1,
            // CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN sdm.qty END AS qty_ppa2,
            // CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN sdm.qty END AS qty_pad,
            // CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN sdm.qty END AS qty_he,
            // CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN sdm.qty END AS qty_auto,
            // CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN sdm.qty END AS qty_artwork,
            // CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN sdm.qty END AS qty_setting,
            // CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN sdm.qty END AS qty_supply
            // FROM sds_detail_movements sdm
            // JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
            // JOIN v_master_style vms on vms.id = sdm.style_id
            // LEFT JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
            // WHERE shm.poreference in(SELECT DISTINCT bh.poreference
            // FROM
            // bundle_header bh
            // JOIN bundle_detail bd on bd.bundle_header_id = bh.id
            // JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
            // WHERE bh.poreference in($po_arr)
            // UNION
            // SELECT DISTINCT shm.poreference
            // FROM
            // sds_header_movements shm
            // JOIN sds_detail_movements sdm on sdm.sds_header_id = shm.id
            // JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
            // WHERE shm.poreference in($po_arr))
            // ORDER BY size,cut_num,start_no,end_no)x
            // JOIN v_master_style vms on vms.style = x.style and vms.season_name = x.season
            // GROUP BY
            // x.size,vms.style,x.cut_num,x.start_no,x.end_no,x.komponen_name,x.process,x.process_name,x.barcode_id,x.poreference,x.season,x.style,x.type_name,x.article)y
            // LEFT JOIN dd_set_garment set_g ON set_g.style = y.style and set_g.season_name = y.season
            // GROUP BY
            // y.poreference,y.season,y.style,y.type_name,set_g.c_set_g,y.article,y.size,y.cut_num,y.start_no,y.end_no)z
            // LEFT JOIN dd_order_qty_v2 dor on dor.poreference = z.poreference AND dor.style = z.style AND dor.size_finish_good = z.size
            // GROUP BY
            // z.poreference,z.season,z.style,z.article,z.size,z.act_set_garment,z.c_set_g,dor.ordered_qty)xz
            // GROUP BY
            // xz.poreference,xz.style,xz.season,xz.article,xz.size)zz
            // GROUP BY
            // zz.poreference,zz.style,zz.season,zz.article"));

            $data = DB::connection('sds_live')->select(DB::raw("SELECT
            xz.season,
            xz.style,
            xz.poreference,
            xz.article,
            xz.order_qty as ordered_qty,
            case when xz.cut is not null and xz.cut < xz.supply then xz.supply else xz.cut end as qty_cut,
            case when xz.art is not null and xz.art < xz.supply then xz.supply else xz.art end as qty_artwork,
            case when xz.he is not null and xz.he < xz.supply then xz.supply else xz.he end as qty_he,
            case when xz.pad is not null and xz.pad < xz.supply then xz.supply else xz.pad end as qty_pad,
            case when xz.ppa is not null and xz.ppa < xz.supply then xz.supply else xz.ppa end as qty_ppa1,
            null as qty_ppa2,
            case when xz.fuse is not null and xz.fuse < xz.supply then xz.supply else xz.fuse end as qty_fuse,
            case when xz.auto is not null and xz.auto < xz.supply then xz.supply else xz.auto end as qty_auto,
            case when xz.setting is not null and xz.setting < xz.supply then xz.supply else xz.setting end as qty_setting,
            xz.supply as qty_supply
            FROM
            (SELECT
            z.season,
            z.style,
            z.poreference,
            z.article,
            sum(z.order_qty) as order_qty,
            sum(z.cut) as cut,
            sum(z.art) as art,
            sum(z.he) as he,
            sum(z.pad) as pad,
            sum(z.ppa) as ppa,
            sum(z.fuse) as fuse,
            sum(z.auto) as auto,
            sum(z.setting) as setting,
            sum(z.supply) as supply
            FROM
            (SELECT
            y.season,
            y.style,
            y.poreference,
            y.article,
            y.cut_num,
            y.size,
            sum(y.qty) as order_qty,
            sum(case when y.cut = 1 THEN y.qty else null end) as cut,
            sum(case when y.art = 1 THEN y.qty else null end) as art,
            sum(case when y.he = 1 THEN y.qty else null end) as he,
            sum(case when y.pad = 1 THEN y.qty else null end) as pad,
            sum(case when y.ppa = 1 THEN y.qty else null end) as ppa,
            sum(case when y.fuse = 1 THEN y.qty else null end) as fuse,
            sum(case when y.auto = 1 THEN y.qty else null end) as auto,
            sum(case when y.setting = 1 THEN y.qty else null end) as setting,
            sum(case when y.supply = 1 THEN y.qty else null end) as supply
            FROM
            (SELECT
            x.season,
            x.style,
            x.poreference,
            x.article,
            x.cut_num,
            x.size,
            x.start_num,
            x.end_num,
            x.qty,
            case when count(x.cut) = sum(x.cut) then 1 when count(x.cut) <> sum(x.cut) then 0 else null end as cut,
            case when count(x.art) = sum(x.art) then 1 when count(x.art) <> sum(x.art) then 0 else null end as art,
            case when count(x.he) = sum(x.he) then 1 when count(x.he) <> sum(x.he) then 0 else null end as he,
            case when count(x.pad) = sum(x.pad) then 1 when count(x.pad) <> sum(x.pad) then 0 else null end as pad,
            case when count(x.ppa) = sum(x.ppa) then 1 when count(x.ppa) <> sum(x.ppa) then 0 else null end as ppa,
            case when count(x.fuse) = sum(x.fuse) then 1 when count(x.fuse) <> sum(x.fuse) then 0 else null end as fuse,
            case when count(x.auto) = sum(x.auto) then 1 when count(x.auto) <> sum(x.auto) then 0 else null end as auto,
            case when count(x.setting) = sum(x.setting) then 1 when count(x.setting) <> sum(x.setting) then 0 else null end as setting,
            case when count(x.supply) = sum(x.supply) then 1 when count(x.supply) <> sum(x.supply) then 0 else null end as supply
            FROM
            (SELECT 
            bi.season,
            case when ad.set_type = '0' THEN bi.style else concat(bi.style,'-',ad.set_type) end as style,
            bi.poreference,
            bi.article,
            bi.cut_num,
            bi.size,
            bi.part_name,
            bi.start_num,
            bi.end_num,
            bi.qty,
            case when bi.distrib_received is not null then 1 else 0 end as cut,
            case when ad.artwork = true and bi.artwork_out is not null then 1 when ad.artwork = true and bi.artwork_out is null then 0 else null end as art,
            case when ad.he = true and bi.he_out is not null then 1 when ad.he = true and bi.he_out is null then 0 else null end as he,
            case when ad.pad = true and bi.pad_out is not null then 1 when ad.pad = true and bi.pad_out is null then 0 else null end as pad,
            case when ad.ppa = true and bi.ppa_out is not null then 1 when ad.ppa = true and bi.ppa_out is null then 0 else null end as ppa,
            case when ad.fuse = true and bi.fuse_out is not null then 1 when ad.fuse = true and bi.fuse_out is null then 0 else null end as fuse,
            case when ad.bobok = true and bi.bobok_out is not null then 1 when ad.bobok = true and bi.bobok_out is null then 0 else null end as auto,
            case when bi.supply is not null then 1 when bi.setting is not null and bi.supply is null then 1 else 0 end as setting,
            case when bi.supply is not null then 1 else 0 end as supply
            from bundle_info_new bi
            left join art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            where bi.poreference in($po_arr)
            ORDER BY
            bi.poreference,
            bi.style,
            ad.set_type,
            bi.article,
            bi.cut_num,
            bi.size,
            bi.start_num)x
            GROUP BY
            x.season,
            x.style,
            x.poreference,
            x.article,
            x.cut_num,
            x.size,
            x.start_num,
            x.end_num,
            x.qty)y
            GROUP BY
            y.season,
            y.style,
            y.poreference,
            y.article,
            y.cut_num,
            y.size)z
            GROUP BY
            z.season,
            z.style,
            z.poreference,
            z.article)xz"));
            // dd($data);

            $text = '<ul>';
            $i=1;
            foreach ($data as $key => $value) {
                //PART 1
                //QTY CUTTING
                if($value->qty_cut != null && (int)$value->qty_cut == (int)$value->ordered_qty){
                    $text_qty_cut = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_cut != null && (int)$value->qty_cut != (int)$value->ordered_qty){
                    $text_qty_cut = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_cut.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_cut = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY PPA1
                if($value->qty_ppa1 != null && (int)$value->qty_ppa1 == (int)$value->ordered_qty){
                    $text_qty_ppa1 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_ppa1 = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_ppa1 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 > (int)$value->qty_setting){
                    $text_qty_ppa1 = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_ppa1.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_ppa1 = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY PPA2
                if($value->qty_ppa2 != null && (int)$value->qty_ppa2 == (int)$value->ordered_qty){
                    $text_qty_ppa2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa2 != null && (int)$value->qty_ppa2 != (int)$value->ordered_qty){
                    $text_qty_ppa2 = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_ppa2.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_ppa2 = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY FUSE
                if($value->qty_fuse != null && (int)$value->qty_fuse == (int)$value->ordered_qty){
                    $text_qty_fuse = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_fuse = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_fuse = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse > (int)$value->qty_setting){
                    $text_qty_fuse = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_fuse.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_fuse = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY ARTWORK
                if($value->qty_artwork != null && (int)$value->qty_artwork == (int)$value->ordered_qty){
                    $text_qty_artwork = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_artwork = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_artwork = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork > (int)$value->qty_setting){
                    $text_qty_artwork = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_artwork.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_artwork = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY AUTO
                if($value->qty_auto != null && (int)$value->qty_auto == (int)$value->ordered_qty){
                    $text_qty_auto = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_auto = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_auto = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto > (int)$value->qty_setting){
                    $text_qty_auto = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_auto.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_auto = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                //QTY PAD
                if($value->qty_pad != null && (int)$value->qty_pad == (int)$value->ordered_qty){
                    $text_qty_pad = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_pad = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_pad = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad > (int)$value->qty_setting){
                    $text_qty_pad = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_pad.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_pad = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY HE
                if($value->qty_he != null && (int)$value->qty_he == (int)$value->ordered_qty){
                    $text_qty_he = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_he != null && (int)$value->qty_he < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_he = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_he != null && (int)$value->qty_he < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_he = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_he != null && (int)$value->qty_he > (int)$value->qty_setting){
                    $text_qty_he = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_he.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_he = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY SETTING
                // dd((int)$value->qty_setting, (int)$value->ordered_qty);
                if($value->qty_setting != null && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_setting = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_setting != null && $value->qty_supply != null && (int)$value->qty_setting == (int)$value->qty_supply){
                    $text_qty_setting = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_supply.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting != null && $value->qty_supply != null && (int)$value->qty_setting > (int)$value->qty_supply){
                    $text_qty_setting = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting != null && $value->qty_supply == null && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_setting = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting == null && $value->qty_supply == null){
                    $text_qty_setting = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.'0'.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_setting = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY SUPPLY
                if($value->qty_supply != null && (int)$value->qty_supply >= (int)$value->ordered_qty){
                    $text_qty_supply = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_supply != null && (int)$value->qty_supply < (int)$value->ordered_qty){
                    $text_qty_supply = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_supply.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_supply == null){
                    $text_qty_supply = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.'0'.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_supply = '<td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }

                //PART 2
                //QTY CUTTING
                if($value->qty_cut != null && (int)$value->qty_cut == (int)$value->ordered_qty){
                    $text_qty_cut2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_cut != null && (int)$value->qty_cut != (int)$value->ordered_qty){
                    $text_qty_cut2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_cut.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_cut2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY PPA1
                if($value->qty_ppa1 != null && (int)$value->qty_ppa1 == (int)$value->ordered_qty){
                    $text_qty_ppa12 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_ppa12 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_ppa12 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa1 != null && (int)$value->qty_ppa1 > (int)$value->qty_setting){
                    $text_qty_ppa12 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_ppa1.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_ppa12 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY PPA2
                if($value->qty_ppa2 != null && (int)$value->qty_ppa2 == (int)$value->ordered_qty){
                    $text_qty_ppa22 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_ppa2 != null && (int)$value->qty_ppa2 != (int)$value->ordered_qty){
                    $text_qty_ppa22 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_ppa2.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_ppa22 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY FUSE
                if($value->qty_fuse != null && (int)$value->qty_fuse == (int)$value->ordered_qty){
                    $text_qty_fuse2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_fuse2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_fuse2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_fuse != null && (int)$value->qty_fuse > (int)$value->qty_setting){
                    $text_qty_fuse2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_fuse.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_fuse2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY ARTWORK
                if($value->qty_artwork != null && (int)$value->qty_artwork == (int)$value->ordered_qty){
                    $text_qty_artwork2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_artwork2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_artwork2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_artwork != null && (int)$value->qty_artwork > (int)$value->qty_setting){
                    $text_qty_artwork2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_artwork.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_artwork2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY AUTO
                if($value->qty_auto != null && (int)$value->qty_auto == (int)$value->ordered_qty){
                    $text_qty_auto2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_auto2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_auto2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_auto != null && (int)$value->qty_auto > (int)$value->qty_setting){
                    $text_qty_auto2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_auto.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_auto2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                //QTY PAD
                if($value->qty_pad != null && (int)$value->qty_pad == (int)$value->ordered_qty){
                    $text_qty_pad2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_pad2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_pad2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_pad != null && (int)$value->qty_pad > (int)$value->qty_setting){
                    $text_qty_pad2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_pad.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_pad2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY HE
                if($value->qty_he != null && (int)$value->qty_he == (int)$value->ordered_qty){
                    $text_qty_he2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_he != null && (int)$value->qty_he < (int)$value->qty_setting && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_he2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_he != null && (int)$value->qty_he < (int)$value->qty_setting && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_he2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }else{
                    $text_qty_he2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY SETTING
                if($value->qty_setting != null && (int)$value->qty_setting >= (int)$value->ordered_qty){
                    $text_qty_setting2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_setting != null && $value->qty_supply != null && (int)$value->qty_setting == (int)$value->qty_supply){
                    $text_qty_setting2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting != null && $value->qty_supply != null && (int)$value->qty_setting > (int)$value->qty_supply){
                    $text_qty_setting2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_supply.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting != null && $value->qty_supply == null && (int)$value->qty_setting < (int)$value->ordered_qty){
                    $text_qty_setting2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_setting.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_setting == null && $value->qty_supply == null){
                    $text_qty_setting2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.'0'.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }else{
                    $text_qty_setting2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                // QTY SUPPLY
                if($value->qty_supply != null && (int)$value->qty_supply == (int)$value->ordered_qty){
                    $text_qty_supply2 = '<td class="bg-green" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"><span class="label label-default label-rounded"><i class="icon-checkmark2"></i></span></td>';
                }elseif($value->qty_supply != null && (int)$value->qty_supply < (int)$value->ordered_qty){
                    $text_qty_supply2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.$value->qty_supply.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }elseif($value->qty_supply == null){
                    $text_qty_supply2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%">'.'0'.'</br>'.$value->ordered_qty.'</br>'.'</td>';
                }
                else{
                    $text_qty_supply2 = '<td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:center;padding-right:5px" width="8.42%"></td>';
                }
                if($i%2==0){
                    $text .= '
                        <li>
                            <table class="table-bordered table-striped" style="width:1305px">
                                <tr>
                                    <td class="bg-grey-300" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:right;padding-right:5px" width="15.8%">'.$value->season.'<br>'.$value->poreference.'<br>'.$value->style.'<br>'.$value->article.'</td>
                                    '.$text_qty_cut.'
                                    '.$text_qty_ppa1.'
                                    '.$text_qty_ppa2.'
                                    '.$text_qty_fuse.'
                                    '.$text_qty_artwork.'
                                    '.$text_qty_auto.'
                                    '.$text_qty_pad.'
                                    '.$text_qty_he.'
                                    '.$text_qty_setting.'
                                    '.$text_qty_supply.'
                                </tr>
                            </table>
                        </li>
                    ';
                }
                else{
                    $text .= '
                        <li>
                            <table class="table-bordered table-striped" style="width:1305px">
                                <tr>
                                    <td class="bg-grey-600" style="border:3px solid white;font-weight:bold;font-size:15px;text-align:right;padding-right:5px" width="15.8%">'.$value->season.'<br>'.$value->poreference.'<br>'.$value->style.'<br>'.$value->article.'</td>
                                    '.$text_qty_cut2.'
                                    '.$text_qty_ppa12.'
                                    '.$text_qty_ppa22.'
                                    '.$text_qty_fuse2.'
                                    '.$text_qty_artwork2.'
                                    '.$text_qty_auto2.'
                                    '.$text_qty_pad2.'
                                    '.$text_qty_he2.'
                                    '.$text_qty_setting2.'
                                    '.$text_qty_supply2.'
                                </tr>
                            </table>
                        </li>
                    ';
                }
                $i++;
            }
            $text       .= '</ul>';
            $obj        = new stdClass();
            $obj->count = $i;
            $obj->text  = $text;
            return response()->json($obj,'200');
        }
    }
}
