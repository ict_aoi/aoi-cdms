$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $('#cutting_date').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: "YYYY-MM-DD",
            separator: " - ",
        }
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#markerDataTable').DataTable({
        ajax: {
            url: '/review-spreading-result/get-data',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'cutting_date', name: 'cutting_date',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'barcode_id', name: 'barcode_id',searchable:true,orderable:true},
            {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            {data: 'cut', name: 'cut',searchable:true,orderable:true},
            {data: 'actual_use', name: 'actual_use',searchable:false,orderable:true},
            {data: 'gl_status', name: 'gl_status',searchable:false,orderable:false},
            {data: 'qc_status', name: 'qc_status',searchable:false,orderable:false},
            {data: 'table_name', name: 'table_name',searchable:true,orderable:true},
            {data: 'operator', name: 'operator',searchable:true,orderable:true},
            {data: 'view', name: 'view',searchable:false,orderable:false},
        ],
    });

    $('#markerDataTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/review-spreading-result/get-data',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#markerDataTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#markerDataTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#markerDataTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    var dtable = $('#markerDataTable').dataTable().api();

    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $('#fabricTable > tbody').html('');
    });

    $('#revisiModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $('#fabricRevisiTable > tbody').html('');
    });

    $('#btn_simpan').on('click', function() {

    });

    // Update actual layer

    var actual_layer = '#actual_layer';

    $('#fabricRevisiTable').on('change', actual_layer, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_layer = $row.find(actual_layer).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#revisi_view_allow').text();
        var barcode_marker = $('#revisi_view_barcode').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-actual-layer',
            data: {
                id: $id,
                actual_layer: $actual_layer,
                barcode_marker: barcode_marker,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    // Update actual sisa

    var actual_sisa = '#actual_sisa';

    $('#fabricRevisiTable').on('change', actual_sisa, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_sisa = $row.find(actual_sisa).val();
        var $id = $row.find('#id').val();
        var barcode_marker = $('#revisi_view_barcode').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-actual-sisa',
            data: {
                id: $id,
                barcode_marker: barcode_marker,
                actual_sisa: $actual_sisa
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    // Update reject

    var reject = '#reject';

    $('#fabricRevisiTable').on('change', reject, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $reject = $row.find(reject).val();
        var $id = $row.find('#id').val();
        var barcode_marker = $('#revisi_view_barcode').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-reject',
            data: {
                id: $id,
                barcode_marker: barcode_marker,
                reject: $reject
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    // Update sambungan

    var sambungan = '#sambungan';

    $('#fabricRevisiTable').on('change', sambungan, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $sambungan = $row.find(sambungan).val();
        var $id = $row.find('#id').val();
        var barcode_marker = $('#revisi_view_barcode').text();

        var detail_view_allow = $('#revisi_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-sambungan',
            data: {
                id: $id,
                sambungan: $sambungan,
                barcode_marker: barcode_marker,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    // Update sambungan end

    var sambungan_end = '#sambungan_end';

    $('#fabricRevisiTable').on('change', sambungan_end, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $sambungan_end = $row.find(sambungan_end).val();
        var $id = $row.find('#id').val();
        var barcode_marker = $('#revisi_view_barcode').text();
        var detail_view_allow = $('#revisi_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-sambungan-end',
            data: {
                id: $id,
                barcode_marker: barcode_marker,
                sambungan_end: $sambungan_end,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    // Update kualitas gelaran

    var kualitas_gelaran = '#kualitas_gelaran';

    $('#fabricRevisiTable').on('change', kualitas_gelaran, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $kualitas_gelaran = $row.find(kualitas_gelaran).val();
        var $id = $row.find('#id').val();
        var barcode_marker = $('#revisi_view_barcode').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/review-spreading-result/update-kualitas-gelaran',
            data: {
                id: $id,
                barcode_marker: barcode_marker,
                kualitas_gelaran: $kualitas_gelaran
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        }).done(function(response) {
            $('#revisiModal').modal();
            $('#fabricRevisiTable > tbody').html('');
            show_data_revisi(response.data);
        });
    });

    $('#btn_download_data').on('click', function() {
        var cutting_date = $('#cutting_date').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: $('#generateDownloadLink').attr('href'),
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                window.open(response.download_link, '_blank');
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });
});

// Function

function detail(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        }
    })
    .done(function (response) {
        $('#detail_view_barcode').text(response.barcode_id);
        $('#detail_view_style').text(response.style);
        $('#detail_view_cut').text(response.cut);
        $('#detail_view_item').text(response.item_code);
        var deliv_date = response.deliv_date.split('-');
        deliv_date = deliv_date[2]+'-'+deliv_date[1]+'-'+deliv_date[0]
        $('#detail_view_deliv').text(deliv_date);
        $('#detail_view_width').text(response.fabric_width - 0.5);
        $('#detail_view_color_code').text(response.color_code);
        $('#detail_view_article').text(response.articleno);
        $('#detail_view_length').text(response.marker_length);
        $('#detail_view_color_name').text(response.color_name);
        $('#detail_view_color_code').text(response.color_code);
        $('#detail_view_item_code').text(response.item_code);
        $('#detail_view_marker_pcs').text(response.qty_ratio * response.layer);
        $('#detail_view_allow').text(parseFloat(response.marker_length) + 0.75);
        $('#detail_view_part').text(response.part_no);
        $('#detail_view_total_garment').text(response.qty_ratio);
        $('#detail_view_layer').text(response.layer);
        $('#detail_view_layer_plan').text(response.layer_plan);
        $('#detail_view_list_po').text(response.list_po);
        $('#detail_view_list_ratio').text(response.list_ratio);
        var cutting_date = response.cutting_date.split('-');
        cutting_date = cutting_date[2]+'-'+cutting_date[1]+'-'+cutting_date[0]
        $('#detail_view_plan').text(cutting_date);
        show_data_detail(response.data_detail);
        $('#detailModal').modal();
        // get_detail_table();
    });
}

function revisi(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {
        $('#revisi_view_barcode').text(response.barcode_id);
        $('#revisi_view_style').text(response.style);
        $('#revisi_view_cut').text(response.cut);
        $('#revisi_view_item').text(response.item_code);
        var deliv_date = response.deliv_date.split('-');
        deliv_date = deliv_date[2]+'-'+deliv_date[1]+'-'+deliv_date[0]
        $('#revisi_view_deliv').text(deliv_date);
        $('#revisi_view_width').text(response.fabric_width - 0.5);
        $('#revisi_view_color_code').text(response.color_code);
        $('#revisi_view_article').text(response.articleno);
        $('#revisi_view_length').text(response.marker_length);
        $('#revisi_view_color_name').text(response.color_name);
        $('#revisi_view_color_code').text(response.color_code);
        $('#revisi_view_item_code').text(response.item_code);
        $('#revisi_view_marker_pcs').text(response.qty_ratio * response.layer);
        $('#revisi_view_allow').text(parseFloat(response.marker_length) + 0.75);
        $('#revisi_view_part').text(response.part_no);
        $('#revisi_view_total_garment').text(response.qty_ratio);
        $('#revisi_view_layer').text(response.layer);
        $('#revisi_view_layer_plan').text(response.layer_plan);
        $('#revisi_view_list_po').text(response.list_po);
        $('#revisi_view_list_ratio').text(response.list_ratio);
        var cutting_date = response.cutting_date.split('-');
        cutting_date = cutting_date[2]+'-'+cutting_date[1]+'-'+cutting_date[0]
        $('#revisi_view_plan').text(cutting_date);
        show_data_revisi(response.data_detail);
        $('#revisiModal').modal();
        // get_detail_table();
    });
}

function show_data_detail(data)
{
    $.each(data, function(index, value) {
        $('#fabricTable > tbody').append($('<tr>')
            .append($('<td>').text(value.no_roll))
            .append($('<td>').text(value.lot))
            .append($('<td>').text(parseFloat(value.qty_fabric).toFixed(2)))
            .append($('<td>').text(value.actual_width))
            .append($('<td>').text(value.suplai_layer))
            .append($('<td>').text(value.actual_layer))
            .append($('<td>').text(parseFloat(value.suplai_sisa).toFixed(2)))
            .append($('<td>').text(parseFloat(value.actual_sisa).toFixed(2)))
            .append($('<td>').text(value.akumulasi_layer))
            .append($('<td>').text(value.reject))
            .append($('<td>').text(value.sambungan))
            .append($('<td>').text(value.sambungan_end))
            .append($('<td>').text(parseFloat(value.actual_use).toFixed(2)))
            .append($('<td>').text(parseFloat(value.remark).toFixed(2)))
            .append($('<td>').text(value.kualitas_gelaran))
        );
        console.log(value.id);
    });
}

function show_data_revisi(data)
{
    $.each(data, function(index, value) {
        $('#fabricRevisiTable > tbody').append($('<tr>')
            .append($('<td>').addClass('set-hidden').append($('<input>').val(value.id).attr('id', 'id')))
            .append($('<td>').text(value.no_roll))
            .append($('<td>').text(value.lot))
            .append($('<td>').text(parseFloat(value.qty_fabric).toFixed(2)))
            .append($('<td>').text(value.actual_width))
            .append($('<td>').text(value.suplai_layer))
            .append($('<td>').append($('<input>').val(value.actual_layer).attr('onkeypress', 'isNumberDot(event)').addClass('set-width').attr('id', 'actual_layer')))
            .append($('<td>').text(parseFloat(value.suplai_sisa).toFixed(2)))
            .append($('<td>').append($('<input>').val(value.actual_sisa).attr('onkeypress', 'isNumberDot(event)').addClass('set-width').attr('id', 'actual_sisa')))
            .append($('<td>').text(value.akumulasi_layer))
            .append($('<td>').append($('<input>').val(value.reject).attr('onkeypress', 'isNumberDot(event)').addClass('set-width').attr('id', 'reject')))
            .append($('<td>').append($('<input>').val(value.sambungan).attr('onkeypress', 'isNumberDot(event)').addClass('set-width').attr('id', 'sambungan')))
            .append($('<td>').append($('<input>').val(value.sambungan_end).attr('onkeypress', 'isNumberDot(event)').addClass('set-width').attr('id', 'sambungan_end')))
            .append($('<td>').text(parseFloat(value.actual_use).toFixed(2)))
            .append($('<td>').text(parseFloat(value.remark).toFixed(2)))
            .append($('<td>').append($('<input>').val(value.kualitas_gelaran).attr('id', 'kualitas_gelaran')))
        );
        console.log(value.id, 'aaaa');
    });
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

function clear_table_revisi() {
    $('#fabricRevisiTable > tbody').html('');
}
