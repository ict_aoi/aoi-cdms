<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminNameToBundleHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bundle_header', function (Blueprint $table) {
            //
            $table->string('admin_name', 191)->nullable();
            $table->string('qc_operator_name', 191)->nullable();
            $table->string('bundle_operator_name', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bundle_header', function (Blueprint $table) {
            //
        });
    }
}
