<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DefectName extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_defect_endlines';
	protected $fillable = ['id','nama_defect','nama_defect_eng','is_repair','is_reject','created_at','updated_at','deleted_at','locator','is_fabric','is_cutting','template','heat_transfer','bobok','fuse','bonding','pad','join_label','ppa1','ppa2','cutting','embro','printing','created_by','deleted_by','updated_by'];

}
