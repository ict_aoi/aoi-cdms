<div id="detailDetailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Part Number Marker</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="cutting_date_detail_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="style_detail_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="po_buyer_detail_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="articleno_detail_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Part No</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="part_no_detail_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailDetailTable">
						<thead>
							<tr>
								<th class="dt_col_hide">Id</th>
								<th>Cut</th>
								<th>Color</th>
								<th>Rasio</th>
								<th>Layer</th>
								<th>Lebar Fabrik</th>
								<th>Lebar Marker</th>
								<th>Perimeter</th>
								<th>Panjang Marker</th>
								<th>Total Kebutuhan</th>
								<th><button class="btn btn-primary btn-xs" id="add_row" data-id="header_id"><i class="icon icon-plus3"></i></button></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="sizeBalanceTable">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="lebarFBTable">
						<thead>
							<tr>
								<th>Fabric Width</th>
								<th>Fabric Prepared</th>
								<th>Total Kebutuhan</th>
							</tr>
						</thead>
						<tfoot>
							<td></td>
							<td></td>
							<td></td>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="overConsumTable">
						<thead>
							<tr>
								<th>Total CSI</th>
								<th>Total Kebutuhan</th>
								<th>Balance Fabric</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="planning_id_detail" id="planning_id_detail" />
				<input type="hidden" name="part_no_detail" id="part_no_detail" />
				<input type="hidden" name="color_detail" id="color_detail" />
			</div>

			<div class="modal-footer">
				<div class="form-group">
                    <div class="col-md-8">
                        <input type="text" class="form-control border-bottom-danger hidden" name="remark-release" id="remark-release" placeholder="input remark jika ada" />
                    </div>
                    <div class="col-md-4">
						<button type="button" class="btn btn-primary hidden" id="btn-release-submit">Submit</button>
						<button type="button" class="btn btn-primary" id="btn-release">Release</button>
						<button type="button" class="btn btn-danger hidden" id="btn-release-cancel">Cancel</button>
						<button type="button" class="btn btn-danger" id="reset_all">Reset</button>
						<button type="button" class="btn btn-link" data-dismiss="modal" id="btn-close-modal">Close</button>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>