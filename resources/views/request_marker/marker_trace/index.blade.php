@extends('layouts.app',['active' => 'qty_fabric_check'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }

    .change-bg-header {
        background-color: aqua;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Marker Trace</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Marker Trace</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_marker" name="barcode_marker" placeholder="#Scan Barcode Marker" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly">
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-info btn-icon icon-make-group" id="spreading_trace"> SPREADING TRACE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="panel-heading">
            <h5>Barcode <span id="detail_view_barcode_marker">-</span></h5>
        </div>
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="headerTable">
                <tbody>
                    <tr>
                        <td><strong>Style</strong></td>
                        <td><span id="detail_view_style">Data Empty!</span></td>
                        <td><strong>Cut</strong></td>
                        <td><span id="detail_view_cut">Data Empty!</span></td>
                        <td><strong>Item</strong></td>
                        <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Deliv</strong></td>
                        <td><span id="detail_view_deliv">Data Empty!</span></td>
                        <td><strong>Marker Width (inch)</strong></td>
                        <td><span id="detail_view_width">Data Empty!</span></td>
                        <td rowspan="2"><strong>Color</strong></td>
                        <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Article</strong></td>
                        <td><span id="detail_view_article">Data Empty!</span></td>
                        <td><strong>Marker Length (inch)</strong></td>
                        <td><span id="detail_view_length">Data Empty!</span></td>
                        <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Marker Pcs</strong></td>
                        <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                        <td><strong>Total Length (+allow)</strong></td>
                        <td><span id="detail_view_allow">Data Empty!</span></td>
                        <td><strong>Part</strong></td>
                        <td><span id="detail_view_part">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Total Grmt</strong></td>
                        <td><span id="detail_view_total_garment">Data Empty!</span></td>
                        <td><strong>Layer</strong></td>
                        <td><span id="detail_view_layer">Data Empty!</span></td>
                        <td><strong>Plan</strong></td>
                        <td><span id="detail_view_plan">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Po Buyer</strong></td>
                        <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                        <td><strong>Ratio</strong></td>
                        <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>History</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable">
                <thead>
                    <tr>
                        <th class="change-bg-header">#</th>
                        <th class="change-bg-header">Spreading</th>
                        <th class="change-bg-header">Approve GL</th>
                        <th class="change-bg-header">Approve QC</th>
                        <th class="change-bg-header">Cutting</th>
                        <th class="change-bg-header">Bundling</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="change-bg-header">Status</td>
                        <td><span id="spreading_status">-</span></td>
                        <td><span id="approve_gl_status">-</span></td>
                        <td><span id="approve_qc_status">-</span></td>
                        <td><span id="cutting_status">-</span></td>
                        <td><span id="bundling_status">-</span></td>
                    </tr>
                    <tr>
                        <td class="change-bg-header">Date/Finish</td>
                        <td><span id="spreading_date">-</span></td>
                        <td><span id="approve_gl_date">-</span></td>
                        <td><span id="approve_qc_date">-</span></td>
                        <td><span id="cutting_date">-</span></td>
                        <td><span id="bundling_date">-</span></td>
                    </tr>
                    <tr>
                        <td class="change-bg-header">By</td>
                        <td><span id="spreading_by">-</span></td>
                        <td><span id="approve_gl_by">-</span></td>
                        <td><span id="approve_qc_by">-</span></td>
                        <td><span id="cutting_by">-</span></td>
                        <td><span id="bundling_by">-</span></td>
                    </tr>
                    <tr>
                        <td class="change-bg-header">Table</td>
                        <td><span id="spreading_table">-</span></td>
                        <td><span>-</span></td>
                        <td><span>-</span></td>
                        <td><span id="cutting_table">-</span></td>
                        <td><span id="bundling_table">-</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('markerTrace.scanBarcode') }}" id="scanBarcodeLink"></a>
@endsection

@section('page-modal')
	@include('request_marker.marker_trace._modal_spreading_track')
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {
            $('.input-new').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();

                    var barcode_id = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url: $('#scanBarcodeLink').attr('href'),
                        data: {
                            barcode_id: barcode_id,
                        },
                        beforeSend: function() {
                            $('.panel').block({
                                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: '10px 15px',
                                    color: '#fff',
                                    width: 'auto',
                                    '-webkit-border-radius': 2,
                                    '-moz-border-radius': 2,
                                    backgroundColor: '#333'
                                }
                            });
                        },
                        success: function(response) {
                            $('.panel').unblock();
                            $('#detail_view_barcode_marker').text(response.data.barcode_id);
                            $('#detail_view_style').text(response.style);
                            $('#detail_view_cut').text(response.data.cut);
                            $('#detail_view_layer').text(response.layer);
                            $('#detail_view_length').text(response.data.marker_length);
                            $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                            $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                            $('#detail_view_part').text(response.data.part_no);
                            $('#detail_view_plan').text(response.plan);
                            $('#detail_view_color_name').text(response.color_name);
                            $('#detail_view_color_code').text(response.color_code);
                            $('#detail_view_item').text(response.item_code);
                            $('#detail_view_deliv').text(response.stat_date);
                            $('#detail_view_width').text(response.marker_width);
                            $('#detail_view_article').text(response.article);
                            $('#detail_view_marker_pcs').text(response.ratio);
                            $('#detail_view_total_garment').text(response.total_garment);
                            $('#detail_view_ratio_size').text(response.ratio_size);
                            $('#detail_view_po_buyer').text(response.po_buyer);
                            $('#spreading_status').text(response.spreading_status);
                            $('#spreading_date').text(response.spreading_date);
                            $('#spreading_by').text(response.spreading_by);
                            $('#spreading_table').text(response.spreading_table);
                            $('#approve_gl_status').text(response.approve_gl_status);
                            $('#approve_gl_date').text(response.approve_gl_date);
                            $('#approve_gl_by').text(response.approve_gl_by);
                            $('#approve_qc_status').text(response.approve_qc_status);
                            $('#approve_qc_date').text(response.approve_qc_date);
                            $('#approve_qc_by').text(response.approve_qc_by);
                            $('#cutting_status').text(response.cutting_status);
                            $('#cutting_date').text(response.cutting_date);
                            $('#cutting_by').text(response.cutting_by);
                            $('#cutting_table').text(response.cutting_table);
                            $('#bundling_status').text(response.bundling_status);
                            $('#bundling_date').text(response.bundling_date);
                            $('#bundling_by').text(response.bundling_by);
                            $('#bundling_table').text(response.bundling_table);
                            $('#barcode_marker').val('');
                            $('#barcode_marker').focus();
                        },
                        error: function(response) {
                            $('.panel').unblock();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });
    $('#spreading_trace').click(function(){
        var barcode_id = $('#detail_view_barcode_marker').text();
        console.log(barcode_id);
        detail(barcode_id);
    })
});
        function detail(id)
        {
            $.ajax({
                type: "get",
                url: "/review-spreading-result/get-data-detail/"+id,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                }
            })
            .done(function (response) {
                console.log(response);
                $('#detail_view_barcode').text(response.barcode_id);
                $('#detail_view_style2').text(response.style);
                $('#detail_view_cut2').text(response.cut);
                $('#detail_view_item2').text(response.item_code);
                var deliv_date = response.deliv_date.split('-');
                deliv_date = deliv_date[2]+'-'+deliv_date[1]+'-'+deliv_date[0]
                $('#detail_view_deliv2').text(deliv_date);
                $('#detail_view_width2').text(response.fabric_width - 0.5);
                $('#detail_view_color_code2').text(response.color_code);
                $('#detail_view_article2').text(response.articleno);
                $('#detail_view_length2').text(response.marker_length);
                $('#detail_view_color_name2').text(response.color_name);
                $('#detail_view_color_code2').text(response.color_code);
                $('#detail_view_item_code2').text(response.item_code);
                $('#detail_view_marker_pcs2').text(response.qty_ratio * response.layer);
                $('#detail_view_allow2').text(parseFloat(response.marker_length) + 0.75);
                $('#detail_view_part2').text(response.part_no);
                $('#detail_view_total_garment2').text(response.qty_ratio);
                $('#detail_view_layer2').text(response.layer);
                $('#detail_view_layer_plan2').text(response.layer_plan);
                $('#detail_view_list_po2').text(response.list_po);
                $('#detail_view_list_ratio2').text(response.list_ratio);
                var cutting_date = response.cutting_date.split('-');
                cutting_date = cutting_date[2]+'-'+cutting_date[1]+'-'+cutting_date[0]
                $('#detail_view_plan2').text(cutting_date);
                show_data_detail(response.data_detail);
                $('#detailModal').modal();
            });
        }

        function show_data_detail(data){
            $.each(data, function(index, value) {
                $('#fabricTable > tbody').append($('<tr>')
                    .append($('<td>').text(value.no_roll))
                    .append($('<td>').text(value.lot))
                    .append($('<td>').text(parseFloat(value.qty_fabric).toFixed(2)))
                    .append($('<td>').text(value.actual_width))
                    .append($('<td>').text(value.suplai_layer))
                    .append($('<td>').text(value.actual_layer))
                    .append($('<td>').text(parseFloat(value.suplai_sisa).toFixed(2)))
                    .append($('<td>').text(parseFloat(value.actual_sisa).toFixed(2)))
                    .append($('<td>').text(value.akumulasi_layer))
                    .append($('<td>').text(value.reject))
                    .append($('<td>').text(value.sambungan))
                    .append($('<td>').text(value.sambungan_end))
                    .append($('<td>').text(parseFloat(value.actual_use).toFixed(2)))
                    .append($('<td>').text(parseFloat(value.remark).toFixed(2)))
                    .append($('<td>').text(value.kualitas_gelaran))
                );
                console.log(value.id);
            });
        }
    </script>
@endsection
