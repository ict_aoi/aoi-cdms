<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RasioMarker extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'ratio';
    protected $fillable = ['id_marker','ratio','created_at','updated_at','size'];
    
    public function cutting_marker()
    {
        return $this->belongsTo('App\Models\CuttingMarker', 'id_marker', 'barcode_id');
    }

    public function po_details()
    {
        return $this->hasMany('App\Models\MarkerDetailPO', 'ratio_id', 'id');
    }

    public function po_detail_temps()
    {
        return $this->hasMany('App\Models\RatioDetailTemp', 'ratio_id', 'id');
    }
}
