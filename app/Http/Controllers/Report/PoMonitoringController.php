<?php

namespace App\Http\Controllers\Report;

use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use App\Models\DistribusiMovement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PoMonitoringController extends Controller
{
    public function index()
    {
        return view('report.po_monitoring.index');
    }

    public function getDataMonitoring(Request $request){

        if ($request->ajax()){
            $poreference = $request->po_buyer;
            $factory = \Auth::user()->factory_id;
            if($poreference != null){
                $data = DB::SELECT(DB::RAW("SELECT z.poreference,z.style,z.season,z.article,z.size,
                CONCAT(z.part,'+',z.komponen_name) AS komponen_name,z.process_name,
                dor.ordered_qty,
                SUM(z.qty_cut) AS qty_cut,
                SUM(z.qty_fuse) AS qty_fuse,
                SUM(z.qty_ppa1) AS qty_ppa1,
                SUM(z.qty_ppa2) AS qty_ppa2,
                SUM(z.qty_pad) AS qty_pad,
                SUM(z.qty_he) AS qty_he,
                SUM(z.qty_auto) AS qty_auto,
                SUM(z.qty_artwork) AS qty_artwork,
                SUM(z.qty_setting) AS qty_setting,
                SUM(z.qty_supply) AS qty_supply
                FROM
                ---z table---
                (SELECT y.poreference,y.season,
                CASE WHEN y.type_name = 'TOP' THEN CONCAT(y.style,'-TOP') WHEN y.type_name = 'BOTTOM' THEN CONCAT(y.style,'-BOT') ELSE y.style END AS style,y.article,y.part,y.komponen_name,y.process_name,y.size,y.cut_num,y.start_no,y.end_no,
                CASE WHEN MIN(y.qty_cut) = 0 THEN 0 ELSE MAX(y.qty_cut) END AS qty_cut,
                MAX(y.qty_fuse) as qty_fuse,
                MAX(y.qty_ppa1) as qty_ppa1,
                MAX(y.qty_ppa2) as qty_ppa2,
                MAX(y.qty_pad) as qty_pad,
                MAX(y.qty_he) qty_he,
                MAX(y.qty_auto) as qty_auto,
                MAX(y.qty_artwork) as qty_artwork,
                MAX(y.qty_setting) as qty_setting,
                MAX(y.qty_supply) as qty_supply
                FROM
                ---y table---
                (SELECT x.barcode_id,x.poreference,x.season,x.style,x.type_name,x.article,x.size,x.part,x.komponen_name,x.process,x.process_name,x.cut_num,x.start_no,x.end_no,
                MAX(x.qty_cut) as qty_cut,
                CASE WHEN MAX(x.qty_fuse) is null and x.process_name like '%FUSE%' THEN 0 ELSE MAX(x.qty_fuse)END AS qty_fuse,
                CASE WHEN MAX(x.qty_ppa1) is null and x.process_name like '%PPA%' THEN 0 ELSE MAX(x.qty_ppa1)END AS qty_ppa1,
                CASE WHEN MAX(x.qty_ppa2) is null AND x.process_name like '%PPA2%' THEN 0 ELSE MAX(x.qty_ppa2) END AS qty_ppa2,
                CASE WHEN MAX(x.qty_pad) is null AND x.process_name like '%PAD%' THEN 0 ELSE MAX(x.qty_pad) END AS qty_pad,
                CASE WHEN MAX(x.qty_he) is null and x.process_name like '%HE%' THEN 0 ELSE MAX(x.qty_he)END AS qty_he,
                CASE WHEN x.process_name like '%TEMPLATE%' or x.process_name like '%AUTO%' or x.process_name like '%BOBOK%' or x.process_name like '%AMS%' AND MAX(x.qty_auto) is not null THEN MAX(x.qty_auto) ELSE null END AS qty_auto,
                CASE WHEN x.process_name like '%EMBRO%' or x.process_name like '%PRINT%' and MAX(x.qty_artwork) is not null THEN MAX(x.qty_artwork) ELSE null END AS qty_artwork,
                CASE WHEN MAX(x.qty_setting) is null THEN 0 ELSE MAX(x.qty_setting)END AS qty_setting,
                CASE WHEN MAX(x.qty_supply) is null THEN 0 ELSE MAX(x.qty_supply) END AS qty_supply
                FROM
                ---x table---
                (SELECT bd.barcode_id,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,vms.part,bd.komponen_name,vms.process,vms.process_name,bh.cut_num,bd.start_no,bd.end_no,bd.qty,
                COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN bd.qty END,0) AS qty_cut,
                CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN bd.qty END AS qty_fuse,
                CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN bd.qty END AS qty_ppa1,
                CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN bd.qty END AS qty_ppa2,
                CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN bd.qty END AS qty_pad,
                CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN bd.qty END AS qty_he,
                CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN bd.qty END AS qty_auto,
                CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN bd.qty END AS qty_artwork,
                CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN bd.qty END AS qty_setting,
                CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN bd.qty END AS qty_supply
                FROM bundle_detail bd
                JOIN bundle_header bh on bh.id = bd.bundle_header_id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                LEFT JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
                WHERE bh.poreference='$poreference' AND bh.factory_id = '$factory'
                UNION
                SELECT sdm.sds_barcode as barcode_id,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,vms.part,sdm.komponen_name,vms.process,vms.process_name,shm.cut_num,sdm.start_no,sdm.end_no,sdm.qty,
                COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN sdm.qty END,0) AS qty_cut,
                CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN sdm.qty END AS qty_fuse,
                CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN sdm.qty END AS qty_ppa1,
                CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN sdm.qty END AS qty_ppa2,
                CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN sdm.qty END AS qty_pad,
                CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN sdm.qty END AS qty_he,
                CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN sdm.qty END AS qty_auto,
                CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN sdm.qty END AS qty_artwork,
                CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN sdm.qty END AS qty_setting,
                CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN sdm.qty END AS qty_supply
                FROM sds_detail_movements sdm
                JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
                JOIN v_master_style vms on vms.id = sdm.style_id
                LEFT JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
                WHERE shm.poreference='$poreference' AND shm.factory_id = '$factory'
                ORDER BY size,cut_num,start_no,end_no)x
                ---end of x table---
                JOIN v_master_style vms on vms.style = x.style and vms.season_name = x.season
                GROUP BY
                x.size,vms.style,x.cut_num,x.start_no,x.end_no,x.part,x.komponen_name,x.process,x.process_name,x.barcode_id,x.poreference,x.season,x.style,x.type_name,x.article
                )y
                ---end of y table---
                GROUP BY
                y.poreference,y.season,y.style,y.type_name,y.part,y.komponen_name,y.process_name,y.size,y.cut_num,y.article,y.start_no,y.end_no)z
                ---end of z table---
                LEFT JOIN (SELECT DISTINCT po_buyer,style,size_finish_good,ordered_qty
                FROM
                data_cuttings
                where po_buyer='$poreference' or job_no like '%$poreference%')dor on dor.po_buyer = z.poreference AND dor.style = z.style AND dor.size_finish_good = z.size
                GROUP BY
                z.poreference,z.season,z.style,z.article,z.size,z.part,z.komponen_name,z.process_name,dor.ordered_qty"));
            }else{
                $data = [];
            }
            return DataTables::of($data)
            ->editColumn('komponen_name',function($data){
                if($data->process_name == null ){
                    return $data->komponen_name;
                }else{
                    return $data->komponen_name.' <b class="text-danger"><i>('.$data->process_name.')</i></b>';
                }
            })
            //BALANCED CUT
            ->addColumn('balanced_cut',function($data){
                return $data->ordered_qty - $data->qty_cut;
            })
            //DONE CUT
            ->editColumn('qty_cut',function($data){
                if($data->ordered_qty == $data->qty_cut){
                    return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                }else{
                    return $data->qty_cut;
                }
            })
            //BALANCED FUSE
            ->addColumn('balanced_fuse',function($data){
                if($data->qty_fuse == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_fuse;
                }
            })
            //DONE FUSE
            ->editColumn('qty_fuse',function($data){
                if($data->qty_fuse == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_fuse){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_fuse;
                    }
                }
            })
            //BALANCED PPA1
            ->addColumn('balanced_ppa1',function($data){
                if($data->qty_ppa1 == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_ppa1;
                }
            })
            //DONE PPA1
            ->editColumn('qty_ppa1',function($data){
                if($data->qty_ppa1 == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_ppa1){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_ppa1;
                    }
                }
            })
            //BALANCED PPA2
            ->addColumn('balanced_ppa2',function($data){
                if($data->qty_ppa2 == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_ppa2;
                }
            })
            //DONE PPA2
            ->editColumn('qty_ppa2',function($data){
                if($data->qty_ppa2 == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_ppa2){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_ppa2;
                    }
                }
            })
            //BALANCED PAD
            ->addColumn('balanced_pad',function($data){
                if($data->qty_pad == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_pad;
                }
            })
            //DONE PAD
            ->editColumn('qty_pad',function($data){
                if($data->qty_pad == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_pad){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_pad;
                    }
                }
            })
            //BALANCED HE
            ->addColumn('balanced_he',function($data){
                if($data->qty_he == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_he;
                }
            })
            //DONE HE
            ->editColumn('qty_he',function($data){
                if($data->qty_he == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_he){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_he;
                    }
                }
            })
            //BALANCED AUTO
            ->addColumn('balanced_auto',function($data){
                if($data->qty_auto == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_auto;
                }
            })
            //DONE AUTO
            ->editColumn('qty_auto',function($data){
                if($data->qty_auto == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_auto){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_auto;
                    }
                }
            })
            //BALANCED ARTWORK
            ->addColumn('balanced_artwork',function($data){
                if($data->qty_artwork == null){
                    return '';
                }else{
                    return $data->ordered_qty - $data->qty_artwork;
                }
            })
            //DONE ARTWORK
            ->editColumn('qty_artwork',function($data){
                if($data->qty_artwork == null){
                    return '';
                }else{
                    if($data->ordered_qty == $data->qty_artwork){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_artwork;
                    }
                }
            })
            //BALANCED SETTING
            ->addColumn('balanced_setting',function($data){
                if($data->qty_setting == null){
                    return 0;
                }else{
                    return $data->ordered_qty - $data->qty_setting;
                }
            })
            //DONE SETTING
            ->editColumn('qty_setting',function($data){
                if($data->qty_setting == null){
                    return 0;
                }else{
                    if($data->ordered_qty == $data->qty_setting){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_setting;
                    }
                }
            })
            //BALANCED SUPPLY
            ->addColumn('balanced_supply',function($data){
                if($data->qty_supply == null){
                    return 0;
                }else{
                    return $data->ordered_qty - $data->qty_supply;
                }
            })
            //DONE SUPPLY
            ->editColumn('qty_supply',function($data){
                if($data->qty_cut == 0){
                    return 0;
                }elseif($data->qty_supply == null){
                    return 0;
                }else{
                    if($data->ordered_qty == $data->qty_supply){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }else{
                        return $data->qty_supply;
                    }
                }
            })
            ->rawColumns(['balanced_cut','qty_cut','balanced_fuse','qty_fuse','balanced_ppa1','qty_ppa1','balanced_ppa2','qty_ppa2','balanced_pad','qty_pad','balanced_he','qty_he','balanced_auto','qty_auto','balanced_artwork','qty_artwork','balanced_setting','qty_setting','balanced_supply','qty_supply','komponen_name'])
            ->make(true);
        }else {
            $datax = array();
            return DataTables::of($datax)
            ->make(true);
        }
    }
}
