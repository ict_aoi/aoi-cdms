@extends('layouts.app',['active' => 'bundle_movement_report'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Bundle Movement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Bundle Movement</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<style type="text/css">
	th{
		text-align: center;
	}
	.text-wrap{
        white-space:normal;
    }
    .width-100{
        width:130px;
        margin-left:0px;
    }
	.dataTables_filter input {
		outline: 0;
		width: 350px !important;
		height: 38px;
		padding: 8px 0;
		padding-right: 24px;
		font-size: 13px;
		line-height: 1.5384616;
		color: #333333;
		background-color: transparent;
		border: 1px solid transparent;
		border-width: 1px 0;
		border-bottom-color: #ddd;
	}
	.dataTables_length select {
		height: 38px;
		width: 50px !important;
		padding: 8px 0;
		font-size: 13px;
		line-height: 1.5384616;
		color: #333333;
		background-color: transparent;
		border: 1px solid transparent;
		border-bottom-color: #ddd;
		outline: 0;
		border-radius: 0;
		-webkit-transition: all ease-in-out 0.15s;
		-o-transition: all ease-in-out 0.15s;
		transition: all ease-in-out 0.15s;
	}
	.po_cut{
		border		: 1px solid;
		padding		:5px;
		border-radius:30px !important;
		text-align: center;
		background-color: #eee;
	}
</style>
<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">WIP LOCATOR</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">Bundle Locator</a></li>
				<li class=""><a href="#basic-tab3" data-toggle="tab" class="legitRipple" aria-hidden="true">Supply Detail</a></li>
				<li class=""><a href="#basic-tab4" data-toggle="tab" class="legitRipple" aria-hidden="true">Supply Set Movement</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="{{ route('reportComponentMovement.downloadBundleMovement') }}" id="form_filter_date" method="POST">
						<fieldset class="content-group">
                            @csrf
                            <div class="form-group">
								<div class="col-lg-12">
									<div class="alert bg-info alert-styled-left">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">This Report Is Used To Monitor The Status Of Selected WIP Locator!</span>
									</div>
								</div>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-sync"></i></button>
										</span>
										<select data-placeholder="Select a locator..." class="form-control select-search" name="locator" id="locator" required>
											<option value=""></option>
												@foreach($locators as $locator)
													<option value="{{ $locator->id }}">{{ $locator->locator_name }}</option>
												@endforeach
										</select>
									</div>
                                </div>
								<div class="col-lg-2">
								<span class="input-group-btn">
									<button class="btn btn-primary legitRipple" type="submit">Download</button>
								</span>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
									<div class="col-md-5">
										<div class="input-group col-md-12">
											<input type="text" class="form-control po_cut" name="po_buyer" id="po_buyer" placeholder="Masukkan PO Buyer..!" onkeydown="return (event.keyCode!=13);"/>
										</div>
									</div>
									<div class="col-md-3">
										<div class="input-group col-md-12">
											<input type="text" class="form-control po_cut" name="cut_num" id="cut_num" placeholder="Masukkan No Cut..!" onkeydown="return (event.keyCode!=13);"/>
										</div>
									</div>
									<div class="col-md-2">
										<div class="input-group col-md-12">
											<input type="text" class="form-control po_cut" name="article_num" id="article_num" placeholder="Masukkan Article..!"/>
										</div>
									</div>
									<div class="col-md-2">
										<div class="input-group">
											<span>
												<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
											</span>
										</div>
									</div>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<span class="input-group-btn">
								<button class="btn btn-xs btn-success legitRipple s1" type="button" id="set">SETTING</button>
								<button class="btn btn-xs bg-slate-400 legitRipple s2 hidden" type="button" id="set2">SETTING</button>
								<button class="btn btn-xs btn-info legitRipple" type="button" id="exportExcel">DOWNLOAD</button>
							</span>
							<div class="table-responsive">
								<table class="table table-basic table-condensed table-hover" id="table-list">
									<thead>
										<tr class="bg-grey-300">
											<!-- <th>No</th> -->
											<th>DIST</th>
											<th>SEW</th>
											<th class="bg-info">Scan Setting</th>
											<th class="bg-info">Last Updated</th>
											<th class="bg-info">Last Locator</th>
											<!-- <th>Season</th> -->
											<th>Style</th>
											<!-- <th>PO Buyer</th> -->
											<th>Article</th>
											<th>Cut | Sticker</th>
											<th>Size</th>
											<th>Komponen</th>
											<th>Qty</th>
											<th class="bg-info">Detail Loc</th>
											<th>User</th>
											<th>Barcode Number</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="basic-tab3">
					<form class="form-horizontal" action="{{ route('reportComponentMovement.downloadSupplymovement') }}" id="supply_move_report" method="POST">
						<fieldset class="content-group">
                            @csrf
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Scan Supply</label>
								<div class="col-lg-10">							
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="scan_supply" placeholder="Masukkan Tanggal Scan" id="scan_supply">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab4">
					<form class="form-horizontal" action="{{ route('reportComponentMovement.downloadSupplyset') }}" id="supply_move_report_set" method="POST">
						<fieldset class="">
							@csrf
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Scan Supply</label>
								<div class="col-lg-10">							
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="supply_date" placeholder="Masukkan Tanggal Supply" id="supply_date">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('reportComponentMovement.exportExcel') }}" id="export_data"></a>
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        // $('#locator').on('change', function() {
        //     $("#state").html('');
        //     $("#state").append($('<option>').attr('value', 'out').text('OUT')).append($('<option>').attr('value', 'in').text('IN'));
        //     if(this.value == 10) {
        //         $("#state option[value='in']").remove();
        //     }
        //     if(this.value == 11) {
        //         $("#state option[value='out']").remove();
        //     }
        // });
		//DATATABLES
		var po_buyer_field = document.getElementById("po_buyer");
		po_buyer_field.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("btn-filter").click();
			}
		});
		var cut_num = document.getElementById("cut_num");
		cut_num.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("btn-filter").click();
			}
		});
			var po_buyer =$('#po_buyer').val();
			var cut_num =$('#cut_num').val();
			var artcile_num =$('#artcile_num').val();
			var url = "{{route('reportComponentMovement.getDataDetailmovement')}}";
			var tableL = $('#table-list').DataTable({
				processing: true,
				serverSide: true,
				deferRender:true,
				// pageLength:10,
				destroy:true,
				scrollX:true,
				scrollY:true,
				dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
				language: {
					search: '<span>Filter:</span> _INPUT_',
					searchPlaceholder: 'Type to filter...',
					lengthMenu: '<span>Show:</span> _MENU_',
					paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
				},
				ajax: {
					type: 'GET',
					url: url,
					data: function(d) {
						return $.extend({}, d, {
							"po_buyer"		: $('#po_buyer').val(),
							"cut_num"		: $('#cut_num').val(),
							"article_num"		: $('#article_num').val(),
						});
					},
				},
				// fnCreatedRow: function (row, data, index) {
				// 	var info = tableL.page.info();
				// 	var value = index+1+info.start;
				// 	$('td', row).eq(0).html(value);
				// },
				columnDefs: [
					// { className: 'text-break', targets: [2,3,9,10,12] },
					{ className: 'text-center', targets: [0,1,11,8] },
					{
						render: function (data, type,full, meta) {
							return "<div class='text-wrap width-100'>" + data + "</div>";
						},
						targets: [2,3,9,12]
					},
				],
				columns: [
					// {data: null, sortable: false, orderable: false, searchable: false},//0
					{data: 'is_supplied', name: 'is_supplied',searchable:false,visible:true,orderable:false},//0
					{data: 'sewing_confirm', name: 'sewing_confirm',searchable:false,visible:true,orderable:false},//1
					{data: 'scan_setting', name: 'scan_setting',searchable:false,visible:false,orderable:true},//2
					{data: 'updated_movement_at', name: 'updated_movement_at',searchable:false,visible:true,orderable:true},//3
					{data: 'current_description', name: 'current_description',searchable:true,visible:true,orderable:true},//4
					// {data: 'season', name: 'season',searchable:false,visible:true,orderable:false},
					{data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//5
					// {data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:false},
					{data: 'article', name: 'article',searchable:false,visible:true,orderable:false},//6
					{data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//7
					{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//8
					{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//9
					{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},//10
					{data: 'line', name: 'line',searchable:true,visible:true,orderable:true},//11
					{data: 'user_scan', name: 'user_scan',searchable:true,visible:true,orderable:true},//12
					{data: 'barcode_id', name: 'barcode_id',searchable:false,visible:false,orderable:true},//13
				]
			});
	
			var tableL = $('#table-list').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						tableL.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						tableL.search("").draw();
					}
					return;
			});
			tableL.on('preDraw',function(){
				Pace.start();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			})
			.on('draw.dt',function(){
				Pace.stop();
				$.unblockUI();
			});

		$('#exportExcel').on('click', function(){
			event.preventDefault();
			var po_buyer    = $('#po_buyer').val();
			var cut_num    = $('#cut_num').val();
			if(po_buyer =='') {
				$("#alert_warning").trigger("click", 'PO Buyer Tidak Boleh Kosong!');
				return false;
			}
			window.open(
				$('#export_data').attr('href')
				+ '?po_buyer=' + po_buyer
				+ '&cut_num=' + cut_num
			);

		});
	
		$('#btn-filter').click(function(){
			event.preventDefault();
			var po_buyer    = $('#po_buyer').val();
			// var cut_num    = $('#cut_num').val();
			if(po_buyer =='') {
				$("#alert_warning").trigger("click", 'PO Buyer Tidak Boleh Kosong!');
				return false;
			}
			// if(po_buyer != '' && cut_num =='') {
			// 	$("#alert_warning").trigger("click", 'Cut Number Tidak Boleh Kosong!');
			// 	return false;
			// }
			$.unblockUI();
			tableL.draw();
		});
		var user 		= "{{ Auth::user()->nik }}";
		const acc		= ['171000316','131005357'];
		var x = acc.includes(user);
		$('#set').on('click', function () {
			$('.s1').addClass('hidden');
			$('.s2').removeClass('hidden');
			var isVisible = tableL.column('scan_setting:name').visible();
			var isVisible = tableL.column('barcode_id:name').visible();
			tableL.column('scan_setting:name').visible( !isVisible );
			if(x){
				tableL.column('barcode_id:name').visible( !isVisible );
			}
		});
		$('#set2').on('click', function () {
			$('.s2').addClass('hidden');
			$('.s1').removeClass('hidden');
			var isVisible = tableL.column('scan_setting:name').visible();
			var isVisible = tableL.column('barcode_id:name').visible();
			tableL.column('scan_setting:name').visible( !isVisible );
			if(x){
				tableL.column('barcode_id:name').visible( !isVisible );
			}
		});
    });
</script>
@endsection