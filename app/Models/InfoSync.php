<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class InfoSync extends Model
{
     use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'infos';
	protected $fillable = ['type','cutting_date','style','articleno','po_buyer','desc','documentno','is_integrated'];
}
