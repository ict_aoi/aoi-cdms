<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\CombinePart;
use App\Models\DetailPlan;
use App\Models\RasioMarker;
use App\Models\MarkerDetailPO;
use App\Models\CuttingMovement;
use App\Http\Controllers\Controller;

class PrintIDMarkerController extends Controller
{
    public function index()
    {
        return view('request_marker.print_id_marker.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                    ->where('cutting_date', $cutting_date)
                    ->where('factory_id', \Auth::user()->factory_id)
                    ->where('is_header', true)
                    ->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                    ->where('cutting_date', $cutting_date)
                    ->where('is_header', true)
                    ->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->addColumn('status', function($data) {
                    if (count($data->cutting_markers) > 0) return '<span class="badge bg-primary">File Uploaded</span>';
                    else return '<span class="badge bg-danger">Not Yet</span>';
                })
                ->addColumn('last_updated_by', function($data) {
                    $get_marker = CuttingMarker::where('id_plan', $data->id)->whereNotNull('ratio_updated_at')->orderBy('ratio_updated_at', 'desc')->first();
                    if($get_marker != null) {
                        return $get_marker->user->name;
                    } else {
                        return '-';
                    }
                })
                ->addColumn('po_buyer', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                        ->whereNull('deleted_at')
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details()
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    }

                    // $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    // return implode(', ', $po_array);
                })
                ->addColumn('detail', function($data) {
                    return view('request_marker.print_id_marker._action', [
                        'model'      => $data,
                        'detail'     => route('printIDMarker.detailModal',$data->id)
                    ]);
                })
                ->rawColumns(['detail', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;

            if($planning_id != NULL) {
                $data = CuttingMarker::orderBy('part_no', 'asc')->orderBy('cut', 'asc')->where('id_plan', $planning_id)->where('deleted_at', null);

                return datatables()->of($data)
                ->addColumn('rasio', function($data) {
                    $data_temp = array();
                    foreach($data->rasio_markers as $a) {
                        $data_temp[] = $a->size.'/'.$a->ratio;
                    }
                    if($data->is_new == null) {
                        return implode(', ', $data_temp);
                    } else {
                        return implode(', ', $data_temp).' - LAPORAN BARU';
                    }
                })
                ->addColumn('status', function($data) {
                    if($data->download_at != null) {
                        return '<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>';
                    } else {
                        return '<span class="label label-default label-icon"><i class="icon-cross3"></i></span>';
                    }
                })
                ->addColumn('selector', function($data) {
                    if($data->is_new == null) {
                        if($data->marker_length != null && $data->perimeter != null) {
                            return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'">';
                        } else {
                            return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'" disabled="disabled">';
                        }
                    } else {
                        return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'" disabled="disabled">';
                    }
                })
                ->rawColumns(['selector','rasio', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('status', function($data) {
                    return null;
                })
                ->addColumn('rasio', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['action','rasio','status'])
                ->make(true);
            }
        }
    }

    public function dataDetailMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            if($planning_id != NULL) {
                $data = CuttingMarker::orderBy('cut', 'asc')->where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                return datatables()->of($data)
                ->addColumn('rasio', function($data) {
                    $data_temp = array();
                    foreach($data->rasio_markers as $a) {
                        $data_temp[] = $a->size.'/'.$a->ratio;
                    }
                    return implode(', ', $data_temp);
                })
                ->addColumn('lebar_fb', function($data) {
                    return $data->fabric_width == null ? 0 : $data->fabric_width;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return $data->qty_fabric == null ? 0 : $data->qty_fabric;
                })
                ->addColumn('perimeter', function($data) {
                    return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="perimeter-'.$data->id.'" onkeypress="isNumberDot(event)" id="perimeter" value="'.$data->perimeter.'" /></div></div>';
                })
                ->addColumn('actual_marker', function($data) {
                    return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="actual_marker-'.$data->id.'" onkeypress="isNumberDot(event)" id="actual_marker" value="'.$data->marker_length.'" /></div></div>';
                })
                ->addColumn('over_consum', function($data) {
                    $color = $data->over_consum >= 0 ? 'bg-success-300' : 'bg-danger-300';
                    return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control '.$color.'" name="over_consum" onkeypress="isNumberDot(event)" readonly id="over_consum" value="'.$data->over_consum.'"/></div></div>';
                })
                ->addColumn('download', function($data) {
                    return view('request_marker.print_id_marker._action', [
                        'model'      => $data,
                        'detail'     => route('printIDMarker.detailModal',$data->id)
                    ]);
                })
                ->rawColumns(['perimeter', 'actual_marker', 'over_consum','download'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('id', function($data) {
                    return null;
                })
                ->editColumn('cut', function($data) {
                    return null;
                })
                ->editColumn('color', function($data) {
                    return null;
                })
                ->editColumn('marker', function($data) {
                    return null;
                })
                ->editColumn('layer', function($data) {
                    return null;
                })
                ->editColumn('part_no', function($data) {
                    return null;
                })
                ->addColumn('rasio', function($data) {
                    return null;
                })
                ->addColumn('lebar_fb', function($data) {
                    return null;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return null;
                })
                ->addColumn('perimeter', function($data) {
                    return null;
                })
                ->addColumn('actual_marker', function($data) {
                    return null;
                })
                ->addColumn('over_consum', function($data) {
                    return null;
                })
                ->addColumn('download', function($data) {
                    return null;
                })
                ->rawColumns(['perimeter', 'actual_marker', 'over_consum','download'])
                ->make(true);
            }
        }
    }

    public function detailModal($id)
    {
        $data_detail = CuttingPlan::where('id', $id)->where('deleted_at', null)->first();
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $data_detail->cutting_date;
        $obj->style = $data_detail->style;
        $obj->articleno = $data_detail->articleno;
		return response()->json($obj,200);
    }

    public function detailDetailModal($id, $part_no)
    {
        $upload = CuttingMarker::where('id_plan', $id)->where('part_no', $part_no)->where('deleted_at', null)->first();
        $obj = new StdClass();
        $obj->id_plan = $upload->id_plan;
        $obj->part_no = $upload->part_no;
		$obj->url_upload = route('printIDMarker.updateDataMarker');

		return response()->json($obj,200);
    }

    public function updateDataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $data = $request->data;
            $planning_id = $request->planning_id;
            $part_no = '';

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    $data_cutting = CuttingMarker::findOrFail($data[$i][0]);
                    $data_cutting->perimeter = $data[$i][9];
                    $data_cutting->marker_length = $data[$i][10];
                    $data_cutting->over_consum = $data[$i][11];
                    $data_cutting->save();
                }
                $part_no = $data[0][6];
            }

            return response()->json(['id_plan' => $planning_id, 'part_no' => $part_no],200);
        }
    }

    public function printId(Request $request)
    {
        if(request()->ajax())
        {
            $data = $request->data;
            $planning_id = $request->planning_id;
            $print_type = $request->print_type;

            $new_data = array();
            $data_result = array();

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    if($data[$i][7] == 'true') {
                        $new_data[] = $data[$i][0];
                    }
                }
            }

            $last_po_buyer = '';

            $barcode_marker = CuttingMarker::where('id_plan', $planning_id)
            ->orderBy('barcode_id', 'asc')
            ->whereNull('deleted_at')
            ->pluck('barcode_id')->toArray();
            // dd($barcode_marker);
            // foreach marker
            foreach($barcode_marker as $a) {
                // get data detail marker
                $get_data_marker = CuttingMarker::where('barcode_id', $a)->where('deleted_at', null)->where('download_at', null)->first();

                // jika marker ada
                $queue_list   = array();
                $plan_id_list = array();
                if($get_data_marker != null) {

                    $cek_alokasi = DB::table('ratio_detail')->where('barcode_id',$a)->whereNull('deleted_at')->first();

                    if($cek_alokasi == null){
                        try {
                            DB::begintransaction();

                            $queue_list = array();
                            if($get_data_marker->combine_id === null) {
                                $queue_list[]   = $get_data_marker->cutting_plan->queu;
                                $plan_id_list[] = $get_data_marker->id_plan;
                            } else {
                                $plan_id_list = CombinePart::where('combine_id', $get_data_marker->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                                foreach($plan_id_list as $planid) {
                                    $get_data_plan = CuttingPlan::where('id', $planid)->where('deleted_at', null)->first();
                                    $queue_list[] = $get_data_plan->queu;
                                }
                            }

                            // get informasi part in marker
                            $part_no = explode('+', $get_data_marker->part_no);

                            // set value sisa menjadi 0
                            $sisa = 0;

                            // foreach ratio size per marker
                            foreach($get_data_marker->rasio_markers as $b) {

                                $qty_marker_size = $get_data_marker->layer * $b->ratio;

                                // if(count($plan_id_list)>1){

                                    $data_plan = CuttingPlan::orderBy('queu', 'asc')
                                    ->whereIn('id', $plan_id_list)
                                    ->where('is_header', true)
                                    ->where('deleted_at', null)
                                    ->get();
                                // }
                                // else{
                                //     $data_plan = CuttingPlan::orderBy('queu', 'asc')
                                //     ->where('id', $plan_id_list)
                                //     ->where('is_header', true)
                                //     ->where('deleted_at', null)
                                //     ->get();
                                // }

                                $plan_list = array();
                                foreach($data_plan as $dp){
                                    if($dp->header_id != null && $dp->is_header) {
                                        $get_plan_id = CuttingPlan::where('header_id', $dp->id)
                                            ->whereNull('deleted_at')
                                            ->pluck('id')
                                            ->toArray();
                                        $plan_list = array_merge($plan_list, $get_plan_id);
                                    } else {
                                        $plan_list[] = $dp->id;
                                    }
                                }

                                // get qty per po per part no per size -> planning
                                // $get_list_po = DB::table('jaz_detail_size_per_part')->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))->where('cutting_date', $get_data_marker->cutting_plan->cutting_date)->whereIn('queu', $queue_list)->where('part_no', $part_no[0])->where('factory_id', $get_data_marker->cutting_plan->factory_id)->orderBy('deliv_date', 'asc')->orderBy('qty', 'asc')->groupBy('po_buyer','deliv_date','part_no')->get();
                                // if(count($get_list_po) < 1) {
                                //     return response()->json(['data' => $new_data, 'status' => 'null', 'print_type' => $print_type, 'message' => 'mo cutting untuk planning ini geser, silakan geser plan terlebih dahulu disistem!']);
                                // }

                                $get_list_po = DB::table('jaz_detail_size_per_part_2')
                                    // ->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))
                                    ->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))
                                    ->whereIn('plan_id', $plan_list)
                                    // ->where('part_no', $part_no[0])
                                    //IN CASE EVEN JIKA DEFINE PART TIDAK STANDAR ex(part_no = Collar,COLLAR)
                                    ->whereIn('part_no', [$part_no[0],strtoupper($part_no[0])])
                                    ->where('size_finish_good', $b->size)
                                    ->where('factory_id', $get_data_marker->cutting_plan->factory_id)
                                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , size_finish_good, deliv_date, qty"))
                                    // ->orderBy('deliv_date', 'asc')
                                    // ->orderBy('qty', 'asc')
                                    // ->groupBy('po_buyer','deliv_date','part_no')
                                    ->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')
                                    ->get();

                                if(count($get_list_po) < 1) {
                                    return response()->json(['data' => $a, 'status' => 'null', 'print_type' => $print_type, 'message' => 'mo cutting untuk planning ini geser, silakan geser plan terlebih dahulu disistem!']);
                                }

                                foreach($get_list_po as $c) {
                                    if($qty_marker_size > 0) {
                                        $get_po_plot = MarkerDetailPO::where('plan_id', $get_data_marker->cutting_plan->id)->where('size', $b->size)->where('part_no', $c->part_no)->where('po_buyer', $c->po_buyer)->where('deleted_at', null)->sum('qty');

                                        $get_po_plan = DB::table('jaz_detail_size_per_part_2')
                                        ->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))
                                        ->whereIn('plan_id', $plan_list)
                                        // ->where('part_no', $part_no[0])
                                        //IN CASE EVEN JIKA DEFINE PART TIDAK STANDAR ex(part_no = Collar,COLLAR)
                                        ->whereIn('part_no', [$part_no[0],strtoupper($part_no[0])])
                                        ->where('po_buyer', $c->po_buyer)
                                        ->where('factory_id', $get_data_marker->cutting_plan->factory_id)
                                        ->where('size_finish_good', $b->size)
                                        ->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')
                                        ->first();

                                        // DB::table('jaz_detail_size_per_part')
                                        // ->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))
                                        // ->where('cutting_date', $get_data_marker->cutting_plan->cutting_date)
                                        // ->whereIn('queu', $queue_list)
                                        // ->where('part_no', $part_no[0])
                                        // ->where('po_buyer', $c->po_buyer)
                                        // ->where('factory_id', $get_data_marker->cutting_plan->factory_id)
                                        // ->where('size_finish_good', $b->size)
                                        // ->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')
                                        // ->first();

                                        if($get_po_plan != null) {
                                            if($get_po_plot < $get_po_plan->qty) {
                                                if(($get_po_plan->qty - $get_po_plot) < $qty_marker_size) {
                                                    MarkerDetailPO::FirstOrCreate([
                                                        'barcode_id' => $a,
                                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                                        'ratio_id' => $b->id,
                                                        'po_buyer' => $c->po_buyer,
                                                        'size' => $b->size,
                                                        'qty' => $get_po_plan->qty - $get_po_plot,
                                                        'is_sisa' => 'f',
                                                        'part_no' => $c->part_no,
                                                    ]);

                                                    $qty_marker_size = $qty_marker_size - ($get_po_plan->qty - $get_po_plot);

                                                    $last_ratio_id = $b->id;
                                                    $last_po_buyer = $c->po_buyer;
                                                    $last_size = $b->size;
                                                    $last_part_no = $c->part_no;
                                                } else {
                                                    MarkerDetailPO::FirstOrCreate([
                                                        'barcode_id' => $a,
                                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                                        'ratio_id' => $b->id,
                                                        'po_buyer' => $c->po_buyer,
                                                        'size' => $b->size,
                                                        'qty' => $qty_marker_size,
                                                        'is_sisa' => 'f',
                                                        'part_no' => $c->part_no,
                                                    ]);

                                                    $qty_marker_size = 0;
                                                }
                                            } else {
                                                $last_po_buyer = $c->po_buyer;
                                            }
                                        }
                                    } else {
                                        break;
                                    }
                                }

                                if($qty_marker_size > 0) {
                                    MarkerDetailPO::FirstOrCreate([
                                        'barcode_id' => $a,
                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                        'ratio_id' => $b->id,
                                        'po_buyer' => $last_po_buyer,
                                        'size' => $b->size,
                                        'qty' => $qty_marker_size,
                                        'is_sisa' => 't',
                                        'part_no' => $part_no[0],
                                    ]);
                                }
                            }

                            DB::commit();
                        }
                        catch (Exception $e) {
                            DB::rollback();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }

                // MarkerDetailPO::
            }

            $cutting_plan = CuttingPlan::where('id', $planning_id)->whereNull('deleted_at')->first();
            if($cutting_plan->header_id != null && $cutting_plan->is_header) {
                $get_plan_id = CuttingPlan::where('header_id', $cutting_plan->id)
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->toArray();
                $plan_list = $get_plan_id;
            } else {
                $plan_list[] = $cutting_plan->id;
            }

            $po = DetailPlan::whereIn('id_plan', $plan_list)
            ->whereNull('deleted_at')
            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
            ->pluck('po_buyer')->toArray();

            $check_part = CuttingMarker::where('id_plan', $planning_id)
                        ->whereNull('deleted_at')
                        ->select(DB::raw("distinct part_no"))
                        ->pluck('part_no')->toArray();
            foreach ($check_part as $y => $p){
                $no        = 1;
                foreach ($po as $key => $value) {

                    $cutting_marker = DB::table('hs_suggest_cut')
                                    ->where('id_plan', $planning_id)
                                    ->where('part_no', $p)
                                    ->get();

                    foreach ($cutting_marker as $k => $v) {

                        if($v->suggest_cut == null){
                            if(strpos($v->po_buyer, $value) !== false){
                                CuttingMarker::where('barcode_id',$v->barcode_id)->update([
                                    'suggest_cut'                  => $no
                                ]);
                                $no++;
                            }
                        }
                    }
                }
            }


            if(count($new_data) > 0) {
                return response()->json(['data' => $new_data, 'status' => 'isset', 'print_type' => $print_type, 'message' => null]);
            } else {
                return response()->json(['data' => $new_data, 'status' => 'null', 'print_type' => $print_type, 'message' => 'belum ada data yang dipilih!']);
            }
        }
    }

    public function printIdPdf(Request $request)
    {
        ini_set('max_execution_time', 0);
        //0 set to unlimitted execution
        
        $print_type = $request->print_type_;
        $data = json_decode($request->list_id_marker);

        $barcode_ids = array();

        $data_marker_query = CuttingMarker::whereIn('barcode_id', $data)->orderBy('part_no', 'asc');

        $data_marker_all = $data_marker_query->orderBy('cut', 'asc')->get();

        $data_marker['data_marker_all'] = $data_marker_all;

        if($print_type == 'double') {
            foreach($data_marker_all as $data_marker) {
                $barcode_ids[] = $data_marker->barcode_id;
                $barcode_ids[] = $data_marker->barcode_id;
            }
        }

        foreach($data_marker_all as $marker) {
            CuttingMovement::firstOrCreate([
                'barcode_id' => $marker->barcode_id,
                'process_from' => 'preparation',
                'status_from' => 'onprogress',
                'process_to' => 'preparation',
                'status_to' => 'completed',
                'is_canceled' => false,
                'user_id' => \Auth::user()->id,
                'description' => 'printing',
                'ip_address' => $this->getIPClient(),
                'barcode_type' => 'spreading',
                'deleted_at' => null
            ]);
        }

        $data_marker['barcode_ids'] = $barcode_ids;
        $data_marker['print_type'] = $print_type;

        $data_marker_query->update(['download_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        // return view('print.identitas_marker', $data_marker);

        $pdf = \PDF::loadView('print.identitas_marker', $data_marker)->setPaper('a4', 'landscape');
        return $pdf->stream('identitas_marker_'.$data[0].'-'.end($data).'.pdf');
    }

    public function realokasi(Request $request)
    {
        if(request()->ajax())
        {
            $plan_id = $request->plan_id;
            $data = $request->data;
            $data_new = array();

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    if($data[$i][7] == 'true') {
                        $data_new[] = $data[$i][0];
                    }
                }
            }

            $cutting_plan = CuttingPlan::where('id', $plan_id)->first();

            if(count($data_new) > 0) {
                try {
                    DB::begintransaction();

                    MarkerDetailPO::whereIn('barcode_id', $data_new)->delete();
                    CuttingMarker::whereIn('barcode_id', $data_new)->update(['download_at' => null]);
                    CuttingMovement::whereIn('barcode_id', $data_new)->whereNotIn('process_to', ['preparation'])->whereNotIn('status_to', ['onprogress'])->update(['deleted_at' => null]);

                    DB::commit();
                }
                catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json(['id' => $plan_id, 'cutting_date' => $cutting_plan->cutting_date, 'style' => $cutting_plan->style, 'articleno' => $cutting_plan->articleno, 'status' => 200, 'responseJSON' => 'Data berhasil di realokasi!']);
            } else {
                return response()->json(['id' => $plan_id, 'cutting_date' => $cutting_plan->cutting_date, 'style' => $cutting_plan->style, 'articleno' => $cutting_plan->articleno, 'status' => 422, 'responseJSON' => 'Tidak ada data yang dipilih!']);
            }
        }
    }

    public function approveDataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->id_plan;
            $part_no = $request->part_no;

            $data_cutting = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->update(['approved' => Carbon::now()]);

            return response()->json(['id_plan' => $planning_id, 'part_no' => $part_no],200);
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
