@extends('layouts.app',['active' => 'dashboard'])

@section('page-css')
<style>
	.center-row {
		text-align: center !important;
	}
	.bg-red {
		background-color: red !important;
		color: white !important;
	}
	.bg-yellow {
		background-color: yellow !important;
	}
	.bg-green {
		background-color: green !important;
		color: white !important;
	}
	.bg-blue {
		background-color: blue !important;
		color: white !important;
	}
	.add-margin-right {
		margin-right: 20px;
	}
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

@include('includes.notif_mo_update')

<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
		<button type="button" class="btn btn-primary btn-sm legitRipple pull-right" data-toggle="modal" data-target="#keteranganModal">Keterangan <i class="icon-play3 position-right"></i></button>
		<button type="button" class="btn btn-success btn-sm pull-right add-margin-right" id="download_report"><i class="icon-download"></i> Download</button>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
                        <th rowspan="2">Queue</th>
                        <th rowspan="2">Style</th>
                        <th rowspan="2">Article</th>
                        <th rowspan="2">Color</th>
                        <th rowspan="2">Size Type</th>
                        <th colspan="2" class="center-row">Preparation Status</th>
                    </tr>
					<tr>
                        <th>Marker</th>
                        <th>Fabric</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<form action="{{ route('dashboard.downloadReport') }}" method="post" target="_blank" id="download_report_submit">
	@csrf
	<input id="data_download" name="data_download" type="hidden" value="[]">
	<input id="data_cutting_date" name="data_cutting_date" type="hidden" value="">
	<div class="" style="">
		<button type="submit" class="btn hidden">download</button>
	</div>
</form>

<!-- MODAL KETERANGAN -->

<div id="keteranganModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Keterangan</span>
				</legend>
			</div>
            <br />
			<div class="modal-body">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="inputTable">
						<thead>
                            <tr>
                                <th colspan="2" class="center-row">Marker</th>
                                <th colspan="2" class="center-row">Fabric</th>
                            </tr>
                            <tr>
                                <th>Simbol</th>
                                <th>Keterangan</th>
                                <th>Simbol</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
						<tbody>
							<tr>
								<td class="bg-red">Prioritas Plan</td>
								<td>Belum dibarikan prioritas oleh cutting</td>
								<td class="bg-red">Preparation</td>
								<td>Belum prepare full sesuai qty CSI & lebar fabric belum komplit semua</td>
							</tr>
							<tr>
								<td class="bg-red">Upload SSP</td>
								<td>Belum upload file hasil SSP oleh tim marker</td>
								<td class="bg-yellow">Relax</td>
								<td>Semua roll sudah full scan preparation sesuai qty CSI</td>
							</tr>
							<tr>
								<td class="bg-yellow">Marker Request</td>
								<td>Belum upload permintaan marker oleh cutting</td>
								<td class="bg-green">Scan Out</td>
								<td>Semua roll sudah full scan out warehouse</td>
							</tr>
							<tr>
								<td class="bg-yellow">Marker Width</td>
								<td>Lebar fabric belum complit dilengkapi oleh warehouse</td>
								<td class="bg-blue">Balance Marker</td>
								<td>Ada qty balance marker yang belum scan preparation</td>
							</tr>
							<tr>
								<td class="bg-green">Marker Upload</td>
								<td>File marker sudah diupload oleh tim marker</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="bg-green">Marker Plot</td>
								<td>File marker sudah didownload semua oleh cutting</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="bg-blue">Overconsumpt</td>
								<td>Menunggu Approve MM, kebutuhan fabric untuk actual marker lebih besar dari CSI</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
            </div>
		</div>
	</div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/notif_mo_update.js') }}"></script>
<script src="{{ mix('js/dashboard_monitoring.js') }}"></script>
@endsection
