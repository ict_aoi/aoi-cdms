@extends('layouts.app',['active' => 'scan_out_subcont'])
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Scan Out Subcont</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Scan Out Subcont</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-12">
                    <center><h5>SCAN OUT SUBCONT</h5></center>
                </div>
                <div class="col-md-10">
                    <input type="hidden" class="form-control" style="opacity: 1;" id="user" name="user" value="{{\Auth::user()->id}}" readonly />
                </div>
            </div>
            <div class="row margin-atas">
                <button class="btn btn-primary col-md-12" style="width:100%" id="scan_barcode">Mulai Scan</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Proses Terbaru</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="componentTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Barcode</th>
                        <th>Style | Article</th>
                        <th>Part - Komponen</th>
                        <th>PO</th>
                        <th>Size</th>
                        <th>Cut / Sticker No</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('scanSubcontOut.dataScanout') }}" id="dataLocatorstate"></a>
<a href="{{ route('scanSubcontOut.dataComponent') }}" id="dataComponentLink"></a>
<a href="{{ route('scanSubcontOut.dataScanOutsubcont') }}" id="dataComponentPackinglistLink"></a>
<a href="{{ route('scanSubcontOut.scanComponent') }}" id="scanComponentLink"></a>
<a href="{{ route('scanSubcontOut.saveComponent') }}" id="saveComponentLink"></a>
@endsection

@section('page-modal')
	@include('subcont.scan_out._modal_scan')
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        $('body').addClass('sidebar-xs');
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
        var table_component = $('#componentTable').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            pageLength:50,
            scrollX:true,
            dom: '<"datatable-header"><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('scanSubcontOut.dataScanOutsubcont') }}",
                data: function (d) {
                    return $.extend({},d,{
                        "user": $('#user').val()
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table_component.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id', searchable:true, visible:true, orderable:false},
                {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false}
            ]
        });

        $('#scanModal').on('hidden.bs.modal', function(){
            get_data_component_packinglist($('#user').val());
        });

        $('#scanModal').on('shown.bs.modal', function () {
            $('#scan_component').focus();
        })

        $('#scan_barcode').on('click', function() {
            var user = $('#user').val();
            if(!user){
                $("#alert_warning").trigger("click", 'Terjadi Kesalahan Refresh Halaman Ini!');
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url : $('#dataLocatorstate').attr('href'),
                data: {
                    user : user,
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    $('#scanModal').modal();
                    $('#locator_res').val(response.locator);
                    $('#state_res').val(response.state);
                    $('#user_res').val(user);
                    $('#scan_component').focus();
                    get_data_component(response.locator,response.state,response.user);
                    $.unblockUI();
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });

        $('#scan_component').keypress(function(event){
            if(event.which == 13) {
                event.preventDefault();
                var locator = $('#locator_res').val();
                var state = $('#state_res').val();
                var user = $('#user_res').val();
                var barcode_component = $(this).val();

                if(!barcode_component){
                    $("#alert_warning").trigger("click", 'Scan Barcode Dahulu!');
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url : $('#scanComponentLink').attr('href'),
                    data: {
                        barcode_component: barcode_component,
                    },
                    beforeSend: function() {
                        $('#scanTable').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#scanTable').unblock();
                        $('#scan_component').val('');
                        $('#scan_component').focus();
                        get_data_component(locator, state, user);
                    },
                    error: function(response) {
                        $('#scanTable').unblock();
                        $('#scan_component').val('');
                        $('#scan_component').focus();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });

        $('#save_component').on('click', function() {
            var locator = $('#locator_res').val();
            var state = $('#state_res').val();
            var user = $('#user_res').val();

            if(!locator){
                $("#alert_warning").trigger("click", 'Terjadi Kesalahan Harap Refresh Halaman Ini!');
                return false;
            }
            if(!state){
                $("#alert_warning").trigger("click", 'Terjadi Kesalahan Harap Refresh Halaman Ini!');
                return false;
            }
            if(!user){
                $("#alert_warning").trigger("click", 'Terjadi Kesalahan Harap Refresh Halaman Ini!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url : $('#saveComponentLink').attr('href'),
                data: {
                    locator: locator,
                    state: state,
                    user : user,
                },
                beforeSend: function() {
                    $.blockUI({
                        theme:true,
                        baseZ:2000,
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    $.unblockUI();
                    myalert('success','Done');
                    $('#scanTable').DataTable().ajax.reload();
                },
                error: function(response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        })
    });

    function get_data_component(locator, state, user) {
        var scanTable = $('#scanTable').DataTable({
            destroy: true,
            bFilter: false,
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                url: $('#dataComponentLink').attr('href'),
                data: {
                    locator: locator,
                    state: state,
                    user:user,
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = scanTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
                {
                    targets: 2,
                    className: 'text-center',
                }
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id', searchable:true, visible:true, orderable:false},
                {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false},
                {data: 'action', name: 'action', searchable:false, orderable:false},
            ],
        });
    }

    function get_data_component_packinglist(user) {
        var table_component = $('#componentTable').DataTable({
            destroy: true,
            bFilter: false,
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                url: $('#dataComponentPackinglistLink').attr('href'),
                data: {
                    user : user,
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table_component.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
                {
                    targets: 2,
                    className: 'text-center',
                }
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'barcode_id', searchable:true, visible:true, orderable:false},
                {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false}
            ],
        });

        $('#componentTable').on('click','.ignore-click', function() {
            return false;
        });
    }

    function hapus(url) {
        var locator = $('#locator_res').val();
        var state = $('#state_res').val();
        var user = $('#user').val();

        if(!locator){
            $("#alert_warning").trigger("click", 'Terjadi kesalahan Harap Refresh Halaman Ini!');
            return false;
        }
        if(!state){
            $("#alert_warning").trigger("click", 'Terjadi kesalahan Harap Refresh Halaman Ini!');
            return false;
        }
        if(!user){
            $("#alert_warning").trigger("click", 'Terjadi kesalahan Harap Refresh Halaman Ini!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : url,
            beforeSend: function() {
                $('#scanTable').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $('#scanTable').unblock();
                $('#scan_component').val('');
                $('#scan_component').focus();
                $("#alert_success").trigger("click",'komponen berhasil di hapus');
                get_data_component(locator, state, user);
                console.log(response);
            },
            error: function(response) {
                $('#scanTable').unblock();
                $('#scan_component').val('');
                $('#scan_component').focus();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    }
</script>
@endsection