<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BundleHeader extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'bundle_header';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['season', 'style', 'set_type', 'poreference', 'article', 'cut_num', 'size', 'factory_id', 'cutting_date', 'color_name', 'cutter', 'note', 'admin', 'qc_operator', 'bundle_operator', 'box_lim', 'max_bundle', 'created_at', 'updated_at', 'qty', 'barcode_marker', 'job', 'admin_name', 'qc_operator_name', 'bundle_operator_name', 'statistical_date', 'destination', 'po_reroute', 'job_reroute', 'cut_info', 'size_edit'];
}
