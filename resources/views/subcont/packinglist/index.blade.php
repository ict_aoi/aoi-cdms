@extends('layouts.app',['active' => 'packinglist_subcont'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Packinglist Subcont</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Packinglist Subcont</li>
            </ul>
        </div>
    </div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="tabbable">
            <ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">Print Packinglist</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">Create Packinglist</a></li>
			</ul>
            <div class="tab-content">
                <div class="tab-pane active" id="basic-tab1">
                    <form class="form-horizontal" method="POST">
                        <fieldset class="content-group">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-lg-2 text-bold">No Packinglist</label>
                                <div class="col-md-8 row">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                                        </span>
                                        <select data-placeholder="Select a No Packinglist..." name="packinglist_no" id="packinglist_no" class="form-control select-search">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
                                    </span>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed" id="packinglistTable" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Packinglist</th>
                                            <th>Style</th>
                                            <th>Division</th>
                                            <th>Factory Order</th>
                                            <th>Remark</th>
                                            <th>Total Panel</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="basic-tab2">
                    <div class="panel panel-flat panel-collapsed">
                        <div class="panel-heading">
                            <h5 class="panel-title text-bold bg-info text-center">PACKINGLIST HEADER</h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li class="bg-grey"><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('packinglistSubcont.store') }}" accept-charset="UTF-8" id="form_insert">
                                <fieldset class="content-group">
                                    <div class="form-group">
                                        @csrf
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label class="control-label col-md-2 text-right">PACKINGLIST NO</label>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input style="background:rgba(29, 57, 115, 0.1);cursor:not-allowed;padding-left:5px" type="text" id="no_packinglist_insert" name="no_packinglist_insert" class="form-control bg-info" onfocus="this.blur()" readonly>
                                                    </div>
                                                </div>
                                                <label class="control-label col-md-2 text-right">PO BUYER</label>
                                                <div class="col-md-5">
                                                    <div style="border:solid 1px;border-color: rgba(0,0,0,0.25);padding-left:5px" class="form-group">
                                                        <select multiple data-placeholder="Select PO..." class="form-control select-search" name="poreference[]" id="poreference">
                                                            <option value=""></option>
                                                            @foreach($poreference as $poreference)
                                                                <option value="{{ $poreference->poreference }}">{{ $poreference->poreference }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label class="control-label col-md-2 text-right">STYLE</label>
                                                <div class="col-md-3">
                                                    <div style="background:rgba(29, 57, 115, 0.1);border:solid 1px;border-color: rgba(0,0,0,0.25);padding-left:5px" class="form-group">
                                                        <select data-placeholder="Select Style..." class="form-control select-search" name="style" id="style">
                                                            <option value=""></option>
                                                            @foreach($style as $style)
                                                                {{$style_set = $style->set_type == 'Non' ? $style->style : $style->style.'-'.$style->set_type}}
                                                                <option value="{{ $style_set }}">{{ $style_set }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <label class="control-label col-md-2 text-right">SIZE</label>
                                                <div class="col-md-5">
                                                    <div style="border:solid 1px;border-color: rgba(0,0,0,0.25);padding-left:5px" class="form-group">
                                                        <select multiple data-placeholder="Select Size..." class="form-control select-search" name="size[]" id="size">
                                                            <option value=""></option>
                                                            @foreach($size as $size)
                                                                <option value="{{ $size->size }}">{{ $size->size }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label class="control-label col-md-2 text-right">KK NO</label>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input style="background:rgba(29, 57, 115, 0.1);" type="text" id="kk_no" name="kk_no" class="form-control text-uppercase">
                                                    </div>
                                                </div>
                                                <label class="control-label col-md-2 text-right">PENGIRIMAN</label>
                                                <div class="col-md-2">
                                                    <div style="background:rgba(29, 57, 115, 0.1);border:solid 1px;border-color: rgba(0,0,0,0.25);padding-left:5px" class="form-group">
                                                        <select data-placeholder="Select Process" class="form-control select-search" name="process" id="process">
                                                            <option value=""></option>
                                                            @foreach($process as $process)
                                                                <option value="{{ $process->id }}">{{ $process->process_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-2">
                                                    <div style="background:rgba(29, 57, 115, 0.1);border:solid 1px;border-color: rgba(0,0,0,0.25);padding-left:5px" class="form-group">
                                                        <select data-placeholder="Select Factory..." class="form-control select-search" name="factory_order" id="factory_order">
                                                            <option value=""></option>
                                                            @foreach($division as $division)
                                                                <option value="{{ $division->id }}">{{ $division->factory_alias }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <label class="control-label col-md-2 text-right">REMARK HEADER</label>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                    <input style="background:rgba(29, 57, 115, 0.1);" type="text" id="remark" name="remark" class="form-control text-uppercase">
                                                    </div>
                                                </div>
                                                <label class="control-label col-md-2 text-right">NOMOR BC</label>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <input style="background:rgba(29, 57, 115, 0.1);" type="text" id="bc_no" name="bc_no" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary" id="btn-insert-packinglist">Save <i class="icon-floppy-disk position-right"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <div id="scan1" class="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <center><h5 class="text-bold">SCAN OUT PROCESS</h5></center>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" value="PACKINGLIST NUMBER" class="form-control bg-info text-center" readonly="readonly" />
                                    </div>
                                    <div class="col-md-5">
                                        <select data-placeholder="Select a No Packinglist..." name="packinglist_input" id="packinglist_input" class="form-control select-search">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                    <input type="text" value="" class="form-control bg-info" readonly="readonly" />
                                    </div>
                                </div>
                                <hr/>
                                <table border="0" width="100%">
                                    <tr>
                                        <td><strong>KK</strong></td>
                                        <td>: <span id="kk_input">Data Empty!</span></td>
                                        <td><strong>Division</strong></td>
                                        <td>: <span id="process_res">Data Empty!</span></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Factory Order</strong></td>
                                        <td>: <span id="factory_res">Data Empty!</span></td>
                                        <td><strong>PO Buyer</strong></td>
                                        <td>: <span id="poreference_res">Data Empty!</span></td>
                                    </tr>
                                </table>
                                <div class="row margin-atas">
                                    <button class="btn btn-primary col-md-12" style="width:100%" id="scan_barcode">Mulai Scan</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5>Proses Terbaru</h5>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed" id="componentTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Barcode</th>
                                            <th>Style | Article</th>
                                            <th>Part - Komponen</th>
                                            <th>PO</th>
                                            <th>Size</th>
                                            <th>Cut / Sticker No</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<a href="{{ route('packinglistSubcont.getData') }}" id="getDataLink"></a>
<a href="{{ route('packinglistSubcont.getNoPackinglist') }}" id="getNoPackinglistLink"></a>
<a href="{{ route('packinglistSubcont.dataPackinglist') }}" id="dataPackinglistLink"></a>
<a href="{{ route('packinglistSubcont.dataComponent') }}" id="dataComponentLink"></a>
<a href="{{ route('packinglistSubcont.ajaxGetDataPackinglist') }}" id="get_data_packinglist"></a>
<a href="{{ route('packinglistSubcont.ajaxGetDataPackinglisthead') }}" id="get_data_packinglisthead"></a>
<a href="{{ route('packinglistSubcont.dataComponentPackinglist') }}" id="dataComponentPackinglistLink"></a>
<a href="{{ route('packinglistSubcont.detailPackinglist') }}" id="detailPackinglistLink"></a>
<a href="{{ route('packinglistSubcont.scanComponent') }}" id="scanComponentLink"></a>
<a href="{{ route('packinglistSubcont.saveComponent') }}" id="saveComponentLink"></a>
@section('page-modal')
	@include('subcont.scan_out._modal_scan')
@endsection
@section('page-js')
<script>
    $(document).ready( function (){
    $('body').addClass('sidebar-xs');
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
    var packinglist_no = $('#packinglist_no').val();
    var tableL = $('#packinglistTable').DataTable({
        dom: 'Bfrtip',
        scrollX:true,
        scrollY:480,
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: $('#getDataLink').attr('href'),
            data: function(d) {
                return $.extend({}, d, {
                    "packinglist_no"		: $('#packinglist_no').val(),
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'no_packinglist', name: 'no_packinglist',searchable:true,orderable:true},
            {data: 'style_set', name: 'style_set',searchable:true,orderable:true},
            {data: 'division', name: 'division',searchable:true,orderable:true},
            {data: 'factory_order', name: 'factory_order',searchable:true,orderable:true},
            {data: 'remark', name: 'remark',searchable:true,orderable:true},
            {data: 'total_panel', name: 'total_panel',searchable:false,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    $('#scanModal').on('hidden.bs.modal', function(){
        get_data_component_packinglist($('#packinglist_input').val());
    });
    $('#process').on('change', function(event) {
        var process = this.value;        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: $('#getNoPackinglistLink').attr('href'),
            data: {
                process: process,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $('#no_packinglist_insert').val(response);
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#packinglist_input').on('change', function() {
        var packinglist_no = $('#packinglist_input').val();
        $.ajax({
            type: 'get',
            url : $('#detailPackinglistLink').attr('href'),
            data: {
                packinglist_no: packinglist_no,
            },
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $('#kk_input').text(response.kk_no);
                $('#process_res').text(response.process_name);
                $('#poreference_res').text(response.po_buyer);
                $('#factory_res').text(response.factory_res);
                get_data_component_packinglist(response.packinglist);
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#scanModal').on('shown.bs.modal', function () {
        $('#scan_component').focus();
    })

    $('#scan_barcode').on('click', function() {
        var packinglist = $('#packinglist_input').val();
        if(!packinglist){
            $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : $('#dataPackinglistLink').attr('href'),
            data: {
                packinglist: packinglist,
            },
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $('#scanModal').modal();
                $('#packinglist_res').val(packinglist);
                $('#locator_res').val(response.locator);
                $('#state_res').val(response.state);
                $('#scan_component').focus();
                get_data_component(response.locator, response.state, response.packinglist);
                $.unblockUI();
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#scan_component').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();
            var packinglist = $('#packinglist_res').val();
            var locator = $('#locator_res').val();
            var state = $('#state_res').val();
            var barcode_component = $(this).val();

            if(!packinglist){
                $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                return false;
            }

            if(!barcode_component){
                $("#alert_warning").trigger("click", 'Scan Barcode Dahulu!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url : $('#scanComponentLink').attr('href'),
                data: {
                    packinglist: packinglist,
                    barcode_component: barcode_component,
                },
                beforeSend: function() {
                    $('#scanTable').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    get_data_component(locator, state, packinglist);
                },
                error: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }
    });

    $('#save_component').on('click', function() {
        var packinglist_no = $('#packinglist_res').val();
        var locator = $('#locator_res').val();
        var state = $('#state_res').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : $('#saveComponentLink').attr('href'),
            data: {
                packinglist_no: packinglist_no,
                locator: locator,
                state: state,
            },
            beforeSend: function() {
                $.blockUI({
                    theme:true,
                    baseZ:2000,
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI();
                myalert('success','Done');
                location.reload();
                // window.open("packinglist-distribusi/print/"+response.id_packinglist, "_blank");
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    })

    $('#btn-filter').click(function(){
        event.preventDefault();
        var packinglist_no    = $('#packinglist_no').val();
        if (packinglist_no == null) {
            $("#alert_warning").trigger("click", 'Silahkan Pilih Packinglist Dahulu..!');
            return false;
        }
        tableL.draw();
    });

    var tableL = $('#packinglistTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                tableL.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                tableL.search("").draw();
            }
            return;
    });
    tableL.draw();

    //INSERT PACKINGLIST HEADER
    $('#form_insert').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var poreference         = $('#poreference').val();
        var no_packinglist      = $('#no_packinglist_insert').val();
        var kk_no               = $('#kk_no').val();
        var size                = $('#size').val();
        var process             = $('#process').val();
        var remark              = $('#remark').val();
        var style               = $('#style').val();
        var bc_no               = $('#bc_no').val();
        var factory_order       = $('#factory_order').val();

        if(!poreference){
            $("#alert_warning").trigger("click", 'PO buyer wajib diisi');
            return false;
        }

        if(!no_packinglist){
            $("#alert_warning").trigger("click", 'Packinglist wajib diisi');
            return false;
        }

        if(!process){
            $("#alert_warning").trigger("click", 'Divisi wajib dipilih');
            return false
        }

        if(!style){
            $("#alert_warning").trigger("click", 'Style wajib dipilih');
            return false
        }

        if(!size){
            $("#alert_warning").trigger("click", 'Size wajib dipilih');
            return false
        }

        if(!factory_order){
            $("#alert_warning").trigger("click", 'Factory wajib dipilih');
            return false
        }

        if(!remark){
            $("#alert_warning").trigger("click", 'Remark wajib dipilih');
            return false
        }
        
        $.ajax({
            type: "POST",
            url: $('#form_insert').attr('action'),
            data:new FormData($("#form_insert")[0]),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function (){
                $.unblockUI();
            },
            success: function (){
                $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                window.location.reload();
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#insertPackinglistModal').modal();
            }
        });
    });

    $('#packinglist_input').select2({
        placeholder: "Pilih Packinglist",
        minimumInputLength: 3,
        ajax: {
            url: $('#get_data_packinglist').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data
                };
            },
            cache: false
        }
    });
    $('#packinglist_no').select2({
        placeholder: "Pilih Packinglist",
        minimumInputLength: 3,
        ajax: {
            url: $('#get_data_packinglist').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                console.log(data);
                return {
                    results: data
                };
            },
            cache: false
        }
    });
});
    function get_data_component(locator, state, packinglist) {
        var table_component = $('#scanTable').DataTable({
            destroy: true,
            bFilter: false,
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                url: $('#dataComponentLink').attr('href'),
                data: {
                    locator: locator,
                    state: state,
                    packinglist: packinglist                        
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = table_component.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
                {
                    targets: 2,
                    className: 'text-center',
                }
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_id', name: 'bundle_id', searchable:true, visible:true, orderable:false},
                {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false},
                {data: 'action', name: 'action', searchable:false, orderable:false},
            ],
        });
    }

    function get_data_component_packinglist(packinglist) {
        var table_component = $('#componentTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: $('#dataComponentPackinglistLink').attr('href'),
            data: {
                packinglist: packinglist,
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table_component.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                className: 'text-center',
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'barcode_id', name: 'barcode_id', searchable:true, visible:true, orderable:false},
            {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
            {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
            {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
            {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
            {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
            {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false}
        ],
    });

    $('#componentTable').on('click','.ignore-click', function() {
        return false;
        });
    }

    function hapus(url) {
        var packinglist = $('#packinglist_res').val();
        var locator = $('#locator_res').val();
        var state = $('#state_res').val();

        // if(!packinglist){
        //     $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
        //     return false;
        // }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url : url,
            beforeSend: function() {
                $('#scanTable').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $('#scanTable').unblock();
                $('#scan_component').val('');
                $('#scan_component').focus();
                $("#alert_success").trigger("click",'komponen berhasil di hapus');
                get_data_component(locator, state, packinglist);
                console.log(response);
            },
            error: function(response) {
                $('#scanTable').unblock();
                $('#scan_component').val('');
                $('#scan_component').focus();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    }
</script>
@endsection
