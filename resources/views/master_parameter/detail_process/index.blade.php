@extends('layouts.app',['active' => 'masterDetailProcess'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Detail Process </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Detail Process</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add Detail Process <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th style="width:10px;">#</th>
                            <th>DETAIL PROCESS NAME</th>
                            <th>GSD NAME</th>
                            <th>PROCESS</th>
                            <th>COL.REPORT</th>
    	                    <th style="width:10px;">ACTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('masterparam.ajaxGetDataMasterDetailProcess') }}" id="get_data"></a>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessDetail') }}" id="get_detail_process_detail"></a>
@endsection

@section('page-modal')
    @include('master_parameter.detail_process._add_modal')
    @include('master_parameter.detail_process._update_modal')
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var _token = $("input[name='csrf-token']").val();
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
    
        var url = $('#get_data').attr('href');
        var table = $('#table-list').DataTable({
            ajax: url,
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false  },
                {data: 'detail_process_name', name: 'detail_process_name'},
                {data: 'gsd_name', name: 'gsd_name'},
                {data: 'process_name', name: 'process_name'},
                {data: 'column_name', name: 'column_name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
        //end of datatables
    
        //add
        $('.add_new').on('click',function(){
            $('#modal_add_new_').modal('show')
        });
        //end of add
    
        $('#modal_add_new_').on('shown.bs.modal', function(){
            $('#detail_process_name').focus();
        });

        $('#modal_edit_').on('shown.bs.modal', function(){
            $('#gsd_name_update').focus();
        });

        $('#modal_edit_').on('hidden.bs.modal', function(){
            $('#form-update').trigger("reset");
            $('#id_update').val(null).trigger('change');
            $('#detail_process_name_update').val('');
            $('#gsd_name_update').val('');
            $('#process_id_update').val(null).trigger('change');
            $('#column_report_edit').val('');
            $('#column_report_edit2').val('');
    
           // table.draw();
        });
    
        //delete
        $("#table-list").on("click", ".delete", function() {
            event.preventDefault();
            var id = $(this).data('id');
            if(id == 'kosong') {
                return false;
            }
            var token = $(this).data("token");
            bootbox.confirm("Are you sure delete this row ?", function (result) {
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url: "master-detail-process/delete/"+id,
                        type: "GET",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        beforeSend: function () {
                            $('.loader-area').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        },
                        complete: function () {
                            $(".loader-area").unblock();
                        },
                        success: function () {
                            myalert('success','Data has been deleted');
                            table.ajax.reload();
                        }
                    });
                }
            });
        });
        //end of delete
    
        //add new
        $('#form-add').submit(function(event) {
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : "POST",
                url : $('#form-add').attr('action'),
                data: $('#form-add').serialize(),
                beforeSend: function () {
                    $('.loader-area').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                complete: function () {
                    $('.loader-area').unblock();
                },
                success: function(response) {
                    myalert('success','GOOD');
                    $('#form-add').trigger("reset");
                    $('#modal_add_new_').trigger('toggle');
                    table.ajax.reload();
                },
                error: function(response) {
                    //myalert('error','NOT GOOD');
                    myalert('error',response['responseJSON']);
                }
            })
        });

        //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'detail_process_name_update', 'gsd_name_update', 'process_id_update', 'id_column_report_update', 'id_column_report2_update'];
        var val = [
            $('#id_update').val(), 
            $('#detail_process_name_update').val(),
            $('#gsd_name_update').val(),
            $('#process_id_update').val(),
            $('#id_column_report_update').val(),
            $('#id_column_report2_update').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#detail_process_name_update').focus();
                table.ajax.reload();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    $('#process_id').on('change keydown', function(){
        
        var id = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_process_detail').attr('href'),
            data: {'id': 0, process_id: id, '_token': _token},
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI();

                $('.select-column-name').children('option:not(:first)').remove();

                $.each(response.column_reports, function(i, v){

                    $(".select-column-name").select2({
                        placeholder:'select a state..',
                        data: [
                            {
                                id: v.id,
                                text: v.column_name
                            }
                        ]
                    });  
                    
                });
                
                if(response.is_printing){
                    $('.select-column-name2').children('option:not(:first)').remove();
    
                    $.each(response.column_reports2, function(i, v){
    
                        $(".select-column-name2").select2({
                            placeholder:'select a state..',
                            data: [
                                {
                                    id: v.id,
                                    text: v.column_name
                                }
                            ]
                        });  
                        
                    });

                    $('#dvColumnReport2').removeClass('hidden');
                    
                }else{
                    $('.select-column-name2').children('option:not(:first)').remove();
                    $('#dvColumnReport2').addClass('hidden');
                }
            },
            error: function(response) {
                console.log(response);
            }
        });
    });

    $('#process_id_update').on('change keydown', function(){
        
        var id = $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_process_detail').attr('href'),
            data: {'id': 0, process_id: id, '_token': _token},    
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                $.unblockUI();

                $('.select-column-name').children('option:not(:first)').remove();

                $.each(response.column_reports, function(i, v){

                    $(".select-column-name").select2({
                        placeholder:'select a state..',
                        data: [
                            {
                                id: v.id,
                                text: v.column_name
                            }
                        ]
                    });  
                    
                });

                if(response.is_printing){
                    $('.select-column-name2').children('option:not(:first)').remove();
    
                    $.each(response.column_reports2, function(i, v){
    
                        $(".select-column-name2").select2({
                            placeholder:'select a state..',
                            data: [
                                {
                                    id: v.id,
                                    text: v.column_name
                                }
                            ]
                        });  
                        
                    });

                    $('#dvColumnReport2Update').removeClass('hidden');
                    
                }else{
                    $('.select-column-name2').children('option:not(:first)').remove();
                    $('#dvColumnReport2Update').addClass('hidden');
                }

                $('#id_column_report_update').val($('#column_report_edit').val()).trigger('change');
                $('#id_column_report2_update').val($('#column_report_edit2').val()).trigger('change');
            },
            error: function(response) {
                console.log(response);
            }
        });
    });

});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {

        var detail_process_name = response.detail_process_name;
        var gsd_name = response.gsd_name;
        var id = response.id;
        var process_id = response.process_id;
        var id_column_report = response.id_column_report;
        var id_column_report2 = response.id_column_report2;

        $('#id_update').val(id);
        $('textarea#detail_process_name_update').val(detail_process_name);
        $('textarea#gsd_name_update').val(gsd_name);
        $('#process_id_update').val(process_id).trigger('change');

        $('#column_report_edit').val(id_column_report);
        $('#column_report_edit2').val(id_column_report2);

        $('#modal_edit_').modal();

    });
}

</script>
@endsection




