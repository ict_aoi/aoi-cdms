<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Geser Planning</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								<span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								<span id="style_detail_view"></span>
							</div>
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								<span id="articleno_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Size Category</strong></span>
							</div>
							<div class="col-md-8">
								<span id="size_category_detail_view"></span>
							</div>
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-8">
								<span id="po_buyer_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
                <fieldset class="content-group">
                    <div class="form-group">
                        <label class="control-label col-lg-3 text-bold">Geser Ke Tanggal</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-btn">	
                                    <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                                </span>
                                <input type="hidden" class="form-control" name="movement_id_plan" id="movement_id_plan">
                                <input type="text" class="form-control pickadate" name="movement_date" placeholder="Enter Cutting Date" id="movement_date">
                            </div>
                        </div>
                        <label class="control-label col-lg-3 text-bold">Queue</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <span class="input-group-btn">	
                                    <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-list-numbered"></i></button>
                                </span>
                                <input type="text" class="form-control" onkeypress="isNumberDot(event)" name="movement_queu" placeholder="Enter Queue" id="movement_queu">
                            </div>
                        </div>
                    </div>
                </fieldset>
			</div>
			<input type="hidden" id="upload_file_id">
			<div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btn-movement">Geser</button>
                <button class="btn btn-danger" type="button" id="btn-delete">Hapus</button>
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>