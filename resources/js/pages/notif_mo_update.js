$(document).ready( function (){
    var url_notif_data = $('#link-notif-data').attr('href');

    $('#notif_detail').click(function() {
        $.ajax({
            type: 'get',
            url: url_notif_data,
            beforeSend: function() {
                $('#notif_view').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success:function(response)
            {    
                var html1 = '';
                var html2 = '';
                var html3 = '';
                var html4 = '';
                $('#day1_data').empty();
                $('#day2_data').empty();
                $('#day3_data').empty();
                $('#day4_data').empty();
                $('#notif_view').unblock();
                $('#day1_label').text(response.d1_label);
                $('#day2_label').text(response.d2_label);
                $('#day3_label').text(response.d3_label);
                $('#day4_label').text(response.d4_label);

                if(response.d1_data.length > 0){
                    $.each(response.d1_data, function(i1, data1){
                        var status_color = data1.type == 'create' ? 'bg-primary-300' : (data1.type == 'update' ? 'bg-success-300' : 'bg-danger-300');
                        html1 += '<a href="#" class="list-group-item '+status_color+'">';
                        html1 += '<i class="icon-calendar"></i><strong>Style:</strong> '+data1.style+' <strong>Article:</strong> '+data1.articleno;
                        html1 += '<span class="pull-right"><strong>'+data1.type+' at </strong>'+data1.date+'</span>';
                        html1 += '</a>';
                    });
                } else {
                    html1 += '<a href="#" class="list-group-item">';
                    html1 += '<center><strong>Tidak ada data</strong></center>';
                    html1 += '</a>';
                }

                $('#day1_data').append(html1);

                if(response.d2_data.length > 0){
                    $.each(response.d2_data, function(i2, data2){
                        var status_color = data2.type == 'create' ? 'bg-primary-300' : (data2.type == 'update' ? 'bg-success-300' : 'bg-danger-300');
                        html2 += '<a href="#" class="list-group-item '+status_color+'">';
                        html2 += '<i class="icon-calendar"></i><strong>Style:</strong> '+data2.style+' <strong>Article:</strong> '+data2.articleno;
                        html2 += '<span class="pull-right"><strong>'+data2.type+' at </strong>'+data2.date+'</span>';
                        html2 += '</a>';
                    });
                } else {
                    html2 += '<a href="#" class="list-group-item">';
                    html2 += '<center><strong>Tidak ada data</strong></center>';
                    html2 += '</a>';
                }

                $('#day2_data').append(html2);

                if(response.d3_data.length > 0){
                    $.each(response.d3_data, function(i3, data3){
                        var status_color = data3.type == 'create' ? 'bg-primary-300' : (data3.type == 'update' ? 'bg-success-300' : 'bg-danger-300');
                        html3 += '<a href="#" class="list-group-item '+status_color+'">';
                        html3 += '<i class="icon-calendar"></i><strong>Style:</strong> '+data3.style+' <strong>Article:</strong> '+data3.articleno;
                        html3 += '<span class="pull-right"><strong>'+data3.type+' at </strong>'+data3.date+'</span>';
                        html3 += '</a>';
                    });
                } else {
                    html3 += '<a href="#" class="list-group-item">';
                    html3 += '<center><strong>Tidak ada data</strong></center>';
                    html3 += '</a>';
                }

                $('#day3_data').append(html3);

                if(response.d4_data.length > 0){
                    $.each(response.d4_data, function(i4, data4){
                        var status_color = data4.type == 'create' ? 'bg-primary-300' : (data4.type == 'update' ? 'bg-success-300' : 'bg-danger-300');
                        html4 += '<a href="#" class="list-group-item '+status_color+'">';
                        html4 += '<i class="icon-calendar"></i><strong>Style:</strong> '+data4.style+' <strong>Article:</strong> '+data4.articleno;
                        html4 += '<span class="pull-right"><strong>'+data4.type+' at </strong>'+data4.date+'</span>';
                        html4 += '</a>';
                    });
                } else {
                    html4 += '<a href="#" class="list-group-item">';
                    html4 += '<center><strong>Tidak ada data</strong></center>';
                    html4 += '</a>';
                }

                $('#day4_data').append(html4);
            }
        });
    });
});