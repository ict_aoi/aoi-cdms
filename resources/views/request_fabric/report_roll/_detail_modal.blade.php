<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Report Per Roll</span>
				</legend>
				<div class="row">
					<div class="col-md-6 col-xs-6 col-sm-6">
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="style_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="articleno_detail_view"></span>
							</div>
						</div>
                        <div class="row">
                            <div class="col-md-4 col-xs-10 col-sm-10">
                                <span><strong>Part No</strong></span>
                            </div>
                            <div class="col-md-1 col-xs-2 col-sm-2">
                                :
                            </div>
                            <div class="col-md-7 col-xs-12 col-sm-12">
                                <span id="part_no_detail_view"></span>
                            </div>
                        </div>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-6">
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="po_buyer_detail_view"></span>
							</div>
						</div>

                        <div class="row">
                           <div class="col-md-4 col-xs-10 col-sm-10">
                                <span><strong>Qty CSI</strong></span>
                            </div>
                           <div class="col-md-1 col-xs-2 col-sm-2">
                                :
                            </div>
                           <div class="col-md-7 col-xs-12 col-sm-12">
                                <span id="qty_csi_detail_view"></span>
                            </div>
                        </div>

                        <div class="row">
                           <div class="col-md-4 col-xs-10 col-sm-10">
                                <span><strong>Qty SSP</strong></span>
                            </div>
                           <div class="col-md-1 col-xs-2 col-sm-2">
                                :
                            </div>
                           <div class="col-md-7 col-xs-12 col-sm-12">
                                <span id="qty_ssp_detail_view"></span>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					{!!
						Form::open([
							'url'    => route('reportRoll.downloadExcel'),
							'method' => 'post',
							'class'  => 'form-horizontal',
							'id'     => 'download_report_roll'
						])
					!!}

                        {!! Form::hidden('cutting_date_detail', '', array('id' => 'cutting_date_detail')) !!}
                        {!! Form::hidden('style_detail', '', array('id' => 'style_detail')) !!}
                        {!! Form::hidden('part_no_detail', '', array('id' => 'part_no_detail')) !!}
                        {!! Form::hidden('articleno_detail', '', array('id' => 'articleno_detail')) !!}
                        {!! Form::hidden('po_buyer_detail', '[]', array('id' => 'po_buyer_detail')) !!}
						<button type="submit" id="down_excel" name="down_excel" class="btn btn-success pull-right margin-kanan">DOWNLOAD <i class="icon-file-excel position-right"></i></button>
                    {!! Form::close() !!}
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr class="bg-grey">
								<th>Barcode</th>
								<th>No Roll</th>
								<th>Material</th>
								<th>Color</th>
								<th>Actual LOT</th>
								<th>Actual Width</th>
								<th>PO Supplier</th>
								<th>Qty Prepared</th>
								<th>Sisa</th>
								<th>Satus Relax</th>
								<th>Machine</th>
								<th>Action</th>
								<th>Reduce</th>
                                <th>User</th>
								<th>Cancel</th>
							</tr>
						</thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th colspan="4" style="text-align:right" bold>TOTAL:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
								<th></th>
								<th></th>
								<th></th>
                            </tr>
                        </tfoot>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="cutting_date_detail" id="cutting_date_detail" />
				<input type="hidden" name="style_detail" id="style_detail" />
                <input type="hidden" name="part_no_detail" id="part_no_detail" />
				<input type="hidden" name="articleno_detail" id="articleno_detail" />
				<input type="hidden" name="po_buyer_detail" id="po_buyer_detail" value="[]" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btn_accepted">Accept</button>
			</div>
		</div>
	</div>
</div>
