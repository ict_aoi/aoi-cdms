@extends('layouts.app',['active' => 'report_scan_cutting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan Cutting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan Cutting</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">By Date</a></li>
				<li><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">Bundle Output</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="{{ route('reportCutting.downloadCutting') }}" id="form_filter_date" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
								<div class="col-lg-10">
									
									@csrf
									
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="{{ route('reportCutting.downloadBundleOutput') }}" id="filter_date_cutting" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Aktual Cutting</label>
								<div class="col-lg-10">
									@csrf
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="actual_cutting" placeholder="Tanggal Aktual Cutting" id="actual_cutting">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead>
										<tr>
											<th>No</th>
											<th>Season</th>
											<th>Style</th>
											<th>PO Buyer</th>
											<th>Article</th>
											<th>Cut | Sticker</th>
											<th>Size</th>
											<th>Part</th>
											<th>Komponen</th>
											<th>Cutting Date</th>
											<th>Qty</th>
											<th>Meja Bundle</th>
											<th>Bundle Operator</th>
											<th>Scan At</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
		//DATATABLES
		var actual_cutting =$('#actual_cutting').val();
		var url = "{{route('reportCutting.getDataBundleOutput')}}";
		var tableL = $('#table-list').DataTable({
			processing: true,
			serverSide: true,
			deferRender:true,
			scrollX:true,
			dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
			language: {
				search: '<span>Filter:</span> _INPUT_',
				searchPlaceholder: 'Type to filter...',
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
			},
			ajax: {
				type: 'GET',
				url: url,
				data: function(d) {
					return $.extend({}, d, {
						"actual_cutting"		: $('#actual_cutting').val()
					});
				}
			},
			fnCreatedRow: function (row, data, index) {
				var info = tableL.page.info();
				var value = index+1+info.start;
				$('td', row).eq(0).html(value);
			},
			columns: [
				{data: null, sortable: false, orderable: false, searchable: false},//0
				{data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
				{data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//2
				{data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//3
				{data: 'article', name: 'article',searchable:true,visible:true,orderable:true},//4
				{data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//5
				{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//6
				{data: 'part', name: 'part',searchable:true,visible:true,orderable:true},//7
				{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//7
				{data: 'cutting_date', name: 'cutting_date',searchable:false,visible:true,orderable:true},//8
				{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},//9
				{data: 'bundle_table', name: 'bundle_table',searchable:true,visible:true,orderable:true},//10
				{data: 'name', name: 'name',searchable:true,visible:true,orderable:true},//11
				{data: 'scan_out', name: 'scan_out',searchable:true,visible:true,orderable:true},//11
			]
		});

		$('#btn-filter').on('click',function(){
			event.preventDefault();
			var actual_cutting    = $('#actual_cutting').val();
				$.ajaxSetup({
					headers:{
						'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					type:'GET',
					url : url,
					data:{
						actual_cutting:actual_cutting
					},
					beforeSend:function(){
						loading_process();
					},
					success:function(response){
						$('#table-list').unblock();
						tableL.clear();
						tableL.draw();
					},
					error:function(response){
						myalert('error','eror');
					}
				});
		});
    });
</script>
@endsection