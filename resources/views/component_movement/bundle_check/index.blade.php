@extends('layouts.app',['active' => 'bundle_check'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    .control-label {
        padding-top: 9px !important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Bundle Check</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Bundle Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_input" name="barcode_input" placeholder="#Scan Barcode Komponen" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Komponen" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable" witdh="100%">
                <tbody>
                    <tr>
                        <th><strong>Komponen</strong></th>
                        <th>: <span id="komponen">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Style (Season)</strong></th>
                        <th>: <span id="style">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>PO Buyer</strong></th>
                        <th>: <span id="po_buyer">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Article</strong></th>
                        <th>: <span id="article">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Size</strong></th>
                        <th>: <span id="size">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Cut / No. Sticker</strong></th>
                        <th>: <span id="cut_sticker">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Qty</strong></th>
                        <th>: <span id="qty">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Process</strong></th>
                        <th>: <span id="process">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Bundling</strong></th>
                        <th>: <span id="bundling">-</span></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('bundleCheck.bundleInfo') }}" id="bundleCheckLink"></a>
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {

            $('#barcode_input').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var barcode_input = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url: $('#bundleCheckLink').attr('href'),
                        data: {
                            barcode_input: barcode_input,
                        },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait',
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent',
                                },
                                theme: true,
                                baseZ: 2000
                            });
                        },
                        success: function(response) {
                            $.unblockUI();
                            $('#barcode_input').val('').focus();
                            $('#komponen').text(response.komponen);
                            $('#style').text(response.style);
                            $('#po_buyer').text(response.po_buyer);
                            $('#article').text(response.article);
                            $('#size').text(response.size);
                            $('#cut_sticker').text(response.cut_sticker);
                            $('#process').text(response.process);
                            $('#qty').text(response.qty);
                            $('#bundling').text(response.bundling);
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                            $('#barcode_input').val('').focus();
                        }
                    });
                }
            });
        });
    </script>
@endsection