@extends('layouts.app',['active' => 'review_qc_v4'])

@section('page-css')
    <style>
        th, td { white-space: nowrap; }
        div.dataTables_wrapper {
            width: 100%;
            margin: 0 auto;
        }
    </style>
@endsection


@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Review QC Check</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Review QC Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
    <form class="form-horizontal" action="{{ route('Qccheck.downloadReviewQcV4') }}" id="review_qc_report" method="GET">
        <fieldset class="content-group">
            @csrf
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">	
                                <i class="icon-calendar"></i>
                            </span>
                            <input type="text" class="form-control daterange-basic" name="qc_date" placeholder="Masukkan Tanggal QC Check" id="qc_date" required style="background-color:#abf6f7;height:43px">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" style="padding-left:16px;text-align:center">	
                                <i class="icon-location3"></i>
                            </span>
                            <div style="background-color:#abf6f7">
                                <select multiple data-placeholder="Select a locator" class="form-control select-search" name="locator[]" id="locator" required style="padding-bottom:4px">
                                    <option value=""></option>
                                    @foreach($locator as $locator)
                                        <option value="{{ $locator->id_table }}">{{ $locator->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <span class="input-group pull-right">
                            <label class="btn btn-primary legitRipple" id="form_filter_date">FILTER</label>
                        </span>
                        <span class="input-group pull-right" style="margin-right:2px">
                            <button class="btn btn-success legitRipple" id="download_report_review" type="submit">Download</button>
                        </span>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
	</div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Season</th>
                        <th>Defect Name</th>
                        <th>Style</th>
                        <th>PO Buyer</th>
                        <th>Article</th>
                        <th>Color</th>
                        <th>Cut | Sticker</th>
                        <th>Barcode Bundle</th>
                        <th>Size</th>
                        <th>Komponen</th>
                        <th>Cutting Date</th>
                        <th>Qty</th>
                        <th>Qty Reject</th>
                        <th>Job</th>
                        <th>QC Name</th>
                        <th>Locator</th>
                        <th>Process Name</th>
                        <th>Supplier</th>
                        <th>Date Check</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>TOTAL:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function () {
    $.extend( $.fn.dataTable.defaults, {
        processing: true,
        serverSide: true,
        deferRender:true,
        scrollX: true,
        scrollY: true,
        pageLength:100,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table-list').DataTable({
        ajax: {
            type: 'GET',
            url: "review-qc-v4/get-data",
            data: function(d) {
                 return $.extend({}, d, {
                     "qc_date"         : $('#qc_date').val(),
                     "locator"         : $('#locator').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false}, //0
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
            {data: 'defect_name', name: 'defect_name',searchable:true,visible:true,orderable:true},//2
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//3
            {data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//4
            {data: 'article', name: 'article',searchable:true,visible:true,orderable:true},//5
            {data: 'color_name', name: 'color_name',searchable:true,visible:true,orderable:false},//6
            {data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//7
            {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//8
			{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//9
			{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//10
            {data: 'cutting_date', name: 'cutting_date',searchable:true,visible:true,orderable:true},//11
			{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},//12
			{data: 'qty_reject', name: 'qty_reject',searchable:false,visible:true,orderable:true},//13
			{data: 'job', name: 'job',searchable:true,visible:true,orderable:true},//14
			{data: 'qc_name', name: 'qc_name',searchable:true,visible:true,orderable:true},//15
            {data: 'locator_name', name: 'locator_name',searchable:true,visible:true,orderable:true},//16
            {data: 'process_name', name: 'process_name',searchable:true,visible:true,orderable:true},//16
            {data: 'subcont_name', name: 'subcont_name',searchable:true,visible:true,orderable:true},//17
            {data: 'date_check', name: 'date_check',searchable:true,visible:true,orderable:true},//17
        ],
        //Footer CallBack digunakan untuk menjumlahkan nilai pada 1 kolom tertentu
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over all pages
            total = api
                .column( 12 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            // Update footer
            $( api.column( 12 ).footer() ).html(
                total
            );
        }
    });

    table.on('preDraw', function() {
        Pace.start();
        loading_process();
    })
    .on('draw.dt', function() {
        $('#table-list').unblock();
        Pace.stop();
    });

    $('#form_filter_date').on('click',function(){
        // event.preventDefault();
        var qc_date = $('#qc_date').val();
        var locator = $('#locator').val();

        if(!qc_date){
            $("#alert_warning").trigger("click", 'Pilih Tanggal Pengecekan!');
            return false;
        }
        if(!locator){
            $("#alert_warning").trigger("click", 'Pilih Locator Dahulu!');
            return false;
        }

        table.draw();
    });
    
    var dtable = $('#table-list').dataTable().api();

    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();

    // $('#btn_download_data').on('click', function() {
    //     var qc_date = $('#qc_date').val();
    //     var locator = $('#locator').val();

    //     if (qc_date == '' || qc_date == null && locator == '' || locator == null) {
    //         return false;
    //     }
    //     window.open('review-qc-v4/get-data/download/'+qc_date+locator, '_blank');
    // });

    $('#download_report_review').on('click',function(){
        var qc_date = $('#qc_date').val();
        var locator = $('#locator').val();

        if (qc_date == '' || qc_date == null && locator == '' || locator == null) {
            return myalert('error','Harap Pilih Tanggal Pengecekan dan Locator Dahulu!');
        }
    });

});
</script>
@endsection
