<?php

namespace App\Http\Controllers\Spreading;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Spreading\FabricUsed;
use App\Models\Spreading\SpreadingReportTemp;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\RequestFabric\ReportRoll;
use App\Http\Controllers\Controller;

class QtyFabricCheckController extends Controller
{
    public function index()
    {
        return view('spreading.qty_fabric_check.index');
    }

    public function checkFabric(Request $request)
    {
        if(request()->ajax())
        {
            $input_barcode = $request->input_barcode;

            if($input_barcode != null) {
                $get_fabric_used = FabricUsed::where('barcode_fabric', strtolower($input_barcode))->orWhere('barcode_fabric',strtoupper($input_barcode))->where('deleted_at', null)->orderBy('created_at', 'asc')->get();
                
                if($get_fabric_used->count() > 0) {
                    $get_fabric_used_last = FabricUsed::where('barcode_fabric', strtolower($input_barcode))->where('deleted_at', null)->orWhere('barcode_fabric',strtoupper($input_barcode))->orderBy('created_at', 'desc')->get();
                    $data = [
                        'barcode' => $input_barcode,
                        'cutting_date' => Carbon::parse($get_fabric_used->first()->cutting_marker->cutting_plan->cutting_date)->format('d-m-Y'),
                        'no_roll' => $get_fabric_used->first()->no_roll,
                        'lebar' => $get_fabric_used->first()->actual_width,
                        'lot' => $get_fabric_used->first()->lot,
                        'material' => $get_fabric_used->first()->item_code,
                        'qty_supply' => $get_fabric_used->first()->qty_fabric,
                        'qty_actual' => $get_fabric_used_last->first()->actual_sisa,
                        'status' => 'sudah digunakan',
                        'detail' => $get_fabric_used_last,
                    ];
                    return response()->json(['data' => $data, 'message' => ''],200);
                } else {
                    $get_temp_spreading = SpreadingReportTemp::where('barcode_fabric', strtolower($input_barcode))->orWhere('barcode_fabric',strtoupper($input_barcode))->orderBy('created_at', 'asc')->get();

                    if($get_temp_spreading->count() > 0) {
                        $data = [
                            'barcode' => $input_barcode,
                            'cutting_date' => Carbon::parse($get_temp_spreading->first()->cutting_marker->cutting_plan->cutting_date)->format('d-m-Y'),
                            'no_roll' => $get_temp_spreading->first()->no_roll,
                            'lebar' => $get_temp_spreading->first()->actual_width,
                            'lot' => $get_temp_spreading->first()->lot,
                            'material' => $get_temp_spreading->first()->item_code,
                            'qty_supply' => $get_temp_spreading->first()->qty_fabric,
                            'qty_actual' => 'sedang digunakan',
                            'status' => 'sedang digunakan',
                            'detail' => null
                        ];
                        return response()->json(['data' => $data, 'message' => ''],200);
                    } else {
                        // $get_fabric_whs = DB::connection('wms_live')->table('integration_whs_to_cutting')->where('barcode', $input_barcode)->where('last_status_movement', 'out')->get();
                        $get_fabric_whs = DB::connection('wms_live_new')->select(DB::raw("SELECT * from integration_to_cutting_f(lower('$input_barcode'))where last_status_movement = 'out' or barcode=upper('$input_barcode')"));

                        if(count($get_fabric_whs) > 0) {
                            $data = [
                                'barcode' => $input_barcode,
                                'cutting_date' => Carbon::parse($get_fabric_whs[0]->planning_date)->format('d-m-Y'),
                                'no_roll' => $get_fabric_whs[0]->nomor_roll,
                                'lebar' => $get_fabric_whs[0]->actual_width,
                                'lot' => $get_fabric_whs[0]->actual_lot,
                                'material' => $get_fabric_whs[0]->item_code,
                                'qty_supply' => $get_fabric_whs[0]->qty_prepared,
                                'qty_actual' => $get_fabric_whs[0]->qty_prepared,
                                'status' => 'belum digunakan',
                                'detail' => null
                            ];
                            return response()->json(['data' => $data, 'message' => ''],200);
                        } else {
                            $data = null;
                            return response()->json(['data' => $data, 'message' => 'Data tidak ditemukan!'],200);
                        }
                    }
                }
            } else {
                $data = null;
                return response()->json(['data' => $data, 'message' => 'Data input kosong!'],200);
            }
        }
    }
}
