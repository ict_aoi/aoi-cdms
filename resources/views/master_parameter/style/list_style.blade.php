<div class="table-responsive">
    <table class="table" id="table-style">
        <thead>
            <tr>
                <th>Style</th>
                <th>Style</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->style }}</td>
                    <td>{{ $val->style }}</td>
                    <td>
                        <button type="button" id="choose-style"
                                data-id="{{ $val->id }}"
                                data-style="{{ $val->style }}"
                                class="btn btn-default">
                                SELECT
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    </div>
    <script type="text/javascript">
        var tables = $('#table-style').DataTable({  
            "lengthChange": false,
            "pageLength": 5,
            "columnDefs": [{
                "targets" : [0],
                "visible" : false,
                "searchable" : false
            }]
        });
    </script>
    