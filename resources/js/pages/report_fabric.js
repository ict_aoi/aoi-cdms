$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        paging: false,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/report-fabric/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: 'queu', orderable: true, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
            // {data: 'material', name: 'material',searchable:true,orderable:true},
            // {data: 'color', name: 'color',searchable:true,orderable:true},
            // {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            // {data: 'csi_qty', name: 'csi_qty',searchable:true,orderable:true},
            // {data: 'ssp_qty', name: 'ssp_qty',searchable:true,orderable:true},
            // {data: 'over_consum', name: 'over_consum',searchable:true,orderable:true},
            // {data: 'already_prepared', name: 'already_prepared',searchable:true,orderable:true},
            // {data: 'balance_whs', name: 'balance_whs',searchable:true,orderable:true},
            // {data: 'total_balance', name: 'total_balance',searchable:true,orderable:true},
            // {data: 'remark', name: 'remark',searchable:true,orderable:true},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/report-fabric/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    // download report

    $('#download_report').on('click', function() {
        var cutting_date = $('#cutting_date').val();
        // var data = Array();
        // $('#planningTable tbody tr').each(function(i, v) {
        //     data[i] = Array();
        //     $(this).children('td').each(function(ii, vv) {
        //         data[i][ii] = $(this).text();
        //     });
        // });

        // if(data.length > 1) {
            // $('#data_download').val(JSON.stringify(data));
            $('#data_cutting_date').val(cutting_date);
            $('#download_report_submit').submit();
        // }
    });
});

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id);
            $('#id_plan').val(response.id_plan);
            $('#po_buyer').val(response.po_buyer);
            $('#po_detail_view').text(response.po);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#detailModal').modal();
            get_data_modal_table();
            get_prepare_modal_table();
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        // info: false,
        ajax: {
            url: '/report-fabric/data-detail',
            data: {
                id: $('#planning_id').val(),
                id_plan: $('#id_plan').val(),
                po_buyer: $('#po_buyer').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [

            {data: 'material', name: 'material',searchable:true,orderable:true},
            {data: 'item_id', name: 'item_id',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            {data: 'csi_qty', name: 'csi_qty',searchable:true,orderable:true},
            {data: 'ssp_qty', name: 'ssp_qty',searchable:true,orderable:true},
            // {data: 'already_prepared', name: 'already_prepared',searchable:true,orderable:true},
            {data: 'qty_prepared', name: 'qty_prepared',searchable:true,orderable:true},
            {data: 'fabric_width', name: 'fabric_width',searchable:true,orderable:true},
            {data: 'remark', name: 'remark',searchable:true,orderable:true},

            // {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            // {data: 'part_no', name: 'part_no', searchable:true, visible:true, orderable:false},
            // {data: 'total_cutting', name: 'total_cutting', searchable:false, visible:true, orderable:false},
            // {data: 'status', name: 'status', searchable:false, visible:true, orderable:false},
            // {data: 'detail', name: 'detail', searchable:false, visible:true, orderable:false},
        ],
    });

    return table1;
}


function get_prepare_modal_table() {
    var table2 = $('#prepareTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        // info: false,
        ajax: {
            url: '/report-fabric/data-prepare',
            data: {
                id: $('#planning_id').val(),
                id_plan: $('#id_plan').val(),
                po_buyer: $('#po_buyer').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [

            {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            {data: 'csi_qty', name: 'csi_qty',searchable:true,orderable:true},
            {data: 'qty_prepared', name: 'qty_prepared',searchable:true,orderable:true},
            {data: 'balance', name: 'balance',searchable:true,orderable:true},
            {data: 'persen', name: 'persen',searchable:true,orderable:true},
        ],
    });

    return table2;
}
