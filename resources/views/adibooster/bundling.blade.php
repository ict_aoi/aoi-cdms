@extends('adibooster.index')

@section('page-css')
<style type="text/css">
    .title-fac{
        color: black;
        font-family: Cooper;
        color: white;
        font-size: 40px;
    }
    .title-fac2{
        color: black;
        font-family: Cooper;
        color: white;
        font-size: 30px;
    }
    .title-fac3{
        color: black;
        font-family: Cooper;
        color: black;
        font-size: 35px;
    }
    .text-table {
        font-size: 25px;
    }
    .box-header {
        background-color: #f8e3c4;
    }
    .add-padding-bottom {
        padding-bottom: 50px;
    }
    .not-target {
        background-color: red;
        color: white;
    }
    .get-target {
        background-color: green;
        color: white;
    }
    .content {
        padding-bottom: 0px !important;
    }
    .header-detail {
        color: white;
        font-size: 22px;
    }
</style>
@endsection

@section('page-header')

@endsection

@section('page-content')
<div class="row">
    <div class="row">
        <div class="panel panel-flat" style="background-color: #0072CA">
            <div class="panel-body" >
                <center><label class="title-fac">DASHBOARD OUTPUT CUTTING AOI <span id="factory_id">{{$factory_id}}</span></label></center>
                <center><label class="header-detail">Last updated at <span id="last_update">-</span></label></center>
            </div>
        </div>
    </div>
    <div class="row add-padding-bottom">
        <div class="panel panel-flat" style="background-color: #0072CA">
            <div class="panel-body" >
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3 header-detail">
                                <span>Hari, Tanggal</span>
                            </div>
                            <div class="col-md-6 header-detail">
                                : <span id="current_time">Tidak ada data</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3 header-detail">
                                <span>Target Hari Ini</span>
                            </div>
                            <div class="col-md-6 header-detail">
                                : <span id="total_pcs_plan">Tidak ada data</span> PCS
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3 header-detail">
                                <span>Man Power</span>
                            </div>
                            <div class="col-md-6 header-detail">
                                : <span id="man_power">Tidak ada data</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row add-padding-bottom">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-flat">
                <div class="panel-body" >
                    <div class="table-responsive" style="height: 100%;">
                        <table class="table table-basic table-condensed">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <th class="text-center text-table"></th>
                                    <th class="text-center text-table box-header" id="current_hours">Pencapaian Jam 7 - 8</th>
                                    <th class="text-center text-table box-header" id="current_hours_all">Akumulasi Jam 7 - 8</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center text-table box-header"><strong>Target Output (Pcs)</strong></td>
                                    <td class="text-center text-table" id="target_current_hours">0</td>
                                    <td class="text-center text-table" id="target_current_hours_all">0</td>
                                </tr>
                                <tr>
                                    <td class="text-center text-table box-header"><strong>Actual Output (Pcs)</strong></td>
                                    <td class="text-center text-table" id="actual_current_hours">0</td>
                                    <td class="text-center text-table" id="actual_current_hours_all">0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-flat" style="background-color: #75e3c8">
            <div class="panel-body" >
                <center><label class="title-fac3">TARGET EFF: <span id="target_eff">0</span>% &nbsp; | &nbsp;</label><label class="title-fac3" id="indicator">ACTUAL EFF: <span id="actual_eff">0</span>%</label></center>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">

    var factory_id = $('#factory_id').text();
    
    $(document).ready(function(){
        $.ajax({
            type: 'get',
            url: "{{ route('adibooster.getDataCutting') }}",
            data: {
                factory_id: factory_id,
            },
            success: function (response) {
                console.log(response.data);
                $('#last_update').text(response.data.last_update);
                $('#man_power').text(response.data.man_power);
                $('#current_time').text(response.data.current_time);
                $('#current_target').text(response.data.current_target);
                $('#target_current_hours').text(response.data.target_current_hours);
                $('#current_hours').text(response.data.current_hours);
                $('#current_hours_all').text(response.data.current_hours_all);
                $('#actual_current_hours').text(response.data.actual_current_hours);
                $('#actual_current_hours_all').text(response.data.actual_current_hours_all);
                $('#target_eff').text(response.data.target_eff);
                $('#target_current_hours_all').text(response.data.target_current_hours_all);
                $('#actual_eff').text(response.data.actual_eff);


                if(response.status == 200) {
                    $('#total_pcs_plan').text(response.data.total_pcs_plan);
                    if($('#indicator').hasClass('get-target')) {
                        $('#indicator').removeClass('get-target')
                    }
                    if($('#indicator').hasClass('not-target')) {
                        $('#indicator').removeClass('not-target')
                    }
                    if(response.data.actual_eff < response.data.target_eff) {
                        $('#indicator').addClass('not-target');
                    } else {
                        $('#indicator').addClass('get-target');
                    }
                }
            },
            error: function (response) {
                
            }
        });

        setInterval(function()
        {
            $.ajax({
                type: 'get',
                url: "{{ route('adibooster.getDataCutting') }}",
                data: {
                    factory_id: factory_id,
                },
                success: function (response) {
                    console.log(response.data);
                    $('#last_update').text(response.data.last_update);
                    $('#man_power').text(response.data.man_power);
                    $('#current_time').text(response.data.current_time);
                    $('#current_target').text(response.data.current_target);
                    $('#target_current_hours').text(response.data.target_current_hours);
                    $('#current_hours').text(response.data.current_hours);
                    $('#current_hours_all').text(response.data.current_hours_all);
                    $('#actual_current_hours').text(response.data.actual_current_hours);
                    $('#actual_current_hours_all').text(response.data.actual_current_hours_all);
                    $('#target_eff').text(response.data.target_eff);
                    $('#target_current_hours_all').text(response.data.target_current_hours_all);
                    $('#actual_eff').text(response.data.actual_eff);
                    if(response.status == 200) {
                        $('#total_pcs_plan').text(response.data.total_pcs_plan);
                        if($('#indicator').hasClass('get-target')) {
                            $('#indicator').removeClass('get-target')
                        }
                        if($('#indicator').hasClass('not-target')) {
                            $('#indicator').removeClass('not-target')
                        }
                        if(response.data.actual_eff < response.data.target_eff) {
                            $('#indicator').addClass('not-target');
                        } else {
                            $('#indicator').addClass('get-target');
                        }
                    }
                },
                error: function (response) {
                    
                }
            });
        }, 600000);
    });

</script>
@endsection