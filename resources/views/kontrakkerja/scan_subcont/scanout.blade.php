@extends('layouts.app',['active' => 'scanout_subcont'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Subcont </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Scan Subcont</li>
            <li class="active">Scan Out</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <label><b>Packinglist</b></label>
                    <select data-placeholder="Search Packinglist" name="packinglist" id="packinglist" class="form-control select-search" required="">
                        <option value=""></option>
                        @foreach($pl_sub as $pl)
                            <option value="{{$pl->no_packinglist}}">{{ $pl->no_packinglist }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-4">
                    <label><b>No. Polybag</b></label>
                    <input type="text" name="polybag" id="polybag" class="form-control" placeholder="Polybag Number">
                </div>
                <div class="col-lg-5">
                    <div class="col-lg-12">
                        <label><b>Barcode Bundel</b></label>
                        <input type="text" name="bundle" id="bundle" class="form-control input-new" placeholder="#scan barcode">
                    </div>
                </div>
            </div> 
            <hr>
            <div class="row">
                <div class="col-lg-2">
                    <label><b>Packinglist</b></label><br>
                    <label id="lbpl"></label>
                </div>
                <div class="col-lg-2">
                    <label><b>No. Kontrak Kerja</b></label><br>
                    <label id="lbkk"></label>
                </div>
                <div class="col-lg-2">
                    <label><b>Style</b></label><br>
                    <label id="lbsty"></label>
                </div>
                <div class="col-lg-2">
                    <label><b>PO. Number</b></label><br>
                    <label id="lbpo"></label>
                </div>
                <div class="col-lg-2">
                    <label><b>Proses</b></label><br>
                    <label id="lbpros"></label>
                </div>
                <div class="col-lg-2">
                    <label><b>Subcont</b></label><br>
                    <label id="lbsubc"></label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-lg-11"></div>
            <div class="col-lg-1">
                <button type="button" class="btn btn-success" id="btn-submit" name="btn-submit"><span class="icon-upload"></span> Submit</button>
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Barcode Id</th>
                        <th>Packinglist</th>
                        <th>Polybag</th>
                        <th>Qty</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
    $(document).ready(function(){
        var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('Subcont.getDataTemp') }}",
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'bundle_id', name: 'bundle_id'},
            {data: 'id_packinglist', name: 'id_packinglist'},
            {data: 'no_polybag', name: 'no_polybag'},
            {data: 'qty', name: 'qty'},
            {data: 'action', name: 'action'}

        ]
    });


    $('.input-new').keypress(function(event){
        if (event.which==13) {
            event.preventDefault();
            if ($('#packinglist').val=='' || $('#polybag').val()=='') {
                 $("#alert_warning").trigger("click",'Packinglist / Polybag tidak boleh kosong !!!');
            }else{
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url : "{{ route('Subcont.ajaxScanOut') }}",
                    data :{barcode:$('#bundle').val(),nopl:$('#packinglist').val(),nobag:$('#polybag').val()},
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        var data_response = response.data;
                        $.unblockUI();
                        if (data_response.status==200) {
                            $("#alert_success").trigger("click", data_response.output);
                        }else{
                            $("#alert_warning").trigger("click",data_response.output);
                        }
                        tableL.clear();
                        tableL.draw();
                        $('.input-new').val('');
                    },
                    error: function(response) {
                        var data_response = response.data;
                        $("#alert_warning").trigger("click",data_response.output);
                        $.unblockUI();
                        tableL.clear();
                        tableL.draw();
                        console.log(response);
                    }
                });
            }
        }
    });

    $('#btn-submit').on('click',function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('Subcont.submitPackinglist') }}",
            beforeSend: function() {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function(response) {
                var data_response = response.data;
                $.unblockUI();
                if (data_response.status==200) {
                    $("#alert_success").trigger("click", data_response.output);
                }else{
                    $("#alert_warning").trigger("click",data_response.output);
                }
                tableL.clear();
                tableL.draw();
            },
            error: function(response) {
                var data_response = response.data;
                $("#alert_warning").trigger("click",data_response.output);
                $.unblockUI();
                tableL.clear();
                tableL.draw();
                console.log(response);
            }
        });

    });

    $('#packinglist').on('change',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('Subcont.getDataPl') }}",
            data :{nopl:$('#packinglist').val()},
            success: function(response) {
                var data = response.data;
                $('#lbpl').text(data.nopl);
                $('#lbkk').text(data.nokk);
                $('#lbsubc').text(data.subcont);
                $('#lbpros').text(data.process);
                $('#lbpo').text(data.po_buyer);
                $('#lbsty').text(data.style);
                $('#polybag').val('');
            },
            error: function(response) {
              console.log(response);  
            }
        });
    });

});
function delbarcode(e){
    var id = $(e).data('id');
    var tableL = $('#table-list').DataTable();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url : "{{ route('Subcont.deleteBundleTmp') }}",
        data :{id:id},
        beforeSend: function() {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function(response) {
            var data_response = response.data;
            $.unblockUI();
            if (data_response.status==200) {
                $("#alert_success").trigger("click", data_response.output);
            }else{
                $("#alert_warning").trigger("click",data_response.output);
            }
            tableL.clear();
            tableL.draw();
        },
        error: function(response) {
            var data_response = response.data;
            $("#alert_warning").trigger("click",data_response.output);
            $.unblockUI();
            tableL.clear();
            tableL.draw();
            console.log(response);
        }
    });

}
</script>
@endsection