<?php

namespace App\Http\Controllers\Spreading;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\Spreading\FabricUsed;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\RequestFabric\ReportRoll;
use App\Http\Controllers\Controller;

class CuttingInstructionController extends Controller
{
    public function index()
    {
        return view('spreading.cutting_instruction.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {
                $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('deleted_at', null);

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->addColumn('po', function($data) {
                    $view = implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                    return $view;
                })
                ->addColumn('action', function($data) {
                    return view('spreading.cutting_instruction._action', [
                        'model'      => $data,
                        'detail'     => route('SpreadingCI.detailModal',$data->id)
                    ]);
                })
                ->rawColumns(['action','status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->make(true);
            }
        }
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax())
        {
            $style = $request->style;
            $articleno = $request->articleno;
            $cutting_date = $request->cutting_date;
            $po_buyer = json_decode($request->po_buyer);
            $po            = "'" . implode("','",$po_buyer) . "'";

            if($style != null && $articleno != null && $cutting_date != null && $po_buyer != null) {
                // $data = DB::connection('wms_live')->table('integration_whs_to_cutting')
                // ->select(DB::raw("planning_date, document_no, style, article_no, barcode, nomor_roll,
                //  item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement"))
                //  ->where('planning_date', $cutting_date)->where('style', $style)->where('article_no', $articleno)
                //  ->whereIn('po_buyer', $po_buyer)->orderBy('item_code', 'asc')->orderBy('article_no', 'asc')
                //  ->groupBy('planning_date', 'document_no', 'style', 'article_no', 'barcode', 'nomor_roll', 'item_code',
                //   'color', 'actual_lot', 'actual_width', 'last_status_movement');

                  $data = DB::connection('wms_live_new')->select(DB::raw("SELECT planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, sum(qty_prepared) as qty_prepared, last_status_movement FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$articleno' AND po_buyer IN($po) GROUP BY planning_date, style, article_no, barcode, nomor_roll, document_no, item_code, color, actual_lot, actual_width, last_status_movement ORDER BY item_code,article_no"));

                return datatables()->of($data)
                ->addColumn('qty_sisa', function($data) {
                    $fabric_use_check = FabricUsed::where('barcode_fabric', $data->barcode)->orderBy('created_spreading', 'desc')->get();
                    if(count($fabric_use_check) > 0) {
                        return $fabric_use_check->first()->actual_sisa;
                    } else {
                        return 'Belum digunakan';
                    }
                })
                ->addColumn('action', function($data) {
                    $report_roll = ReportRoll::where('barcode', $data->barcode)->get();
                    if(count($report_roll) > 0) {
                        return '<center>Sudah Diterima</center>';
                    } else {
                        if($data->last_status_movement == 'relax') {
                            return '<center>Masih Prepare</center>';
                        } else {
                            if($data->actual_lot == 'CLOSING') {
                                return '<center>-</center>';
                            } else {
                                return '<center>Belum Diterima</center>';
                            }
                        }
                    }
                })
                ->editColumn('qty_prepared', function($data) {
                    return round($data->qty_prepared, 2);
                })
                ->setRowAttr([
                    'style' => function($data)
                    {
                        $report_roll = ReportRoll::where('barcode', $data->barcode)->get();
                        if(count($report_roll) > 0) {
                            $fabric_use_check = FabricUsed::where('barcode_fabric', $data->barcode)->orderBy('created_spreading', 'desc')->get();
                            if(count($fabric_use_check) > 0) {
                                return  'background-color: #96bfff;';
                            } else {
                                return  'background-color: #74ff76;';
                            }
                        } else {
                            if($data->last_status_movement == 'relax') {
                                return  'background-color: #fffa99;';
                            } else {
                                if($data->actual_lot == 'CLOSING') {
                                    return  'background-color: #ffac91;';
                                }
                            }
                        }
                    },
                ])
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailModal($id)
    {
        $detail = CuttingPlan::where('id', $id)->where('deleted_at', null)->first();
        $obj = new StdClass();
        $obj->id = $detail->id;
        $obj->style = $detail->style;
        $obj->articleno = $detail->articleno;
        $obj->po_buyer = $detail->po_details->pluck('po_buyer')->toArray();
        $obj->cutting_date = $detail->cutting_date;
		return response()->json($obj,200);
    }
}
