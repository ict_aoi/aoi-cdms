<!-- MODAL -->
<div id="modal_sync_" class="modal fade" role="dialog">
    <div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title" id="title-style"><i class="position-left"></i>Choose Style</h5>
					</div>
                  	<div class="panel-body loader-area">
						<fieldset>
                            <input type="hidden" id="style_sync" name="style_sync">
                            <div id="listing" class="col-lg-12">
                                <!-- template pilihan -->
                            </div>
                        </fieldset>
                  	</div>
              	</div>
            </div>
    </div>
</div>
<!-- /MODAL -->