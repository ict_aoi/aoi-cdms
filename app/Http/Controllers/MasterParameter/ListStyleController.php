<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class ListStyleController extends Controller
{
    //
    public function index()
    {
        $seasons = DB::table('master_seasons')
                    ->whereNull('deleted_at')
                    ->get();
        
        $difficulty_level = \Config::get('constants.difficulty_level');
        $fabric_category = \Config::get('constants.fabric_category');
        $fabric_group = \Config::get('constants.fabric_group');
        $status_style = \Config::get('constants.status_style');
        $stripes = \Config::get('constants.stripes');
        $status_ad = \Config::get('constants.status_ad');
        $engineering_fabric = \Config::get('constants.engineering_fabric');
        $asc_priority = \Config::get('constants.ASC_PRIORITY');

        $product_type = DB::table('master_product_type')
                            ->whereNull('deleted_at')
                            ->get();

        $product_category = DB::table('master_product_category')
                            ->whereNull('deleted_at')
                            ->get();

        $main_material = DB::table('master_main_material')
                            ->whereNull('deleted_at')
                            ->get();
        
        $bisnis_unit = DB::table('master_bisnis_unit')
                            ->whereNull('deleted_at')
                            ->get();

        $styles = DB::connection('erp_live') 
                    ->table('rma_style_fg')
                    ->select(DB::raw('upc as style'))
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%TOP%')
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%BOTTOM%')
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%INNER%')
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%OUT%')
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%-%')
                    ->where(DB::raw('upper(upc)'), 'NOT LIKE', '%X%')
                    ->groupBy('upc')
                    ->get();

        $types = DB::table('types')
                        ->whereNull('deleted_at')
                        ->get();

        return view('master_parameter.style.listing.index', compact('seasons', 'difficulty_level', 'fabric_category', 'fabric_group', 'status_style', 'product_type', 'product_category', 'main_material', 'bisnis_unit', 'styles', 'stripes', 'types', 'status_ad', 'engineering_fabric', 'asc_priority'));
    }

    public function ajaxGetDataListStyle(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_list_style')
                    ->select('master_list_style.id','master_list_style.season', 'master_list_style.style_standart', 'master_list_style.style', 'master_list_style.product_type', 'master_list_style.product_category', 'master_list_style.bisnis_unit', 'master_list_style.main_material', 'master_list_style.desc_material', 'master_list_style.fabric_category', 'master_list_style.fabric_group', 'master_list_style.difficulty_level', 'master_list_style.mp', 'master_list_style.total', 'master_list_style.status_style', 'master_list_style.tsme', 'master_seasons.season_name', 'master_list_style.tslc', 'master_list_style.set_type', 'master_list_style.stripes', 'master_list_style.ad', 'master_list_style.status_ad', 'master_list_style.engineering_fabric', 'master_list_style.asc_priority', 'master_list_style.min_diameter_hemning', 'master_list_style.max_diameter_hemning', 'master_list_style.critical_process', 'master_list_style.update_spt_input')
                    ->join('master_seasons', 'master_seasons.id', '=', 'master_list_style.season')
                    ->wherenull('master_list_style.deleted_at')
                    ->wherenull('master_seasons.deleted_at')
                    ->orderBy('master_list_style.created_at', 'desc')
                    ->get();

            //datatables
            return DataTables::of($data)
                ->editColumn('tsme', function($data){
                    return str_replace('.',',',$data->tsme);
                })
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editListStyle', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteListStyle', $data->id)
                    ]);
                })
            ->make(true);
        }
    }

    //add new master list style
    public function addListStyle(Request $request)
    {

        $this->validate($request, [
            'season' => 'required',
            'style_standart' => 'required',
            'style' => 'required',
            'product_type' => 'required',
            'product_category' => 'required',
            'bisnis_unit' => 'required',
            'main_material' => 'required',
            'fabric_category' => 'required',
            'fabric_group' => 'required',
            'difficulty_level' => 'required',
            'mp' => 'required',
            'total' => 'required',
            'status_style' => 'required',
            'tsme' => 'required'
        ]);

        $data = array([
            'season' => $request->season,
            'style_standart' => $request->style_standart,
            'style' => trim($request->style),
            'product_type' => $request->product_type,
            'product_category' => $request->product_category,
            'bisnis_unit' => $request->bisnis_unit,
            'main_material' => $request->main_material,
            'desc_material' => $request->desc_material,
            'fabric_category' => $request->fabric_category,
            'fabric_group' => $request->fabric_group,
            'difficulty_level' => $request->difficulty_level,
            'stripes' => $request->stripes != 'null' ? $request->stripes : null,
            'set_type' => $request->set_type,
            'ad' => $request->ad,
            'status_ad' => $request->status_ad,
            'mp' => $request->mp,
            'total' => $request->total,
            'status_style' => $request->status_style,
            'engineering_fabric' => $request->engineering_fabric,
            'asc_priority' => $request->asc_priority,
            'tsme' =>  $request->tsme ? str_replace(',', '.', $request->tsme) : null,
            'tslc' =>  $request->tslc ? str_replace(',', '.', $request->tslc) : null,
            'min_diameter_hemning' =>  $request->min_diameter_hemning ? str_replace(',', '.', $request->min_diameter_hemning) : null,
            'max_diameter_hemning' =>  $request->max_diameter_hemning ? str_replace(',', '.', $request->max_diameter_hemning) : null,
            'critical_process' => $request->critical_process,
            'update_spt_input' => $request->update_spt_input,
            'created_at' => Carbon::now()
        ]);

        // cek exists
        $cekifexist = DB::table('master_list_style')
                        ->where('style_standart', $request->style_standart)
                        ->where('style', trim($request->style))
                        ->where('set_type', trim($request->set_type))
                        ->where('season', $request->season)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('List Style already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table
            DB::table('master_list_style')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master
    public function editListStyle($id)
    {
        $master_list_style = DB::table('master_list_style')
                                ->select('master_list_style.id','master_list_style.season', 'master_list_style.style_standart', 'master_list_style.style', 'master_list_style.product_type', 'master_list_style.product_category', 'master_list_style.bisnis_unit', 'master_list_style.main_material', 'master_list_style.desc_material', 'master_list_style.fabric_category', 'master_list_style.fabric_group', 'master_list_style.difficulty_level', 'master_list_style.mp', 'master_list_style.total', 'master_list_style.status_style', 'master_list_style.tsme', 'master_seasons.season_name', 'master_list_style.tslc', 'master_list_style.set_type', 'master_list_style.stripes', 'master_list_style.ad', 'master_list_style.status_ad','master_list_style.engineering_fabric', 'master_list_style.asc_priority', 'master_list_style.min_diameter_hemning', 'master_list_style.max_diameter_hemning', 'master_list_style.critical_process', 'master_list_style.update_spt_input')
                                ->join('master_seasons', 'master_seasons.id', '=', 'master_list_style.season')
                                ->where('master_list_style.id', $id)
                                ->first();

        return response()->json($master_list_style, 200);
    }

    //update master
    public function updateListStyle(Request $request)
    {
        $data = $request->all();

        try {
            DB::beginTransaction();

                $update = DB::table('master_list_style')
                            ->where('id', $data['id_update'])
                            ->update([
                                'season' => $request->season_update,
                            // 'style' => trim($request->style_update),
                                'product_type' => $request->product_type_update,
                                'product_category' => $request->product_category_update,
                                'bisnis_unit' => $request->bisnis_unit_update,
                                'main_material' => $request->main_material_update,
                                'desc_material' => $request->desc_material_update,
                                'fabric_category' => $request->fabric_category_update,
                                'fabric_group' => $request->fabric_group_update,
                                'difficulty_level' => $request->difficulty_level_update,
                                'stripes' => $request->stripes_update != 'null' ? $request->stripes_update : null,
                                'set_type' => $request->set_type_update,
                                'ad' => $request->ad_update,
                                'status_ad' => $request->status_ad_update,
                                'mp' => $request->mp_update,
                                'total' => $request->total_update,
                                'status_style' => $request->status_style_update,
                                'engineering_fabric' => $request->engineering_fabric_update,
                                'asc_priority' => $request->asc_priority_update,
                                'tsme' => $request->tsme_update ? str_replace(',', '.', $request->tsme_update) : null,
                                'tslc' => $request->tslc_update ? str_replace(',', '.', $request->tslc_update) : null,
                                'min_diameter_hemning' => $request->min_diameter_hemning_update ? str_replace(',', '.', $request->min_diameter_hemning_update) : null,
                                'max_diameter_hemning' => $request->max_diameter_hemning_update ? str_replace(',', '.', $request->max_diameter_hemning_update) : null,
                                'critical_process' => $request->critical_process_update,
                                'update_spt_input' => $request->update_spt_input_update,
                                'updated_at' => Carbon::now()
                            ]);

                if ($update) {
                    // cek exists
                    $cekifexist = DB::table('master_list_style')
                            ->where('style_standart', $request->style_update)
                            ->where('style', trim($request->style_update))
                            ->where('set_type', trim($request->set_type_update))
                            ->where('season', $request->season_update)
                            ->whereNull('deleted_at')
                            ->count();

                    if ($cekifexist > 1) {
                        return response()->json('List Style already exists..!', 422);
                    }
                }
                
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        // return $this->index();

        // return view('master_parameter.style.listing.index');

    }

    //delete master
    public function deleteListStyle(Request $request)
    {
        $id = $request->id;
        try {
            DB::beginTransaction();

                DB::table('master_list_style')
                    ->where('id', $id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
                    
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('master_parameter.style.listing.index');
    }
}
