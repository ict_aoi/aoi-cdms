<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ItemIdRelated extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'item_id_relateds';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['relation_id', 'item_id', 'deleted_at'];
}
