<?php

namespace App\Http\Controllers\Ppcm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterPpcm;
use App\Models\MasterSetting;
use File;
use Excel;
use Carbon\Carbon;
use DB;
use Auth;
use Uuid;
use Illuminate\Support\Facades\Storage;
use DataTables;

class MasterPpcmController extends Controller
{
    public function index(){
    	return view('ppcm.index');
    }

    public function uploadPpcm(Request $request){
    	$ext = $request->file_upload->getClientOriginalExtension();

    	if ($request->hasFile('file_upload') && $ext='csv') {
    		$path = $request->file('file_upload')->getRealPath();
    		$data = Excel::selectSheets('active')->load($path,function($render){})->get();
     
    		if (!empty($data)) {
    			try {
    				db::begintransaction();
   						foreach ($data as $key => $value) {
                
                if ($value!='') {
                    $fact = $this->factoryCheck(explode(' ',$value->line)[0]);
                    $area = $this->areaId($value->line);
                    $po = substr($value->po_buyer,1);
                    $styleset = substr(explode('::',$value->style)[0], 1) ;
                    $season = !empty(explode('::',$value->style)[1]) ? explode('::',$value->style)[1] : '-';
                    $qty_order = $value->qty_order;
                    $qty_split = $value->split_qty;
                    $start = date('Y-m-d',strtotime($value->start));
                    $end = date('Y-m-d',strtotime($value->end));
                    $article = substr($value->article,1);
                    
                    $style = explode('-',$styleset)[0];
                    $set_type = !empty(explode('-',$styleset)[1]) ? explode('-',$styleset)[1] : 'Non';

                    if($set_type != 'Non') {
                      if($set_type == 'BOT') {
                        $set_type = 'BOTTOM';
                      }
                    }

                    $this->_check_data($area,$po,$style,$article);

                    if ($area!=null) {
                      $data_in = array(
                        'area_id'=>$area,
                        'factory_id'=>$fact,
                        'po_buyer'=>$po,
                        'style'=>$style,
                        'season'=>$season,
                        'set_type'=>$set_type,
                        'articleno'=>$article,
                        'qty_order'=>$qty_order,
                        'qty_split'=>$qty_split,
                        'start_date'=>$start,
                        'end_date'=>$end,
                        'user_id'=>Auth::user()->id,
                      );
                      MasterPpcm::FirstOrCreate($data_in);
                    }
                }
                 
   							
   						}
            
    				db::commit();
    				$data_response = [
                            'status' => 200,
                            'output' => 'Upload Success'
                          ];
            
    			} catch (Exception $ex) {
    				db::rollback();
			        $message = $ex->getMessage();
			        \ErrorHandler($message);

			        $data_response = [
			                            'status' => 422,
			                            'output' => 'upload failed !!!'
			                          ];
    			}
    		}else{
    				 $data_response = [
			                            'status' => 422,
			                            'output' => 'upload failed, empty !!!'
			                          ];
    		}
    		
    	}else{
    		 $data_response = [
				                'status' => 422,
				                'output' => 'upload failed, file type !!!'
				              ];
    	}

    	return response()->json(['data' => $data_response]);
    }


    public function getDataPpcm(Request $request){
      $from = date_format(date_create(explode('-', preg_replace('/\s+/', '', $request->date_range))[0]),'Y-m-d 00:00:00');
      $to = date_format(date_create(explode('-', preg_replace('/\s+/', '', $request->date_range))[1]),'Y-m-d 23:59:59');

            $data = DB::table('master_ppcm')
                          ->leftJoin('master_setting','master_ppcm.area_id','=','master_setting.id')
                          ->whereBetween('master_ppcm.start_date',[$from,$to])
                          ->where('master_setting.factory_id',Auth::user()->factory_id)
                          ->whereNull('master_ppcm.deleted_at')
                          ->orderBy('master_ppcm.start_date','asc')
                          ->select('master_setting.area_name','master_ppcm.po_buyer','master_ppcm.style','master_ppcm.season','master_ppcm.qty_order','master_ppcm.qty_split','master_ppcm.start_date','master_ppcm.end_date','master_ppcm.articleno','master_ppcm.created_at','master_ppcm.set_type','master_setting.alias');
          return DataTables::of($data)
                        ->editColumn('area_name',function($data){
                          if(\Auth::user()->factory_id == 1){
                            return $data->alias;
                          }else{
                            return $data->area_name;
                          }
                        })
                        ->addColumn('upload',function($data){
                            return $data->created_at;
                        })
                        ->editColumn('start_date',function($data){
                          return Carbon::parse($data->start_date)->format('d M Y');
                        })
                        ->editColumn('end_date',function($data){
                          return Carbon::parse($data->end_date)->format('d M Y');
                        })
                      ->rawColumns(['start_date','end_date'])
                        ->make(true);
    }




    //function check
    private function factoryCheck($factory){
    	switch ($factory) {
    		case 'AOI#1':
    			$factory_id = '1';
    			break;
    		case 'AOI#2':
    			$factory_id = '2';
    			break;
    		
    		default:
    			$factory_id = '0';
    			break;
    	}

    	return $factory_id;
    }

    private function areaId($line){
    	
    	$factory = explode(' ',$line)[0];
    	$area = explode(' ',$line)[1];
    	switch ($factory) {
    		case 'AOI#1':
    			$factory_id = '1';
    			break;
    		case 'AOI#2':
    			$factory_id = '2';
    			break;
    		
    		default:
    			$factory_id = '0';
    			break;
    	}

    	$areaid = MasterSetting::where('area_name',$area)->where('factory_id',$factory_id)->whereNull('deleted_at')->first();

      if ($areaid!=null) {
         $return = $areaid->id;
      }else{
          $return = null;
      }

    	return $return;
    }

    private function _check_data($area_id,$pobuyer,$style,$article){
    		$ppcm = MasterPpcm::where('area_id',$area_id)->where('po_buyer',$pobuyer)->where('style',$style)->where('articleno',$article)->whereNull('deleted_at')->first();

    		if ($ppcm!=null) {
    			MasterPpcm::where('id',$ppcm->id)->update(['deleted_at'=>Carbon::now()]);
    		}
    }

    public function UploadPpcmNew(Request $request){
      if($request->hasFile('file_upload')){
          $extension = \File::extension($request->file_upload->getClientOriginalName());
          if ($extension == "csv" | $extension == "xlx" || $extension == "xlsx") {
              $path = $request->file_upload->getRealPath();
              $data = \Excel::selectSheetsByIndex(0)->load($path)->get();
          }else{
            return response()->json('Ekstensi File Tidak Diizinkan!',422);
          }
      }else{
          return response()->json('Pilih File PPCM Dahulu!', 422);
      }
      foreach($data as $key => $val){
        if($val->line == null){
          return response()->json('Format Data di Kolom Line pada baris A'. (string)($key+2) . ' Tidak Valid / Kosong!',422);
        }
        if($val->po_buyer == null){
          return response()->json('Format Data di Kolom PO Buyer pada baris B'. (string)($key+2) . ' Tidak Valid / Kosong!',422);
        }
        if($val->style == null){
          return response()->json('Format Data di Kolom Style pada baris C'. (string)($key+2) . ' Tidak Valid / Kosong!',422);
        }
        if($val->qty_order == null){
          return response()->json('Format Data di Kolom Qty Order pada baris D'. (string)($key+2) . ' Tidak Valid / Kosong!',422);
        }
        if($val->qty_split == null){
          return response()->json('Format Data di Kolom Qty Split pada baris E'. (string)($key+2) . ' Tidak Valid / Kosong!',422);
        }
        if($val->start == null){
          return response()->json('Format Data di Kolom Start pada baris F'. (string)($key+2) . ' Tidak Valid / Kosong! ',422);
        }
        if($val->end == null){
          return response()->json('Format Data di Kolom End pada baris G'. (string)($key+2) . ' Tidak Valid / Kosong! ',422);
        }
        if($val->article == null){
          return response()->json('Format Data di Kolom Article pada baris H'. (string)($key+2) . ' Tidak Valid / Kosong! ',422);
        }
        $factory = explode(' ',$val->line);
        if(strpos($factory[0],'1') == true){
            $factory_id = '1';
        }elseif(strpos($factory[0],'2') == true){
            $factory_id = '2';
        }else{
            $factory_id = '0';
        }
        if($factory_id != \Auth::user()->factory_id){
          return response()->json('Anda Tidak Diperkenankan Mengunggah File lintas Factory!',422);
        }
        $style_temp = explode("::",$val->style);
        $style_set    = substr($style_temp[0],0);
        $style = explode('-',$style_set);
        $season = $style_temp[1];
        if(strpos($style_temp[0],"TOP") > 0){
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = 'TOP';
        }elseif(strpos($style_temp[0],"BOT")  > 0){
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = "BOTTOM";
        }else{
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = "Non";
        }
        $line = explode(' ',$val->line);
        if(\Auth::user()->factory_id != 1){
          $area_id = MasterSetting::where('area_name',strtoupper(trim($line[1])))
          ->where('factory_id',$factory_id)
          ->first();
          if($area_id == null){
            return response()->json($line[1].' Tidak Ditemukan!', 422);
          }
          $area = $area_id->area_name;
          $area_id = $area_id->id;
        }else{
          $area_id = MasterSetting::where('alias',strtoupper(trim($line[1])))
          ->where('factory_id',$factory_id)
          ->first();
          if($area_id == null){
            return response()->json($line[1].' Tidak Ditemukan!', 422);
          }
          $area = $area_id->alias;
          $area_id = $area_id->id;
        }
        if($line[1] == '' || $line[1] == null){
            return response()->json('LINE TIDAK BOLEH KOSONG!', 422);
        }
        if($val->po_buyer == '' || $val->po_buyer == null){
            return response()->json('PO Buyer Tidak Boleh Kosong !', 422);
        }
        if($val->qty_order == '' || $val->qty_order == null){
            return response()->json('Qty Order Tidak Boleh Kosong !', 422);
        }
        if(strpos($val->qty_order,'.') > 0 || strpos($val->qty_order,',') > 0){
          return response()->json('Format Qty Order Salah !', 422);
        }
        if(strpos($val->qty_split,'.') > 0 || strpos($val->qty_split,',') > 0){
            return response()->json('Format Qty Split Salah!', 422);
        }
        if($val->qty_split == '' || $val->qty_split == null){
            return response()->json('Split Qty Tidak Boleh Kosong !'.substr($val->po_buyer,1).' Pada Baris '.$key, 422);
        }
        if($val->start == '' || $val->start == null){
            return response()->json('Tanggal Start Sewing Tidak Boleh Kosong !', 422);
        }
        if($val->end == '' || $val->end == null){
          return response()->json('Tanggal End Sewing Tidak Boleh Kosong !', 422);
        }
  
        $cek_exists = MasterPpcm::where('po_buyer',substr($val->po_buyer,1))
        ->where('season',$season)
        ->where('style',$style)
        ->where('set_type',$set_type)
        ->where('articleno',trim(substr($val->article,1)))
        ->where('factory_id',\Auth::user()->factory_id)
        ->whereNull('deleted_at')
        ->first();
        if($cek_exists != null){
          try {
            DB::begintransaction();
              MasterPpcm::where('po_buyer',substr($val->po_buyer,1))
              ->where('season',$season)
              ->where('style',$style)
              ->where('set_type',$set_type)
              ->where('articleno',substr($val->article,1))
              ->where('factory_id',\Auth::user()->factory_id)
              ->whereNull('deleted_at')
              ->update([
                'deleted_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
              ]);
            DB::commit();
          } catch (Exception $er) {
              DB::rollback();
              $message = $er->getMessage();
              ErrorHandler::db($message);
          }
        }
      }
      foreach($data as $x => $y){
        $factory = explode(' ',$y->line);
        if(strpos($factory[0],'1') > 0){
            $factory_id = 1;
        }elseif(strpos($factory[0],'2') > 0){
            $factory_id = 2;
        }else{
            $factory_id = 0;
        }
        $style_temp = explode("::",$y->style);
        $style_set    = substr($style_temp[0],0);
        $style = explode('-',$style_set);
        $season = $style_temp[1];
        if(strpos($style_temp[0],"TOP") > 0){
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = 'TOP';
        }elseif(strpos($style_temp[0],"BOT")  > 0){
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = "BOTTOM";
        }else{
            $style    = substr(trim(strtoupper($style[0])),1);
            $set_type = "Non";
        }
        $line = explode(' ',$y->line);
        if(\Auth::user()->factory_id != 1){
          $area_id = MasterSetting::where('area_name',strtoupper(trim($line[1])))
          ->where('factory_id',$factory_id)
          ->first();
          if($area_id == null){
            return response()->json($line[1].' Tidak Ditemukan!', 422);
          }
          $area = $area_id->area_name;
          $area_id = $area_id->id;
        }else{
          $area_id = MasterSetting::where('alias',strtoupper(trim($line[1])))
          ->where('factory_id',$factory_id)
          ->first();
          if($area_id == null){
            return response()->json($line[1].' Tidak Ditemukan!', 422);
          }
          $area = $area_id->alias;
          $area_id = $area_id->id;
        }
        //AKUMULATIF DARI PO-SEASON-STYLE YANG SUDAH PERNAH DI SUPPLY
        $get_info_last_updated = MasterPpcm::where('po_buyer',substr($y->po_buyer,1))
        ->where('season',$season)
        ->where('style',$style)
        ->where('set_type',$set_type)
        ->where('articleno',substr($y->article,1))
        ->where('factory_id',\Auth::user()->factory_id)
        ->orderBy('deleted_at','DESC')
        ->first();
        try {
          DB::begintransaction();
              $actual_qty = $get_info_last_updated == null ? 0 : $get_info_last_updated->qty_actual;
              DB::table('master_ppcm')->insert([
                'id'            => Uuid::generate()->string,
                'area_id'       => $area_id,
                'factory_id'    => $factory_id,
                'set_type'      => $set_type,
                'season'        => trim(strtoupper($season)),
                'articleno'     => trim(substr($y->article,1)),
                'qty_order'     => (int)$y->qty_order,
                'qty_split'     => (int)$y->qty_split,
                'qty_actual'    => $actual_qty,
                'start_date'    => date('Y-m-d',strtotime($y->start)),
                'end_date'      => date('Y-m-d',strtotime($y->end)),
                'po_buyer'      => trim(substr($y->po_buyer,1)),
                'style'         => $style,
                'created_at'    => Carbon::now(),
                'area_name'     => $area,
                'user_id'       => \Auth::user()->id
              ]);
              // DB::connection('sds_live')->table('ppcm_upload_new')->insertGetId([
              //   'line'            => $y->line,
              //   'poreference'     => trim(substr($y->po_buyer,1)),
              //   'article'         => trim(substr($y->article,1)),
              //   'style'           => $style,
              //   'season'          => trim(strtoupper($season)),
              //   'qty_order'       => (int)$y->qty_order,
              //   'qty_split'       => (int)$y->qty_split,
              //   'start'           => date('Y-m-d',strtotime($y->start)),
              //   'end'             => date('Y-m-d',strtotime($y->end)),
              //   'created_at'      => Carbon::now(),
              //   'nik'             => \Auth::user()->nik
              // ]);
          DB::commit();
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }
      }

      // foreach($data as $x => $y){
      //   $poreference = trim(substr($y->po_buyer,1));
      //   $d_rec = DB::connection('sds_live')->select(DB::raw("SELECT
      //   ds.style_regx,ds.season,TRIM(ds.poreference) AS poreference,ds.article,
      //   CASE WHEN pu.line ~ ds.line_regx THEN pu.line ELSE NULL END AS line_alloc,
      //   SUM(ds.line_stock) AS line_stock
      //   FROM dist_stock_new ds
      //   LEFT JOIN (
      //       SELECT * FROM ppcm_upload_new WHERE deleted_at IS NULL AND poreference='$poreference'
      //   ) pu ON ds.season=pu.season AND ds.poreference=pu.poreference AND ds.article=pu.article AND pu.style ~ ds.style_regx AND pu.line ~ ds.line_regx
      //   WHERE ds.line_stock > 0 AND ds.poreference='$poreference'
      //   GROUP BY ds.season,ds.poreference,ds.article,ds.style_regx,line_alloc
      //   ORDER BY ds.season,ds.poreference,ds.article,ds.style_regx,line_alloc ASC"));
      //   if(count($d_rec) > 0){
      //     foreach($d_rec as $dr){
      //         $c_qty=$dr->line_stock;
      //         $c_ssn=$dr->season;
      //         $c_po=$dr->poreference;
      //         $c_st=$dr->style_regx;
  
      //         if($dr->line_alloc == '' || $dr->line_alloc == null){
      //             // $sql="SELECT ppcm_upload_gen_new AS c_qty FROM ppcm_upload_gen_new(?,?,?,?,?::INTEGER)";
      //             // $arg=array($dr->season,$dr->article,$dr->poreference,$dr->style_regx,$c_qty); // SEASON,ARTICLE,PO,STYLE_REGEX,C_QTY
      //             DB::connection('sds_live')->select(DB::raw("SELECT ppcm_upload_gen_new AS c_qty FROM ppcm_upload_gen_new('$dr->season','$dr->article','$dr->poreference','$dr->style_regx','$c_qty')"));
      //         }else{
      //             // $sql="SELECT ppcm_upload_sp_line_new AS c_qty FROM ppcm_upload_sp_line_new(?,?,?,?,?,?::INTEGER)";
      //             // $arg=array($dr->season,$dr->article,$dr->poreference,$dr->style_regx,$dr->line_alloc,$c_qty); // SEASON,ARTICLE,PO,STYLE_REGEX,LINE_STRING,C_QTY
      //             DB::connection('sds_live')->select(DB::raw("SELECT ppcm_upload_sp_line_new AS c_qty FROM ppcm_upload_sp_line_new('$dr->season','$dr->article','$dr->poreference','$dr->style_regx','$dr->line_alloc','$c_qty')"));
      //         }
      //         // $c_qty = $this->db->query($sql,$arg)->result()[0]->c_qty;
      //     }
      //   }
      // }
      // DB::connection('sds_live')->table('system_log')->insert([
      //   'user_nik'      => \Auth::user()->nik,
      //   'user_proc'     => '0',
      //   'log_operation' => 'ppcm_upload',
      //   'log_text'      => 'upload file '.$request->file_upload->getClientOriginalName(),
      //   'add_note'      => count($data).' new PO',
      //   'log_time'      => Carbon::now()
      // ]);
    }
}
