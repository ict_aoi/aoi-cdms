@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Download File SSP</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Download File SSP</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="uploadDataTable">
                <thead>
                    <tr>
                        <th>Queue</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO Buyer</th>
                        <th>Color</th>
                        <th>Size Type</th>
                        <th>Factory</th>
                        <th>SSP Upload</th>
                        <th>Last Update</th>
                        <th>Detail</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('request_marker.download_file_ssp._upload_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/download_file_ssp.js') }}"></script>
@endsection

