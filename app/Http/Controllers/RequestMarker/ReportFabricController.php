<?php

namespace App\Http\Controllers\RequestMarker;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ReportFabricController extends Controller
{
    public function index()
    {
        return view('request_marker.report_fabric.index');
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id == 1) {
                    $factory = '1000001';
                } elseif(\Auth::user()->factory_id == 2) {
                    $factory = '1000011';
                } else {
                    $factory = null;
                }

                $data = DB::connection('wms_live')->table('preparation_to_cutting_v')->where('planning_date', $cutting_date)->where('deleted_at', null)->where('is_piping', 'f')->whereNotNull('_style')->whereNotNull('article_no')->where('warehouse_id', $factory)->orderBy('_style', 'asc')->orderBy('article_no', 'asc')->orderBy('po_buyer', 'asc')->orderBy('part_no', 'asc')->orderBy('item_code', 'asc');

                return datatables()->of($data)
                ->editColumn('po_buyer', function($data) {
                    $po_array = explode(',', $data->po_buyer);
                    return implode(', ', $po_array);
                })
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }
}
