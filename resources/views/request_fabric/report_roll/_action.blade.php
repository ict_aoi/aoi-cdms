@if (isset($part))
    <a href="#" onclick="partModal('{!! $part !!}')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>
@endif

@if (isset($reduce))
    <a href="#" onclick="reduceModal('{!! $reduce !!}')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>
@endif
@permission('menu-cancel-received-roll')
@if(isset($cancelrcvd))
    <a href="{{ $cancelrcvd }}" data-id="{{ isset($id) ? $id : 'kosong' }}" class="btn btn-default btn-icon bg-danger cancelReceived"><i class="icon-trash"></i></a>
@endif
@endpermission