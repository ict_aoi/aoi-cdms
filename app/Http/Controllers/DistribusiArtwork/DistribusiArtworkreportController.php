<?php

namespace App\Http\Controllers\DistribusiArtwork;

use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use App\Models\DistribusiMovement;
use App\Models\DetailPackinglistDistribusi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DistribusiArtworkreportController extends Controller
{
    public function indexArtworkmovement()
    {
        return view('report.distribusi_artwork.index');
    }

    public function downloadArtworkmovement(Request $request)
    {
        $scan_artwork = explode('-', preg_replace('/\s+/', '', $request->scan_artwork));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $scan_artwork[0].' 06:00:00')->format('Y-m-d H:i:s');
        
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $scan_artwork[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');

        $data = DB::table('dd_daily_artwork_movement_new')
        ->whereBetween('scan_in', [$from, $to])
        ->where('factory_id', \Auth::user()->factory_id)
        ->whereNull('deleted_at')
        ->orderBy('poreference','asc')
        ->get();

        if($data == null)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Artwork_movement_AOI".\Auth::user()->factory_id.'_'.$from." - ".$to;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                            'H' => '@',
                            'I' => '@',
                            'J' => '@',
                            'K' => '@',
                            'L' => '@',
                            'M' => '@',
                            'N' => '@',
                            'O' => '@',
                            'P' => '@',
                            'Q' => '@',
                            'R' => '@',
                            'S' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','PO BUYER');
                        $sheet->setCellValue('C1','BARCODE ID');
                        $sheet->setCellValue('D1','SEASON');
                        $sheet->setCellValue('E1','STYLE');
                        $sheet->setCellValue('F1','SIZE');
                        $sheet->setCellValue('G1','CUT NUMBER');
                        $sheet->setCellValue('H1','STICKER');
                        $sheet->setCellValue('I1','QTY');
                        $sheet->setCellValue('J1','KOMPONEN');
                        $sheet->setCellValue('K1','USER IN');
                        $sheet->setCellValue('L1','SCAN IN');
                        $sheet->setCellValue('M1','USER OUT');
                        $sheet->setCellValue('N1','SCAN OUT');
                        $sheet->setCellValue('O1','PACKINGLIST NO');
                        $sheet->setCellValue('P1','PRINT AT');
                        $sheet->setCellValue('Q1','PRINTED BY');
                        $sheet->setCellValue('R1','SUPPLIER');
                        $sheet->setCellValue('S1','PROSES');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->poreference);
                            $sheet->setCellValue('C'.$rw,"'".$vl->barcode_id);
                            $barcode_cdms = DB::table('bundle_detail as bd')
                            ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                            ->where('bd.barcode_id',$vl->barcode_id)
                            ->first();
                            $barcode_sds = DB::table('sds_detail_movements as sdm')
                                ->leftJoin('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')
                                ->where('sdm.sds_barcode',$vl->barcode_id)
                                ->first();
                            if($barcode_cdms != null){
                                $season = $barcode_cdms->season;
                                $size = $barcode_cdms->size;
                                $cut_num = $barcode_cdms->cut_num;
                            }elseif($barcode_sds != null){
                                $season = $barcode_sds->season;
                                $size   = $barcode_sds->size;
                                $cut_num = $barcode_sds->cut_num;
                            }else{
                                $season = '-';
                                $size   = '-';
                                $cut_num = '-';
                            }
                            $sheet->setCellValue('D'.$rw,$season);
                            if($vl->set_type == 'Non'){
                                $style_set = $vl->style;
                            }else{
                                $style_set = $vl->style.'-'.$vl->set_type;
                            }
                            $sheet->setCellValue('E'.$rw,$style_set);
                            $sheet->setCellValue('F'.$rw,$size);
                            $sheet->setCellValue('G'.$rw,$cut_num);
                            $sheet->setCellValue('H'.$rw,$vl->start_no.'-'.$vl->end_no);
                            $sheet->setCellValue('I'.$rw,$vl->qty);
                            $sheet->setCellValue('J'.$rw,$vl->komponen_name);

                            $user_in = DB::table('users')->where('id',$vl->user_in)->first();
                            $username_in = $user_in == null ? '-' : $user_in->name;
                            
                            $sheet->setCellValue('K'.$rw,strtoupper($username_in));
                            $sheet->setCellValue('L'.$rw,$vl->scan_in);

                            $user_out = DB::table('users')->where('id',$vl->user_out)->first();
                            $username_out = $user_out == null ? '-' : $user_out->name;

                            $sheet->setCellValue('M'.$rw,strtoupper($username_out));
                            $sheet->setCellValue('N'.$rw,$vl->scan_out);
                            $sheet->setCellValue('O'.$rw,$vl->no_packinglist);
                            $sheet->setCellValue('P'.$rw,$vl->print_at);

                            $user_print = DB::table('users')->where('id',$vl->printed_by)->first();
                            $username_print = $user_print == null ? '-' : $user_print->name;

                            $sheet->setCellValue('Q'.$rw,strtoupper($username_print));
                            $sheet->setCellValue('R'.$rw,strtoupper($vl->subcont));
                            $process = DB::table('master_subcont')->where('id',$vl->subcont_id)->first();
                            $sheet->setCellValue('S'.$rw,strtoupper($process->division));
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }

    public function getDataArtworkmovement(Request $request){

        if ($request->ajax()){
            if($request->style != null && $request->no_kk != null){
                $data = DB::table('doc_kk_ppc_header')
                ->where('factory_id',\Auth::user()->factory_id)
                ->where('kk_no','like','%'.$request->no_kk)
                ->where('style','like','%'.$request->style);
            }elseif($request->style != null){
                $data = DB::table('doc_kk_ppc_header')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('style','like','%'.$request->style);
            }else{
                $data = [];
            }
            
            return DataTables::of($data)
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->addColumn('factory_id',function ($data)
            {
                if($data->factory_id == 1){
                    $factory_id = 'AOI 1';
                }else{
                    $factory_id = 'AOI 2';
                }
                return $factory_id;
            })
            ->addColumn('kk_no',function ($data)
            {
                return strtoupper($data->kk_no);
            })
            ->addColumn('kk_type',function ($data)
            {
                return strtoupper($data->kk_type);
            })
            ->addColumn('document_no',function ($data)
            {
                return strtoupper($data->document_no);
            })
            ->addColumn('total_qty',function ($data)
            {
                return number_format($data->total_qty);
            })
            ->addColumn('quota_used',function ($data)
            {
                return number_format($data->quota_used);
            })
            ->addColumn('balanced_quota',function ($data)
            {
                return number_format($data->balanced_quota);
            })
            ->rawColumns(['style','factory_id','kk_no','total_qty','quota_used','balanced_quota'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function getDataMovemendetail(Request $request){

        if ($request->ajax()){
            if($request->po_buyer != null && $request->cut_num != null && $request->packinglist_no){
                $data = DB::table('dd_artwork_movement')
                ->where('factory_id',\Auth::user()->factory_id)
                ->where('poreference','like','%'.$request->po_buyer)
                ->where('cut_num','like','%'.$request->cut_num)
                ->where('no_packinglist','like','%'.$request->packinglist_no);
            }elseif($request->po_buyer != null && $request->cut_num != null){
                $data = DB::table('dd_artwork_movement')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('poreference','like','%'.$request->po_buyer)
                    ->where('cut_num','like','%'.$request->cut_num);
            }elseif($request->packinglist_no != null){
                $data = DB::table('dd_artwork_movement')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('no_packinglist',$request->packinglist_no);
            }elseif($request->po_buyer != null){
                $data = DB::table('dd_artwork_movement')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->where('poreference','like','%'.$request->po_buyer);
            }else{
                $data = [];
            }
            
            return DataTables::of($data)
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->addColumn('factory_id',function ($data)
            {
                if($data->factory_id == 1){
                    $factory_id = 'AOI 1';
                }else{
                    $factory_id = 'AOI 2';
                }
                return $factory_id;
            })
            ->addColumn('sticker',function ($data)
            {
                return $data->start_no.'-'.$data->end_no;
            })
            ->addColumn('current_description',function ($data)
            {
                return strtoupper($data->current_description);
            })
            ->addColumn('current_user_id',function ($data)
            {
                $user_name = DB::table('users')->where('id',$data->current_user_id)->first();
                $user_names = $user_name == null ? '-' : $user_name->name;
                return strtoupper($user_names);
            })
            ->addColumn('no_packinglist',function ($data)
            {
                $no_packinglist = $data->print_at == null ? '-' : $data->no_packinglist;
                return $no_packinglist;
            })
            ->addColumn('print_at',function ($data)
            {
                $print_at = $data->print_at == null ? '-' : Carbon::parse($data->print_at)->format('d-m-Y');
                return $print_at;
            })
            ->addColumn('printed_by',function ($data)
            {
                $user_name = DB::table('users')->where('id',$data->printed_by)->first();
                $user_names = $user_name == null ? '-' : $user_name->name;
                return strtoupper($user_names);
            })
            
            ->rawColumns(['style','factory_id','sticker','current_description','current_user_id','print_at','printed_by'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
}
