@extends('layouts.app',['active' => 'scan_component'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    .control-label {
        padding-top: 9px !important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Scan Component</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Scan Component</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" style="opacity: 1;" id="locator_check" name="locator_check" placeholder="#Scan Barcode/Pilih Locator" readonly="readonly" />
                    <input type="hidden" id="locator_id" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <select name="state" id="state" class="form-control select">
                    </select>
                </div>
                <div class="col-md-4">
                    <input type="text" value="Locator" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_input" name="barcode_input" placeholder="#Scan Barcode Komponen" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Komponen" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">
                <h5>Proses Terbaru</h5>
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <span class="btn btn-warning mr-20" id="reset_count">Reset</span>
                    <span class="btn border-slate text-slate-800 btn-flat legitRipple" id="scan_count">0</span>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Style | Article</th>
                        <th>Part - Komponen</th>
                        <th>PO</th>
                        <th>Size</th>
                        <th>Cut / Sticker No</th>
                        <th>Qty</th>
                        <th>Process</th>
                        <th>Status</th>
                        <th>Loc</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('ScanComponent.scanManualSet') }}" id="scanManualSetLink"></a>
<a href="{{ route('ScanComponent.deleteScanProcSet') }}" id="deleteScanProcSetLink"></a>
@endsection

@section('page-modal')
	@include('component_movement.scan_component._modal_locator')
	@include('component_movement.scan_component._modal_line_set')
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {

            $(".file-styled").uniform({
                fileButtonClass: 'action btn bg-warning'
            });

            $.extend( $.fn.dataTable.defaults, {
                stateSave: true,
                autoWidth: false,
                autoLength: false,
                processing: true,
                // serverSide: true, //sync
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                }
            });

            $('#locator_check').on('click', function() {
                $('#locatorModal').modal();
                get_data_locator();
            });

            $('#locatorTable').on('click', '#select_locator', function() {
                var id_locator = $(this).data('id');
                var locator_name = $(this).data('name');
                var is_cutting = $(this).data('is_cutting');
                var is_setting = $(this).data('is_setting');
                var is_artwork = $(this).data('is_artwork');

                $("#state").html('');

                $("#state").append($('<option>').attr('value', 'in').text('IN')).append($('<option>').attr('value', 'out').text('OUT'));

                if(is_cutting) {
                    $("#state option[value='in']").remove();
                }

                if(is_setting) {
                    $("#state option[value='out']").remove();
                }

                $('#locator_check').val(locator_name);
                $('#locator_id').val(id_locator);
                $('#barcode_input').focus();

                $('#locatorModal').modal('hide');
            })

            $('#locatorModal').on('hidden.bs.modal', function(){
                // $('#marker_check').focus();  
            });
            
            $('#lineSetModal').on('hidden.bs.modal', function(){
                var barcode_input = $('#barcode_id_set_locator').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: $('#deleteScanProcSetLink').attr('href'),
                    data: {
                        barcode_input: barcode_input,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                    },
                    success: function(response) {
                        $.unblockUI();
                        $('#barcode_id_set_locator').val('');
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('#barcode_input').val('').focus();
                    }
                });
            });

            $('#barcode_input').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var barcode_input = $(this).val();
                    var locator_id = $('#locator_id').val();
                    var state = $('#state').val();

                    validationInputBarcode(barcode_input, locator_id, state, function() {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
    
                        $.ajax({
                            type: 'post',
                            url: '/scan-component/scan-component',
                            data: {
                                barcode_input: barcode_input,
                                locator_id: locator_id,
                                state: state,
                            },
                            beforeSend: function() {
                                $.blockUI({
                                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait',
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent',
                                    },
                                    theme: true,
                                    baseZ: 2000
                                });
                            },
                            success: function(response) {
                                $.unblockUI();
                                $('#barcode_input').val('').focus();
                                appendBarcode(response);
                            },
                            error: function(response) {
                                $.unblockUI();
                                if (response.status == 331) {
                                    $("#lineSetModal").modal();
                                    $('#barcode_id_set_locator').val(response.responseJSON);
                                }
                                if (response.status == 422) {
                                    $("#alert_warning").trigger("click",response.responseJSON);
                                }
                                $('#barcode_input').val('').focus();
                            }
                        });
                    });
                }
            });

            $('#reset_count').on('click', function() {
                $('#lastActivityTable > tbody').html('');
                $('#scan_count').text(0);
            });

            $('#lineSetModal').on('click', '.select-locator-set', function() {
                var id = $(this).data('id');
                var barcode_id = $('#barcode_id_set_locator').val();
                var area_name = $(this).data('area_name');

                if(!id) {
                    $("#alert_warning").trigger("click", "terjadi kesalahan saat parsing data, silakan reload halaman!");
                }

                if(!barcode_id) {
                    $("#alert_warning").trigger("click", "terjadi kesalahan saat parsing data, silakan reload halaman!");
                }
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: $('#scanManualSetLink').attr('href'),
                    data: {
                        id: id,
                        barcode_id: barcode_id,
                        area_name: area_name,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                    },
                    success: function(response) {
                        $.unblockUI();
                        $('#lineSetModal').modal('hide');
                        $('#barcode_input').val('').focus();
                        appendBarcode(response);
                    },
                    error: function(response) {
                        $.unblockUI();
                        $('#lineSetModal').modal('hide');
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('#barcode_input').val('').focus();
                    }
                });
            })
        });

        function get_data_locator() {       
            var table = $('#locatorTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: '/scan-component/data-locator',
                },
                fnCreatedRow: function (row, data, index) {
                    var info = table.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'locator_name', name: 'locator_name', searchable:true, visible:true, orderable:false},
                    {data: 'action', name: 'action', searchable:false, orderable:false},
                ],
            });

            $('#tableTable').on('click','.ignore-click', function() {
                return false;
            });
        }

        function appendBarcode(data) {
            var scanCount = parseInt($('#scan_count').text()) + 1;
            $('#scan_count').text(scanCount);
            
            $('#lastActivityTable > tbody').prepend(
                $('<tr>').append(
                    $('<td>').text(data.barcode_id)
                ).append(
                    $('<td>').text(data.style+' | '+data.article)
                ).append(
                    $('<td>').text(data.part+' - '+data.komponen_name)
                ).append(
                    $('<td>').text(data.po_buyer)
                )
                .append(
                    $('<td>').text(data.size)
                ).append(
                    $('<td>').text(data.cut+' / '+data.sticker_no)
                ).append(
                    $('<td>').text(data.qty)
                ).append(
                    $('<td>').text(data.process)
                ).append(
                    $('<td>').text(data.status)
                ).append(
                    $('<td>').text(data.loc_dist)
                )
            )
        }

        function validationInputBarcode(barcode_input, locator_id, state, callbackFucntion) {
            if(!locator_id) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            if(!state) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            callbackFucntion(barcode_input, locator_id, state);
        }

    </script>
@endsection