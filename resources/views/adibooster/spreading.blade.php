@extends('adibooster.index')
@section('page-css')
<style type="text/css">

.title-fac{
	color: black;
	font-family:Cooper Black;
	font-size:  40px;
}

</style>
@endsection
@section('page-header')
<div class="page-header" style="height: 60px;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1px; padding-bottom: 5px;">
        	<center><label class="title-fac">DASHBOARD SPREADING AOI {{$factory_id}}</label></center>
        	<input type="text" name="factory_id" id="factory_id" class="hidden" value="{{$factory_id}}">
        </div>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
		    <div class="panel-body" >
		        <div id="chart" style="width:100%; height:400px;"></div>
		    </div>
		</div>
@endsection

@section('page-js')
<script type="text/javascript">
Highcharts.chart('chart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'SPREADING TABLE'
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Yards'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "spreading",
            colorByPoint: true,
            data:[
            	<?php foreach ($dash as $key) {
            		echo $key;
            	} ?>
            ]
        }
    ]
});

$(document).ready(function(){
     var factory_id = $('#factory_id').val();     	
     setInterval(function(){window.location.href="/adibooster/preparation/"+factory_id },300000);

});


</script>
@endsection