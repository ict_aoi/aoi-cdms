<?php

namespace App\Http\Controllers\Api;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\Spreading\RemarkFabric;
use App\Models\CuttingMarker;

class ReportRollController extends Controller
{
    public function index(Request $request) {
        $cutting_date = $request->cutting_date;
        if($cutting_date != NULL) {
            $data = array();
            $data_cutting_plan = CuttingPlan::where('cutting_date', $cutting_date)->where('deleted_at', null)->orderBy('queu', 'asc')->get();
            foreach($data_cutting_plan as $a) {
                $partial_po = DetailPlan::where('id_plan', $a->id)->pluck('po_buyer')->toArray();
                
                $partial_po_view = implode(', ',$partial_po);

                $csi_qty_data = DB::table('jaz_detail_per_material')->select(DB::raw("material, color_name, part_no, sum(csi_qty) as csi_qty, uom"))->where('cutting_date', $cutting_date)->where('style', 'like', '%'.$a->style.'%')->where('articleno', $a->articleno)->whereIn('po_buyer', $partial_po)->groupBy('material', 'color_name', 'part_no', 'uom')->get();

                foreach($csi_qty_data as $b) {

                    if($b->uom == 'M') {
                        $csi_qty = $b->csi_qty * 1.09161;
                    } else {
                        $csi_qty = $b->csi_qty;
                    }

                    $ssp_qty = CuttingMarker::where('id_plan', $a->id);
                    $ssp_normal = $ssp_qty->where('part_no', $b->part_no)->sum('need_total');
                    if($ssp_normal == 0) {
                        $ssp_normal = $ssp_qty->where('part_no', 'like', $b->part_no)->sum('need_total');
                    }

                    $already_prepared = DB::connection('wms_live')->table('integration_whs_to_cutting')->where('planning_date', $cutting_date)->where('style', $a->style)->where('article_no', $a->articleno)->whereIn('po_buyer', $partial_po)->where('item_code', $b->material)->sum('qty_prepared');

                    $over_consum = round($csi_qty, 2) - round($ssp_normal, 2);
                    $balance_whs = round($already_prepared, 2) - round($csi_qty, 2);
                    $total_balance = $balance_whs + $over_consum;
                    
                    if(round($ssp_normal, 2) != 0) {
                        $total_balance = $total_balance;
                    } else {
                        $total_balance = $balance_whs;
                    }

                    $remark_check = RemarkFabric::where('id_plan', $a->id)->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('material', $b->material)->where('part_no', $b->part_no)->get()->first();

                    $remark = '';
                    $status = '';

                    if($remark_check != null) {
                        $remark = $remark_check->remark;
                        $status = $remark_check->status;
                    }

                    $data[] = [
                        'queu' => $a->queu,
                        'id_plan' => $a->id,
                        'cutting_date' => $a->cutting_date,
                        'style' => $a->style,
                        'articleno' => $a->articleno,
                        'po_buyer' => $partial_po_view,
                        'material' => $b->material,
                        'color' => $b->color_name,
                        'part_no' => $b->part_no,
                        'csi_qty' => round($csi_qty, 2),
                        'ssp_qty' => round($ssp_normal, 2),
                        'already_prepared' => round($already_prepared, 2),
                        'over_consum' => $over_consum,
                        'balance_whs' => $balance_whs,
                        'total_balance' => $total_balance,
                        'remark' => $remark,
                        'status' => $status,
                        'params' => $a->id.','.$a->cutting_date.','.$a->articleno.','.$b->material.','.$b->part_no.','.$a->style,
                    ];
                }
            }
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        } else {
            $data = null;
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        }
    }

    public function updateRemarkMaterial(Request $request) {
        $cutting_date = $request->cutting_date;
        $id_plan = $request->id_plan;
        $style = $request->style;
        $articleno = $request->articleno;
        $material = $request->material;
        $part_no = $request->part_no;
        $remark = $request->remark;

        $data = array();

        if($cutting_date != NULL && $id_plan != null && $style != null && $articleno != null && $material != null && $remark != null) {
            $remark_check = RemarkFabric::where('id_plan', $id_plan)->where('cutting_date', $cutting_date)->where('style', $style)->where('articleno', $articleno)->where('material', $material)->where('part_no', $part_no)->get()->first();

            if($remark_check != null) {
                $data = $remark_check->update(['remark' => $remark]);
            } else {
                $data = RemarkFabric::FirstOrCreate([
                    'id_plan' => $id_plan,
                    'cutting_date' => $cutting_date,
                    'style' => $style,
                    'articleno' => $articleno,
                    'material' => $material,
                    'color' => '-',
                    'part_no' => $part_no,
                    'remark' => $remark,
                ]);
            }
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        } else {
            $data = null;
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        }
    }

    public function updateStatusMaterial(Request $request) {
        $cutting_date = $request->cutting_date;
        $id_plan = $request->id_plan;
        $style = $request->style;
        $articleno = $request->articleno;
        $material = $request->material;
        $part_no = $request->part_no;
        $status = $request->status;

        $data = array();

        if($cutting_date != NULL && $id_plan != null && $style != null && $articleno != null && $material != null && $status != null) {
            $status_check = RemarkFabric::where('id_plan', $id_plan)->where('cutting_date', $cutting_date)->where('style', $style)->where('articleno', $articleno)->where('material', $material)->where('part_no', $part_no)->get()->first();

            if($status_check != null) {
                $data = $status_check->update(['status' => $status]);
            } else {
                $data = RemarkFabric::FirstOrCreate([
                    'id_plan' => $id_plan,
                    'cutting_date' => $cutting_date,
                    'style' => $style,
                    'articleno' => $articleno,
                    'material' => $material,
                    'color' => '-',
                    'part_no' => $part_no,
                    'remark' => null,
                    'status' => $status,
                ]);
            }
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        } else {
            $data = null;
            return response()->json($data,200)
                            ->header('Access-Control-Allow-Origin',  '*')
                            ->header('Access-Control-Allow-Methods',  'GET, POST, PUT, DELETE, OPTIONS')
                            ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin, Authorization');
        }
    }
}
