<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingPlan2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutting_plan2', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->date('cutting_date');
            $table->string('style');
            $table->string('articleno');
            $table->string('size_category', 50)->nullable();
            $table->string('queu', 20)->nullable();
            $table->string('remark', 20)->nullable();
            $table->integer('user_id')->nullable();
            $table->string('color')->nullable();
            $table->string('material')->nullable();
            $table->string('color_code')->nullable();
            $table->timestamp('mo_created')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutting_plan2');
    }
}
