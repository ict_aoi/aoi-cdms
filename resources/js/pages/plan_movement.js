$(document).ready( function () {
		
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            url: '/plan-movement/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po', name: 'po',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'queu', name: 'queu',searchable:true,orderable:true},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:false,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/plan-movement/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });
    
    var dtable = $('#planningTable').dataTable().api();

    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();

    $('#btn-movement').on('click', function(event) {
        event.preventDefault();
        var movement_date = $('#movement_date').val();
        var id_plan = $('#movement_id_plan').val();
        var movement_queu = $('#movement_queu').val();

        if(!movement_date){
            $("#alert_warning").trigger("click", 'Tanggal Geser Planning Wajib Diisi!');
            return false;
        }

        if(!movement_queu){
            $("#alert_warning").trigger("click", 'Queue Geser Planning Wajib Diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/plan-movement/move-plan',
            data: {
                movement_date: movement_date,
                id_plan: id_plan,
                movement_queu: movement_queu,
            },
            beforeSend: function() {
                $('#detailModal').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#detailModal').unblock();
                if (response.status == 'success') {
                    $("#alert_success").trigger("click",response.message);
                    $('#detailModal').modal('hide');
                }
                if (response.status == 'failed') $("#alert_warning").trigger("click",response.message);
            },
            error: function(response) {
                $('#detailModal').unblock();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $('#movement_date').val('');
        $('#movement_queu').val('');
    });

    // Hapus Plan
    $('#btn-delete').on('click', function() {
        var id_plan = $('#movement_id_plan').val();

        $('#detailModal').modal('hide');
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        
                $.ajax({
                    type: 'post',
                    url : '/plan-movement/delete-plan',
                    data: {
                        id_plan: id_plan,
                    },
                    beforeSend: function() {
                        $('#detailModal').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#detailModal').unblock();
                        if (response.status == 'success') {
                            $("#alert_success").trigger("click",response.message);
                            $('#detailModal').modal('hide');
                            $('#form_filter_date').submit();
                        }
                        if (response.status == 'failed') $("#alert_warning").trigger("click",response.message);
                    },
                    error: function(response) {
                        $('#detailModal').unblock();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            } else {
                $('#detailModal').modal();
            }
        });

    });
});

function detail(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        }
    })
    .done(function (response) {
        console.log(response);
        $('#cutting_date_detail_view').text(response.cutting_date);
        $('#style_detail_view').text(response.style);
        $('#articleno_detail_view').text(response.articleno);
        $('#size_category_detail_view').text(response.size_category);
        $('#po_buyer_detail_view').text(response.po_view);
        $('#movement_id_plan').val(response.id);
        $('#detailModal').modal();
    });
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

