@extends('layouts.app',['active' => 'bundle_removal'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }

    .change-bg-header {
        background-color: aqua;
    }

    .slc{
        background-color:#abf6f7 !important;
    }
    .slc2{
        /* background-color:#abf6f7 !important; */
        height: 44px !important;
    }
    .switch {
    position: relative;
    display: inline-block;
    width: 45px;
    height: 24px;
    }

    .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
    }

    .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
    }

    .slider:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    left: 2px;
    right:0px;
    bottom: 2px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
    }

    input:checked + .slider {
    background-color: #2196F3;
    }

    input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
    -webkit-transform: translateX(20px);
    -ms-transform: translateX(20px);
    transform: translateX(20px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }
    .lb_tk{
        margin:2px;
        padding:2px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .border_arround{
        border:1px solid !important;
        border-color: #96A5BA !important;
        border-radius: 10px;
        height:37px;
    }
    .mrt{
        margin-top:0px !important;
        margin-right:0px !important;
    }
    .f_col{
        color:white !important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ticket Bundle Removal</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Ticket Bundle Removal</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat all_content">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class=""><a href="#basic-tab1" data-toggle="tab" class="legitRipple tab_text1" aria-expanded="true">CDMS</a></li>
                <li class="active"><a href="#basic-tab2" data-toggle="tab" class="legitRipple tab_text2" aria-expanded="true">SDS</a></li>
			</ul>
			<div class="tab-content">
                <div class="tab-pane" id="basic-tab1">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <div id="scan1" class="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_marker" name="barcode_marker" placeholder="MK-00000000000" autofocus autocomplete="off" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly">
                                    </div>
                                    @csrf
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-danger btn-icon icon-trash" id="delete_ticket"> DELETE</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="panel-heading">
                                <h5>MARKER -## <span id="detail_view_barcode_marker">-</span></h5>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed" id="headerTable">
                                    <tbody>
                                        <tr>
                                            <td><strong>Style</strong></td>
                                            <td><span id="detail_view_style">Data Empty!</span></td>
                                            <td><strong>Cut</strong></td>
                                            <td><span id="detail_view_cut">Data Empty!</span></td>
                                            <td><strong>Item</strong></td>
                                            <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Deliv</strong></td>
                                            <td><span id="detail_view_deliv">Data Empty!</span></td>
                                            <td><strong>Marker Width (inch)</strong></td>
                                            <td><span id="detail_view_width">Data Empty!</span></td>
                                            <td rowspan="2"><strong>Color</strong></td>
                                            <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Article</strong></td>
                                            <td><span id="detail_view_article">Data Empty!</span></td>
                                            <td><strong>Marker Length (inch)</strong></td>
                                            <td><span id="detail_view_length">Data Empty!</span></td>
                                            <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Marker Pcs</strong></td>
                                            <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                                            <td><strong>Total Length (+allow)</strong></td>
                                            <td><span id="detail_view_allow">Data Empty!</span></td>
                                            <td><strong>Part</strong></td>
                                            <td><span id="detail_view_part">Data Empty!</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Total Grmt</strong></td>
                                            <td><span id="detail_view_total_garment">Data Empty!</span></td>
                                            <td><strong>Layer</strong></td>
                                            <td><span id="detail_view_layer">Data Empty!</span></td>
                                            <td><strong>Plan</strong></td>
                                            <td><span id="detail_view_plan">Data Empty!</span></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Po Buyer</strong></td>
                                            <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                                            <td><strong>Ratio</strong></td>
                                            <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-basic table-condensed" id="table-list">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Barcode Marker</th>
                                                <th>Style</th>
                                                <th>Size</th>
                                                <th>CDMS Barcode</th>
                                                <th>SDS Barcode</th>
                                                <th>Komponen</th>
                                                <th>Sticker</th>
                                                <th>Qty</th>
                                                <th>Last Locator</th>
                                                <th>Admin Cutting</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane active" id="basic-tab2">
                    <form class="form-horizontal" action="{{route('bundle_removal.addParameter')}}" id="data_parameter" method="POST" enctype="multipart/form-data">
                        <fieldset class="content-group">
                            <div class="alert bg-info alert-styled-left" style="margin-bottom:10px">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                <span class="text-semibold">MODULE INI DIGUNAKAN UNTUK MENGHAPUS DETAIL TICKET SDS!</span>
                            </div>
                            @csrf
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <select class="form-control select-search" id="poreference" name="poreference" required>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control select-search" id="cut_num" name="cut_num">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control select-search" id="article" name="article">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control select-search" id="style_num" name="style_num">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <select class="form-control select-search" id="part_name" name="part_name">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control select-search" id="size" name="size[]" multiple>
                                            <option value=""></option>
                                        </select>
                                        <span class="help-block"><b>PILIH SIZE</b></span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control bg-grey-300 text-center" name="start_num" id="start_num">
                                        <span class="help-block"><b>STICKER AWAL</b></span>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control bg-grey-300 text-center" name="end_num"  id="end_num">
                                        <span class="help-block"><b>STICKER AKHIR</b></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="padding-top:10px">
                                        <button type="submit" id="btn-add" style="margin-left:10px" class="btn btn-info pull-right margin-kanan">ADD <i class="icon-plus2 position-right"></i></button>
                                        <button type="button" id="cancel-detail" style="margin-left:10px" class="btn btn-warning pull-right margin-kanan">Cancel <i class="icon-trash position-right"></i></button>
                                        <button type="button" id="delete-sds" class="btn btn-danger pull-right margin-kanan">Delete Ticket <i class="icon-database-remove position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed table-hover" id="table_removal">
                                    <thead class="bg-grey-700">
                                        <tr>
                                            <th>NO</th>
                                            <th>Barcode</th>
                                            <th>Season</th>
                                            <th>Style</th>
                                            <th>PO &nbsp;&nbsp; Buyer</th>
                                            <th>Article</th>
                                            <th>Cut | Sticker</th>
                                            <th>Size</th>
                                            <th>PART | Komponen</th>
                                            <th>Qty</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('bundle_removal.scanBarcode') }}" id="scanBarcodeLink"></a>
<a href="{{ route('bundle_removal.dataTicket') }}" id="scanBarcodeLink2"></a>
<a href="{{ route('bundle_removal.ajaxGetDataPobuyer') }}" id="get_data_po"></a>
<a href="{{ route('bundle_removal.ajaxGetDataSize') }}" id="get_data_size"></a>
@endsection

@section('page-js')
<script>
$(document).ready( function () {
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    $('.input-new').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();
            var barcode_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url: $('#scanBarcodeLink').attr('href'),
                data: {
                    barcode_id: barcode_id,
                },
                beforeSend: function() {
                    $('.panel').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('.panel').unblock();
                    $('#detail_view_barcode_marker').text(response.data.barcode_id);
                    $('#detail_view_style').text(response.style);
                    $('#detail_view_cut').text(response.data.cut);
                    $('#detail_view_layer').text(response.layer);
                    $('#detail_view_length').text(response.data.marker_length);
                    $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                    $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                    $('#detail_view_part').text(response.data.part_no);
                    $('#detail_view_plan').text(response.plan);
                    $('#detail_view_color_name').text(response.color_name);
                    $('#detail_view_color_code').text(response.color_code);
                    $('#detail_view_item').text(response.item_code);
                    $('#detail_view_deliv').text(response.stat_date);
                    $('#detail_view_width').text(response.marker_width);
                    $('#detail_view_article').text(response.article);
                    $('#detail_view_marker_pcs').text(response.ratio);
                    $('#detail_view_total_garment').text(response.total_garment);
                    $('#detail_view_ratio_size').text(response.ratio_size);
                    $('#detail_view_po_buyer').text(response.po_buyer);
                    $('#spreading_status').text(response.spreading_status);
                    $('#spreading_date').text(response.spreading_date);
                    $('#spreading_by').text(response.spreading_by);
                    $('#spreading_table').text(response.spreading_table);
                    $('#approve_gl_status').text(response.approve_gl_status);
                    $('#approve_gl_date').text(response.approve_gl_date);
                    $('#approve_gl_by').text(response.approve_gl_by);
                    $('#approve_qc_status').text(response.approve_qc_status);
                    $('#approve_qc_date').text(response.approve_qc_date);
                    $('#approve_qc_by').text(response.approve_qc_by);
                    $('#cutting_status').text(response.cutting_status);
                    $('#cutting_date').text(response.cutting_date);
                    $('#cutting_by').text(response.cutting_by);
                    $('#cutting_table').text(response.cutting_table);
                    $('#bundling_status').text(response.bundling_status);
                    $('#bundling_date').text(response.bundling_date);
                    $('#bundling_by').text(response.bundling_by);
                    $('#bundling_table').text(response.bundling_table);
                    $('#barcode_marker').val('');
                    $('#barcode_marker').focus();
                },
                error: function(response) {
                    $('.panel').unblock();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }
    });

    $('.input-new').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();
            var barcode_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var barcode_marker = $('#barcode_marker').val();
            var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            scrollX:true,
            destroy: true,
            searching: false,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: $('#scanBarcodeLink2').attr('href'),
                data: {
                    barcode_marker: barcode_marker,
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},//0
                {data: 'barcode_marker', name: 'barcode_marker',orderable:true},//1
                {data: 'style', name: 'style',orderable:true},//2
                {data: 'size', name: 'size',orderable:true},//3
                {data: 'barcode_id', name: 'barcode_id',orderable:true},//4
                {data: 'sds_barcode', name: 'sds_barcode',orderable:true},//5
                {data: 'komponen_name', name: 'komponen_name',orderable:true},//6
                {data: 'sticker', name: 'sticker',orderable:true},//7
                {data: 'qty', name: 'qty',orderable:true},//8
                {data: 'current_location', name: 'current_location',orderable:true},//9
                {data: 'admin_name', name: 'admin_name',orderable:true},//10
            ]
            });
            tableL.draw();
        }
    });

    $('#delete_ticket').on('click',function(){
        var barcode_marker = $('#detail_view_barcode_marker').text();
        console.log(barcode_marker);
        if(barcode_marker == null){
            $("#alert_warning").trigger("click","Masukkan Barcode Marker Dahulu!");
        }else{
            bootbox.confirm("Apakah Anda Yakin Menghapus Semua Transaksi Bundle ini?", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: "{{ route('bundle_removal.deleteTicket') }}",
                    data: {
                        barcode_marker: barcode_marker,
                    },
                    beforeSend: function() {
                        $('.panel').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function (response) {
                        $('.panel').unblock();
                        $("#alert_success").trigger("click",'TICKET BUNDLE DI HAPUS!');
                        location.reload(true);
                    },
                    error: function(response) {
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        location.reload();
                    }
                });
            }
            else{
                $('.modal').on('hidden.bs.modal', function (e) {
                if($('.modal').hasClass('in')) {
                    $('body').addClass('modal-open');
                }
                });
            }
            });
        }
    });

    $('#poreference').select2({
        placeholder: "Pilih PO Buyer",
        minimumInputLength: 5,
        allowClear:true,
        ajax: {
            url: $('#get_data_po').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $('#style_num').select2({
        placeholder: "Pilih Style",
        allowClear:true,
    });

    $('#cut_num').select2({
        placeholder: "Pilih CutNum",
        allowClear:true,
    });

    $('#article').select2({
        placeholder: "Pilih Article",
        allowClear:true,
    });

    $('#part_name').select2({
        placeholder: "Pilih Komponen",
        allowClear:true,
    });
    
    $('.select2-selection--single').addClass('bg-grey-300');
    $('.select2-selection--multiple').addClass('border_arround');

    //SIZE SELECT
    $('#cut_num').on('change', function (event){  
        var poreference = $("#poreference").val();
        var cut_num  = $("#cut_num").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url :  "{{route('bundle_removal.ajaxGetDataSize')}}",
            data : {
                poreference:poreference,
                cut_num:cut_num
            },
            success: function(response) {
                $('.select2-selection--multiple').addClass('bg-grey-300');
                $('.select2-selection--multiple').removeClass('border_arround');
                var data = response.response;
                $('#size').empty();
                $('#size').append('<option></option>');
                for (var i = 0; i < data.length; i++) {
                $('#size').append('<option value="'+data[i]['size']+'">'+data[i]['size']+'</option>')
                }
            },
            error: function(response) {
                $.unblockUI();
                alert(response.status,response.responseText);
            }
        });
    });

    //STYLE SELECT
    $('#poreference').on('change', function (event){  
        var poreference = $("#poreference").val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'get',
			url :  "{{route('bundle_removal.ajaxGetDataStyle')}}",
			data : {
				poreference:poreference,
			},
			success: function(response) {
				var data = response.response;
				$('#style_num').empty();
				$('#style_num').append('<option></option>');
				for (var i = 0; i < data.length; i++) {
                    var set_type = data[i]['set_type'] == '0' ? '' : '-'+data[i]['set_type'];
                    $('#style_num').append('<option value="'+data[i]['style']+set_type+'">'+data[i]['style']+set_type+'</option>')
				}
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
			}
		});
    });

    //ARTICLE SELECT
    $('#poreference').on('change', function (event){  
        var poreference = $("#poreference").val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'get',
			url :  "{{route('bundle_removal.ajaxGetDataArticle')}}",
			data : {
				poreference:poreference,
			},
			success: function(response) {
				var data = response.response;
				$('#article').empty();
				$('#article').append('<option></option>');
				for (var i = 0; i < data.length; i++) {
                    $('#article').append('<option value="'+data[i]['article']+'">'+data[i]['article']+'</option>')
				}
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
			}
		});
    });

    //CUT NUM SELECT
    $('#poreference').on('change', function (event){  
        var poreference = $("#poreference").val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'get',
			url :  "{{route('bundle_removal.ajaxGetDataCut')}}",
			data : {
				poreference:poreference,
			},
			success: function(response) {
				var data = response.response;
				$('#cut_num').empty();
				$('#cut_num').append('<option></option>');
				for (var i = 0; i < data.length; i++) {
                    $('#cut_num').append('<option value="'+data[i]['cut_num']+'">'+data[i]['cut_num']+'</option>')
				}
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
			}
		});
    });

    $('#poreference').on('change', function (event){  
        var poreference = $("#poreference").val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: 'get',
			url :  "{{route('bundle_removal.ajaxGetDataPartname')}}",
			data : {
				poreference:poreference,
			},
			success: function(response) {
				var data = response.response;
				$('#part_name').empty();
				$('#part_name').append('<option></option>');
				for (var i = 0; i < data.length; i++) {
                    $('#part_name').append('<option value="'+data[i]['part_name']+'">'+data[i]['part_name']+'</option>')
				}
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
			}
		});
    });

    var url = "{{route('bundle_removal.getDataTicket')}}";
    var tableL = $('#table_removal').DataTable({
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
                return $.extend({}, d, {
                    "user_id"		: "{{Auth::user()->id}}"
                });
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//0
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:false},//1
            {data: 'style', name: 'style',searchable:false,visible:false,orderable:true},//2
            {data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//3
            {data: 'article', name: 'article',searchable:false,visible:true,orderable:false},//4
            {data: 'cut_num', name: 'cut_num',searchable:true,visible:true,orderable:true},//7
            {data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//8
            {data: 'part_name', name: 'part_name',searchable:true,visible:true,orderable:true},//9
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},//10
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},//10
        ]
    });

    var tableL = $('#table_removal').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                tableL.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                tableL.search("").draw();
            }
            return;
    });
    tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#data_parameter').submit(function(event) {
        var poreference     = $('#poreference').val();
        var cut_num         = $('#cut_num').val();
        var style_num       = $('#style_num').val();
        var size            = $('#size').val();

        if(size == '' || size == null){
            myalert('error','Size Tidak Boleh Kosong!')
            return false;
        }

        if(poreference == '' || poreference == null){
            myalert('error','PO Buyer Tidak Boleh Kosong!')
            return false;
        }
        if(cut_num == '' || cut_num == null){
            myalert('error','Cutting Number Tidak Boleh Kosong!')
            return false;
        }
        if(style_num == '' || style_num == null){
            myalert('error','Style Tidak Boleh Kosong!')
            return false;
        }
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#data_parameter').attr('action'),
            data: $('#data_parameter').serialize(),
            beforeSend: function () {
                $('#data_parameter').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing...</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function () {
                $('#data_parameter').unblock();
            },
            success: function(response) {
                $('#data_parameter').unblock();
                tableL.draw();
            },
            error: function(response) {
                $('#data_parameter').unblock();
                if(response['status'] == 422) {
                    myalert('error', response['responseJSON']);
                }
            }
        })
    });

    $('#cancel-detail').on('click',function(){
        var user = "{{Auth::user()->id}}";
        bootbox.confirm("Apakah Anda Yakin Membatalkan Transaksi Ini?", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'get',
                    url :  "{{route('bundle_removal.cancelDelete')}}",
                    data : {
                        user:user,
                    },
                    beforeSend: function() {
                        $('#table_removal').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#table_removal').unblock();
                        myalert('success','DETAIL TICKET DELETED');
                        $('#table_removal').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        $('#table_removal').unblock();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });

    $('#delete-sds').on('click',function(){
        var user = "{{Auth::user()->id}}";
        bootbox.confirm("Apakah Anda Yakin Menghapus Ticket Bundle Ini?", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'get',
                    url :  "{{route('bundle_removal.deleteTicketSds')}}",
                    data : {
                        user:user,
                    },
                    beforeSend: function() {
                        $('#table_removal').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('#table_removal').unblock();
                        myalert('success','DETAIL TICKET DELETED');
                        $('#table_removal').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        $('#table_removal').unblock();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });

    $("#table_removal").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this detail ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "bundle-removal/delete-rowdetail/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $("#table_removal").block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $("#table_removal").unblock();
                    },
                    success: function () {
                        myalert('success','Deleted Successfully');
                        tableL.draw();
                    },
                    error: function() {
                        myalert('error','Cannot Delete This Size');
                    }
                });
            }
        });
    });
});
</script>
@endsection
