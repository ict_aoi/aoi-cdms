<?php

namespace App\Http\Controllers\Subcont;

use DB;
use Carbon\Carbon;
use App\Models\DetailPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;

class PackinglistController extends Controller
{
    public function index()
    {
        $po_buyers = DB::table('detail_cutting_plan')
                        ->join('cutting_plans2', 'cutting_plans2.id', '=', 'detail_cutting_plan.id_plan')
                        ->where('cutting_plans2.factory_id', \Auth::user()->factory_id)
                        ->whereNull('cutting_plans2.deleted_at')
                        ->where(function ($query) {
                            $query->whereNull('detail_cutting_plan.statistical_date')
                            ->orWhere('detail_cutting_plan.statistical_date', '>', Carbon::now()->format('Y-m-d H:i:s'));
                        })->get();
        $subconts = DB::table('master_factory')->whereNotIn('id', array(\Auth::user()->factory_id))->where('id', '>', 0)->whereNull('deleted_at')->get();
        $locators = DB::table('master_locator')->whereNotIn('id',array(1,10,11,13,16,15))->whereNull('deleted_at')->get();
        return view('subcont.packinglist.index', compact('subconts', 'po_buyers', 'locators'));
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $data = DB::table('packinglist_subcont')->select('packinglist_subcont.*', 'master_locator.locator_name')->leftJoin('master_locator', 'packinglist_subcont.process', '=', 'master_locator.id')->where('packinglist_subcont.factory_id', \Auth::user()->factory_id)->whereNull('packinglist_subcont.deleted_at')->orderby('packinglist_subcont.created_at','desc');
            return datatables()->of($data)
            ->addColumn('status',function ($data){
                return strtoupper($data->status);
            })
            ->addColumn('action', function($data) {
                return view('subcont.packinglist._action', [
                    'model' => $data,
                    'edit' => route('packinglistSubcont.edit', $data->no_packinglist),
                    'delete' => route('packinglistSubcont.delete', $data->no_packinglist),
                    'print' => route('packinglistSubcont.print', $data->no_packinglist),
                    'release' => route('packinglistSubcont.release', $data->no_packinglist),

                ]);
                return 'action';
            })
            ->rawColumns(['action','status'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        if(request()->ajax()) 
        {
            $no_kk = $request->no_kk_insert;
            $subcont = $request->subcont_insert;
            $no_packinglist = $request->no_packinglist_insert;
            $po_buyer = $request->po_buyer_insert;
            $locator = $request->locator_insert;
            $remark = $request->remark_insert;

            $subcont_detail = DB::table('master_factory')->where('id', $subcont)->first();

            $insert_packinglist = DB::table('packinglist_subcont')->insert([
                'no_packinglist' => $no_packinglist,
                'no_kk' => $no_kk,
                'po_buyer' => implode(',', $po_buyer),
                'subcont' => $subcont_detail->factory_name,
                'subcont_id' => $subcont,
                'user_id' => \Auth::user()->id,
                'factory_id' => \Auth::user()->factory_id,
                'status' => 'draft',
                'process' => $locator,
                'remark' => $remark,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            return response()->json(200);
        }
    }

    public function getNoPackinglist(Request $request)
    {
        if(request()->ajax()) 
        {
            $subcont = $request->subcont;

            $get_subcont = DB::table('master_factory')->where('id', $subcont)->first();
            $get_last_number = DB::table('packinglist_subcont')->where('no_packinglist', 'like', '%'.$get_subcont->factory_alias.\Auth::user()->factory_id.'%')->orderBy('no_packinglist', 'desc')->first();
            if($get_last_number == null) {
                $no_packinglist = $get_subcont->factory_alias.\Auth::user()->factory_id.'00000001';
            } else {
                $no_packinglist = (int) str_replace($get_subcont->factory_alias.\Auth::user()->factory_id,'',$get_last_number->no_packinglist) + 1;
                $no_packinglist = $get_subcont->factory_alias.\Auth::user()->factory_id.sprintf("%08d", $no_packinglist);
            }

            return response()->json($no_packinglist,200);
        }
    }

    public function delete(Request $request, $id)
    {
        if(request()->ajax()) 
        {
            DB::table('packinglist_subcont')
            ->where('no_packinglist', $id)
            ->update([
                'deleted_at' => Carbon::now(),
                'deleted_by' => \Auth::user()->id
            ]);

            return response()->json(200);
        }
    }

    public function print($id)
    {
        $filename ='packinglist_'.$id;
        $packinglist = DB::table('packinglist_subcont')->where('no_packinglist', $id)->first();
        $subcont = DB::table('master_factory')->where('id', $packinglist->subcont_id)->first();
        $factory = DB::table('master_factory')->where('id', $packinglist->factory_id)->first();

        $size = DB::table('detail_packinglist_subcont')->select('bundle_header.size')->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('bundle_header.size')->orderByRaw('LENGTH(bundle_header.size) asc')->orderBy('bundle_header.size', 'asc')->pluck('bundle_header.size')->toArray();

        $data_details = DB::table('detail_packinglist_subcont')->select(DB::raw('detail_packinglist_subcont.id_packinglist, bundle_header.style, max(detail_packinglist_subcont.created_at) as created_at, bundle_header.poreference, bundle_header.article, bundle_detail.komponen_name, detail_packinglist_subcont.no_polybag'))->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('bundle_header.style', 'bundle_header.poreference', 'bundle_header.article', 'bundle_detail.komponen_name', 'detail_packinglist_subcont.id_packinglist', 'detail_packinglist_subcont.no_polybag')->orderBy('detail_packinglist_subcont.no_polybag', 'bundle_header.poreference')->get();

        $data_qty_array = array();
        $total_qty_all = array();
        
        foreach($data_details as $detail) {
            $data_size_details = array();
            $size_detail = DB::table('detail_packinglist_subcont')->select(DB::raw('detail_packinglist_subcont.no_polybag, bundle_header.size, sum(bundle_detail.qty) as qty'))->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.no_polybag', $detail->no_polybag)->where('bundle_header.style', $detail->style)->where('bundle_header.article', $detail->article)->where('bundle_header.poreference', $detail->poreference)->where('bundle_detail.komponen_name', $detail->komponen_name)->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('detail_packinglist_subcont.no_polybag', 'bundle_header.size')->get();

            $data_size_details['data'] = $size_detail;
            $data_size_details['count'] = $size_detail->count();

            $total_temp = 0;

            foreach($size_detail as $aa) {
                $total_temp = $total_temp + $aa->qty;
            }

            $total_qty_all[] = $total_temp;

            $data_qty_array[] = $data_size_details;
        }
        
        $data_size = array();

        if(count($size) > 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } elseif(count($size) == 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } else {
            for($i = 0; $i < count($size); $i++) {
                $data_size[] = $size[$i];
            }
            $selisih_size = 6 - count($size);
            for($i = 0; $i < $selisih_size; $i++) {
                $data_size[] = 0;
            }
        }

        $paper = array(0,0,609.449,396.85);

        $data = [
            'no_packinglist' => $id,
            'kk' => $packinglist->no_kk,
            'kepada' => $subcont->factory_name,
            'proses'=>'Embro',
            'factory' => $factory->factory_alias,
            'sizes' => $data_size,
            'data_details' => $data_details,
            'data_size_details' => $data_qty_array,
            'total_qty_all' => $total_qty_all,
        ];
        
        $pdf = \PDF::loadView('print.packinglist', $data)->setPaper($paper,'potrait');

        return $pdf->stream($filename);
    }

    public function edit($id)
    {
        $packinglist_details = DB::table('packinglist_subcont')
                        ->where('no_packinglist', $id)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->first();
        $locator_name = DB::table('master_locator')->where('id',$packinglist_details->process)->first();
        $data = new StdClass();
        $data->subcont = $packinglist_details->subcont;
        $data->no_packinglist = $packinglist_details->no_packinglist;
        $data->po_buyer = $packinglist_details->po_buyer;
        $data->no_kk = $packinglist_details->no_kk;
        $data->locator_name = $locator_name->locator_name;  
        $data->remark = $packinglist_details->remark;
        return response()->json($data, 200);
    }

    //UPDATE PACKINGLIST
    public function update(Request $request)
    {
        if(request()->ajax()){
            $data = $request->all();
            $no_kk = $data['no_kk_insert_update'];
            $subcont = $data['subcont_insert_update'];
            $no_packinglist = $data['no_packinglist_insert_update'];
            $locator = $data['locator_insert_update'];
            $po_buyer = $data['po_buyer_insert_update'];
            $remark = $data['remark_insert_update'];
            try {
                DB::beginTransaction();
                    DB::table('packinglist_subcont')
                    ->where('no_packinglist',$no_packinglist)
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->update([
                        'no_kk' => $no_kk,
                        'subcont' => $subcont,
                        'process' => $locator[0],
                        'po_buyer' => implode(',',$po_buyer),
                        'updated_by' => \Auth::user()->id,
                        'factory_id' => \Auth::user()->factory_id,
                        'updated_at' => Carbon::now(),
                        'remark' => $remark,
                    ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }    
        }

    }

    public function release($id){
        if(request()->ajax()) 
        {
            DB::table('packinglist_subcont')
            ->where('no_packinglist', $id)
            ->update([
                'status' => 'release',
                'released_by' => \Auth::user()->id,
                'released_at' => Carbon::now()
            ]);

            return response()->json(200);
        }
    }
}
