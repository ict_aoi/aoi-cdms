@extends('layouts.app',['active' => 'master_set_table'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Setting Table </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Setting Table</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-horizontal">
            <div class="form-group col-md-12">
                <label for="dates" class="control-label col-md-2 col-lg-2 col-sm-12">Date</label>
                <div class="input-group">
                    <input type="date" class="form-control pickadate" name="dates" id="dates" placeholder="Date Spreading">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <label><b>Table Number</b></label>
                <select id="table" class="form-control">
                    <option value="">Select Table</option>
                    <?php
                        foreach ($mtable as $mt) {
                    ?>
                        <option value="{{$mt->id_table}}">{{$mt->table_name}}</option>
                    <?php
                        }
                     ?>
                </select>
            </div>
            <div class="col-md-3">
                <label><b>Shift</b></label>
                <select id="shift" class="form-control">
                    <option value="1">Shift 1</option>
                    <option value="2">Shift 2</option>
                </select>
            </div>
            <div class="col-md-3">
                <label><b>Type Spreading</b></label>
                <select id="spreading" class="form-control">
                    <?php
                        foreach ($mspreading as $ms) {
                    ?>
                        <option value="{{$ms->id}}">{{$ms->type}}</option>
                    <?php
                        }
                     ?>
                </select>
            </div>
            <div class="col-md-3">
                <label><b>Working Hours</b></label>
                <div class="input-group">
                    <input type="text" name="working" id="working" class="form-control" placeholder="Working Hours" required="">
                    <span class="input-group-addon">
                        <b>hours</b>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
               <button type="submit" id="btn_save" style="margin-top: 30px;" class="btn btn-primary col-md-12 col-xs-12 col-lg-12"><span class="icon-checkmark4"></span> Save</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row " >
            <form action="{{route('masterparam.getTableSpreading')}}" id="form_filter">
                @csrf
                <label><b>Choose Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="date" class="form-control pickadate" name="fdates" id="fdates" placeholder="Spreading Date">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary" id="btn-filter">Filter</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Table</th>
                            <th>Shift</th>
                            <th>Sprading Type</th>
                            <th>Target (yard/hour)</th>
                            <th>Working Hours</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){

    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: $('#form_filter').attr('action'),
            data:function (d) {
                    return $.extend({},d,{
                        "fdates": $('#fdates').val()
                    });
                }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'table_name', name: 'table_name'},
            {data: 'shift', name: 'shift'},
            {data: 'type', name: 'type'},
            {data: 'target', name: 'target'},
            {data: 'work_hours', name: 'work_hours'}
        ]
    });



    $('#form_filter').submit(function(event) {

        event.preventDefault();
        var fdates    = $('#fdates').val();


        if (fdates=='') {
            $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
            return false;
        }
        tableL.clear();
        tableL.draw();


    });




    $('#btn_save').on('click',function(){
        if ($('#table').val!='' && $('#working').val()!='' && $('#dates').val()!='') {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : "{{ route('masterparam.insertTableSpreading') }}",
                data: {tabel:$('#table').val(),shift:$('#shift').val(),spreading:$('#spreading').val(),working:$('#working').val(),dates:$('#dates').val()},
                success:function(response){

                    var data_response = response.data;

                    if (data_response.status == 200) {
                        myalert('success', data_response.output);
                    }else{
                        myalert('error', data_response.output);
                    }

                    $('#table-list').unblock();


                    $('#table').val('');
                    $('#working').val('');
                    $('#dates').val('');
                    tableL.draw();
                },
                eror:function(response){
                    myalert('error',response);

                }

            });
        }else{
            myalert('error','Table, date, work hours cannot empty !!');
        }

    });

    $('#table-list').on('blur','.work_unfilled',function(){
            var work = $(this).val();
            var id = $(this).data('id');

            if (work.trim()!=='') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url : "{{ route('masterparam.editWorkHour') }}",
                    data: {id:id,work:work},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            myalert('success', data_response.output);
                        }else{
                            myalert('error', data_response.output);
                        }
                        $('#work_'+id).html(response);
                        $('#table-list').unblock();

                        tableL.clear();
                        tableL.draw();
                    },
                    eror:function(response){
                        myalert('error',response);
                        return false;
                        $('#table-list').unblock();
                        tableL.clear();
                        tableL.draw();
                    }

                });
            }
    });

});

function deltabspread(e){
    var _token = $("input[name='_token]").val();
    var id = e.getAttribute('data-id');
    var tableL = $('#table-list').DataTable();

    bootbox.confirm('Are you sure inactive ?',
        function(result){
            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                        url:"{{ route('masterparam.delTableSpreading') }}",
                        type: "POST",
                        data:{
                            "id": id,
                        },
                        beforeSend:function(){
                            loading_process();
                        },
                        success: function(response){
                           var data_response = response.data;

                            if (data_response.status == 200) {
                                myalert('success', data_response.output);
                            }else{
                                myalert('error', data_response.output);
                            }
                            $('#table-list').unblock();
                            tableL.clear();
                            tableL.draw();
                        },
                        error: function(response) {
                            myalert('error',response);
                        }
                    });
            }
        }
    );
}
</script>
@endsection




