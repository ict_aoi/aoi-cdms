<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>CDMS | Cutting Distribution Management System</title>
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('page-css')
        <script src="{{ mix('js/backend.js') }}"></script>
        <script src="{{ mix('js/notification.js') }}"></script>
        <script src="{{ mix('js/datepicker.js') }}"></script>
        <script src="{{ mix('js/highcharts.js') }}"></script>
        
    </head>
    <body>
       
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content" style="padding-top: 0px;">
                    		@yield('page-header')
                            @yield('page-content')
                            @yield('page-js')
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
