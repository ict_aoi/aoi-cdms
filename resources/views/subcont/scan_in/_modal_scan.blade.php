<div id="scanModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Scan Barcode Component</span>
				</legend>
			</div>
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-10">
						<input type="hidden" class="form-control" style="opacity: 1;" id="user_res" name="user_res" readonly />
						<input type="hidden" class="form-control" style="opacity: 1;" id="locator_res" name="locator_res" readonly />
						<input type="hidden" class="form-control" style="opacity: 1;" id="state_res" name="state_res" readonly />
                        <input type="text" class="form-control" style="opacity: 1;" id="scan_component" name="scan_component" placeholder="#Scan Barcode Bundle" autocomplete="off" />
                    </div>
                    <div class="col-md-2">
                        <input type="text" value="Barcode Komponen" class="form-control bg-info" readonly="readonly" />
                    </div>
                </div>
			</div>
            <br />
			<div class="modal-body">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="scanTable">
						<thead>
							<tr>
								<th>No</th>
								<th>Barcode</th>
								<th>Style | Article</th>
								<th>Part - Komponen</th>
								<th>PO</th>
								<th>Size</th>
								<th>Cut / Sticker No</th>
								<th>Qty</th>
								<th>Action</th>
							</tr>
                        </thead>
					</table>
				</div>
			</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a type="button" class="btn btn-primary" id="save_component">Submit</a>
            </div>
		</div>
	</div>
</div>