@extends('layouts.app',['active' => 'replacement_bundle'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Replacement Bundle</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Replacement Bundle</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			{{--<ul class="nav nav-tabs">
				<li class="active" style="color:white"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">MARKER</a></li>
                <li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">PO BUYER</a></li>
			</ul>--}}

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="/" id="" method="">
						<fieldset class="content-group">
                            @csrf
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <div style="margin-top:10px">
                                        {{--<button type="button" id="btn-filter" style="margin-left:10px" class="btn btn-info pull-right margin-kanan">Filter <i class="icon-pointer position-right"></i></button>--}}
                                        <button type="button" id="btn-replace" class="btn btn-success pull-right margin-kanan">Proses Replacement <i class="icon-sync position-right"></i></button>
                                    </div>
                                    <input type="hidden" name="detail_id" id="detail_id" />                                    
                                </div>
                                
                            </div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead>
										<tr>
											<th>No</th>
                                            <th><input type="checkbox" onclick="toggle(this)" id="check_all"></th>
                                            <th>Barcode Bundle</th>
                                            <th>Defect Name</th>
											<th>Season</th>
											<th>Style</th>
											<th>PO Buyer</th>
											<th>Article</th>
                                            <th>Color</th>
											<th>Cut | Sticker</th>
											<th>Size</th>
											<th>Komponen</th>
											<th>Qty</th>
											<th>Qty Reject</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-js')
<script>
	//DATATABLES 1
	var barcode_marker =$('#barcode_marker').val();
    var url = "{{route('replacement.getDataDetailbundle')}}";
    var url_process = "{{route('replacement.process')}}";
    var tableL = $('#table-list').DataTable({
        // processing: true,
        serverSide: true,
        deferRender:true,
		scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
				return $.extend({}, d, {
					"barcode_marker"		: $('#barcode_marker').val()
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false}, //0
            {data: 'selector', name: 'selector',searchable:false,visible:true,orderable:false},//11
            {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//6
            {data: 'defect_name', name: 'defect_name',searchable:false,visible:true,orderable:true},//10
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//2
            {data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//3
            {data: 'article', name: 'article',searchable:false,visible:true,orderable:true},//4
            {data: 'color_name', name: 'color_name',searchable:false,visible:true,orderable:false},//5
            {data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//6
			{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//7
			{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//8
			{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},//10
			{data: 'qty_reject', name: 'qty_reject',searchable:false,visible:true,orderable:true},//10
        ]
    });

    $('#btn-filter').click(function(){
        event.preventDefault();
        var barcode_marker    = $('#barcode_marker').val();

        if (barcode_marker=='') {
            $("#alert_warning").trigger("click", 'Pilih Marker Dahulu..!');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url,
                data:{
					barcode_marker:barcode_marker
				},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    barcode_marker = barcode_marker;
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });
    $('#btn-replace').click(function(){
        event.preventDefault();
        var data = Array();
        var data_new = Array();

        var url_print = '/replace-bundle/print-pdf';
        
        $('#table-list tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {

                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } 
                    else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } 
                else {
                    data[i][ii] = $(this).text();
                }
                
            });
        });
        
        if (data.length > 0) {
            for (let index = 0; index < data.length; index++) {
                
                if (data[index][1]) {
                    data_new.push(data[index]);
                }
                
            }
        }else{
            myalert('error', 'tidak ada data');
            return false;
        }

        if (data_new.length <= 0) {
            myalert('error', 'data belum dipilih');
            return false;
        }

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url : url_process,
            data:{
                data:JSON.stringify(data_new)
            },
            beforeSend:function(){
                $.blockUI();
            },
            success:function(response){
                $.unblockUI();
                tableL.clear();
                tableL.draw();
                console.log(response, JSON.stringify(response));
                window.open(url_print+'?data='+JSON.stringify(response), '_blank');
            },
            error:function(response){
                $.unblockUI();
                myalert('error','eror');
            }
        });

    });

    $('#print').on('click', function() {
        var data = Array();
        var url_print = '/partial-print-ticket/printPdf';
        $('#table-list tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });

        window.open(url_print+'?data='+JSON.stringify(data), '_blank');
    });
    $('#print2').on('click', function() {
        var data = Array();
        var url_print = '/partial-print-ticket/printPdf';
        $('#table-list2 tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });
        console.log(data);
        window.open(url_print+'?data='+JSON.stringify(data), '_blank');
    });

    function toggle(source) {
        checkboxes = document.getElementsByName('selector[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {

                var attr = checkboxes[i].hasAttribute('disabled');

                if (attr === false) {
                    checkboxes[i].checked = source.checked;
                }
            }
    }
</script>
@endsection