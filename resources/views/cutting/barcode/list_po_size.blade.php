<div class="row">
    {{--<button class="btn btn-primary btn-sm pull-right" id="add_po"><i class="icon icon-plus3"></i> Tambah</button>--}}
</div>
<div class="table-responsive">
    <table class="table" id="table-list-po">
        <thead>
            <tr>
                <th>Style</th>
                <th>PO</th>
                <th>Article</th>
                <th style="width: 12%">Size</th>
                <th style="width: 10%">Qty</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->style }}</td>
                    <td>
                        <div id="po_{{$val->ratio_detail_id}}">
                            <select class="form-control po_unfilled" data-id="{{$val->ratio_detail_id}}">
                                @foreach ($list_po as $item)
                                    <option value="{{$item->po_buyer}}" {{ $item->po_buyer==$val->po_buyer ? " selected" : " " }}>{{$item->po_buyer}}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                    <td>{{ $val->articleno }}</td>
                    <td>
                        <div id="size_{{$val->ratio_detail_id}}">
                            <input type="text"
                                data-id="{{$val->ratio_detail_id}}"
                                class="form-control size_unfilled"
                                onfocus="return $(this).select();"
                                value="{{$val->size}}">
                        </div>
                    </td>
                    <td>
                        <div id="qty_{{$val->ratio_detail_id}}">
                            <input type="text"
                                data-id="{{$val->ratio_detail_id}}"
                                class="form-control qty_unfilled text-right"
                                onkeypress="return isNumberKey(event);"
                                onfocus="return $(this).select();"
                                value="{{$val->qty}}">
                        </div>
                    </td>
                    {{--<td>
                        <button class="btn btn-primary btn-sm" id="add_row" data-id="{{$val->ratio_detail_id}}"><i class="icon icon-plus3"></i></button>
                    </td>--}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<!-- MODAL ADD -->
<div id="modal_detail_po_" data-backdrop="static" data-keyboard="false" class="modal">
    <div class="modal-dialog modal-full">
		<div class="modal-content modal-full">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Tambah PO</h5>
            </div>
            <form action="{{route('cuttingbarcode.addPoRatio')}}" method="post" id="form-add-po">
                @csrf
                <div class="modal-body">
                    <div class="panel-body">
                        <fieldset>                    
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="barcode_ratio" class="form-control col-sm-4">Barcode:</label>
                                                <input type="text" class="form-control col-sm-9" id="barcode_ratio" name="barcode_ratio" required readonly>
                                            </div>
                                            <div class="row">
                                                <label for="style_ratio" class="form-control col-sm-4">Style:</label>
                                                <input type="text" class="form-control col-sm-9" id="style_ratio" name="style_ratio" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="style_ratio" class="form-control col-sm-4">PO:</label>
                                                <select name="po_ratio" id="po_ratio" class="form-control">
                                                    @foreach ($list_po as $item)
                                                        <option value="{{$item->po_buyer}}">{{$item->po_buyer}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /MODAL ADD -->
<script>
    $(document).ready(function () {
        // update qty
        $(document).on('blur', '.qty_unfilled', function() {
            var id = $(this).data('id');
            var qty = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-qty-ratio',
                data: {id:id, qty:qty},
                success: function(response){
                    $('#qty_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });

        // update queu
        $(document).on('blur', '.queue_unfilled', function() {
            var id = $(this).data('id');
            var queue = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-queue-ratio',
                data: {id:id, queue:queue},
                success: function(response){
                    $('#queue_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });

        // update size
        $(document).on('blur', '.size_unfilled', function() {
            var id = $(this).data('id');
            var size_edit = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-size-ratio',
                data: {id:id, size_edit:size_edit},
                success: function(response){
                    $('#size_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });

        // // update cut info
        // $(document).on('blur', '.cutinfo_unfilled', function() {
        //     var id = $(this).data('id');
        //     var cut_info = $(this).val();

        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     $.ajax({
        //         type: 'post',
        //         url: 'cutting-barcode/update-cutinfo-ratio',
        //         data: {id:id, cut_info:cut_info},
        //         success: function(response){
        //             $('#cutinfo_' + id).html(response);
        //         },
        //         error: function(response) {
        //             console.log(response);
        //             myalert('error', response['responseText']);
        //         }
        //     });
        // });

        // update po
        $(document).on('blur', '.po_unfilled', function() {
            var id = $(this).data('id');
            var po_buyer = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-po-ratio',
                data: {id:id, po_buyer:po_buyer},
                success: function(response){
                    //$('#po_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });

        //
        $('#add_po').on('click', function(){
            $('#modal_detail_po_').modal('show');

            $('#barcode_ratio').val($('#barcode_id').val());
            $('#style_ratio').val($('#style').val());
        });

        $('#form-add-po').on('submit', function(e){
            e.preventDefault();

        });

    });
</script>