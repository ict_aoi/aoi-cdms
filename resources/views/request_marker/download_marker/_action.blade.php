<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($download))
                <li><a href="#" onclick="uploadModal('{!! $download !!}')" ><i class="icon-download"></i> Download</a></li>
            @endif
            @if(is_null($model->terima_user_id))
                @if (isset($terima))
                    <li><a href="#" onclick="terima('{!! $terima !!}')" ><i class="icon-download"></i> Terima</a></li>
                @endif
            @else
                @if (isset($unduh))
                    <li><a href="#" onclick="unduh('{!! $unduh !!}')" ><i class="icon-download"></i> Download</a></li>
                @endif
            @endif
            @if (isset($detail))
                <li><a href="#" onclick="detailRatioModal('{!! $detail !!}')"><i class="icon-file-text"></i> Ratio</a></li>
            @endif
        </ul>
    </li>
</ul>