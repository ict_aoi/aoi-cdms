
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($active))
                <li><a href="#" onclick="active('{!! $active !!}')" ><i class="icon-user-check"></i> Active</a></li>
            @endif
            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
            @endif
        </ul>
    </li>
</ul>
