@extends('layouts.scan',['active' => 'dashboard-scan'])

@section('page-css')
<style>
	.center-row {
		text-align: center !important;
	}
	.bg-red {
		background-color: red !important;
		color: white !important;
	}
	.bg-yellow {
		background-color: yellow !important;
	}
	.bg-green {
		background-color: green !important;
		color: white !important;
	}
	.bg-blue {
		background-color: blue !important;
		color: white !important;
	}
	.add-margin-right {
		margin-right: 20px;
	}

    .white-text {
        color: #ffffff !important;
        font-size: 30px;
    }

    .btn-scan {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .title-distribusi {
        padding-top: 0px !important;
    }

    .login-text{
        line-height: 36px;
        vertical-align:center !important;
    }

    .btn-custom-header {
        width: 80px;
    }

</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title text-center title-distribusi">
            <h1 class="white-text"><i class="icon-grid5 position-left"></i> <span class="text-semibold">SCAN LOCATOR</span> <i class="icon-grid5 position-right"></i> </h1>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            {{-- <li class="active">Dashboard</li> --}}
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<fieldset class="content-group">
    <div class="row text-center">
        <div class="col-md-4">
            {{-- <a href="{{ route('distribusi.index') }}" class="btn btn-sm btn-info btn-custom-header pull-left"> BACK </a> --}}
        </div>
        <div class="col-md-4 login-text">
            <label class="control-label col-md-12 col-xs-12">Anda login sebagai, <strong>{{ Auth::user()->name }}<strong></label>
        </div>
        <div class="col-md-4">
            <a href="#"
            {{-- <a href="{{ route('logout') }}" --}}
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            class="btn btn-sm btn-danger pull-right btn-custom-header">
            Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</fieldset>

<div class="panel panel-flat">
    <div class="panel-body">
        @php
            $roles = Auth::user()->roles->pluck('id')->toArray();
        @endphp
        @if (in_array('31',$roles))
            <a href="{{ route('distribusi.menu','10')}}" data-id='10' class="btn-scan btn btn-sm bg-primary-400 col-md-12 col-xs-12"> Cutting </a>
        @endif

        @if (in_array('22',$roles))
            <a href="{{ route('distribusi.menu','3')}}" data-id='3' class="btn-scan btn btn-sm bg-violet-400 col-md-12 col-xs-12"> Artwork </a>
        @endif
		{{-- <a href="#" class="btn-scan btn btn-sm bg-success-400 col-md-12 col-xs-12"> Fuse </a>
		<a href="#" class="btn-scan btn btn-sm bg-success col-md-12 col-xs-12"> Heat Transfer </a>
		<a href="#" class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Pad Print </a>
		<a href="#" class="btn-scan btn btn-sm bg-success-700 col-md-12 col-xs-12"> PPA Jahit </a>
		<a href="#" class="btn-scan btn btn-sm bg-success-800 col-md-12 col-xs-12"> Auto </a> --}}

        @if (in_array('24',$roles))
            <a href="{{ route('distribusi.menu','1')}}" data-id='1' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Fuse </a>
        @endif

        @if (in_array('20',$roles))
            <a href="{{ route('distribusi.menu','2')}}" data-id='2' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Heat Transfer </a>
        @endif

        @if (in_array('66',$roles))
            <a href="{{ route('distribusi.menu','8')}}" data-id='8' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Bonding / Laser </a>
        @endif

        @if (in_array('23',$roles))
            <a href="{{ route('distribusi.menu','4')}}" data-id='4' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Pad Print </a>
        @endif

        @if (in_array('19',$roles))
            <a href="{{ route('distribusi.menu','6')}}" data-id='6' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> PPA 1 </a>
        @endif

        @if (in_array('49',$roles))
            <a href="{{ route('distribusi.menu','7')}}" data-id='7' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> PPA 2</a>
        @endif

        @if (in_array('21',$roles))
            <a href="{{ route('distribusi.menu','5')}}" data-id='5' class="btn-scan btn btn-sm bg-success-600 col-md-12 col-xs-12"> Auto </a>
        @endif

        @if (in_array('32',$roles))
            <a href="{{ route('distribusi.menu','11')}}" data-id='11' class="btn-scan btn btn-sm bg-orange-400 col-md-12 col-xs-12"> Setting </a>
        @endif

        @if (in_array('50',$roles))
            <a href="{{ route('distribusi.menu','16')}}" data-id='16' class="btn-scan btn btn-sm bg-warning-600 col-md-12 col-xs-12"> SUPPLY</a>
        @endif
        <a href="{{ route('distribusi.bundleLocator')}}" class="btn-scan btn btn-sm bg-teal-600 col-md-12 col-xs-12"> BUNDLE LOCATOR</a>
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/notif_mo_update.js') }}"></script>
<script src="{{ mix('js/dashboard_monitoring.js') }}"></script>
@endsection
