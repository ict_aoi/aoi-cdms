<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class QcCheck extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'qc_cutting';
	protected $fillable = ['barcode_id','komponen_id','size','qty','bundle_table','bowing','sliding','pilling','snag','siult','rease_mark','foresign_yam','mising_yam','shide_bar','exposure','hole','dirly_mark','stain','printing_defact','streak_line','dye_spot','shrinkage','slide_cutting','wrong_fice','tag_pin','poor_cutting','needle_hole','fabric_join','snaging_cut','wrong_size','remark','created_at','updated_at','deleted_at','user_id','total_defect','is_approve','is_recycle'];
}
