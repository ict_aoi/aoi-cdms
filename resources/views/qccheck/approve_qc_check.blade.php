@extends('layouts.app',['active' => 'approve_qc_check'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Approval QC Check </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Approval QC Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group" id="form-filter">
            <div class="col-lg-4">
                <label><b>Table QC</b></label>
                <select data-placeholder="Search Table" name="table_id" id="table_id" class="form-control select-search">
                    <option value=""></option>
                    @foreach($table as $tab)
                    <option value="{{ $tab->bundle_table }}">{{ $tab->name }}</option>
                @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <label><b>Barcode Marker</b></label>
                <select data-placeholder="Search Barcode Marker" name="barcode_id" id="barcode_id" class="form-control select-search">
                    <option value=""></option>
                    
                </select>
            </div>
            
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Komponen</th>
                        <th>Size</th>
                        <th>Qty Cutting</th>
                        <th>Qty QC</th>
                        <th>Total Defact</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('Qccheck.qcaApvGetQc') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "barcode_id"         : $('#barcode_id').val(),
                         "table_id"           : $('#table_id').val()
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'komponen_id', name: 'komponen_id'},
                {data: 'size', name: 'size'},
                {data: 'qty_cut', name: 'qty_cut'},
                {data: 'qty_check', name: 'qty_check'},
                {data: 'total_defect', name: 'total_defect'},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });


    $('#table_id').on('change',function(event){
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'get',
            url:"{{ route('Qccheck.qcaApvBarcode') }}",
            data:{id_table:$('#table_id').val()},
            success:function(response){
                var barcode_id = response.barcode;
                $('#barcode_id').empty();
                $('#barcode_id').append('<option value=""></option>');
                for (var i = 0; i < barcode_id.length; i++) {
                    $('#barcode_id').append('<option value="'+barcode_id[i]['barcode_id']+'">'+barcode_id[i]['barcode_id']+'</option>');
                }
            },
            error:function(response){
                console.log(response);
            }
        });
    });

    $('#barcode_id').on('change',function(event){
        event.preventDefault();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
        tableL.clear();
        tableL.draw();
        $.unblockUI();
    });
});

function approve(e){
    var tableL =  $('#table-list').DataTable();
    var barcode = $(e).data('barcode');
    var table = $(e).data('table');
    var size = $(e).data('size');
    var komponen = $(e).data('komponen');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'post',
        url:"{{ route('Qccheck.qcaApvsetQc') }}",
        data:{barcode:barcode,table:table,size:size,komponen:komponen},
        beforeSend:function(){
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success:function(response){
            var data_response = response.data;
            if (data_response.status == 200) {
                $("#alert_success").trigger("click", data_response.output);
                
            }else{
                $("#alert_warning").trigger("click",data_response.output);
               
            }
            tableL.clear();
            tableL.draw();
            $.unblockUI();
        },
        error:function(response){
            var data_response = response.data;
             $("#alert_warning").trigger("click",data_response.output);
            console.log(response);
        }
    });


}
</script>
@endsection

