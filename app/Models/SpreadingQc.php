<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
class SpreadingQc extends Model
{
    use Uuids;
    public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'spreading_qc';
	protected $fillable = ['bau','l_jeblos','w_jeblos','rapi','kencang','kendur',
    'sambungan', 'short_roll', 'fabric_used_id','created_at','updated_at','deleted_at', 'spreading_report_temp_id'];
}
