@php
    $color = str_replace(' ', '-', $model->color_name);
    $color_code = str_replace(' ', '-', $model->color_code);
@endphp

<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($combine))
                <li><a href="#" onclick="combineModal('{!! $combine !!}')"><span class="icon-list"></span> Combine Plan</a></li>
            @endif
            @if (isset($combine_plan_action))
                <li><a href="#" data-cutting_date="{{ $model->cutting_date }}" data-style="{{ $model->style }}" data-articleno="{{ $model->articleno }}" data-size_category="{{ $model->size_category }}" data-queu="{{ $model->queu }}" data-plan_id="{{ $model->plan_id }}" data-mo_created="{{ $model->mo_updated_at }}" data-color="{{ $model->color_name }}" data-color_code="{{ $model->color_code }}" data-material="{{ $model->material }}" data-top_bot="{{ $model->top_bot }}" data-season="{{ $model->season }}" data-factory_id="{{ $model->factory_id }}" class="btn-combine-plan-action"><span class="icon-shrink7"></span> Combine</a></li>
            @endif
            @if (isset($split))
                <li><a href="#" data-pcd="{{ $model->cutting_date }}" data-style="{{ $model->style }}" data-article="{{ $model->articleno }}" data-size="{{ $model->size_category }}" data-color="{{ $model->color_name }}" data-mo="{{ $model->mo_updated_at }}" data-material="{{ $model->material }}" data-color_code="{{ $color_code }}" data-queu="{{ $model->queu }}" data-tb="{{ $model->top_bot }}" data-season="{{ $model->season }}" class="btn-detail" id="btn-detail"><span class="icon-list"></span> Split PO</a></li>
            @endif
            @if (isset($split_set))
                <li><a href="#" data-pcd="{{ $model->cutting_date }}" data-style="{{ $model->style }}" data-article="{{ $model->articleno }}" data-size="{{ $model->size_category }}" data-color="{{ $color }}" data-mo="{{ $model->mo_updated_at }}" data-material="{{ $model->material }}" data-color_code="{{ $color_code }}"  data-queu="null" class="btn-tb" id="btn-tb"><span class="icon-sort"></span> Split Set</a></li>
            @endif
            @if (isset($merge_set))
                <li><a href="#" data-pcd="{{ $model->cutting_date }}" data-style="{{ $model->style }}" data-article="{{ $model->articleno }}" data-size="{{ $model->size_category }}" data-color="{{ $color }}" data-mo="{{ $model->mo_updated_at }}" data-material="{{ $model->material }}" data-color_code="{{ $color_code }}" data-queu="null" class="btn-marge" id="btn-marge"><span class="icon-shrink7"></span> Merge Set</a></li>
            @endif
            @if (isset($delete))
                <li><a href="#" data-pcd="{{ $model->cutting_date }}" data-style="{{ $model->style }}" data-article="{{ $model->articleno }}" data-size="{{ $model->size_category }}" data-color="{{ $color }}" data-mo="{{ $model->mo_updated_at }}" data-queu="{{ $model->queu }}" data-tb="{{ $model->top_bot }}" class="btn-delqueu" id="btn-delqueu"><span class="icon-trash"></span> Delete</a></li>
            @endif
            @if (isset($cancel_combine))
                <li><a href="#" onclick="cancelCombine('{!! $cancel_combine !!}')"><span class="icon-trash"></span> Cancel Combine</a></li>
            @endif
        </ul>
    </li>
</ul>