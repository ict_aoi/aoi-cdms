<div id="downtimeModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Downtime Cutting</span>
				</legend>
			</div>
			<div class="modal-body">
                <fieldset class="content-group">
					<div class="row">
						<div class="form-group col-md-12">
							<div class="row">
								<label class="control-label col-lg-2">Category Downtime</label>
								<div class="input-group col-md-8">
									<select data-placeholder="Pilih a downtime..." name="downtime_category" id="downtime_category" class="form-control select-search">
										<option value="" id=""></option>
										@foreach($downtime_categorys as $downtime_category)
											<option value="{{ $downtime_category->id }}">{{ $downtime_category->category_name }}</option>
										@endforeach
									</select>
									{{-- <div class="input-group-btn">
										<button type="submit" class="btn btn-primary">Filter</button>
									</div> --}}
								</div>
							</div>
						</div>
						<div class="form-group col-md-12">
							<div class="row">
								<label class="control-label col-lg-2">Lama Downtime</label>
								<div class="col-lg-8">
									<input type="number" id="downtime" class="form-control" onkeypress="isNumberDot(event)">
								</div>
								<span class="help-inline">Menit</span>
							</div>
						</div>
						<div class="form-group col-md-12">
							<div class="row">
								<label class="control-label col-lg-2">Keterangan</label>
								<div class="col-lg-8">
									<textarea name="" class="form-control" id="keterangan" cols="3" rows="10"></textarea>
								</div>
							</div>
						</div>
					</div>
                </fieldset>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn_downtime_submit">OK</button>
			</div>
		</div>
	</div>
</div>