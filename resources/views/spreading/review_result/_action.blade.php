@if (isset($detail))
    <a href="#" onclick="detail('{!! $detail !!}')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>
@endif

@if (isset($revisi))
    @if(in_array(\Auth::user()->nik, array('130103737', '190400072')))
        <a href="#" onclick="revisi('{!! $revisi !!}')" class="btn btn-danger btn-icon btn-sm"><i class="icon-cross2"></i></a>
    @else
        <a href="#" class="btn btn-danger btn-icon btn-sm disabled"><i class="icon-cross2"></i></a>
    @endif
@endif