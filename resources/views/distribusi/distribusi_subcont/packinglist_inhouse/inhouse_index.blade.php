@extends('layouts.app',['active' => 'packinglist_distribusi_inhouse'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Packinglist Distribusi - Inhouse</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Packinglist Distribusi - Inhouse</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
        <form class="form-horizontal" action="" id="" method="POST">
			<fieldset class="content-group">
                @csrf
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">No Packinglist</label>
					<div class="col-md-8 row">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<select data-placeholder="Select a No Packinglist..." name="packinglist_no" id="packinglist_no" class="form-control select-search">
                                <option value=""></option>
                            </select>
						</div>
					</div>
                    <div class="form-group col-md-2">
                        <span class="input-group-btn">
                            <button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
                        </span>

                    </div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertPackinglistModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="packinglistTable" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Packinglist</th>
                        <th>Style</th>
                        <th>Subcont</th>
                        <th>Locator</th>
                        <th>Division</th>
                        <th>Remark</th>
                        <th>Total Panel</th>
                        <th>Status</th>
                        <th>Released By</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
<a href="{{ route('packinglistDistribusi.getDataInhouse') }}" id="getDataLink"></a>
<a href="{{ route('packinglistDistribusi.getNoPackinglist') }}" id="getNoPackinglistaLink"></a>
<a href="{{ route('packinglistDistribusi.ajaxGetDataPackinglistinhouse') }}" id="get_data_packinglist"></a>
@section('page-modal')
    @include('distribusi.distribusi_subcont.packinglist_inhouse._insert_modal_inhouse')
    @include('distribusi.distribusi_subcont.packinglist_inhouse._update_modal_inhouse')
@endsection

@section('page-js')
<script>
    $(document).ready( function (){
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });
        var packinglist_no = $('#packinglist_no').val();
        var tableL = $('#packinglistTable').DataTable({
            dom: 'Bfrtip',
            scrollX:true,
            scrollY:480,
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: $('#getDataLink').attr('href'),
                data: function(d) {
                    return $.extend({}, d, {
                        "packinglist_no"		: $('#packinglist_no').val(),
                    });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'no_packinglist', name: 'no_packinglist',searchable:true,orderable:true},
                {data: 'style_set', name: 'style_set',searchable:true,orderable:true},
                {data: 'subcont', name: 'subcont',searchable:true,orderable:true},
                {data: 'locator_name', name: 'locator_name',searchable:true,orderable:true},
                {data: 'division', name: 'division',searchable:true,orderable:true},
                {data: 'remark', name: 'remark',searchable:true,orderable:true},
                {data: 'total_panel', name: 'total_panel',searchable:false,orderable:true},
                {data: 'status', name: 'status',searchable:true,orderable:true},
                {data: 'released_by', name: 'released_by',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        tableL.on('preDraw',function(){
            Pace.start();
            $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            })
        .on('draw.dt',function(){
            Pace.stop();
            $.unblockUI();
        });

        $('#btn-filter').click(function(){
            event.preventDefault();
            var packinglist_no    = $('#packinglist_no').val();
            if (packinglist_no == null) {
                $("#alert_warning").trigger("click", 'Silahkan Pilih Packinglist Dahulu..!');
                return false;
            }
            tableL.draw();
        });

        var dtable = $('#packinglistTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#subcont_insert').on('change', function(event) {
            var subcont = this.value;        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $.ajax({
                type: "POST",
                url: $('#getNoPackinglistaLink').attr('href'),
                data: {
                    subcont: subcont,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('#no_packinglist_insert').val(response);
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });

        $('#subcont_insert_update').on('change', function(event) {
            var subcont = this.value;        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $.ajax({
                type: "POST",
                url: $('#getNoPackinglistaLink').attr('href'),
                data: {
                    subcont: subcont,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('#no_packinglist_insert_update').val(response);
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });
    
        $('#form_insert').submit(function(event) {
            event.preventDefault();
            var po_buyer = $('#po_buyer_insert').val();
            var subcont = $('#subcont_insert').val();
            var no_packinglist = $('#no_packinglist_insert').val();
            var no_kk = $('#no_kk_insert').val();
            var locator = $('#locator_insert').val();
            var remark = $('#remark_insert').val();
            var style = $('#style').val();
            console.log(subcont);
            if(!no_kk){
                $("#alert_warning").trigger("click", 'No KK wajib diisi');
                return false;
            }
    
            if(!po_buyer){
                $("#alert_warning").trigger("click", 'PO buyer wajib diisi');
                return false;
            }
    
            if(!no_packinglist){
                $("#alert_warning").trigger("click", 'packinglist wajib diisi');
                return false;
            }
    
            if(!subcont){
                $("#alert_warning").trigger("click", 'subcont wajib dipilih');
                return false
            }

            if(!style){
                $("#alert_warning").trigger("click", 'style wajib dipilih');
                return false
            }
    
            if(!locator){
                $("#alert_warning").trigger("click", 'locator wajib dipilih');
                return false
            }
            
            $.ajax({
                type: "POST",
                url: $('#form_insert').attr('action'),
                data:new FormData($("#form_insert")[0]),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function (){
                    $.unblockUI();
                },
                success: function (){
                    $('#remark_insert').val(''),
                    $('#packinglist_no').val(''),
                    $('#no_kk_insert').empty(),
                    $('#packinglistTable').DataTable().ajax.reload();
                    $('#insertPackinglistModal').modal('hide');
                    $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                    window.location.reload();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    $('#insertPackinglistModal').modal();
                }
            });
        });

        $('#style').on('change', function (){  
            var url_get_kk = $('#url_get_kk').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url :  url_get_kk,
                data : {
                    style:$('#style').val()
                },
                success: function(response) {
                    var data = response.response;
                    $('#no_kk_insert').empty();
                    $('#no_kk_insert').append('<option>Select KK No</option>');
                    for (var i = 0; i < data.length; i++) {
                        $('#no_kk_insert').append('<option value="'+data[i]['document_no']+'">'+data[i]['kk_no']+'</option>')
                    }
                    
                },
                error: function(response) {
                    $.unblockUI();
                    alert(response.status,response.responseText);
                }
            });
        });
        $('#style_update').on('change', function (){  
            var url_get_kk_update = $('#url_get_kk_update').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url :  url_get_kk_update,
                data : {
                    style_update:$('#style_update').val()
                },
                success: function(response) {
                    var data = response.response;
                    $('#no_kk_insert_update').empty();
                    for (var i = 0; i < data.length; i++) {
                        if(typeof(data[i]['kk_no']) === 'undefined'){
                            $('#no_kk_insert_update').append('<option value="'+data[i]['document_no']+'">'+data[i]['no_kk']+'</option>')
                        }else{
                            $('#no_kk_insert_update').append('<option value="'+data[i]['document_no']+'">'+data[i]['kk_no']+'</option>')
                        }
                    }
                    
                },
                error: function(response) {
                    $.unblockUI();
                    alert(response.status,response.responseText);
                }
            });
        });

        $('#packinglist_no').select2({
            placeholder: "Pilih Packinglist",
            minimumInputLength: 3,
            ajax: {
                url: $('#get_data_packinglist').attr('href'),
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data
                    };
                },
                cache: false
            }
        });

        $('#updatePackinglistModal').on("hidden.bs.modal", function(){
            $.ajax({
                url: '/packinglist-distribusi/destroySessionedit',
                complete: function () {
                $.unblockUI();
                },
                success: function(response) {
                    myalert('success','Edit Selesai');
                    dtable.ajax.reload();
                    window.location.reload();
                },
                error: function(response) {
                    $.unblockUI();
                    myalert('error','Gagal Edit');
                }
            });
            
        });


        $('#form_update').submit(function(event) {
            event.preventDefault();
            var formData = new FormData($(this)[0]);
            var parameter = ['no_packinglist_insert_update', 'no_kk_insert_update', 'subcont_insert_update', 
            'remark_insert_update', 'po_buyer_insert_update','locator_insert_update','style_update','id_update'];
            var val = [
                $('#id_update').val(),
                $('#no_kk_insert_update').val(),
                $('#subcont_insert_update').val(),
                $('#no_packinglist_insert_update').val(),
                $('#po_buyer_insert_update').val(),
                $('#style_update').val(),
                $('#locator_insert_update').val(),
                $('#remark_insert_update').val(),
            ]
            console.log(formData);
            for(var i = 0; i < parameter.length; i++ ) {
                formData.append(parameter[i], val[i]);
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/packinglist-distribusi/update-data-inhouse',
                data: new FormData($('#form_update')[0]),
                type: 'post',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function(response) {
                    myalert('success','GOOD');
                    $('#updatePackinglistModal').modal('hide');
                    dtable.ajax.reload();
                    window.location.reload();
                },
                error: function(response) {
                    // myalert('error',response['responseJSON']);
                    $.unblockUI();
                    if (response.status == 422) $("#alert_info").trigger("click",response.responseJSON);
                    return false;
                }
            });
        });
    });
    function hapus(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "delete",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                myalert('success','Packinglist Deleted!');
                $('#packinglistTable').DataTable().ajax.reload();
                // return false;
                // $('#packinglistTable').DataTable().ajax.reload();
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                location.reload();
                // $('#packinglistTable').DataTable().ajax.reload();
            }
        });
    }
    function edit(url) {
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                var id = response.id;
                var po_buyer_insert = response.po_buyer;
                var style = response.style;
                var locator_name = response.locator_name;
                var locator_id = response.locator_id
                var no_kk_insert = response.no_kk;
                var subcont_insert = response.subcont_id;
                $.unblockUI();
                $('#id_update').val(id);
                $('#no_packinglist_insert_update').val(response.no_packinglist);
                $('#no_kk_insert_update').val(response.document_no).trigger('change');
                $('#locator_insert_update').val(response.locator_id).trigger('change');
                $('#style_update').val(response.style).trigger('change');
                $('#subcont_insert_update').val(response.subcont_id).trigger('change');
                $('#remark_insert_update').val(response.remark);

                if(po_buyer_insert != null && po_buyer_insert != ''){
                $.each(po_buyer_insert.split(","), function(i, v){
                    $('#po_buyer_insert_update option[value='+ v +']').prop('selected', true).trigger('change');
                });
                }else{
                    $('#po_buyer_insert_update').val(null).trigger('change');
                }

                $('#updatePackinglistModal').modal();
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                return false;
            }
        });
    }
    function release(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
                $('#packinglistTable').DataTable().ajax.reload();
            },
            error: function () {
                $.unblockUI();
            }
        })
        .done(function () {
            $('#packinglistTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Document Released..!');
        });
    }
</script>
@endsection
