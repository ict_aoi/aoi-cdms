<div id="tableModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Choose Cutting Table</span>
				</legend>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="tableTable">
						<thead>
							<tr>
								<th>#</th>
								<th>Cutting Table</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
		</div>
	</div>
</div>