<!-- MODAL -->
<div id="modal_sync_sds_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-header">
                <legend class="text-semibold">
                    <i class="icon-file-text2 position-left"></i>
                    <span id="title"> Style</span> <span id="style_selected">-</span>
                </legend>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-semibold">Season:</label>
                        <div class="col-sm-9">
                            <select data-placeholder="Select a Season..." name="season_sync_sds" id="season_sync_sds" class="form-control select-search" required>
                                <option value=""></option>
                                @foreach ($seasons as $item)  
                                    <option value="{{ $item->season_name }}">{{ $item->season_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_migrate_from_sds">Migrate From SDS</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL -->