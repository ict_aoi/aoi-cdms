<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail ID Marker</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="style_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								: <span id="articleno_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr>
								<th>ID</th>
								<th>Color</th>
								<th>Part No</th>
								<th>Cut</th>
								<th>Rasio</th>
								<th>Layer</th>
								<th>Status</th>
								<th><input type="checkbox" onclick="toggle(this)" id="check_all"></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="planning_id" id="planning_id" />
				<div class="form-group">
					<label class="display-block text-semibold">Choose One</label>
					<label class="radio-inline">
						<input type="radio" name="print_type" value="single" checked="checked">
						Single
					</label>

					<label class="radio-inline">
						<input type="radio" name="print_type" value="double">
						Double
					</label>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="btn_realokasi">Re-alokasi</button>
				<button type="button" class="btn btn-success" id="btn_print_id">Print</button>
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>