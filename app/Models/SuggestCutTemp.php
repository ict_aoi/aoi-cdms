<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SuggestCutTemp extends Model
{
    use Uuids;
    public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'suggest_cut_temp';
    protected $fillable = ['barcode_id','po_buyer','size','part_no','qty','plan_id','factory_id','user_id','created_at','updated_at','deleted_at','queue'];
}
