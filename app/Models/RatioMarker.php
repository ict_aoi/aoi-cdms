<?php

namespace App\Models;
use App\Uuids;

use Illuminate\Database\Eloquent\Model;

class RatioMarker extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'ratio';
	protected $fillable = ['id_marker','ratio','size','created_at','updated_at'];
}
