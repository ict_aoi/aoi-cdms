<?php

namespace App\Http\Controllers\RequestFabric;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\RasioMarker;
use App\Models\TempPageAccess;
use App\Models\Factory;
use App\Http\Controllers\Controller;

class FabricNeedController extends Controller
{
    public function index()
    {
        $factorys = Factory::where('deleted_at', null)->where('status', 'internal')->orderBy('factory_alias', 'asc')->get();
        return view('request_fabric.fabric_need.index', compact('factorys'));
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = isset($request->factory) ? $request->factory : \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('deleted_at', null);
                }
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('poreference', function($data) {
                    $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    return implode(', ', $po_array);
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('status', function($data) {
                    if (count($data->cutting_markers) > 0) return '<span class="badge bg-primary">File Uploaded</span>';
                    else return '<span class="badge bg-danger">Not Yet</span>';
                })
                ->addColumn('last_updated', function($data) {
                    $get_marker = CuttingMarker::where('id_plan', $data->id)->whereNotNull('ratio_updated_at')->orderBy('ratio_updated_at', 'desc')->first();
                    if($get_marker != null) {
                        return $get_marker->user->name;
                    } else {
                        return '-';
                    }
                })
                ->addColumn('detail', function($data) {
                    return view('request_marker.actual_marker._action', [
                        'model'      => $data,
                        'detail'     => route('fabricNeed.detailModal',$data->id)
                    ]);
                })
                ->rawColumns(['detail', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;

            if($planning_id != NULL) {
                $data = CuttingMarker::select('color', 'part_no', 'id_plan')->orderBy('part_no', 'asc')->where('id_plan', $planning_id)->where('deleted_at', null)->groupBy('color', 'part_no', 'id_plan');

                return datatables()->of($data)
                ->addColumn('detail', function($data) {
                    return view('request_marker.actual_marker._action', [
                        'model'      => $data,
                        'detailDetail'     => route('fabricNeed.detailDetailModal', ['id' => $data->id_plan, 'part_no' => $data->part_no])
                    ]);
                })
                ->addColumn('status', function($data) {
                    $data_is_null = 0;
                    $data_approved = 0;
                    $data_marker = CuttingMarker::where('id_plan', $data->id_plan)->where('part_no', $data->part_no)->where('color', $data->color)->where('deleted_at', null)->get();
                    foreach($data_marker as $a) {
                        if($a->approved == null) {
                            if($a->marker_length == null) {
                                $data_is_null++;
                            }
                        } else {
                            $data_approved++;
                        }
                    }

                    if($data_approved > 0) {
                        return '<span class="badge bg-success">Approved</span>';
                    } elseif($data_is_null > 0) {
                        return '<span class="badge bg-danger">Not Yet</span>';
                    } else {
                        return '<span class="badge bg-primary">Completed</span>';
                    }
                })
                ->addColumn('total_cutting', function($data) {
                    $data_marker = CuttingMarker::where('id_plan', $data->id_plan)->where('part_no', $data->part_no)->where('color', $data->color)->where('deleted_at', null)->get();
                    return count($data_marker);
                })
                ->rawColumns(['status','detail','action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('status', function($data) {
                    return null;
                })
                ->addColumn('detail', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['status','detail','action'])
                ->make(true);
            }
        }
    }

    public function dataDetailMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            if($planning_id != NULL) {
                $data = CuttingMarker::orderBy('cut', 'asc')->where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                return datatables()->of($data)
                ->editColumn('id_row', function($data) {
                    return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="id_marker-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="id_marker" value="'.$data->barcode_id.'" readonly /></div></div>';
                })
                ->addColumn('rasio', function($data) {
                    $list_ratio = array();
                    foreach($data->rasio_markers as $a) {
                        $list_ratio[] = $a->size.'-'.$a->ratio;
                    }
                    $data_temp = implode(', ', $list_ratio);
                    return $data_temp;
                })
                ->editColumn('layer', function($data) {
                    return $data->layer;
                })
                ->editColumn('barcode_marker', function($data) {
                    return $data->barcode_id;
                })
                ->addColumn('lebar_fb', function($data) {
                    return $data->fabric_width;
                })
                ->addColumn('lebar_marker', function($data) {
                    if($data->fabric_width != null && $data->fabric_width != 0) {
                        return $data->fabric_width - 0.5;
                    } else {
                        return $data->fabric_width;
                    }
                })
                ->addColumn('perimeter', function($data) {
                    return $data->perimeter;
                })
                ->addColumn('actual_marker', function($data) {
                    return $data->marker_length;
                })
                ->rawColumns(['id_row', 'perimeter', 'rasio', 'actual_marker', 'layer', 'lebar_fb', 'action','last_edit','barcode_marker'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataLebarFB(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            if($planning_id != NULL) {
                $data = array();

                $data_marker_query = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                $data_marker_row = $data_marker_query->get()->first();

                $part_no_array = explode('+', $part_no);

                // $data_lebar = DB::connection('wms_live_new')
                // ->table('integration_whs_to_cutting')
                // ->select(DB::raw("actual_width, sum(qty_prepared) as alokasi_fb"))
                // ->where('style', $data_marker_row->cutting_plan->style)
                // ->where('planning_date', $data_marker_row->cutting_plan->cutting_date)
                // ->where('article_no', $data_marker_row->cutting_plan->articleno)
                // ->whereIn('part_no', $part_no_array)->groupBy('actual_width')
                // ->get();
                $plan_date      = $data_marker_row->cutting_plan->cutting_date;
                $article        = $data_marker_row->cutting_plan->articleno;
                $part           = "'" . implode("','",$part_no_array) . "'";
                $style          = $data_marker_row->cutting_plan->style;

                $data_lebar = DB::connection('wms_live_new')->select(DB::raw("SELECT actual_width, sum(qty_prepared) as alokasi_fb FROM integration_to_cutting_by_planning_f('$plan_date') WHERE style='$style' AND article_no ='$article' AND part_no IN($part) GROUP BY actual_width"));

                foreach($data_lebar as $a) {
                    $data_qty_kebutuhan = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->where('fabric_width', $a->actual_width)->sum('need_total');
                    $data[] = [
                        'lebar_fb' => $a->actual_width,
                        'alokasi_fb' => $a->alokasi_fb,
                        'need_total' => $data_qty_kebutuhan,
                    ];
                }

                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('lebar_fb', function($data) {
                    return null;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataSizeBalance(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $data_temp = array();
            $data = array();
            $data_rasio_size = array();

            if($planning_id != NULL) {
                $data_marker = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->get();

                $data_per_size = DB::table('jaz_detail_size_per_po')->select(DB::raw("size_finish_good, sum(qty) as qty"))->where('cutting_date', $data_marker->first()->cutting_plan->cutting_date)->where('queu', $data_marker->first()->cutting_plan->queu)->where('factory_id', $data_marker->first()->cutting_plan->factory_id)->whereIn('po_buyer', $data_marker->first()->cutting_plan->po_details->pluck('po_buyer')->toArray())->groupBy('size_finish_good')->get();

                $data_temp[0][0] = 'Size';
                $data_temp[1][0] = 'Total Kebutuhan';
                $data_temp[2][0] = 'Total Marker';

                for($i1 = 0; $i1 < 17; $i1++) {
                    if(isset($data_per_size[$i1])) {
                        $data_temp[0][$i1 + 1] = $data_per_size[$i1]->size_finish_good;
                        $data_temp[1][$i1 + 1] = $data_per_size[$i1]->qty;
                    } else {
                        $data_temp[0][$i1 + 1] = null;
                        $data_temp[1][$i1 + 1] = null;
                    }
                }

                foreach($data_marker as $a) {
                    foreach($a->rasio_markers as $b) {
                        $data_rasio_size[] = [
                            'size' => $b->size,
                            'rasio' => $b->ratio * $a->layer,
                        ];
                    }
                }

                for($i2 = 1; $i2 < 17; $i2++) {
                    $jumlah_qty = 0;
                    foreach($data_rasio_size as $a) {
                        if($data_temp[0][$i2] != null) {
                            if($a['size'] == $data_temp[0][$i2]) {
                                $jumlah_qty += $a['rasio'];
                            }
                        }
                    }
                    if($jumlah_qty == 0) {
                        $jumlah_qty = null;
                    }
                    $data_temp[2][$i2] = $jumlah_qty;
                }

                for($i3 = 0; $i3 < 3; $i3++) {
                    if($i3 == 2) {
                        for($i4 = 0; $i4 < 16; $i4++) {
                            if($i4 == 0) {
                                $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                            } else {
                                if($data_temp[$i3][$i4] != null) {
                                    if($data_temp[$i3][$i4] > $data_temp[$i3-1][$i4]) {
                                        $data_temp[$i3][$i4] = '<div class="bg-danger">'.$data_temp[$i3][$i4].'</div>';
                                    } elseif($data_temp[$i3][$i4] < $data_temp[$i3-1][$i4]) {
                                        $data_temp[$i3][$i4] = '<div class="bg-orange-300">'.$data_temp[$i3][$i4].'</div>';
                                    } else {
                                        $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                    }
                                } else {
                                    $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                }
                            }
                        }
                    } else {
                        for($i5 = 0; $i5 < 16; $i5++) {
                            $data_temp[$i3][$i5] = $data_temp[$i3][$i5];
                        }
                    }

                    $data[] = [
                        'column1' => $data_temp[$i3][0],
                        'column2' => $data_temp[$i3][1],
                        'column3' => $data_temp[$i3][2],
                        'column4' => $data_temp[$i3][3],
                        'column5' => $data_temp[$i3][4],
                        'column6' => $data_temp[$i3][5],
                        'column7' => $data_temp[$i3][6],
                        'column8' => $data_temp[$i3][7],
                        'column9' => $data_temp[$i3][8],
                        'column10' => $data_temp[$i3][9],
                        'column11' => $data_temp[$i3][10],
                        'column12' => $data_temp[$i3][11],
                        'column13' => $data_temp[$i3][12],
                        'column14' => $data_temp[$i3][13],
                        'column15' => $data_temp[$i3][14],
                        'column16' => $data_temp[$i3][15],
                    ];
                }

                return datatables()->of($data)
                ->rawColumns(['column1', 'column2', 'column3', 'column4', 'column5', 'column6', 'column7', 'column8', 'column9', 'column10', 'column11', 'column12', 'column13', 'column14', 'column15', 'column16'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('lebar_fb', function($data) {
                    return null;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataOverConsum(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $data = array();

            if($planning_id != NULL) {
                $data_marker_query = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                $data_marker = $data_marker_query->get()->first();

                $part_no_array = explode('+', $part_no);

                $data_uom_cons = DB::table('data_cuttings')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no_array)->first()->uom;
                $data_cons = DB::table('data_cuttings')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no_array)->sum('fbc');
                if($data_uom_cons == 'M') {
                    $data_cons = $data_cons * 1.0936;
                }

                $data_qty_kebutuhan = $data_marker_query->sum('need_total');

                $data[] = [
                    'qty_csi' => $data_cons,
                    'qty_need' => $data_qty_kebutuhan,
                    'over_consum' => $data_cons - $data_qty_kebutuhan,
                ];

                return datatables()->of($data)
                ->rawColumns(['qty_csi', 'qty_need', 'over_consum'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('qty_csi', function($data) {
                    return null;
                })
                ->addColumn('qty_need', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailModal($id)
    {
        $get_data = CuttingPlan::where('id', $id)->where('deleted_at', null)->firstOrFail();
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $get_data->cutting_date;
        $obj->style = $get_data->style;
        $obj->articleno = $get_data->articleno;
        $obj->size_category = $get_data->size_category;
        $obj->poreference = implode(', ', $get_data->po_details->pluck('po_buyer')->toArray());
		$obj->url_upload = '#';

		return response()->json($obj,200);
    }

    public function detailDetailModal($id, $part_no)
    {
        $upload = CuttingMarker::where('id_plan', $id)->where('part_no', $part_no)->where('deleted_at', null)->firstOrFail();

        $obj = new StdClass();
        $obj->id_plan = $upload->id_plan;
        $obj->cutting_date = $upload->cutting_plan->cutting_date;
        $obj->style = $upload->cutting_plan->style;
        $obj->articleno = $upload->cutting_plan->articleno;
        $obj->size_category = $upload->cutting_plan->size_category;
        $obj->part_no = $upload->part_no;
        $obj->color = $upload->color;
        $obj->user = null;
        $obj->url_upload = route('fabricNeed.updateDataMarker');

        return response()->json($obj,200);
    }

    public function downloadGenerate(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            if($planning_id != null && $planning_id != '') {
                $obj = new StdClass();
                $obj->download_link = route('fabricNeed.download', $planning_id);
                return response()->json($obj,200);
            } else {
                return response()->json('Pilih tanggal cutting terlebih dahulu',422);
            }
        }
    }

    public function download(Request $request, $planning_id)
    {
        $planning_id = $request->planning_id;

        if($planning_id != null && $planning_id != '') {

            $data = CuttingMarker::where('id_plan', $planning_id)
            ->where('deleted_at', null)
            ->orderBy('part_no', 'asc')
            ->orderBy('cut', 'asc');

            if(count($data->get()) < 1)
            {
                return response()->json('Data tidak ditemukan.', 422);
            } else {
                $data = $data->get();
                $filename = 'Report_Fabric_Used';

                $i = 1;

                $export = \Excel::create($filename, function($excel) use ($data, $i, $planning_id) {

                    $excel->sheet('report', function($sheet) use ($data, $i, $planning_id) {

                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '0',
                            'D' => '0',
                            'E' => '0',
                            'F' => '0',
                            'G' => '0',
                            'H' => '0',
                        ));

                        $data_header = CuttingPlan::where('id', $planning_id)
                        ->where('is_header', true)
                        ->where('deleted_at', null)
                        ->first();

                        if($data_header->header_id != null && $data_header->is_header) {
                            $query = CuttingPlan::where('header_id', $data_header->id)
                            ->whereNull('deleted_at');

                            $get_plan_id = $query->pluck('id')
                            ->toArray();

                            $po = implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                            ->whereNull('deleted_at')
                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                            ->pluck('po_buyer')->toArray());

                            $style = implode(', ', $query->pluck('style')
                            ->toArray());

                            $articleno = implode(', ',$query->pluck('articleno')
                            ->toArray());
                        } else {
                            $po = implode(', ', $data_header->po_details()
                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                            ->pluck('po_buyer')->toArray());

                            $style = $data_header->style;

                            $articleno = $data_header->articleno;
                        }

                        $sheet->setCellValue('A1', 'STYLE');
                        $sheet->setCellValue('B1', $style);
                        $sheet->setCellValue('A2', 'PO');
                        $sheet->setCellValue('B2', $po);
                        $sheet->setCellValue('A3', 'PLAN');
                        $sheet->setCellValue('B3', $data_header->cutting_date);
                        $sheet->setCellValue('A4', 'ARTICLE');
                        $sheet->setCellValue('B4', $articleno);

                        $sheet->setCellValue('A6','CUT');
                        $sheet->setCellValue('B6','PART');
                        $sheet->setCellValue('C6','RASIO');
                        $sheet->setCellValue('D6','LAYER');
                        $sheet->setCellValue('E6','LEBAR MARKER');
                        $sheet->setCellValue('F6','PERIMETER');
                        $sheet->setCellValue('G6','PANJANG MARKER');
                        $sheet->setCellValue('H6','TOTAL KEBUTUHAN');

                        $index = 0;
                        foreach ($data as $value) {
                            $list_ratio = array();
                            foreach($value->rasio_markers as $a) {
                                $list_ratio[] = $a->size.'-'.$a->ratio;
                            }
                            $data_temp = implode(', ', $list_ratio);

                            if($value->fabric_width != null && $value->fabric_width != 0) {
                                $lebar_marker = $value->fabric_width - 0.5;
                            } else {
                                $lebar_marker = $value->fabric_width;
                            }

                            $row = $index + 7;
                            $sheet->setCellValue('A'.$row, $value->cut);
                            $sheet->setCellValue('B'.$row, $value->part_no);
                            $sheet->setCellValue('C'.$row, $data_temp);
                            $sheet->setCellValue('D'.$row, $value->layer);
                            $sheet->setCellValue('E'.$row, $lebar_marker);
                            $sheet->setCellValue('F'.$row, $value->perimeter);
                            $sheet->setCellValue('G'.$row, $value->marker_length);
                            $sheet->setCellValue('H'.$row, $value->need_total);
                            $index++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->download('xlsx');


                return response()->json(200);
            }
        } else {
            return response()->json('Select tanggal cutting terlebih dahulu!.', 422);
        }
    }
}
