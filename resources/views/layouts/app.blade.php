<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>CDMS | Cutting Distribution Management System</title>
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        <link rel="icon" href="{{ asset('/images/logo2.png')  }}" type="image/x-icon">
        @yield('page-css')

        <script type="text/javascript">

            function myalert(type, text) {
                var color = '#66BB6A';
                var title = 'Good Job!';
                if(type == 'error') {
                    color = '#EF5350';
                    title = 'Oops...';
                }
                swal({
                    title: title,
                    text: text,
                    confirmButtonColor: color,
                    type: type
                });
            }



          // loading
        function loading_process() {
            $('#table-list').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        }

        function loading_md() {
            $('#table-list-md').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        }

        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
            return true;
        }

        function isNumberDot(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        </script>
    </head>
    <body>
        @include('includes.main_navbar')
        @include('includes.top_notif')
        <div class="page-container">
            <div class="page-content">
                @include('includes.side_navbar')
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                            @yield('page-content')
                            @yield('page-modal')
                            <script src="{{ mix('js/backend.js') }}"></script>
                            <script src="{{ mix('js/notification.js') }}"></script>
                            <script src="{{ mix('js/datepicker.js') }}"></script>
                            @yield('page-js')
                            <script type="text/javascript">
                                $('.number2').keypress(function (event) {
                                    var $this = $(this);

                                    if ((event.which != 44 || $this.val().indexOf(',') != -1) &&
                                            ((event.which < 48 || event.which > 57) &&
                                                    (event.which != 0 && event.which != 8))) {
                                        event.preventDefault();
                                    }

                                    var text = $(this).val();
                                    if ((event.which == 188) && (text.indexOf(',') == -1)) {
                                        setTimeout(function () {
                                            if ($this.val().substring($this.val().indexOf(',')).length > 3) {
                                                $this.val($this.val().substring(0, $this.val().indexOf(',') + 3));
                                            }
                                        }, 1);
                                    }

                                   /* if ((text.indexOf(',') != -1) &&
                                            (text.substring(text.indexOf(',')).length > 2) &&
                                            (event.which != 0 && event.which != 8) &&
                                            ($(this)[0].selectionStart >= text.length - 2)) {
                                        event.preventDefault();
                                    }*/
                                });

                                $('.number2').bind("paste", function (e) {
                                    var text = e.originalEvent.clipboardData.getData('Text');
                                    if ($.isNumeric(text)) {
                                        if ((text.substring(text.indexOf(',')).length > 3) && (text.indexOf(',') > -1)) {
                                            e.preventDefault();
                                            $(this).val(text.substring(0, text.indexOf(',') + 3));
                                        }
                                    } else {
                                        e.preventDefault();
                                    }
                                });

                                $('.select-search2').select2({
                                    placeholder: "Select a State",
                                    minimumInputLength: 3,
                                    formatInputTooShort: function () {
                                        return "Enter Minimum 3 Character";
                                    },
                                });
                            </script>
                        <div class="footer text-muted">
                            &copy; 2019. <a href="route('home')">CDMS | Cutting Distribution Management System</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
