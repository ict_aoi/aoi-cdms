<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundle_header', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('season', 100)->nullable();
            $table->string('style', 200)->nullable();
            $table->string('set_type', 50)->nullable();
            $table->string('poreference', 100)->nullable();
            $table->string('article', 200)->nullable();
            $table->string('cut_num', 100)->nullable();
            $table->string('size', 100)->nullable();
            $table->integer('factory_id')->nullable();
            $table->date('cutting_date')->nullable();
            $table->string('color_name', 200)->nullable();
            $table->string('cutter', 200)->nullable();
            $table->text('note')->nullable();
            $table->integer('admin')->nullable();
            $table->integer('qc_operator')->nullable();
            $table->integer('bundle_operator')->nullable();
            $table->integer('box_lim')->nullable();
            $table->integer('max_bundle')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_header');
    }
}
