@extends('layouts.app',['active' => 'distribusi_artwork_in'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    .control-label {
        padding-top: 9px !important;
    }
    .margin-atas {
        margin-top: 20px;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Distribusi Artwork In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Distribusi Artwork In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-12">
                    <center><h5>Input Data</h5></center>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" value="PACKINGLIST NUMBER" class="form-control `bg-info" readonly="readonly" />
                </div>
                <div class="col-md-5">
                    <select data-placeholder="Select a No Packinglist..." name="packinglist_input" id="packinglist_input" class="form-control select-search">
                        <option value=""></option>
                        @foreach($packinglists as $list)
                            <option value="{{ $list->no_packinglist }}">{{ $list->no_packinglist }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                <input type="text" value="" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <hr />
            <table border="0" width="100%">
                <tr>
                    <td><strong>KK</strong></td>
                    <td>: <span id="kk_input">Data Empty!</span></td>
                    <td><strong>Subcont</strong></td>
                    <td>: <span id="subcont_input">Data Empty!</span></td>
                </tr>
                <tr>
                    <td><strong>Locator</strong></td>
                    <td>: <span id="locator_input">Data Empty!</span></td>
                    <td><strong>PO Buyer</strong></td>
                    <td>: <span id="po_buyer_input">Data Empty!</span></td>
                </tr>
            </table>
            <div class="row margin-atas">
                <button class="btn btn-primary col-md-12" style="width:100%" id="scan_barcode">Mulai Scan</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Proses Terbaru</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="componentTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Barcode</th>
                        <th>Style | Article</th>
                        <th>Part - Komponen</th>
                        <th>PO</th>
                        <th>Size</th>
                        <th>Cut / Sticker No</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('distribusi_artworkin.detailPackinglist') }}" id="detailPackinglistLink"></a>
<a href="{{ route('distribusi_artworkin.dataPackinglist') }}" id="dataPackinglistLink"></a>
<a href="{{ route('distribusi_artworkin.dataComponent') }}" id="dataComponentLink"></a>
<a href="{{ route('distribusi_artworkin.dataComponentPackinglist') }}" id="dataComponentPackinglistLink"></a>
<a href="{{ route('distribusi_artworkin.scanComponent') }}" id="scanComponentLink"></a>
<a href="{{ route('distribusi_artworkin.saveComponent') }}" id="saveComponentLink"></a>
@endsection

@section('page-modal')
	@include('distribusi.distribusi_subcont.scan_in._modal_scan')
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {
            $('body').addClass('sidebar-xs');
            $(".file-styled").uniform({
                fileButtonClass: 'action btn bg-warning'
            });

            $.extend( $.fn.dataTable.defaults, {
                stateSave: true,
                autoWidth: false,
                autoLength: false,
                processing: true,
                // serverSide: true, //sync
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                }
            });

            $('#scanModal').on('hidden.bs.modal', function(){
                get_data_component_packinglist($('#packinglist_input').val());
            });

            $('#packinglist_input').on('change', function() {
                var packinglist_no = $('#packinglist_input').val();
                $.ajax({
                    type: 'get',
                    url : $('#detailPackinglistLink').attr('href'),
                    data: {
                        packinglist_no: packinglist_no,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#kk_input').text(response.kk);
                        $('#locator_input').text(response.locator);
                        $('#po_buyer_input').text(response.po_buyer);
                        $('#subcont_input').text(response.subcont);
                        get_data_component_packinglist(response.packinglist);
                        $.unblockUI();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });

            $('#scanModal').on('shown.bs.modal', function () {
                $('#scan_component').focus();
            })

            $('#scan_barcode').on('click', function() {
                var packinglist = $('#packinglist_input').val();
                if(!packinglist){
                    $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                    return false;
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url : $('#dataPackinglistLink').attr('href'),
                    data: {
                        packinglist: packinglist,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#scanModal').modal();
                        $('#packinglist_res').val(packinglist);
                        $('#locator_res').val(response.locator);
                        $('#state_res').val(response.state);
                        $('#scan_component').focus();
                        get_data_component(response.locator, response.state, response.packinglist);
                        $.unblockUI();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });

            $('#scan_component').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var packinglist = $('#packinglist_res').val();
                    var locator = $('#locator_res').val();
                    var state = $('#state_res').val();
                    var barcode_component = $(this).val();

                    if(!packinglist){
                        $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                        return false;
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url : $('#scanComponentLink').attr('href'),
                        data: {
                            packinglist: packinglist,
                            barcode_component: barcode_component,
                        },
                        beforeSend: function() {
                            $('#scanTable').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        success: function(response) {
                            $('#scanTable').unblock();
                            $('#scan_component').val('');
                            $('#scan_component').focus();
                            get_data_component(locator, state, packinglist);
                        },
                        error: function(response) {
                            $('#scanTable').unblock();
                            $('#scan_component').val('');
                            $('#scan_component').focus();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });

            $('#save_component').on('click', function() {
                var packinglist_no = $('#packinglist_res').val();

                if(!packinglist_no){
                    $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url : $('#saveComponentLink').attr('href'),
                    data: {
                        packinglist_no: packinglist_no,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            theme:true,
                            baseZ:2000,
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $.unblockUI();
                        myalert('success','Done');
                        location.reload();
                        // window.open("packinglist-distribusi/print/"+response.id_packinglist, "_blank");
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            })
        });

        function get_data_component(locator, state, packinglist) {
            var table_component = $('#scanTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: $('#dataComponentLink').attr('href'),
                    data: {
                        locator: locator,
                        state: state,
                        packinglist: packinglist                        
                    }
                },
                fnCreatedRow: function (row, data, index) {
                    var info = table_component.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'bundle_id', name: 'bundle_id', searchable:true, visible:true, orderable:false},
                    {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                    {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                    {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                    {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                    {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                    {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false},
                    {data: 'action', name: 'action', searchable:false, orderable:false},
                ],
            });
        }

        function get_data_component_packinglist(packinglist) {
            var table_component = $('#componentTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: $('#dataComponentPackinglistLink').attr('href'),
                    data: {
                        packinglist: packinglist,
                    }
                },
                fnCreatedRow: function (row, data, index) {
                    var info = table_component.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'bundle_id', name: 'bundle_id', searchable:true, visible:true, orderable:false},
                    {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                    {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                    {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                    {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                    {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                    {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false}
                ],
            });

            $('#componentTable').on('click','.ignore-click', function() {
                return false;
            });
        }

        function hapus(url) {
            var packinglist = $('#packinglist_res').val();
            var locator = $('#locator_res').val();
            var state = $('#state_res').val();

            // if(!packinglist){
            //     $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
            //     return false;
            // }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url : url,
                beforeSend: function() {
                    $('#scanTable').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    $("#alert_success").trigger("click",'komponen berhasil di hapus');
                    get_data_component(locator, state, packinglist);
                    console.log(response);
                },
                error: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }

        function validationInputBarcode(barcode_input, locator_id, state, callbackFucntion) {
            if(!locator_id) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            if(!state) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            callbackFucntion(barcode_input, locator_id, state);
        }

    </script>
@endsection