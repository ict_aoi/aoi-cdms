$(document).ready( function () {
    $('.input-new').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();
        
            var input_barcode = $(this).val();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url: '/qty-fabric-check/check-fabric',
                data: {
                    input_barcode: input_barcode,
                },
                beforeSend: function() {
                    $('.panel').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('.panel').unblock();
                    if(response.message == '') {
                        $('#barcode_view').text(response.data.barcode);
                        $('#plan_view').text(response.data.cutting_date);
                        $('#no_roll_view').text(response.data.no_roll);
                        $('#lebar_view').text(response.data.lebar);
                        $('#lot_view').text(response.data.lot);
                        $('#material_view').text(response.data.material);
                        $('#qty_supply_view').text(response.data.qty_supply);
                        $('#qty_actual_view').text(response.data.qty_actual);
                        if(response.data.detail != null) {
                            appendHaveHistory(response.data.detail);
                        } else {
                            appendNotHaveHistory(response.data.detail);
                        }
                        $('#barcode_fabric').val('');
                        $('#barcode_fabric').focus();
                    } else {
                        console.log(response.message);
                        $("#alert_warning").trigger("click",response.message);
                        $('#barcode_fabric').val('');
                        $('#barcode_fabric').focus();
                        $('#lastActivityTable > tbody').html('');
                    }
                },
                error: function(response) {
                    $('#panel').unblock();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }
    });
});

function appendHaveHistory(data) {
    $('#lastActivityTable > tbody').html('');
    $.each(data, function(index, value) {
        $('#lastActivityTable > tbody').append(
            $('<tr>').append(
                $('<td>').text(value.created_spreading)
            ).append(
                $('<td>').text(value.barcode_marker)
            ).append(
                $('<td>').text(value.barcode_table)
            ).append(
                $('<td>').text(value.qty_fabric)
            ).append(
                $('<td>').text(value.actual_sisa)
            )
        );
    });
}

function appendNotHaveHistory(data) {
    $('#lastActivityTable > tbody').html('');
    $('#lastActivityTable > tbody').append(
        $('<tr>').append(
            $('<td>').attr('colspan', '5').append(
                $('<center>').text('Fabric belum pernah digunakan')
            )
        )
    );
}