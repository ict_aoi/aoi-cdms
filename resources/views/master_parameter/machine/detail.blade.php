@extends('layouts.app',['active' => 'machine_details'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Machine Details </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Machine Details</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add Machine Name <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th style="width:10px;">No</th>
                            <th>Nama Mesin</th>
                            <th>Mesin Alias</th>
                            <th>Kode Mesin</th>
                            <th>Type Mesin</th>
                            <th style="width:10px;">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('machine_details.ajaxGetDataMachinedetails') }}" id="get_data"></a>
@endsection

@section('page-modal')
<!-- MODAL ADD -->
    <div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('machine_details.addMachineDetails') }}" id="form-add">
        @csrf
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="panel-body loader-area">
                        <fieldset>
                            <legend class="text-semibold">
                                <i class="icon-file-text2 position-left"></i>
                                <span id="title"> ADD NEW MACHINE NAME</span> <!-- title -->
                            </legend>

                            <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">MACHINE NAME:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="machine_name" id="machine_name" placeholder="e.g: 1 Needle">
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label text-semibold">MACHINE ALIAS:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control text-uppercase" name="machine_alias" id="machine_alias" placeholder="e.g: 1L">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label text-semibold">MACHINE CODE:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control text-uppercase" name="machine_code" id="machine_code" placeholder="e.g: SN">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label text-semibold">Machine Type:</label>
                                <div class="col-lg-9">
                                        <select data-placeholder="Select a Type..." name="machine_type" id="machine_type" class="form-control select-search">
                                            <option value=""></option>
                                            @foreach ($machine_type as $item)  
                                            <option value="{{ $item->id }}">{{ strtoupper($item->machine_type) }}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>                            
                            <hr>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
<!-- END OF MODAL ADD -->

<!-- MODAL EDIT -->
<div id="modal_edit_" class="modal fade" role="dialog">
<div class="modal-dialog">
    <form class="form-horizontal" action="{{ route('machine_details.updateMachineDetails') }}" id="form-update" enctype="multipart/form-data">
        @csrf
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="panel-body loader-area">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> UPDATE MACHINE NAME</span> <!-- title -->
                            </legend>
                            <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>

                        <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">MACHINENAME:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="machine_name_update" id="machine_name_update" placeholder="e.g: 1 Needle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">MACHINE ALIAS:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="machine_alias_update" id="machine_alias_update" placeholder="e.g: 1L">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">MACHINE CODE:</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control text-uppercase" name="machine_code_update" id="machine_code_update" placeholder="e.g: SN">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label text-semibold">MACHINE TYPE:</label>
                            <div class="col-lg-9">
                                <select data-placeholder="Select a Type..." name="machine_type_update" id="machine_type_update" class="form-control select-search">
                                    @foreach ($machine_type as $item)  
                                        <option value=""></option>
                                        <option value="{{ $item->id }}">{{ strtoupper($item->machine_type) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Update <i class="icon-floppy-disk position-right"></i></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </form>
</div>
</div>
<!-- /MODAL EDIT -->
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
    
        var url = $('#get_data').attr('href');
        var table = $('#table-list').DataTable({
            ajax: url,
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false  },
                {data: 'machine_name', name: 'machine_name'},
                {data: 'machine_alias', name: 'machine_alias'},
                {data: 'code', name: 'code'},
                {data: 'machine_type', name: 'machine_type'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
        //end of datatables
    
        //add
        $('.add_new').on('click',function(){
            $('#modal_add_new_').modal('show')
        });
        //end of add
    
        $('#modal_add_new_').on('shown.bs.modal', function(){
            $('input[name=machine_name]').focus();
        });

        $('#modal_edit_').on('shown.bs.modal', function(){
            $('input[name=machine_name_update]').focus();
        });

        $('#modal_edit_').on('hidden.bs.modal', function(){
            $('#form-update').trigger("reset");
            $('#id_update').val(null).trigger('change');
            $('#machine_type_update').val('');
            $('#machine_name_update').val('');
            $('#machine_alias_update').val('');
            $('#machine_code_update').val('');
            table.draw();
        });
    
        //delete
        $("#table-list").on("click", ".delete", function() {
            event.preventDefault();
            var id = $(this).data('id');
            if(id == 'kosong') {
                return false;
            }
            var token = $(this).data("token");
            bootbox.confirm("Are you sure delete this row ?", function (result) {
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url: "master-machine-detail/delete/"+id,
                        type: "GET",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        beforeSend: function () {
                            $('.loader-area').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        },
                        complete: function () {
                            $(".loader-area").unblock();
                        },
                        success: function () {
                            myalert('success','Data has been deleted');
                            table.ajax.reload();
                        }
                    });
                }
            });
        });
        //end of delete
    
        //add new
        $('#form-add').submit(function(event) {
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : "POST",
                url : $('#form-add').attr('action'),
                data: $('#form-add').serialize(),
                beforeSend: function () {
                    $('.loader-area').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                complete: function () {
                    $('.loader-area').unblock();
                },
                success: function(response) {
                    myalert('success','SUCCESS');
                    $('#form-add').trigger("reset");
                    $('#machine_type').val(null).trigger('change');
                    $('#modal_add_new_').trigger('toggle');
                    table.ajax.reload();
                },
                error: function(response) {
                    myalert('error','INSERT FAILED');
                }
            })
        });

        //update
    $('#form-update').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'machine_name_update', 'machine_type_update','machine_code_update','machine_alias_update'];
        var val = [
            $('#id_update').val(), 
            $('#machine_name_update').val(),
            $('#machine_type_update').val(),
            $('#machine_code_update').val(),
            $('#machine_alias_update').val(),
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {

        var machine_name = response.machine_name;
        var machine_code = response.code;
        var machine_alias = response.machine_alias;
        var type_id = response.type_id;
        var machine_type = response.machine_type;
        var id = response.id;
        $.unblockUI();
        $('#id_update').val(id);
        $('#machine_name_update').val(machine_name);
        $("#machine_type_update option:selected").val(type_id);
        //$('#machine_type_update').append("<option value='"+type_id+"' selected>"+machine_type+"</option>");
        $('#machine_code_update').val(machine_code);
        $('#machine_alias_update').val(machine_alias);
        $('#modal_edit_').modal();
        console.log(response);
    });
}

</script>
@endsection




