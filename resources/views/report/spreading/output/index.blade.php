@extends('layouts.app',['active' => 'report_output_spreading'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Laporan Output Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Laporan Output Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">By Date</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">RESUME PER DAY</a></li>
				<li class=""><a href="#basic-tab3" data-toggle="tab" class="legitRipple" aria-expanded="true">DETAIL PER DAY</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadOutput') }}" id="form_filter_date" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Spreading</label>
								<div class="col-lg-10">

									@csrf

									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="cutting_date" placeholder="Masukkan Tanggal Spreading" id="cutting_date" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadSpreadingdaytable') }}" id="filter_date" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Spreading</label>
								<div class="col-lg-10">
									@csrf
									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="spreading_date" placeholder="Masukkan Tanggal Spreading" id="spreading_date">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab3">
					<form class="form-horizontal" action="{{ route('reportSpreading.downloadSpreadingdetailtable') }}" id="filter_date_" method="POST">
						<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Spreading</label>
								<div class="col-lg-10">
									@csrf
									<div class="input-group">
										<span class="input-group-btn">
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="spreading_date_x" placeholder="Masukkan Tanggal Spreading" id="spreading_date_x">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
