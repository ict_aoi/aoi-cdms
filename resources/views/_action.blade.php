<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($upload))
                <li><a data-value="{!! $upload !!}" class="ignore-click upload"><i class="icon-upload4"></i> Add New</a></li>
            @endif
            @if (isset($upload_komponen))
                <li><a data-value="{!! $upload_komponen !!}" data-seasonid="{!! $seasonid !!}" data-seasonname="{!! $seasonname !!}" data-types="{!! $type_name !!}" data-typeid="{!! $type_id !!}" class="ignore-click upload"><i class="icon-upload4"></i> Add New</a></li>
            @endif
            @if (isset($upload_sync))
                <li><a data-value="{!! $upload_sync !!}" class="ignore-click upload_sync"><i class="icon-spinner4"></i> Sync</a></li>
            @endif
            @if (isset($edit))
               <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($deletes))
                <li><a href="{{ $deletes }}" data-id="{{ isset($id) ? $id : 'kosong' }}" class="ignore-click delete"><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($edits))
                <li><a data-value="{!! $edits !!}" class="ignore-click edits"><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($edit_modal))
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($delete_modal))
                <li><a href="#" onclick="deleteDetail('{!! $delete_modal !!}')" ><i class="icon-close2"></i> Delete</a></li>
            @endif
            @if (isset($delete_style))
                <li><a href="{{ $delete_style }}" data-id="{{ isset($id) ? $id : 'kosong' }}" data-style="{{ isset($style) ? $style : 'kosong' }}" data-komponen="{{ isset($komponen) ? $komponen : 'kosong' }}" data-type="{{ isset($type_id) ? $type_id : 'kosong' }}" data-seasonid="{{ isset($seasonid) ? $seasonid : 'kosong' }}" class="ignore-click deleteStyle"><i class="icon-close2"></i> Delete</a></li>
            @endif

        </ul>
    </li>
</ul>