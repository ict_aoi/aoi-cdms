<?php

namespace App\Http\Controllers\DistribusiArtwork;

use DB;
use Uuid;
use StdClass;
use Auth;
use App\Models\User;
use App\Models\PackingSubTemp;
use App\Models\PackinglistDistribusi;
use App\Models\DetailPackinglistDistribusi;
use App\Models\DistribusiMovement;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class DistribusiArtworkinController extends Controller
{
    public function index()
    {
        $packinglists = DB::table('packinglist_distribusi')
        ->whereNull('deleted_at')
        ->where('factory_id', \Auth::user()->factory_id)->where('status', 'release')
        ->where('is_completed',false)
        ->get();
        $user_name = DB::table('users')->where('id',\Auth::user()->id)->first();
        return view('distribusi.distribusi_subcont.scan_in.index',compact('packinglists','user_name'));
    }

    public function dataComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $locator = $request->locator;
            $state = $request->state;

            if($packinglist != null && $locator != null && $state != null) {
                $data = DB::table('detail_packinglist_subtemp')
                ->where('id_packinglist',$packinglist)
                ->where('factory_id',\Auth::user()->factory_id)
                ->where('user_id',\Auth::user()->id)
                ->orderBy('size','asc')
                ->orderBy('cut_num','asc')
                ->orderBy('start_no','asc')
                ->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->addColumn('action', function($data) {
                    return view('distribusi.distribusi_subcont.scan_in._action', [
                        'model'      => $data,
                        'hapus'     => route('distribusi_artworkin.deleteComponent', $data->bundle_id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataComponentPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;

            if($packinglist != null) {
                $data = DB::table('detail_packinglist_subtemp')
                ->where('id_packinglist',$packinglist)
                ->where('factory_id',\Auth::user()->factory_id)
                ->where('user_id',\Auth::user()->id)
                ->orderBy('size','asc')
                ->orderBy('cut_num','asc')
                ->orderBy('start_no','asc')
                ->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == null || $data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $data = DB::table('packinglist_distribusi')
            ->select('packinglist_distribusi.*', 'master_locator.locator_name')
            ->leftJoin('master_locator', 'packinglist_distribusi.process', '=', 'master_locator.id')
            ->where('packinglist_distribusi.no_packinglist', $packinglist)
            ->where('packinglist_distribusi.deleted_at', null)
            ->first();
            
            $po_buyer = implode(', ', explode(',', $data->po_buyer));

            $obj = new StdClass();
            $obj->packinglist = $data->no_packinglist;
            $obj->kk = $data->no_kk;
            $obj->locator = $data->locator_name;
            $obj->po_buyer = $po_buyer;
            $obj->subcont = $data->subcont;
            
            return response()->json($obj,200);
        }
    }

    public function dataPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $user_access = DB::table('detail_packinglist_subtemp')->where('id_packinglist',$packinglist)->first();
            if($user_access != null){
                $user_create = User::where('id',$user_access->user_id)->first();
                if(Auth::user()->id != $user_access->user_id){
                    return response()->json('PACKINGLIST MASIH DI PROSES OLEH '.$user_create->name,422);
                }
            }
            $polibag = $request->polibag;
            $data = DB::table('packinglist_distribusi')->where('no_packinglist', $packinglist)->whereNull('deleted_at')->first();

            $obj = new StdClass();
            $obj->packinglist = $packinglist;
            $obj->polibag = $polibag;
            $obj->locator = $data->process;
            $obj->state = 'in';
            
            return response()->json($obj,200);
        }
    }
    
    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $state = 'in';
            $locator = '3';
            $barcode_component = $request->barcode_component;

            if($packinglist != null && $state != null && $barcode_component != null && $locator != null)
            {
                $bundle_detail = DB::table('bundle_detail')->where('barcode_id',$barcode_component)->first();
                $bundle_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                $barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                $barcode_sds_check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();
                
                    if($bundle_detail != null && $bundle_detail->barcode_id == $barcode_component){
                        $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->where('locator_to',10)->where('status_to','out')->first();
                        if($out_cutting != null)
                        {
                            $part_num = DB::table('master_style_detail')->where('id',$bundle_detail->style_detail_id)->first();
                            $type_name = DB::table('types')->where('id',$part_num->type_id)->first();
                            $bundle_header = DB::table('bundle_header')->where('id',$bundle_detail->bundle_header_id)->first();
                            $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$bundle_detail->sds_barcode)->first();
                            if($barcode_sds->artwork != null){
                                return response()->json('Bundle Sudah Scan di SDS!',422);
                            }
                            $last_movements = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->orderBy('created_at', 'DESC')->first();
                            $locator = DB::table('master_locator')->where('id',$last_movements->locator_to)->first();
        
                            if($last_movements->status_to == 'in'){
                                return response()->json('Harap Scan Out '.$locator->locator_name.' Dahulu!',422);
                            }
                            $get_process = explode(',', $part_num->process);

                            if($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }
        
                            $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                            if(!in_array(3, $get_locator_process)){
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }

                            $style = $bundle_header->style;

                            $po_packinglist = DB::table('packinglist_distribusi')
                            ->where('no_packinglist',$packinglist)
                            ->where('style',$style)
                            ->where('set_type',$type_name->type_name)
                            ->whereNull('deleted_at')
                            ->first();

                            if($po_packinglist == null){
                                return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                            }else{
                                $po_packinglist_header = explode(',',$po_packinglist->po_buyer);
                                if(in_array($bundle_header->poreference,$po_packinglist_header) == false){
                                    return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                                }
                            }


                            $component_check = DistribusiMovement::where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                            $current_scan = PackingSubTemp::where('bundle_id',$barcode_component)->first();
            
                            if($current_scan == null){
                                if($component_check != null && $component_check->description == 'in ARTWORK'){
                                    return response()->json('Barcode sudah melalui scan In Artwork!',422);
                                }else{
                                    PackingSubTemp::firstOrCreate([
                                        'id_packinglist' => $packinglist,
                                        'bundle_id' => $barcode_component,
                                        'qty' => $bundle_detail->qty,
                                        'size'  => $bundle_header->size,
                                        'komponen_name' => trim($bundle_detail->komponen_name),
                                        'user_id' => \Auth::user()->id,
                                        'factory_id' => \Auth::user()->factory_id,
                                        'poreference'=> $bundle_header->poreference,
                                        'article'   => $bundle_header->article,
                                        'cut_num'   => $bundle_header->cut_num,
                                        'start_no'  => $bundle_detail->start_no,
                                        'end_no'    => $bundle_detail->end_no,
                                        'style'     => $bundle_header->style,
                                        'set_type'  => $bundle_header->set_type,
                                        'color_name'    => $bundle_header->color_name,
                                        'season'        => $bundle_header->season
                                    ]);

                                    DB::connection('sds_live')->table('temp_scan_proc')->insert([
                                        'ap_id'         => $bundle_detail->sds_barcode,
                                        'proc'          => 'ART',
                                        'user_id'       => \Auth::user()->nik,
                                        'scan_time'     => Carbon::now(),
                                        'check_out'     => false,
                                        'factory'       => \Auth::user()->factory_id
                                    ]);
                                }
                            }else{
                                return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                            }
                        }else{
                            return response()->json('Bundle Belum Scan Out Cutting!',422);
                        }
                    }elseif($bundle_detail == null && $barcode_sds_check != null && $bundle_sds_cdms != null && $barcode_component == $bundle_sds_cdms->sds_barcode){
                        $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$bundle_sds_cdms->barcode_id)->where('locator_to',10)->where('status_to','out')->first();
                        if($out_cutting != null)
                        {
                            $barcode_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                            $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_detail_id)->first();
                            $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                            $bundle_header_sds = DB::table('bundle_header')->where('id',$barcode_sds_cdms->bundle_header_id)->first();
                            $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                            if($barcode_sds->artwork != null){
                                return response()->json('Bundle Sudah Scan di SDS!',422);
                            }
                            $last_movements = DB::table('distribusi_movements')->where('barcode_id',$bundle_sds_cdms->barcode_id)->orderBy('created_at','DESC')->first();
                            $locator = DB::table('master_locator')->where('id',$last_movements->locator_to)->first();
        
                            if($last_movements->status_to == 'in'){
                                return response()->json('Harap Scan Out '.$locator->locator_name.' Dahulu!',422);
                            }

                            $get_process = explode(',', $part_num_sds->process);

                            if($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }
        
                            $get_locator_process = DB::table('master_process')
                            ->select('locator_id')
                            ->whereIn('id', $get_process)
                            ->groupBy('locator_id')
                            ->pluck('locator_id')->toArray();

                            if(!in_array(3, $get_locator_process)){
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }

                            if($bundle_header_sds->set_type == 'Non'){
                                $style = $bundle_header_sds->style;
                            }else{
                                $style = $bundle_header_sds->style;
                            }

                            $po_packinglist = DB::table('packinglist_distribusi')
                            ->where('no_packinglist',$packinglist)
                            ->where('style',$style)
                            ->whereNull('deleted_at')
                            ->where('set_type',$type_name_sds->type_name)
                            ->first();

                            
                            if($po_packinglist == null){
                                return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                            }else{
                                $po_packinglist_header = explode(',',$po_packinglist->po_buyer);
                                if(in_array($bundle_header_sds->poreference,$po_packinglist_header) == false){
                                    return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                                }
                            }
                            

                            $component_check = DistribusiMovement::where('barcode_id', $bundle_sds_cdms->barcode_id)->orderBy('created_at','desc')->first();
                            $current_scan = PackingSubTemp::where('bundle_id',$bundle_sds_cdms->barcode_id)->first();

                            $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                            if(!$temp_scan_proc){
                                if($component_check != null && $component_check->description == 'in ARTWORK'){
                                    return response()->json('Barcode sudah melalui scan In Artwork!',422);
                                }else{
                                    PackingSubTemp::firstOrCreate([
                                        'id_packinglist' => $packinglist,
                                        'bundle_id' => $bundle_sds_cdms->barcode_id,
                                        'qty' => $bundle_sds_cdms->qty,
                                        'size'  => $bundle_header_sds->size,
                                        'komponen_name' => trim($bundle_sds_cdms->komponen_name),
                                        'user_id' => \Auth::user()->id,
                                        'factory_id' => \Auth::user()->factory_id,
                                        'poreference'=> $bundle_header_sds->poreference,
                                        'article'   => $bundle_header_sds->article,
                                        'cut_num'   => $bundle_header_sds->cut_num,
                                        'start_no'  => $bundle_sds_cdms->start_no,
                                        'end_no'    => $bundle_sds_cdms->end_no,
                                        'style'     => $bundle_header_sds->style,
                                        'set_type'  => $bundle_header_sds->set_type,
                                        'color_name'    => $bundle_header_sds->color_name,
                                        'season'        => $bundle_header_sds->season
                                    ]);

                                    DB::connection('sds_live')->table('temp_scan_proc')->insert([
                                        'ap_id'         => $barcode_component,
                                        'proc'          => 'ART',
                                        'user_id'       => \Auth::user()->nik,
                                        'scan_time'     => Carbon::now(),
                                        'check_out'     => false,
                                        'factory'       => \Auth::user()->factory_id
                                    ]);
                                }
                            }else{
                                return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                            }
                        }else{
                            return response()->json('Bundle Belum Scan Out Cutting!',422);
                        }
                    }elseif($bundle_sds_cdms == null  && $barcode_sds_only != null)
                    {
                        $out_cutting = DB::table('distribusi_movements')
                        ->where('barcode_id',$barcode_component)
                        ->where('locator_to',10)->where('status_to','out')->first();
                        if($out_cutting != null){
                            $barcode_sds_cdms = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                            $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_id)->first();
                            $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                            $bundle_header_sds = DB::table('sds_header_movements')->where('id',$barcode_sds_cdms->sds_header_id)->first();
                            $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                            if($barcode_sds->artwork != null){
                                return response()->json('Bundle Sudah Scan di SDS!',422);
                            }
                            $last_movements = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->orderBy('created_at','DESC')->first();
                            $locator = DB::table('master_locator')->where('id',$last_movements->locator_to)->first();
        
                            if($last_movements->status_to == 'in'){
                                return response()->json('Harap Scan Out '.$locator->locator_name.' Dahulu!',422);
                            }

                            $get_process = explode(',', $part_num_sds->process);

                            if($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }
        
                            $get_locator_process = DB::table('master_process')
                            ->select('locator_id')
                            ->whereIn('id', $get_process)
                            ->groupBy('locator_id')
                            ->pluck('locator_id')->toArray();

                            if(!in_array(3, $get_locator_process)){
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }

                            if($bundle_header_sds->set_type == 'Non'){
                                $style = $bundle_header_sds->style;
                            }else{
                                $style = $bundle_header_sds->style;
                            }

                            $po_packinglist = DB::table('packinglist_distribusi')
                            ->where('no_packinglist',$packinglist)
                            ->where('style',$style)
                            ->whereNull('deleted_at')
                            ->where('set_type',$type_name_sds->type_name)
                            ->first();

                            if($po_packinglist == null){
                                return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                            }else{
                                $po_packinglist_header = explode(',',$po_packinglist->po_buyer);
                                if(in_array($bundle_header_sds->poreference,$po_packinglist_header) == false){
                                    return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                                }
                            }
                            

                            $component_check = DistribusiMovement::where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                            $current_scan = PackingSubTemp::where('bundle_id',$barcode_component)->first();

                            $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                            if(!$temp_scan_proc){
                                if($component_check != null && $component_check->description == 'in ARTWORK'){
                                    return response()->json('Barcode sudah melalui scan In Artwork!',422);
                                }else{
                                    PackingSubTemp::firstOrCreate([
                                        'id_packinglist' => $packinglist,
                                        'bundle_id'     => $barcode_component,
                                        'qty'           => $barcode_sds_cdms->qty,
                                        'size'          => $bundle_header_sds->size,
                                        'komponen_name' => trim($barcode_sds_cdms->komponen_name),
                                        'user_id'       => \Auth::user()->id,
                                        'factory_id'    => \Auth::user()->factory_id,
                                        'poreference'   => $bundle_header_sds->poreference,
                                        'article'       => $bundle_header_sds->article,
                                        'cut_num'       => $bundle_header_sds->cut_num,
                                        'start_no'      => $barcode_sds_cdms->start_no,
                                        'end_no'        => $barcode_sds_cdms->end_no,
                                        'style'         => $bundle_header_sds->style,
                                        'set_type'      => $bundle_header_sds->set_type,
                                        'color_name'    => $bundle_header_sds->color_name,
                                        'season'        => $bundle_header_sds->season
                                    ]);

                                    DB::connection('sds_live')->table('temp_scan_proc')->insert([
                                        'ap_id'         => $barcode_component,
                                        'proc'          => 'ART',
                                        'user_id'       => \Auth::user()->nik,
                                        'scan_time'     => Carbon::now(),
                                        'check_out'     => false,
                                        'factory'       => \Auth::user()->factory_id
                                    ]);
                                }
                            }else{
                                return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                            }
                        }else{
                            return response()->json('Bundle Belum Scan Out Cutting!',422);
                        }
                    }else{
                        return response()->json('Barcode Tidak Ditemukan!',422);
                    }
            }else{
                return response()->json('Harap refresh Halaman Ini!',422);
            }
        }
    }

    public function deleteComponent(Request $request, $id)
    {
        if(request()->ajax())
        {
            if($id != null) {
                $state = 'in';
                    $data = PackingSubTemp::where('bundle_id', $id)->first();
                    $packinglist = DB::table('packinglist_distribusi')->where('no_packinglist', $data->id_packinglist)->whereNull('deleted_at')->first();
                    PackingSubTemp::where('bundle_id', $id)->delete();
                    $barcode_sds_cdms = DB::table('bundle_detail')->where('barcode_id',$id)->first();
                    if($barcode_sds_cdms != null){
                        DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds_cdms->sds_barcode)->update([
                            'artwork'       => null,
                            'artwork_pic'   => null,
                            'packing_list_number' => null
                        ]);
                        DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_sds_cdms->sds_barcode)->delete();
                    }else{
                        DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$id)->update([
                            'artwork'       => null,
                            'artwork_pic'   => null,
                            'packing_list_number' => null
                        ]);
                        DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$id)->delete();
                    }

                    $obj = new StdClass();
                    $obj->packinglist = $packinglist->no_packinglist;
                    $obj->locator = $packinglist->process;
                    $obj->state = $state;

                    return response()->json($obj,200);
            } else {
                return response()->json('Terjadi Kesalahan Silahkan Refresh Halaman Ini!',422);
            }
        }
    }

    public function saveComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $state = 'in';
            if($packinglist != null) {
                $data = PackingSubTemp::where('id_packinglist', $packinglist)->get();
                $count_size_temp = PackingSubTemp::select('size')->where('id_packinglist', $packinglist)->groupBy('size')->pluck('size')->toArray();
                $count_size_created = DB::table('detail_packinglist_distribusi as dpd')
                ->leftJoin('packinglist_distribusi as pd','pd.no_packinglist','=','dpd.id_packinglist')
                ->leftJoin('bundle_detail as bd','bd.barcode_id','=','dpd.bundle_id')
                ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                ->select('bh.size')
                ->where('dpd.id_packinglist', $packinglist)
                ->groupBy('bh.size')
                ->pluck('bh.size')
                ->toArray();

                $output = array_unique(array_merge($count_size_temp, $count_size_created));
                $x = count($output);

                if($x > 6){
                    return response()->json('Data Size Melebihi Kolom Yang Tersedia!',422);
                }
                if(count($data) > 0) {
                    try {
                        DB::beginTransaction();
                        $packinglist_data = DB::table('packinglist_distribusi')->where('no_packinglist', $packinglist)->whereNull('deleted_at')->first();
                        $locator = DB::table('master_locator')->where('id', $packinglist_data->process)->first();
                        foreach($data as $aa) {
                            DB::table('detail_packinglist_distribusi')->insert([
                                'id'                    => Uuid::generate()->string,
                                'id_packinglist'        => $aa->id_packinglist,
                                'bundle_id'             => $aa->bundle_id,
                                'qty'                   => $aa->qty,
                                'created_at'            => Carbon::now(),
                                'user_id'               => $aa->user_id,
                                'factory_id'            => $aa->factory_id,
                                'style'                 => $aa->style,
                                'article'               => $aa->article,
                                'poreference'           => $aa->poreference,
                                'komponen_name'         => $aa->komponen_name,
                                'color_name'            => $aa->color_name,
                                'set_type'              => $aa->set_type,
                                'size'                  => $aa->size,
                                'season'                => $aa->season
                            ]);
                        }
                        foreach($data as $x){
                            $last_locator = DistribusiMovement::where('barcode_id',$x->bundle_id)->orderBy('created_at','desc')->first();
                            $last_locator_to = $last_locator->locator_to == null ? null : $last_locator->locator_to;
                            $last_status_to = $last_locator->status_to == null ? null : $last_locator->status_to;
                            DistribusiMovement::FirstOrCreate([
                                'barcode_id'    => $x->bundle_id,
                                'locator_from'  => $last_locator_to,
                                'status_from'   => $last_status_to,
                                'locator_to'    => $locator->id,
                                'status_to'     => $state,
                                'description'   => $state.' '.'ARTWORK',
                                'user_id'       => \Auth::user()->id,
                                'ip_address'    => $this->getIPClient(),
                            ]);

                            $barcode_sds = DB::table('bundle_detail')->where('barcode_id',$x->bundle_id)->first();
                            if($barcode_sds == null){
                                DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$x->bundle_id)->update([
                                    'location'              => 'ART',
                                    'factory'               => 'AOI '.\Auth::user()->factory_id,
                                    'packing_list_number'   => $packinglist,
                                    'artwork'               => Carbon::now(),
                                    'artwork_pic'           => \Auth::user()->nik
                                ]);
                                DB::table('sds_detail_movements')->where('sds_barcode',$x->bundle_id)->update([
                                    'current_location_id'    => $locator->id,
                                    'current_status_to'     => $state,
                                    'current_description'   => $state.' '.$locator->locator_name,
                                    'updated_movement_at'   => Carbon::now(),
                                    'current_user_id'       => \Auth::user()->id,
                                ]);
                                DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$x->bundle_id)->delete();
                            }else{
                                DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->update([
                                    'location'              => 'ART',
                                    'factory'               => 'AOI '.\Auth::user()->factory_id,
                                    'packing_list_number'   => $packinglist,
                                    'artwork'               => Carbon::now(),
                                    'artwork_pic'           => \Auth::user()->nik
                                ]);
                                DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                    'current_locator_id'    => $locator->id,
                                    'current_status_to'     => $state,
                                    'current_description'   => $state.' '.$locator->locator_name,
                                    'updated_movement_at'   => Carbon::now(),
                                    'current_user_id'       => \Auth::user()->id,
                                ]);
                                DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_sds->sds_barcode)->delete();
                            }
                        }

                        //SUMMARY QTY AFTER SCAN
                        $quota_temp = DB::table('detail_packinglist_subtemp')
                        ->where('id_packinglist',$packinglist_data->no_packinglist)
                        ->sum('qty');
                        
                        //CHECK QUOTA FILED BEFORE
                        $quota_filled_check = DB::table('doc_kk_ppc_header')
                        ->where('style',$packinglist_data->style)
                        ->where('set_type',$packinglist_data->set_type)
                        ->where('season',$packinglist_data->season)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->where('balanced_quota','>',0)
                        ->where(function($query) use ($packinglist_data){
                            $query->where('kk_no',$packinglist_data->no_kk)
                            ->orWhere('kk_no',strtolower($packinglist_data->no_kk));
                        })
                        ->first();

                        $booking_quota = $quota_filled_check->quota_used + $quota_temp;

                        if($quota_filled_check == null){
                            return response()->json('Quota Anda Tidak Mencukupi!#ERR01',422);
                        }else{
                            $quota_get = $quota_filled_check->quota_used > 0 ? $quota_filled_check->quota_used + $quota_temp : $quota_temp;
                            if($quota_get / $quota_filled_check->total_qty > 1){
                                return response()->json('Quota Anda Tidak Mencukupi!#ERR02',422);
                            }
                        }
                        //FILED IN QUOTA USED WHILE SAVING DATA SCAN-IN ARTWORK ON DOC PPC HEADER
                        DB::table('doc_kk_ppc_header')
                        ->where('style',$packinglist_data->style)
                        ->where('set_type',$packinglist_data->set_type)
                        ->where('season',$packinglist_data->season)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->where('balanced_quota','>',0)
                        ->where(function($query) use ($packinglist_data){
                            $query->where('kk_no',$packinglist_data->no_kk)
                            ->orWhere('kk_no',strtolower($packinglist_data->no_kk));
                        })
                        ->update([
                            'quota_used'        => $quota_get,
                            'balanced_quota'    => $quota_filled_check->total_qty - $quota_get
                        ]);

                        PackingSubTemp::where('id_packinglist', $packinglist)->delete();
                        $obj = new StdClass();
                        $obj->packinglist = $packinglist;
                        $obj->id_packinglist = $packinglist_data->id;
                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($obj,200);
                } else {
                    return response()->json('Harap Refresh Halaman Ini!',422);
                }
            } else {
                return response()->json('Harap Refresh Halaman Ini!',422);
            }
        }
    }

    private function getIPClient()
    {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
