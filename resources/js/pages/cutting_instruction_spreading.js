$(document).ready(function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/cutting-instruction-spreading/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po', name: 'po',searchable:true},
            {data: 'size_category', name: 'size_category'},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/cutting-instruction-spreading/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
    });
});

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#cutting_date_detail').val(response.cutting_date);
            $('#style_detail').val(response.style);
            $('#articleno_detail').val(response.articleno);
            $('#po_buyer_detail').val(JSON.stringify(response.po_buyer));
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#po_buyer_detail_view').text(response.po_buyer.join(', '));
            $('#detailModal').modal();
            get_data_modal_table();
        }
    });
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        scrollX: true,
        destroy: true,
        ajax: {
            url: '/cutting-instruction-spreading/data-detail',
            data: {
                cutting_date: $('#cutting_date_detail').val(),
                style: $('#style_detail').val(),
                articleno: $('#articleno_detail').val(),
                po_buyer: $('#po_buyer_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        order: [
            [ 2, 'asc' ]
        ],
        columns: [
            {data: 'barcode', name: 'barcode', searchable:true, visible:true, orderable:false},
            {data: 'nomor_roll', name: 'nomor_roll', searchable:true, visible:true, orderable:false},
            {data: 'document_no', name: 'document_no', searchable:true, visible:true, orderable:false},
            {data: 'item_code', name: 'item_code', searchable:true, visible:true, orderable:true},
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'actual_lot', name: 'actual_lot', searchable:true, visible:true, orderable:false},
            {data: 'actual_width', name: 'actual_width', searchable:true, visible:true, orderable:false},
            {data: 'qty_prepared', name: 'qty_prepared', searchable:true, visible:true, orderable:false},
            {data: 'qty_sisa', name: 'qty_sisa', searchable:true, visible:true, orderable:false},
            {data: 'action', name: 'action', searchable:true, visible:true, orderable:false},
        ],
    });

    return table1;
}
