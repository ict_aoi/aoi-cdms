<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;

use App\Models\MasterSpreading;
use App\Models\TableSpreading;

class MasterController extends Controller
{
   public function index(){
         return view('master_parameter/spreading');
   }

   public function getMasterSpreading(){
      $data = MasterSpreading::whereNull('deleted_at')
                    ->get();
      return DataTables::of($data)
                ->addColumn('action',function($data){

                  if (is_null($data->deleted_at)) {
                    $btn = '<button class="btn btn-danger btn-delete"  id="btn-delete" data-id="'.$data->id.'" onclick="deletepar(this);"><span class="icon-trash"></span></button>';
                  }else{
                    $btn = '';
                  }

                  return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
   }

   public function inSpreading(Request $request){
      $type = $request->type;
      $target =$request->target;


      $cek = MasterSpreading::where('type',$type)->whereNull('deleted_at')->first();
      if (is_null($cek)) {
        try {
          DB::begintransaction();
          $dtin = array('type'=>$type,'target'=>$target,'created_at'=>Carbon::now());
          MasterSpreading::FirstOrCreate($dtin);
          $data_response = [
                              'status' => 200,
                              'output' => 'Insert Success !!!'
                            ];
          DB::commit();
        } catch (Exception $ex) {
          db::rollback();
            $message = $ex->getMessage();
                    \ErrorHandler($message);

              $data_response = [
                              'status' => 422,
                              'output' => 'Insert Field !!!'
                            ];
        }
      }else{
          $data_response = [
                              'status' => 422,
                              'output' =>  'type spreading is already  !!!'
                            ];
      }


      return response()->json(['data' => $data_response]);

   }

   public function inactiveSpreading(Request $request){
      $dtid = MasterSpreading::where('id',$request->id)->first();

      if (is_null($dtid->deleted_at)) {
          try {
              db::begintransaction();
                MasterSpreading::where('id',$request->id)->update(['deleted_at'=>Carbon::now()]);
                $data_response = [
                            'status' => 200,
                            'output' => 'Inactive Success !!!'
                          ];
              db::commit();
          } catch (Exception $ex) {
              db::rollback();
              $message = $ex->getMessage();
                  \ErrorHandler($message);
                  $data_response = [
                            'status' => 422,
                            'output' => 'Inactive field !!!'
                          ];
          }
      }else{
        $data_response = [
                            'status' => 422,
                            'output' => 'Inactive field !!!'
                          ];
      }

      return response()->json(['data' => $data_response]);
   }

   public function masterSetTable(){
      $table = DB::table('master_table')->whereNull('deleted_at')->orderBy('id_table')->get();
      $spreading = MasterSpreading::whereNull('deleted_at')->get();
         return view('master_parameter/setting_table')->with('mtable',$table)
                                              ->with('mspreading',$spreading);
   }


   public function getTableSpreading(Request $request){

      $date = Carbon::parse($request->fdates)->format('Y-m-d');
      if ($date!=null) {
       $data = DB::table('master_table')
                    ->join('table_spreading','master_table.id_table','=','table_spreading.id_table')
                    ->join('master_spreading','table_spreading.id_spreading','=','master_spreading.id')
                    ->whereDate('table_spreading.date_spreading',$date)
                    ->whereNull('master_table.deleted_at')
                    ->whereNull('table_spreading.deleted_at')
                    ->whereNull('master_spreading.deleted_at')
                    ->where('table_spreading.factory_id',\Auth::user()->factory_id)
                    ->orderBy('table_spreading.shift','asc')
                    ->orderBy('master_table.table_name','asc')
                    ->select('master_table.table_name','master_spreading.type','master_spreading.target','table_spreading.work_hours','table_spreading.shift','table_spreading.id');
      }else{
        $data = DB::table('master_table')
                    ->join('table_spreading','master_table.id_table','=','table_spreading.id_table')
                    ->join('master_spreading','table_spreading.id_spreading','=','master_spreading.id')
                    ->whereDate('table_spreading.date_spreading',Carbon::now())
                    ->whereNull('master_table.deleted_at')
                    ->whereNull('table_spreading.deleted_at')
                    ->whereNull('master_spreading.deleted_at')
                    ->where('table_spreading.factory_id',\Auth::user()->factory_id)
                    ->orderBy('table_spreading.shift','asc')
                    ->orderBy('master_table.table_name','asc')
                    ->select('master_table.table_name','master_spreading.type','master_spreading.target','table_spreading.work_hours','table_spreading.shift','table_spreading.id');
      }

      return DataTables::of($data)
                // ->editColumn('work_hours',function($data){
                //     return '<div id="work_'.$data->id.'">
                //                      <input type="text"
                //                          data-id="'.$data->id.'"
                //                          value="'.$data->work_hours.'"
                //                          class="form-control work_unfilled" >
                //                      </input>
                //                  </div>';
                // })
                // ->rawColumns(['work_hours'])
                ->make(true);
   }

   public function insertTableSpreading(Request $request){
      $table = $request->tabel;
      $spreading = $request->spreading;
      $work_hours = $request->working;
      $shift = $request->shift;
      $dates = Carbon::parse($request->dates)->format('Y-m-d');


        $getDT=TableSpreading::where('id_table',$table)->where('factory_id',\Auth::user()->factory_id)->where('shift',$shift)->whereDate('date_spreading',$dates)->whereNull('deleted_at')->first();

        if (is_null($getDT)) {
            try {
                db::begintransaction();
                $dtin =  array(
                    'id_table'       => $table,
                    'shift'          => $shift,
                    'id_spreading'   => $spreading,
                    'work_hours'     => $work_hours,
                    'date_spreading' => $dates,
                    'factory_id'     => \Auth::user()->factory_id,
                );
                TableSpreading::FirstOrCreate($dtin);
                db::commit();
                $data_response = [
                    'status' => 200,
                    'output' => 'Insert Table Spreading Success !!!'
                ];
            } catch (Exception $ex) {
                db::rollback();
                $message = $ex->getMessage();
                \ErrorHandler($message);
                $data_response = [
                    'status' => 422,
                    'output' => 'Insert Table Spreading Field !!!'
                ];
            }
        }else{
            $data_response = [
                            'status' => 422,
                            'output' => 'Table Spreading Already On !!!'
                          ];
        }
         return response()->json(['data'=>$data_response]);
   }

   public function delTableSpreading(Request $request){
        $id = $request->id;
        try {
          db::begintransaction();
              TableSpreading::where('id',$id)->update(['deleted_at'=>Carbon::now()]);
              $data_response = [
                            'status' => 200,
                            'output' => 'Delete Table Spreading Success !!!'
                          ];
          db::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Delete Table Spreading Field !!!'
                          ];

        }

        return response()->json(['data'=>$data_response]);
   }

   public function masterTable(){
      $id_table = $this->idtable();

      return view('master_parameter/table')->with('id_table',$id_table);
   }

   public function getTable(){
        $data = DB::table('master_table')
                      ->whereNull('deleted_at')
                      ->orderBy('id_table','asc');
        return DataTables::of($data)
                        // ->addColumn('checkbox',function($data){
                        //     return '<input type="checkbox" class="setmj" name="selector[]" id="Inputselector" value="'.$data->id_table.'" >';
                        // })
                        ->addColumn('action',function($data){
                              return '<button class="btn btn-danger btn-delete"  id="btn-delete" data-id="'.$data->id_table.'" onclick="delTab(this);"><span class="icon-trash"></span></button>';
                        })
                        ->rawColumns(['action'])
                        ->make(true);
   }

   private function idtable(){
      $maxid= DB::table('master_table')
                            ->max('id_table');
        $subid = (int)substr($maxid, 3,4);

        $subid++;
        $newid = 'MJ-'.sprintf("%04s",$subid);
        return $newid;
   }

   public function newTable(Request $request){
      $id_table = $request->id_table;
      $table_name = $request->name;


      try {
        db::begintransaction();
            $dtin=array('id_table'=>$id_table,'table_name'=>$table_name,'created_at'=>Carbon::now());
            DB::table('master_table')->insert($dtin);
              $data_response = [
                            'status' => 200,
                            'output' => 'Add New Table '.$table_name.' Success !!!'
                          ];
        db::commit();
      } catch (Exception $ex) {
         db::rollback();
              $message = $ex->getMessage();
                  \ErrorHandler($message);
                  $data_response = [
                            'status' => 422,
                            'output' => 'Add New Table field !!!'
                          ];
      }

      return response()->json(['data'=>$data_response]);
   }

   public function delTab(Request $request){
      $id_table = $request->id;

        try {
          db::begintransaction();
              DB::table('master_table')->where('id_table',$id_table)->update(['deleted_at'=>Carbon::now()]);

              $data_response = [
                            'status' => 200,
                            'output' => 'Delete Table Success !!!'
                          ];
          db::commit();
        } catch (Exception $ex) {
            db::rollback();
              $message = $ex->getMessage();
              \ErrorHandler($message);
              $data_response = [
                        'status' => 422,
                        'output' => 'Delete Table field !!!'
                      ];
        }

        return response()->json(['data'=>$data_response]);
   }

   public function printTable(Request $request){


        $data = DB::table('master_table')
                  ->whereNull('deleted_at')
                  ->orderBy('id_table','asc')
                  ->get();

        $papersize = 'a4';
        $orientation = 'portrait';
        $pdf = \PDF::loadView('master_parameter.print_barcode_table',
                                [
                                    'data' => $data
                                ])
                       ->setPaper($papersize, $orientation);

        return $pdf->stream('print_barcode_table');
   }

   public function editWorkHour(Request $request){
        $id = $request->id;
        $work_hours = $request->work;

        try {
            db::begintransaction();
                TableSpreading::where('id',$id)->update(['work_hours'=>$work_hours]);
                $data_response = [
                            'status' => 200,
                            'output' => 'Update Working Hour Success !!!'
                          ];
            db::commit();
        } catch (Exception $e) {
            db::rollback();
              $message = $ex->getMessage();
              \ErrorHandler($message);
              $data_response = [
                        'status' => 422,
                        'output' => 'Update Working Hour  field !!!'
                      ];
        }

        return response()->json(['data'=>$data_response]);
   }





}
