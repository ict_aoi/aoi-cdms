<?php

namespace App\Http\Controllers\LabFabric;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LabFabric;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Auth;
use DataTables;
use PDF;

class FabricTestingController extends Controller
{
    public function index(){
        return view('lab_fabric.index');
    }

    public function getDatatrf(Request $request){
        if ($request->ajax())
        {

            $fabric_testing = LabFabric::whereNull('deleted_at')
            ->where('platform', 'CDMS')
            ->orderByDesc('created_at')->get();

            return DataTables::of($fabric_testing)
            ->editColumn('nik',function ($fabric_testing)
            {
                $users = User::where('nik', $fabric_testing->nik)->first();
                if($users == null){
                    return '-';
                }else{
                    return $users->name;
                }
                
            })
            ->editColumn('lab_location',function ($fabric_testing)
            {
                if($fabric_testing->lab_location == 'AOI1'){
                    return 'AOI 1';
                }elseif($fabric_testing->lab_location == 'AOI2'){
                    return 'AOI 2';
                }else{
                    return '-';
                }
            })
            ->editColumn('date_information_remark',function ($fabric_testing)
            {
                if($fabric_testing->date_information_remark == 'buy_ready'){
                    return 'Buy Ready - '. $fabric_testing->date_information ;
                }elseif($fabric_testing->date_information_remark == 'output_sewing'){
                    return '1st Output Sewing - '. $fabric_testing->date_information ;
                }else return 'PODD - '. $fabric_testing->date_information ;
            })
            ->editColumn('test_required',function ($fabric_testing)
            {
                if($fabric_testing->test_required == 'developtesting_t1'){
                    return 'Develop Testing T1';
                }elseif($fabric_testing->test_required == 'developtesting_t2'){
                    return 'Develop Testing T2';
                }elseif($fabric_testing->test_required == 'developtesting_selective'){
                    return 'Develop Testing Selective';
                }elseif($fabric_testing->test_required == 'bulktesting_m'){
                    return '1st Bulk Testing M (Model Level)';
                }elseif($fabric_testing->test_required == 'bulktesting_a'){
                    return '1st Bulk Testing A (Article Level)';
                }elseif($fabric_testing->test_required == 'bulktesting_selective'){
                    return '1st Bulk Testing Selective';
                }elseif($fabric_testing->test_required == 'reordertesting_m'){
                    return 'Re-Order Testing M (Model Level)';
                }elseif($fabric_testing->test_required == 'reordertesting_a'){
                    return 'Re-Order Testing A (Article Level)';
                }elseif($fabric_testing->test_required == 'reordertesting_selective'){
                    return 'Re-Order Testing Selective';
                }else{
                    return 'Re-Test';
                }
            })
            ->editColumn('last_status',function ($fabric_testing)
            {
                $absensi = DB::connection('absence_aoi');
                if($fabric_testing->last_status == 'OPEN'){
                    return '<span class="label bg-grey-400">open</span>';
                }elseif($fabric_testing->last_status == 'VERIFIED'){
                    $user_verified = $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->verified_lab_by)->first();
                    return '<span class="label bg-blue">verified by '.$user_verified->name.'</span>';
                }elseif($fabric_testing->last_status == 'REJECT'){
                    $user_rejected =  $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->reject_by)->first();
                    return '<span class="label bg-danger">rejected by '.$user_rejected->name.'</span>';
                }elseif($fabric_testing->last_status == 'ONPROGRESS'){
                    return '<span class="label bg-success-400">onprogress</span>';
                }elseif($fabric_testing->last_status == 'CLOSED'){
                    return '<span class="label bg-danger">closed</span>';
                }else{
                    return 'eror';
                }
            })
            ->editColumn('testing_method_id',function ($fabric_testing)
            {
            $lab = DB::connection('lab');

            $testings = explode(",",$fabric_testing->testing_method_id);
            
            $master_method = $lab->table('master_method')
            ->select('master_method.method_name')
            ->wherein('method_code', $testings)
            ->get();
            $methods = $master_method->implode('method_name', ', ');

            return $methods; 
            
            })
            ->addColumn('action', function($fabric_testing) {
                if($fabric_testing->last_status == 'OPEN' || $fabric_testing->last_status == 'VERIFIED' || $fabric_testing->last_status == 'ONPROGRESS' || $fabric_testing->last_status == 'CLOSED')
                {
                    return view('lab_fabric._action', [
                        'model'         => $fabric_testing,
                        'print_notrf'   => route('lab_fabric.barcode', $fabric_testing->id),
                        'print_detail'   => route('lab_fabric.print_detail', $fabric_testing->id),
                    ]);
                }else {
                    return view('lab_fabric._action', [
                        'model'         => $fabric_testing,
                        'print_detail'   => route('lab_fabric.print_detail', $fabric_testing->id),
                    ]);
                }
            })
            ->rawColumns(['last_status','action'])
            ->make(true);
        }
    }
    public function getRequirements(Request $request)
    {
        $data_lab = DB::connection('lab');
        $category_specimen = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('category_specimen')
                            ->get();
        return response()->json(['response' => $category_specimen],200);
    }
    public function getTypeSpecimen(Request $request)
    {
        $data_lab = DB::connection('lab');
        $data = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('type_specimen')
                            ->where('category_specimen', $request->category_specimen)
                            ->get();
        return response()->json(['response' => $data],200);
    }

    public function getCategory(Request $request)
    {  
        $data_lab = DB::connection('lab');
        $category_specimen = $request->category_specimen;
        $type_specimen     = $request->type_specimen;
        $data = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('category')
                            ->where('category_specimen', $request->category_specimen)
                            ->where('type_specimen', $request->type_specimen)
                            ->get();
        return response()->json(['response' => $data],200);
    }


    public function getPoreference(Request $request)
    {

        if(!$request->po_mo){
            $data = ['null'];
        }else{
            $erp = DB::connection('erp_live');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;
                $getpo = $erp->table('bw_wms_testing_lab')
                ->distinct()
                ->select('poreference as po')
                ->where('poreference','like', '%'.$po_mo)
                ->first();
                $data = $erp->table('bw_wms_testing_lab')
                ->distinct()
                ->select('poreference as po','item_code')
                ->where('poreference', $getpo->po)
                ->get();

                $code = 200;
        }
        return response()->json(['response' => $data],$code);
    }

    public function getSize(Request $request)
    {

        if(!$request->po_mo){
            $data = ['null'];
        }else{
            $erp = DB::connection('erp_live');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;
            $item_code      = $request->item_code;

            $data = $erp->table('bw_wms_testing_lab')
            ->distinct()
            ->select('size','style','article','color', 'country')
            ->where('poreference', $po_mo)
            ->where('item_code', $item_code)
            ->get();
            $code=200;
        }
        return response()->json(['response' => $data],$code);
    }

    public function getStyle(Request $request)
    {

        if(!$request->po_mo){
            $data = ['null'];
        }else{
            $erp = DB::connection('erp_live');
            $po_mo          = $request->po_mo;
            $asal_specimen  = $request->asal_specimen;
            $item_code      = $request->item_code;
            $size           = $request->size;

            $data = $erp->table('bw_wms_testing_lab')
            ->distinct()
            ->select('size','style','article','color','country')
            ->where('poreference', $po_mo)
            ->where('item_code', $item_code)
            ->where('size', $size)
            ->first();
            $code=200;
        }
        
        return response()->json(['response' => $data],$code);
    }

    public function getTestingMethod(Request $request)
    {
        $category           = $request->category;
        $category_specimen  = $request->category_specimen;
        $type_specimen      = $request->type_specimen;

        $data_lab = DB::connection('lab');
        $data = $data_lab->table('get_requirement_testings')
                            ->distinct()
                            ->select('method_code', 'method_name')
                            ->where('category_specimen', $request->category_specimen)
                            ->where('type_specimen', $request->type_specimen)
                            ->where('category', $request->category)
                            ->get();
        $code=200;
        
        return response()->json(['response' => $data],$code);
    }

    public function store(Request $request)
    {
        $buyer              = $request->buyer;
        $asal_specimen      = "PRODUCTION";
        $category_specimen  = $request->category_specimen;
        $type_specimen      = $request->type_specimen;
        $category           = $request->category;
        $factory            = $request->factory;
        $test_required      = $request->test_required;
        $no_trf             = $request->no_trf;
        $radio_date         = $request->radio_date;
        $part_akan_dites    = $request->part_akan_dites;
        $po_mo              = $request->po_mo;
        $size               = $request->size;
        $style              = $request->style;
        $article            = $request->article;
        $color              = $request->color;
        $return_sample      = $request->return_sample;
        $other_buyer        = $request->other_buyer;
        $testing_date       = $request->testing_date;
        $testing_methods    = $request->testing_methods;
        $item               = $request->item;
        $destination        = $request->destination;


        $string_testing_methods = null;   
        if($testing_methods != null){
            $string_testing_methods = implode(",",$testing_methods);
        }
        $asal_specimen_code = "D";

        if($asal_specimen = "PRODUCTION"){
            $asal_specimen_code = "P";
        }else{
            $asal_specimen_code = "D";
        }
        $category_specimen_code = substr($category_specimen,0,1);
        $date = date('Ymd');
        $count = LabFabric::where('asal_specimen', $asal_specimen)
                                ->where(DB::raw("to_char(created_at,'YYYYMMDD')"),$date)
                                ->where('platform', 'CDMS')
                                ->whereNull('deleted_at')
                                ->count();
        $count = $count + 1;
        $trf_number = "TRF/". $factory ."/" . $date . "-CDMS-". $asal_specimen_code . $category_specimen_code . sprintf("%03s", $count);
        $has_dash       = strpos(\Auth::user()->nik, "-");
        if($has_dash == false){
            $nik = \Auth::user()->nik;
        }else{
            $split = explode('-',\Auth::user()->nik);
            $nik = $split[0];
        }if(\Auth::user()->factory_id =='1'){
            $factory_id = '1';
        }else {
            $factory_id = '2';
        }
            try {
                DB::beginTransaction();
                $movement_date = carbon::now();
                
                LabFabric::Create([
                    'no_trf'                => $trf_number,
                    'buyer'                 => $buyer,
                    'lab_location'          => 'AOI'.\Auth::user()->factory_id,
                    'nik'                   => \Auth::user()->nik,
                    'asal_specimen'         => $asal_specimen,
                    'category_specimen'     => $category_specimen,
                    'type_specimen'         => $type_specimen,
                    'test_required'         => $test_required,
                    'previous_trf'         => $no_trf,
                    'date_information_remark'=> $radio_date,
                    'category'              => $category,
                    'part_of_specimen'      => $part_akan_dites,
                    'is_pobuyer'            => true,
                    'is_mo'                 => false,
                    'size'                  => $size,
                    'style'                 => $style,
                    'article_no'            => $article,
                    'color'                 => $color,
                    'created_at'            => $movement_date,
                    'updated_at'            => null,
                    'return_test_sample'    => $return_sample,
                    'last_status'           => 'OPEN',
                    'deleted_at'            => null,
                    'testing_method_id'     => $string_testing_methods,
                    'orderno'               => $po_mo,
                    'date_information'      => $testing_date,
                    'platform'              => 'CDMS',
                    'is_mo'                 => false,
                    'item'                  => $item,
                    'destination'           => $destination,
                    'ishandover'            => false,
                    'factory_id'            => \Auth::user()->factory_id
                ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        return response()->json('success', 200);
    }

    public function barcode($id)
    {
        $id = $id;
        $master_testings = LabFabric::where('id', $id)
                            ->whereNull('deleted_at')
                            ->select('no_trf')
                            ->first();

    return view('lab_fabric.barcode', compact('master_testings'));
    }

    public function print_detail($id)
    {
        $lab = DB::connection('lab');
        $id = $id;
        $data['detil_trf'] = LabFabric::where('id', $id)
                            ->whereNull('deleted_at')
                            ->first();

        $data['name']      = DB::table('users')->where('nik', $data['detil_trf']->nik)->select('name')->first();

        $testings = explode(",",$data['detil_trf']->testing_method_id);
        $data['method'] = $lab->table('master_method')
        ->select('master_method.method_name')
        ->wherein('method_code', $testings)
        ->get();
        // $methods = $master_method->implode('method_name', '\n');

        $pdf = \PDF::loadView('lab_fabric.report_pdf', $data)->setPaper('A4','potrait');

        return $pdf->stream('DetailTRF.pdf');
    }
}
