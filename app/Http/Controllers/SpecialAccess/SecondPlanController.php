<?php

namespace App\Http\Controllers\SpecialAccess;

use DB;
use Carbon\Carbon;
use App\Models\CuttingMarker;
use App\Models\User;
use App\Models\ScanCutting;
use App\Models\Spreading\FabricUsed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SecondPlanController extends Controller
{
    public function index()
    {
        return view('special_access.second_plan.index');
    }

    public function scanBarcode(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();

            if($data != null) {
                $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));
    
                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();
    
                $ratio = 0;
                foreach ($data->rasio_markers as $a) {
                    $ratio = $ratio + $a->ratio;
                }
                $total_garment = $ratio * $data->layer;
    
                $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
    
                $po_buyer = implode(', ', $po_buyer);
    
                $ratio_size = '';
    
                foreach ($data->rasio_markers as $aa) {
                    $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                }

                $spreading_check = FabricUsed::where('barcode_marker', $data->barcode_id)->orderBy('created_spreading', 'desc')->get();

                if(count($spreading_check) > 0) {
                    $spreading_status = 'Done';
                    $spreading_date = Carbon::parse($spreading_check->first()->created_spreading)->format('d M Y / H:i');
                    $spreading_by = User::where('id', $spreading_check->first()->user_id)->first()->name;
                    $spreading_table = DB::table('master_table')->where('id_table', $spreading_check->first()->barcode_table)->first()->table_name;
                } else {
                    $spreading_status = '-';
                    $spreading_date = '-';
                    $spreading_by = '-';
                    $spreading_table = '-';
                }

                $approval_gl_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'GL Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_gl_check != null) {
                    $approve_gl_status = $approval_gl_check->status;
                    $approve_gl_date = Carbon::parse($approval_gl_check->created_at)->format('d M Y / H:i');
                    $approve_gl_by = User::where('id', $approval_gl_check->user_id)->first()->name;
                    $approve_gl_table = '-';
                } else {
                    $approve_gl_status = '-';
                    $approve_gl_date = '-';
                    $approve_gl_by = '-';
                    $approve_gl_table = '-';
                }

                $approval_qc_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'QC Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_qc_check != null) {
                    $approve_qc_status = $approval_qc_check->status;
                    $approve_qc_date = Carbon::parse($approval_qc_check->created_at)->format('d M Y / H:i');
                    $approve_qc_by = User::where('id', $approval_qc_check->user_id)->first()->name;
                    $approve_qc_table = '-';
                } else {
                    $approve_qc_status = '-';
                    $approve_qc_date = '-';
                    $approve_qc_by = '-';
                    $approve_qc_table = '-';
                }

                $cutting_check = ScanCutting::where('barcode_marker', $data->barcode_id)->where('state', 'done')->first();

                if($cutting_check != null) {
                    $get_user_array = $cutting_check->operator->pluck('user_id')->toArray();
                    $operator = User::whereIn('id', $get_user_array)->pluck('name')->toArray();
                    $cutting_status = 'Done';
                    $cutting_date = Carbon::parse($cutting_check->end_time)->format('d M Y / H:i');
                    $cutting_by = implode(', ', $operator);
                    $cutting_table = $cutting_check->table_name;
                } else {
                    $cutting_status = '-';
                    $cutting_date = '-';
                    $cutting_by = '-';
                    $cutting_table = '-';
                }

                if($data->second_plan == null) {
                    $second_plan = 'Tidak ada';
                } else {
                    $second_plan = Carbon::parse($data->second_plan)->format('d M Y');
                }
    
                $response = [
                    'data' => $data,
                    'style' => $data->cutting_plan->style,
                    'article' => $data->cutting_plan->articleno,
                    'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'),
                    'color_name' => substr($get_data_cutting->color_name, 0, 15),
                    'ratio' => $ratio,
                    'total_garment' => $total_garment,
                    'item_code' => $get_data_cutting->material,
                    'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20),
                    'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                    'marker_width' => $data->fabric_width - 0.5,
                    'po_buyer' => $po_buyer,
                    'ratio_size' => $ratio_size,
                    'ket' => null,
                    'spreading_status' => $spreading_status,
                    'approve_gl_status' => $approve_gl_status,
                    'approve_qc_status' => $approve_qc_status,
                    'cutting_status' => $cutting_status,
                    'bundling_status' => '-',
                    'spreading_date' => $spreading_date,
                    'approve_gl_date' => $approve_gl_date,
                    'approve_qc_date' => $approve_qc_date,
                    'cutting_date' => $cutting_date,
                    'bundling_date' => '-',
                    'spreading_by' => $spreading_by,
                    'approve_gl_by' => $approve_gl_by,
                    'approve_qc_by' => $approve_qc_by,
                    'cutting_by' => $cutting_by,
                    'bundling_by' => '-',
                    'spreading_table' => $spreading_table,
                    'cutting_table' => $cutting_table,
                    'bundling_table' => '-',
                    'second_plan' => $second_plan,
                ];
                return response()->json($response,200);
            } else {
                return response()->json('marker tidak ditemukan', 422);
            }
        }
    }

    public function add(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $cutting_date = $request->cutting_date;

            CuttingMarker::where('barcode_id', $barcode_id)->whereNull('deleted_at')->update(['second_plan' => $cutting_date]);

            $response = [
                'second_plan' => Carbon::parse($cutting_date)->format('d M Y')
            ];
            return response()->json($response,200);
        }
    }

    public function delete(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;

            CuttingMarker::where('barcode_id', $barcode_id)->whereNull('deleted_at')->update(['second_plan' => null]);

            $response = [
                'second_plan' => 'Tidak ada'
            ];
            return response()->json($response,200);
        }
    }
}
