$(document).ready(function() {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#laporanBaruTable').DataTable({
        ajax: {
            url: '/laporan-baru/data-marker',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'plan', name: 'plan',searchable:true,orderable:true},
            {data: 'barcode_id', name: 'barcode_id',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            {data: 'cut', name: 'cut',searchable:true,orderable:true},
            {data: 'detail', name: 'detail',searchable:false,orderable:false},
            {data: 'download_at', name: 'download_at',searchable:false,orderable:false},
            {data: 'selector', name: 'selector',searchable:false,orderable:false},
        ],
    });

    $('#laporanBaruTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/laporan-baru/data-marker',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#laporanBaruTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#laporanBaruTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#laporanBaruTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#detail_view_notes').text('');
    });

    $('#btn_print_id').on('click', function() {
        var cutting_date = $('#cutting_date').val();
        var data = Array();
        $('#laporanBaruTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });

        var print_type = $("input[name='print_type']:checked").val();
        
        $.ajax({
            type: "post",
            url: '/laporan-baru/print-id',
            data: {
                data: data,
                print_type: print_type,
                cutting_date: cutting_date
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },
            success: function (response) {
                var data = response.data
                $.unblockUI();
                console.log(response);
                $('#cutting_date').val(response.cutting_date);
                $('#form_filter_date').submit();
                
                if(response.status == 'isset') {
                    $('#list_id_marker').val(JSON.stringify(data));
                    $('#print_type_').val(response.print_type);
                    $('#get_barcode').submit();
                    $('#list_id_marker').val('');
                    $('#print_type_').val('');
                } else {
                    $("#alert_warning").trigger("click", 'Belum ada data yang dipilih!');
                    return false;
                }
            }
        });
    });

    $('#btn_change_ratio').on('click', function() {
        bootbox.confirm("Apakah anda yakin akan mengubah ratio ini ?", function (result) {
            if(result){
                var data = Array();
                $('#laporanBaruTable tbody tr').each(function(i, v) {
                    data[i] = Array();
                    $(this).children('td').each(function(ii, vv) {
                        if ($(this).find('input').length > 0) {
                            if ($(this).find('#Inputselector').length > 0) {
                                data[i][ii] = $(this).find("#Inputselector").is(':checked');
                            } else {
                                data[i][ii] = $(this).find('input').val();
                            }
                        } else {
                            data[i][ii] = $(this).text();
                        }
                    });
                });

                var cutting_date = $('#cutting_date').val();

                $.ajax({
                    type: "post",
                    url: '/laporan-baru/change-ratio',
                    data: {
                        data: data,
                        cutting_date: cutting_date,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                    },
                    success: function (response) {
                        // var data = response.data
                        $.unblockUI();
                        console.log(response);
                        $('#cutting_date').val(response.cutting_date);
                        $('#form_filter_date').submit();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });
});

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }
}

function detail(url)
{
    $.ajax({
        type: 'get',
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        }
    })
    .done(function (response) {
        $('#detail_view_barcode_marker').text(response.barcode_id);
        $('#detail_view_style').text(response.style);
        $('#detail_view_article').text(response.articleno);
        $('#detail_view_plan').text(response.cutting_date);
        $('#detail_view_cut').text(response.cut);
        $('#detail_view_length').text(response.marker_length);
        $('#detail_view_allow ').text(response.total_length);
        $('#detail_view_width ').text(response.marker_width);
        $('#detail_view_layer').text(response.layer);
        $('#detail_view_deliv').text(response.deliv);
        $('#detail_view_item').text(response.item_code);
        $('#detail_view_color_code').text(response.color_code);
        $('#detail_view_color_name').text(response.color_name);
        $('#detail_view_ratio_size').text(response.ratio_size);
        $('#detail_view_marker_pcs').text(response.marker_pcs);
        $('#detail_view_part').text(response.part_no);
        $('#detail_view_total_garment').text(response.total_garment);
        $('#detail_view_po_buyer').text(response.po_buyer);
        $('#detail_view_notes').text(response.notes);
        $('#detailModal').modal();
    });
}