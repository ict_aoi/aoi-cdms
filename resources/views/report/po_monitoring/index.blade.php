@extends('layouts.app',['active' => 'po_monitoring'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">PO Monitoring Distribusi</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">PO Monitoring Distribusi</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">PO Monitoring</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
                    <form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
									<label class="control-label col-lg-2 text-bold">PO Buyer</label>
									<div class="col-lg-10">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="po_buyer" id="po_buyer" placeholder="Masukkan PO Buyer..!" onkeydown="return (event.keyCode!=13);"/>
                                            <span class="input-group-btn">
												<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
											</span>
										</div>
									</div>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div>
							<div class="alert bg-info alert-styled-left hidden g-btn">
								<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
								<span class="text-semibold">Press Button To Set Columns Visibility!</span>
							</div>
							<span class="input-group-btn hidden g-btn">
								<button class="btn btn-xs btn-success legitRipple c1" type="button" id="cut">CUTTING</button>
								<button class="btn btn-xs bg-slate-400 legitRipple c2 hidden" type="button" id="cut2">CUTTING</button>
								<button class="btn btn-xs btn-success legitRipple f1" type="button" id="fuse">FUSE</button>
								<button class="btn btn-xs bg-slate-400 legitRipple f2 hidden" type="button" id="fuse2">FUSE</button>
								<button class="btn btn-xs btn-success legitRipple p1" type="button" id="ppa1">PPA1</button>
								<button class="btn btn-xs bg-slate-400 legitRipple p2 hidden" type="button" id="ppa12">PPA1</button>
								<button class="btn btn-xs btn-success legitRipple pp1" type="button" id="ppa2">PPA2</button>
								<button class="btn btn-xs bg-slate-400 legitRipple pp2 hidden" type="button" id="ppa22">PPA2</button>
								<button class="btn btn-xs btn-success legitRipple pd1" type="button" id="pad">PAD</button>
								<button class="btn btn-xs bg-slate-400 legitRipple pd2 hidden" type="button" id="pad2">PAD</button>
								<button class="btn btn-xs btn-success legitRipple he1" type="button" id="he">HE</button>
								<button class="btn btn-xs bg-slate-400 legitRipple he2 hidden" type="button" id="he2">HE</button>
								<button class="btn btn-xs btn-success legitRipple a1" type="button" id="auto">AUTO</button>
								<button class="btn btn-xs bg-slate-400 legitRipple a2 hidden" type="button" id="auto2">AUTO</button>
								<button class="btn btn-xs btn-success legitRipple ar1" type="button" id="art">ART</button>
								<button class="btn btn-xs bg-slate-400 legitRipple ar2 hidden" type="button" id="art2">ART</button>
								<button class="btn btn-xs btn-success legitRipple s1" type="button" id="set">SET</button>
								<button class="btn btn-xs bg-slate-400 legitRipple s2 hidden" type="button" id="set2">SET</button>
								<button class="btn btn-xs btn-success legitRipple sp1" type="button" id="supply">SUPPLY</button>
								<button class="btn btn-xs bg-slate-400 legitRipple sp2 hidden" type="button" id="supply2">SUPPLY</button>
							</span>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed datatable-colvis-basic" id="table-list">
									<thead class="bg-grey table-bordered">
										<tr>
											<th rowspan="2" style="border:solid 1px">No</th>
											<th rowspan="2" style="border:solid 1px">PO Buyer</th>
											<th rowspan="2" style="border:solid 1px">Season</th>
											<th rowspan="2" style="border:solid 1px">Style</th>
											<th rowspan="2" style="border:solid 1px">Article</th>
                                            <th rowspan="2" style="border:solid 1px">Size</th>
											<th rowspan="2" style="border:solid 1px">Komponen</th>
											<th rowspan="2" style="border:solid 1px">QTY Order</th>
											<th colspan="2" style="text-align:center;border:solid 1px">CUT</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">FUSE</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">PPA 1</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">PPA 2</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">PAD</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">HE</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">AUTO</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">ART</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">SETTING</th>
                                            <th colspan="2" style="text-align:center;border:solid 1px">SUPPLY</th>
										</tr>
										<tr>
											@for($i=1;$i < 11;$i++)
											<th style="border:solid 1px">BALANCED</th>
											<th style="border:solid 1px">DONE</th>
											@endfor
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-js')
<script>
    $(document).ready( function () {
		$('body').addClass('sidebar-xs');
		//DATATABLES
		var po_buyer_field = document.getElementById("po_buyer");
		po_buyer_field.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("btn-filter").click();
			}
		});
		var po_buyer =$('#po_buyer').val();
			var cut_num =$('#cut_num').val();
			var url = "{{route('po_monitoring.getDataMonitoring')}}";
			var tableL = $('#table-list').DataTable({
				processing: true,
				serverSide: true,
				deferRender:false,
				pageLength:100,
				destroy:true,
				scrollX:true,
				dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
				language: {
					search: '<span>Filter:</span> _INPUT_',
					searchPlaceholder: 'Type to filter...',
					lengthMenu: '<span>Show:</span> _MENU_',
					paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
				},
				ajax: {
					type: 'GET',
					url: url,
					data: function(d) {
						return $.extend({}, d, {
							"po_buyer"		: $('#po_buyer').val(),
						});
					},
				},
				fnCreatedRow: function (row, data, index) {
					var info = tableL.page.info();
					var value = index+1+info.start;
					$('td', row).eq(0).html(value);
				},
				columns: [
					{data: null, sortable: false, orderable: false, searchable: false},//0
					{data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:false},//12
					{data: 'season', name: 'season',searchable:false,visible:true,orderable:false},//1
					{data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//2
					{data: 'article', name: 'article',searchable:false,visible:true,orderable:false},//3
                    {data: 'size', name: 'size',searchable:false,visible:true,orderable:true},//3
					{data: 'komponen_name', name: 'komponen_name',searchable:false,visible:true,orderable:true},//4
					{data: 'ordered_qty', name: 'ordered_qty',searchable:false,visible:true,orderable:false},//4
					{data: 'balanced_cut', name: 'balanced_cut',searchable:false,visible:true,orderable:true},//5
					{data: 'qty_cut', name: 'qty_cut',searchable:false,visible:true,orderable:true},//5
					{data: 'balanced_fuse', name: 'balanced_fuse',searchable:true,visible:true,orderable:true},//6
					{data: 'qty_fuse', name: 'qty_fuse',searchable:true,visible:true,orderable:true},//6
					{data: 'balanced_ppa1', name: 'balanced_ppa1',searchable:true,visible:true,orderable:true},//7
					{data: 'qty_ppa1', name: 'qty_ppa1',searchable:true,visible:true,orderable:true},//7
					{data: 'balanced_ppa2', name: 'balanced_ppa2',searchable:true,visible:true,orderable:true},//8
					{data: 'qty_ppa2', name: 'qty_ppa2',searchable:true,visible:true,orderable:true},//8
					{data: 'balanced_pad', name: 'balanced_pad',searchable:false,visible:true,orderable:false},//11
					{data: 'qty_pad', name: 'qty_pad',searchable:false,visible:true,orderable:false},//11
					{data: 'balanced_he', name: 'balanced_he',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_he', name: 'qty_he',searchable:true,visible:true,orderable:true},//12
					{data: 'balanced_auto', name: 'balanced_auto',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_auto', name: 'qty_auto',searchable:true,visible:true,orderable:true},//12
					{data: 'balanced_artwork', name: 'balanced_artwork',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_artwork', name: 'qty_artwork',searchable:true,visible:true,orderable:true},//12
					{data: 'balanced_setting', name: 'balanced_setting',searchable:true,visible:true,orderable:true},//13
					{data: 'qty_setting', name: 'qty_setting',searchable:true,visible:true,orderable:true},//13
					{data: 'balanced_supply', name: 'balanced_supply',searchable:true,visible:true,orderable:true},//13
                    {data: 'qty_supply', name: 'qty_supply',searchable:true,visible:true,orderable:true},//13
				]
			});

			$('#cut').on('click', function () {
				$('.c1').addClass('hidden');
				$('.c2').removeClass('hidden');
				var isVisible = tableL.column('cut:name').visible();
				tableL.column('qty_cut:name').visible( !isVisible );
				tableL.column('balanced_cut:name').visible( !isVisible );
			});
			$('#cut2').on('click', function () {
				$('.c2').addClass('hidden');
				$('.c1').removeClass('hidden');
				var isVisible = tableL.column('cut:name').visible();
				tableL.column('qty_cut:name').visible( isVisible );
				tableL.column('balanced_cut:name').visible( isVisible );
			});
			$('#fuse').on('click', function () {
				$('.f1').addClass('hidden');
				$('.f2').removeClass('hidden');
				var isVisible = tableL.column('fuse:name').visible();
				tableL.column('qty_fuse:name').visible( !isVisible );
				tableL.column('balanced_fuse:name').visible( !isVisible );
			});
			$('#fuse2').on('click', function () {
				$('.f2').addClass('hidden');
				$('.f1').removeClass('hidden');
				var isVisible = tableL.column('fuse:name').visible();
				tableL.column('qty_fuse:name').visible( isVisible );
				tableL.column('balanced_fuse:name').visible( isVisible );
			});
			$('#ppa1').on('click', function () {
				$('.p1').addClass('hidden');
				$('.p2').removeClass('hidden');
				var isVisible = tableL.column('ppa1:name').visible();
				tableL.column('qty_ppa1:name').visible( !isVisible );
				tableL.column('balanced_ppa1:name').visible( !isVisible );
			});
			$('#ppa12').on('click', function () {
				$('.p2').addClass('hidden');
				$('.p1').removeClass('hidden');
				var isVisible = tableL.column('ppa1:name').visible();
				tableL.column('qty_ppa1:name').visible( isVisible );
				tableL.column('balanced_ppa1:name').visible( isVisible );
			});
			$('#ppa2').on('click', function () {
				$('.pp1').addClass('hidden');
				$('.pp2').removeClass('hidden');
				var isVisible = tableL.column('ppa2:name').visible();
				tableL.column('qty_ppa2:name').visible( !isVisible );
				tableL.column('balanced_ppa2:name').visible( !isVisible );
			});
			$('#ppa22').on('click', function () {
				$('.pp2').addClass('hidden');
				$('.pp1').removeClass('hidden');
				var isVisible = tableL.column('ppa2:name').visible();
				tableL.column('qty_ppa2:name').visible( isVisible );
				tableL.column('balanced_ppa2:name').visible( isVisible );
			});
			$('#pad').on('click', function () {
				$('.pd1').addClass('hidden');
				$('.pd2').removeClass('hidden');
				var isVisible = tableL.column('pad:name').visible();
				tableL.column('qty_pad:name').visible( !isVisible );
				tableL.column('balanced_pad:name').visible( !isVisible );
			});
			$('#pad2').on('click', function () {
				$('.pd2').addClass('hidden');
				$('.pd1').removeClass('hidden');
				var isVisible = tableL.column('pad:name').visible();
				tableL.column('qty_pad:name').visible( isVisible );
				tableL.column('balanced_pad:name').visible( isVisible );
			});
			$('#he').on('click', function () {
				$('.he1').addClass('hidden');
				$('.he2').removeClass('hidden');
				var isVisible = tableL.column('he:name').visible();
				tableL.column('qty_he:name').visible( !isVisible );
				tableL.column('balanced_he:name').visible( !isVisible );
			});
			$('#he2').on('click', function () {
				$('.he2').addClass('hidden');
				$('.he1').removeClass('hidden');
				var isVisible = tableL.column('he:name').visible();
				tableL.column('qty_he:name').visible( isVisible );
				tableL.column('balanced_he:name').visible( isVisible );
			});
			$('#auto').on('click', function () {
				$('.a1').addClass('hidden');
				$('.a2').removeClass('hidden');
				var isVisible = tableL.column('auto:name').visible();
				tableL.column('qty_auto:name').visible( !isVisible );
				tableL.column('balanced_auto:name').visible( !isVisible );
			});
			$('#auto2').on('click', function () {
				$('.a2').addClass('hidden');
				$('.a1').removeClass('hidden');
				var isVisible = tableL.column('auto:name').visible();
				tableL.column('qty_auto:name').visible( isVisible );
				tableL.column('balanced_auto:name').visible( isVisible );
			});
			$('#art').on('click', function () {
				$('.ar1').addClass('hidden');
				$('.ar2').removeClass('hidden');
				var isVisible = tableL.column('artwork:name').visible();
				tableL.column('qty_artwork:name').visible( !isVisible );
				tableL.column('balanced_artwork:name').visible( !isVisible );
			});
			$('#art2').on('click', function () {
				$('.ar2').addClass('hidden');
				$('.ar1').removeClass('hidden');
				var isVisible = tableL.column('artwork:name').visible();
				tableL.column('qty_artork:name').visible( isVisible );
				tableL.column('balanced_artwork:name').visible( isVisible );
			});
			$('#set').on('click', function () {
				$('.s1').addClass('hidden');
				$('.s2').removeClass('hidden');
				var isVisible = tableL.column('setting:name').visible();
				tableL.column('qty_setting:name').visible( !isVisible );
				tableL.column('balanced_setting:name').visible( !isVisible );
			});
			$('#set2').on('click', function () {
				$('.s2').addClass('hidden');
				$('.s1').removeClass('hidden');
				var isVisible = tableL.column('setting:name').visible();
				tableL.column('qty_setting:name').visible( isVisible );
				tableL.column('balanced_setting:name').visible( isVisible );
			});
			$('#supply').on('click', function () {
				$('.sp1').addClass('hidden');
				$('.sp2').removeClass('hidden');
				var isVisible = tableL.column('supply:name').visible();
				tableL.column('qty_supply:name').visible( !isVisible );
				tableL.column('balanced_supply:name').visible( !isVisible );
			});
			$('#supply2').on('click', function () {
				$('.sp2').addClass('hidden');
				$('.sp1').removeClass('hidden');
				var isVisible = tableL.column('supply:name').visible();
				tableL.column('qty_supply:name').visible( isVisible );
				tableL.column('balanced_supply:name').visible( isVisible );
			});
	
			var tableL = $('#table-list').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						tableL.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						tableL.search("").draw();
					}
					return;
			});
			tableL.on('preDraw',function(){
				Pace.start();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			})
			.on('draw.dt',function(){
				Pace.stop();
				$.unblockUI();
			});
	
		$('#btn-filter').click(function(){
			event.preventDefault();
			var po_buyer    = $('#po_buyer').val();
			if(po_buyer =='') {
				$("#alert_warning").trigger("click", 'PO Buyer Tidak Boleh Kosong!');
				return false;
			}
			tableL.draw();
			$('.g-btn').removeClass('hidden');
		});
    });
</script>
@endsection