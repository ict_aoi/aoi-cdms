@extends('layouts.app',['active' => 'packinglist_kk'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Packinglist Subcont </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Packinglist Subcont</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-lg-3">
                <label>Nomor Kontrak Kerja</label>
                <select data-placeholder="Search Nomor Kontrak Kerja" name="fil_kk" id="fil_kk" class="form-control select-search" required="">
                    <option value=""></option>
                    @foreach($nokk as $no)
                        <option value="{{ $no->id.'&'.$no->no_kk }}">{{ $no->no_kk }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3">
                <label>Style</label>
                <select data-placeholder="Search Style" name="fil_style" id="fil_style" class="form-control select-search" required="">
                        <option value=""></option>
                </select>
            </div>
            <div class="col-lg-3">
                <label>Subcont To</label><br>
                <label id="lbsubcont" style="margin-top: 10px; font-weight:  bold;"><b></b></label>
                <input type="text" name="txinitials" id="txinitials" hidden="">
                <input type="text" name="txsubcontname" id="txsubcontname" hidden="">
            </div>
            <div class="col-lg-3">
                <label>Packinglist Number</label><br>
                <label id="lbnopl" style="margin-top: 10px; font-weight:  bold;"></label>
                <input type="text" name="txnopl" id="txnopl" hidden="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-5">
                <label>PO Buyer</label>
                <select multiple data-placeholder="Select Po Buyer..." name="fil_po" id="fil_po" class="form-control select-search" required="">
                    <option value=""></option>
                </select>
            </div>
            <div class="col-lg-5">
                <label>Process</label>
                <select multiple data-placeholder="Select process..." name="fil_process" id="fil_process" class="form-control select-search" required="">
                    <option value=""></option>
                    @foreach ($proses as $pro)  
                        <option value="{{ $pro->id }}">{{ $pro->process_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-2">
                <button class="btn btn-success" id="btn-savepl" style="margin-top: 13px;"><span class="icon-checkmark4"></span> Save</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="packinglistTable" width="100%">
                <thead>
                    <tr>
                        <th>Packinglist No</th>
                        <th>KK No</th>
                        <th>To Factory</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('MasterKK.packinglistGetData') }}" id="getDataLink"></a>
@endsection

@section('page-js')
<script type="text/javascript">
    $(document).ready(function(){

        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });

        $('#packinglistTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 20,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: $('#getDataLink').attr('href'),
            },
            columns: [
                {data: 'no_packinglist', name: 'no_packinglist',searchable:true,orderable:true},
                {data: 'no_kk', name: 'no_kk',searchable:true,orderable:true},
                {data: 'subcont', name: 'subcont',searchable:true,orderable:true},
                {data: 'status', name: 'status',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        var dtable = $('#packinglistTable').dataTable().api();

        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });

        dtable.draw();
        
        $('#fil_kk').on('change',function(){
            var fkk = $('#fil_kk').val();
            var idkk = fkk.split("&")[0];
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url : "{{ route('MasterKK.ajaxGetStyle') }}",
                data :{idkk:idkk},
                success: function(response) {
                    var headkk = response.kontrak;

                    $('#lbsubcont').text(headkk.subcont_name);
                    $('#txinitials').val(headkk.initials);
                    $('#txsubcontname').val(headkk.subcont_name);
                    generatPL($('#fil_kk').val(),headkk.initials);
                    var style = response.style;
                
                    $('#fil_style').empty();
                    $('#fil_style').append('<option value="">Search Style</option>');
                    for (var i = 0; i < style.length; i++) {
                    
                    $('#fil_style').append('<option value="'+style[i]['style']+'">'+style[i]['style']+'</option>');
                    }
                },
                error: function(response) {
                    console.log(response);
                }
            });

            
        });

        $('#fil_style').on('change',function(){
            var fkk = $('#fil_kk').val();
            var idkk = fkk.split("&")[0];

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url : "{{ route('MasterKK.ajaxGetPo') }}",
                data :{idkk:idkk,style:$('#fil_style').val()},
                success: function(response) {
                    var po_buyer = response.po_buyer;

                
                    $('#fil_po').empty();
                    $('#fil_po').append('<option value="">Search PO Buyer</option>');
                    for (var i = 0; i < po_buyer.length; i++) {
                    
                    $('#fil_po').append('<option value="'+po_buyer[i]['po_number']+'">'+po_buyer[i]['po_number']+'</option>');
                    }
                },
                error: function(response) {
                    console.log(response);
                }
            });
        });

        $('#btn-savepl').on('click',function(){
            var fkk = $('#fil_kk').val();
            var idkk = fkk.split("&")[0];
            var nokk = fkk.split("&")[1];
            var style  = $('#fil_style').val();
            var subcont_name = $('#txsubcontname').val();
            var nopl = $('#txnopl').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url : "{{ route('MasterKK.createPackinglistKK') }}",
                data :{dt_po:$('#fil_po').val(),dt_process:$('#fil_process').val(),idkk:idkk,nokk:nokk,style:style,subcont_name:subcont_name,nopl:nopl},
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    var data_response = response.data;
                    $.unblockUI();
                    if (data_response.status==200) {
                        $("#alert_success").trigger("click", data_response.output);
                        window.location.reload();
                    }else{
                        $("#alert_warning").trigger("click",data_response.output);
                    }
                },
                error: function(response) {
                    var data_response = response.data;
                    $("#alert_warning").trigger("click",data_response.output);
                    $.unblockUI();
                    console.log(response);
                }
            });
        });
    });

    function generatPL(idkk,initial){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : "{{ route('MasterKK.generatePackingNumber') }}",
            data :{idkk:idkk,initial:initial},
            success: function(response) {
                var nopl = response.nopl;
                $('#txnopl').val(nopl);
                $('#lbnopl').text(nopl);
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
</script>
@endsection