<ul class="icons-list">
  <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="icon-menu9"></i>
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
                @if(isset($list))
                    <li><a href="#" onclick="detailPlan('{$list}')"><i class="icon-list"></i> List</a></li>
                @endif
                @if(isset($edit))
                    <li><a href="#" ><i class="icon-pencil5"></i> Edit</a></li>
                @endif
      </ul>
  </li>
</ul>