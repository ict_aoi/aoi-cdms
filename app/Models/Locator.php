<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locator extends Model
{
    // use Uuids;
	// public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_locator';
    protected $fillable = ['locator_name','permissions','is_artwork', 'role', 'is_cutting', 'is_setting', 'created_at', 'updated_at', 'deleted_at','finished_at'];

	public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role', 'id');
    }
}
