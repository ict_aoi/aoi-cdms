var spreading_case = 'normal';
var list_marker_tumpuk = [];

$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    // normal
    $('#table_cutting_check').on('click', function() {
        $('#tableModal').modal();
        get_data_cutting_table();
    });

    // tumpuk
    $('#table_cutting_check_tumpuk').on('click', function() {
        $('#tableModal').modal();
        get_data_cutting_table();
    })

    $('#tableTable').on('click', '#select_cutting_table', function() {
        var id_cutting_table = $(this).data('id');
        var barcode_cutting_table = $(this).data('barcode');
        var name_cutting_table = $(this).data('name');

        if(spreading_case == 'normal') {
            $('#table_cutting_check').val(name_cutting_table);
            $('#id_cutting_table').val(id_cutting_table);
            $('#barcode_cutting_table').val(barcode_cutting_table);
        } else if(spreading_case == 'tumpuk') {
            $('#table_cutting_check_tumpuk').val(name_cutting_table);
            $('#id_cutting_table_tumpuk').val(id_cutting_table);
            $('#barcode_cutting_table_tumpuk').val(barcode_cutting_table);
        }

        $('#tableModal').modal('hide');
        $('#marker_check_tumpuk').val('');
        $('#marker_check_tumpuk').focus();
    });

    // input normal
    $('.input-new').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();

            var input_barcode = $(this).val();
            var barcode_check = input_barcode.substr(0, 3);

            if(barcode_check == 'MJ-') {
                barcode_for_table(input_barcode);
                return false
            } else if(barcode_check == 'MK-') {
                var id_cutting_table = $('#id_cutting_table').val();
                $('#marker_check').val('');
                if(!id_cutting_table){
                    $("#alert_warning").trigger("click", 'Scan Barcode Meja Cutting Terlebih Dahulu!');
                    $('#marker_check').val('');
                    return false;
                }
                barcode_for_marker(input_barcode);
                return false;
            } else {
                $("#alert_warning").trigger("click", 'Barcode Tidak Dapat Dikenali!');
                $('#marker_check').val('');
                return false;
            }
        }
    });

    // input tumpuk
    $('.input-new-tumpuk').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();

            var input_barcode = $(this).val();
            var barcode_check = input_barcode.substr(0, 3);

            if(barcode_check == 'MJ-') {
                barcode_for_table(input_barcode);
                return false
            } else if(barcode_check == 'MK-') {
                var id_cutting_table = $('#id_cutting_table_tumpuk').val();
                $('#marker_check_tumpuk').val('');
                if(!id_cutting_table){
                    $("#alert_warning").trigger("click", 'Scan Barcode Meja Cutting Terlebih Dahulu!');
                    $('#marker_check_tumpuk').val('');
                    return false;
                }
                if(list_marker_tumpuk.indexOf(input_barcode) === -1) {
                    barcode_for_marker_tumpuk(input_barcode);
                } else {
                    $("#alert_warning").trigger("click", 'Barcode marker sudah pernah discan!');
                    $('#marker_check_tumpuk').val('');
                    return false;
                }
                return false;
            } else {
                $("#alert_warning").trigger("click", 'Barcode Tidak Dapat Dikenali!');
                $('#marker_check_tumpuk').val('');
                return false;
            }
        }
    });

    $('#fabric_check').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();
            var detail_view_allow = $('#detail_view_allow').text();
            if(spreading_case == 'normal') {
                var marker_check = $('#marker_check').val();
                var barcode_meja = $('#barcode_cutting_table').val();
            } else if(spreading_case == 'tumpuk') {
                var marker_check = $('#marker_check_tumpuk').val();
                var barcode_meja = $('#barcode_cutting_table_tumpuk').val();
            } else {
                $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
            }
            var input_barcode = $(this).val();
            var detail_view_layer = $('#detail_view_layer').text();

            if(!marker_check){
                $("#alert_warning").trigger("click", 'Tidak Ada Barcode Marker Yang Terpilih!');
                $('#marker_check').val('');
                return false;
            }
            if(!detail_view_allow){
                $("#alert_warning").trigger("click", 'Tidak Ada Data Panjang Marker!');
                $('#marker_check').val('');
                return false;
            }
            barcode_for_fabric(input_barcode, detail_view_allow, marker_check, barcode_meja, detail_view_layer);
            return false
        }
    });

    var actual_width = '#actual_width';

    $('#inputTable').on('change', actual_width, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_width = $row.find(actual_width).val();
        var $id = $row.find('#id').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-actual-width',
            data: {
                id: $id,
                actual_width: $actual_width,
                barcode_marker: marker_check,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $.unblockUI();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var actual_layer = '#actual_layer';

    $('#inputTable').on('change', actual_layer, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_layer = $row.find(actual_layer).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#detail_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-actual-layer',
            data: {
                id: $id,
                actual_layer: $actual_layer,
                barcode_marker: marker_check,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $.unblockUI();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var actual_sisa = '#actual_sisa';

    $('#inputTable').on('change', actual_sisa, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_sisa = $row.find(actual_sisa).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#detail_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-actual-sisa',
            data: {
                id: $id,
                actual_sisa: $actual_sisa,
                barcode_marker: marker_check,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $('#loading_view').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $('#loading_view').unblock();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $.unblockUI();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var reject = '#reject';

    $('#inputTable').on('change', reject, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $reject = $row.find(reject).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#detail_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-reject',
            data: {
                id: $id,
                reject: $reject,
                barcode_marker: marker_check,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $('#loading_view').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $('#loading_view').unblock();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $('#loading_view').unblock();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var sambungan = '#sambungan';

    $('#inputTable').on('change', sambungan, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $sambungan = $row.find(sambungan).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#detail_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-sambungan',
            data: {
                id: $id,
                sambungan: $sambungan,
                barcode_marker: marker_check,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $('#loading_view').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $('#loading_view').unblock();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $('#loading_view').unblock();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var sambungan_end = '#sambungan_end';

    $('#inputTable').on('change', sambungan_end, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $sambungan_end = $row.find(sambungan_end).val();
        var $id = $row.find('#id').val();
        var detail_view_allow = $('#detail_view_allow').text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-sambungan-end',
            data: {
                id: $id,
                sambungan_end: $sambungan_end,
                barcode_marker: marker_check,
                detail_view_allow: detail_view_allow,
            },
            beforeSend: function () {
                $('#loading_view').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $('#loading_view').unblock();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $('#loading_view').unblock();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    var kualitas_gelaran = '#kualitas_gelaran';

    $('#inputTable').on('change', kualitas_gelaran, function(event) {
        if(spreading_case == 'normal') {
            var marker_check = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk') {
            var marker_check = $('#marker_check_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $kualitas_gelaran = $row.find(kualitas_gelaran).val();
        var $id = $row.find('#id').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/update-kualitas-gelaran',
            data: {
                id: $id,
                kualitas_gelaran: $kualitas_gelaran,
                barcode_marker: marker_check,
            },
            beforeSend: function () {
                $('#loading_view').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $('#loading_view').unblock();
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_marker);
            },
            error: function(response) {
                $('#loading_view').unblock();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else {
                    $("#alert_warning").trigger("click","Terjadi kesalahan eksekusi, silakan kontak ICT!");
                }
            }
        });
    });

    $('#tableModal').on('hidden.bs.modal', function(){
        if(spreading_case == 'normal') {
            $('#marker_check').focus();
        } else if(spreading_case == 'normal') {
            $('#marker_check_tumpuk').focus();
        }
    });

    $('#inputModal').on('shown.bs.modal', function(){
        $('#fabric_check').focus();
    });

    $('#inputModal').on('hidden.bs.modal', function(){
        if(spreading_case == 'normal') {
            var marker_barcode = $('#marker_check').val();
        } else if(spreading_case == 'tumpuk'){
            var marker_barcode = $('#marker_check_tumpuk').val();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/delete-report-spreading-temp',
            data: {
                barcode_marker: marker_barcode,
            },
            success: function (response) {
                if(spreading_case == 'normal') {
                    $('#table_cutting_check').val('');
                    $('#id_cutting_check').val('');
                    $('#barcode_cutting_check').val('');
                    $('#marker_check').val('');
                    $('#marker_check').focus();
                    $('#id_cutting_table').val('');
                } else if(spreading_case == 'tumpuk'){
                    $('#table_cutting_check_tumpuk').val('');
                    $('#id_cutting_check_tumpuk').val('');
                    $('#barcode_cutting_check_tumpuk').val('');
                    $('#marker_check_tumpuk').val('');
                    $('#marker_check_tumpuk').focus();
                    $('#barcode-view-tumpuk').empty();
                    $('#id_cutting_table_tumpuk').val('');
                }
            },
            error: function(response) {
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else if (response.status == 422) {
                    $("#alert_warning").trigger("click",response.responseJSON);
                }
            }
        });

    });

    $('#delete_last_fabric').on('click', function() {

        bootbox.confirm("Apakah anda yakin akan menghapus data fabric terakhir ini ?", function (result) {
            if(result){
                if(spreading_case == 'normal') {
                    var barcode_marker = $('#marker_check').val();
                    var barcode_table = $('#barcode_cutting_table').val();
                } else if(spreading_case == 'tumpuk') {
                    var barcode_marker = $('#marker_check_tumpuk').val();
                    var barcode_table = $('#barcode_cutting_table_tumpuk').val();
                } else {
                    $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
                }


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '/actual-spreading/delete-last-fabric-scan',
                    data: {
                        barcode_marker: barcode_marker,
                        barcode_table: barcode_table,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function (response) {
                        $.unblockUI();
                        if(response.status == 442) {
                            $("#alert_warning").trigger("click",response.report);
                            return false;
                        } else {
                            $('#inputModal').modal();
                            get_data_spreading_report(response.barcode_marker);
                        }
                    },
                    error: function(response) {
                        if(response.status == 419) {
                            window.location = $('#home_menu').attr('href');
                        } else if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            }
                        });
                    }
                });
            }
            else{
                $('.modal').on('hidden.bs.modal', function (e) {
                    if($('.modal').hasClass('in')) {
                        $('body').addClass('modal-open');
                    }
                });
            }
        });


    });

    $('#delete_all_spreading').on('click', function() {
        bootbox.confirm("Apakah anda yakin akan menghapus data spreading ini ?", function (result) {
            if(result){
                $('#inputModal').modal('toggle');
                list_marker_tumpuk = [];
            }
            else{
                $('.modal').on('hidden.bs.modal', function (e) {
                    if($('.modal').hasClass('in')) {
                        $('body').addClass('modal-open');
                    }
                });
            }
        });
    });

    $('#save_laporan_speading').on('click', function() {
        if(spreading_case == 'normal') {
            var barcode_marker = $('#marker_check').val();
            var barcode_table = $('#barcode_cutting_table').val();
        } else if(spreading_case == 'tumpuk') {
            var barcode_marker = $('#marker_check_tumpuk').val();
            var barcode_table = $('#barcode_cutting_table_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: '/actual-spreading/spreading-report-save',
            data: {
                barcode_marker: barcode_marker,
                barcode_table: barcode_table,
            },
            beforeSend: function() {
                $.blockUI({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                if(response.status == 442) {
                    $("#alert_warning").trigger("click",response.report);
                    return false;
                } else {
                    $('#inputModal').modal();
                    get_data_spreading_report(response.barcode_marker);
                }
            },
            error: function(response) {
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else if (response.status == 422) {
                    $("#alert_warning").trigger("click",response.responseJSON);
                }
            }
        });
    });

    $('#insert_laporan_speading').on('click', function() {
        if(spreading_case == 'normal') {
            var barcode_marker = $('#marker_check').val();
            var barcode_table = $('#barcode_cutting_table').val();
        } else if(spreading_case == 'tumpuk') {
            var barcode_marker = $('#marker_check_tumpuk').val();
            var barcode_table = $('#barcode_cutting_table_tumpuk').val();
        } else {
            $("#alert_warning").trigger("click",'Telah terjadi kesalahan!');
        }

        var layer = $('#detail_view_layer').text();
        var akumulasi_layer = $('#inputTable tbody tr:last').find('#akumulasi_layer').text();

        if(parseInt(akumulasi_layer) > parseInt(layer)) {
            $("#alert_warning").trigger("click", "Layer melebihi batas maksimal layer marker!");
            return false;
        }

        bootbox.confirm("Apakah anda yakin akan selesai spreading ini ?", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: '/actual-spreading/spreading-report-insert',
                    data: {
                        barcode_marker: barcode_marker,
                        barcode_table: barcode_table,
                        layer: layer,
                    },
                    beforeSend: function() {
                        $('#loading_view').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function (response) {
                        $('#loading_view').unblock();
                        if(response.status == 442) {
                            $("#alert_warning").trigger("click",response.report);
                            return false;
                        } else {
                            $('#inputModal').modal('toggle');
                            $('#downtimeModal').modal('toggle');
                        }
                    },
                    error: function(response) {
                        if(response.status == 419) {
                            window.location = $('#home_menu').attr('href');
                        } else if (response.status == 422) {
                            $("#alert_warning").trigger("click",response.responseJSON);
                        }
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            }
                        });
                    }
                });
            }
            else{
                $('.modal').on('hidden.bs.modal', function (e) {
                    if($('.modal').hasClass('in')) {
                        $('body').addClass('modal-open');
                    }
                });
            }
        });

    });

    $('#btn_downtime_submit').on('click', function() {
        var keterangan = $('#keterangan').val();
        var downtime = $('#downtime').val();
        var downtime_category = $('#downtime_category').val();
        var barcode_marker = $('#detail_view_barcode_marker').text();

        validation_check_callback(downtime_category, downtime, keterangan, barcode_marker, function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: '/actual-spreading/update-downtime',
                data: {
                    barcode_marker: barcode_marker,
                    keterangan: keterangan,
                    downtime_category: downtime_category,
                    downtime: downtime,
                },
                beforeSend: function() {
                    $('#loading_view').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function (response) {
                    $('#loading_view').unblock();
                    $('#downtime_category').each(function(i, v) {
                        $(this).children('option').removeAttr('selected');
                    });

                    $('#select2-downtime_category-container').text('Pilih a downtime...');
                    $('#select2-downtime_category-container').removeAttr('title');
                    $('#select2-downtime_category-container').attr('title', 'Pilih a downtime...');

                    $("#alert_success").trigger("click", response.message);
                    $('#downtime').val('');
                    $('#keterangan').val('');
                    $('#id_scan').val('');
                    $('#downtimeModal').modal('hide')
                },
                error: function(response) {
                    if(response.status == 419) {
                        window.location = $('#home_menu').attr('href');
                    } else if (response.status == 422) {
                        $("#alert_warning").trigger("click",response.responseJSON);
                    }
                }
            });
        })
    });

    $('.change_scan').on('click', function(e) {
        e.preventDefault();
        $('.change_scan').removeClass('border-primary text-primary');
        $('.change_scan').removeClass('btn-primary');
        $('.change_scan').addClass('border-primary text-primary');
        $(this).removeClass('border-primary text-primary');
        $(this).addClass('btn-primary');

        if($(this).text() == 'Normal') {
            $('#scan1').removeClass('hidden');
            $('#scan2').removeClass('hidden');
            $('#scan2').addClass('hidden');
            $('#scan3').removeClass('hidden');
            $('#scan3').addClass('hidden');
            $('#marker_check').focus();
            spreading_case = 'normal';
        } else if($(this).text() == 'Tumpuk') {
            $('#scan1').removeClass('hidden');
            $('#scan1').addClass('hidden');
            $('#scan2').removeClass('hidden');
            $('#scan3').removeClass('hidden');
            $('#scan3').addClass('hidden');
            $('#marker_check_tumpuk').focus();
            spreading_case = 'tumpuk';
        } else if($(this).text() == 'Sambung') {
            $('#scan1').removeClass('hidden');
            $('#scan1').addClass('hidden');
            $('#scan2').removeClass('hidden');
            $('#scan2').addClass('hidden');
            $('#scan3').removeClass('hidden');
        }

        // clear data scan

        // normal
        $('#table_cutting_check').val('');
        $('#id_cutting_table').val('');
        $('#barcode_cutting_table').val('');

        // tumpuk
        $('#table_cutting_check_tumpuk').val('');
        $('#id_cutting_table_tumpuk').val('');
        $('#barcode_cutting_table_tumpuk').val('');
    });

    // marker tumpuk delete
    $(document).on('click', '.marker-tumpuk-delete', function() {
        var barcode_marker = $(this).data('id');
        $(this).parent().parent().remove();

        if(list_marker_tumpuk.indexOf(barcode_marker) != -1) { // Make sure the value exists
            list_marker_tumpuk.splice(list_marker_tumpuk.indexOf(barcode_marker), 1);
        }
    });

    $('#btn-spread-tumpuk').on('click', function() {

        // console.log(list_marker_tumpuk);
        // console.log($('#id_cutting_table_tumpuk').val());
        // console.log($('#barcode_cutting_table_tumpuk').val());
        // console.log(list_marker_tumpuk.length)
        var id_cutting_table = $('#id_cutting_table_tumpuk').val();

        if(!id_cutting_table.length) {
            $("#alert_warning").trigger("click","Scan barcode Meja terlebih dahulu!");
            return false;
        }

        if(list_marker_tumpuk.length < 1) {
            $("#alert_warning").trigger("click","Scan barcode marker terlebih dahulu!");
            return false;
        }

        if(list_marker_tumpuk.length == 1) {
            $("#alert_warning").trigger("click","Barcode Marker yang discan baru 1 saja!");
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/actual-spreading/check-marker-id-tumpuk-submit',
            data: {
                barcode_ids: list_marker_tumpuk,
            },
            beforeSend: function() {
                $('#loading_view').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#marker_check_tumpuk').val(response.data.barcode_id);
                $('#detail_view_barcode_marker').text(response.data.barcode_id);
                $('#detail_view_style').text(response.style);
                $('#detail_view_cut').text(response.data.cut);
                $('#detail_view_layer').text(response.data.layer);
                $('#detail_view_length').text(response.data.marker_length);
                $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                $('#detail_view_part').text(response.data.part_no);
                $('#detail_view_plan').text(response.plan);
                $('#detail_view_color_name').text(response.color_name);
                $('#detail_view_color_code').text(response.color_code);
                $('#detail_view_item').text(response.item_code);
                $('#detail_view_deliv').text(response.stat_date);
                $('#detail_view_width').text(response.marker_width);
                $('#detail_view_article').text(response.article);
                $('#detail_view_marker_pcs').text(response.ratio);
                $('#detail_view_total_garment').text(response.total_garment);
                $('#detail_view_ratio_size').text(response.ratio_size);
                $('#detail_view_po_buyer').text(response.po_buyer);
                $('#inputModal').modal();
                get_data_spreading_report(response.data.barcode_id);
            },
            error: function(response) {
                $('#loading_view').unblock();
                $.unblockUI();
                if(response.status == 419) {
                    window.location = $('#home_menu').attr('href');
                } else if (response.status == 422) {
                    $("#alert_warning").trigger("click",response.responseJSON);
                }
            }
        });
    });
});

function barcode_for_table(input_barcode) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/actual-spreading/check-table-id',
        data: {
            table_check: input_barcode,
        },
        beforeSend: function() {
            $('#loading_view').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('#loading_view').unblock();
            if(response.status == 200) {
                if(spreading_case == 'normal') {
                    $('#table_cutting_check').val(response.data.table_name);
                    $('#barcode_cutting_table').val(response.data.id_table);
                    $('#id_cutting_table').val(response.data.id_table);
                    $('#marker_check').val('');
                    $('#marker_check').focus();
                } else if(spreading_case == 'tumpuk') {
                    $('#table_cutting_check_tumpuk').val(response.data.table_name);
                    $('#barcode_cutting_table_tumpuk').val(response.data.id_table);
                    $('#id_cutting_table_tumpuk').val(response.data.id_table);
                    $('#marker_check_tumpuk').val('');
                    $('#marker_check_tumpuk').focus();
                }
            } else {
                $('#table_cutting_check').val('');
                $('#marker_check').val('');
                $('#marker_check').focus();
                $("#alert_warning").trigger("click", 'Barcode Meja Tidak Ditemukan!');
            }
        },
        error: function(response) {
            $('#loading_view').unblock();
            $.unblockUI();
            if(response.status == 419) {
                window.location = $('#home_menu').attr('href');
            } else if (response.status == 422) {
                $("#alert_warning").trigger("click",response.responseJSON);
            }
        }
    });
}

// normal
function barcode_for_marker(input_barcode) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/actual-spreading/check-marker-id',
        data: {
            marker_check: input_barcode,
        },
        beforeSend: function() {
            $('#loading_view').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('#loading_view').unblock();
            $('#marker_check').val(response.data.barcode_id);
            $('#detail_view_barcode_marker').text(response.data.barcode_id);
            $('#detail_view_style').text(response.style);
            $('#detail_view_cut').text(response.data.cut);
            $('#detail_view_layer').text(response.data.layer);
            $('#detail_view_length').text(response.data.marker_length);
            $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
            $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
            $('#detail_view_part').text(response.data.part_no);
            $('#detail_view_plan').text(response.plan);
            $('#detail_view_color_name').text(response.color_name);
            $('#detail_view_color_code').text(response.color_code);
            $('#detail_view_item').text(response.item_code);
            $('#detail_view_deliv').text(response.stat_date);
            $('#detail_view_width').text(response.marker_width);
            $('#detail_view_article').text(response.article);
            $('#detail_view_marker_pcs').text(response.ratio);
            $('#detail_view_total_garment').text(response.total_garment);
            $('#detail_view_ratio_size').text(response.ratio_size);
            $('#detail_view_po_buyer').text(response.po_buyer);
            $('#inputModal').modal();
            get_data_spreading_report(response.data.barcode_id);
        },
        error: function(response) {
            $('#loading_view').unblock();
            $('#marker_check').val('');
            $('#marker_check').focus();
            if(response.status == 419) {
                window.location = $('#home_menu').attr('href');
            } else if (response.status == 422) {
                $("#alert_warning").trigger("click",response.responseJSON);
            }
        }
    });
}

// tumpuk
function barcode_for_marker_tumpuk(input_barcode) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/actual-spreading/check-marker-id-tumpuk',
        data: {
            marker_check: input_barcode,
        },
        beforeSend: function() {
            $('#loading_view').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('#loading_view').unblock();
            if(list_marker_tumpuk.indexOf(response.barcode_id) === -1) {
                list_marker_tumpuk.push(response.barcode_id);
            }
            appendMarkerTumpuk(response);
        },
        error: function(response) {
            $('#loading_view').unblock();
            $('#marker_check_tumpuk').val('');
            $('#marker_check_tumpuk').focus();
            if(response.status == 419) {
                window.location = $('#home_menu').attr('href');
            } else if (response.status == 422) {
                $("#alert_warning").trigger("click",response.responseJSON);
            }
        }
    });
}

function barcode_for_fabric(input_barcode, detail_view_allow, marker_check, barcode_meja, detail_view_layer) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/actual-spreading/check-fabric-id',
        data: {
            barcode_meja: barcode_meja,
            fabric_check: input_barcode,
            marker_check: marker_check,
            detail_view_allow: detail_view_allow,
            detail_view_layer: detail_view_layer,
        },
        beforeSend: function() {
            $('.page-content').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('.page-content').unblock();
            if(response.status == 200) {
                $('#inputModal').modal();
                get_data_spreading_report(response.barcode_id);
                $('#fabric_check').val('');
                $('#fabric_check').focus();
            } else {
                $("#alert_warning").trigger("click", response.report);
                get_data_spreading_report(response.barcode_id);
                $('#fabric_check').val('');
                $('#fabric_check').focus();
            }
        },
        error: function(response) {
            $('.page-content').unblock();
            $.unblockUI();
            if(response.status == 419) {
                window.location = $('#home_menu').attr('href');
            } else if (response.status == 422) {
                $("#alert_warning").trigger("click",response.responseJSON);
            }
        }
    });
}

function get_data_cutting_table() {
    var table = $('#tableTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/actual-spreading/data-cutting-table',
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                  className: 'text-center',
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'table_name', name: 'table_name', searchable:true, visible:true, orderable:false},
            {data: 'action', name: 'action', searchable:false, orderable:false},
        ],
    });

    $('#tableTable').on('click','.ignore-click', function() {
        return false;
    });
}

function get_data_spreading_report(barcode_marker) {
    var table = $('#inputTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            type: 'post',
            url: '/actual-spreading/data-spreading-report-temp',
            data: {
                barcode_marker: barcode_marker,
            }
        },
        columnDefs: [
            {
                className: 'dt-center',
            },
            {
                  className: 'text-center',
            },
            {
                targets: 1,
                className: 'dt_col_hide',
            },
            {
                targets: [4,6,7,8,11,13,15],
                className: 'remove-padding',
            },
            {
                targets: [0,1,2,3,5,9,10,12,14,16],
                className: 'custom-padding',
            }
        ],
        columns: [
            {data: 'no_roll', name: 'no_roll'}, // 0
            {data: 'id', name: 'id'}, // 1
            {data: 'lot', name: 'lot'}, // 2
            {data: 'qty_fabric', name: 'qty_fabric'}, // 3
            {data: 'actual_width', name: 'actual_width'}, // 4
            {data: 'suplai_layer', name: 'suplai_layer'}, // 5
            {data: 'actual_layer', name: 'actual_layer'}, // 6
            {data: 'sambungan', name: 'sambungan'}, // 11
            {data: 'sambungan_end', name: 'sambungan_end'}, // 12
            {data: 'akumulasi_layer', name: 'akumulasi_layer'}, // 9
            {data: 'actual_use', name: 'actual_use'}, // 13
            {data: 'reject', name: 'reject'}, // 10
            {data: 'suplai_sisa', name: 'suplai_sisa'}, // 7
            {data: 'actual_sisa', name: 'actual_sisa'}, // 8
            {data: 'remark', name: 'remark'}, // 14
            {data: 'kualitas_gelaran', name: 'kualitas_gelaran'}, // 15
            {data: 'saved', name: 'saved'}, // 16
        ],
    });

    $('#tableTable').on('click','.ignore-click', function() {
        return false;
    });
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

function downtimeModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            if(response.user == null) {
                $('#planning_id_detail').val(response.id_plan);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#style_detail_detail_view').text(response.style);
                $('#articleno_detail_detail_view').text(response.articleno);
                $('#cutting_date_detail_detail_view').text(response.cutting_date);
                $('#part_no_detail_detail_view').text(response.part_no);
                $('#po_buyer_detail_detail_view').text(response.po_buyer);
                $('#detailModal').modal('toggle');
                $('#detailDetailModal').modal('toggle');
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            } else {
                $('#planning_id').val(response.id_plan);
                $('#style_detail_view').text(response.style);
                $('#articleno_detail_view').text(response.articleno);
                $('#cutting_date_detail_view').text(response.cutting_date);
                $('#detailModal').modal();
                get_data_modal_table();

                $("#alert_warning").trigger("click", 'Halaman ini sedang di edit oleh '+ response.user);
                return false;
            }
        },
        error: function (response) {
            $.unblockUI();
            if(response.status == 419) {
                window.location = $('#home_menu').attr('href');
            }
        }
    });
}

function validation_check_callback(downtime_category, downtime, keterangan, barcode_marker, callback_function) {
    if(keterangan) {
        if(!downtime) {
            $("#alert_warning").trigger("click", "waktu downtime tidak boleh kosong!");
            return false;
        }

        if(!downtime_category) {
            $("#alert_warning").trigger("click", "kategori downtime tidak boleh kosong!");
            return false;
        }
    }

    if(downtime) {
        if(!downtime_category) {
            $("#alert_warning").trigger("click", "kategori downtime tidak boleh kosong!");
            return false;
        }
    }

    callback_function(downtime_category, downtime, keterangan, barcode_marker);
}

function appendMarkerTumpuk(data) {
    $('#barcode-view-tumpuk').append(
        $('<div>').addClass('row').append(
            $('<div>').addClass('col-md-3').append(
                $('<input>').attr('type', 'text').addClass('form-control marker-tumpuk').attr('id', 'list_barcode_tumpuk').attr('name', 'list_barcode_tumpuk').attr('readonly', true).val(data.barcode_id)
            )
        ).append(
            $('<div>').addClass('col-md-5').append(
                $('<input>').attr('type', 'text').addClass('form-control').attr('readonly', true).val(data.style+' | '+data.article+' | '+data.po_buyer+' | '+data.cut)
            )
        ).append(
            $('<div>').addClass('col-md-3').append(
                $('<input>').attr('type', 'text').addClass('form-control bg-info').attr('readonly', true).val('Barcode Marker')
            )
        ).append(
            $('<div>').addClass('col-md-1').append(
                $('<button>').attr('type', 'button').addClass('btn btn-danger btn-sm marker-tumpuk-delete').attr('data-id', data.barcode_id).append(
                    $('<i>').addClass('icon-trash')
                )
            )
        )
    );
}
