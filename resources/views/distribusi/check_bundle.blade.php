@extends('layouts.scan',['active' => 'dashboard-scan'])

@section('page-css')
<style>
	.center-row {
		text-align: center !important;
	}
	.bg-red {
		background-color: red !important;
		color: white !important;
	}
	.bg-yellow {
		background-color: yellow !important;
	}
	.bg-green {
		background-color: green !important;
		color: white !important;
	}
	.bg-blue {
		background-color: blue !important;
		color: white !important;
	}
	.add-margin-right {
		margin-right: 20px;
	}

    .white-text {
        color: #ffffff !important;
        font-size: 30px;
    }

    .btn-scan {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .locator-title {
        margin-top: 10px;
        margin-bottom: 5px;

        text-size-adjust: 100pt;
    }

    .title-distribusi {
        padding-top: 0px !important;
    }

    .login-text{
        line-height: 36px;
        vertical-align:center !important;
    }

    .btn-custom-header {
        width: 80px;
    }


</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title text-center title-distribusi">
            <h1 class="white-text"><span class="text-semibold">SCAN LOCATOR {{$locator->locator_name}}</span> </h1>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="{{ route('distribusi.menu',Request::segment(2))}}"> Menu</a></li>
            {{-- <li><a href="{{ route('distribusi.checkIn',Request::segment(2))}}"> {{ucwords(strtolower($locator->locator_name))}}</a></li> --}}
            <li class="active">Check Bundle</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<fieldset class="content-group">
    <div class="row text-center">
        <div class="col-md-4">
            <a href="{{ route('distribusi.menu',Request::segment(2))}}" class="btn btn-sm btn-info btn-custom-header pull-left"> BACK </a>
        </div>

        <div class="col-md-4 login-text">
            <label class="control-label col-md-12 col-xs-12">Anda login sebagai, <strong>{{ Auth::user()->name }}<strong></label>
        </div>
        <div class="col-md-4">
            {{-- <a href="#" class="btn btn-sm btn-danger pull-right btn-custom-header "> Logout </a> --}}
            <a href="#"
            {{-- <a href="{{ route('logout') }}" --}}
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            class="btn btn-sm btn-danger pull-right btn-custom-header">
            Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</fieldset>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_input" name="barcode_input" placeholder="#Scan Barcode Komponen" autofocus autocomplete="off" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable" witdh="100%">
                <tbody>
                    <tr>
                        <th><strong>Barcode</strong></th>
                        <th colspan="6">: <span id="barcode">-</span></th>
                    <tr>
                    <tr>
                        <th><strong>Komponen</strong></th>
                        <th colspan="6">: <span id="komponen">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Style (Season)</strong></th>
                        <th colspan="6">: <span id="style">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>PO Buyer</strong></th>
                        <th colspan="6">: <span id="po_buyer">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Article</strong></th>
                        <th colspan="6">: <span id="article">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Size</strong></th>
                        <th colspan="6">: <span id="size">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Cut / No. Sticker</strong></th>
                        <th colspan="6">: <span id="cut_sticker">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Qty</strong></th>
                        <th colspan="6">: <span id="qty">-</span></th>
                    <tr>
                    </tr>
                        <th><strong>Process</strong></th>
                        <th hidden>: <span id="process"></span></th>
                        <th>: <span class="proc1" id="process1">-</span></th>
                        <th><span class="proc2" id="process2"></span></th>
                        <th><span class="proc3" id="process3"></span></th>
                        <th><span class="proc4" id="process4"></span></th>
                        <th><span class="proc5" id="process5"></span></th>
                        <th><span class="proc6" id="process6"></span></th>
                    <tr>
                    </tr>
                        <th hidden><strong>statusIn</strong></th>
                        <th hidden>: <span id="statusIn"></span></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                    <tr>
                    </tr>
                        <th hidden><strong>statusOut</strong></th>
                        <th hidden>: <span id="statusOut"></span></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                        <th hidden></th>
                    <tr>
                    </tr>
                        <th><strong>Bundling</strong></th>
                        <th colspan="6">: <span id="bundling">-</span></th>
                    </tr>
                    </tr>
                        <th id="th_id1" hidden class="lb_legends"><strong>LEGENDS</strong></th>
                        <th id="th_id2" hidden colspan="2" class="legend1">IN-OUT</th>
                        <th id="th_id3" hidden colspan="2" class="legend2">UNSCANNED!</th>
                        <th id="th_id4" hidden colspan="2" class="legend3">IN</th>
                    <tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('distribusi.bundleInfo') }}" id="bundleCheckLink"></a>
@endsection

@section('page-modal')
	{{-- @include('component_movement.scan_component._modal_locator') --}}
	@include('distribusi._modal_line_set')
@endsection

@section('page-js')
    <script>

        $(document).ready( function () {
            $('#barcode_input').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var barcode_input = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url: $('#bundleCheckLink').attr('href'),
                        data: {
                            barcode_input: barcode_input,
                        },
                        beforeSend: function(response) {
                            $.blockUI({
                                message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait',
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent',
                                },
                                theme: true,
                                baseZ: 2000
                            });
                            var barcode_temp = $('#barcode').text(response.barcode);
                            var barcode_cache = barcode_temp[0].innerText;
                            if(barcode_cache != barcode_input && barcode_cache != '-' || barcode_cache == barcode_input){
                                $('#process').text('');
                                $('#process1').text('');
                                $('#process2').text('');
                                $('#process3').text('');
                                $('#process4').text('');
                                $('#process5').text('');
                                $('#process6').text('');
                                $('#statusIn').text('');
                                $('#statusOut').text('');
                                $('.proc1').removeClass('text-success text-bold');
                                $('.proc1').removeClass('text-danger text-bold');
                                $('.proc1').removeClass('text-violet text-bold');
                                $('.proc2').removeClass('text-success text-bold');
                                $('.proc2').removeClass('text-danger text-bold');
                                $('.proc2').removeClass('text-violet text-bold');
                                $('.proc3').removeClass('text-success text-bold');
                                $('.proc3').removeClass('text-danger text-bold');
                                $('.proc3').removeClass('text-violet text-bold');
                                $('.proc4').removeClass('text-success text-bold');
                                $('.proc4').removeClass('text-danger text-bold');
                                $('.proc4').removeClass('text-violet text-bold');
                                $('.proc5').removeClass('text-success text-bold');
                                $('.proc5').removeClass('text-danger text-bold');
                                $('.proc5').removeClass('text-violet text-bold');
                                $('.proc6').removeClass('text-success text-bold');
                                $('.proc6').removeClass('text-danger text-bold');
                                $('.proc6').removeClass('text-violet text-bold');
                            }
                        },
                        success: function(response) {
                            $.unblockUI();
                            $('#barcode_input').val('').focus();
                            $('#barcode').text(barcode_input+' || AOI'+response.factory_id);
                            $('#komponen').text(response.komponen);
                            $('#style').text(response.style);
                            $('#po_buyer').text(response.po_buyer);
                            $('#article').text(response.article);
                            $('#size').text(response.size);
                            $('#process').text(response.process);
                            $('#cut_sticker').text(response.cut_sticker);
                            $('#statusIn').text(response.tir);
                            $('#statusOut').text(response.tor);
                            var proc= $('#process').text().split(',');
                            if(proc.length > 1)
                                $('#process1').text(proc[0]);
                                $('#process2').text(proc[1]);
                                $('#process3').text(proc[2]);
                                $('#process4').text(proc[3]);
                                $('#process5').text(proc[4]);
                                $('#process6').text(proc[5]);
                                var dmStatIn = $('#statusIn').text(response.tir);
                                var dmStatOut = $('#statusOut').text(response.tor);
                                var indmStat = dmStatIn[0].innerText;
                                var outdmStat = dmStatOut[0].innerText;
                                var proc1 = $('#process1').text(proc[0]);
                                var proc2 = $('#process2').text(proc[1]);
                                var proc3 = $('#process3').text(proc[2]);
                                var proc4 = $('#process4').text(proc[3]);
                                var proc5 = $('#process5').text(proc[4]);
                                var proc6 = $('#process5').text(proc[5]);
                                var procdetail1 = proc1[0].innerText;
                                var procdetail2 = proc2[0].innerText;
                                var procdetail3 = proc3[0].innerText;
                                var procdetail4 = proc4[0].innerText;
                                var procdetail5 = proc5[0].innerText;
                                var procdetail6 = proc6[0].innerText;

                                //process1
                                if(indmStat.indexOf(procdetail1) >= 0 && outdmStat.indexOf(procdetail1) == -1)
                                    //ONLY IN
                                    $('.proc1').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail1) >= 0 && outdmStat.indexOf(procdetail1) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc1').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail1) == -1 && outdmStat.indexOf(procdetail1) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc1').addClass('text-danger text-bold');

                                //process2
                                if(indmStat.indexOf(procdetail2) >= 0 && outdmStat.indexOf(procdetail2) == -1)
                                    //ONLY IN
                                    $('.proc2').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail2) >= 0 && outdmStat.indexOf(procdetail2) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc2').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail2) == -1 && outdmStat.indexOf(procdetail2) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc2').addClass('text-danger text-bold');

                                //process3
                                if(indmStat.indexOf(procdetail3) >= 0 && outdmStat.indexOf(procdetail3) == -1)
                                    //ONLY IN
                                    $('.proc3').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail3) >= 0 && outdmStat.indexOf(procdetail3) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc3').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail3) == -1 && outdmStat.indexOf(procdetail1) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc3').addClass('text-danger text-bold');

                                //process4
                                if(indmStat.indexOf(procdetail4) >= 0 && outdmStat.indexOf(procdetail4) == -1)
                                    //ONLY IN
                                    $('.proc4').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail4) >= 0 && outdmStat.indexOf(procdetail4) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc4').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail4) == -1 && outdmStat.indexOf(procdetail4) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc4').addClass('text-danger text-bold');

                                //process5
                                if(indmStat.indexOf(procdetail5) >= 0 && outdmStat.indexOf(procdetail5) == -1)
                                    //ONLY IN
                                    $('.proc5').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail5) >= 0 && outdmStat.indexOf(procdetail5) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc5').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail5) == -1 && outdmStat.indexOf(procdetail5) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc5').addClass('text-danger text-bold');
                                
                                //process6
                                if(indmStat.indexOf(procdetail6) >= 0 && outdmStat.indexOf(procdetail6) == -1)
                                    //ONLY IN
                                    $('.proc6').addClass('text-violet text-bold');
                                if(indmStat.indexOf(procdetail6) >= 0 && outdmStat.indexOf(procdetail6) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc6').addClass('text-success text-bold');
                                if(indmStat.indexOf(procdetail6) == -1 && outdmStat.indexOf(procdetail6) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc6').addClass('text-danger text-bold');
                            
                            if(proc.length == 1)
                                var dmStatIn = $('#statusIn').text(response.tir);
                                var dmStatOut = $('#statusOut').text(response.tor);
                                var indmStat = dmStatIn[0].innerText;
                                var outdmStat = dmStatOut[0].innerText;
                                var proc1 = $('#process1').text(proc[0]);
                                var procdetail1 = proc1[0].innerText;
                                $('#process1').text(proc[0])
                                if(indmStat.indexOf(procdetail1) >= 0 && outdmStat.indexOf(procdetail1) == -1)
                                    //ONLY IN
                                    $('.proc1').addClass('text-violet text-bold');

                                if(indmStat.indexOf(procdetail1) >= 0 && outdmStat.indexOf(procdetail1) >= 0)
                                    //IN OUT EXISTS
                                    $('.proc1').addClass('text-success text-bold');
                                
                                if(indmStat.indexOf(procdetail1) == -1 && outdmStat.indexOf(procdetail1) == -1)
                                    // IN OUT DOESN'T EXISTS
                                    $('.proc1').addClass('text-danger text-bold');

                            $('#th_id1').removeAttr('hidden');
                            $('#th_id2').removeAttr('hidden');
                            $('#th_id3').removeAttr('hidden');
                            $('#th_id4').removeAttr('hidden');
                            $('.lb_legends').addClass('text-bold bg-grey text-center');
                            $('.legend1').addClass('bg-success text-center');
                            $('.legend2').addClass('bg-danger text-center');
                            $('.legend3').addClass('bg-violet text-center');
                            $('#qty').text(response.qty)
                            $('#bundling').text(response.bundling);
                        },
                        error: function(response) {
                            $.unblockUI();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                            $('#barcode_input').val('').focus();
                        }
                    });
                }
            });
        });

    </script>
@endsection
