@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    body:not(.modal-open){
        padding-right: 0px !important;
    }
    .float-left {
        float: left;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Input Actual Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Input Actual Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="row">
            <button type="button" class="btn btn-primary col-md-4 change_scan">Normal</button>
            <button type="button" class="btn border-primary text-primary col-md-4 change_scan">Tumpuk</button>
            <button type="button" class="btn border-primary text-primary col-md-4 change_scan">Sambung</button>
        </div>
    </div>
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control" style="opacity: 1;" id="table_cutting_check" name="table_cutting_check" placeholder="#Scan Barcode Table" readonly="readonly" />
                    <input type="hidden" id="barcode_cutting_table" name="barcode_cutting_table" />
                    <input type="hidden" id="id_cutting_table" name="id_cutting_table" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Cutting Table" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="marker_check" name="marker_check" placeholder="#Scan Barcode Marker" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
        </div>
        <div id="scan2" class="hidden">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control" style="opacity: 1;" id="table_cutting_check_tumpuk" name="table_cutting_check_tumpuk" placeholder="#Scan Barcode Table" readonly="readonly" />
                    <input type="hidden" id="barcode_cutting_table_tumpuk" name="barcode_cutting_table_tumpuk" />
                    <input type="hidden" id="id_cutting_table_tumpuk" name="id_cutting_table_tumpuk" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Cutting Table" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <br>
            <div id="barcode-view-tumpuk"></div>
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new-tumpuk" style="opacity: 1;" id="marker_check_tumpuk" name="marker_check_tumpuk" placeholder="#Scan Barcode Marker" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <button class="btn btn-primary pull-right col-md-4" id="btn-spread-tumpuk">Spread</button>
            </div>
        </div>
        <div id="scan3" class="hidden">
            <div class="row">
               <center><h2>Masih Dalam Tahap Pengembangan</h2></center>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('spreading.actual_marker._modal_table')
    @include('spreading.actual_marker._modal_input')
    @include('spreading.actual_marker._downtime_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/input_actual_spreading.js') }}"></script>
@endsection
