<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\RequestRasio\FileMarker;
use App\Models\CuttingPlan;
use App\Models\Factory;
use App\Models\DetailPlan;
use App\Http\Controllers\Controller;

class UploadMarkerController extends Controller
{
    public function index()
    {
        $factorys = Factory::where('deleted_at', null)
            ->where('status', 'internal')
            ->orderBy('factory_alias', 'asc')
            ->get();

        return view('request_marker.upload_marker.index', compact('factorys'));
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = isset($request->factory) ? $request->factory : \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('is_header', true)
                        ->where('factory_id', $factory)
                        ->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('is_header', true)
                        ->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->addColumn('po', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                    }
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('status', function($data) {
                    if (count($data->file_markers) > 0) return '<span class="badge bg-primary">'.count($data->file_markers).' File</span>';
                    else return '<span class="badge bg-danger">Not Yet</span>';
                })
                ->addColumn('last_update', function($data) {
                    if (count($data->file_markers) > 0) return $data->file_markers->first()->updated_at;
                    else return '-';
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.upload_marker._action', [
                        'model'      => $data,
                        'upload'     => route('uploadMarker.uploadModal',$data->id)
                    ]);
                })
                ->rawColumns(['action', 'status', 'po'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['action'])
                ->make(true);
            }
        }
    }

    public function dataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $file_id = $request->file_id;

            if($file_id != NULL) {
                $data = FileMarker::orderBy('created_at', 'desc')
                    ->where('cutting_plan_id', $file_id);

                return datatables()->of($data)
                ->editColumn('user_id', function($data) {
                    return $data->user->name;
                })
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->created_at)->format('d/M/Y H:i');
                })
                ->editColumn('terima_user_id', function($data) {
                    if($data->terima_user_id == null) {
                        return '-';
                    } else {
                        return $data->terima_user->name;
                    }
                })
                ->editColumn('terima_at', function($data) {
                    if($data->terima_at == null) {
                        return '-';
                    } else {
                        return Carbon::parse($data->terima_at)->format('d/M/Y H:i');
                    }
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.upload_marker._action2', [
                        'model'      => $data,
                        'download'     => route('uploadMarker.download',$data->id),
                        'delete'     => route('uploadMarker.delete',$data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function uploadModal($id)
    {
        $upload = CuttingPlan::where('id', $id)->where('deleted_at', null)->first();


        if($upload->header_id != null && $upload->is_header) {
            $get_plan_id = CuttingPlan::where('header_id', $upload->id)
                ->whereNull('deleted_at')
                ->pluck('id')
                ->toArray();

            $po = implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray());
        } else {
            $po = implode(', ', $upload->po_details->pluck('po_buyer')->toArray());
        }


        // $po_buyer = implode(', ', $upload->po_details->pluck('po_buyer')->toArray());
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $upload->cutting_date;
        $obj->style = $upload->style;
        $obj->articleno = $upload->articleno;
        $obj->po_buyer = $po;
        $obj->size_category = $upload->size_category;
		$obj->url_upload = route('uploadMarker.upload');

		return response()->json($obj,200);
    }

    public function upload(Request $request)
    {
        $file = $request->file('file_marker');
        $file_name = $file->getClientOriginalName();
        $tujuan_upload = 'public/files/marker/'.Carbon::now()->format('Y').'/'.Carbon::now()->format('m').'/'.Carbon::now()->format('d');

        $path = $file->storeAs($tujuan_upload, $file_name);

        $upload_file = FileMarker::FirstOrCreate([
            'cutting_plan_id' => $request->upload_file_id,
            'file_name' => $file_name,
            'file_path' => $path,
            'user_id' => \Auth::user()->id,
        ]);
        return response()->json($upload_file, 200);
    }

    public function download($id)
    {
        $data = FileMarker::findOrFail($id);
        return response()->json($data->id, 200);
    }

    public function downloadFile($id)
    {
        $data = FileMarker::findOrFail($id);
        $path = storage_path('app/'.$data->file_path);
        return response()->download($path);
    }

    public function delete($id)
    {
        $data = FileMarker::findOrFail($id);
        Storage::disk('local')->delete($data->file_path);
        $data->delete();
        return response()->json(200);
    }
}
