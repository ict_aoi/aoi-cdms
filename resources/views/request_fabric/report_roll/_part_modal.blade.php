<div id="partModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Report Per Roll</span>
				</legend>
				<div class="row">
					<div class="col-md-6 col-xs-6 col-sm-6">
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="cutting_date_part_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="style_part_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-6 col-sm-6">
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="articleno_part_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-xs-10 col-sm-10">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-1 col-xs-2 col-sm-2">
								:
							</div>
							<div class="col-md-7 col-xs-12 col-sm-12">
								<span id="po_buyer_part_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="partTable">
						<thead>
							<tr>
								<th>Part No</th>
								<th>Detail</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="cutting_date_part" id="cutting_date_part" />
				<input type="hidden" name="style_part" id="style_part" />
				<input type="hidden" name="articleno_part" id="articleno_part" />
				<input type="hidden" name="po_buyer_part" id="po_buyer_part" value="[]" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				{{-- <button type="button" class="btn btn-primary" id="btn_accepted">Accept</button> --}}
			</div>
		</div>
	</div>
</div>

