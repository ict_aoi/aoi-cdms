<div id="reduceModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Report Per Roll</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="cutting_date_reduce_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="style_reduce_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="articleno_reduce_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="po_buyer_reduce_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Barcode</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="barcode_reduce_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Material</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="material_reduce_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>No Roll</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="no_roll_reduce_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Color</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="color_reduce_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Actual Width</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="width_reduce_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>LOT</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="lot_reduce_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Qty</strong></span>
							</div>
							<div class="col-md-1">
								:
							</div>
							<div class="col-md-7">
								<span id="qty_reduce_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
			<fieldset class="content-group">
					<div class="row">
						<div class="form-group col-md-6">
							<div class="row">
								<label class="control-label col-lg-12">Total Pengurangan</label>
								<div class="col-lg-12">
									<input type="number" id="jumlah_pengurangan" class="form-control" onkeypress="isNumberDot(event)" />
								</div>
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="row">
								<label class="control-label col-lg-12">Sisa</label>
								<div class="col-lg-12">
									<input type="number" id="sisa_pengurangan" class="form-control" readonly />
								</div>
							</div>
						</div>
                    </div>
                </fieldset>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btn_reduce">Reduce</button>
			</div>
		</div>
	</div>
</div>