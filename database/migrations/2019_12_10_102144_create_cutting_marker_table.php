<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingMarkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutting_marker', function (Blueprint $table) {
            $table->string('barcode_id', 15)->primary();
            $table->char('id_plan', 36);
            $table->string('part_no', 30);
            $table->integer('cut');
            $table->string('color')->nullable();
            $table->integer('marker')->nullable();
            $table->integer('layer')->nullable();
            $table->double('fabric_width', 15, 8)->nullable();
            $table->double('fabric_cons', 15, 8)->nullable();
            $table->double('perimater', 15, 8)->nullable();
            $table->double('marker_length', 15, 8)->nullable();
            $table->double('over_consum', 15, 8)->nullable();
            $table->double('need_total', 15, 8)->nullable();
            $table->double('qty_cut', 15, 8)->nullable();
            $table->double('qty_fabric', 15, 8)->nullable();
            $table->string('fabric_lot', 30)->nullable();
            $table->timestamp('download_at')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutting_marker');
    }
}
