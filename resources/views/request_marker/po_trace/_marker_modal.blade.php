<div id="markerModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Marker Trace</span>
				</legend>

                {{-- <div class="panel-heading"> --}}
                    <h5>Barcode <span id="detail_view_barcode_marker">-</span></h5>
                {{-- </div> --}}
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="headerTable">
                        <tbody>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td><span id="detail_view_style">Data Empty!</span></td>
                                <td><strong>Cut</strong></td>
                                <td><span id="detail_view_cut">Data Empty!</span></td>
                                <td><strong>Item</strong></td>
                                <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Deliv</strong></td>
                                <td><span id="detail_view_deliv">Data Empty!</span></td>
                                <td><strong>Marker Width (inch)</strong></td>
                                <td><span id="detail_view_width">Data Empty!</span></td>
                                <td rowspan="2"><strong>Color</strong></td>
                                <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Article</strong></td>
                                <td><span id="detail_view_article">Data Empty!</span></td>
                                <td><strong>Marker Length (inch)</strong></td>
                                <td><span id="detail_view_length">Data Empty!</span></td>
                                <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Marker Pcs</strong></td>
                                <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                                <td><strong>Total Length (+allow)</strong></td>
                                <td><span id="detail_view_allow">Data Empty!</span></td>
                                <td><strong>Part</strong></td>
                                <td><span id="detail_view_part">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Total Grmt</strong></td>
                                <td><span id="detail_view_total_garment">Data Empty!</span></td>
                                <td><strong>Layer</strong></td>
                                <td><span id="detail_view_layer">Data Empty!</span></td>
                                <td><strong>Plan</strong></td>
                                <td><span id="detail_view_plan">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Po Buyer</strong></td>
                                <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
			</div>
			<div class="modal-body">
                {{-- <div class="panel-heading"> --}}
                    <h5>History</h5>
                {{-- </div> --}}
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="lastActivityTable">
                            <thead>
                                <tr>
                                    <th class="change-bg-header">#</th>
                                    <th class="change-bg-header">Spreading</th>
                                    <th class="change-bg-header">Approve GL</th>
                                    <th class="change-bg-header">Approve QC</th>
                                    <th class="change-bg-header">Cutting</th>
                                    <th class="change-bg-header">Bundling</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="change-bg-header">Status</td>
                                    <td><span id="spreading_status">-</span></td>
                                    <td><span id="approve_gl_status">-</span></td>
                                    <td><span id="approve_qc_status">-</span></td>
                                    <td><span id="cutting_status">-</span></td>
                                    <td><span id="bundling_status">-</span></td>
                                </tr>
                                <tr>
                                    <td class="change-bg-header">Date/Finish</td>
                                    <td><span id="spreading_date">-</span></td>
                                    <td><span id="approve_gl_date">-</span></td>
                                    <td><span id="approve_qc_date">-</span></td>
                                    <td><span id="cutting_date">-</span></td>
                                    <td><span id="bundling_date">-</span></td>
                                </tr>
                                <tr>
                                    <td class="change-bg-header">By</td>
                                    <td><span id="spreading_by">-</span></td>
                                    <td><span id="approve_gl_by">-</span></td>
                                    <td><span id="approve_qc_by">-</span></td>
                                    <td><span id="cutting_by">-</span></td>
                                    <td><span id="bundling_by">-</span></td>
                                </tr>
                                <tr>
                                    <td class="change-bg-header">Table</td>
                                    <td><span id="spreading_table">-</span></td>
                                    <td><span>-</span></td>
                                    <td><span>-</span></td>
                                    <td><span id="cutting_table">-</span></td>
                                    <td><span id="bundling_table">-</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				{{-- <button type="button" class="btn btn-primary" id="btn_accepted">Accept</button> --}}
			</div>
		</div>
	</div>
</div>

