<div id="inputModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Spreading Report Input - <span id="detail_view_barcode_marker">Data Empty!</span></span>
				</legend>
			</div>
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-10">
                        <input type="text" class="form-control" style="opacity: 1;" id="fabric_check" name="fabric_check" placeholder="#Scan Barcode Fabric" autocomplete="off" />
                    </div>
                    <div class="col-md-2">
                        <input type="text" value="Barcode Fabric" class="form-control bg-info" readonly="readonly" />
                    </div>
                </div>
			</div>
            <br />
            <div class="modal-header">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="headerTable">
						<tbody>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td><span id="detail_view_style">Data Empty!</span></td>
                                <td><strong>Cut</strong></td>
                                <td><span id="detail_view_cut">Data Empty!</span></td>
                                <td><strong>Item</strong></td>
                                <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Deliv</strong></td>
                                <td><span id="detail_view_deliv">Data Empty!</span></td>
                                <td><strong>Marker Width (inch)</strong></td>
                                <td><span id="detail_view_width">Data Empty!</span></td>
                                <td rowspan="2"><strong>Color</strong></td>
                                <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Article</strong></td>
                                <td><span id="detail_view_article">Data Empty!</span></td>
                                <td><strong>Marker Length (inch)</strong></td>
                                <td><span id="detail_view_length">Data Empty!</span></td>
                                <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Marker Pcs</strong></td>
                                <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                                <td><strong>Total Length (+allow)</strong></td>
                                <td><span id="detail_view_allow">Data Empty!</span></td>
                                <td><strong>Part</strong></td>
                                <td><span id="detail_view_part">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Total Grmt</strong></td>
                                <td><span id="detail_view_total_garment">Data Empty!</span></td>
                                <td><strong>layer</strong></td>
                                <td><span id="detail_view_layer">Data Empty!</span></td>
                                <td><strong>Plan</strong></td>
                                <td><span id="detail_view_plan">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Po Buyer</strong></td>
                                <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                            </tr>
                        </tbody>
					</table>
				</div>
			</div>
			<div class="modal-body">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="inputTable">
						<thead>
                            <tr>
                                <th rowspan="2" class="remove-padding-side">No Roll</th>
                                <th rowspan="2" class="remove-padding-side">ID</th>
                                <th rowspan="2" class="remove-padding-side">Lot</th>
                                <th rowspan="2" class="remove-padding-side">Yard In Roll</th>
                                <th rowspan="2" class="remove-padding-side">Lebar Aktual (inch)</th>
                                <th colspan="2" class="remove-padding-side"><center>Layer</center></th>
                                <th colspan="2" class="remove-padding-side">Sambungan (yard)</th>
                                <th rowspan="2" class="remove-padding-side">Akumulasi Layer</th>
                                <th rowspan="2" class="remove-padding-side">Aktual Pemakaian (yard)</th>
                                <th rowspan="2" class="remove-padding-side">Reject (yard)</th>
                                <th colspan="2" class="remove-padding-side"><center>Sisa (yard)</center></th>
                                <th rowspan="2" class="remove-padding-side">Remark</th>
                                <th rowspan="2" class="remove-padding-side">Catatan</th>
                                <th rowspan="2" class="remove-padding-side">Saved</th>
                            </tr>
                            <tr>
                                <th class="remove-padding-side">Suplai</th>
                                <th class="remove-padding-side">Aktual</th>
                                <th class="remove-padding-side">Awal</th>
                                <th class="remove-padding-side">Akhir</th>
                                <th class="remove-padding-side">Suplai</th>
                                <th class="remove-padding-side">Aktual</th>
                            </tr>
                        </thead>
					</table>
				</div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger float-left" id="delete_last_fabric">Delete</button>
                <button type="button" class="btn btn-success float-left" id="save_laporan_speading">save</button>
                <button type="button" class="btn btn-default" id="delete_all_spreading">Cancel</button>
                <button type="button" class="btn btn-primary" id="insert_laporan_speading">Commit</button>
            </div>
		</div>
	</div>
</div>
