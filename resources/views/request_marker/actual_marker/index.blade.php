@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .no-padding {
        padding: 0px;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Actual Marker</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Actual Marker</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
                <div class="form-group">
                    
                    @if(\Auth::user()->factory_id == '0')

                        <label class="control-label col-lg-2 text-bold">Pilih Factory</label>

                        <div class="col-lg-10">

                            @foreach($factorys as $factory)

                            <label class="radio-inline">
                                <div><input type="radio" name="factory" value="{{ $factory->id }}" required></div>
                                {{ $factory->factory_alias }}
                            </label>

                            @endforeach

                        </div>
                    
                    @endif
                </div>
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Input Cutting Date</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Enter Cutting Date" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" id='bt_select' name='bt_select' value='1' type="submit">Select</button>
							</span>
                            {{--<span class="input-group-btn">
								<button class="btn btn-warning hidden legitRipple" id="down_ratio" name="down_ratio"><i class="icon-download position-left"> RATIO</i></button>
							</span>--}}
                            <span class="input-group-btn">
								<button class="btn btn-success hidden legitRipple" id="down_rep" name="down_rep"><i class="icon-download position-left"> REPORT</i></button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
                        <th>Queue</th>
                        <th>Style</th>
                        <th>Season</th>
                        <th>Article</th>
                        <th>PO Buyer</th>
                        <th>Color</th>
                        <th>Size Type</th>
                        <th>Factory</th>
                        <th>Status</th>
                        <th>Last Updated</th>
                        <th>Detail</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('actualMarker.addNote') }}" id="NoteLink"></a>

@endsection

@section('page-modal')
	@include('request_marker.actual_marker._detail_modal')
	@include('request_marker.actual_marker._detail_modal_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/actual_marker.js') }}"></script>
<script src="{{ mix('js/notif_mo_update.js') }}"></script>
<script type="text/javascript">
$('#bt_select').on('click',function(){
    var bt_val      = $('#bt_select').val();
    var cut_date    = $('input[name="cutting_date"]').val();
    var factory     = $("input[name='factory']:checked").val();
    var user        = "{{Auth::user()->factory_id}}";
    if(user == 0){
        if(bt_val == '1' && cut_date != ''){
            if($('#down_rep').hasClass('hidden')){
                // $('#down_ratio').removeClass('hidden');
                $('#down_rep').removeClass('hidden');
            }
        }
    }else{
        if(bt_val == '1' && cut_date != ''){
            // if($('#down_ratio').hasClass('hidden') && $('#down_rep').hasClass('hidden')){
            if($('#down_rep').hasClass('hidden')){
                // $('#down_ratio').removeClass('hidden');
                $('#down_rep').removeClass('hidden');
            }
        }
    }
});
// $('#down_ratio').on('click', function(e){
//     var cut_date = $('#cutting_date').val();
//     var factory = $("input[name='factory']:checked").val();
//     if(factory === undefined){
//         var factory_id   = "{{Auth::user()->factory_id}}";
//     }else{
//         var factory_id   = factory;
//     }
//     window.location.href = '/actual-marker/download-ratio-cutdate/'+cut_date+'/'+factory_id;
// });
$('#down_rep').on('click', function(e){
    var cut_date        = $('#cutting_date').val();
    var factory_id      = $("input[name='factory']:checked").val();
    if(factory_id === undefined){
        var factory_id   = "{{Auth::user()->factory_id}}";
    }else{
        var factory_id   = factory_id;
    }
    window.location.href = '/actual-marker/download-report-cutdate/'+cut_date+'/'+factory_id;
});
</script>
@endsection
