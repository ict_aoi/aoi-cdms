<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterSpreading extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_spreading';
	protected $fillable = ['type','target','created_at','updated_at','deleted_at'];
}
