<?php

namespace App\Http\Controllers\General;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\RequestRasio\FileExcel;
use App\Models\CuttingPlan;
use App\Models\Info;
use App\Http\Controllers\Controller;


class NotifController extends Controller
{
    public function data()
    {
        if(request()->ajax()) 
        {
            $day1 = Carbon::now();
            $day2 = Carbon::now()->addWeekdays(1);
            $day3 = Carbon::now()->addWeekdays(2);
            $day4 = Carbon::now()->addWeekdays(3);
            $data = [
                'd1_label' => $day1->format('l, d F Y'),
                'd2_label' => $day2->format('l, d F Y'),
                'd3_label' => $day3->format('l, d F Y'),
                'd4_label' => $day4->format('l, d F Y'),
                'd1_data' => DB::table('jaz_notif')->where('cutting_date', $day1->format('Y-m-d'))->whereBetween('date', [$day1->subDays(2)->format('Y-m-d'), $day1->format('Y-m-d')])->get(),
                'd2_data' => DB::table('jaz_notif')->where('cutting_date', $day2->format('Y-m-d'))->whereBetween('date', [$day1->subDays(2)->format('Y-m-d'), $day1->format('Y-m-d')])->get(),
                'd3_data' => DB::table('jaz_notif')->where('cutting_date', $day3->format('Y-m-d'))->whereBetween('date', [$day1->subDays(2)->format('Y-m-d'), $day1->format('Y-m-d')])->get(),
                'd4_data' => DB::table('jaz_notif')->where('cutting_date', $day4->format('Y-m-d'))->whereBetween('date', [$day1->subDays(2)->format('Y-m-d'), $day1->format('Y-m-d')])->get(),
            ];
            return response()->json($data);
        }
    }
}
