@if (isset($upload))
    <a href="#" onclick="uploadModal('{!! $upload !!}')" class="btn btn-default btn-sm"><i class="icon-file-text"></i></a>
@endif
@if (isset($download))
    <li><a href="#" onclick="download('{!! $download !!}')"><i class="icon-folder-download3"></i> Download</a></li>
@endif
@if (isset($delete))
    <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-close2"></i> Delete</a></li>
@endif
@if (isset($edit_modal))
    <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
@endif