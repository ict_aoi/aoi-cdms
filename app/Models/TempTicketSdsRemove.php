<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TempTicketSdsRemove extends Model
{
    use Uuids;
	public $incrementing    = false;
	protected $guarded      = ['id'];
	protected $table        = 'temp_ticket_sds_remove';
	protected $fillable     = ['barcode_id','season','style','poreference','article','cut_num','size','part_num','part_name','start_num','end_num','qty','factory_id','created_by','note'];
    protected $dates        = ['created_at','updated_at'];


	public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
}
