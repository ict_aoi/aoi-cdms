<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuttingMovement extends Model
{
	protected $guarded = ['id'];
	protected $table = 'cutting_movements';
	protected $fillable = ['barcode_id', 'process_from', 'status_from', 'process_to', 'status_to', 'deleted_at', 'is_canceled', 'user_id', 'description', 'ip_address', 'barcode_type'];
}
