@extends('layouts.app',['active' => 'report_fabric_ci'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Daily Cutting Instruction</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Daily Cutting Instruction</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
        </form>
        {{-- <button type="button" class="btn btn-success btn-sm pull-right add-margin-right" id="download_report"><i class="icon-download"></i> Download</button> --}}
	</div>
</div>

<div class="panel panel-flat">
    {{-- <div class="panel-heading"> --}}
		{{-- <h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6> --}}
	{{-- </div> --}}
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic table-striped table-hover table-responsive" id="planningTable">
                <thead>
                    <tr>
						<th>ID</th>
						<th>Planning Date</th>
						<th>Warehouse Preparation</th>
                        <th>Article</th>
                        <th>Style</th>
                        <th>PO Buyer</th>
                        <th>Item Code</th>
                        <th>UOM</th>
                        <th>Total Need</th>
                        <th>Total Relax</th>
                        <th>Total Out</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

{{-- <form action="{{ route('reportFabric.downloadReport') }}" method="post" target="_blank" id="download_report_submit">
	@csrf}
	<input id="data_cutting_date" name="data_cutting_date" type="hidden" value="">
	<div class="" style="">
		<button type="submit" class="btn hidden">download</button>
	</div>
</form> --}}

@endsection

@section('page-modal')
	{{-- @include('request_fabric.report_ci._detail_modal')
	@include('request_fabric.report_ci._detail_modal_modal') --}}
@endsection

@section('page-js')
<script>
    $(document).ready( function () {

        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });

        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            paging: false,
            // serverSide: true, //sync
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });

        var table = $('#planningTable').DataTable({
            ajax: {
                type: 'get',
                url: '/report-ci/data-ci',
                data: {
                    cutting_date: $('#cutting_date').val(),
                },
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
            ],
            columns: [
                {
					data: 'id',
					name: 'id',
					searchable: true,
					visible: false,
					orderable: false
				},
				{
					data: 'planning_date',
					name: 'planning_date',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'warehouse_id',
					name: 'warehouse_id',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'article_no',
					name: 'article_no',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'style',
					name: 'style',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'po_buyer',
					name: 'po_buyer',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'item_code',
					name: 'item_code',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'uom',
					name: 'uom',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'qty_need',
					name: 'qty_need',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'total_relax',
					name: 'total_relax',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'total_out',
					name: 'total_out',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'status',
					name: 'status',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'action',
					name: 'source',
					searchable: false,
					visible: true,
					orderable: false
				},
			],

            // columns: [
            //     {data: 'queu', sortable: 'queu', orderable: true, searchable: false},
            //     {data: 'style', name: 'style',searchable:true,orderable:true},
            //     {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            //     {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            //     {data: 'action', name: 'action',searchable:true,orderable:true},
            //     // {data: 'material', name: 'material',searchable:true,orderable:true},
            //     // {data: 'color', name: 'color',searchable:true,orderable:true},
            //     // {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            //     // {data: 'csi_qty', name: 'csi_qty',searchable:true,orderable:true},
            //     // {data: 'ssp_qty', name: 'ssp_qty',searchable:true,orderable:true},
            //     // {data: 'over_consum', name: 'over_consum',searchable:true,orderable:true},
            //     // {data: 'already_prepared', name: 'already_prepared',searchable:true,orderable:true},
            //     // {data: 'balance_whs', name: 'balance_whs',searchable:true,orderable:true},
            //     // {data: 'total_balance', name: 'total_balance',searchable:true,orderable:true},
            //     // {data: 'remark', name: 'remark',searchable:true,orderable:true},
            // ],
        });

        var dtable = $('#planningTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();

        $('#planningTable').on('click','.ignore-click', function() {
            return false;
        });

        $('#form_filter_date').submit(function(event){
            event.preventDefault();
            var cutting_date = $('#cutting_date').val();

            if(!cutting_date){
                $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'get',
                url: '/report-ci/data-ci',
                data: {
                    cutting_date: cutting_date,
                },
                beforeSend: function() {
                    $('#planningTable').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('#planningTable').unblock();

                    table.clear();
                    table.rows.add(response['data']);
                    table.draw();
                },
                error: function(response) {
                    $('#planningTable').unblock();
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });

        // download report

        $('#download_report').on('click', function() {
            var cutting_date = $('#cutting_date').val();
            // var data = Array();
            // $('#planningTable tbody tr').each(function(i, v) {
            //     data[i] = Array();
            //     $(this).children('td').each(function(ii, vv) {
            //         data[i][ii] = $(this).text();
            //     });
            // });

            // if(data.length > 1) {
                // $('#data_download').val(JSON.stringify(data));
                $('#data_cutting_date').val(cutting_date);
                $('#download_report_submit').submit();
            // }
        });
    });
</script>
@endsection
