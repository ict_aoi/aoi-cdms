<?php

namespace App\Http\Controllers\Report;

use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use App\Models\DistribusiMovement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PdsController extends Controller
{
    public function index()
    {
        return view('report.pds.index',compact('month'));
    }

    public function downloadPds($months,$years,$factory_id)
    {
        $month      = (int)$months;
        $year       = (int)$years;
        $factory_id = $factory_id;

        // $data = DB::connection('sds_live')->select(DB::raw("SELECT y.season,y.style,y.poreference,y.article,prev_tb.qty_cut as prev_cut,
        // SUM(y.qty_cut) AS qty_cut,
        // CASE WHEN prev_tb.qty_cut IS NULL THEN 0 + SUM(y.qty_cut) ELSE prev_tb.qty_cut + SUM(y.qty_cut) END AS total_cut,
        // prev_tb.qty_he AS prev_he,
        // SUM(y.qty_he) AS qty_he,
        // CASE WHEN prev_tb.qty_he IS NULL THEN 0 + SUM(y.qty_he) ELSE prev_tb.qty_he + SUM(y.qty_he) END AS total_he,
        // prev_tb.qty_ppa AS prev_ppa,
        // SUM(y.qty_ppa) AS qty_ppa,
        // CASE WHEN prev_tb.qty_ppa IS NULL THEN 0 + SUM(y.qty_ppa) ELSE prev_tb.qty_ppa + SUM(y.qty_ppa) END AS total_ppa,
        // prev_tb.qty_pad AS prev_pad,
        // SUM(y.qty_pad) AS qty_pad,
        // CASE WHEN prev_tb.qty_pad IS NULL THEN 0 + SUM(y.qty_pad) ELSE prev_tb.qty_pad + SUM(y.qty_pad) END AS total_pad,
        // prev_tb.qty_artwork AS prev_artwork,
        // SUM(y.qty_artwork) AS qty_artwork,
        // CASE WHEN prev_tb.qty_artwork IS NULL THEN 0 + SUM(y.qty_artwork) ELSE prev_tb.qty_artwork + SUM(y.qty_artwork) END AS total_artwork,
        // prev_tb.qty_fuse AS prev_fuse,
        // SUM(y.qty_fuse) AS qty_fuse,
        // CASE WHEN prev_tb.qty_fuse IS NULL THEN 0 + SUM(y.qty_fuse) ELSE prev_tb.qty_fuse + SUM(y.qty_fuse) END AS total_fuse,
        // prev_tb.qty_bobok AS prev_bobok,
        // SUM(y.qty_bobok) AS qty_bobok,
        // CASE WHEN prev_tb.qty_bobok IS NULL THEN 0 + SUM(y.qty_bobok) ELSE prev_tb.qty_bobok + SUM(y.qty_bobok) END AS total_bobok,
        // prev_tb.qty_bonding AS prev_bonding,
        // SUM(y.qty_bonding) AS qty_bonding,
        // CASE WHEN prev_tb.qty_bonding IS NULL THEN 0 + SUM(y.qty_bonding) ELSE prev_tb.qty_bonding + SUM(y.qty_bonding) END AS total_bonding,
        // prev_tb.qty_setting AS prev_setting,
        // SUM(y.qty_setting) AS qty_setting,
        // CASE WHEN prev_tb.qty_setting IS NULL THEN 0 + SUM(y.qty_setting) ELSE prev_tb.qty_setting + SUM(y.qty_setting) END AS total_setting,
        // prev_tb.qty_supply AS prev_supply,
        // SUM(y.qty_supply) AS qty_supply,
        // CASE WHEN prev_tb.qty_supply IS NULL THEN 0 + SUM(y.qty_supply) ELSE prev_tb.qty_supply + SUM(y.qty_supply) END AS total_supply
        // FROM(SELECT DISTINCT x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,
        // FIRST_VALUE(x.qty_cut) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_cut)qty_cut,
        // FIRST_VALUE(x.qty_he) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_he)qty_he,
        // FIRST_VALUE(x.qty_ppa) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_ppa)qty_ppa,
        // FIRST_VALUE(x.qty_artwork) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_artwork)qty_artwork,
        // FIRST_VALUE(x.qty_pad) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_pad)qty_pad,
        // FIRST_VALUE(x.qty_fuse) OVER(PARTITION BY
        // x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_fuse)qty_fuse,
        // FIRST_VALUE(x.qty_bobok) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bobok)qty_bobok,
        // FIRST_VALUE(x.qty_bonding) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bonding)qty_bonding,
        // FIRST_VALUE(x.qty_setting) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_setting)qty_setting,
        // FIRST_VALUE(x.qty_supply) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_supply)qty_supply
        // FROM(SELECT bi.barcode_id,bi.season,
        // CASE WHEN ad.set_type = '0' THEN bi.style WHEN ad.set_type = 'BOTTOM' THEN CONCAT(bi.style,'-BOT') ELSE CONCAT(bi.style,'-TOP') END AS style,
        // bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
        // CASE WHEN bi.distrib_received IS NULL THEN 0 ELSE bi.qty END AS qty_cut,
        // CASE WHEN ad.he IS TRUE AND bi.he_out IS NOT NULL THEN bi.qty WHEN ad.he IS TRUE AND bi.he_out IS NULL THEN 0 ELSE NULL END AS qty_he,
        // CASE WHEN ad.ppa IS TRUE AND bi.ppa_out IS NOT NULL THEN bi.qty WHEN ad.ppa IS TRUE AND bi.ppa_out IS NULL THEN 0 ELSE NULL END AS qty_ppa,
        // CASE WHEN ad.artwork IS TRUE AND bi.artwork_out IS NOT NULL THEN bi.qty WHEN ad.artwork IS TRUE AND bi.artwork_out IS NULL THEN 0 ELSE NULL END AS qty_artwork,
        // CASE WHEN ad.pad IS TRUE AND bi.pad_out IS NOT NULL THEN bi.qty WHEN ad.pad IS TRUE AND bi.pad_out IS NULL THEN 0 ELSE NULL END AS qty_pad,
        // CASE WHEN ad.fuse IS TRUE AND bi.fuse_out IS NOT NULL THEN bi.qty WHEN ad.fuse IS TRUE AND bi.fuse_out IS NULL THEN 0 ELSE NULL END AS qty_fuse,
        // CASE WHEN ad.bobok IS TRUE AND bi.bobok_out IS NOT NULL THEN bi.qty WHEN ad.bobok IS TRUE AND bi.bobok_out IS NULL THEN 0 ELSE NULL END AS qty_bobok,
        // CASE WHEN ad.bonding IS TRUE AND bi.bonding_out IS NOT NULL THEN bi.qty WHEN ad.bonding IS TRUE AND bi.bonding_out IS NULL THEN 0 ELSE NULL END AS qty_bonding,
        // CASE WHEN bi.setting IS NOT NULL THEN bi.qty ELSE 0 END AS qty_setting,
        // CASE WHEN bi.supply IS NOT NULL THEN bi.qty ELSE 0 END AS qty_supply,
        // bi.create_date
        // FROM bundle_info_new bi
        // LEFT JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
        // WHERE EXTRACT(MONTH FROM bi.create_date) = '$month' and EXTRACT(YEAR FROM bi.create_date) = '$year' and bi.processing_fact='$factory_id')x)y
        // -- PREVIOUS TABLE
        // LEFT JOIN(SELECT y.season,y.style,y.poreference,y.article,
        // SUM(y.qty_cut) as qty_cut,
        // SUM(y.qty_he) as qty_he,
        // SUM(y.qty_ppa) as qty_ppa,
        // SUM(y.qty_pad) as qty_pad,
        // SUM(y.qty_artwork) as qty_artwork,
        // SUM(y.qty_fuse) as qty_fuse,
        // SUM(y.qty_bobok) as qty_bobok,
        // SUM(y.qty_bonding) as qty_bonding,
        // SUM(y.qty_setting) as qty_setting,
        // SUM(y.qty_supply) as qty_supply
        // FROM(SELECT DISTINCT x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,
        // FIRST_VALUE(x.qty_cut) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_cut)qty_cut,
        // FIRST_VALUE(x.qty_he) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_he )qty_he,
        // FIRST_VALUE(x.qty_ppa) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_ppa)qty_ppa,
        // FIRST_VALUE(x.qty_artwork) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_artwork)qty_artwork,
        // FIRST_VALUE(x.qty_pad) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_pad)qty_pad,
        // FIRST_VALUE(x.qty_fuse) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_fuse)qty_fuse,
        // FIRST_VALUE(x.qty_bobok) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bobok)qty_bobok,
        // FIRST_VALUE(x.qty_bonding) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bonding)qty_bonding,
        // FIRST_VALUE(x.qty_setting) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_setting)qty_setting,
        // FIRST_VALUE(x.qty_supply) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_supply)qty_supply
        // FROM(SELECT bi.barcode_id,bi.season,
        // CASE WHEN ad.set_type = '0' THEN bi.style WHEN ad.set_type = 'BOTTOM' THEN CONCAT(bi.style,'-BOT') ELSE CONCAT(bi.style,'-TOP') END AS style,
        // bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
        // CASE WHEN bi.distrib_received IS NULL THEN 0 ELSE bi.qty END AS qty_cut,
        // CASE WHEN ad.he IS TRUE AND bi.he_out IS NOT NULL THEN bi.qty WHEN ad.he IS TRUE AND bi.he_out IS NULL THEN 0 ELSE NULL END AS qty_he,
        // CASE WHEN ad.ppa IS TRUE AND bi.ppa_out IS NOT NULL THEN bi.qty WHEN ad.ppa IS TRUE AND bi.ppa_out IS NULL THEN 0 ELSE NULL END AS qty_ppa,
        // CASE WHEN ad.artwork IS TRUE AND bi.artwork_out IS NOT NULL THEN bi.qty WHEN ad.artwork IS TRUE AND bi.artwork_out IS NULL THEN 0 ELSE NULL END AS qty_artwork,
        // CASE WHEN ad.pad IS TRUE AND bi.pad_out IS NOT NULL THEN bi.qty WHEN ad.pad IS TRUE AND bi.pad_out IS NULL THEN 0 ELSE NULL END AS qty_pad,
        // CASE WHEN ad.fuse IS TRUE AND bi.fuse_out IS NOT NULL THEN bi.qty WHEN ad.fuse IS TRUE AND bi.fuse_out IS NULL THEN 0 ELSE NULL END AS qty_fuse,
        // CASE WHEN ad.bobok IS TRUE AND bi.bobok_out IS NOT NULL THEN bi.qty WHEN ad.bobok IS TRUE AND bi.bobok_out IS NULL THEN 0 ELSE NULL END AS qty_bobok,
        // CASE WHEN ad.bonding IS TRUE AND bi.bonding_out IS NOT NULL THEN bi.qty WHEN ad.bonding IS TRUE AND bi.bonding_out IS NULL THEN 0 ELSE NULL END AS qty_bonding,
        // CASE WHEN bi.setting IS NOT NULL THEN bi.qty ELSE 0 END AS qty_setting,
        // CASE WHEN bi.supply IS NOT NULL THEN bi.qty ELSE 0 END AS qty_supply,
        // bi.create_date
        // FROM bundle_info_new bi 
        // JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
        // where (bi.season,bi.style,bi.poreference,bi.article,bi.size,bi.part_num,bi.part_name) in
        // (SELECT DISTINCT season,style,poreference,article,size,part_num,part_name
        // FROM bundle_info_new
        // WHERE EXTRACT(MONTH FROM create_date) < '$month' and EXTRACT(YEAR FROM create_date) = '$year' and processing_fact='$factory_id' and factory <> 'GEN')
        // AND EXTRACT(MONTH FROM bi.create_date) = '$month' and EXTRACT(YEAR FROM bi.create_date) = '$year' and bi.processing_fact='$factory_id' and bi.factory <> 'GEN')x)y
        // GROUP BY y.season,y.style,y.poreference,y.article)prev_tb USING(season,style,poreference,article)
        // GROUP BY y.season,y.style,y.poreference,y.article,prev_tb.qty_cut,prev_tb.qty_he,prev_tb.qty_ppa,prev_tb.qty_pad,prev_tb.qty_artwork,prev_tb.qty_fuse,prev_tb.qty_bobok,prev_tb.qty_bonding,prev_tb.qty_setting,prev_tb.qty_supply"));

        $data = DB::connection('sds_live')->select(DB::raw("SELECT y.season,y.style,y.poreference,y.article,prev_tb.qty_cut as prev_cut,
        SUM(y.qty_cut) AS qty_cut,
        CASE WHEN prev_tb.qty_cut IS NULL THEN 0 + SUM(y.qty_cut) ELSE prev_tb.qty_cut + SUM(y.qty_cut) END AS total_cut,
        prev_tb.qty_he AS prev_he,
        SUM(y.qty_he) AS qty_he,
        CASE WHEN prev_tb.qty_he IS NULL THEN 0 + SUM(y.qty_he) ELSE prev_tb.qty_he + SUM(y.qty_he) END AS total_he,
        prev_tb.qty_ppa AS prev_ppa,
        SUM(y.qty_ppa) AS qty_ppa,
        CASE WHEN prev_tb.qty_ppa IS NULL THEN 0 + SUM(y.qty_ppa) ELSE prev_tb.qty_ppa + SUM(y.qty_ppa) END AS total_ppa,
        prev_tb.qty_pad AS prev_pad,
        SUM(y.qty_pad) AS qty_pad,
        CASE WHEN prev_tb.qty_pad IS NULL THEN 0 + SUM(y.qty_pad) ELSE prev_tb.qty_pad + SUM(y.qty_pad) END AS total_pad,
        prev_tb.qty_artwork AS prev_artwork,
        SUM(y.qty_artwork) AS qty_artwork,
        CASE WHEN prev_tb.qty_artwork IS NULL THEN 0 + SUM(y.qty_artwork) ELSE prev_tb.qty_artwork + SUM(y.qty_artwork) END AS total_artwork,
        prev_tb.qty_fuse AS prev_fuse,
        SUM(y.qty_fuse) AS qty_fuse,
        CASE WHEN prev_tb.qty_fuse IS NULL THEN 0 + SUM(y.qty_fuse) ELSE prev_tb.qty_fuse + SUM(y.qty_fuse) END AS total_fuse,
        prev_tb.qty_bobok AS prev_bobok,
        SUM(y.qty_bobok) AS qty_bobok,
        CASE WHEN prev_tb.qty_bobok IS NULL THEN 0 + SUM(y.qty_bobok) ELSE prev_tb.qty_bobok + SUM(y.qty_bobok) END AS total_bobok,
        prev_tb.qty_bonding AS prev_bonding,
        SUM(y.qty_bonding) AS qty_bonding,
        CASE WHEN prev_tb.qty_bonding IS NULL THEN 0 + SUM(y.qty_bonding) ELSE prev_tb.qty_bonding + SUM(y.qty_bonding) END AS total_bonding,
        prev_tb.qty_setting AS prev_setting,
        SUM(y.qty_setting) AS qty_setting,
        CASE WHEN prev_tb.qty_setting IS NULL THEN 0 + SUM(y.qty_setting) ELSE prev_tb.qty_setting + SUM(y.qty_setting) END AS total_setting,
        prev_tb.qty_supply AS prev_supply,
        SUM(y.qty_supply) AS qty_supply,
        CASE WHEN prev_tb.qty_supply IS NULL THEN 0 + SUM(y.qty_supply) ELSE prev_tb.qty_supply + SUM(y.qty_supply) END AS total_supply
        FROM(SELECT DISTINCT x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,
        FIRST_VALUE(x.qty_cut) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_cut)qty_cut,
        FIRST_VALUE(x.qty_he) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_he)qty_he,
        FIRST_VALUE(x.qty_ppa) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_ppa)qty_ppa,
        FIRST_VALUE(x.qty_artwork) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_artwork)qty_artwork,
        FIRST_VALUE(x.qty_pad) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_pad)qty_pad,
        FIRST_VALUE(x.qty_fuse) OVER(PARTITION BY
        x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_fuse)qty_fuse,
        FIRST_VALUE(x.qty_bobok) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bobok)qty_bobok,
        FIRST_VALUE(x.qty_bonding) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bonding)qty_bonding,
        FIRST_VALUE(x.qty_setting) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_setting)qty_setting,
        FIRST_VALUE(x.qty_supply) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_supply)qty_supply
        FROM(SELECT bi.barcode_id,bi.season,
        CASE WHEN ad.set_type = '0' THEN bi.style WHEN ad.set_type = 'BOTTOM' THEN CONCAT(bi.style,'-BOT') ELSE CONCAT(bi.style,'-TOP') END AS style,
        bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
        CASE WHEN bi.distrib_received IS NULL THEN 0 ELSE bi.qty END AS qty_cut,
        CASE WHEN ad.he IS TRUE AND bi.he_out IS NOT NULL THEN bi.qty WHEN ad.he IS TRUE AND bi.he_out IS NULL THEN 0 ELSE NULL END AS qty_he,
        CASE WHEN ad.ppa IS TRUE AND bi.ppa_out IS NOT NULL THEN bi.qty WHEN ad.ppa IS TRUE AND bi.ppa_out IS NULL THEN 0 ELSE NULL END AS qty_ppa,
        CASE WHEN ad.artwork IS TRUE AND bi.artwork_out IS NOT NULL THEN bi.qty WHEN ad.artwork IS TRUE AND bi.artwork_out IS NULL THEN 0 ELSE NULL END AS qty_artwork,
        CASE WHEN ad.pad IS TRUE AND bi.pad_out IS NOT NULL THEN bi.qty WHEN ad.pad IS TRUE AND bi.pad_out IS NULL THEN 0 ELSE NULL END AS qty_pad,
        CASE WHEN ad.fuse IS TRUE AND bi.fuse_out IS NOT NULL THEN bi.qty WHEN ad.fuse IS TRUE AND bi.fuse_out IS NULL THEN 0 ELSE NULL END AS qty_fuse,
        CASE WHEN ad.bobok IS TRUE AND bi.bobok_out IS NOT NULL THEN bi.qty WHEN ad.bobok IS TRUE AND bi.bobok_out IS NULL THEN 0 ELSE NULL END AS qty_bobok,
        CASE WHEN ad.bonding IS TRUE AND bi.bonding_out IS NOT NULL THEN bi.qty WHEN ad.bonding IS TRUE AND bi.bonding_out IS NULL THEN 0 ELSE NULL END AS qty_bonding,
        CASE WHEN bi.setting IS NOT NULL THEN bi.qty ELSE 0 END AS qty_setting,
        CASE WHEN bi.supply IS NOT NULL THEN bi.qty ELSE 0 END AS qty_supply,
        bi.create_date
        FROM bundle_info_new bi
        LEFT JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
        WHERE (bi.season,bi.style,ad.set_type,bi.poreference,bi.article) in (
        SELECT DISTINCT
        bi.season,bi.style,ad.set_type,bi.poreference,bi.article
        FROM bundle_info_new bi
        JOIN art_desc_new ad USING (season,style,part_num,part_name)
        WHERE
        (EXTRACT(MONTH FROM bi.distrib_received) = '$month'
        OR (EXTRACT(MONTH FROM bi.artwork_out) = '$month' and EXTRACT(YEAR FROM bi.artwork_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.he_out) = '$month' and EXTRACT(YEAR FROM bi.he_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.pad_out) = '$month' and EXTRACT(YEAR FROM bi.pad_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.ppa_out) = '$month' and EXTRACT(YEAR FROM bi.ppa_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.fuse_out) = '$month' and EXTRACT(YEAR FROM bi.fuse_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.bobok_out) = '$month' and EXTRACT(YEAR FROM bi.bobok_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.bonding_out) = '$month' and EXTRACT(YEAR FROM bi.bonding_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.setting) = '$month' and EXTRACT(YEAR FROM bi.setting) = '$year')
        OR (EXTRACT(MONTH FROM bi.supply) = '$month' and EXTRACT(YEAR FROM bi.supply) = '$year'))
        AND bi.processing_fact='$factory_id'
        ))x)y
        -- PREVIOUS TABLE
        LEFT JOIN(SELECT y.season,y.style,y.poreference,y.article,
        SUM(y.qty_cut) as qty_cut,
        SUM(y.qty_he) as qty_he,
        SUM(y.qty_ppa) as qty_ppa,
        SUM(y.qty_pad) as qty_pad,
        SUM(y.qty_artwork) as qty_artwork,
        SUM(y.qty_fuse) as qty_fuse,
        SUM(y.qty_bobok) as qty_bobok,
        SUM(y.qty_bonding) as qty_bonding,
        SUM(y.qty_setting) as qty_setting,
        SUM(y.qty_supply) as qty_supply
        FROM(SELECT DISTINCT x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,
        FIRST_VALUE(x.qty_cut) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_cut)qty_cut,
        FIRST_VALUE(x.qty_he) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_he )qty_he,
        FIRST_VALUE(x.qty_ppa) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_ppa)qty_ppa,
        FIRST_VALUE(x.qty_artwork) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_artwork)qty_artwork,
        FIRST_VALUE(x.qty_pad) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_pad)qty_pad,
        FIRST_VALUE(x.qty_fuse) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_fuse)qty_fuse,
        FIRST_VALUE(x.qty_bobok) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bobok)qty_bobok,
        FIRST_VALUE(x.qty_bonding) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_bonding)qty_bonding,
        FIRST_VALUE(x.qty_setting) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_setting)qty_setting,
        FIRST_VALUE(x.qty_supply) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty_supply)qty_supply
        FROM(SELECT bi.barcode_id,bi.season,
        CASE WHEN ad.set_type = '0' THEN bi.style WHEN ad.set_type = 'BOTTOM' THEN CONCAT(bi.style,'-BOT') ELSE CONCAT(bi.style,'-TOP') END AS style,
        bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
        CASE WHEN bi.distrib_received IS NULL THEN 0 ELSE bi.qty END AS qty_cut,
        CASE WHEN ad.he IS TRUE AND bi.he_out IS NOT NULL THEN bi.qty WHEN ad.he IS TRUE AND bi.he_out IS NULL THEN 0 ELSE NULL END AS qty_he,
        CASE WHEN ad.ppa IS TRUE AND bi.ppa_out IS NOT NULL THEN bi.qty WHEN ad.ppa IS TRUE AND bi.ppa_out IS NULL THEN 0 ELSE NULL END AS qty_ppa,
        CASE WHEN ad.artwork IS TRUE AND bi.artwork_out IS NOT NULL THEN bi.qty WHEN ad.artwork IS TRUE AND bi.artwork_out IS NULL THEN 0 ELSE NULL END AS qty_artwork,
        CASE WHEN ad.pad IS TRUE AND bi.pad_out IS NOT NULL THEN bi.qty WHEN ad.pad IS TRUE AND bi.pad_out IS NULL THEN 0 ELSE NULL END AS qty_pad,
        CASE WHEN ad.fuse IS TRUE AND bi.fuse_out IS NOT NULL THEN bi.qty WHEN ad.fuse IS TRUE AND bi.fuse_out IS NULL THEN 0 ELSE NULL END AS qty_fuse,
        CASE WHEN ad.bobok IS TRUE AND bi.bobok_out IS NOT NULL THEN bi.qty WHEN ad.bobok IS TRUE AND bi.bobok_out IS NULL THEN 0 ELSE NULL END AS qty_bobok,
        CASE WHEN ad.bonding IS TRUE AND bi.bonding_out IS NOT NULL THEN bi.qty WHEN ad.bonding IS TRUE AND bi.bonding_out IS NULL THEN 0 ELSE NULL END AS qty_bonding,
        CASE WHEN bi.setting IS NOT NULL THEN bi.qty ELSE 0 END AS qty_setting,
        CASE WHEN bi.supply IS NOT NULL THEN bi.qty ELSE 0 END AS qty_supply,
        bi.create_date
        FROM bundle_info_new bi 
        JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
        where (bi.season,bi.style,bi.poreference,bi.article,ad.set_type) in
        (
        SELECT DISTINCT
        bi.style,ad.set_type,bi.season,bi.poreference,bi.article
        FROM bundle_info_new bi
        JOIN art_desc_new ad USING (season,style,part_num,part_name)
        WHERE
        (EXTRACT(MONTH FROM bi.distrib_received) = '$month'
        OR (EXTRACT(MONTH FROM bi.artwork_out) = '$month' and EXTRACT(YEAR FROM bi.artwork_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.he_out) = '$month' and EXTRACT(YEAR FROM bi.he_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.pad_out) = '$month' and EXTRACT(YEAR FROM bi.pad_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.ppa_out) = '$month' and EXTRACT(YEAR FROM bi.ppa_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.fuse_out) = '$month' and EXTRACT(YEAR FROM bi.fuse_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.bobok_out) = '$month' and EXTRACT(YEAR FROM bi.bobok_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.bonding_out) = '$month' and EXTRACT(YEAR FROM bi.bonding_out) = '$year')
        OR (EXTRACT(MONTH FROM bi.setting) = '$month' and EXTRACT(YEAR FROM bi.setting) = '$year')
        OR (EXTRACT(MONTH FROM bi.supply) = '$month' and EXTRACT(YEAR FROM bi.supply) = '$year'))
        AND bi.processing_fact='$factory_id'
        ))x)y
        GROUP BY y.season,y.style,y.poreference,y.article)prev_tb USING(season,style,poreference,article)
        GROUP BY y.season,y.style,y.poreference,y.article,prev_tb.qty_cut,prev_tb.qty_he,prev_tb.qty_ppa,prev_tb.qty_pad,prev_tb.qty_artwork,prev_tb.qty_fuse,prev_tb.qty_bobok,prev_tb.qty_bonding,prev_tb.qty_setting,prev_tb.qty_supply"));

        if(count($data) == 0)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "PDS_REPORT_BULAN ".$month." Tahun ".$year." AOI ".$factory_id;
            return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@'
                    ));
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','SEASON');
                    $sheet->setCellValue('C1','PO BUYER');
                    $sheet->setCellValue('D1','STYLE');
                    $sheet->setCellValue('E1','ARTICLE');
                    $sheet->setCellValue('F1','ORDER QTY');
                    // CUTTING
                    $sheet->setCellValue('G1','PREV CUT');
                    $sheet->setCellValue('H1','CUT');
                    $sheet->setCellValue('I1','TOTAL CUT');
                    $sheet->setCellValue('J1','BLC CUT');
                    // HEAT TRANSFER
                    $sheet->setCellValue('K1','PREV HE');
                    $sheet->setCellValue('L1','HE');
                    $sheet->setCellValue('M1','TOTAL HE');
                    $sheet->setCellValue('N1','BLC HE');
                    // PPA
                    $sheet->setCellValue('O1','PREV PPA');
                    $sheet->setCellValue('P1','PPA');
                    $sheet->setCellValue('Q1','TOTAL PPA');
                    $sheet->setCellValue('R1','BLC PPA');
                    // PAD
                    $sheet->setCellValue('S1','PREV PAD');
                    $sheet->setCellValue('T1','PAD');
                    $sheet->setCellValue('U1','TOTAL PAD');
                    $sheet->setCellValue('V1','BLC PAD');
                    // ARTWORK
                    $sheet->setCellValue('W1','PREV ARTWORK');
                    $sheet->setCellValue('X1','ARTWORK');
                    $sheet->setCellValue('Y1','TOTAL ARTWORK');
                    $sheet->setCellValue('Z1','BLC ARTWORK');
                    // FUSE
                    $sheet->setCellValue('AA1','PREV FUSE');
                    $sheet->setCellValue('AB1','FUSE');
                    $sheet->setCellValue('AC1','TOTAL FUSE');
                    $sheet->setCellValue('AD1','BLC FUSE');
                    // BOBOK
                    $sheet->setCellValue('AE1','PREV AUTO');
                    $sheet->setCellValue('AF1','AUTO');
                    $sheet->setCellValue('AG1','TOTAL AUTO');
                    $sheet->setCellValue('AH1','BLC AUTO');
                    // BONDING
                    $sheet->setCellValue('AI1','PREV BONDING');
                    $sheet->setCellValue('AJ1','BONDING');
                    $sheet->setCellValue('AK1','TOTAL BONDING');
                    $sheet->setCellValue('AL1','BLC BONDING');
                    // SETTING
                    $sheet->setCellValue('AM1','PREV SETTING');
                    $sheet->setCellValue('AN1','SETTING');
                    $sheet->setCellValue('AO1','TOTAL SETTING');
                    $sheet->setCellValue('AP1','BLC SETTING');
                    // SUPPLY
                    $sheet->setCellValue('AQ1','PREV SUPPLY');
                    $sheet->setCellValue('AR1','SUPPLY');
                    $sheet->setCellValue('AS1','TOTAL SUPPLY');
                    $sheet->setCellValue('AT1','BLC SUPPLY');

                    $i = 1;
                    foreach ($data as $key => $vl) {
                        $ordered = DB::select(DB::raw("SELECT x.season,x.poreference,x.style,x.articleno,
                        SUM(x.ordered_qty) as ordered_qty
                        FROM(SELECT DISTINCT documentno,season,po_buyer as poreference,style,articleno,size_finish_good,ordered_qty
                        FROM data_cuttings where po_buyer='$vl->poreference' AND style='$vl->style' AND articleno='$vl->article')x
                        GROUP BY x.season,x.poreference,x.style,x.articleno"));
                        $order = $ordered[0]->ordered_qty;
                        $rw = $i+1;
                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,$vl->season);
                        $sheet->setCellValue('C'.$rw,$vl->poreference);
                        $sheet->setCellValue('D'.$rw,$vl->style);
                        $sheet->setCellValue('E'.$rw,$vl->article);
                        $sheet->setCellValue('F'.$rw,$order);
                        $sheet->setCellValue('G'.$rw,$vl->prev_cut == '' ? 0 : $vl->prev_cut);
                        $sheet->setCellValue('H'.$rw,$vl->qty_cut);
                        $sheet->setCellValue('I'.$rw,$vl->total_cut);
                        $sheet->setCellValue('J'.$rw,$order - (int)$vl->total_cut);
                        $sheet->setCellValue('K'.$rw,$vl->prev_he);
                        $sheet->setCellValue('L'.$rw,$vl->qty_he);
                        $sheet->setCellValue('M'.$rw,$vl->total_he);
                        $sheet->setCellValue('N'.$rw,$vl->total_he == null ? null : $order - (int)$vl->total_he);
                        $sheet->setCellValue('O'.$rw,$vl->prev_ppa);
                        $sheet->setCellValue('P'.$rw,$vl->qty_ppa);
                        $sheet->setCellValue('Q'.$rw,$vl->total_ppa);
                        $sheet->setCellValue('R'.$rw,$vl->total_ppa == null ? null : $order - (int)$vl->total_ppa);
                        $sheet->setCellValue('S'.$rw,$vl->prev_pad);
                        $sheet->setCellValue('T'.$rw,$vl->qty_pad);
                        $sheet->setCellValue('U'.$rw,$vl->total_pad);
                        $sheet->setCellValue('V'.$rw,$vl->total_pad == null ? null : $order - (int)$vl->total_pad);
                        $sheet->setCellValue('W'.$rw,$vl->prev_artwork);
                        $sheet->setCellValue('X'.$rw,$vl->qty_artwork);
                        $sheet->setCellValue('Y'.$rw,$vl->total_artwork);
                        $sheet->setCellValue('Z'.$rw,$vl->total_artwork == null ? null : $order - (int)$vl->total_artwork);
                        $sheet->setCellValue('AA'.$rw,$vl->prev_fuse);
                        $sheet->setCellValue('AB'.$rw,$vl->qty_fuse);
                        $sheet->setCellValue('AC'.$rw,$vl->total_fuse);
                        $sheet->setCellValue('AD'.$rw,$vl->total_fuse == null ? null : $order - (int)$vl->total_fuse);
                        $sheet->setCellValue('AE'.$rw,$vl->prev_bobok);
                        $sheet->setCellValue('AF'.$rw,$vl->qty_bobok);
                        $sheet->setCellValue('AG'.$rw,$vl->total_bobok);
                        $sheet->setCellValue('AH'.$rw,$vl->total_bobok == null ? null : $order - (int)$vl->total_bobok);
                        $sheet->setCellValue('AI'.$rw,$vl->prev_bonding);
                        $sheet->setCellValue('AJ'.$rw,$vl->qty_bonding);
                        $sheet->setCellValue('AK'.$rw,$vl->total_bonding);
                        $sheet->setCellValue('AL'.$rw,$vl->total_bonding == null ? null : $order - (int)$vl->total_bonding);
                        $sheet->setCellValue('AM'.$rw,$vl->prev_setting);
                        $sheet->setCellValue('AN'.$rw,$vl->qty_setting);
                        $sheet->setCellValue('AO'.$rw,$vl->total_setting);
                        $sheet->setCellValue('AP'.$rw,$order - (int)$vl->total_setting);
                        $sheet->setCellValue('AQ'.$rw,$vl->prev_supply);
                        $sheet->setCellValue('AR'.$rw,$vl->qty_supply);
                        $sheet->setCellValue('AS'.$rw,$vl->total_supply);
                        $sheet->setCellValue('AT'.$rw,$order - (int)$vl->total_supply);
                        $i++;
                    }
                });
                $excel->setActiveSheetIndex(0);
            })->export('xlsx');
        }
    }

}
