<?php

namespace App\Http\Controllers;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingPlan;
use App\Models\CuttingMarker;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {
                $data = array();
                $data_planning = DB::table('jaz_cutting_plan_new_update_2')->orderBy('queu', 'asc')->where('factory_id', \Auth::user()->factory_id)->where('cutting_date', $cutting_date)->get();

                foreach($data_planning as $a) {
                    $marker = '';
                    $fabric = '';
                    $over_consum_check = 0;

                    // Set size category

                    if($a->size_category == 'J') {
                        $size_ctg = 'Japan';
                    } elseif($a->size_category == 'A') {
                        $size_ctg = 'Asian';  
                    } else {
                        $size_ctg = 'Inter';
                    }

                    // set status marker

                    if ($a->queu != null) {
                        $data_check = CuttingPlan::where('cutting_date', $a->cutting_date)->where('queu', $a->queu)->where('deleted_at', null)->get()->first();

                        $po_buyer_array = $data_check->po_details->pluck('po_buyer')->toArray();
                        $po            = "'" . implode("','",$po_buyer_array) . "'";
                        
                        $data_uom_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('queu', $a->queu)->first()->uom;
                        $data_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('queu', $a->queu)->sum('fbc');
                        if($data_uom_cons == 'M') {
                            $data_cons = $data_cons  * 1.0936;
                        }

                        $data_marker = CuttingMarker::where('id_plan')->whereNotNull('marker_length')->whereNotNull('perimeter')->get();

                        $data_marker_check = CuttingMarker::select(DB::raw("part_no, sum(need_total) as need_total"))->where('id_plan', $data_check->id)->where('deleted_at')->groupBy('part_no')->get();

                        $total_need_qty = CuttingMarker::where('id_plan', $data_check->id)->where('deleted_at')->sum('need_total');

                        foreach($data_marker_check as $c) {
                            $data_part_no_array = explode('+', $c->part_no);
                            $data_uom_cons_per_part = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('queu', $a->queu)->whereIn('part_no', $data_part_no_array)->first()->uom;
                            $data_cons_per_part = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('queu', $a->queu)->whereIn('part_no', $data_part_no_array)->sum('fbc');
                            if($data_uom_cons_per_part == 'M') {
                                $data_cons_per_part = $data_cons_per_part  * 1.0936;
                            }
                            
                            $balance_over = $data_cons_per_part - $c->need_total;
                            if($balance_over < 0) {
                                $over_consum_check++;
                            }
                        }
                    } else {
                        if($a->top_bot != null) {
                            $po_buyer_array = DB::table('data_cuttings')->select('po_buyer')->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('size_category', $a->size_category)->where('top_bot', $a->top_bot)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                        } else {
                            $po_buyer_array = DB::table('data_cuttings')->select('po_buyer')->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('size_category', $a->size_category)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                        }

                        $data_uom_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('size_category', '$a->size_category')->where('top_bot', $a->top_bot)->first()->uom;
                        $data_cons = DB::table('data_cuttings')->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('size_category', '$a->size_category')->where('top_bot', $a->top_bot)->sum('fbc');

                        if($data_uom_cons == 'M') {
                            $data_cons = $data_cons  * 1.0936;
                        }

                        $total_need_qty = 0;
                    }

                        
                    $data_fabric_query = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$a->style' AND article_no ='$a->articleno' AND po_buyer IN($po)"));

                    $data_fabric_prepared = $data_fabric_query == null ? 0 : (int)$data_fabric_query[0]->qty_prepared;
                    $data_balance = $data_fabric_prepared - $data_cons;

                    $balance_over_consum = $data_fabric_prepared - $total_need_qty;

                    $data_fabric_relax = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$a->cutting_date') WHERE style='$a->style' AND article_no ='$a->articleno' AND po_buyer IN($po) AND last_status_movement = 'relax'"));

                    if ($a->queu == null) {
                        $marker = 'Prioritas Plan';
                    } elseif(count($data_check->file_excels) < 1) {
                        $marker = 'SSP Upload';
                    } elseif(count($data_check->cutting_markers) < 1) {
                        $marker = 'Marker Request';
                    } elseif(count($data_marker) > 0) {
                        if($data_balance < 0) {
                            $marker = 'Fabric Width';
                        } else {
                            $marker = 'Marker Upload';
                        }
                    } elseif($over_consum_check > 0) {
                        if($data_balance < 0) {
                            $marker = 'Fabric Width';
                        } else {
                            $marker = 'Over Consum';
                        }
                    } else {
                        if($data_balance < 0) {
                            $marker = 'Fabric Width';
                        } else {
                            $marker = 'Marker Plot';
                        }
                    }

                    if ($data_balance < 0) {
                        $fabric = 'Prepare';
                    } elseif($data_fabric_prepared <= 0) {
                        $fabric = 'Prepare';
                    } elseif(count($data_fabric_relax) > 0) {
                        if($balance_over_consum < 0) {
                            $fabric = 'Over Consum';
                        } else {
                            $fabric = 'Relax';
                        }
                    } else {
                        if($balance_over_consum < 0) {
                            $fabric = 'Over Consum';
                        } else {
                            $fabric = 'Scan Out';
                        }
                    }
    
                    $data[] = [
                        'queu' => $a->queu,
                        'style' => $a->style,
                        'articleno' => $a->articleno,
                        'color_name' => substr($a->color_name, 0, 15),
                        'size_category' => $size_ctg,
                        'marker' => $marker,
                        'fabric' => $fabric,
                    ];
                }

                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function downloadReport(Request $request)
    {
        $data = json_decode($request->data_download);
        $cutting_date = $request->data_cutting_date;

        $filename = 'Laporan_Dashboard_'.$cutting_date;
        
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'QUEUE',
                    'STYLE',
                    'ARTICLE',
                    'COLOR',
                    'SIZE CATEGORY',
                    'MARKER STATUS',
                    'FABRIC STATUS',
                ));
                foreach ($data as $row)
                {
                    $data_excel = array(
                        $row[0],
                        $row[1],
                        $row[2],
                        $row[3],
                        $row[4],
                        $row[5],
                        $row[6],
                    );

                    $sheet->appendRow($data_excel);
                }
            });
        })->download('xlsx');
    }
}
