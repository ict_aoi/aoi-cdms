@extends('layouts.app',['active' => 'fabric_testing'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Fabric Testing </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Testing Lab</li>
			<li><a href="{{route('lab_fabric.index')}}" id="url_index">Fabric Testing</a></li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new" id="add_test">Add Process <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
							<th>Date Submit</th>
							<th>Asal Specimen </th>
							<th>Test Required</th>
							<th>Date Information</th>
							<th>Buyer</th>
							<th>Lab Location</th>
							<th>No TRF</th>
							<th>User</th>
							<th>PO Buyer</th>
							<th>Item</th>
							<th>Size</th>
							<th>Style</th>
							<th>Color</th>
							<th>Article No</th>
							<th>Category</th>
							<th>Category Specimen</th> 
							<th>Type Specimen</th>
							<th>Testing Methods</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4 style="font-weight: bold;">CREATE FABRIC TESTING</h4></center>
            </div>
            <div class="modal-body">
			<form action="{{route('lab_fabric.store')}}" id="form-create" method="POST">
				<div class="row">
					<div class="col-lg-6">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Buyer</label>
								<select name="buyer" id="buyer" class="form-control select-search">
									<option>== Select Buyer ==</option>
									<option value="ADIDAS">ADIDAS</option>
								</select>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block" style="font-weight: bold;">Category Spesimen</label>
								<select name="category_specimen" id="category_specimen" class="form-control select-search">
									<option value="">== Select Category Specimen ==</option>
								</select>
								<input type="hidden" name="url_categoryspecimen" id="url_categoryspecimen" value ="{{ route('lab_fabric.getTypeSpecimen') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_save" id="url_save" value ="{{ route('lab_fabric.store') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_getpo" id="url_getpo" value ="{{ route('lab_fabric.getPoreference') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_style" id="url_style" value ="{{ route('lab_fabric.getStyle') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_testingmethod" id="url_testingmethod" value ="{{ route('lab_fabric.getTestingMethod') }}" class="form-control" required="" style="text-transform: uppercase;">
								<input type="hidden" name="url_get_requirement" id="url_get_requirement" value ="{{ route('lab_fabric.getRequirements') }}" class="form-control" required="" >
								<a href="{{ route('lab_fabric.getCategory') }}" id="url_typespecimen"></a>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Type Spesimen</label>
								<select name="type_specimen" id="type_specimen" class="form-control select-search">
									<option>== Select Type Specimen ==</option>
								</select>
							</div>
						</div>
						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Category</label>
								<select name="category" id="category" class="form-control select-search">
									<option>== Select Category ==</option>
								</select>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Testing Methods</label>
								<select name="testing_methods" id="testing_methods" class="form-control select-search" style="border-bottom:1px solid" multiple="multiple">
									<option>== Select Testing Methods ==</option>
								</select>
							</div>
						</div>
						
					</div>
					<div class=" col-lg-6">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Lab Location</label>
								<select name="factory" id="factory" class="form-control select-search">
									<option value="AOI1">AOI 1</option>
									<option value="AOI2">AOI 2</option>
								</select>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Test Required</label>
								<div class = "col-md-3" id="bulkTesting">
									<label class="display-block">1 Bulk Testing</label>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_m">
											M (Model Level)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_a">
											A (Article Level)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "bulktesting_selective">
											Selective
										</label>
									</div>
								</div>
								<div class = "col-md-3" id="reorderTesting">
									<label class="display-block">Re-Order Testing</label>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_m">
											M (Model Level)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_a">
											A (Article Level)
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" id="test_required" name="test_required" class="styled" value = "reordertesting_selective">
											Selective
										</label>
									</div>
								</div>
								<div class = "col-md-3" id="reTest">
									<label class="display-block">Re-Test</label>
								<div class="radio">
									<label>
										<input type="radio" id="test_required" name="test_required" class="styled" checked="checked" value = "retest">
										Re-Test
									</label>
								</div>
								<div style="margin-top:22px">
									<input type="text" name="no_trf" id="no_trf" class="form-control"  placeholder="Input previous no trf">
								</div>
								</div>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Date Information</label>
								<div class = "col-md-6">
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "buy_ready" checked="checked">
											Buy ready (special for development testing)
										</label>
									</div>
								</div>
								<div class = "col-md-3">
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "podd">
											PODD
										</label>
									</div>
								</div>
								<div class = "col-md-3">
									<div class="radio_date">
										<label>
											<input type="radio" name="radio_date" class="styled" id = "radio_date" value = "output_sewing">
											1st output sewing
										</label>
									</div>
								</div>								
								<input type="date" name="testing_date" id="testing_date" class="form-control" required="" style="text-transform: uppercase;">
							</div>
						</div>

						
						<div class="col-lg-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Part yang akan di test</label>
								<div class="form-group">
									<textarea rows="3" cols="3" class="form-control elastic" placeholder="Part yang akan di test" id="part_akan_dites"></textarea>
								</div>
							</div>
						</div>

						
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Return Test Sample</label>
								<select name="return_sample" id="return_sample" class="form-control select-search">
									<option>== Choose return Sample ==</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<br><br>
				<div class="row">
					<div class="col-lg-6">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">PO Buyer</label>
								<input type="text" name="po_mo" id="po_mo" class="form-control" required="" style="text-transform: uppercase;">
							</div>
						</div>
						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Item</label>
								<select name="item" id="item" class="form-control select-search">
									<option>== Select Item ==</option>
								</select>
							</div>
						</div> 

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Size</label>
									<select name="size" id="size" class="form-control select-search" >
									<option>== Select Size ==</option>
									</select>
								<input type="hidden" name="urlsize" id="urlsize" class="form-control" value = "{{ route('lab_fabric.getSize') }}" style="text-transform: uppercase;">
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Style</label>
								<input type="text" name="style" id="style" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Article</label>
								<input type="text" name="article" id="article" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>

						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Color</label>
								<input type="text" name="color" id="color" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>
						<div class="col-md-12" style="margin-top:12px">
							<div class="row">
								<label class="display-block text-semibold">Destination</label>
								<input type="text" name="destination" id="destination" class="form-control" required="" style="text-transform: uppercase;" readonly>
							</div>
						</div>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-md-12">
							<div class="row">
								<label class="display-block text-semibold">Remarks</label>
								<label>Reporting maximum of 4 days. Status received specimen & status finished specimen</label>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
			</div>
				<button type="submit" class="btn btn-info col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
			</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-top: 10px" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(function(){
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        //scrollX:true,
        dom: '<"datatable-header"fBlr><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('lab_fabric.getDatatrf') }}",
        },
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},//0
            {data: 'asal_specimen', name: 'asal_specimen',searchable:true,visible:true,orderable:true},//1
            {data: 'test_required', name: 'test_required',searchable:false,visible:true,orderable:true},//2
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false,visible:true,orderable:true},//3
            {data: 'buyer', name: 'buyer',searchable:false,visible:true,orderable:true},//4
            {data: 'lab_location', name: 'lab_location',searchable:false,visible:true,orderable:true},//5
            {data: 'no_trf', name: 'no_trf',searchable:true,visible:true,orderable:true},//6
            {data: 'nik', name: 'nik',searchable:false,visible:true,orderable:true},//7
            {data: 'orderno', name: 'orderno',searchable:true,visible:true,orderable:true},//8
            {data: 'item', name: 'item',searchable:true,visible:true,orderable:true},//9
            {data: 'size', name: 'size',searchable:false,visible:true,orderable:true},//10
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//11
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},//12
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},//13
            {data: 'category', name: 'category',searchable:false,visible:true,orderable:true},//14
            {data: 'category_specimen', name: 'category_specimen',searchable:false,visible:true,orderable:true},//15
            {data: 'type_specimen', name: 'type_specimen',searchable:false,visible:true,orderable:true},//16
            {data: 'testing_method_id', name: 'testing_method_id',searchable:false,visible:true,orderable:true},//17
            {data: 'last_status', name: 'last_status',searchable:true,visible:true,orderable:true},//18
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},//19
	]
    });
    $('#add_test').on('click',function(){
        $('#modalUpload').modal('show');
    });

	var dtable = $('#table-list').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#buyer').on('change', function (){  
        var url_get_requirement = $('#url_get_requirement').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	$.ajax({
		type: 'get',
		url :  url_get_requirement,
		data : {
			buyer:$('#buyer').val()
		},
		success: function(response) {
			var data = response.response;
			$('#category_specimen').empty();
			$('#category_specimen').append('<option>== Select Category Specimen ==</option>');
			for (var i = 0; i < data.length; i++) {
			$('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>')
			}
			
		},
		error: function(response) {
			$.unblockUI();
			alert(response.status,response.responseText);
		}
	});
    });

    $('#category_specimen').on('change', function () {  
	var url_categoryspecimen = $('#url_categoryspecimen').val();
	console.log(url_categoryspecimen);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
        

	$.ajax({
		type: 'get',
		url :  url_categoryspecimen,
		data : {category_specimen:$('#category_specimen').val()},

		success: function(response) {
			console.log(response);
			var data = response.response;
			$('#type_specimen').empty();
			$('#type_specimen').append('<option>== Select Type Specimen ==</option>');
			
			for (var i = 0; i < data.length; i++) {
			$('#type_specimen').append('<option value="'+data[i]['type_specimen']+'">'+data[i]['type_specimen']+'</option>')
			}
		},
		error: function(response) {
			$.unblockUI();
			alert(response.status,response.responseText);
			
		}
	});
    });

    $('#type_specimen').on('change', function (event) {
	event.preventDefault();
	var url_typespecimen = $('#url_typespecimen').attr('href');
	// console.log(url_typespecimen);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	// console.log($('#type_specimen').val());
	$.ajax({
		type: 'get',
		url :  url_typespecimen,
		data : {category_specimen   :$('#category_specimen').val(),
				type_specimen       :$('#type_specimen').val()},
		success: function(response) {
			var data = response.response;
			$('#category').empty();
			$('#category').append('<option>== Select Category ==</option>');

			for (var i = 0; i < data.length; i++) {
			$('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+' - '+data[i]['category']+'</option>');
			$('#testing_methods').empty();
			$('#testing_methods').append('<option value="'+data[i]['id']+'">'+data[i]['method_code']+' - '+data[i]['method_name']+'</option>');
			
			}
		},
		error: function(response) {
			$.unblockUI();
			alert(response.status,response.responseText);
			
		}
	});
    });

	$("#testing_methods").select2({
		placeholder: "Silahkan Pilih Testing Methods"
	});


    $('#category').on('change', function () {  
	var url_testingmethod = $('#url_testingmethod').val();

	var category_specimen = $('#category_specimen').val();
	var type_specimen     = $('#type_specimen').val();
	var category          = $('#category').val();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	

	$.ajax({
		type: 'get',
		url :  url_testingmethod,
		data : {category_specimen   :category_specimen,
				type_specimen       :type_specimen,
				category            :category},
		// beforeSend:function(){
		// 	loading();
		// },
		success: function(response) {
			console.log(response);
			var data = response.response;

			$('#testing_methods').empty();
			for (var i = 0; i < data.length; i++) {
			$('#testing_methods').append('<option value="'+data[i]['method_code']+'">'+data[i]['method_code']+' - '+data[i]['method_name']+'</option>')
			}
			// alert(notif.status,notif.output);
			// print(notif.idpr);
			// $.unblockUI();
			// $('#modal_daftar').modal('hide');
			
		},
		error: function(response) {
			$.unblockUI();
			alert(response.status,response.responseText);
			
		}
	});
    });


    $('#po_mo').on('change', function () {  
        var asal_specimen   = 'PRODUCTION';        
		var po_mo           = $('#po_mo').val();
		var url_getpo       = $('#url_getpo').val();
		var asal_specimen   = $('#asal_specimen').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
            
    
	$.ajax({
		type: 'get',
		url :  url_getpo,
		data : {po_mo:po_mo, asal_specimen:asal_specimen},
		// beforeSend:function(){
		// 	loading();
		// },
		success: function(response) {
			// console.log(response);
			var data = response.response;
			console.log(data);
			$('#item').empty();
			for (var i = 0; i < data.length; i++) {
			$('#item').append('<option value="'+data[i]['item_code']+'">'+data[i]['item_code']+'</option>')
			$('#po_mo').val(data[i]['po']);
			}
			
			// alert(notif.status,notif.output);
			// print(notif.idpr);
			// $.unblockUI();
			// $('#modal_daftar').modal('hide');
				
		},
		error: function(response) {
			$.unblockUI();
			alert(response.status,response.responseText);
			
		}
	});
    });
    
    $('#item').on('change', function () {  
	var po_mo   = $('#po_mo').val();
	var item    = $('#item').val();
	var asal_specimen   = 'PRODUCTION';
	if(po_mo == null && item == null)
	{
		$("#alert_error").trigger("click", 'Silahkan isi PO/MO dan Size.');
		return false;
	}else{
		var urlsize       = $('#urlsize').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		

		$.ajax({
			type: 'get',
			url :  urlsize,
			data : {po_mo:po_mo, item_code:item, asal_specimen},

			success: function(response) {
				var data = response.response;
				console.log(data);

				$('#size').empty();
				for (var i = 0; i < data.length; i++) {
				$('#size').append('<option value="'+data[i]['size']+'">'+data[i]['size']+'</option>')
				$('#style').val(data[i]['style']);
				$('#article').val(data[i]['article']);
				$('#color').val(data[i]['color']);;
				$('#destination').val(data[i]['country']);
				}
					
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
				
			}
		});
	}
    });

    $('#size').on('change', function () {  
	var po_mo   = $('#po_mo').val();
	var item    = $('#item').val();
	var Size    = $('#size').val();
	var asal_specimen   = 'PRODUCTION';
	if(po_mo == null && item == null)
	{
		$("#alert_error").trigger("click", 'Silahkan isi PO/MO dan Size.');
		return false;
	}else{
		var urlstyle       = $('#url_style').val();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		

		$.ajax({
			type: 'get',
			url :  urlstyle,
			data : {po_mo:po_mo, item_code:item, asal_specimen, size:Size},
			// beforeSend:function(){
			// 	loading();
			// },
			success: function(response) {
				// console.log(response);
				var data = response.response;
				console.log(data);
				$('#style').val(data['style']);
				$('#article').val(data['article']);
				$('#color').val(data['color']);
				$('#destination').val(data['country']);
				
					
			},
			error: function(response) {
				$.unblockUI();
				alert(response.status,response.responseText);
				
			}
		});
	}
    });
})
function hapus(url) 
{
    bootbox.confirm("Are you sure want to delete this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "delete",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#table-list').DataTable().ajax.reload();
			});
		}
	});
}



$('#form-create').submit(function (){
	event.preventDefault();
	var testing_methods   = $('#testing_methods').val();
	var buyer             = $('#buyer').val();
	var asal_specimen     = $('#asal_specimen').val(); 
	var category_specimen = $('#category_specimen').val();
	var type_specimen     = $('#type_specimen').val();
	var category          = $('#category').val();
	var factory           = $('#factory').val();
	var test_required     = $('#test_required').val();
	var no_trf = null;
	var radio_date        = $('#radio_date').val();
	var part_akan_dites   = $('#part_akan_dites').val();
	var po_mo             = $('#po_mo').val();
	var size              = $('#size').val();
	var style             = $('#style').val();
	var article           = $('#article').val();
	var color             = $('#color').val();
	var return_sample     = $('#return_sample').val();
	var other_buyer       = null;
	var testing_date      = $('#testing_date').val();
	var item              = $('#item').val();
	var destination       = $('#destination').val();

  if(buyer == "other")
  {
    var get_otherbuyer       = $('#other_buyer').val();
    other_buyer = get_otherbuyer;

  }

  if(category_specimen == "== Select Category Specimen ==" || category_specimen == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi category specimen.');
    return false;
  }

  if(type_specimen == "== Select Type Specimen ==" || type_specimen == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi type specimen.');
    return false;
  }

  if(category == "== Select Category ==" || category == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi category.');
    return false;
  }

  if(testing_methods == null)
  {
    $("#alert_error").trigger("click", 'Silahkan pilih testing method.');
    return false;
  }

  if(return_sample == "== Choose return Sample ==" || return_sample == null)
  {
    $("#alert_error").trigger("click", 'Silahkan pilih return sample.');
    return false;
  }
  if(testing_date == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi tanggal testing.');
    return false;
  }

  if(test_required == "retest" && no_trf == null)
  {
      
      var trf = $('#no_trf').val();
      no_trf = trf;
      $("#alert_error").trigger("click", 'Silahkan isi no trf sebelumnya.');
		return false;
  }
  var url_save = $('#url_save').val();

  

	bootbox.confirm("Are you sure want to save this data ?.", function (result) 
	{
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: 'POST',
				url: url_save,
				data: {
					buyer                 :buyer,
					asal_specimen         :asal_specimen,
					category_specimen     :category_specimen,
					type_specimen         :type_specimen,
					category              :category,
					factory               :factory,
					test_required         :test_required,
					no_trf                :no_trf,
					radio_date            :radio_date,
					part_akan_dites       :part_akan_dites,
					po_mo                 :po_mo,
					size                  :size,
					style                 :style,
					article               :article,
					color                 :color,
					return_sample         :return_sample,
					other_buyer           :other_buyer,
					testing_methods       :testing_methods,
					testing_date          :testing_date,
					item                  :item,
					destination           :destination

				},
				beforeSend: function () 
				{
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
				var url = $('#url_index').attr('href'); 
				document.location.href = url;
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully saved.');
				$('#modalUpload').modal('hide');
				$('#table-list').DataTable().ajax.reload();
			});
		}
	});
});
</script>
@endsection