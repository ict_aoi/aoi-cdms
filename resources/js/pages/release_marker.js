$(document).ready( function () {
		
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#releaseTable').DataTable({
        ajax: {
            type: 'get',
            url: '/release-marker/data-release',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: [8],
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData > 0 ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'green'});
                    } else {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    }
                }
            }
        ],            
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'cutting_date', name: 'cutting_date',searchable:true,orderable:true},
            {data: 'lc_date', name: 'lc_date',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            {data: 'part_no', name: 'part_no',searchable:true,orderable:true},
            {data: 'item_id', name: 'item_id',searchable:true,orderable:true},
            {data: 'balance', name: 'balance',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });

    $('#releaseTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/release-marker/data-release',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#releaseTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#releaseTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#uploadModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
    });
});

function detail(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#detailModal').modal();
            $('#style_view').text(response.style);
            $('#part_no_view').text(response.part_no);
            $('#balance_view').text(response.balance);
            $('#pcd_view').text(response.pcd);
            $('#lc_view').text(response.lc);
            $('#po_buyer_view').text(response.po_buyer);
            $('#item_view').text(response.item);
            $('#factory_view').text(response.factory);
            get_data_modal_table(response.id);
        }
    })
}

function get_data_modal_table(id) {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/release-marker/data-release-detail',
            data: {
                release_id: id,
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table1.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                className: 'text-center',
            },
            {
                targets: [6, 7],
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData > 0 ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'green'});
                    } else {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    }
                }
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:false},
            {data: 'release_by', name: 'release_by',searchable:true,visible:true,orderable:false},
            {data: 'marker_prod', name: 'marker_prod',searchable:false,orderable:false},
            {data: 'supply_whs', name: 'supply_whs',searchable:false,orderable:false},
            {data: 'actual_marker_prod', name: 'actual_marker_prod',searchable:false,orderable:false},
            {data: 'balance', name: 'balance',searchable:false,orderable:false},
            {data: 'persen', name: 'persen',searchable:false,orderable:false},
            {data: 'remark', name: 'remark',searchable:false,orderable:false},
            {data: 'confirm_by', name: 'confirm_by',searchable:false,orderable:false},
            {data: 'confirm_at', name: 'confirm_at',searchable:false,orderable:false},
        ],
    });
    
    $('#detailTable').on('click','.ignore-click', function() {
        return false;
    });
}