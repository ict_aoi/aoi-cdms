$(document).ready( function () {
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#uploadDataTable').DataTable({
        ajax: {
            url: '/upload-file-ssp/data',
            data: {
                cutting_date: $('#cutting_date').val(),
                factory: $("input[name='factory']:checked").val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po', name: 'po',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:false,orderable:false},
            {data: 'last_update', name: 'last_update',searchable:false,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });

    $('#uploadDataTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#fileExcelTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();
        var factory = $("input[name='factory']:checked").val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/upload-file-ssp/data',
            data: {
                cutting_date: cutting_date,
                factory: factory,
            },
            beforeSend: function() {
                $('#uploadDataTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#uploadDataTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#uploadDataTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });
    
    var dtable = $('#uploadDataTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    // Upload File Excel
    $('#form_upload_file').submit(function(event) {
        event.preventDefault();

        var file_excel = $('#file_excel').val();

        if(!file_excel){
            $("#alert_warning").trigger("click", 'File excel masih kosong!');
            return false;
        }

        $.ajax({
            type: "POST",
            url: $('#form_upload_file').attr('action'),
            data:new FormData($("#form_upload_file")[0]),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $('#form_upload_file').trigger("reset");
                $('#fileExcelTable').DataTable().ajax.reload();
                $('#file_excel').val(null);
                $("#alert_success").trigger("click", 'Data Berhasil disimpan');
            },
            error: function (response) {
                $.unblockUI();
                
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#form_upload_file').modal();
            }
        });
    });

    $('#uploadSspResultModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $('#file_excel').val(null);
    });
});

function upload(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#upload_ssp_result').attr('action', response.url_upload);
            $('#upload_cutting_date').val(response.cutting_date);
            $('#upload_style').val(response.style);
            $('#upload_articleno').val(response.articleno);
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#po_buyer_detail_view').text(response.po_buyer);
            $('#upload_file_id').val(response.id);
            $('#upload_size_category').val(response.size_category);
            $('#uploadSspResultModal').modal();
            get_file_table();
        }
    })
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
            $('#fileExcelTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil hapus');
            $('#file_excel').val(null);
        },
        error: function () {
            $.unblockUI();
        }
    });
}

function unduh(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            window.location.href = "/upload-file-ssp/data-file/download/"+response;
            $('#fileExcelTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil download');
        },
        error: function () {
            $.unblockUI();
        }
    });
}

function get_file_table() {
    var table1 = $('#fileExcelTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            type: 'post',
            url: '/upload-file-ssp/data-file',
            data: {
                file_id: $('#upload_file_id').val(),
            },
        },
        fnCreatedRow: function (row, data, index) {
            var info = table1.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                  className: 'text-center',
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'file_name', name: 'file_name',searchable:true,visible:true,orderable:false},
            {data: 'user_id', name: 'user_id',searchable:true,visible:true,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:true,visible:true,orderable:false},
            {data: 'terima_user_id', name: 'terima_user_id',searchable:true,visible:true,orderable:false},
            {data: 'terima_at', name: 'terima_at',searchable:true,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],
    });
}