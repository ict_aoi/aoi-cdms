<?php

namespace App\models;
use App\Uuids;


use Illuminate\Database\Eloquent\Model;

class RequestMarker extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'request_marker';
	protected $fillable = ['id_plan','color','part','cut','ratio','layer','marker','created_at','updated_at','deleted_at','fabric_lot'];
}
