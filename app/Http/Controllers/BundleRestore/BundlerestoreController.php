<?php

namespace App\Http\Controllers\BundleRestore;

use DB;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CuttingMarker;
use App\Models\Spreading\FabricUsed;
use App\Models\User;
use App\Models\ScanCutting;
use Auth;

class BundlerestoreController extends Controller
{
    public function index()
    {
        return view('distribusi.bundle_restore.index');
    }
    public function scanBarcode(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();
            $check_created_1 = DB::table('bundle_header')->where('barcode_marker',$barcode_id)->first();
            $check_created_2 = DB::table('cutting_marker')->where('barcode_id',$barcode_id)->where('is_bundling',true)->first();
            if($check_created_1 != null && $check_created_2 != null){
                return response()->json('TICKET SUDAH DIBUAT!', 422);
            }elseif($data != null) {
                $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));

                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                $ratio = 0;
                foreach ($data->rasio_markers as $a) {
                    $ratio = $ratio + $a->ratio;
                }
                $total_garment = $ratio * $data->layer;

                $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                $po_buyer = implode(', ', $po_buyer);

                $ratio_size = '';

                foreach ($data->rasio_markers as $aa) {
                    $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                }

                $spreading_check = FabricUsed::where('barcode_marker', $data->barcode_id)->orderBy('created_spreading', 'desc')->get();

                if(count($spreading_check) > 0) {
                    $spreading_status = 'Done';
                    $spreading_date = Carbon::parse($spreading_check->first()->created_at)->format('d M Y / H:i');
                    $spreading_by = User::where('id', $spreading_check->first()->user_id)->first()->name;
                    $spreading_table = DB::table('master_table')->where('id_table', $spreading_check->first()->barcode_table)->first()->table_name;
                } else {
                    $spreading_status = '-';
                    $spreading_date = '-';
                    $spreading_by = '-';
                    $spreading_table = '-';
                }

                $approval_gl_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'GL Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_gl_check != null) {
                    $approve_gl_status = $approval_gl_check->status;
                    $approve_gl_date = Carbon::parse($approval_gl_check->created_at)->format('d M Y / H:i');
                    $approve_gl_by = User::where('id', $approval_gl_check->user_id)->first()->name;
                    $approve_gl_table = '-';
                } else {
                    $approve_gl_status = '-';
                    $approve_gl_date = '-';
                    $approve_gl_by = '-';
                    $approve_gl_table = '-';
                }

                $approval_qc_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'QC Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_qc_check != null) {
                    $approve_qc_status = $approval_qc_check->status;
                    $approve_qc_date = Carbon::parse($approval_qc_check->created_at)->format('d M Y / H:i');
                    $approve_qc_by = User::where('id', $approval_qc_check->user_id)->first()->name;
                    $approve_qc_table = '-';
                } else {
                    $approve_qc_status = '-';
                    $approve_qc_date = '-';
                    $approve_qc_by = '-';
                    $approve_qc_table = '-';
                }

                $cutting_check = ScanCutting::where('barcode_marker', $data->barcode_id)->where('state', 'done')->first();

                if($cutting_check != null) {
                    $get_user_array = $cutting_check->operator->pluck('user_id')->toArray();
                    $operator = User::whereIn('id', $get_user_array)->pluck('name')->toArray();
                    $cutting_status = 'Done';
                    $cutting_date = Carbon::parse($cutting_check->end_time)->format('d M Y / H:i');
                    $cutting_by = implode(', ', $operator);
                    $cutting_table = $cutting_check->table_name;
                } else {
                    $cutting_status = '-';
                    $cutting_date = '-';
                    $cutting_by = '-';
                    $cutting_table = '-';
                }

                $response = [
                    'data' => $data,
                    'layer' => $data->layer_plan.' ('.$data->layer.')',
                    'style' => $data->cutting_plan->style,
                    'article' => $data->cutting_plan->articleno,
                    'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'),
                    'color_name' => substr($get_data_cutting->color_name, 0, 15),
                    'ratio' => $ratio,
                    'total_garment' => $total_garment,
                    'item_code' => $get_data_cutting->material,
                    'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20),
                    'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                    'marker_width' => $data->fabric_width - 0.5,
                    'po_buyer' => $po_buyer,
                    'ratio_size' => $ratio_size,
                    'ket' => null,
                    'spreading_status' => $spreading_status,
                    'approve_gl_status' => $approve_gl_status,
                    'approve_qc_status' => $approve_qc_status,
                    'cutting_status' => $cutting_status,
                    'bundling_status' => '-',
                    'spreading_date' => $spreading_date,
                    'approve_gl_date' => $approve_gl_date,
                    'approve_qc_date' => $approve_qc_date,
                    'cutting_date' => $cutting_date,
                    'bundling_date' => '-',
                    'spreading_by' => $spreading_by,
                    'approve_gl_by' => $approve_gl_by,
                    'approve_qc_by' => $approve_qc_by,
                    'cutting_by' => $cutting_by,
                    'bundling_by' => '-',
                    'spreading_table' => $spreading_table,
                    'cutting_table' => $cutting_table,
                    'bundling_table' => '-',
                ];
                return response()->json($response,200);
            } else {
                return response()->json('marker tidak ditemukan', 422);
            }
        }
    }
    public function dataTicket(Request $request)
    {
        if ($request->ajax()){
            $barcode_id = $request->barcode_marker;
            $data = DB::table('dd_bundle_restore')
            ->where('barcode_marker',$barcode_id)
            ->get();
            
            return DataTables::of($data)
            ->addColumn('sticker',function ($data)
            {
                return $data->start_no.'-'.$data->end_no;
            })
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->rawColumns(['style','sticker'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function restoreTicket(Request $request){
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            $is_restored = DB::table('cutting_marker')->where('barcode_id',$barcode_marker)->first();
            if($is_restored->is_bundling == 't'){
                return response()->json('Bundle Sudah di Buat / Sudah di Restore!', 422);
            }
            if($barcode_marker != null){
                $last_deleted = DB::table('deleted_bundle_flag')->where('barcode_marker',$barcode_marker)->OrderBy('created_at','desc')->first();
                try{
                    DB::beginTransaction();
                    $get_bundle_header_id = DB::table('deleted_bundle_header')->where('deleted_bundle_flag_id',$last_deleted->id)->get();
                    foreach($get_bundle_header_id as $key){
                        DB::table('bundle_header')->insert([
                            'id'                    => $key->id,
                            'season'                => $key->season,
                            'style'                 => $key->style,
                            'set_type'              => $key->set_type,
                            'poreference'           => $key->poreference,
                            'article'               => $key->article,
                            'cut_num'               => $key->cut_num,
                            'size'                  => $key->size,
                            'factory_id'            => $key->factory_id,
                            'cutting_date'          => $key->cutting_date,
                            'color_name'            => $key->color_name,
                            'cutter'                => $key->cutter,
                            'note'                  => $key->note,
                            'admin'                 => $key->admin,
                            'qc_operator'           => $key->qc_operator,
                            'bundle_operator'       => $key->bundle_operator,
                            'box_lim'               => $key->box_lim,
                            'max_bundle'            => $key->max_bundle,
                            'created_at'            => $key->created_at,
                            'updated_at'            => $key->updated_at,
                            'qty'                   => $key->qty,
                            'barcode_marker'        => $key->barcode_marker,
                            'job'                   => $key->job,
                            'admin_name'            => $key->admin_name,
                            'qc_operator_name'      => $key->qc_operator_name,
                            'bundle_operator_name'  => $key->bundle_operator_name,
                            'statistical_date'      => $key->statistical_date,
                            'destination'           => $key->destination,
                            'po_reroute'            => $key->po_reroute,
                            'job_reroute'           => $key->job_reroute
                        ]);
                    }
                    $bundle_header_id_arr = DB::table('deleted_bundle_header')->where('barcode_marker',$barcode_marker)->pluck('id')->toArray();
                    $get_bundle_detail = DB::table('deleted_bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->get();
                    foreach($get_bundle_detail as $x){
                        DB::table('bundle_detail')->insert([
                            'barcode_id'            => $x->barcode_id,
                            'bundle_header_id'      => $x->bundle_header_id,
                            'style_detail_id'       => $x->style_detail_id,
                            'komponen_name'         => $x->komponen_name,
                            'start_no'              => $x->start_no,
                            'end_no'                => $x->end_no,
                            'qty'                   => $x->qty,
                            'location'              => $x->location,
                            'factory_id'            => $x->factory_id,
                            'sds_barcode'           => $x->sds_barcode,
                            'created_at'            => $x->created_at,
                            'updated_at'            => $x->updated_at,
                            'job'                   => $x->job,
                            'is_recycle'            => $x->is_recycle,
                            'is_out_cutting'        => $x->is_out_cutting,
                            'out_cutting_at'        => $x->out_cutting_at,
                            'is_out_setting'        => $x->is_out_setting,
                            'out_setting_at'        => $x->out_setting_at,
                            'job_reroute'           => $x->job_reroute,
                            'current_locator_id'    => $x->current_locator_id,
                            'current_status_to'     => $x->current_status_to,
                            'current_description'   => $x->current_description,
                            'updated_movement_at'   => $x->updated_movement_at,
                            'current_user_id'       => $x->current_user_id
                        ]);
                    }
                    $list_barcode = DB::table('deleted_bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->pluck('barcode_id')->toArray();
                    $data_movements = DB::table('deleted_distribusi_movements')->whereIn('barcode_id',$list_barcode)->get();
                    foreach($data_movements as $xx){
                        DB::table('distribusi_movements')->insert([
                            'id'                => $xx->id,
                            'barcode_id'        => $xx->barcode_id,
                            'locator_from'      => $xx->locator_from,
                            'status_from'       => $xx->status_from,
                            'locator_to'        => $xx->locator_to,
                            'status_to'         => $xx->status_to,
                            'description'       => $xx->description,
                            'user_id'           => $xx->user_id,
                            'ip_address'        => $xx->ip_address,
                            'created_at'        => $xx->created_at,
                            'updated_at'        => $xx->updated_at,
                            'deleted_at'        => null,
                            'loc_dist'          => $xx->loc_dist,
                            'bundle_table'      => $xx->bundle_table
                        ]);
                    }
                    DB::table('deleted_bundle_header')->where('barcode_marker',$barcode_marker)->delete();
                    DB::table('deleted_bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->delete();
                    DB::table('deleted_distribusi_movements')->whereIn('barcode_id',$list_barcode)->delete();
                    DB::table('cutting_marker')->where('barcode_id',$barcode_marker)->update([
                        'is_bundling'       => true
                    ]);
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }else{
                return response()->json('Harap Input Barcode Marker!', 422);
            }
        }
    }
}
