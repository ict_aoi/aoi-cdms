@extends('layouts.app',['active' => 'distribusi_artwork_report'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Artwork Movements</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Artwork Movements</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">By Scan Date</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">Cek Quota KK</a></li>
				<li class=""><a href="#basic-tab3" data-toggle="tab" class="legitRipple" aria-expanded="false">Tracking Artwork Movement</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
                <form class="form-horizontal" action="{{ route('distribusi_artwork.downloadArtworkmovement') }}" method="POST">
						<fieldset class="content-group">
                            @csrf
							<div class="form-group">
								<label class="control-label col-lg-2 text-bold">Tanggal Scan Artwork</label>
								<div class="col-lg-10">							
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="scan_artwork" id="scan_artwork" placeholder="Masukkan Tanggal Scan">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
									<label class="control-label col-lg-2 text-bold">STYLE</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="style" id="style" placeholder="Masukkan Style..!"/>
										</div>
									</div>
									<label class="control-label col-lg-2 text-bold">NO KK</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="no_kk" id="no_kk" placeholder="Masukkan No KK..!"/>
											<span class="input-group-btn">
												<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
											</span>
										</div>
									</div>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead>
										<tr>
											<th>No</th>
											<th>Document No</th>
											<th>Season</th>
											<th>Style</th>
											<th>No KK</th>
											<th>KK Type</th>
											<th>Booking Quota</th>
											<th>Quota Used</th>
											<th>Balanced Quota</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="basic-tab3">
					<form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
									<label class="control-label col-lg-2 text-bold">PO Buyer</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="po_buyer" id="po_buyer" placeholder="Masukkan PO Buyer..!"/>
										</div>
									</div>
									<label class="control-label col-lg-2 text-bold">Cut Num</label>
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="cut_num" id="cut_num" placeholder="Masukkan Cut Number..!"/>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2 text-bold">Packinglist Number</label>
									<div class="col-lg-10">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="packinglist_no" id="packinglist_no" placeholder="Masukkan No Packinglist..!"/>
											<span class="input-group-btn">
												<button class="btn btn-primary legitRipple" type="button" id="btn-filter2">FILTER</button>
											</span>
										</div>
									</div>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-artwork">
									<thead>
										<tr>
											<th>No</th>
											<th>PO</th>
											<th>Barcode</th>
											<th>Season</th>
											<th>Style</th>
											<th>Cut Number</th>
											<th>Size</th>
											<th>Komponen Name</th>
											<th>Sticker</th>
											<th>Qty</th>
											<th>Last Movement At</th>
											<th>Last Locator</th>
											<th>Last User Scan</th>
											<th>No Packinglist</th>
											<th>Print At</th>
											<th>Print By</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-js')
<script>
    // $(document).ready( function () {
        
    // });
	//DATATABLES
	var style =$('#style').val();
	var no_kk =$('#no_kk').val();
    var url = "{{route('distribusi_artwork.getDataArtworkmovement')}}";
    var tableL = $('#table-list').DataTable({
		processing: true,
        serverSide: true,
        deferRender:true,
		pageLength:50,
		destroy:true,
		scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
				return $.extend({}, d, {
					"style"		: $('#style').val(),
					"no_kk"		: $('#no_kk').val(),
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'document_no', name: 'document_no',searchable:false,visible:true,orderable:true},//1
            {data: 'season', name: 'season',searchable:true,visible:true,orderable:true},//3
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//2
            {data: 'kk_no', name: 'kk_no',searchable:true,visible:true,orderable:true},//4
            {data: 'kk_type', name: 'kk_type',searchable:true,visible:true,orderable:true},//5
			{data: 'total_qty', name: 'total_qty',searchable:true,visible:true,orderable:true},//6
			{data: 'quota_used', name: 'quota_used',searchable:true,visible:true,orderable:true},//7
			{data: 'balanced_quota', name: 'balanced_quota',searchable:true,visible:true,orderable:true},//8
        ]
    });

	var po_buyer =$('#po_buyer').val();
	var cut_num =$('#cut_num').val();
	var packinglist_no =$('#packinglist_no').val();
    var url = "{{route('distribusi_artwork.getDataMovemendetail')}}";
    var tablex = $('#table-artwork').DataTable({
		processing: true,
        serverSide: true,
        deferRender:true,
		pageLength:50,
		destroy:true,
		scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
				return $.extend({}, d, {
					"po_buyer"		: $('#po_buyer').val(),
					"cut_num"		: $('#cut_num').val(),
					"packinglist_no"		: $('#packinglist_no').val(),
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:true},//1
			{data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//2
            {data: 'season', name: 'season',searchable:true,visible:true,orderable:true},//3
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//4
            {data: 'cut_num', name: 'cut_num',searchable:true,visible:true,orderable:true},//5
            {data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//6
			{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//7
			{data: 'sticker', name: 'sticker',searchable:true,visible:true,orderable:true},//8
			{data: 'qty', name: 'qty',searchable:true,visible:true,orderable:true},//9
			{data: 'updated_movement_at', name: 'updated_movement_at',searchable:true,visible:true,orderable:true},//10
			{data: 'current_description', name: 'current_description',searchable:true,visible:true,orderable:true},//10
			{data: 'current_user_id', name: 'current_user_id',searchable:true,visible:true,orderable:true},//11
			{data: 'no_packinglist', name: 'no_packinglist',searchable:true,visible:true,orderable:true},//12
			{data: 'print_at', name: 'print_at',searchable:true,visible:true,orderable:true},//13
			{data: 'printed_by', name: 'printed_by',searchable:true,visible:true,orderable:true},//14
        ]
    });

	tablex.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

	tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').click(function(){
		event.preventDefault();
        var style    = $('#style').val();
        if (style=='') {
            $("#alert_warning").trigger("click", 'Silahkan Masukkan Style Dahulu..!');
            return false;
        }
		tableL.draw();
    });
	$('#btn-filter2').click(function(){
		event.preventDefault();
        var po_buyer    = $('#po_buyer').val();
        if (po_buyer == '') {
            $("#alert_warning").trigger("click", 'Silahkan Masukkan PO Buyer Atau No Packinglist Dahulu..!');
            return false;
        }
		tablex.draw();
    });
</script>
@endsection