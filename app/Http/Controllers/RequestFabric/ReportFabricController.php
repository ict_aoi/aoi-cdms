<?php

namespace App\Http\Controllers\RequestFabric;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\Spreading\RemarkFabric;
use App\Http\Controllers\Controller;

class ReportFabricController extends Controller
{
    public function index()
    {
        return view('request_fabric.report_fabric.index');
    }

    public function indexOutstanding()
    {
        return view('request_fabric.report_fabric.index_outstanding');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('po_buyer', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        $po = implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)
                        ->whereNull('deleted_at')
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    } else {
                        $po = implode(', ', $data->po_details()
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                        ->pluck('po_buyer')->toArray());
                    }
                    return $po;
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('action', function($data) {
                    return '<a href="#" onclick="detailModal(\''.route('reportFabric.detailModal', $data->id).'\')" class="btn btn-primary btn-icon"><i class="icon-file-text"></i></a>';
                })
                ->rawColumns(['po_buyer','action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataPlanningOutstanding(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            $factory = \Auth::user()->factory_id;
            if($cutting_date != NULL) {
                $data = array();
                $data_cutting_plan = CuttingPlan::where('cutting_date', $cutting_date)
                ->where('deleted_at', null)
                ->where('factory_id',$factory)
                ->orderBy('queu', 'asc')->get();
                foreach($data_cutting_plan as $a) {
                    $partial_po = DetailPlan::where('id_plan', $a->id)->pluck('po_buyer')->toArray();
                    $partial_po_view = implode(', ', $partial_po);

                    $get_part = CuttingMarker::select(DB::raw("part_no, sum(need_total) as need_total"))->where('id_plan', $a->id)->where('deleted_at', null)->groupBy('part_no')->get();

                    foreach($get_part as $aa) {
                        $part_no = explode('+', $aa->part_no);

                        $csi_qty_data = DB::table('jaz_detail_per_material')
                        ->select(DB::raw("material, color_name, sum(csi_qty) as csi_qty, uom"))
                        ->where('cutting_date', $cutting_date)
                        ->where('style', 'like', '%'.$a->style.'%')
                        ->where('articleno', $a->articleno)
                        ->whereIn('po_buyer', $partial_po)
                        ->whereIn('part_no', $part_no)
                        ->groupBy('material', 'color_name', 'uom')
                        // ->orderBy('')
                        ->first();

                        if($csi_qty_data != null) {

                            // $already_prepared = DB::connection('wms_live_new')->table('integration_whs_to_cutting')
                            // ->where('planning_date', $cutting_date)
                            // ->where('style', $a->style)
                            // ->where('article_no', $a->articleno)
                            // ->whereIn('po_buyer', $partial_po)
                            // ->where('item_code', $csi_qty_data->material)
                            // ->whereIn('part_no', $part_no)
                            // ->sum('qty_prepared');
                            $po                 = "'" . implode("','",$partial_po) . "'";
                            $part               = "'" . implode("','",$part_no) . "'";
                            // $already_prepared   = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$a->style' AND article_no='$a->articleno' AND po_buyer in($po) AND item_code='$csi_qty_data->material' AND part_no in($part)"));

                            $already_prepared   = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$a->style' AND article_no='$a->articleno' AND po_buyer in($po) AND part_no in($part)"));

                            foreach($already_prepared as $ap => $ax){
                                $already_prepared_ = $ax->qty_prepared == null ? 0 : $ax->qty_prepared;
                            }

                            
                            $remark_check = RemarkFabric::where('id_plan', $a->id)->where('cutting_date', $a->cutting_date)->where('style', $a->style)->where('articleno', $a->articleno)->where('material', $csi_qty_data->material)->where('part_no', $aa->part_no)->get()->first();

                            $remark = '';

                            if($remark_check != null) {
                                $remark = $remark_check->remark;
                            } else {
                                $remark = '';
                            }

                            if($csi_qty_data->uom == 'M') {
                                $csi_qty = $csi_qty_data->csi_qty * 1.09161;
                            } else {
                                $csi_qty = $csi_qty_data->csi_qty;
                            }

                            $data[] = [
                                'queu'             => $a->queu,
                                'cutting_date'     => $a->cutting_date,
                                'style'            => $a->style,
                                'articleno'        => $a->articleno,
                                'po_buyer'         => $partial_po_view,
                                'material'         => $csi_qty_data->material,
                                'color'            => $csi_qty_data->color_name,
                                'part_no'          => $aa->part_no,
                                'csi_qty'          => round($csi_qty, 2),
                                'ssp_qty'          => round($aa->need_total, 2),
                                'already_prepared' => round($already_prepared_, 2),
                                'remark'           => $remark,
                            ];
                        }
                    }
                }
                return datatables()->of($data)
                ->addColumn('over_consum', function($data) {
                    $over_consum = $data['csi_qty'] - $data['ssp_qty'];
                    if($over_consum < 0) {
                        return '<span class="text-danger">'.$over_consum.'</span>';
                    } else {
                        return '<span class="text-success-600">'.$over_consum.'</span>';
                    }
                })
                ->addColumn('balance_whs', function($data) {
                    $balance_whs = $data['already_prepared'] - $data['csi_qty'];
                    if($balance_whs < 0) {
                        return '<span class="text-danger">'.$balance_whs.'</span>';
                    } else {
                        return '<span class="text-success-600">'.$balance_whs.'</span>';
                    }
                })
                ->addColumn('total_balance', function($data) {
                    $balance_whs = $data['already_prepared'] - $data['csi_qty'];
                    $over_consum = $data['csi_qty'] - $data['ssp_qty'];
                    $total_balance = $balance_whs + $over_consum;
                    if($data['ssp_qty'] != 0) {
                        if($total_balance < 0) {
                            return '<span class="text-danger">'.$total_balance.'</span>';
                        } else {
                            return '<span class="text-success-600">'.$total_balance.'</span>';
                        }
                    } else {
                        return '<span class="text-danger">'.$balance_whs.'</span>';
                    }
                })
                ->rawColumns(['over_consum','balance_whs','total_balance'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->make(true);
            }
        }
    }

    public function detailModal($id)
    {
        $data             = CuttingPlan::where('id', $id)->where('deleted_at', null)->first();

        if($data->header_id != null && $data->is_header) {
            $get_plan_id = CuttingPlan::where('header_id', $data->id)
                ->whereNull('deleted_at')
                ->pluck('id')
                ->toArray();

            $po = DetailPlan::whereIn('id_plan', $get_plan_id)
            ->whereNull('deleted_at')
            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
            ->pluck('po_buyer')->toArray();
        } else {
            $get_plan_id[] = $id;
            $po = $data->po_details()
            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
            ->pluck('po_buyer')->toArray();
        }

        $obj               = new StdClass();
        $obj->id           = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style        = $data->style;
        $obj->articleno    = $data->articleno;
        $obj->po           = implode(', ', $po);
        $obj->po_buyer     = json_encode($po);
        $obj->id_plan      = json_encode($get_plan_id);
		return response()->json($obj,200);
    }

    public function downloadReport(Request $request)
    {
        // $data = json_decode($request->data_download);
        $cutting_date = $request->data_cutting_date;
        $factory = \Auth::user()->factory_id;

        $filename = 'Laporan_Fabric_WHS_CUTTING_'.$cutting_date;

        $i = 1;

        if($factory > 0) {
            $plan = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('factory_id', $factory)->where('deleted_at', null)->get();
        } else {
            $plan = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('is_header',true)->where('factory_id', $factory)->where('deleted_at', null)->get();
        }


        // dd($factory);
        // dd($cutting_date);
        if($plan){
            foreach ($plan as $key => $value) {

                if($value->header_id != null && $value->is_header) {
                    $get_plan_id = CuttingPlan::where('header_id', $value->id)
                        ->whereNull('deleted_at')
                        ->pluck('id')
                        ->toArray();

                    $po = DetailPlan::whereIn('id_plan', $get_plan_id)
                    ->whereNull('deleted_at')
                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                    ->pluck('po_buyer')->toArray();
                } else {
                    $get_plan_id[] = $value->id;

                    $po = $value->po_details()
                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , statistical_date, qty"))
                    ->pluck('po_buyer')->toArray();
                }
                $get_part = CuttingMarker::select(DB::raw("part_no, sum(need_total) as need_total"))->where('id_plan', $value->id)->where('deleted_at', null)->groupBy('part_no')->get();

                foreach($get_part as $aa) {
                    $part_no = explode('+', $aa->part_no);

                    $csi_qty_data = DB::table('data_cuttings')
                    ->select(DB::raw("material, color_name, sum(fbc) as csi_qty, uom"))
                    ->where('cutting_date', $value->cutting_date)
                    ->where('style', 'like', '%'.$value->style.'%')
                    ->where('articleno', $value->articleno)
                    ->whereIn('po_buyer', $po)
                    ->whereIn('plan_id', $get_plan_id)
                    ->whereIn('part_no', $part_no)
                    ->groupBy('material', 'color_name', 'uom')
                    // ->orderBy('material', 'color_name', 'uom')
                    ->first();

                    if($csi_qty_data != null) {

                        $wms_data = DB::connection('wms_live_new')
                            ->table('info_actual_width_per_plan_v')
                            ->where('planning_date', $value->cutting_date)
                            ->where('style', $value->style)
                            ->where('article_no', $value->articleno)
                            ->whereIn('po_buyer', $po)
                            ->where('item_code', $csi_qty_data->material)
                            ->whereIn('part_no', $part_no)
                            ->get();

                        $remark_check = RemarkFabric::where('id_plan', $value->id)->where('cutting_date', $value->cutting_date)->where('style', $value->style)->where('articleno', $value->articleno)->where('material', $csi_qty_data->material)->where('part_no', $aa->part_no)->get()->first();

                        $remark = '';

                        if($remark_check != null) {
                            $remark = $remark_check->remark;
                        } else {
                            $remark = '';
                        }

                        if($csi_qty_data->uom == 'M') {
                            $csi_qty = $csi_qty_data->csi_qty * 1.09161;
                        } else {
                            $csi_qty = $csi_qty_data->csi_qty;
                        }

                        foreach ($wms_data as $key => $x) {
                            $data[] = [
                                // 'queu'         => $value->queu,
                                0  => $value->cutting_date,
                                1  => $value->style,
                                2  => $value->articleno,
                                3  => implode(', ',  $po),
                                4  => $csi_qty_data->material,
                                5  => $csi_qty_data->color_name,
                                6  => $aa->part_no,
                                7  => round($csi_qty, 2),
                                8  => round($aa->need_total, 2),
                                9  => $x->qty_prepared,
                                10 => $x->actual_width,
                                11 => $remark,
                            ];
                        }
                    }
                }
            }
        }
        else{
            $data = array();
        }

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'CUTTING DATE',
                    'STYLE',
                    'ARTICLE',
                    'PO BUYER',
                    'MATERIAL',
                    'COLOR',
                    'PART NO',
                    'CSI QTY',
                    'SSP QTY',
                    'OVER CONSUMTION',
                    'QTY PREPARED',
                    'FABRIC WIDTH',
                    'REMARK',
                ));
                foreach ($data as $row)
                {
                    $data_excel = array(
                        $row[0],
                        $row[1],
                        $row[2],
                        $row[3],
                        $row[4],
                        $row[5],
                        $row[6],
                        $row[7],
                        $row[8],
                        $row[8] - $row[9],
                        $row[9],
                        $row[10],
                        $row[11],
                    );

                    $sheet->appendRow($data_excel);
                }
            });
        })->download('xlsx');
    }

    public function downloadReportOutstanding(Request $request)
    {
        $data = json_decode($request->data_download);
        $cutting_date = $request->data_cutting_date;

        $filename = 'Laporan_Fabric_WHS_CUTTING_'.$cutting_date;

        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($data, $i) {
            $excel->sheet('report', function($sheet) use($data, $i) {
                $sheet->appendRow(array(
                    'QUEUE',
                    'STYLE',
                    'ARTICLE',
                    'PO BUYER',
                    'MATERIAL',
                    'COLOR',
                    'PART NO',
                    'CSI QTY',
                    'SSP QTY',
                    'OVER CONSUMTION',
                    'ALREADY PREPARED',
                    'WHS BALANCE',
                    'TOTAL BALANCE',
                    'REMARK',
                ));
                foreach ($data as $row)
                {
                    $data_excel = array(
                        $row[0],
                        $row[1],
                        $row[2],
                        $row[3],
                        $row[4],
                        $row[5],
                        $row[6],
                        $row[7],
                        $row[8],
                        $row[9],
                        $row[10],
                        $row[11],
                        $row[12],
                        $row[13],
                    );

                    $sheet->appendRow($data_excel);
                }
            });
        })->download('xlsx');
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax())
        {
            $id       = $request->id;
            $id_plan  = json_decode($request->id_plan);
            $po_buyer = json_decode($request->po_buyer);
            $po_buyer_arr = "'".implode("','",$po_buyer)."'";

            $data     = array();

            $get_plan = CuttingPlan::where('id',$id)->whereNull('deleted_at')->first();
            if($get_plan->header_id != null && $get_plan->is_header) {
                $get_plan_date = CuttingPlan::where('header_id', $get_plan->header_id)
                                    ->whereNull('deleted_at')
                                    ->pluck('cutting_date')->toArray();
                if(count($get_plan_date) == 3){
                    // for($i=0;$i < count($get_plan_date);$i++){
                        $get_plan_date = "'".implode("','",[$get_plan_date[0],$get_plan_date[1],$get_plan_date[2]])."'";
                    // }
                }elseif(count($get_plan_date) == 2){
                    // for($i=0;$i < count($get_plan_date);$i++){
                        $get_plan_date = "'".implode("','",[$get_plan_date[0],$get_plan_date[1],$get_plan_date[1]])."'";
                    // }
                }else{
                    // for($i=0;$i < count($get_plan_date);$i++){
                        $get_plan_date = "'".implode("','",[$get_plan_date[0],$get_plan_date[0],$get_plan_date[0]])."'";
                    // }
                }
            } else {
                $get_plan_date = "'".implode("','",[$get_plan->cutting_date,$get_plan->cutting_date,$get_plan->cutting_date])."'";
            }
            $get_part = DB::table('cutting_marker as cm')->select(DB::raw("cm.part_no, sum(cm.need_total) as need_total,cp.style,cp.articleno"))
            ->leftJoin('cutting_plans2 as cp','cp.id','=','cm.id_plan')
            ->where('cm.id_plan', $id)
            ->where('cm.deleted_at', null)
            ->groupBy('cp.style','cp.articleno','cm.part_no')
            ->orderBy('part_no')
            ->get();

            if(count($get_part)>0){
                foreach($get_part as $aa) {
                    $part_no = explode('+', $aa->part_no);

                    $per_part = DB::table('data_cuttings')
                    ->select(DB::raw("material, color_name, item_id, articleno, sum(fbc) as csi_qty, uom"))
                    ->whereIn('plan_id', $id_plan)
                    ->whereIn('part_no', $part_no)
                    ->orderBy(DB::raw("material"))
                    ->groupBy('material', 'color_name', 'item_id', 'articleno', 'uom')
                    ->first();

                    $remark_check = RemarkFabric::whereIn('id_plan', $id_plan)
                    ->where('style', $get_plan->style)
                    ->where('articleno', $per_part->articleno)
                    ->where('material', $per_part->material)
                    ->whereIn('part_no', $part_no)
                    ->get()->first();

                    $remark = '';

                    if($remark_check != null) {
                        $remark = $remark_check->remark;
                    } else {
                        $remark = '';
                    }

                    // $wms_data = DB::connection('wms_live')
                    // ->table('info_actual_width_per_plan_v')
                    // ->select(DB::raw("item_code, item_id, sum(qty_prepared) as qty_prepared, actual_width"))
                    // ->groupBy('item_code', 'item_id', 'actual_width')
                    // ->whereIn('planning_date', $get_plan_date)
                    // ->where('style', 'like', '%'.$get_plan->style.'%')
                    // ->where('article_no', $per_part->articleno)
                    // ->whereIn('po_buyer', $po_buyer)
                    // ->whereIn('part_no', $part_no)
                    // ->get();

                    $wms_data = DB::connection('wms_live_new')->select(DB::RAW("SELECT item_code, item_id, sum(qty_prepared) as qty_prepared, actual_width FROM integration_to_cutting_fabric_width_f($get_plan_date) WHERE style ='$aa->style' AND article_no='$aa->articleno' AND po_buyer in($po_buyer_arr) AND part_no='$aa->part_no' GROUP BY item_code,item_id,actual_width"));

                    if($per_part->uom == 'M') {
                        $csi_qty = $per_part->csi_qty * 1.09161;
                    } else {
                        $csi_qty = $per_part->csi_qty;
                    }


                    if(count($wms_data)>0){
                        foreach ($wms_data as $key => $value) {
                            $data[] = [
                                'material'     => $per_part->material,
                                'item_id'      => $per_part->item_id,
                                'color'        => $per_part->color_name,
                                'part_no'      => $part_no,
                                'csi_qty'      => round($csi_qty, 2),
                                'ssp_qty'      => round($aa->need_total, 2),
                                'qty_prepared' => $value->qty_prepared,
                                'fabric_width' => $value->actual_width,
                                'remark'       => $remark,
                            ];
                        }
                    }
                    else{
                        $data[] = [
                            'material'     => $per_part->material,
                            'item_id'      => $per_part->item_id,
                            'color'        => $per_part->color_name,
                            'part_no'      => $part_no,
                            'csi_qty'      => round($csi_qty, 2),
                            'ssp_qty'      => round($aa->need_total, 2),
                            'qty_prepared' => '-',
                            'fabric_width' => '-',
                            'remark'       => $remark,
                        ];
                    }
                }
                return datatables()->of($data)
                ->make(true);
            }else {
                $get_part = DB::table('data_cuttings')
                ->select(DB::raw("material, color_name, item_id, style, articleno, part_no, sum(fbc) as csi_qty, uom"))
                ->whereIn('plan_id', $id_plan)
                ->orderBy(DB::raw("part_no, material"))
                // ->orderBy('articleno')
                // ->orderBy('part_no')
                // ->orderBy('material')
                ->groupBy('material', 'color_name', 'item_id', 'style', 'articleno', 'part_no', 'uom')
                ->get();

                foreach($get_part as $aa) {

                    $remark_check = RemarkFabric::whereIn('id_plan', $id_plan)
                    ->where('style', $aa->style)
                    ->where('articleno', $aa->articleno)
                    ->where('material', $aa->material)
                    ->where('part_no', $aa->part_no)
                    ->get()->first();

                    $remark = '';

                    if($remark_check != null) {
                        $remark = $remark_check->remark;
                    } else {
                        $remark = '';
                    }

                    // $wms_data = DB::connection('wms_live')
                    //     ->table('info_actual_width_per_plan_v')
                    //     ->select(DB::raw("item_code, item_id, sum(qty_prepared) as qty_prepared, actual_width"))
                    //     ->groupBy('item_code', 'item_id', 'actual_width')
                    //     ->whereIn('planning_date', $get_plan_date)
                    //     ->where('style', 'like', '%'.$aa->style.'%')
                    //     ->where('article_no', $aa->articleno)
                    //     ->whereIn('po_buyer', $po_buyer)
                    //     ->where('part_no', $aa->part_no)
                    //     ->get();

                    $wms_data = DB::connection('wms_live_new')->select(DB::RAW("SELECT item_code, item_id, sum(qty_prepared) as qty_prepared, actual_width FROM integration_to_cutting_fabric_width_f($get_plan_date) WHERE style ='$aa->style' AND article_no='$aa->articleno' AND po_buyer in($po_buyer_arr) AND part_no='$aa->part_no' GROUP BY item_code,item_id,actual_width"));

                    if($aa->uom == 'M') {
                        $csi_qty = $aa->csi_qty * 1.09161;
                    } else {
                        $csi_qty = $aa->csi_qty;
                    }

                    if(count($wms_data)>0){
                        foreach ($wms_data as $key => $value) {
                            $data[] = [
                                'material'     => $aa->material,
                                'item_id'      => $aa->item_id,
                                'color'        => $aa->color_name,
                                'part_no'      => $aa->part_no,
                                'csi_qty'      => round($csi_qty, 2),
                                'ssp_qty'      => '-',
                                'qty_prepared' => round($value->qty_prepared,2),
                                'fabric_width' => $value->actual_width,
                                'remark'       => $remark,
                            ];
                        }
                    }
                    else{
                        $data[] = [
                            'material'     => $aa->material,
                            'item_id'      => $aa->item_id,
                            'color'        => $aa->color_name,
                            'part_no'      => $aa->part_no,
                            'csi_qty'      => round($csi_qty, 2),
                            'ssp_qty'      => '-',
                            'qty_prepared' => '-',
                            'fabric_width' => '-',
                            'remark'       => $remark,
                        ];
                    }

                }
                return datatables()->of($data)
                ->make(true);
            }
        }
    }

      public function dataPrepare(Request $request)
    {
        if(request()->ajax())
        {
            $id       = $request->id;
            $id_plan  = json_decode($request->id_plan);
            $po_buyer = json_decode($request->po_buyer);

            $data     = array();

            $get_plan = CuttingPlan::where('id',$id)->whereNull('deleted_at')->first();

            if($get_plan->header_id != null & $get_plan->is_header) {
                $get_plan_date = CuttingPlan::where('header_id', $get_plan->header_id)
                                    ->whereNull('deleted_at')
                                    ->pluck('cutting_date')->toArray();

            } else {
                $get_plan_date[] = $get_plan->cutting_date;
            }

            $get_plan_id = $id_plan;


            $get_part = DB::table('data_cuttings')
            ->select(DB::raw("style, articleno, part_no, sum(fbc) as csi_qty, uom"))
            ->whereIn('plan_id', $get_plan_id)
            ->groupBy('style', 'articleno', 'part_no', 'uom')
            ->orderBy('part_no')
            ->get();
            foreach($get_part as $aa) {
                // $part_no = explode('+', $aa->part_no);

                    $wms_data = DB::connection('wms_live')
                        ->table('info_actual_width_per_plan_v')
                        ->whereIn('planning_date', $get_plan_date)
                        ->where('style', 'like', '%'.$aa->style.'%')
                        ->where('article_no', $aa->articleno)
                        ->whereIn('po_buyer', $po_buyer)
                        ->where('part_no', $aa->part_no)
                        ->sum('qty_prepared');

                    if($aa->uom == 'M') {
                        $csi_qty = $aa->csi_qty * 1.09161;
                    } else {
                        $csi_qty = $aa->csi_qty;
                    }

                    $persen = round(($wms_data/round($csi_qty, 2))*100,2);
                    $data[] = [
                        'part_no'      => $aa->part_no,
                        'csi_qty'      => round($csi_qty, 2),
                        'qty_prepared' => round($wms_data,2),
                        'balance'      => round($wms_data-round($csi_qty, 2),2),
                        'persen'       => $persen >= 100 ? 100.00 : $persen
                    ];

            }
            return datatables()->of($data)
            ->make(true);
        }
    }

    public function reportCuttingInstruction()
    {
        return view('request_fabric.report_ci.index');
    }


    public function dataCuttingInstruction(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                $factory = \Auth::user()->factory_id;

                if($factory == 1){
                    $warehouse_id = '1000001';
                }
                else if($factory == 2){
                    $warehouse_id = '1000011';
                }
                else{
                    $warehouse_id = '';
                }

                // $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
                // $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

                $cutting_instructions   = DB::connection('wms_live')->table('cutting_instruction_report_v')
                ->where('warehouse_id','LIKE',"%$warehouse_id%")
                ->where(DB::raw("date(planning_date)"),$cutting_date)
                ->orderby('planning_date','desc');

                return DataTables::of($cutting_instructions)
                ->editColumn('warehouse_id',function ($cutting_instructions)
                {
                    if($cutting_instructions->warehouse_id == '1000001') return 'Warehouse Fabric AOI 1';
                    elseif($cutting_instructions->warehouse_id == '1000011') return 'Warehouse Fabric AOI 2';
                })
                ->editColumn('planning_date',function ($cutting_instructions)
                {
                    return Carbon::createFromFormat('Y-m-d', $cutting_instructions->planning_date)->format('d/M/Y');
                })
                ->editColumn('qty_need',function ($cutting_instructions)
                {
                    return number_format($cutting_instructions->qty_need, 4, '.', ',');
                })
                ->editColumn('total_out',function ($cutting_instructions)
                {
                    return number_format($cutting_instructions->total_out, 4, '.', ',');
                })
                ->editColumn('total_relax',function ($cutting_instructions)
                {
                    return number_format($cutting_instructions->total_relax, 4, '.', ',');
                })
                ->addColumn('status',function($cutting_instructions)
                {
                    if($cutting_instructions->total_prepared != 0 && round($cutting_instructions->total_prepared, 4) - (round($cutting_instructions->total_rejected, 4) + round($cutting_instructions->total_approved, 4)) > 0.001)
                    {
                        return 'PROGRESS';
                    }
                    elseif($cutting_instructions->total_prepared != 0 && $cutting_instructions->total_prepared == $cutting_instructions->total_approved)
                    {
                        return 'APPROVED';
                    }
                    elseif($cutting_instructions->total_prepared != 0 && round($cutting_instructions->total_prepared, 4) - (round($cutting_instructions->total_approved,4) + round($cutting_instructions->total_rejected, 4)) < 0.001 && $cutting_instructions->total_rejected > 0)
                    {
                        return 'REJECT';
                    }
                })
                ->addColumn('action',function($cutting_instructions){
                    return view('request_fabric.report_ci._action',[
                        // 'model'             => $cutting_instructions,
                        'detail'            => route('reportCI.detailCuttingInstruction',$cutting_instructions->id),
                        // 'export'            => route('reportCI.exportCuttingInstruction',$cutting_instructions->id),
                    ]);
                })
                ->rawColumns(['status','action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailCuttingInstruction($id)
    {
        $cutting_instruction = DB::connection('wms_live')->table('cutting_instruction_report_v')->where('id',$id)->first();
        return view('request_fabric.report_ci.detail',compact('cutting_instruction'));
    }

    public function dataDetailCuttingInstruction(Request $request,$id)
    {
        if(request()->ajax())
        {
            $article_no     = $request->article_no;
            $planning_date  = $request->planning_date;
            $style          = $request->style;
            $warehouse_id   = $request->warehouse_id;

            $detail_cutting_instructions = DB::connection('wms_live')->select(db::raw("SELECT * FROM get_detail_ci(
                '".$article_no."',
                '".$planning_date."',
                '".$style."',
                '".$warehouse_id."'
                )"
            ));

            return DataTables::of($detail_cutting_instructions)
            ->editColumn('checkbox',function($data){
                return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
           })
            ->editColumn('is_selected',function($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->last_status_movement == 'out' && $detail_cutting_instructions->user_receive_cutting_id == null) return "<input type='checkbox' onclick='checkbox(&apos;".$detail_cutting_instructions->id."&apos;)' name='is_checked' id='is_checked_".$detail_cutting_instructions->id."' data-id='".$detail_cutting_instructions->id."' data-status='0'>";
            })
            ->editColumn('last_movement_date',function ($detail_cutting_instructions)
            {
                return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('TANGGAL_PREPARE',function ($detail_cutting_instructions)
            {
                return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->TANGGAL_PREPARE)->format('d/M/Y H:i:s');
            })
            ->editColumn('date_receive_cutting',function ($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->date_receive_cutting) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->date_receive_cutting)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('piping',function($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->piping) return '<span class="label label-success">Piping</span>';
                else return '<span class="label label-info">Not Piping</span>';
            })
            ->setRowAttr([
            'style' => function($detail_cutting_instructions)
                {
                    if($detail_cutting_instructions->last_status_movement != 'out') return  'background-color: #e8e8e8;';
                    else if($detail_cutting_instructions->user_receive_cutting_id != null) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['is_selected','action','piping', 'checkbox'])
            ->make(true);
        }
    }

    public function exportDetailCuttingInstruction(Request $request,$id)
    {
        $cutting_instruction    = DB::connection('wms_live')->table('cutting_instruction_report_v')->where('id',$id)->first();
        $article_no             = $cutting_instruction->article_no;
        $planning_date          = $cutting_instruction->planning_date;
        $style                  = $cutting_instruction->style;
        $warehouse_id           = $cutting_instruction->warehouse_id;

        $detail_cutting_instructions = DB::connection('wms_live')->select(db::raw("SELECT * FROM get_detail_ci(
            '".$article_no."',
            '".$planning_date."',
            '".$style."',
            '".$warehouse_id."'
            )"
        ));

        $file_name = 'cutting_instructions_'.$planning_date;
        return \Excel::create($file_name,function($excel) use ($cutting_instruction,$detail_cutting_instructions)
        {
            $excel->sheet('active',function($sheet) use ($cutting_instruction,$detail_cutting_instructions)
            {
                //Header
                //JUDUL
                $sheet->mergeCells('B1:P2');
                $sheet->setCellValue('B1','CUTTING INSTRUCTION');

                //PIPING
                $sheet->setCellValue('A7','PIPING');

                //TANGGAL PLANNING
                $sheet->mergeCells('B3:C3');
                $sheet->mergeCells('E3:F3');
                $sheet->setCellValue('B3','TANGGAL PLANNING');
                $sheet->setCellValue('D3',':');
                $sheet->setCellValue('E3',$cutting_instruction->planning_date);

                //ARTICLE
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('E4:F4');
                $sheet->setCellValue('B4','ARTICLE');
                $sheet->setCellValue('D4',':');
                $sheet->setCellValue('E4',$cutting_instruction->article_no);

                //STYLE
                $sheet->mergeCells('B5:C5');
                $sheet->mergeCells('E5:F5');
                $sheet->setCellValue('B5','STYLE');
                $sheet->setCellValue('D5',':');
                $sheet->setCellValue('E5',$cutting_instruction->style);

                //PO BUYER
                $sheet->mergeCells('I3:J3');
                $sheet->mergeCells('L3:M3');
                $sheet->setCellValue('I3','PO BUYER');
                $sheet->setCellValue('K3',':');
                $sheet->setCellValue('L3',$cutting_instruction->po_buyer);

                //TANGGAL PREPARE
                $sheet->mergeCells('B7:C7');
                $sheet->setCellValue('B7','TANGGAL PREPARE');

                //PIC
                $sheet->mergeCells('D7:E7');
                $sheet->setCellValue('D7','PIC');

                //NO. PO SUPPLIER
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('F7','NO. PO SUPPLIER');

                //KODE ITEM
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('G7','KODE ITEM');

                //WARNA
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('H7','WARNA');

                //NOMOR ROLL
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('I7','NOMOR ROLL');

                //AKTUAL LOT
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('J7','AKTUAL LOT');

                //TOP WIDTH
                $sheet->mergeCells('K7:L7');
                $sheet->setCellValue('K7','TOP WIDTH');

                //MIDDLE WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('M7','MIDDLE WIDTH');

                //END WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('N7','END WIDTH');

                //ACTUAL WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('O7','ACTUAL WIDTH');

                //RESERVED QTY (IN YDS)
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('P7','RESERVED QTY (IN YDS)');

                //Detail Data
                $row    = 8 ;
                $total  = 0;
                foreach($detail_cutting_instructions as $key => $i)
                {
                    //piping
                    $status ="";
                    if($i->piping) $piping='PIPING';
                    else  $piping='BUKAN PIPING';

                    $sheet->setCellValue('A'.$row, $piping);

                    $sheet->mergeCells('B'.$row.':C'.$row);
                    $sheet->mergeCells('K'.$row.':L'.$row);
                    $sheet->mergeCells('D'.$row.':E'.$row);

                    $sheet->setCellValue('B'.$row,$i->TANGGAL_PREPARE);
                    $sheet->setCellValue('D'.$row,$i->PIC);
                    $sheet->setCellValue('F'.$row,$i->NO_PO_SUPPLIER);
                    $sheet->setCellValue('G'.$row,$i->KODE_ITEM);
                    $sheet->setCellValue('H'.$row,$i->WARNA);
                    $sheet->setCellValue('I'.$row,$i->NOMOR_ROLL);
                    $sheet->setCellValue('J'.$row,$i->AKTUAL_LOT);
                    $sheet->setCellValue('K'.$row,$i->TOP_WIDTH);
                    $sheet->setCellValue('M'.$row,$i->MIDDLE_WIDTH);
                    $sheet->setCellValue('N'.$row,$i->BOTTOM_WIDTH);
                    $sheet->setCellValue('O'.$row,$i->AKTUAL_WIDTH);
                    $sheet->setCellValue('P'.$row,$i->QTY_RESERVED);

                    if ($total == 0) $total = $i->QTY_RESERVED;
                    else $total = $total + $i->QTY_RESERVED;

                    $row++;
                    $sheet->setCellValue('O'.($row), 'Total : ');
                    $sheet->setCellValue('P'.($row), $total);
                    $sheet->setCellValue('O'.($row+1), 'Jumlah Roll: ');
                    $sheet->setCellValue('P'.($row+1), $key+1);
                }
            });
        })->export('xlsx');
    }


    public function reportFabricPrepare()
    {
        return view('request_fabric.report_prepare.index');
    }

    public function dataReguler(Request $request)
    {
        if(request()->ajax())
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('m/d/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('m/d/Y', $request->end_date)->format('Y-m-d') : Carbon::now();

            $material_preparations = DB::connection('wms_live_new')->select(db::raw("SELECT * FROM daily_material_preparation_f('".$start_date."', '".$end_date."', '".$warehouse_id."')"));

            return DataTables::of($material_preparations)
            ->editColumn('is_piping',function ($material_preparations)
            {
                if($material_preparations->is_piping) return '<span class="label label-info">Piping</span>';
                else return '<span class="label label-default">Non Piping</span>';
            })
            ->editColumn('planning_date',function ($material_preparations)
            {
                if($material_preparations->planning_date) return Carbon::createFromFormat('Y-m-d', $material_preparations->planning_date)->format('d/M/Y');
                else return null;
            })
            ->editColumn('total_reserved_qty',function ($material_preparations)
            {
                return number_format($material_preparations->total_reserved_qty, 4, '.', ',');
            })
            ->editColumn('total_qty_rilex',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_rilex, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($material_preparations)
            {
                return number_format($material_preparations->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('warehouse_id',function ($material_preparations)
            {
                if($material_preparations->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($material_preparations->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('status',function ($material_preparations)
            {
                if($material_preparations->status == false)
                {
                    return '<span class="label label-default">OPEN</span>';
                }
                else
                {
                    return '<span class="label label-info">CLOSED</span>';
                }
            })
            ->editColumn('po_buyer',function ($material_preparations)
            {
                $list_po_buyer = explode(',', $material_preparations->po_buyer);
                $po_buyer      = array_unique($list_po_buyer);
                $po_buyer      = implode(',', $po_buyer);
                return $po_buyer;
            })
            ->addColumn('action',function($material_preparations){
                return view('request_fabric.report_prepare._action',[
                    'model'         => $material_preparations,
                    'detail'        => route('reportFabricPrepare.detail',['id' => $material_preparations->id, 'wms_version' => $material_preparations->wms_version]),
                ]);
              })
            ->setRowAttr([
                'style' => function($material_preparations)
                {
                    if(round($material_preparations->total_qty_outstanding, 4) != 0) return  'background-color: #fab1b1;';
                    if($material_preparations->remark_planning) return  'background-color: #ffd400;';
                },
            ])
            ->rawColumns(['is_piping','action','style','status'])
            ->make(true);
        }
    }

    public function detail($id,$wms_version)//id & wmsversion
    {
        if($wms_version == 'wmsv2'){
            $material_preparation = DB::connection('wms_live_new')->table('material_planning_preparations')->select('id','supplier_name','planning_date','article_no','is_piping','item_code_source','uom','purchase_number as document_no','factory_id as warehouse_id','style as _style','item_code_book as item_code','total_qty_need as total_reserved_qty','total_qty_preparation as total_qty_rilex')->where('id',$id)->first();
            $details = DB::connection('wms_live_new')
            ->table('detail_material_planning_preparations as dmpp')
            ->Join('material_planning_preparations as mpp','mpp.id','=','dmpp.material_planning_preparation_id')
            ->where('dmpp.material_planning_preparation_id',$id)
            ->first();
            return view('request_fabric.report_prepare.detail',compact('material_preparation','details','wms_version'));
        }else{
            $material_preparation = DB::connection('wms_live')->table('material_preparation_fabrics')->where('id',$id)->first();
            if($material_preparation != null){
                $details =  DB::connection('wms_live')
                ->table('detail_preparation_fabric_v')
                ->where('material_preparation_fabric_id',$id)
                ->orderby('created_at','asc')
                ->orderby('barcode','asc')
                ->get();
    
                return view('request_fabric.report_prepare.detail',compact('material_preparation','details','wms_version'));
            }else{
                return redirect()->action('reportFabricPrepare.index');
            }
        }

    }

    public function dataDetailPrepare(Request $request,$id,$wms_version)
    {
        if(request()->ajax())
        {
            if($request->wms_version == 'wmsv1'){
                $detail_material_preparations =  DB::connection('wms_live')
                ->table('detail_preparation_fabric_v')
                ->where('material_preparation_fabric_id',$id)
                ->orderby('created_at','asc')
                ->orderby('barcode','asc')
                ->get();
            }else{
                $detail_material_preparations = DB::connection('wms_live_new')->select(DB::raw(DB::RAW("SELECT dmpp.id,dmpp.barcode,dmpr.nomor_roll,UPPER((SELECT item_code_book FROM material_planning_preparations WHERE id = dmpp.material_planning_preparation_id)) AS item_code,dmpr.batch_number,dmpr.actual_lot as load_actual,dmpr.actual_length,dmpr.top_width as begin_width,dmpr.middle_width,dmpr.bottom_width as end_width,dmpr.actual_width,dmpp.created_at,dmpp.material_out_date as movement_out_date,dmpp.qty_preparation as reserved_qty,dmpp.last_status AS last_status_movement,(SELECT name FROM users WHERE id = dmpp.user_id),(SELECT code FROM locators WHERE id = dmpp.locator_id) AS last_locator,dmpp.material_planning_preparation_id,(SELECT factory_id FROM material_planning_preparations WHERE id = dmpp.material_planning_preparation_id) AS warehouse_id,(SELECT erp_org FROM material_planning_preparations WHERE id = dmpp.material_planning_preparation_id) AS erp_org,(SELECT is_new_bima FROM material_planning_preparations WHERE id = dmpp.material_planning_preparation_id) AS is_new_bima FROM detail_material_planning_preparations dmpp,detail_material_per_rolls dmpr WHERE dmpp.detail_material_per_roll_id = dmpr.id and dmpp.material_planning_preparation_id='$id' AND dmpp.deleted_at is null")));
            }

            return DataTables::of($detail_material_preparations)
            ->editColumn('created_at',function ($detail_material_preparations)
            {
                if($detail_material_preparations->created_at) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_material_preparations->created_at)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('movement_out_date',function ($detail_material_preparations)
            {
                if($detail_material_preparations->movement_out_date) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_material_preparations->movement_out_date)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('reserved_qty',function ($detail_material_preparations)
            {
                return number_format($detail_material_preparations->reserved_qty, 4, '.', ',');
            })
            ->make(true);
        }
    }
}
