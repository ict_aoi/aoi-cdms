@extends('layouts.app',['active' => 'cuttingplan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Preparation </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Preparation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-css')
<style>
    .margin-kanan {
        margin-right: 10px;
    }
</style>
@endsection

@section('page-content')

@include('includes.notif_mo_update')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_by_lcdate">
            <label><b>Choose Date</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="date" class="form-control" name="date_cutting" id="date_cutting">

                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary margin-kanan" id="btn-filter">Filter</button>
                    @if(\Auth::user()->factory_id != 2)
                    <button type="button" class="btn btn-success" id="btn-sync-day">Sync</button>
                    @endif
                </div>
            </div>
        </div>
        <div class="row form-group">
        @if(\Auth::user()->factory_id != 2)
            <button class="btn btn-success add-margin-left" id="sync_plan"><i class="icon-spinner9"></i> Sync | 2 Prev - 3 Next</button>
            <button class="btn btn-success add-margin-left" id="sync_plan7"><i class="icon-spinner9"></i> Sync | 2 Prev - 5 Next</button>
        @endif
            <div class="pull-right">
                <form action="{{ route('prioritasPlan.downloadPrioritas') }}" method="POST">
                    @csrf
                    <input type="hidden" name="date_cutting_download" id="date_cutting_download">
                    <button type="submit" class="btn btn-primary" id="download_prioritas"><i class="icon-download"></i> Download</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Style</th>
                        <th>PO Buyer</th>
                        <th>Article</th>
                        <th>Size Ctgy</th>
                        <th>Color</th>
                        <th>Queu</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('prioritasPlan.ajaxGetPlaning') }}" id="ajaxGetPlaningLink"></a>
<a href="{{ route('prioritasPlan.dailySync') }}" id="dailySyncLink"></a>
<a href="{{ route('prioritasPlan.dailySync7') }}" id="dailySync7Link"></a>
<a href="{{ route('prioritasPlan.dailySyncDay') }}" id="dailySyncDayLink"></a>
<a href="{{ route('prioritasPlan.partialIn') }}" id="partialInLink"></a>
<a href="{{ route('prioritasPlan.allIn') }}" id="allInLink"></a>
<a href="{{ route('prioritasPlan.edQueu') }}" id="edQueuLink"></a>
<a href="{{ route('prioritasPlan.deletedAll') }}" id="deletedAllLink"></a>
<a href="{{ route('prioritasPlan.splitTopBot') }}" id="splitTopBotLink"></a>
<a href="{{ route('prioritasPlan.margeTopBot') }}" id="margeTopBotLink"></a>
<a href="{{ route('prioritasPlan.delpart') }}" id="delpartLink"></a>
<a href="{{ route('prioritasPlan.getDataCombinePlan') }}" id="getDataCombinePlanLink"></a>
<a href="{{ route('prioritasPlan.combinePlanAction') }}" id="combinePlanActionLink"></a>

@endsection

@section('page-modal')
	@include('request_marker.prioritas_plan._modal_po')
	@include('request_marker.prioritas_plan._combine_modal')
@endsection

@section('page-js')

<script src="{{ mix('js/notif_mo_update.js') }}"></script>
<script>
    $(document).ready(function(){


        $('#date_cutting').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: "YYYY-MM-DD",
                separator: " - ",
            }
        });

        $('#cutting_date_combine_plan').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: "YYYY-MM-DD",
                separator: " - ",
            }
        });

        var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: $('#ajaxGetPlaningLink').attr('href'),
                data: function(d) {
                     return $.extend({}, d, {
                         "date_cutting"         : $('#date_cutting').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(2).css('max-width','130px');
                $('td', row).eq(4).css('max-width','80px');
                $('td', row).eq(5).css('max-width','100px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style'},
                {data: 'po_buyer', name: 'po_buyer'},
                {data: 'articleno', name: 'articleno'},
                {data: 'size_category', name: 'size_category'},
                {data: 'color_name', name: 'color_name'},
                {data: 'queu', name: 'queu',sortable: false, orderable: false, searchable: false},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

        $('#btn-filter').click(function() {
            event.preventDefault();
            var date_cutting    = $('#date_cutting').val();

            if (date_cutting == '') {
                $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
                return false;
            } else {
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'GET',
                    url : $('#ajaxGetPlaningLink').attr('href'),
                    data:{date_cutting:date_cutting},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        $('#table-list').unblock();
                        tableL.clear();
                        tableL.draw();
                    },
                    error:function(response){
                        myalert('error','eror');
                    }
                });
            }
        });

        $('#table-list').on('click','.btn-delqueu',function(){
            var date = $(this).data('pcd');
            var style = $(this).data('style');
            var articleno = $(this).data('article');
            var size = $(this).data('size');
            var queu = $(this).data('queu');
            var top_bot = $(this).data('tb');

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'get',
                    url : $('#deletedAllLink').attr('href'),
                    data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,top_bot:top_bot},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){

                        $("#alert_success").trigger("click", 'delete success');
                        $('#table-list').unblock();
                        $('#btn-filter').click();
                    },
                    eror:function(response){
                        $("#alert_warning").trigger("click",response);

                        return false;
                        $('#btn-filter').click();
                    }

                });

        });

        $('#table-list').on('blur','.queu_unfilled', function(){

            var date = $(this).data('date');
            var style = $(this).data('style');
            var articleno = $(this).data('article');
            var color = $(this).data('color');
            var size = $(this).data('size');
            var mo_created = $(this).data('mo_created');
            var material = $(this).data('material');
            var color_code = $(this).data('color_code');
            var top_bot = $(this).data('tb');
            var gtop_bot = $(this).data('gtb');
            var season = $(this).data('season');
            var queu = $(this).val();
            if (queu!="" || queu.trim()!="") {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url : $('#allInLink').attr('href'),
                    data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,mo_created:mo_created,color:color,material:material,color_code:color_code,top_bot:top_bot,season:season,gtopbot:gtop_bot},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);

                        }else{
                            $("#alert_warning").trigger("click",data_response.output);

                        }
                        $('#queu_'+style).html(response);
                        $('#btn-filter').click();
                    },
                    eror:function(response){
                        myalert('error',response);
                        return false;
                        $('#btn-filter').click();
                    }

                });
            }
        });

        $('#table-list').on('blur','.edqueu_unfilled', function(){
            var date = $(this).data('date');
            var id = $(this).data('id');
            var queu = $(this).val();
            if (queu!="" || queu.trim()!="") {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url : $('#edQueuLink').attr('href'),
                    data: {id:id,queu:queu,date:date},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);

                        }else{
                            $("#alert_warning").trigger("click",data_response.output);

                        }
                        $('#edqueu_'+id).html(response);
                        $('#btn-filter').click();
                    },
                    eror:function(response){
                        myalert('error',response);
                        return false;
                        $('#btn-filter').click();
                    }

                });
            }
        });

        $('#table-list').on('click','.btn-detail',function(){
            var pcd =  $(this).data('pcd');
            var style = $(this).data('style');
            var article =  $(this).data('article');
            var color = $(this).data('color');
            var queu = $(this).data('queu');
            var ncolor = color.replace(/-/g," ");
            var size_category = $(this).data('size');
            var mo_update = $(this).data('mo');
            var material = $(this).data('material');
            var color_code = String($(this).data('color_code'));
            var top_bot = $(this).data('tb');
            var season = $(this).data('season');
            var gtopbot = $(this).data('gtb');

            var ncolor_code = color_code.replace(/-/g," ");
            $('#pcd_md').val(pcd);
            $('#style_md').val(style);
            $('#article_md').val(article);
            $('#color_md').val(ncolor);
            $('#size_md').val(size_category);
            $('#mo_md').val(mo_update);
            $('#material_md').val(material);
            $('#color_code_md').val(ncolor_code);
            $('#top_bot_md').val(top_bot);
            $('#season_md').val(season);
            $('#gtb_md').val(gtopbot);

            $('#modalDetail').modal('show');

            var tableLD = $('#table-list-detail').DataTable({
                processing: true,
                serverSide: true,
                deferRender:true,
                destroy:true,
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                ajax: {
                    type: 'GET',
                    url: "{{ route('prioritasPlan.getPO') }}",
                    data: function(d) {
                        return $.extend({}, d, {
                            "cutting_date"         : pcd,
                            "style" : style,
                            "articleno" : article,
                            "size_category": size_category,
                            "color": ncolor,
                            "queu": queu,
                            "mo_update":mo_update,
                            "top_bot":top_bot
                        });
                    }
                },
                fnCreatedRow: function (row, data, index) {
                    $('td',row).eq(0).css('max-widht: 50px;')
                    $('td',row).eq(2).css('max-widht: 150px;')
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox',sortable: false, orderable: false, searchable: false},
                    {data: 'po_buyer', name: 'po_buyer'},
                    {data: 'queu', name: 'queu'}
                ]
            });
        });


        $('#table-list-detail').on('blur','.queup_unfilled', function(){
            var tableLD = $('#table-list-detail').DataTable();
            var date = $(this).data('date');
            var style = $(this).data('style');
            var articleno = $(this).data('article');
            var color = $(this).data('color');
            var size = $(this).data('size');
            var pobuyer = $(this).data('pobuyer');
            var mo_created = $(this).data('mo_update');
            var queu = $(this).val();
            if (queu!="" || queu.trim()!="") {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url : $('#partialInLink').attr('href'),
                    data: {cutting_date:date,style:style,articleno:articleno,size_category:size,queu:queu,mo_created:mo_created,color:color,pobuyer:pobuyer},
                    beforeSend:function(){
                        $('#table-list-detail').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success:function(response){
                        $('#table-list-detail').unblock();
                        $('#queup_'+style).html(response);
                        tableLD.clear();
                        tableLD.draw();
                    },
                    eror:function(response){
                        console.log(response);
                        return false;

                    }
                });
            }
        });


        $('#btn-save').on('click',function(){
            var data = [];

            var queue = $('#queue_md').val();
            var date = $('#pcd_md').val();
            var style = $('#style_md').val();
            var color = $('#color_md').val();
            var article = $('#article_md').val();
            var sizectg = $('#size_md').val();
            var mo_created = $('#mo_md').val();
            var material = $('#material_md').val();
            var color_code = $('#color_code_md').val();
            var top_bot = $('#top_bot_md').val();
            var season = $('#season_md').val();
            var gtopbot = $('#gtb_md').val();


            $('.setpo:checked').each(function(){
                data.push({
                    pobuyer :$(this).val(),
                    stat_date :$(this).data('stat'),
                    qty:$(this).data('qty')
                });
            });

            if (data.length > 0) {

                if ($('#queue_md').val()=='') {
                    $("#alert_warning").trigger("click", 'Queu cannot empty');

                    return false;
                }

                $.ajaxSetup({
                    headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                });
                $.ajax({
                    type:'POST',
                    url : $('#partialInLink').attr('href'),
                    data:{data:data,date:date,style:style,article:article,sizectg:sizectg,queue:queue,mo_created:mo_created,color:color,material:material,color_code:color_code,top_bot:top_bot,season:season,gtopbot:gtopbot},
                    beforeSend:function(){
                        loading_md();
                    },
                    success:function(response){

                        var data_response = response.data;
                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);

                        }else{
                            $("#alert_warning").trigger("click", data_response.output);
                        }
                    $('#modalDetail').modal('hide');
                    $('#btn-filter').click();
                    $('#queue_md').val('');
                    },
                    error:function(response){
                    myalert('error', response);
                    console.log(response);
                    }
                });
            }else{
                myalert('warning','Select mo first');
            }
        });

        $('#table-list').on('click','.btn-tb',function(){
            var pcd =  $(this).data('pcd');
            var style = $(this).data('style');
            var article =  $(this).data('article');
            var size  = $(this).data('size');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : $('#splitTopBotLink').attr('href'),
                data: {pcd:pcd,style:style,article:article,size_category:size},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    var data_response = response.data;
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);

                    }else{
                        $("#alert_warning").trigger("click", data_response.output);

                    }
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                }
            });
        });

        $('#table-list').on('click','.btn-marge',function(){
            var pcd =  $(this).data('pcd');
            var style = $(this).data('style');
            var article =  $(this).data('article');
            var size  = $(this).data('size');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : $('#margeTopBotLink').attr('href'),
                data: {pcd:pcd,style:style,article:article,size_category:size},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    var data_response = response.data;
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);

                    }else{
                        $("#alert_warning").trigger("click", data_response.output);
                    }
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                },
                eror:function(response){
                    myalert('error',response);
                    return false;
                }
            });
        });

        $('#sync_plan').on('click', function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : $('#dailySyncLink').attr('href'),
                beforeSend:function(){
                    mulaiLoading();
                },
                success:function(response){
                    selesaiLoading();
                    myalert('success',response);
                },
                eror:function(response){
                    myalert('danger',response);

                }
            });
        });

        $('#sync_plan7').on('click', function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : $('#dailySync7Link').attr('href'),
                beforeSend:function(){
                    mulaiLoading();
                },
                success:function(response){
                    selesaiLoading();
                    myalert('success',response);
                },
                eror:function(response){
                    myalert('danger',response);
                }
            });
        });

        $('#btn-sync-day').on('click', function() {
            var cuttingDate = $('#date_cutting').val();

            console.log(cuttingDate);

            if(cuttingDate == '') {
                $("#alert_warning").trigger("click", 'Tanggal Cutting Masih Kosong!');
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url: $('#dailySyncDayLink').attr('href'),
                    data: {
                        cutting_date: cuttingDate,
                    },
                    beforeSend:function(){
                        mulaiLoading();
                    },
                    success:function(response){
                        selesaiLoading();
                        myalert('success',response);
                    },
                    eror:function(response){
                        myalert('danger',response);
                    }
                });
            }
        });

        $('#date_cutting').change(function() {
            var d = new Date($(this).val());
            $('#date_cutting_download').val($(this).val());
        });

        $('#submit_filter_combine_plan').on('click', function() {
            var cutting_date = $('#cutting_date_combine_plan').val();

            if(!cutting_date){
                $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
                return false;
            } else {
                getDataCombinePlan(cutting_date);
            }
        });

        $('#combineModal').on('hidden.bs.modal', function(){
            $('#cutting_date_combine_plan').pickadate('picker').set('select', null);
            $('#combineTable').DataTable().clear();
            $('#combineTable').DataTable().destroy();
        });

        $('#combineTable').on('click','.btn-combine-plan-action',function(){
            var cutting_date =  $(this).data('cutting_date');
            var style = $(this).data('style');
            var articleno =  $(this).data('articleno');
            var size_category = $(this).data('size_category');
            var queu = $(this).data('queu');
            var plan_id = $(this).data('plan_id');
            var mo_created = $(this).data('mo_created');
            var color = $(this).data('color');
            var color_code = $(this).data('color_code');
            var material = $(this).data('material');
            var top_bot = $(this).data('top_bot');
            var season = $(this).data('season');
            var factory_id = $(this).data('factory_id');
            var combine_planning_id = $('#combine_planning_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : $('#combinePlanActionLink').attr('href'),
                data: {
                    cutting_date: cutting_date,
                    style: style,
                    articleno: articleno,
                    size_category: size_category,
                    queu: queu,
                    plan_id: plan_id,
                    mo_created: mo_created,
                    color: color,
                    color_code: color_code,
                    material: material,
                    top_bot: top_bot,
                    season: season,
                    factory_id: factory_id,
                    combine_planning_id: combine_planning_id,
                },
                beforeSend:function(){
                    mulaiLoading();
                },
                success:function(response){
                    $("#alert_success").trigger("click", 'combine plan berhasil!');
                    $('#submit_filter_combine_plan').click();
                    selesaiLoading();
                },
                eror:function(response){
                    $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });
    });

    function delplan(e){
        var _token = $("input[name='_token]").val();
        var pcd = e.getAttribute('data-date');
        var style = e.getAttribute('data-style');
        var article = e.getAttribute('data-article');
        var queu = e.getAttribute('data-queu');
        var po_buyer = e.getAttribute('data-po');
        var tableLD = $('#table-list-detail').DataTable();
        bootbox.confirm("Are you sure delete this ?", function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: $('#delpartLink').attr('href'),
                    type: "POST",
                    data:{
                        "pcd": pcd,
                        "style": style,
                        "article": article,
                        "queu": queu,
                        "po_buyer":po_buyer,
                        "_token": _token,
                    },
                    success: function(response){
                        $("#alert_success").trigger("click", 'Success Deleted');

                        tableLD.clear();
                        tableLD.draw();
                    },
                    error: function(response) {
                        console.log("error");
                    }
                });
            }
        });
    }

    function combineModal(url) {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                mulaiLoading();
            },
            success: function (response) {
                selesaiLoading();
                $('#combine_planning_id').val(response.id);
                $('#combineModal').modal();
            },
            error: function (response) {
                selesaiLoading();
                $("#alert_success").trigger("click", response.responseJSON);
            }
        });
    }

    function getDataCombinePlan(cutting_date) {
        var combine_planning_id = $('#combine_planning_id').val();
        var table1 = $('#combineTable').DataTable({
            destroy: true,
            bFilter: false,
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                url: $('#getDataCombinePlanLink').attr('href'),
                data: {
                    cutting_date: cutting_date,
                    combine_planning_id: combine_planning_id,
                },
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
            ],
            fnCreatedRow: function (row, data, index) {
                var info = table1.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(2).css('max-width','130px');
                $('td', row).eq(4).css('max-width','80px');
                $('td', row).eq(5).css('max-width','100px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style'},
                {data: 'articleno', name: 'articleno'},
                {data: 'size_category', name: 'size_category'},
                {data: 'color_name', name: 'color_name'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

        return table1;
    }

    function cancelCombine(url) {
        var combine_planning_id = $('#combine_planning_id').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "post",
            url: url,
            data: {
                combine_planning_id: combine_planning_id,
            },
            beforeSend: function () {
                mulaiLoading();
            },
            success:function(response){
                $("#alert_success").trigger("click", 'combine plan berhasil!');
                $('#submit_filter_combine_plan').click();
                selesaiLoading();
            },
            eror:function(response){
                $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    }

    function mulaiLoading() {
        $.blockUI({
            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait',
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent',
            },
            theme: true,
            baseZ: 2000
        });
    }

    function selesaiLoading() {
        $.unblockUI();
    }
</script>
@endsection
