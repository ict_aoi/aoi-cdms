<?php

namespace App\Http\Controllers\Spreading;

use DB;
use StdClass;
use Carbon\Carbon;
use App\Models\CuttingMarker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Spreading\FabricUsed;
use App\Models\SpreadingReportTemp;
use App\Models\MarkerDetailPO;

class ReviewSpreadingResultController extends Controller
{
    public function index()
    {
        return view('spreading.review_result.index');
    }

    public function getData(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                $role = DB::table('role_user')
                    ->where('user_id', \Auth::user()->id)
                    ->pluck('role_id')
                    ->toArray();

                $from = Carbon::parse($cutting_date);
                $from->hour(6);
                $from->minute(0);
                $from->second(0);

                $to = Carbon::parse($cutting_date)->addDays(1);
                $to->hour(5);
                $to->minute(59);
                $to->second(59);

                // Validate User Role
                if(in_array(1, $role) || in_array(10, $role) || in_array(11, $role) || in_array(6, $role) || in_array(29, $role) || in_array(30, $role)) {
                    $data = DB::table('ns_spreading_approval')
                        ->where('factory_id', \Auth::user()->factory_id)
                        ->whereBetween('time_stamp', [$from->format('Y-m-d H:i:s'), $to->format('Y-m-d H:i:s')]);

                } else {
                    $data = DB::table('ns_spreading_approval')
                        ->whereBetween('time_stamp', [$from->format('Y-m-d H:i:s'), $to->format('Y-m-d H:i:s')])
                        ->where('factory_id', \Auth::user()->factory_id)
                        ->where('user_id', \Auth::user()->id);

                }

                return datatables()->of($data)
                ->addColumn('view', function($data) {
                    return view('spreading.review_result._action', [
                        'data'      => $data,
                        'detail'     => route('reviewSpreadingResult.getRevisiData',$data->barcode_id)
                    ]);
                })
                ->editColumn('gl_status', function($data) {
                    if($data->gl_status == null) {
                        return '<a href="#" class="btn btn-default btn-icon btn-sm disabled"><i class="icon-database-time2"></i></a>';
                    } elseif ($data->gl_status == 'approve') {
                        return '<a href="#" class="btn btn-success btn-icon btn-sm disabled"><i class="icon-checkmark"></i></a>';
                    } else {
                        return view('spreading.review_result._action', [
                            'data'      => $data,
                            'revisi'     => route('reviewSpreadingResult.getDetailData',$data->barcode_id)
                        ]);
                    }
                })
                ->editColumn('qc_status', function($data) {
                    if($data->qc_status == null) {
                        return '<a href="#" class="btn btn-default btn-icon btn-sm disabled"><i class="icon-database-time2"></i></a>';
                    } elseif ($data->qc_status == 'approve') {
                        return '<a href="#" class="btn btn-success btn-icon btn-sm disabled"><i class="icon-checkmark"></i></a>';
                    } else {
                        return view('spreading.review_result._action', [
                            'data'      => $data,
                            'revisi'     => route('reviewSpreadingResult.getDetailData',$data->barcode_id)
                        ]);
                    }
                })
                ->editColumn('actual_use', function($data) {
                    return round($data->actual_use, 2);
                })
                ->addColumn('operator',function($data){
                    return $data->name;
                })
                ->rawColumns(['view','operator','gl_status','qc_status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function getDetailData($id)
    {
        $data = DB::table('ns_spreading_approval')
            ->where('barcode_id', $id)
            ->first();

        $get_marker = CuttingMarker::where('barcode_id', $data->barcode_id)
            ->where('deleted_at', null)
            ->first();

        $list_po = implode(', ', $get_marker->cutting_plan->po_details->pluck('po_buyer')->toArray());
        $list_ratio = array();
        foreach($get_marker->rasio_markers as $rsio) {
            $list_ratio[] = $rsio->size.'/'.$rsio->ratio;
        }
        $list_ratio = implode(', ', $list_ratio);
        $part_no = explode('+', $data->part_no);
        $data1 = DB::table('jaz_detail_size_per_part')
            ->where('cutting_date', $data->cutting_date)
            ->where('queu', $data->queu)
            ->whereIn('part_no', $part_no)
            ->whereNotNull('deliv_date')
            ->first();

        $obj = new StdClass();
        $obj->barcode_id = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style = $data->style;
        $obj->articleno = $data->articleno;
        $obj->size_category = $data->size_category;
        $obj->top_bot = $data->top_bot;
        $obj->season = $data->season;
        $obj->part_no = $data->part_no;
        $obj->cut = $data->cut;
        $obj->layer = $data->layer;
        $obj->layer_plan = $data->layer_plan;
        $obj->color_name = $data1->color_name;
        $obj->color_code = $data1->color_code;
        $obj->deliv_date = $data1->deliv_date;
        $obj->item_code = $data1->item_code;
        $obj->fabric_width = $data->fabric_width;
        $obj->marker_length = $data->marker_length;
        $obj->qty_ratio = $data->qty_ratio;
        $obj->fabric_width = $data->fabric_width;
        $obj->list_ratio = $list_ratio;
        $obj->list_po = $list_po;
        $obj->data_detail = FabricUsed::where('barcode_marker', $id)
            ->orderBy('created_spreading', 'asc')
            ->get();

		return response()->json($obj,200);
    }

    public function getRevisiData($id)
    {
        $data = DB::table('ns_spreading_approval')
            ->where('barcode_id', $id)
            ->first();

        $get_marker = CuttingMarker::where('barcode_id', $data->barcode_id)
            ->where('deleted_at', null)
            ->first();


        $list_po = implode(', ', MarkerDetailPO::select('po_buyer')->where('barcode_id', $id)->groupBy('po_buyer')->pluck('po_buyer')->toArray());
        // $list_po = implode(', ', $get_marker->cutting_plan->po_details->pluck('po_buyer')->toArray());
        $list_ratio = array();
        foreach($get_marker->rasio_markers as $rsio) {
            $list_ratio[] = $rsio->size.'/'.$rsio->ratio;
        }
        $list_ratio = implode(', ', $list_ratio);
        $part_no = explode('+', $data->part_no);
        $data1 = DB::table('jaz_detail_size_per_part')
            ->where('cutting_date', $data->cutting_date)
            ->where('queu', $data->queu)
            ->whereIn('part_no', $part_no)
            ->where('factory_id', \Auth::user()->factory_id)
            ->whereNotNull('deliv_date')
            ->first();

        $obj = new StdClass();
        $obj->barcode_id = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style = $data->style;
        $obj->articleno = $data->articleno;
        $obj->size_category = $data->size_category;
        $obj->top_bot = $data->top_bot;
        $obj->season = $data->season;
        $obj->part_no = $data->part_no;
        $obj->cut = $data->cut;
        $obj->layer = $data->layer;
        $obj->layer_plan = $data->layer_plan;
        $obj->color_name = $data1->color_name;
        $obj->color_code = $data1->color_code;
        $obj->deliv_date = $data1->deliv_date;
        $obj->item_code = $data1->item_code;
        $obj->fabric_width = $data->fabric_width;
        $obj->marker_length = $data->marker_length;
        $obj->qty_ratio = $data->qty_ratio;
        $obj->fabric_width = $data->fabric_width;
        $obj->list_ratio = $list_ratio;
        $obj->list_po = $list_po;
        $obj->data_detail = FabricUsed::where('barcode_marker', $id)->orderBy('created_spreading', 'asc')->get();
		// $obj->url_upload = route('downloadFileSSP.dataFileUpload');

		return response()->json($obj,200);
    }

    public function updateActualLayer(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $actual_layer = $request->actual_layer;
            $barcode_marker = $request->barcode_marker;
            $total_length_allow = $request->detail_view_allow;

            if($id != null && $actual_layer != null && $total_length_allow != null && $barcode_marker != null) {
                $total_length_allow_in_inch = $total_length_allow / 36;
                $actual_use = $total_length_allow_in_inch * $actual_layer;
                $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if($data_row != null) {

                    $actual_sisa = $data_row->actual_sisa != null ? $data_row->actual_sisa : 0;
                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $reject = $data_row->reject != null ? $data_row->reject : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                    $remark = ($actual_use + $actual_sisa + $sambungan + $reject + $sambungan_end) - $data_row->qty_fabric;

                    $actual_use = $actual_use + $sambungan_end + $sambungan;
                }

                $data = $data_query->update([
                    'actual_layer' => $actual_layer,
                    'actual_use' => $actual_use,
                    'remark' => $remark
                ]);

                $data_spreading = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                $increment_layer_add = 0;

                foreach($data_spreading as $key => $a) {
                    $akumulasi_layer = 0;
                    for($i = 0; $i <= $key; $i++) {
                        $akumulasi_layer += $data_spreading[$i]->actual_layer;
                    }

                    if($key != 0) {
                        if($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                            $increment_layer_add = $increment_layer_add + 1;
                        }
                    }

                    $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                    FabricUsed::where('id', $a->id)->update([
                        'akumulasi_layer' => $akumulasi_layer
                    ]);
                }

                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data,
                    'barcode_marker' => null
                ],442);
            }
        }
    }

    public function updateActualSisa(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $actual_sisa = $request->actual_sisa;
            $total_length_allow = $request->detail_view_allow;

            if($id != null && $barcode_marker != null) {

                if($actual_sisa == null || $actual_sisa == '') {
                    $actual_sisa = 0;
                }

                $total_length_allow_in_inch = $total_length_allow / 36;
                $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if($data_row != null) {

                    $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                    $actual_use = $total_length_allow_in_inch * $actual_layer;

                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $reject = $data_row->reject != null ? $data_row->reject : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                    $remark = ($actual_use + $actual_sisa + $sambungan + $reject + $sambungan_end) - $data_row->qty_fabric;
                }

                $data = $data_query->update([
                    'actual_sisa' => $actual_sisa,
                    'remark' => $remark
                ]);
                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data,
                    'barcode_marker' => null
                ],442);
            }
        }
    }

    public function updateReject(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $reject = $request->reject;
            $barcode_marker = $request->barcode_marker;
            $total_length_allow = $request->detail_view_allow;

            if($id != null && $barcode_marker != null) {

                if($reject == null || $reject == '') {
                    $reject = 0;
                }

                $total_length_allow_in_inch = $total_length_allow / 36;
                $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if($data_row != null) {

                    $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                    $actual_use = $total_length_allow_in_inch * $actual_layer;

                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $actual_sisa = $data_row->actual_sisa != null ? $data_row->actual_sisa : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                    $remark = ($actual_use + $actual_sisa + $sambungan + $reject + $sambungan_end) - $data_row->qty_fabric;
                }

                $data = $data_query->update([
                    'reject' => $reject,
                    'remark' => $remark
                ]);
                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data,
                    'barcode_marker' => null
                ],442);
            }
        }
    }

    public function updateSambungan(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $sambungan = $request->sambungan;
            $barcode_marker = $request->barcode_marker;
            $total_length_allow = $request->detail_view_allow;

            if($id != null && $barcode_marker != null) {

                try
                {
                    DB::beginTransaction();

                    if($sambungan == null || $sambungan == '') {
                        $sambungan = 0;
                    }

                    $total_length_allow_in_inch = $total_length_allow / 36;
                    $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                    $data_row = $data_query->first();

                    $remark = 0;

                    if($data_row != null) {

                        $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                        $actual_use = $total_length_allow_in_inch * $actual_layer;

                        $reject = $data_row->reject != null ? $data_row->reject : 0;
                        $actual_sisa = $data_row->actual_sisa != null ? $data_row->actual_sisa : 0;
                        $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                        $actual_use = $actual_use + $sambungan_end + $sambungan;

                        $remark = ($actual_use + $actual_sisa + $reject) - $data_row->qty_fabric;
                    }

                    $data = $data_query->update([
                        'sambungan' => $sambungan,
                        'remark' => $remark,
                        'actual_use' => $actual_use
                    ]);
                    $data_spreading = FabricUsed::where('barcode_marker', $barcode_marker)
                        ->orderBy('created_spreading', 'asc')
                        ->get();

                    $increment_layer_add = 0;

                    foreach($data_spreading as $key => $a) {
                        $akumulasi_layer = 0;
                        for($i = 0; $i <= $key; $i++) {
                            $akumulasi_layer += $data_spreading[$i]->actual_layer;
                        }

                        if($key != 0) {
                            if($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                                $increment_layer_add = $increment_layer_add + 1;
                            }
                        }

                        $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                        FabricUsed::where('id', $a->id)->update([
                            'akumulasi_layer' => $akumulasi_layer
                        ]);
                    }

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data,
                    'barcode_marker' => null
                ],442);
            }
        }
    }

    public function updateSambunganEnd(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $sambungan_end = $request->sambungan_end;
            $barcode_marker = $request->barcode_marker;
            $total_length_allow = $request->detail_view_allow;

            if($id != null && $barcode_marker != null) {

                try
                {
                    DB::beginTransaction();

                    if($sambungan_end == null || $sambungan_end == '') {
                        $sambungan_end = 0;
                    }

                    $total_length_allow_in_inch = $total_length_allow / 36;
                    $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                    $data_row = $data_query->first();

                    $remark = 0;

                    if($data_row != null) {

                        $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                        $actual_use = $total_length_allow_in_inch * $actual_layer;

                        $reject = $data_row->reject != null ? $data_row->reject : 0;
                        $actual_sisa = $data_row->actual_sisa != null ? $data_row->actual_sisa : 0;
                        $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;

                        $actual_use = $actual_use + $sambungan_end + $sambungan;

                        $remark = ($actual_use + $actual_sisa + $reject) - $data_row->qty_fabric;
                    }

                    $data = $data_query->update([
                        'sambungan_end' => $sambungan_end,
                        'remark' => $remark,
                        'actual_use' => $actual_use
                    ]);
                    $data_spreading = FabricUsed::where('barcode_marker', $barcode_marker)
                        ->orderBy('created_spreading', 'asc')
                        ->get();

                    $increment_layer_add = 0;

                    foreach($data_spreading as $key => $a) {
                        $akumulasi_layer = 0;
                        for($i = 0; $i <= $key; $i++) {
                            $akumulasi_layer += $data_spreading[$i]->actual_layer;
                        }

                        if($key != 0) {
                            if($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                                $increment_layer_add = $increment_layer_add + 1;
                            }
                        }

                        $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                        FabricUsed::where('id', $a->id)->update([
                            'akumulasi_layer' => $akumulasi_layer
                        ]);
                    }

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data, 'barcode_marker' => null
                ],442);
            }
        }
    }

    public function updateKualitasGelaran(Request $request)
    {
        if(request()->ajax())
        {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $kualitas_gelaran = $request->kualitas_gelaran;
            if($id != null && $barcode_marker != null) {

                $data_query = FabricUsed::where('id', $id)->where('deleted_at', null);
                $data_row = $data_query->get()->first();

                $data = $data_query->update([
                    'kualitas_gelaran' => $kualitas_gelaran
                ]);

                $data_spreading_new = FabricUsed::where('barcode_marker', $barcode_marker)
                    ->orderBy('created_spreading', 'asc')
                    ->get();

                return response()->json([
                    'data' => $data_spreading_new,
                    'barcode_marker' => $data_row->barcode_marker
                ],200);
            } else {
                $data = null;
                return response()->json([
                    'data' => $data,
                    'barcode_marker' => null
                ],442);
            }
        }
    }

    public function downloadGenerate(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != null && $cutting_date != '') {
                $obj = new StdClass();
                $obj->download_link = route('reviewSpreadingResult.download', $cutting_date);
                return response()->json($obj,200);
            } else {
                return response()->json('Pilih tanggal cutting terlebih dahulu',422);
            }
        }
    }

    public function download(Request $request, $cutting_date)
    {
        $cutting_date = $request->cutting_date;

        if($cutting_date != null && $cutting_date != '') {
            $from = Carbon::parse($cutting_date);
            $from->hour(6);
            $from->minute(0);
            $from->second(0);

            $to = Carbon::parse($cutting_date)->addDays(1);
            $to->hour(5);
            $to->minute(59);
            $to->second(59);

            $data = DB::table('ns_spreading_approval')
                ->where('factory_id', \Auth::user()->factory_id)
                ->whereBetween('time_stamp', [$from->format('Y-m-d H:i:s'), $to->format('Y-m-d H:i:s')])
                ->orderBy('time_stamp', 'asc');

            if(count($data->get()) < 1)
            {
                return response()->json('Data tidak ditemukan.', 422);
            } else {
                $filename = 'Approval_Spreading_GL_'.$cutting_date;

                $i = 1;

                $export = \Excel::create($filename, function($excel) use ($data, $i) {
                    $excel->sheet('report', function($sheet) use($data, $i) {
                        $sheet->appendRow(array(
                            'PLAN',
                            'STYLE',
                            'ARTICLE',
                            'MARKER ID',
                            'PART NO',
                            'CUT NO',
                            'PEMAKAIAN FABRIC',
                            'GL APPROVAL',
                            'QC APPROVAL',
                            'TABLE',
                            'OPERATOR',
                            'START SPREAD',
                            'END SPREAD'
                        ));
                        $data->chunk(100, function($rows) use ($sheet, $i)
                        {
                            foreach ($rows as $row)
                            {
                                $data_excel = array(
                                    $row->cutting_date,
                                    $row->style,
                                    $row->articleno,
                                    $row->barcode_id,
                                    $row->part_no,
                                    $row->cut,
                                    $row->actual_use,
                                    $row->gl_status,
                                    $row->qc_status,
                                    $row->table_name,
                                    $row->name,
                                    $row->spread_start,
                                    $row->time_stamp
                                );

                                $sheet->appendRow($data_excel);
                            }
                        });
                    });
                })->download('xlsx');

                return response()->json(200);
            }
        } else {
            return response()->json('Select tanggal cutting terlebih dahulu!.', 422);
        }
    }
}
