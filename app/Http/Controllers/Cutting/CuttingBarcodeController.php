<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DetailPlan;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;
use Uuid;

class CuttingBarcodeController extends Controller
{
    //
    public function index(Request $request)
    {
        $styles = DB::connection('erp_live')
                    ->table('rma_style_fg')
                    ->select(DB::raw('upc as style'))
                    ->groupBy('upc')
                    ->get();

        $factories = DB::table('master_factory')
                        ->where('id', Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->first();

        return view('cutting.barcode.index', compact('styles', 'factories'));
    }

    public function ajaxGetSize(Request $request)
    {
        $style = $request->style;

    }

    public function ajaxGetDataMarker(Request $request)
    {
        $data = DB::table('cutting_marker')
                    ->join('scan_cuttings', 'scan_cuttings.barcode_marker', '=', 'cutting_marker.barcode_id')
                    ->where('cutting_marker.is_body', true)
                    // ->where('scan_cuttings.state', 'done')
                    ->whereNotNull('scan_cuttings.start_time')
                    ->where('scan_cuttings.factory_id', Auth::user()->factory_id)
                    ->get();

        return view('cutting.barcode.list_marker', compact('data'));
    }

    public function ajaxSetMarker(Request $request)
    {
        $marker = DB::table('cutting_marker')
                    ->join('cutting_plans2', 'cutting_plans2.id', '=', 'cutting_marker.id_plan')
                    ->where('cutting_marker.barcode_id', $request->barcode_id)
                    ->where('cutting_plans2.factory_id', Auth::user()->factory_id)
                    ->first();
        
        if($marker == null){
            return response()->json('Barcode Marker Tidak Dikenali', 422);
        }

        $check_header = DB::table('bundle_header')->select('size')->where('barcode_marker',$request->barcode_id)->distinct('size')->pluck('size')->toArray();

        $data = [
            'marker' => $marker,
            'check_header'  => $check_header
        ];

        if ($data == null) {
            return response()->json('data not found..!', 422);
        }

        return response()->json($data, 200);
    }

    public function generateBarcode(Request $request)
    {
         $barcode_marker = $request->barcode_id;
         $cut_number     = $request->cut_number;
         $layer          = $request->layer;
         $max_bundle     = $request->max_bundle;
         $box_lim        = $request->box_lim;
         $cutter         = $request->cutter;
         $note           = $request->note;
         $style          = $request->style;
         $id_plan        = $request->id_plan;

        $data_marker = DB::table('cutting_marker')
                            ->leftJoin('types', 'types.id', '=', 'cutting_marker.set_type')
                            ->where('cutting_marker.barcode_id', $barcode_marker)
                            ->first();

        $cutting_plans2 = DB::table('cutting_plans2')
                            ->where('id', $id_plan)
                            ->first();

        if ($data_marker->set_type != null && $data_marker->set_type != 1) {
            $master_style = DB::table('v_master_style')
                                 ->where('style', $style)
                                 ->where('season_name', $cutting_plans2->season)
                                 ->where('type_id', $data_marker->set_type)
                                 ->get();
        }else{

            $master_style = DB::table('v_master_style')
                                 ->where('style', $style)
                                 ->where('season_name', $cutting_plans2->season)
                                 ->get();
        }

        //VALIDASI UNTUK CHECK PERMINTAAN MARKER SALAH DEFINE SET GARMENT JADI 1 MARKER TETAPI PART TIDAK DI COMBINE
        // $po_buyer = DB::table('detail_cutting_plan')->where('id_plan',$id_plan)->first();
        // $set_gmt_check = DB::table('data_cuttings')->select('style')
        // ->where(function($query) use ($po_buyer){
        //     $query->where('po_buyer',$po_buyer->po_buyer)
        //     ->orWhere('job_no','like','%'.$po_buyer->po_buyer.'%');
        // })->distinct('style')->get();
        // $part_as_set = explode('+',$data_marker->part_no);
        // if(count($part_as_set) == 1 && count($set_gmt_check) > 1 && $data_marker->set_type == 1){
        //     return response()->json('HARAP PISAH TOP / BOTTOM DENGAN ID MARKER BERBEDA!',422);
        // }

        $ratio_cutting_detail = DB::table('dd_ratio_cutting_detail') //before v_ratio_cutting_detail
                                ->where('barcode_id', $barcode_marker)
                                ->where('id_plan', $id_plan)
                                ->where('qty','>',0)
                                ->get()->toArray();

        // cek style sudah diinput komponen atau belum
        $cekkomponen = DB::table('v_master_style')
                ->where('style', $style)
                ->where('season_name', $cutting_plans2->season)
                ->exists();

        if (!$cekkomponen) {
            return response()->json('tolong input komponen style terlebih dahulu !', 422);
        }

        // // cek spreaidng atau belum

        if(!in_array(\Auth::user()->nik, array('131005357','1310053571'))){
            $cek_spreading = DB::table('fabric_useds')
                        ->where('barcode_marker', $barcode_marker)
                        ->exists();
            if (!$cek_spreading) {
                return response()->json('barcode marker belum di spreading !', 422);
            }
        }
        // cek cutting marker
        $cekmarker = DB::table('cutting_marker')
                        ->where('barcode_id', $barcode_marker)
                        ->where(function($query){
                            $query->orWhere('is_bundling',true);
                            $query->orWhere('is_body',false);
                        })
                        ->exists();

        if ($cekmarker) {
            return response()->json('barcode marker sudah dibuat atau bukan body !', 422);
        }

        $result = array_reduce($ratio_cutting_detail, function($carry, $item) {
            if(!isset($carry[$item->po_buyer.$item->size])) {
                $carry[$item->po_buyer.$item->size] = $item;
            } else {
                $carry[$item->po_buyer.$item->size]->qty += $item->qty;
            }
            return $carry;
        });

        $result = $result !=null ? array_values($result) : array();

        $data_bundle_header = array();
        $data_bundle_detail = array();
        $data_bundle_detail_ = array();
        $data_cutting_movement = array();

         try {
             DB::beginTransaction();

             $no = 1;
             $from = 1;
             $to = $max_bundle - 1;
             $qty = $max_bundle;

             $no_ = 50000;
             $data_result = array();

            foreach ($result as $key => $val) {
                    $color = $val->color;
                    $style = $val->style;

                    // get color from data cutting
                    if ($data_marker->set_type == 2) {
                        $style = $style.'-TOP';
                    }elseif ($data_marker->set_type == 3) {
                        $style = $style.'-BOT';
                    }
                    $data_cuts = DB::table('data_cuttings')
                                    ->select(DB::raw('po_buyer,style,season, color_name, max(cons)'))
                                    ->where('po_buyer', $val->po_buyer)
                                    ->where('season', $val->season)
                                    ->where('style', $style)
                                    ->where('color_name',$val->color)
                                    ->groupBy(['po_buyer','style','season', 'color_name'])
                                    ->orderByRaw('max(cons) DESC')
                                    ->first();

                    if($data_cuts){
                        $header['color_name'] = $data_cuts->color_name ? $data_cuts->color_name : $color;
                    }
                    else{
                        $header['color_name'] = $color;
                    }
                    $header['id'] = Uuid::generate()->string;
                    $header['barcode_marker'] = $barcode_marker;
                    $header['season'] = $val->season;
                    $header['style'] = $val->style;
                    $header['set_type'] = $data_marker->type_name != null ? $data_marker->type_name : 'Non';
                    $header['poreference'] =  $val->po_buyer;
                    $header['article'] = $val->articleno;
                    $header['cut_num'] = $cut_number;
                    $header['size'] = $val->size;
                    $header['factory_id'] = $cutting_plans2->factory_id;
                    $header['cutting_date'] = $val->cutting_date;
                    $header['cutter'] = $cutter;
                    $header['note'] = $note;
                    $header['admin'] = Auth::user()->id;
                    $header['qc_operator'] = Auth::user()->id; //?
                    $header['bundle_operator'] = Auth::user()->id;
                    $header['max_bundle'] = $max_bundle;
                    $header['qty'] = $val->qty;
                    $header['created_at'] = Carbon::now();
                    $header['updated_at'] = Carbon::now();
                    $header['job'] = $val->job;
                    $header['admin_name'] = Auth::user()->name;
                    $header['qc_operator_name'] = Auth::user()->name;
                    $header['bundle_operator_name'] = Auth::user()->name;
                    $header['statistical_date'] = $val->statistical_date;
                    $header['destination'] = $val->destination;
                    $header['box_lim'] = $box_lim;
                    // $header['cut_info'] = $val->cut_info;
                    $data_bundle_header[] = $header;

                }

            DB::table('bundle_header')->insert($data_bundle_header);

            $LastInsertId = DB::table('bundle_header')->latest('created_at')->first()->id;

            if ($LastInsertId) {

                foreach ($master_style as $key1 => $val1) {
                    foreach ($data_bundle_header as $key2 => $val2) {

                        $total_qty = $val2['qty'];
                        $totalan = $total_qty + ($from-1);
                        $qty_modulus = $total_qty%$qty;


                            for ($i=1; $i <= $totalan; $i+=$qty) {

                                $panel = $i;
                                $panel_to = $panel+$to;

                                if ($panel_to > $totalan) {
                                    $panel_to = $totalan;

                                    // if (($panel_to-$panel)<=5) {
                                    //     continue;
                                    // }
                                }

                                $qty = ($panel_to-$panel)+1;

                                $is_recycle = DB::table('data_cuttings')
                                ->select('is_recycle')
                                ->where('po_buyer', $val2['poreference'])
                                ->groupBy('is_recycle')
                                ->first();

                                $detail['barcode_id']       = $this->generateBarcodeNumber($no);
                                $detail['bundle_header_id'] = $val2['id'];
                                $detail['style_detail_id']  = $val1->id;
                                $detail['komponen_name']    = $val1->komponen_name;
                                $detail['start_no']         = $panel;
                                $detail['end_no']           = $panel_to;
                                $detail['qty']              = $qty;
                                $detail['location']         = 'barcoding';
                                $detail['factory_id']       = $cutting_plans2->factory_id;
                                $detail['sds_barcode']      = null;
                                $detail['created_at']       = Carbon::now();
                                $detail['updated_at']       = Carbon::now();
                                $detail['job']              = $val2['job'];
                                $detail['is_recycle']       = $is_recycle->is_recycle;
                                $detail['size']             = $val2['size'];
                                //
                                $data_bundle_detail[] = $detail;

                                // data cutting movement
                                $move['barcode_id']   = $detail['barcode_id'];
                                $move['process_from'] = null;
                                $move['status_from']  = null;
                                $move['process_to']   = 'barcoding';
                                $move['status_to']    = 'onprogress';
                                $move['user_id']      = Auth::user()->id;
                                $move['description']  = 'barcoding';
                                $move['ip_address']   = \Request::ip();
                                $move['created_at']   = Carbon::now();
                                $move['updated_at']   = Carbon::now();
                                $move['barcode_type'] = 'bundling';

                                $data_cutting_movement[] = $move;

                                $no++;

                            }

                    }

                }
            }

            $data_bundle_detail_new = array();
            $newkeytest = 0;
            foreach ($data_bundle_detail as $key => $val) {
                $data_bundle_detail_new[$val['size'].$val['style_detail_id']][$newkeytest] = $val;

                $newkeytest++;
            }

            $rows = array();
            $rows_new = array();
            $rows_new2 = array();
            $inc = 0;
            foreach ($data_bundle_detail_new as $vall) {
                $num = 1;
                foreach($vall as $row){
                    $row['num'] = $num;
                    $rows[] = $row;


                    $inc++;
                    $num++;
                }
            }

            foreach ($rows as $k => $v) {
                if ($v['num'] != 1) {
                    $rows[$k]['start_no'] = $rows[$k-1]['end_no']+1;
                    $rows[$k]['end_no'] = $rows[$k-1]['end_no'] + $v['qty'];
                }
            }


            foreach ($rows as $kk => $vv) {
                $rr['barcode_id']       = $vv['barcode_id'];
                $rr['bundle_header_id'] = $vv['bundle_header_id'];
                $rr['style_detail_id']  = $vv['style_detail_id'];
                $rr['komponen_name']    = $vv['komponen_name'];
                $rr['start_no']         = $vv['start_no'];
                $rr['end_no']           = $vv['end_no'];
                $rr['qty']              = $vv['qty'];
                $rr['location']         = $vv['location'];
                $rr['factory_id']       = $vv['factory_id'];
                $rr['sds_barcode']      = $vv['sds_barcode'];
                $rr['created_at']       = $vv['created_at'];
                $rr['updated_at']       = $vv['updated_at'];
                $rr['job']              = $vv['job'];
                $rr['is_recycle']       = $vv['is_recycle'];

                $rows_new[] = $rr;
            }

            // insert bundle detail
            $insert = DB::table('bundle_detail')->insert($rows_new);

            if ($insert) {
                foreach ($data_bundle_header as $key3 => $val3) {
                    $bundle_header_id = $val3['id'];
                    $qty_header = $val3['qty']; //55

                    $dat_barcode = DB::table('bundle_detail')
                                    ->select(DB::raw('max(barcode_id) as max'))
                                    ->where('bundle_header_id', $bundle_header_id)
                                    ->first();

                    $dat_detail = DB::table('bundle_detail')
                                    ->where('bundle_header_id', $bundle_header_id)
                                    ->where('barcode_id', $dat_barcode->max)
                                    ->first();

                    $qty_sisa = $dat_detail->qty;
                    if ($max_bundle<$qty_header) {

                        if ($max_bundle>$qty_sisa) {
                            if ($qty_sisa<=5) {

                                $list_barcode = array();

                                $dat_list =  DB::table('bundle_detail')
                                                ->where('bundle_header_id', $bundle_header_id)
                                                ->where('end_no', $dat_detail->end_no)
                                                ->get();

                                foreach ($dat_list as $k3 => $v3) {
                                    $list_barcode[] = $v3->barcode_id;
                                }

                                // delete movement
                                DB::table('cutting_movements')
                                        ->whereIn('barcode_id', $list_barcode)
                                        ->delete();

                                $delete =  DB::table('bundle_detail')
                                                // ->where('bundle_header_id', $bundle_header_id)
                                                // ->where('end_no', $dat_detail->end_no)
                                                ->whereIn('barcode_id', $list_barcode)
                                                ->delete();

                                if ($delete) {

                                    $dat_barcodex = DB::table('bundle_detail')
                                            ->select(DB::raw('max(barcode_id) as max'))
                                            ->where('bundle_header_id', $bundle_header_id)
                                            ->first();

                                    $dat_detailx = DB::table('bundle_detail')
                                                    ->where('bundle_header_id', $bundle_header_id)
                                                    ->where('barcode_id', $dat_barcodex->max)
                                                    ->first();

                                    // update
                                    DB::table('bundle_detail')
                                            ->where('bundle_header_id', $bundle_header_id)
                                            ->where('end_no', $dat_detailx->end_no)
                                            ->update([
                                                'end_no' =>  $dat_detailx->end_no + $qty_sisa,
                                                'qty' => (($dat_detailx->end_no+$qty_sisa) - $dat_detailx->start_no)+1
                                                // 'updated_at' => Carbon::now()
                                            ]);
                                }
                            }
                        }
                    }
                }
            }

            // insert movement
            DB::table('cutting_movements')->insert($data_cutting_movement);

                //  update cutting marker
            DB::table('cutting_marker')->where('barcode_id', $barcode_marker)
                ->update([
                            'is_bundling' => true,
                            'updated_at' => Carbon::now()
                        ]);

            DB::commit();

         } catch (Exception $e) {
             DB::rollback();
             $message = $e->getMessage();
             ErrorHandler::db($message);
         }

        //  return response()->json(, 200);
    }

    //print preview
    public function printPreview(Request $request) {

        $barcode_marker = $request->barcode_marker;
        $size = isset($request->size) ? explode(',',$request->size) : 0;

        // cek cutting marker
        $cekmarker = DB::table('cutting_marker')
                        ->where('barcode_id', $barcode_marker)
                        ->where(function($query){
                            $query->orWhere('is_bundling',true);
                            $query->orWhere('is_body',false);
                        })
                        ->exists();

        if (!$cekmarker) {
            return response()->json('Data not Found..!', 422);
        }

        // cek apakah barcode sds sudah ada atau belum ?
        $ceksdsbarcode = DB::table('bundle_detail')
                            ->join('bundle_header','bundle_header.id','=','bundle_detail.bundle_header_id')
                            ->where('bundle_header.barcode_marker', $barcode_marker)
                            ->whereNull('bundle_detail.sds_barcode')
                            ->exists();

        if ($ceksdsbarcode) {
            return response()->json('Barcode SDS not found..!', 422);
        }
        //PENGECEKAN JIKA AWAL SUDAH CREATE BUNDLE LEWAT SDS MAKA TIDAK BOLEH DI CETAK LAGI LEWAT CDMS
        $sds_barcode_in_transaction = DB::table('bundle_detail')->Join('bundle_header','bundle_header.id','=','bundle_detail.bundle_header_id')
        ->where('bundle_header.barcode_marker',$barcode_marker)->whereNotNull('bundle_detail.sds_barcode')->pluck('sds_barcode')->toArray();
        if(count($sds_barcode_in_transaction) > 0){
            $cek_sds_transaction = DB::table('sds_detail_movements')->whereIn('sds_barcode',$sds_barcode_in_transaction)->get();
            if(count($cek_sds_transaction) > 0){
                return response()->json('DETAIL BARCODE SUDAH MENJALANI TRANSAKSI SCAN!', 422);
            }
        }

        $data = DB::table('bundle_detail')
                    ->select(
                            'bundle_detail.barcode_id',
                            'bundle_detail.bundle_header_id',
                            'bundle_detail.style_detail_id',
                            'bundle_detail.komponen_name',
                            'bundle_detail.start_no',
                            'bundle_detail.end_no',
                            'bundle_detail.qty',
                            'bundle_detail.location',
                            'bundle_detail.sds_barcode',
                            'bundle_detail.is_recycle',
                            'bundle_header.season',
                            'bundle_header.style',
                            'bundle_header.set_type',
                            'bundle_header.poreference',
                            'bundle_header.article',
                            'bundle_header.cut_num',
                            'bundle_header.size',
                            'bundle_header.factory_id',
                            'bundle_header.cutting_date',
                            'bundle_header.color_name',
                            'bundle_header.cutter',
                            'bundle_header.note',
                            'bundle_header.admin',
                            'bundle_header.qc_operator',
                            'bundle_header.bundle_operator',
                            'bundle_header.box_lim',
                            'bundle_header.max_bundle',
                            'bundle_header.barcode_marker',
                            'bundle_header.job',
                            'bundle_header.cut_info',
                            'v_master_style.part',
                            'v_master_style.process',
                            'v_master_style.process_name',
                            'v_master_style.type_id',
                            'v_master_style.type_name',
                            'master_factory.factory_name',
                            'master_factory.factory_alias',
                            'bundle_header.admin_name',
                            'bundle_header.qc_operator_name',
                            'bundle_header.bundle_operator_name',
                            'bundle_header.statistical_date',
                            'bundle_header.created_at',
                            'bundle_header.destination'
                    )
                    ->join('bundle_header','bundle_header.id','=','bundle_detail.bundle_header_id')
                    ->join('v_master_style', 'v_master_style.id', '=', 'bundle_detail.style_detail_id')
                    ->join('master_factory', 'master_factory.id', '=', 'bundle_header.factory_id');
                    // ->whereIn('bundle_header.size',['62']);
                    //JIKA JUMLAH SIZE TERLALU BANYAK DAN GAGAL PRINT TAMBAHKAN PARAMETER WHERE IN SIZE DI BAWAH INI
                    //KEMUDIAN NON AKTIFKAN PRINT CHECKLIST SEMENTARA DI index2.blade dan comment chunk_new
            if ($size != 0) {
                # code...
                $data = $data->whereIn('bundle_header.size',$size);
            }

            $data = $data->where('bundle_header.barcode_marker', $barcode_marker);


            $filename = 'barcode_marker_#' . $barcode_marker . '.pdf';

            $paper = $request->paper;

            //config layout
            if ($paper == 'a3') {
                $orientation = 'landscape';
                $papersize = 'a3';

                // $load_view = 'cutting.barcode.print.index';
                $load_view = 'cutting.barcode.print.index2';

            }elseif($paper == 'a3_kanban'){
                $orientation = 'landscape';
                $papersize = 'a3';

                // $load_view = 'cutting.barcode.print.index';
                $load_view = 'cutting.barcode.print.index_kanban';
            }elseif ($paper == 'a4') {
                $orientation = 'potrait';
                $papersize = 'a4';

                // $load_view = 'cutting.barcode.print.index_a4';
                $load_view = 'cutting.barcode.print.index2_a4';
            }else{
                $orientation = 'landscape';
                $papersize = 'a3';

                // $load_view = 'cutting.barcode.print.index';
                $load_view = 'cutting.barcode.print.index2';
            }

            $data = $data
                            ->orderBy('bundle_header.cut_num')
                            ->orderBy('bundle_header.style')
                            ->orderBy('v_master_style.type_id')
                            ->orderBy('bundle_header.size')
                            ->orderBy('bundle_detail.komponen_name')
                            ->orderBy('bundle_detail.start_no')
                            ->orderBy('bundle_detail.end_no', 'ASC')
                            ->orderBy('bundle_header.poreference')
                            ->get();
            
            if ($data[0]->factory_id != Auth::user()->factory_id) {
                return abort(403);
            }
            // get list komponen
            $list_komponen = DB::table('v_list_komponen')
                                    ->where('barcode_marker', $barcode_marker);

            if ($size != 0) {
                # code...
                $list_komponen = $list_komponen->whereIn('size',$size);
            }
            $list_komponen = $list_komponen->get();

            // get id plan
            $get_plan = DB::table('cutting_marker')
                            ->where('barcode_id', $barcode_marker)
                            ->first();

            $data_plan = DB::table('cutting_plans2')
                            ->where('id', $get_plan->id_plan)
                            ->first();

            // get bundle header
            $bundle_header = DB::table('bundle_header')
                                ->where('barcode_marker', $barcode_marker)
                                ->get();

            // test //

            $data_marker = DB::table('cutting_marker')
                            ->leftJoin('types', 'types.id', '=', 'cutting_marker.set_type')
                            ->where('cutting_marker.barcode_id', $barcode_marker)
                            ->first();

            // $season_array = array_unique($data->pluck('season')->toArray());
            if ($data_marker->set_type != null && $data_marker->set_type != 1) {
                $master_style = DB::table('v_master_style')
                                    ->where('style', $data[0]->style)
                                    ->where('season_name', $data[0]->season)
                                    ->where('type_id', $data_marker->set_type)
                                    // ->where('season_name', $season_array)
                                    ->get();
            }else{

                $master_style = DB::table('v_master_style')
                                 ->where('style', $data[0]->style)
                                 ->where('season_name', $data[0]->season)
                                //  ->whereIn('season_name', $season_array)
                                 ->get();
            }


            $data_bundle_header = DB::table('bundle_header')
                                    ->where('barcode_marker', $barcode_marker);
                                    // ID BUNDLE HEADER (MASUKKAN ID BUNDLE HEADER UNTUK CUSTOM CHECKLIST)
                                    // ->whereIn('id',[
                                    //     '2ffec160-e044-11ec-8e6d-09514d7be495'
                                    // ])
            if ($size != 0) {
                # code...
                $data_bundle_header = $data_bundle_header->whereIn('size',$size)->get();
            }else{
                $data_bundle_header = $data_bundle_header->get();
            }

            $no = 1;
            $from = 1;

            $data_bundle_detail = array();

            foreach ($master_style as $key1 => $val1) {
                foreach ($data_bundle_header as $key2 => $val2) {

                    $box_lim = $val2->box_lim > 0 ? $val2->box_lim : $val2->max_bundle;

                    $total_qty = $val2->qty;
                    $totalan = $total_qty + ($from-1);
                    $to = $box_lim - 1;
                    // $qty_modulus = $total_qty%$qty;


                        for ($i=1; $i <= $totalan; $i+=$box_lim) {

                            $panel = $i;
                            $panel_to = $panel+$to;
                            // $panel_to = $box_lim;

                            if ($panel_to > $totalan) {
                                $panel_to = $totalan;
                            }

                            $qty = ($panel_to-$panel)+1;

                            $detail['bundle_header_id'] =  $val2->id;
                            $detail['style_detail_id'] = $val1->id;
                            $detail['komponen_name'] = $val1->komponen_name;
                            $detail['start_no'] = $panel;
                            $detail['end_no'] = $panel_to;
                            $detail['qty'] = $qty;
                            $detail['location'] = 'barcoding';
                            $detail['sds_barcode'] = null;
                            $detail['created_at'] = Carbon::now();
                            $detail['updated_at'] = Carbon::now();
                            $detail['job'] = $val2->job;
                            $detail['season'] = $val2->season;
                            $detail['style'] = $val2->style;
                            $detail['set_type'] = $val2->set_type;
                            $detail['poreference'] = $val2->poreference;
                            $detail['article'] = $val2->article;
                            $detail['cut_num'] = $val2->cut_num;
                            $detail['size'] = $val2->size;
                            $detail['factory_id'] = $val2->factory_id;
                            $detail['cutting_date'] = $val2->cutting_date;
                            $detail['color_name'] = $val2->color_name;
                            $detail['cutter'] = $val2->cutter;
                            $detail['note'] = $val2->note;
                            $detail['admin'] = $val2->admin;
                            $detail['qc_operator'] = $val2->qc_operator;
                            $detail['bundle_operator'] = $val2->bundle_operator;
                            $detail['box_lim'] = $val2->box_lim;
                            $detail['max_bundle'] = $val2->max_bundle;
                            $detail['created_at'] = $val2->created_at;
                            $detail['updated_at'] = $val2->updated_at;
                            // $detail['qty'] = $val2->qty;
                            $detail['barcode_marker'] = $val2->barcode_marker;
                            $detail['job'] = $val2->job;
                            $detail['admin_name'] = $val2->admin_name;
                            $detail['qc_operator_name'] = $val2->qc_operator_name;
                            $detail['bundle_operator_name'] = $val2->bundle_operator_name;
                            $detail['statistical_date'] = $val2->statistical_date;
                            $detail['destination'] = $val2->destination;
                            $detail['part'] = $val1->part;
                            $detail['komponen'] = $val1->komponen;
                            $detail['total'] = $val1->total;
                            $detail['process'] = $val1->process;
                            $detail['process_name'] = $val1->process_name;
                            $detail['type_id'] = $val1->type_id;
                            $detail['type_name'] = $val1->type_name;


                            $data_bundle_detail[] = $detail;

                            $no++;

                        }

                }

            }

            $data_bundle_detail_new = array();
            $newkeytest = 0;
            foreach ($data_bundle_detail as $key => $val) {
                $data_bundle_detail_new[$val['size'].$val['style_detail_id']][$newkeytest] = $val;

                $newkeytest++;
            }

            $rows = array();
            $rows_new = array();
            $rows_new2 = array();
            $inc = 0;
            foreach ($data_bundle_detail_new as $vall) {
                $num = 1;
                foreach($vall as $row){
                    $row['num'] = $num;
                    $rows[] = $row;


                    $inc++;
                    $num++;
                }
            }

            foreach ($rows as $k => $v) {
                if ($v['num'] != 1) {
                    $rows[$k]['start_no'] = $rows[$k-1]['end_no']+1;
                    $rows[$k]['end_no'] = $rows[$k-1]['end_no'] + $v['qty'];
                }
            }


            foreach ($rows as $kk => $vv) {
                $rr['bundle_header_id'] =  $vv['bundle_header_id'];
                $rr['style_detail_id'] = $vv['style_detail_id'];
                $rr['komponen_name'] = $vv['komponen_name'];
                $rr['start_no'] = $vv['start_no'];
                $rr['end_no'] = $vv['end_no'];
                $rr['qty'] = $vv['qty'];
                $rr['location'] = $vv['location'];
                $rr['sds_barcode'] = $vv['sds_barcode'];
                $rr['created_at'] = $vv['created_at'];
                $rr['updated_at'] = $vv['updated_at'];
                $rr['job'] = $vv['job'];
                $rr['season'] = $vv['season'];
                $rr['style'] = $vv['style'];
                $rr['set_type'] = $vv['set_type'];
                $rr['poreference'] = $vv['poreference'];
                $rr['article'] = $vv['article'];
                $rr['cut_num'] = $vv['cut_num'];
                $rr['size'] = $vv['size'];
                $rr['factory_id'] = $vv['factory_id'];
                $rr['cutting_date'] = $vv['cutting_date'];
                $rr['color_name'] = $vv['color_name'];
                $rr['cutter'] = $vv['cutter'];
                $rr['note'] = $vv['note'];
                $rr['admin'] = $vv['admin'];
                $rr['qc_operator'] = $vv['qc_operator'];
                $rr['bundle_operator'] = $vv['bundle_operator'];
                $rr['box_lim'] = $vv['box_lim'];
                $rr['max_bundle'] = $vv['max_bundle'];
                $rr['created_at'] = $vv['created_at'];
                $rr['updated_at'] = $vv['updated_at'];
                $rr['barcode_marker'] = $vv['barcode_marker'];
                $rr['job'] = $vv['job'];
                $rr['admin_name'] = $vv['admin_name'];
                $rr['qc_operator_name'] = $vv['qc_operator_name'];
                $rr['bundle_operator_name'] = $vv['bundle_operator_name'];
                $rr['statistical_date'] = $vv['statistical_date'];
                $rr['destination'] = $vv['destination'];
                $rr['part'] = $vv['part'];
                $rr['komponen'] = $vv['komponen'];
                $rr['total'] = $vv['total'];
                $rr['process'] = $vv['process'];
                $rr['process_name'] = $vv['process_name'];
                $rr['type_id'] = $vv['type_id'];
                $rr['type_name'] = $vv['type_name'];

                $rows_new[] = $rr;
            }

            foreach ($rows_new as $kk1 => $vv1) {
                // cek apakah ada di bundle detail
                $cekbundledetail = DB::table('bundle_detail')
                                    ->where('bundle_header_id', $vv1['bundle_header_id'])
                                    ->where('style_detail_id', $vv1['style_detail_id'])
                                    ->where('start_no', $vv1['start_no'])
                                    ->first();
                if (!$cekbundledetail) {
                    unset($rows_new[$kk1]);
                }
            }

            foreach ($rows_new as $kk2 => $vv2) {
                //  bundle detail
                $bundledetail = DB::table('bundle_detail')
                                    ->where('bundle_header_id', $vv2['bundle_header_id'])
                                    ->where('style_detail_id', $vv2['style_detail_id'])
                                    ->where('start_no', $vv2['start_no'])
                                    ->first();

                $rr2['bundle_header_id'] =  $vv2['bundle_header_id'];
                $rr2['style_detail_id'] = $vv2['style_detail_id'];
                $rr2['komponen_name'] = $vv2['komponen_name'];
                $rr2['start_no'] = $vv2['start_no'];
                $rr2['end_no'] = $bundledetail->end_no;
                $rr2['qty'] = $bundledetail->qty;
                $rr2['location'] = $vv2['location'];
                $rr2['sds_barcode'] = $vv2['sds_barcode'];
                $rr2['created_at'] = $vv2['created_at'];
                $rr2['updated_at'] = $vv2['updated_at'];
                $rr2['job'] = $vv2['job'];
                $rr2['season'] = $vv2['season'];
                $rr2['style'] = $vv2['style'];
                $rr2['set_type'] = $vv2['set_type'];
                $rr2['poreference'] = $vv2['poreference'];
                $rr2['article'] = $vv2['article'];
                $rr2['cut_num'] = $vv2['cut_num'];
                $rr2['size'] = $vv2['size'];
                $rr2['factory_id'] = $vv2['factory_id'];
                $rr2['cutting_date'] = $vv2['cutting_date'];
                $rr2['color_name'] = $vv2['color_name'];
                $rr2['cutter'] = $vv2['cutter'];
                $rr2['note'] = $vv2['note'];
                $rr2['admin'] = $vv2['admin'];
                $rr2['qc_operator'] = $vv2['qc_operator'];
                $rr2['bundle_operator'] = $vv2['bundle_operator'];
                $rr2['box_lim'] = $vv2['box_lim'];
                $rr2['max_bundle'] = $vv2['max_bundle'];
                $rr2['created_at'] = $vv2['created_at'];
                $rr2['updated_at'] = $vv2['updated_at'];
                $rr2['barcode_marker'] = $vv2['barcode_marker'];
                $rr2['job'] = $vv2['job'];
                $rr2['admin_name'] = $vv2['admin_name'];
                $rr2['qc_operator_name'] = $vv2['qc_operator_name'];
                $rr2['bundle_operator_name'] = $vv2['bundle_operator_name'];
                $rr2['statistical_date'] = $vv2['statistical_date'];
                $rr2['destination'] = $vv2['destination'];
                $rr2['part'] = $vv2['part'];
                $rr2['komponen'] = $vv2['komponen'];
                $rr2['total'] = $vv2['total'];
                $rr2['process'] = $vv2['process'];
                $rr2['process_name'] = $vv2['process_name'];
                $rr2['type_id'] = $vv2['type_id'];
                $rr2['type_name'] = $vv2['type_name'];
                $rr2['is_recycle'] = $bundledetail->is_recycle;

                $rows_new2[] = $rr2;

            }

            // end test //

            $list_komponen_arr = array();
            $list_sticker = array();
            $newkey = 0;
            $list_komponen_new = array();
            $sum_qty = 0;
            foreach ($rows_new2 as $key => $value) {
                $style = $value['type_name'] != 'Non' ? $value['style'].$value['type_name'] : $value['style'];

                $list_komponen_arr[$style.$value['poreference'].$value['size'].$value['start_no'].$value['end_no'].$value['cut_num']][$newkey] = $value;
                $list_sticker[$style.$value['poreference'].$value['size'].$value['start_no'].$value['end_no'].$value['cut_num'].$value['is_recycle']] = $value;
                // $list_sticker[$style.$value['poreference'].$value['size'].$value['start_no'].$value['end_no'].$value['cut_num']] = $value;

                $newkey++;
            }

            $chunk = array_chunk($list_komponen_arr,2);
            $chunk_new = array();
            foreach ($chunk as $key2 => $val2) {
                foreach ($val2 as $key3 => $val3) {
                        $chunk_new[] = $val3;
                }
            }

            $list_sticker_new = array();
            $keyy = 0;
            foreach ($list_sticker as $vals) {
                $list_sticker_new[$keyy] = $vals;
                $keyy++;
            }

            // count komponen
            $count_komponen = DB::table('master_style_detail')
                                    ->where('style', $master_style[0]->style)
                                    ->where('season', $master_style[0]->season)
                                    ->whereNull('deleted_at')
                                    ->count();

            $factories = DB::table('master_factory')
                            ->where('id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
            
            $pdf = \PDF::loadView($load_view,
                                    [
                                        'data' => $data,
                                        // 'list_komponen' => $list_komponen,
                                        //KOMEN 3 ELEMEN DI BAWAH INI UNTUK DISABLE PRINT CHECKLIST SEMENTARA
                                        //DAN HAPUS CODE SEMENTARA CODE DI index2.blade UNTUK BAGIAN PRINT CHECKLIST
                                        'list_komponen_arr' => $list_komponen_arr,
                                        'list_sticker_new' => $list_sticker_new,
                                        'chunk_new' => $chunk_new,
                                        'showbarcode' => true,
                                        'data_plan' => $data_plan,
                                        'count_komponen' => $count_komponen,
                                        'factories' => $factories
                                    ])
                        ->setPaper($papersize, $orientation);

            return $pdf->stream($filename);
    }

    // update checklist komponen break
    public function updateKomponenBreak(Request $request)
    {
        if ($request->action == 'komponen_break') {

            $checklist_komponen_break = $request->checklist_komponen_break;

            $request->validate([
                'checklist_komponen_break' => 'required|numeric',
            ]);

            try {
                DB::beginTransaction();
                    // update master factory
                    DB::table('master_factory')
                            ->where('id', Auth::user()->factory_id)
                            ->update([
                                'checklist_komponen_break' => $checklist_komponen_break,
                                'updated_at' => Carbon::now()
                            ]);

                DB::commit();

            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($checklist_komponen_break, 200);

        }elseif ($request->action == 'komponen_bundle') {

            $komponen_on_bundle = $request->komponen_on_bundle;

            $request->validate([
                'komponen_on_bundle' => 'required|numeric',
            ]);

            try {
                DB::beginTransaction();
                    // update master factory
                    DB::table('master_factory')
                            ->where('id', Auth::user()->factory_id)
                            ->update([
                                'komponen_on_bundle' => $komponen_on_bundle,
                                'updated_at' => Carbon::now()
                            ]);

                DB::commit();

            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($komponen_on_bundle, 200);
        }

    }

    public function getPoSize(Request $request)
    {
        $style = $request->style;
        $barcode_id = $request->barcode_id;
        $data = DB::table('v_ratio_cutting_detail')
                    ->where('barcode_id', $barcode_id)
                    ->orderBy('size','asc')
                    ->get();

        $list_po = DetailPlan::where('id_plan', $data[0]->id_plan)
                            ->whereNull('deleted_at')
                            ->get();

        if ($data == null) {
            return response()->json('Data tidak ditemukan..!', 422);
        }
        return view('cutting.barcode.list_po_size',compact('data','list_po'));

    }

    public function updateQtyRatio(Request $request)
    {
        $id = $request->id;
        $qty = $request->qty;

        if (empty($id)) {
            $qty = null;
        }else {
            $qty = $qty;
        }

        //update
        try {
            DB::table('ratio_detail')->where('id', $id)
                                ->update([
                                    'qty' => $qty,
                                    'updated_at' => Carbon::now(),
                                ]);
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text"
                    data-id="'.$id.'"
                    class="form-control qty_unfilled text-right"
                    onkeypress="return isNumberKey(event);"
                    onfocus="return $(this).select();"
                    value="'.$qty.'">';

        return response()->json($html, 200);

    }

    public function updateCutInfoRatio(Request $request)
    {
        $id = $request->id;
        $cut_info = $request->cut_info;

        if (empty($id)) {
            $cut_info = null;
        }else {
            $cut_info = $cut_info;
        }

        $get_data_ratio = DB::table('ratio_detail')
                            ->where('id', $id)
                            ->first();

        //update
        try {
            DB::beginTransaction();

            DB::table('ratio_detail')->where('id', $id)
                                ->update([
                                    'cut_info' => $cut_info,
                                    'updated_at' => Carbon::now(),
                                ]);

            //update di bundle header
            DB::table('bundle_header')
                ->where('barcode_marker', $get_data_ratio->barcode_id)
                ->where('poreference', $get_data_ratio->po_buyer)
                ->where('size', $get_data_ratio->size)
                ->update([
                    'cut_info' => $cut_info,
                    'updated_at' => Carbon::now(),
                ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text"
                    data-id="'.$id.'"
                    class="form-control cutinfo_unfilled"
                    onfocus="return $(this).select();"
                    value="'.$cut_info.'">';

        return response()->json($html, 200);

    }
    public function updateSizeRatio(Request $request)
    {
        $id = $request->id;
        $size_edit = $request->size_edit;

        if (empty($id)) {
            $size_edit = null;
        }else {
            $size_edit = $size_edit;
        }

        $get_data_ratio = DB::table('ratio_detail')
                            ->where('id', $id)
                            ->first();

        //update
        try {
            DB::beginTransaction();

                DB::table('ratio_detail')->where('id', $id)
                                ->update([
                                    'size_edit' => $size_edit,
                                    'updated_at' => Carbon::now(),
                                ]);

                //update di bundle header
                DB::table('bundle_header')
                        ->where('barcode_marker', $get_data_ratio->barcode_id)
                        ->where('poreference', $get_data_ratio->po_buyer)
                        ->where('size', $get_data_ratio->size)
                        ->update([
                            'size_edit' => $size_edit,
                            'updated_at' => Carbon::now(),
                        ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text"
                    data-id="'.$id.'"
                    class="form-control size_unfilled"
                    onfocus="return $(this).select();"
                    value="'.$size_edit.'">';

        return response()->json($html, 200);

    }

    public function updatePoRatio(Request $request)
    {
        $id = $request->id;
        $po_buyer = $request->po_buyer;

        if (empty($id)) {
            $po_buyer = null;
        }else {
            $po_buyer = $po_buyer;
        }

        $get_data_ratio = DB::table('ratio_detail')
                            ->where('id', $id)
                            ->first();

        //update
        try {
            DB::beginTransaction();

            DB::table('ratio_detail')->where('id', $id)
                                ->update([
                                    'po_buyer' => $po_buyer,
                                    'updated_at' => Carbon::now(),
                                ]);

            //update di bundle header
            DB::table('bundle_header')
                ->where('barcode_marker', $get_data_ratio->barcode_id)
                ->where('poreference', $get_data_ratio->po_buyer)
                ->where('size', $get_data_ratio->size)
                ->update([
                    'poreference' => $po_buyer,
                    'updated_at' => Carbon::now(),
                ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler::db($message);
        }

        $html = '<input type="text"
                    data-id="'.$id.'"
                    class="form-control po_unfilled"
                    onfocus="return $(this).select();"
                    value="'.$po_buyer.'">';

        return response()->json($html, 200);

    }

    public function sinkronSDS(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $sds_connection = DB::connection('sds_live');
            $get_data_cdms = DB::table('jaz_sinkron_sds')->where('barcode_marker', $barcode_id)->get();
            if(count($get_data_cdms) > 0) {
                // CHECK IF GENERATE IS DOUBLE DATA
                $data_cdms = DB::table('bundle_header as bh')
                ->Join('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                ->where('bh.barcode_marker',$barcode_id)
                ->count('bd.barcode_id');

                $param_sds = DB::select(DB::raw("SELECT DISTINCT bh.poreference,bh.cut_num,
                CASE WHEN vms.type_name = 'Non' THEN '0' ELSE vms.type_name END AS set_type,bh.article,bh.size
                FROM bundle_header bh
                JOIN bundle_detail bd ON bd.bundle_header_id = bh.id
                JOIN v_master_style vms ON vms.id = bd.style_detail_id
                where bh.barcode_marker='$barcode_id'"));
                $po     = [];
                $cut    = [];
                $set    = [];
                $art_   = [];
                $size_  = [];
                foreach($param_sds as $ps){
                    $po[]       = $ps->poreference;
                    $cut[]      = $ps->cut_num;
                    $set[]      = $ps->set_type;
                    $art_[]     = $ps->article;
                    $size_[]    = $ps->size;
                }
                $poreference    = "'".implode("','",$po)."'";
                $cut_num        = "'".implode("','",$cut)."'";
                $set_type       = "'".implode("','",$set)."'";
                $article        = "'".implode("','",$art_)."'";
                $size           = "'".implode("','",$size_)."'";
                $data_sds       = DB::connection('sds_live')->select(DB::raw("SELECT count(bi.barcode_id) as data_sds
                FROM bundle_info_new bi
                JOIN art_desc_new ad ON ad.season = bi.season AND ad.style = bi.style AND ad.part_num = bi.part_num AND ad.part_name = bi.part_name
                WHERE bi.poreference IN(
                $poreference
                )and bi.cut_num IN($cut_num) AND ad.set_type IN($set_type) AND bi.size IN($size) AND bi.article IN($article)"));
                //PENGECEKAN TOTAL DATA
                if($data_cdms != $data_sds[0]->data_sds){
                    return response()->json('TERDAPAT PERBEDAAN JUMLAH DATA CDMS DENGAN SDS HARAP GENERATE ULANG TICKET CDMS!',422);
                }
                try {
                    DB::beginTransaction();

                    foreach($get_data_cdms as $key => $cdms) {
                        $get_data_sds = $sds_connection->table('jaz_sinkron_cdms')->where('style', $cdms->style)
                            ->where('set_type', $cdms->set_type)
                            ->where('season', $cdms->season)
                            ->where('article', $cdms->article)
                            ->where('poreference', $cdms->poreference)
                            ->where('cut_num', $cdms->cut_num)
                            ->where('size', $cdms->size)
                            ->where('part_num', $cdms->part_num)
                            ->where('part_name', $cdms->part_name)
                            ->where('start_num', $cdms->start_num)
                            ->where('end_num', $cdms->end_num)
                            ->where('qty', $cdms->qty)
                            ->where('processing_fact', $cdms->processing_fact)->first();
                        if($get_data_sds != null){
                            $check = DB::table('distribusi_movements')->where('barcode_id',$get_data_sds->barcode_id)->whereNull('deleted_at')->first();
                            if($check != null){
                                return response()->json('BARCODE '.$get_data_sds->barcode_id.' SUDAH DALAM TRANSAKSI SCAN!',422);
                            }else{
                                // if($get_data_sds != null) {
                                    $update_ = DB::table('bundle_detail')->where('barcode_id', $cdms->barcode_id)->update(['sds_barcode' => $get_data_sds->barcode_id]);
                                // } else {
                                //     return response()->json('Style: '.$cdms->style.' || Set: '.$cdms->set_type.' || PO: '.$cdms->poreference.' || No Cut: '.$cdms->cut_num.' || size: '.$cdms->size.' || part: '.$cdms->part_num.' || komponen: '.$cdms->part_name.' || sticker: '.$cdms->start_num.'-'.$cdms->end_num.' || qty: '.$cdms->qty.' ||Terdapat data yang tidak sama!',422);
                                // }
                            }
                        }else{
                            return response()->json('Style: '.$cdms->style.' || Set: '.$cdms->set_type.' || PO: '.$cdms->poreference.' || No Cut: '.$cdms->cut_num.' || size: '.$cdms->size.' || part: '.$cdms->part_num.' || komponen: '.$cdms->part_name.' || sticker: '.$cdms->start_num.'-'.$cdms->end_num.' || qty: '.$cdms->qty.' ||Terdapat data yang tidak sama!',422);
                        }
                    }

                    DB::commit();

                } catch (Exception $ex) {
                    DB::rollback();
                    $message = $ex->getMessage();
                    ErrorHandler::db($message);
                }
            } else {
                return response()->json('MARKER BELUM DI GENERATE!',422);
            }
            return response()->json(200);
        }
    }


    public static function generateBarcodeNumber($string) {
        $string = str_pad($string, 5, "0", STR_PAD_LEFT);
        // $number = mt_rand(1000000000, 9999999999); // better than rand()
        $number = Carbon::now()->format('ymdhis').$string; // better than rand()

        // call the same function if the barcode exists already
        // if ($this->barcodeNumberExists($number)) {
        //     return $this->generateBarcodeNumber($string);
        // }

        // otherwise, it's valid and can be used
        return $number;
    }

    static function random_code_scan($po, $size, $from, $to, $string){
        $string = str_pad($string, 5, "0", STR_PAD_LEFT);
        $from = str_pad($from, 5, "0", STR_PAD_LEFT);
        $to = str_pad($to, 5, "0", STR_PAD_LEFT);
        return $po.$from.$to.$string;
    }

    static function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return DB::table('bundle_detail')
                    ->where('barcode_id', $number)
                    ->exists();
    }

    public function migrateSDS(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $is_body = DB::table('cutting_marker')->where('barcode_id',$barcode_id)->whereNull('deleted_at')->first();
        $check_generated = DB::table('bundle_header')->where('barcode_marker',$barcode_id)->first();
        if($check_generated != null){
            if($is_body->is_body == true){
                $bundle_header = DB::SELECT(DB::RAW("SELECT DISTINCT bh.id,bh.barcode_marker,bh.poreference,bh.season,bh.style,bh.article,cp.cutting_date,bh.color_name,bh.qc_operator_name,bh.bundle_operator_name,bh.cutter,bh.note,bh.box_lim,
                CASE WHEN vms.type_name = 'Non' THEN '0' ELSE vms.type_name END AS type_name,bh.size,bh.cut_num,bh.factory_id
                FROM bundle_header bh
                JOIN bundle_detail bd on bd.bundle_header_id = bh.id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                JOIN cutting_marker cm on cm.barcode_id = bh.barcode_marker
                JOIN cutting_plans2 cp on cp.id = cm.id_plan
                where bh.barcode_marker='$barcode_id'"));
                foreach($bundle_header as $bh){
                    $check_ppcm_sds = DB::connection('sds_live')->table('ppcm_new')->where('poreference',$bh->poreference)->where('style',$bh->style)->where('size',$bh->size)->where('season',$bh->season)->where('article',$bh->article)->first();
                    if($check_ppcm_sds == null){
                        return response()->json('HARAP REGISTRASI PO '.$bh->poreference.' DI SDS',422);
                    }else{
                        $is_created_sds = DB::connection('sds_live')
                        ->table('bundle_info_new as bi')
                        ->Join('art_desc_new as ad',function($join){
                            $join->on('ad.part_num','=','bi.part_num')
                            ->on('ad.part_name','=','bi.part_name')
                            ->on('ad.style','=','bi.style')
                            ->on('ad.season','=','bi.season');
                        })
                        ->where('bi.poreference',$bh->poreference)
                        ->where('bi.cut_num',$bh->cut_num)
                        ->where('bi.size',$bh->size)
                        ->where('ad.set_type',$bh->type_name)
                        ->first();
                        if($is_created_sds != null){
                            return response()->json('DETAIL TICKET SUDAH DI BUAT DI SDS! '.'PO: '.$bh->poreference.' || '.'STYLE: '.$bh->style.'-'.$bh->type_name.' || '.' CUT: '.$bh->cut_num.' || '.' SIZE: '.$bh->size,422);
                        }else{
                            $header_sds = DB::connection('sds_live')->table('cut_data_new')->where('poreference',$bh->poreference)->where('season',$bh->season)->where('style',$bh->style)->where('set_type',$bh->type_name)->where('article',$bh->article)->where('cut_num',$bh->cut_num)->where('size',$bh->size)->where('processing_fact',$bh->factory_id)->first();
                            if($header_sds == null){
                                $pcd = Carbon::createFromFormat('Y-m-d',$bh->cutting_date)->format('Y-m-d 00:00:00');
                                $set_type = $bh->type_name == 'Non' ? '0' : $bh->type_name;
                                $user_nik = Auth::user()->nik;
                                $cut_code = Carbon::now()->format('YmdHis').(sprintf('%09d',(string)$user_nik)).(sprintf('%04d',rand(1,9999)));
                                try{
                                    DB::beginTransaction();
                                        DB::connection('sds_live')->table('cut_data_new')->insertGetId([
                                            'season'            => $bh->season,
                                            'style'             => $bh->style,
                                            'set_type'          => $set_type,
                                            'poreference'       => $bh->poreference,
                                            'article'           => $bh->article,
                                            'cut_num'           => $bh->cut_num,
                                            'size'              => $bh->size,
                                            'act_spread'        => Carbon::now(),
                                            'adm'               => Auth::user()->nik,
                                            'processing_fact'   => $bh->factory_id,
                                            'ed_pcd'            => $pcd,
                                            'coll'              => $bh->color_name,
                                            'qc_op'             => $bh->qc_operator_name,
                                            'bd_op'             => $bh->bundle_operator_name,
                                            'cutter'            => $bh->cutter,
                                            'add_note'          => $bh->note,
                                            'box_lim'           => $bh->box_lim,
                                            'cut_code'          => $cut_code
                                        ]);
                                    DB::commit();
                                }catch(Exception $ex){
                                    DB::rollback();
                                    $message = $ex->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }
                        }
                    }
                }
                //INSERT INTO BUNDLE INFO NEW
                $bundle_detail = DB::select(DB::raw("SELECT bh.season,bh.style,
                CASE WHEN vms.type_name = 'Non' THEN '0' ELSE vms.type_name END AS type_name,
                bh.poreference,bh.article,bh.cut_num::INTEGER,bh.size,vms.part,bd.komponen_name,vms.process_name,bd.start_no::INTEGER,bd.end_no::INTEGER,bd.qty::INTEGER,bh.factory_id,bh.color_name
                FROM bundle_detail bd
                JOIN bundle_header bh on bh.id = bd.bundle_header_id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                WHERE bh.barcode_marker='$barcode_id'
                ORDER BY poreference,size"));

                foreach($bundle_detail as $bd){
                    $cut_id = DB::connection('sds_live')->table('cut_data_new')->where('poreference',$bd->poreference)->where('season',$bd->season)->where('style',$bd->style)->where('set_type',$bd->type_name)->where('article',$bd->article)->where('cut_num',$bd->cut_num)->where('size',$bd->size)->where('processing_fact',$bd->factory_id)->first();
                    $process_name = count(explode(';',$bd->process_name)) > 1 ? str_replace(';','+',$bd->process_name) : $bd->process_name;
                    try{
                        DB::beginTransaction();
                            DB::connection('sds_live')->table('bundle_info_new')->insert([
                                'barcode_id'            => 1,
                                'season'                => $bd->season,
                                'style'                 => $bd->style,
                                'poreference'           => $bd->poreference,
                                'article'               => $bd->article,
                                'cut_num'               => $bd->cut_num,
                                'size'                  => $bd->size,
                                'part_num'              => $bd->part,
                                'part_name'             => $bd->komponen_name,
                                'note'                  => $process_name,
                                'start_num'             => $bd->start_no,
                                'end_num'               => $bd->end_no,
                                'qty'                   => $bd->qty,
                                'location'              => 'GEN',
                                'factory'               => 'GEN',
                                'processing_fact'       => $bd->factory_id,
                                'create_date'           => Carbon::now(),
                                'bc_print'              => 1,
                                'cut_id'                => $cut_id->id,
                                'coll'                  => $bd->color_name
                            ]);
                        DB::commit();
                    }catch(Exception $ex){
                        DB::rollback();
                        $message = $ex->getMessage();
                        ErrorHandler::db($message);
                    }
                }
                return response()->json(200);
            }else{
                return response()->json('BARCODE '.$barcode_id.' BUKAN BODY!',422);
            }
        }else{
            return response()->json('BARCODE '.$barcode_id.' BELUM GENERATE CDMS!',422);
        }
    }
}
