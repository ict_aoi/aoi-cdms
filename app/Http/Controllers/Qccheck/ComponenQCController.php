<?php

namespace App\Http\Controllers\Qccheck;

use DB;
use StdClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponenQCController extends Controller
{
    public function index()
    {
        $process = DB::table('master_process')
                    ->whereNull('deleted_at')
                    ->get();

        $komponen = DB::table('master_komponen')
                    ->whereNull('deleted_at')
                    ->get();

        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        $styles = DB::connection('erp_live') 
                    ->table('rma_style_fg')
                    ->select(DB::raw('upc as style'))
                    ->groupBy('upc')
                    ->get();

        $seasons = DB::table('master_seasons')
                    ->get();

        return view('qccheck.qc_component.index', compact('process', 'komponen', 'types', 'styles', 'seasons'));
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $style  = $request->style;
            $season = $request->season;

            if($style != null && $season != null) {
                $data = DB::table('master_style_detail')->where('style', $style)->where('season', $season)->whereNull('deleted_at');
    
                return datatables()->of($data)
                ->addColumn('komponen_name', function($data) {
                    return DB::table('master_komponen')->where('id', $data->komponen)->first()->komponen_name;
                })
                ->addColumn('type_name', function($data) {
                    return DB::table('types')->where('id', $data->type_id)->first()->type_name;
                })
                ->editColumn('is_check_qc', function($data) {
                    if($data->is_check_qc == 't') {
                        return '<span class="label label-success label-icon"><i class="icon-check"></i></span>';
                    } else {
                        return '<span class="label label-warning label-icon"><i class="icon-cross"></i></span>';
                    }
                })
                ->addColumn('action', function($data) {
                    return view('qccheck.qc_component._action', [
                        'model' => $data,
                        'edit'  => route('ComponenQC.editModal', ['style' => $data->style, 'season' => $data->season, 'komponen' => $data->komponen])
                    ]);
                })
                ->rawColumns(['is_check_qc','action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function editModal($style, $season, $komponen)
    {
        $data = DB::table('master_style_detail')->where('style', $style)->where('season', $season)->where('komponen', $komponen)->where('deleted_at', null)->first();
        $season = DB::table('master_seasons')->where('id', $data->season)->first();
        $komponen = DB::table('master_komponen')->where('id', $data->komponen)->first();
        $obj = new StdClass();
        $obj->style = $data->style;
        $obj->season = $season->season_name;
        $obj->season_id = $data->season;
        $obj->komponen_id = $data->komponen;
        $obj->komponen = $komponen->komponen_name;
        $obj->is_check_qc = $data->is_check_qc;
        $obj->qc_name_komponen = $data->qc_name_komponen;
		
		return response()->json($obj,200);
    }

    public function updateData(Request $request)
    {
        if(request()->ajax()) 
        {
            $name_alias = $request->name_alias;
            $is_check_qc = $request->is_check_qc;
            $season = $request->detail_season;
            $style = $request->detail_style;
            $komponen = $request->detail_komponen;

            if($is_check_qc == 'yes') {
                $is_check_qc = true;
            } else {
                $is_check_qc = false;
            }

            if($name_alias == '') {
                $name_alias = null;
            } else {
                $name_alias = $name_alias;
            }

            $data = [
                'is_check_qc' => $is_check_qc,
                'qc_name_komponen' => $name_alias,
            ];

            $data_update = DB::table('master_style_detail')->where('style', $style)->where('season', $season)->where('komponen', $komponen)->update($data);

            $response = [
                'status' => 200,
            ];

            return response()->json($response,200);
        }
    }
}
