@extends('layouts.app',['active' => 'master_spreading'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Spreading </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3">
                <label><b>Type Spreading</b></label>
                <select id="type" class="form-control">
                    <option value="Manual">Manual</option>
                    <option value="Automatic">Automatic</option>
                </select>
            </div>
            <div class="col-lg-3">
                <label><b>Target</b></label>
                <input type="text" name="target" id="target" class="form-control" placeholder="Target" required="">
            </div>
            <div class="col-lg-1">
                <label style="margin-top: 40px;"><b>yard / hour</b></label>
            </div>
            <div class="col-lg-1">
                <button type="submit" id="btn_save" style="margin-top: 30px;" class="btn btn-primary"><span class="icon-checkmark4"></span> Save</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Target</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
        var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{route('masterparam.getMasterSpreading')}}"
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'type', name: 'type'},
                {data: 'target', name: 'target'},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

        $('#btn_save').on('click',function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url : "{{ route('masterparam.inSpreading') }}",
                data: {type:$('#type').val(),target:$('#target').val()},
                beforeSend:function(){
                    loading_process();
                },          
                success:function(response){
                    
                    var data_response = response.data;
                       
                    if (data_response.status == 200) {
                        myalert('success', data_response.output);
                    }else{
                        myalert('error', data_response.output);
                    }
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                    $('#target').val('');
                },
                eror:function(response){
                    myalert('error',response);
                   
                }

            });
        });
});

function deletepar(e){
    var _token = $("input[name='_token]").val();
    var id = e.getAttribute('data-id');
    var tableL = $('#table-list').DataTable();
    console.log(id);
    bootbox.confirm('Are you sure inactive ?',
        function(result){
            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                        url:"{{ route('masterparam.inactiveSpreading') }}",
                        type: "POST",
                        data:{
                            "id": id,
                        },
                        beforeSend:function(){
                            loading_process();
                        },  
                        success: function(response){    
                           var data_response = response.data;
                       
                            if (data_response.status == 200) {
                                myalert('success', data_response.output);
                            }else{
                                myalert('error', data_response.output);
                            }
                            $('#table-list').unblock();
                            tableL.clear();
                            tableL.draw();
                        },
                        error: function(response) {
                            myalert('error',response);
                        }
                    });
            }
        }
    );
}
</script>
@endsection



