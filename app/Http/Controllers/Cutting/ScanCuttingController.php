<?php

namespace App\Http\Controllers\Cutting;

use DB;
use App\Models\User;
use App\Models\CuttingMarker;
use App\Models\ScanCutting;
use App\Models\TempScanCutting;
use App\Models\ScanCuttingOperator;
use App\Models\Spreading\FabricUsed;
use App\Models\MasterDowntimeCategory;
use App\Models\CuttingMovement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScanCuttingController extends Controller
{
    public function index()
    {
        $downtime_categorys = MasterDowntimeCategory::where('deleted_at', null)->get();
        return view('cutting.scan_cutting.index', compact('downtime_categorys'));
    }

    public function dataCuttingTable()
    {
        if(request()->ajax())
        {
            $data = DB::table('master_table')->where('deleted_at', null)->orderBy('id_table', 'asc');

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return '<button class="btn btn-primary btn-xs" data-id="'.$data->id_table.'" data-barcode="'.$data->id_table.'" data-name="'.$data->table_name.'" id="select_cutting_table">Select</button>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function checkTable(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->table_check;
            $get_id_open = null;
            $get_id_progress = null;
            if($barcode_id != null) {
                $data = DB::table('master_table')->where('id_table', $barcode_id)->get()->first();
                if($data != null) {
                    // check state open
                    $check_state_open = ScanCutting::where('barcode_table', $data->id_table)->where('deleted_at', null)->where('state', 'open')->where('factory_id', \Auth::user()->factory_id)->get();

                    if(count($check_state_open) > 0) {
                        foreach($check_state_open as $get_operators) {
                            $operator_array = $get_operators->operator->pluck('id')->toArray();
                            if(in_array(\Auth::user()->id, $operator_array)) {
                                $get_id_open = $get_operators->id;
                                break;
                            }
                        }

                        //jika get id tidak null
                        if($get_id_open != null) {
                            $scan_cutting = ScanCutting::where('id', $get_id_open)->first();
                            return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                        } else {
                            // jika id null akan cari progress
                            $check_state_progress= ScanCutting::where('barcode_table', $data->id_table)->where('deleted_at', null)->where('state', 'progress')->where('factory_id', \Auth::user()->factory_id)->get();

                            if(count($check_state_progress) > 0) {
                                foreach($check_state_progress as $get_operators) {
                                    $operator_array = $get_operators->operator->pluck('id')->toArray();
                                    if(in_array(\Auth::user()->id, $operator_array)) {
                                        $get_id_progress = $get_operators->id;
                                        break;
                                    }
                                }

                                //jika get id tidak null
                                if($get_id_progress != null) {
                                    $scan_cutting = ScanCutting::where('id', $get_id_progress)->first();
                                    $data_marker = CuttingMarker::where('barcode_id', $scan_cutting->barcode_marker)->where('deleted_at', null)->first();
                                    // $response = [
                                    //     'scan_id' => $scan_cutting->id,
                                    //     'barcode_id' => $data_marker->barcode_id,
                                    //     'style' => $data_marker->cutting_plan->style,
                                    //     'articleno' => $data_marker->cutting_plan->articleno,
                                    //     'size_category' => $data_marker->size_category == 'A' ? 'Asian' : $data_marker->size_category == 'J' ? 'Japan' : 'Inter',
                                    //     'cut' => $data_marker->cut,
                                    //     'part_no' => $data_marker->part_no,
                                    //     'marker_length' => $data_marker->marker_length,
                                    //     'perimeter' => $data_marker->perimeter,
                                    //     'start_time' => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                    //     'state' => $scan_cutting->state,
                                    // ];
                                    $responseArray = array();
                                    $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                    foreach ($listMarker->get() as $key => $value) {

                                        $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                        $response = [
                                            'scan_id'       => $scan_cutting->id,
                                            'barcode_id'    => $marker_data->barcode_id,
                                            'style'         => $marker_data->cutting_plan->style,
                                            'articleno'     => $marker_data->cutting_plan->articleno,
                                            'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                            'cut'           => $marker_data->cut,
                                            'part_no'       => $marker_data->part_no,
                                            'marker_length' => $marker_data->marker_length,
                                            'perimeter'     => $marker_data->perimeter,
                                            'ratio_print'   => $marker_data->ratio_print,
                                            'layer'         => $marker_data->layer,
                                            'marker_pcs'    => $marker_data->qty_ratio,
                                            'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                            'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                            'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                            'state'         => $scan_cutting->state,
                                            'table_name'    => $scan_cutting->table_name,
                                        ];
                                        // var_dump($response);
                                        $responseArray[] = $response;
                                        $ratio       = $marker_data->ratio_print;
                                    }

                                    $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                                    return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $response, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                                } else {
                                    $scan_cutting = ScanCutting::FirstOrCreate([
                                        'barcode_table' => $data->id_table,
                                        'state' => 'open',
                                        'factory_id' => \Auth::user()->factory_id,
                                        'deleted_at' => null,
                                        'table_name' => $data->table_name,
                                    ]);
                                    ScanCuttingOperator::FirstOrCreate([
                                        'scan_cutting_id' => $scan_cutting->id,
                                        'user_id' => \Auth::user()->id,
                                        'name' => \Auth::user()->name,
                                        'nik' => \Auth::user()->nik,
                                        'deleted_at' => null,
                                    ]);

                                    $responseArray = array();
                                    $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                    foreach ($listMarker->get() as $key => $value) {

                                        $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                        $response = [
                                            'scan_id'       => $scan_cutting->id,
                                            'barcode_id'    => $marker_data->barcode_id,
                                            'style'         => $marker_data->cutting_plan->style,
                                            'articleno'     => $marker_data->cutting_plan->articleno,
                                            'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                            'cut'           => $marker_data->cut,
                                            'part_no'       => $marker_data->part_no,
                                            'marker_length' => $marker_data->marker_length,
                                            'perimeter'     => $marker_data->perimeter,
                                            'ratio_print'   => $marker_data->ratio_print,
                                            'layer'         => $marker_data->layer,
                                            'marker_pcs'    => $marker_data->qty_ratio,
                                            'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                            'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                            'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                            // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                            'state'         => $scan_cutting->state,
                                            'table_name'    => $scan_cutting->table_name,
                                        ];
                                        // var_dump($response);
                                        $responseArray[] = $response;
                                        $ratio       = $marker_data->ratio_print;
                                    }

                                    $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                                    return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                                }
                            } else {
                                // data tidak ditemukan -> create scan id baru
                                $scan_cutting = ScanCutting::FirstOrCreate([
                                    'barcode_table' => $data->id_table,
                                    'state' => 'open',
                                    'factory_id' => \Auth::user()->factory_id,
                                    'deleted_at' => null,
                                    'table_name' => $data->table_name,
                                ]);
                                ScanCuttingOperator::FirstOrCreate([
                                    'scan_cutting_id' => $scan_cutting->id,
                                    'user_id' => \Auth::user()->id,
                                    'name' => \Auth::user()->name,
                                    'nik' => \Auth::user()->nik,
                                    'deleted_at' => null,
                                ]);

                                $responseArray = array();
                                $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                foreach ($listMarker->get() as $key => $value) {

                                    $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                    $response = [
                                        'scan_id'       => $scan_cutting->id,
                                        'barcode_id'    => $marker_data->barcode_id,
                                        'style'         => $marker_data->cutting_plan->style,
                                        'articleno'     => $marker_data->cutting_plan->articleno,
                                        'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                        'cut'           => $marker_data->cut,
                                        'part_no'       => $marker_data->part_no,
                                        'marker_length' => $marker_data->marker_length,
                                        'perimeter'     => $marker_data->perimeter,
                                        'ratio_print'   => $marker_data->ratio_print,
                                        'layer'         => $marker_data->layer,
                                        'marker_pcs'    => $marker_data->qty_ratio,
                                        'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                        'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                        'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                        // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                        'state'         => $scan_cutting->state,
                                        'table_name'    => $scan_cutting->table_name,
                                    ];
                                    // var_dump($response);
                                    $responseArray[] = $response;

                                    $ratio       = $marker_data->ratio_print;
                                }

                                // $responseArray = json_encode($responseArray);
                                $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                                return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'mynik' => \Auth::user()->nik, 'marker'=> $responseArray, 'list_marker' => $listBarcode]);
                            }
                        }
                    } else {
                        // check state progress
                        $check_state_progress = ScanCutting::where('barcode_table', $data->id_table)->where('deleted_at', null)->where('state', 'progress')->where('factory_id', \Auth::user()->factory_id)->get();
                        if(count($check_state_progress) > 0) {

                            foreach($check_state_progress as $get_operators) {
                                $operator_array = $get_operators->operator->pluck('user_id')->toArray();
                                if(in_array(\Auth::user()->id, $operator_array)) {
                                    $get_id_progress = $get_operators->id;
                                    break;
                                }
                            }

                            if($get_id_progress != null) {
                                $scan_cutting = ScanCutting::where('id', $get_id_progress)->first();
                                $data_marker = CuttingMarker::where('barcode_id', $scan_cutting->barcode_marker)->where('deleted_at', null)->first();
                                // $response = [
                                //     'scan_id' => $scan_cutting->id,
                                //     'barcode_id' => $data_marker->barcode_id,
                                //     'style' => $data_marker->cutting_plan->style,
                                //     'articleno' => $data_marker->cutting_plan->articleno,
                                //     'size_category' => $data_marker->size_category == 'A' ? 'Asian' : $data_marker->size_category == 'J' ? 'Japan' : 'Inter',
                                //     'cut' => $data_marker->cut,
                                //     'part_no' => $data_marker->part_no,
                                //     'marker_length' => $data_marker->marker_length,
                                //     'perimeter' => $data_marker->perimeter,
                                //     'start_time' => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                //     'state' => $scan_cutting->state,
                                // ];
                                $responseArray = array();
                                $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                foreach ($listMarker->get() as $key => $value) {

                                    $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                    $response = [
                                        'scan_id'       => $scan_cutting->id,
                                        'barcode_id'    => $marker_data->barcode_id,
                                        'style'         => $marker_data->cutting_plan->style,
                                        'articleno'     => $marker_data->cutting_plan->articleno,
                                        'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                        'cut'           => $marker_data->cut,
                                        'part_no'       => $marker_data->part_no,
                                        'marker_length' => $marker_data->marker_length,
                                        'perimeter'     => $marker_data->perimeter,
                                        'ratio_print'   => $marker_data->ratio_print,
                                        'layer'         => $marker_data->layer,
                                        'marker_pcs'    => $marker_data->qty_ratio,
                                        'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                        'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                        'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                        // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                        'state'         => $scan_cutting->state,
                                        'table_name'    => $scan_cutting->table_name,
                                    ];
                                    // var_dump($response);
                                    $responseArray[] = $response;

                                    $ratio       = $marker_data->ratio_print;
                                }
                                $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                                return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                            } else {
                                // data tidak ditemukan -> create scan id baru
                                $scan_cutting = ScanCutting::FirstOrCreate([
                                    'barcode_table' => $data->id_table,
                                    'state' => 'open',
                                    'factory_id' => \Auth::user()->factory_id,
                                    'deleted_at' => null,
                                    'table_name' => $data->table_name,
                                ]);
                                ScanCuttingOperator::FirstOrCreate([
                                    'scan_cutting_id' => $scan_cutting->id,
                                    'user_id' => \Auth::user()->id,
                                    'name' => \Auth::user()->name,
                                    'nik' => \Auth::user()->nik,
                                    'deleted_at' => null,
                                ]);

                                $responseArray = array();
                                $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                foreach ($listMarker->get() as $key => $value) {

                                    $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                    $response = [
                                        'scan_id'       => $scan_cutting->id,
                                        'barcode_id'    => $marker_data->barcode_id,
                                        'style'         => $marker_data->cutting_plan->style,
                                        'articleno'     => $marker_data->cutting_plan->articleno,
                                        'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                        'cut'           => $marker_data->cut,
                                        'part_no'       => $marker_data->part_no,
                                        'marker_length' => $marker_data->marker_length,
                                        'perimeter'     => $marker_data->perimeter,
                                        'ratio_print'   => $marker_data->ratio_print,
                                        'layer'         => $marker_data->layer,
                                        'marker_pcs'    => $marker_data->qty_ratio,
                                        'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                        'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                        'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                        // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                        'state'         => $scan_cutting->state,
                                        'table_name'    => $scan_cutting->table_name,
                                    ];
                                    // var_dump($response);
                                    $responseArray[] = $response;

                                    $ratio       = $marker_data->ratio_print;
                                }
                                $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());

                                return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                            }
                        } else {
                            // data tidak ditemukan -> create scan id baru
                            $scan_cutting = ScanCutting::FirstOrCreate([
                                'barcode_table' => $data->id_table,
                                'state' => 'open',
                                'factory_id' => \Auth::user()->factory_id,
                                'deleted_at' => null,
                                'table_name' => $data->table_name,
                            ]);
                            ScanCuttingOperator::FirstOrCreate([
                                'scan_cutting_id' => $scan_cutting->id,
                                'user_id' => \Auth::user()->id,
                                'name' => \Auth::user()->name,
                                'nik' => \Auth::user()->nik,
                                'deleted_at' => null,
                            ]);
                            $responseArray = array();
                            $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                            foreach ($listMarker->get() as $key => $value) {

                                $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                $response = [
                                    'scan_id'       => $scan_cutting->id,
                                    'barcode_id'    => $marker_data->barcode_id,
                                    'style'         => $marker_data->cutting_plan->style,
                                    'articleno'     => $marker_data->cutting_plan->articleno,
                                    'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                    'cut'           => $marker_data->cut,
                                    'part_no'       => $marker_data->part_no,
                                    'marker_length' => $marker_data->marker_length,
                                    'perimeter'     => $marker_data->perimeter,
                                    'ratio_print'   => $marker_data->ratio_print,
                                    'layer'         => $marker_data->layer,
                                    'marker_pcs'    => $marker_data->qty_ratio,
                                    'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                    'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                    'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                    // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                    'state'         => $scan_cutting->state,
                                    'table_name'    => $scan_cutting->table_name,
                                ];
                                // var_dump($response);
                                $responseArray[] = $response;

                                $ratio       = $marker_data->ratio_print;
                            }
                            $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());

                            return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                        }
                    }
                } else {
                    return response()->json(['status' => 442, 'message' => 'Barcode meja tidak ditemukan!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }

    public function checkOperator(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->operator_check;
            $id_scan = $request->id_scan;
            if($barcode_id != null) {
                $data = User::where('nik', $barcode_id)->where('last_login_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->first();
                if($data != null) {
                    $scan_cutting = ScanCutting::where('id', $id_scan)->where('deleted_at', null)->where('state', 'open')->first();
                    if($scan_cutting != null) {
                        $operator_check = ScanCuttingOperator::where('scan_cutting_id', $id_scan)->where('user_id', $data->id)->where('deleted_at', null)->first();
                        if($operator_check != null) {
                            return response()->json(['status' => 442, 'operator' => $scan_cutting->operator, 'message' => 'Operator Sudah ditambahkan!']);
                        } else {
                            $check = ScanCuttingOperator::FirstOrCreate([
                                'scan_cutting_id' => $scan_cutting->id,
                                'user_id' => $data->id,
                                'name' => $data->name,
                                'nik' => $data->nik,
                                'deleted_at' => null,
                            ]);

                            return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                        }
                    } else {
                        return response()->json(['status' => 442, 'message' => 'Operator tidak dapat ditambahkan karena cutting sedeng proses!']);
                    }
                } else {
                    return response()->json(['status' => 442, 'message' => 'Barcode operator tidak ditemukan atau sudah expire!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }

    public function checkMarker(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->marker_check;
            $id_scan = $request->id_scan;
            $state = $request->state;
            $temp = TempScanCutting::where('barcode_marker', $barcode_id)->count(['barcode_marker']);
                //dd($temp);
                if($temp > 0){
                return response()->json(['status' => 442, 'message' => 'MARKER SEDANG DI GUNAKAN DI MEJA LAIN..!']);
                }else{
            if($barcode_id != null) {
                if($state == 'open') {
                    $data = FabricUsed::where('barcode_marker', $barcode_id)->where('deleted_at', null)->where('factory_id', \Auth::user()->factory_id)->first();
                    if($data != null) {
                        $check_progress = ScanCutting::where('barcode_marker', $barcode_id)->where('deleted_at', null)->whereIn('state', ['progress','done'])->first();
                        if($check_progress != null) {
                            return response()->json(['status' => 442, 'message' => 'Marker sudah digunakan!']);
                        } else {
                            $scan_cutting = ScanCutting::where('id', $id_scan)->where('deleted_at', null)->where('state', 'open')->first();
                            $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();
                            if($scan_cutting != null && $data_marker != null) {
                                // $scan_cutting->state = 'progress';
                                // $scan_cutting->start_time = Carbon::now();
                                // $scan_cutting->barcode_marker = $barcode_id;
                                // $scan_cutting->save();
                                $now       = Carbon::now();
                                $checkTemp = TempScanCutting::where('barcode_marker',$barcode_id)->first();
                                if($checkTemp){
                                    return response()->json(['status' => 442, 'message' => 'Barcode marker sedang dipakai di meja lain!']);
                                }
                                else{
                                    $firstMarker = TempScanCutting::where('scan_cutting_id', $scan_cutting->id)->orderBy('created_at');
                                    if($firstMarker->count()>0){
                                        foreach($firstMarker->get() as $val){
                                            $ratioMarker = CuttingMarker::where('barcode_id', $val->barcode_marker)->where('deleted_at', null)->first();
                                            if($data_marker->item_id != $ratioMarker->item_id){
                                                if($data_marker->ratio_print != $ratioMarker->ratio_print){
                                                    return response()->json(['status' => 442, 'message' => 'Barcode Marker Rasio Beda!']);
                                                }
                                            }
                                        }
                                    }

                                    $checkMarker = TempScanCutting::FirstOrCreate([
                                        'barcode_table'   => $scan_cutting->barcode_table,
                                        'barcode_marker'  => $barcode_id,
                                        'created_at'      => $now,
                                        'updated_at'      => $now,
                                        'created_by'      => \Auth::user()->nik,
                                        'factory_id'      => \Auth::user()->factory_id,
                                        'deleted_at'      => null,
                                        'scan_cutting_id' => $scan_cutting->id,
                                        // 'user_id'         => $data->id,
                                        // 'name'            => $data->name,
                                        // 'nik'             => $data->nik,
                                        // 'deleted_at'      => null,
                                    ]);
                                }

                                $responseArray = array();
                                $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                                foreach ($listMarker->get() as $key => $value) {

                                    $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                                    $response = [
                                        'scan_id'       => $scan_cutting->id,
                                        'barcode_id'    => $marker_data->barcode_id,
                                        'style'         => $marker_data->cutting_plan->style,
                                        'articleno'     => $marker_data->cutting_plan->articleno,
                                        'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                        'cut'           => $marker_data->cut,
                                        'part_no'       => $marker_data->part_no,
                                        'marker_length' => $marker_data->marker_length,
                                        'perimeter'     => $marker_data->perimeter,
                                        'ratio_print'   => $marker_data->ratio_print,
                                        'layer'         => $marker_data->layer,
                                        'marker_pcs'    => $marker_data->qty_ratio,
                                        'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                        'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                        'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                        // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                        'state'         => $scan_cutting->state,
                                        'table_name'    => $scan_cutting->table_name,
                                    ];
                                    // var_dump($response);
                                    $responseArray[] = $response;

                                    $ratio       = $marker_data->ratio_print;
                                }
                                $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                                // $response = [
                                //     'scan_id'       => $scan_cutting->id,
                                //     'barcode_id'    => $data_marker->barcode_id,
                                //     'style'         => $data_marker->cutting_plan->style,
                                //     'articleno'     => $data_marker->cutting_plan->articleno,
                                //     'size_category' => $data_marker->size_category == 'A' ? 'Asian' : $data_marker->size_category == 'J' ? 'Japan' : 'Inter',
                                //     'cut'           => $data_marker->cut,
                                //     'part_no'       => $data_marker->part_no,
                                //     'marker_length' => $data_marker->marker_length,
                                //     'perimeter'     => $data_marker->perimeter,
                                //     'ratio_print'   => $data_marker->ratio_print,
                                //     'layer'         => $data_marker->layer,
                                //     'marker_pcs'    => $data_marker->qty_ratio,
                                //     'total'         => $data_marker->layer * $data_marker->qty_ratio,
                                //     'cutting_date'  => $data_marker->cutting_plan->cutting_date,
                                //     // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                //     // 'state'         => $scan_cutting->state,
                                //     'table_name'    => $scan_cutting->table_name,
                                // ];
                                return response()->json(['status' => 200, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'list_marker' => $listBarcode]);
                                // return response()->json(['status' => 200, 'marker' => $response, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'list_marker' => $listMarker]);
                            } else {
                                return response()->json(['status' => 442, 'message' => 'Tidak tersedia untuk barcode tersebut!']);
                            }
                        }
                    } else {
                        return response()->json(['status' => 442, 'message' => 'Marker belum di spreading!']);
                    }
                }else {
                    return response()->json(['status' => 442, 'message' => 'Proses tidak dapat dikenali!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
        }
    }
    public function saveCutting(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->marker_check;
            $id_scan = $request->id_scan;
            $state = $request->state;
            if($barcode_id != null) {
                if($state == 'open') {
                    $data = FabricUsed::where('barcode_marker', $barcode_id)->where('deleted_at', null)->where('factory_id', \Auth::user()->factory_id)->first();
                    if($data != null) {
                        $check_progress = ScanCutting::where('barcode_marker', $barcode_id)->where('deleted_at', null)->whereIn('state', ['progress','done'])->first();
                        if($check_progress != null) {
                            return response()->json(['status' => 442, 'message' => 'Marker sudah digunakan!']);
                        } else {
                            $scan_cutting = ScanCutting::where('id', $id_scan)->where('deleted_at', null)->where('state', 'open')->first();
                            $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();
                            if($scan_cutting != null && $data_marker != null) {
                                $scan_cutting->state = 'progress';
                                $scan_cutting->start_time = Carbon::now();
                                $scan_cutting->barcode_marker = $barcode_id;
                                $scan_cutting->save();
                                $response = [
                                    'scan_id' => $scan_cutting->id,
                                    'barcode_id' => $data_marker->barcode_id,
                                    'style' => $data_marker->cutting_plan->style,
                                    'articleno' => $data_marker->cutting_plan->articleno,
                                    'size_category' => $data_marker->size_category == 'A' ? 'Asian' : $data_marker->size_category == 'J' ? 'Japan' : 'Inter',
                                    'cut' => $data_marker->cut,
                                    'part_no' => $data_marker->part_no,
                                    'marker_length' => $data_marker->marker_length,
                                    'perimeter' => $data_marker->perimeter,
                                    'start_time' => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                                    'state' => $scan_cutting->state,
                                    'table_name' => $scan_cutting->table_name,
                                ];
                                return response()->json(['status' => 200, 'marker' => $response, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                            } else {
                                return response()->json(['status' => 442, 'message' => 'Tidak tersedia untuk barcode tersebut!']);
                            }
                        }
                    } else {
                        return response()->json(['status' => 442, 'message' => 'Marker belum di spreading!']);
                    }
                } elseif($state == 'progress') {
                    $scan_cutting = ScanCutting::where('id', $id_scan)->where('barcode_marker', $barcode_id)->first();
                    $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();
                    if($scan_cutting != null && $data_marker != null) {
                        $scan_cutting->state = 'done';
                        $scan_cutting->end_time = Carbon::now();
                        $scan_cutting->save();
                        $response = [
                            'scan_id' => $scan_cutting->id,
                            'barcode_id' => $data_marker->barcode_id,
                            'style' => $data_marker->cutting_plan->style,
                            'articleno' => $data_marker->cutting_plan->articleno,
                            'size_category' => $data_marker->size_category == 'A' ? 'Asian' : $data_marker->size_category == 'J' ? 'Japan' : 'Inter',
                            'cut' => $data_marker->cut,
                            'part_no' => $data_marker->part_no,
                            'marker_length' => $data_marker->marker_length,
                            'perimeter' => $data_marker->perimeter,
                            'start_time' => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                            'end_time' => Carbon::parse($scan_cutting->end_time)->format('H:i:s'),
                            'state' => $scan_cutting->state,
                            'table_name' => $scan_cutting->table_name,
                        ];

                        return response()->json(['status' => 200, 'marker' => $response, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                    } else {
                        return response()->json(['status' => 442, 'message' => 'Tidak tersedia untuk barcode tersebut!']);
                    }
                } else {
                    return response()->json(['status' => 442, 'message' => 'Proses tidak dapat dikenali!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }


    public function deleteOperator(Request $request)
    {
        if(request()->ajax())
        {
            $id_scan_operator = $request->id_scan_operator;
            if($id_scan_operator != null) {
                $data = ScanCuttingOperator::where('id', $id_scan_operator)->first();
                if($data != null) {
                    $scan_cutting = ScanCutting::where('id', $data->scan_cutting_id)->first();
                    $data->deleted_at = Carbon::now();
                    $data->save();
                    return response()->json(['status' => 200, 'data' => $data, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                } else {
                    return response()->json(['status' => 442, 'message' => 'Barcode operator tidak ditemukan atau sudah expire!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }

    public function updateDowntime(Request $request)
    {
        if(request()->ajax())
        {
            $downtime          = $request->downtime;
            $keterangan        = $request->keterangan;
            $downtime_category = $request->downtime_category;
            $list_scan_id      = json_decode($request->list_scan_id);

            // dd(json_decode($list_scan_id));

            if($list_scan_id != null || ($downtime != null || $keterangan != null || $downtime_category != null)) {


                foreach ($list_scan_id as $key => $value) {

                    $scan_cutting = ScanCutting::where('id', $value)->first();

                    CuttingMovement::firstOrCreate([
                        'barcode_id'   => $scan_cutting->barcode_marker,
                        'process_from' => 'spreading',
                        'status_from'  => 'completed',
                        'process_to'   => 'cutting',
                        'status_to'    => 'completed',
                        'is_canceled'  => false,
                        'user_id'      => \Auth::user()->id,
                        'description'  => 'cutting',
                        'ip_address'   => $this->getIPClient(),
                        'barcode_type' => 'spreading',
                        'deleted_at'   => null
                    ]);

                    $scan_cutting->downtime = $downtime;
                    $scan_cutting->keterangan = $keterangan;
                    $scan_cutting->downtime_category_id = $downtime_category;
                    $scan_cutting->save();
                }

                return response()->json(['status' => 200, 'message' => 'Data berhasil disimpan!']);
            } else {
                return response()->json(['status' => 200, 'message' => 'Data berhasil disimpan!']);
            }
        }
    }


    public function deleteMarker(Request $request)
    {
        if(request()->ajax())
        {
            $scan_id = $request->scan_id;
            $barcode_id = $request->barcode_id;
            if($barcode_id != null) {
                $data = TempScanCutting::where('barcode_marker', $barcode_id)->first();
                if($data != null) {
                    // $data->deleted_at = Carbon::now();
                    $data->delete();

                    $responseArray = array();
                    $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_id);
                    $scan_cutting  = ScanCutting::where('id', $scan_id)->first();
                    foreach ($listMarker->get() as $key => $value) {


                        $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                        $response = [
                            'scan_id'       => $value->scan_cutting_id,
                            'barcode_id'    => $marker_data->barcode_id,
                            'style'         => $marker_data->cutting_plan->style,
                            'articleno'     => $marker_data->cutting_plan->articleno,
                            'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                            'cut'           => $marker_data->cut,
                            'part_no'       => $marker_data->part_no,
                            'marker_length' => $marker_data->marker_length,
                            'perimeter'     => $marker_data->perimeter,
                            'ratio_print'   => $marker_data->ratio_print,
                            'layer'         => $marker_data->layer,
                            'marker_pcs'    => $marker_data->qty_ratio,
                            'total'         => $marker_data->layer * $marker_data->qty_ratio,
                            'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                            'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                            // 'start_time'    => Carbon::parse($scan_cutting->start_time)->format('H:i:s'),
                            'state'         => $scan_cutting->state,
                            // 'table_name'    => $scan_cutting->table_name,
                        ];
                        // var_dump($response);
                        $responseArray[] = $response;

                        $ratio       = $marker_data->ratio_print;
                    }
                    $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                    // return response()->json($data,200);
                    return response()->json(['status' => 200, 'marker' => $responseArray, 'list_marker' => $listBarcode]);
                } else {
                    return response()->json('Proses cutting tidak dapat ditemukan, silakan kontak ict!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat transfer data!',422);
            }
        }
    }

    public function cancelCutting(Request $request)
    {
        if(request()->ajax())
        {
            $scan_id = $request->id_scan;
            if($scan_id != null) {
                $data = ScanCutting::where('id', $scan_id)->first();
                if($data != null) {
                    $data->deleted_at = Carbon::now();
                    $data->save();
                    return response()->json($data,200);
                } else {
                    return response()->json('Proses cutting tidak dapat ditemukan, silakan kontak ict!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat transfer data!',422);
            }
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }


    public function startCutting(Request $request)
    {
        if(request()->ajax())
        {
            $list_marker = $request->list_marker;
            $id_scan     = $request->id_scan;
            $state       = $request->state;

            // dd($list_marker);
            if($list_marker != null) {
                $scan_cutting = ScanCutting::where('id', $id_scan)->first();
                $state        = $scan_cutting->state;

                if($state == 'open') {
                    $check_progress = ScanCutting::whereIn('barcode_marker', $list_marker)->where('deleted_at', null)->whereIn('state', ['progress','done'])->count();
                    if($check_progress>0) {
                        return response()->json(['status' => 442, 'message' => 'Marker sudah digunakan!']);
                    } else {
                        foreach ($list_marker as $key => $value) {
                            $temp           = TempScanCutting::where('barcode_marker', $value)->first();
                            $temp->start_at = Carbon::now();
                            $temp->save();
                        }

                        $scan_cutting->state = 'progress';
                        $scan_cutting->save();

                        $responseArray = array();
                        $listMarker    = TempScanCutting::where('scan_cutting_id', $scan_cutting->id);
                        foreach ($listMarker->get() as $key => $value) {

                            $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                            $response = [
                                'scan_id'       => $scan_cutting->id,
                                'barcode_id'    => $marker_data->barcode_id,
                                'style'         => $marker_data->cutting_plan->style,
                                'articleno'     => $marker_data->cutting_plan->articleno,
                                'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                                'cut'           => $marker_data->cut,
                                'part_no'       => $marker_data->part_no,
                                'marker_length' => $marker_data->marker_length,
                                'perimeter'     => $marker_data->perimeter,
                                'ratio_print'   => $marker_data->ratio_print,
                                'layer'         => $marker_data->layer,
                                'marker_pcs'    => $marker_data->qty_ratio,
                                'total'         => $marker_data->layer * $marker_data->qty_ratio,
                                'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                                'start_at'      => $value->start_at == null ? '-' : $value->start_at,
                                'state'         => $scan_cutting->state,
                                'table_name'    => $scan_cutting->table_name,
                            ];
                            // var_dump($response);
                            $responseArray[] = $response;
                            $ratio       = $marker_data->ratio_print;
                        }

                        $listBarcode = json_encode($listMarker->pluck('barcode_marker')->toArray());
                        return response()->json(['status' => 200, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_marker' => $listBarcode]);
                        // return response()->json(['status' => 200, 'marker' => $response, 'header' => $scan_cutting, 'operator' => $scan_cutting->operator]);
                    }
                } elseif($state == 'progress') {
                    return response()->json(['status' => 442, 'message' => 'Meja sedang dipakai proses cutting!']);
                } else {
                    return response()->json(['status' => 442, 'message' => 'Proses tidak dapat dikenali!']);
                }
            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }

    public function endCutting(Request $request)
    {
        if(request()->ajax())
        {
            $list_marker = $request->list_marker;
            $id_scan     = $request->id_scan;
            $state       = $request->state;

            if($list_marker != null) {
                $check_scan = ScanCutting::where('id', $id_scan)->first();
                $state      = $check_scan->state;
                $now        = Carbon::now();

                try
                {
                    DB::beginTransaction();

                    foreach ($list_marker as $key => $value) {
                        $temp           = TempScanCutting::where('barcode_marker', $value)->first();

                        $scan_cutting = ScanCutting::FirstOrCreate([
                            'barcode_table'  => $check_scan->barcode_table,
                            'state'          => 'done',
                            'factory_id'     => \Auth::user()->factory_id,
                            'deleted_at'     => null,
                            'table_name'     => $check_scan->table_name,
                            'barcode_marker' => $temp->barcode_marker,
                            'start_time'     => $temp->start_at,
                            'end_time'       => $now,
                        ]);

                        foreach($check_scan->operator as $op){
                            ScanCuttingOperator::FirstOrCreate([
                                'scan_cutting_id' => $scan_cutting->id,
                                'user_id'         => $op->user_id,
                                'name'            => $op->name,
                                'nik'             => $op->nik,
                                'deleted_at'      => null,
                            ]);
                        }

                        $temp->delete();
                    }

                    $check_scan->delete();

                    $responseArray = array();
                    $listMarker    = ScanCutting::whereIn('barcode_marker', $list_marker);
                    foreach ($listMarker->get() as $key => $value) {

                        $marker_data = CuttingMarker::where('barcode_id', $value->barcode_marker)->where('deleted_at', null)->first();
                        $response = [
                            'scan_id'       => $value->id,
                            'barcode_id'    => $marker_data->barcode_id,
                            'style'         => $marker_data->cutting_plan->style,
                            'articleno'     => $marker_data->cutting_plan->articleno,
                            'size_category' => $marker_data->size_category == 'A' ? 'Asian' : $marker_data->size_category == 'J' ? 'Japan' : 'Inter',
                            'cut'           => $marker_data->cut,
                            'part_no'       => $marker_data->part_no,
                            'marker_length' => $marker_data->marker_length,
                            'perimeter'     => $marker_data->perimeter,
                            'ratio_print'   => $marker_data->ratio_print,
                            'layer'         => $marker_data->layer,
                            'marker_pcs'    => $marker_data->qty_ratio,
                            'total'         => $marker_data->layer * $marker_data->qty_ratio,
                            'cutting_date'  => $marker_data->cutting_plan->cutting_date,
                            'start_at'      => $value->start_time == null ? '-' : $value->start_time,
                            'end_at'        => $value->end_time == null ? '-' : $value->end_time,
                            'state'         => $value->state,
                            'table_name'    => $value->table_name,
                        ];
                        // var_dump($response);
                        $responseArray[] = $response;
                        $ratio       = $marker_data->ratio_print;
                    }

                    $listScanId = json_encode($listMarker->pluck('id')->toArray());

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json(['status' => 200, 'marker' => $responseArray, 'mynik' => \Auth::user()->nik, 'list_scan_id' => $listScanId]);

            } else {
                return response()->json(['status' => 442, 'message' => 'Terjadi kesalahan saat transfer data!']);
            }
        }
    }
}
