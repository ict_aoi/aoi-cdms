<div id="detailRequestFabricModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				<div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="detailRequestFabricTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Style</th>
                                    <th>Article</th>
                                    <th>Item Code</th>
                                    <th>Part No</th>
                                    <th>UOM</th>
                                    <th>CSI</th>
                                    <th>SSP</th>
                                    <th>BM</th>
                                    <th>Diajukan Oleh</th>
                                    <th>Diajukan pada</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <input type="hidden" name="req_fabric_id" id="req_fabric_id" />

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					{{-- <button type="submit" class="btn btn-primary">Submit Form</button> --}}
				</div>
			{{-- {!! Form::close() !!} --}}
		</div>
	</div>
</div>
