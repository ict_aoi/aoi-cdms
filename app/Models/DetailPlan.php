<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;


class DetailPlan extends Model
{
   	use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_cutting_plan';
	protected $fillable = ['po_buyer','id_plan','queu','deleted_at','created_at','updated_at','statistical_date','qty','destination','job'];

	public function cutting_plan()
    {
        return $this->belongsTo('App\Models\CuttingPlan', 'id_plan', 'id');
    }
}
