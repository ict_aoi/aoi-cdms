<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: TRF</title>
</head>
<body>
    <div class="outer-mini text-uppercase" style="padding-top:50px">
		<div style="text-align:center">CUTTING DISTRIBUTION MANAGEMENT SYSTEM - FABRIC</div>
		<div style="text-align:center">PT. APPAREL ONE INDONESIA</div><br>
		<div class="qrcode" style="text-align:center">
            <img style="height: 2cm; width: 8cm;margin-top: 10px; margin-left: 0px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$master_testings->no_trf", 'C128') }}" alt="barcode"   />	
        </div>
        <br/>
		<div class="footer" style="text-align:center">			
			<p style="font-size:20px">{{ $master_testings->no_trf }}</p>
		</div>
	</div>	
</body>
</html> 
