@extends('layouts.app',['active' => 'qc_bundle'])

@section('page-css')
    <style>
        .defect-table{
            margin-top:20px;
        }
    </style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">QC Check </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">QC Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

        {{-- @if ($found==0)
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="row form-group" id="form-filter">
                        <h1 class="text-center text-semibold">MAAF, BARCODE <b>{{ $id }}</b> TIDAK DITEMUKAN</h1>
                        <h4 class="text-center text-semibold">SILAHKAN CEK KEMBALI</h4>
                    </div>
                </div>
            </div>
        @else --}}
            <div id="input-0" class="panel panel-flat">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="col-md-12">
                            <div class="form-group col-lg-12">
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Meja</label>
                                <div class="col-md-10 col-lg-10 col-sm-12" style="margin-bottom:15px;">
                                    @include('form.select', [
                                    'field'     => 'i_meja_qc',
                                    'options'   => [
                                        '' => 'Pilih Meja Bundling',
                                        ]+$qc_table,
                                        'class'      => 'select-search inputDisabled',
                                        'style'     => 'padding-left:0px',
                                        'attributes' => [
                                            'id' => 'i_meja_qc'
                                    ]])
                                </div>
                                <label class="control-label col-md-2 col-lg-2 col-sm-12 op_lable hidden">Operator Name</label>
                                <div class="col-md-10 col-lg-10 col-sm-12 op_select hidden" style="margin-bottom:15px;">
                                    <div class="col-md-8 col-lg-10 col-sm-12" style="margin-bottom:15px;padding-left:20px;padding-right:15px">
                                        <select data-placeholder="Select Operator" class="form-control select-search operator_c" style="width:81%" name="operator_v" id="operator_v">
                                            <option value="" style="padding-left:0px"></option>
                                            @foreach($operator as $op)
                                                <option value="{{ $op->nik }}" style="padding-left:0px">{{ $op->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Barcode Bundle</label>
                                <div class="col-md-8 col-lg-8 col-sm-12" style="padding-left:30px">
                                    <input type="text" class="form-control" id="s_barcode_bundle" placeholder="Barcode Bundle" required>
                                </div>
                                <button type="button" class="btn btn-warning col-md-2 col-lg-2 col-sm-12" id="btn-search">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="input-1" class="panel panel-flat">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="headerTable">
                            <tbody>
                                <tr>
                                    <td><strong>Style</strong></td>
                                    <td><span id="i_style">Data Empty!</span></td>
                                    <td><strong>Cut Num | Proses</strong></td>
                                    <td><span id="i_cut">Data Empty!</span></td>
                                </tr>
                                <tr>
                                    <td><strong>Supplier</strong></td>
                                    <td><span id="i_supplier">Data Empty!</span></td>
                                    <td><strong>Sticker</strong></td>
                                    <td><span id="i_sticker">Data Empty!</span></td>

                                </tr>
                                <tr>
                                    <td><strong>PO Buyer | Article</strong></td>
                                    <td><span id="i_pobuyer">Data Empty!</span></td>
                                    <td><strong>Part - Komponen</strong></td>
                                    <td><span id="i_komponen">Data Empty!</span></td>
                                </tr>
                                <tr>
                                    <td><strong>Size</strong></td>
                                    <td><span id="i_size">Data Empty!</span></td>
                                    <td><strong>Qty</strong></td>
                                    <td><span id="i_qty">Data Empty!</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div id="input-2" class="panel panel-flat">
                <div class="panel-body">
                    <div class="form-horizontal">

                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group col-lg-12">
                                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="defect">Ada Defect</label>
                                <label class="radio-inline"><input type="radio" name="radio_status" value="ok">OK (Tidak Ada Defect)</label>
                            </div>
                        </div>

                        
                        <div class="col-md-12 col-lg-12 col-sm-12 dvDefect">
                            @include('form.select', [
                                'field'     => 'i_defect',
                                'label'     => 'Defect',
                                // 'mandatory' => '*Wajib diisi',
                                'options'   => [
                                    '' => 'Pilih Defect',
                                ],
                                'class'      => 'select-search',
                                'attributes' => [
                                    'id' => 'i_defect'
                                ]
                            ])
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 ror_c">
                            <div class="form-group col-lg-12">
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Repair / Reject</label>
                                <div class="col-md-10 col-lg-10 col-sm-10">
                                    <select data-placeholder="REPAIR / REJECT" class="form-control select-search ror_c" style="width:81%" name="ror_v" id="ror_v">
                                        <option value="" style="padding-left:0px"></option>
                                        @foreach($ror as $ror)
                                            <option value="{{ $ror }}" style="padding-left:0px">{{ strtoupper($ror) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 dvDefect">
                            <div class="form-group col-lg-12">
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Qty</label>
                                <div class="col-md-10 col-lg-10 col-sm-12">
                                    <input type="number" class="form-control" id="i_numbering_1" placeholder="Qty" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 dvDefect mes_and_pos hidden">
                            <div class="form-group col-lg-12">
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Measurement</label>
                                <div class="col-md-4 col-lg-4 col-sm-4">
                                    <input type="text" class="form-control" onkeypress="return isNumberDot();" id="measurement" name="measurement" placeholder="0.00 cm">
                                </div>
                                <label class="control-label col-md-2 col-lg-2 col-sm-12">Position</label>
                                <div class="col-md-4 col-lg-4 col-sm-4">
                                    <select class="form-control select-search" id="position">
                                        <option></option>
                                        <option value="top">TOP</option>
                                        <option value="middle">MIDDLE</option>
                                        <option value="bottom">BOTTOM</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 dvDefect">
                            <button type="button" class="btn btn-info col-md-12 col-lg-12 col-sm-12" id="btn-add">Tambah</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="defect-table">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-semibold">Defect List</h6>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                    <table class="table table table-striped" id="table-defect">
                                            <thead>
                                                <tr>
                                                    <th>Meja Bundle</th>
                                                    <th>Defect</th>
                                                    <th>Qty</th>
                                                    <th>Type</th>
                                                    <th>Measeurement</th>
                                                    <th>Position</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="list_defect"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {!!
                            Form::open([
                                'role'   => 'form',
                                'url'    => route('Qccheck.simpanQc'),
                                'method' => 'post',
                                'class'  => 'form-horizontal',
                                'id'     => 'insert_qc'
                            ])
                        !!}

                        {!! Form::hidden('bundle_table_id', '', array('id' => 'bundle_table_id')) !!}
                        {!! Form::hidden('barcode_id', '', array('id' => 'barcode_id')) !!}
                        {!! Form::hidden('defectlist', '[]', array('id' => 'defectlist')) !!}
                        {!! Form::hidden('status_komponen', 'defect', array('id' => 'status_komponen')) !!}
                        {!! Form::hidden('supplier_id', '', array('id' => 'supplier_id')) !!}
                        {!! Form::hidden('operator', '', array('id' => 'operator')) !!}
                        {!! Form::hidden('rep_or_rej', '', array('id' => 'rep_or_rej')) !!}
                        {!! Form::hidden('locator_id', '', array('id' => 'locator_id')) !!}
                        <button type="submit" class="col-md-12 col-lg-12 col-sm-12 btn btn-success">Submit</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            {{-- <div id="notice-done" class="panel panel-flat hidden">
                <div class="panel-body">
                    <div class="row form-group" id="form-filter">
                        <h1 class="text-center text-semibold">BARCODE <b>{{ $id }}</b> SUDAH DI INPUT</h1>
                        <h4 class="text-center text-semibold">TERIMA KASIH</h4>
                    </div>
                </div>
            </div>
        @endif --}}
@endsection

@section('page-modal')
    @include('qccheck._defect_list')
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $('.select-search').select2({
        allowClear: true,
    });

    $('#i_meja_qc').select2();
    $('#i_defect').select2();
    $('#position').select2();

    defectlist = JSON.parse($('#defectlist').val());
    setInputFilter(document.getElementById("i_numbering_1"), function(value) {
        return /^\d*$/.test(value);
    });


    var s_barcode = document.getElementById("s_barcode_bundle");
    s_barcode.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("btn-search").click();
            $("#s_barcode_bundle").blur();
        }
    });

    $('#i_meja_qc').on('change',function(event){
        event.preventDefault();
        var locator = $('#i_meja_qc').val();
        $('#bundle_table_id').val($(this).val());
        if(!locator){
            $("#i_defect").empty();
            $("#i_defect").append('<option value="">Pilih Defect</option>');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'get',
            url : "get-defect-list/"+locator,
            beforeSend:function(){
                $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
            },
            success:function(response){
                $.unblockUI();
                if(response.status == 200) {

                    var list_defect = response.data.list_defect;

                    $("#i_defect").empty();
                    $("#i_defect").append('<option value="">Pilih Defect</option>');

                    $.each(list_defect, function(index, value) {
                        $("#i_defect").append('<option value="' + value.id + '">' + value.nama_defect + '</option>');
                    });
                    var x = $('#locator_id').val(response.data.locator_id);
                    if(x[0].value != ""){
                        if(x[0].value != 10 || x[0].value != 3){
                            $('.op_lable').removeClass('hidden');
                            $('.op_select').removeClass('hidden');
                            var op = $('#operator_v').trigger('change').val();
                            $('#operator').val(op);
                        }
                        if(x[0].value == 10 || x[0].value == 3){
                            $('.op_lable').addClass('hidden');
                            $('.op_select').addClass('hidden');
                            $('#operator_v').val('').change();
                        }
                    }else{
                        $('.op_lable').addClass("hidden");
                        $('.op_select').addClass("hidden");
                    }
                } else {
                    $("#i_defect").empty();
                    $("#i_defect").append('<option value="">Pilih Defect</option>');
                }
            },
            error:function(response){
                $("#alert_warning").trigger("click", 'Terjadi Error, Cek Koneksi Jaringan!');
                $("#i_defect").empty();
                $("#i_defect").append('<option value="">Pilih Defect</option>');
            }
        });
    });

    $('#operator_v').on('change',function(){
        $('#operator').val(this.value);
    });

    $('#ror_v').on('change',function(){
        $('#rep_or_rej').val(this.value);
    });

    $('#btn-search').on('click',function(){
            var barcode_id = $('#s_barcode_bundle').val();
            var operator = $('#operator_v').val();
            var meja_qc =$('#i_meja_qc').val();
            var locator = $('#locator_id').val();

            if(meja_qc == null || meja_qc == ''){
                $("#alert_warning").trigger("click", 'Pilih Meja Dahulu!');
                return false;
            }

            if (barcode_id == null || barcode_id == '') {
                $("#alert_warning").trigger("click", 'Barcode tidak ditemukan..!');
                return false;
            }
            if(locator == 10 || locator == 3){

            }else{
                if(operator == '' || operator == null){
                    $("#alert_warning").trigger("click", 'Pilih Operator Dahulu!');
                return false;
                }
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url : "get-bundle/",
                data:{
                    barcode_id:barcode_id,
                    meja_qc:meja_qc,
                    operator:operator,
                },
                beforeSend:function(){
                   $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                    });
                },
                success:function(response){
                    $.unblockUI();
                    if(response.status == 200) {
                        var data = response.data;
                        $('#insert_support').trigger("reset");
                        defectlist = [];
                        render();
                        $('#barcode_id').val(response.data.id);
                        $('#operator').val(response.data.operator);
                        $('#i_style').text(data.check_barcode.style);
                        if(data.check_barcode.process_name == null){
                            $('#i_cut').text(data.check_barcode.cut_num);
                        }else{
                            $('#i_cut').text(data.check_barcode.cut_num+' | '+data.check_barcode.process_name);
                        }
                        $('#i_article').text(data.check_barcode.article);
                        $('#i_sticker').text(data.check_barcode.start_no+' - '+data.check_barcode.end_no);
                        $('#i_pobuyer').text(data.check_barcode.poreference + ' | ' + data.check_barcode.article);
                        $('#i_komponen').text(data.check_barcode.part+' - '+data.check_barcode.komponen_name);
                        $('#i_size').text(data.check_barcode.size);
                        $('#i_qty').text(data.check_barcode.qty);
                        $('#i_meja_qc').val(response.data.meja_qc).trigger('change');
                        if(response.data.supplier == null){
                            $('#supplier_id').val(null);
                            $('#i_supplier').text('Data Empty!');
                        }else{
                            $('#supplier_id').val(data.supplier.id);
                            $('#i_supplier').text(data.supplier.initials+'-'+data.supplier.division);
                        }
                        if(locator != 10 || locator != 3){
                            $('.op_lable').removeClass('hidden');
                            $('.op_select').removeClass('hidden');
                        }
                        $('#locator_id').val(response.data.locator_id);
                        // $("#alert_success").trigger("click", 'Marker berhasil direlease!');
                    } else {
                        $("#s_barcode_bundle").val('');
                        $("#operator").val('');
                        $('#i_meja_qc').val('').trigger('change');
                        $('#supplier').val('').trigger('change');
                        $("#s_barcode_bundle").focus();
                        $('#insert_support').trigger("reset");
                        defectlist = [];
                        render();
                        $('#barcode_id').val('');
                        $('#i_style').text('Data Empty!');
                        $('#i_cut').text('Data Empty!');
                        $('#i_article').text('Data Empty!');
                        $('#i_sticker').text('Data Empty!');
                        $('#i_pobuyer').text('Data Empty!');
                        $('#i_komponen').text('Data Empty!');
                        $('#i_size').text('Data Empty!');
                        $('#i_qty').text('Data Empty!');
                        $('#i_supplier').text('Data Empty!');
                        $('#operator_v').val('').change();
                        $("#alert_warning").trigger("click", response.responseJSON);
                    }
                },
                error:function(response){
                    // $("#alert_warning").trigger("click", 'Terjadi Error, Cek Koneksi Jaringan!');
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });


    });

    ////////////
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'ok' || this.value == 'okAll') {
            if($('.dvDefect').hasClass('hidden')) {
                $('.dvDefect').removeClass('hidden');
            }

            if($('.ror_c').hasClass('hidden')){
                $('.ror_c').removeClass('hidden');
            }
            $('.dvDefect').addClass('hidden');
            $('.ror_c').addClass('hidden');
            $('#ror_v').trigger('change').val('');
            $('#rep_or_rej').val('');
        }
        else if (this.value == 'defect') {
            if($('.dvDefect').hasClass('hidden')) {
                $('.dvDefect').removeClass('hidden');
            }

            if($('.ror_c').hasClass('hidden')){
                $('.ror_c').removeClass('hidden');
            }

            $('.dvDefect').removeClass('hidden');
            $('.ror_c').removeClass('hidden');
            $('#ror_v').trigger('change').val('');
            $('#rep_or_rej').val('');
        }

        $('#status_komponen').val(this.value);
    });
    ///////////

    $('#insert_qc').submit(function (event){
		event.preventDefault();

        $('.inputDisabled').prop("disabled", false); // Element(s) are now enabled.

        var bundle_table = $('#bundle_table_id').val();
        var barcode_id   = $('#barcode_id').val();
        var i_defect     = $('#i_defect').val();
        var supplier_id  = $('#supplier_id').val();
        var operator     = $('#operator').val();
        var locator      = $('#locator_id').val();
        var ror_res      = $('#rep_or_rej').val();
        var stat_komponen = $('#status_komponen').val();
        var measurement     = $('#measurement').val();
        var position        = $('#position').val();


        if(!bundle_table){
            $("#alert_warning").trigger("click", 'Input Meja/Defect Terlebih Dahulu!');
            return false;
        }

        if(!barcode_id){
            $("#alert_warning").trigger("click", 'Bundle Belum Dipilih!');
            return false;
        }

        if(locator == 10 || locator == 3){

        }else{
            if(operator == '' || operator == null){
                $("#alert_warning").trigger("click", 'Operator Belum Dipilih!');
            return false;
            }
        }

        if(stat_komponen == 'defect'){
            if(ror_res == null || ror_res == ''){
                $("#alert_warning").trigger("click", 'Pilih Repair / Reject Dahulu!');
            return false;
            }
        }

		bootbox.confirm("Apakah anda yakin akan menyimpan data defect ini?", function (result) {
		    if(result){
				var formData = new FormData($('#insert_qc')[0]);
		        $.ajax({
		            type: "POST",
		            url: $('#insert_qc').attr('action'),
		            // data: $('#insert_support').serialize(),
					data: formData,
					processData: false,
					contentType: false,
		            beforeSend: function () {
		                $.blockUI({
		                    message: '<i class="icon-spinner4 spinner"></i>',
		                    overlayCSS: {
		                        backgroundColor: '#fff',
		                        opacity: 0.8,
		                        cursor: 'wait'
		                    },
		                    css: {
		                        border: 0,
		                        padding: 0,
		                        backgroundColor: 'transparent'
		                    }
		                });
		            },
		            complete: function () {
		                $.unblockUI();
		            },
		            success: function (response) {
                        console.log(response);
                        var data_response = response.data;
                        if (typeof data_response !== "undefined" && data_response) {
                            $("#alert_success").trigger("click", data_response.output);
                            $('#insert_support').trigger("reset");
                            $('#i_numbering').val('');
                            defectlist = [];
                            $("#i_defect").val('').change();
                            $('#i_meja_qc').val('').change();
                            $('#supplier').val('').change();
                            $("#s_barcode_bundle").val('');
                            $("#s_barcode_bundle").focus();
                            $('#barcode_id').val('');
                            $('#i_style').text('Data Empty!');
                            $('#i_cut').text('Data Empty!');
                            $('#i_article').text('Data Empty!');
                            $('#i_sticker').text('Data Empty!');
                            $('#i_pobuyer').text('Data Empty!');
                            $('#i_komponen').text('Data Empty!');
                            $('#i_size').text('Data Empty!');
                            $('#i_qty').text('Data Empty!');
                            $('#i_supplier').text('Data Empty!');
                            $('#operator').val('');
                            $('#operator_v').val('').change();
                            $('#rep_or_rej').val('');
                            $('#ror_v').val('').change();
                            $('#measurement').val('');
                            $('#position').val('').change();
                            render();
                            window.scrollTo(0,0);
                        }else{
                            $("#alert_warning").trigger("click",data_response.output);
                        }
		            },
		            error: function (response) {
		                $.unblockUI();
                        $("#alert_warning").trigger("click",'Save failed, table required');
		            }
		        });
		    }
		});
	});

    $('#i_defect').on('change',function(){
        var text = $("#i_defect option:selected").text();
        if(text == 'Kurang Dari Pola' || text == 'Lebih Dari Pola'){
            $('.mes_and_pos').removeClass('hidden');
        }else{
            $('.mes_and_pos').addClass('hidden');
            $('input[name=measurement]').val(0);
            $('#position').val(null).change();
        }
    });

    render();
});


$('#btn-add').on('click',function()
{
    var i_defect    = $("#i_defect").val();
    var numbering1  = $('#i_numbering_1').val();
    var meja        = $('#i_meja_qc').val();
    var supplier    = $('#supplier').val();
    var nama_meja   = $('#i_meja_qc').select2('data')[0].text;
    var id_defect   = $('#i_defect').val();
    var nama_defect = $('#i_defect').select2('data')[0].text;
    var barcode_id  = $('#barcode_id').val();
    var qty_asli    = $('#i_qty').text();
    var ror         = $('#rep_or_rej').val();
    var measurement = $('#measurement').val();
    var position    = $('#position').select2('data')[0].text;

    if(numbering1 == 0){
        $("#alert_warning").trigger("click", 'Qty Tidak Boleh = 0!');
        return false;
    }else if(!barcode_id){
        $("#alert_warning").trigger("click", 'Scan Bundle terlebih dahulu!');
        $('input[name=measurement]').val('');
        $('#position').val('').change();
        $('#i_defect').val('').change();
        $('#ror_v').val('').change();
        $('#i_numbering_1').val('');
        if(!$('.mes_and_pos').hasClass('hidden')){
            $('.mes_and_pos').addClass('hidden');
        }
        return false;
    }
    else if(!numbering1){
        $("#alert_warning").trigger("click", 'Input Qty terlebih dahulu!');
        return false;
    }
    else if(!meja){
        $("#alert_warning").trigger("click", 'Pilih Meja Bundle terlebih dahulu!');
        return false;
    }
    else if(!i_defect){
        $("#alert_warning").trigger("click", 'Pilih Defect Bundle terlebih dahulu!');
        return false;
    }
    else if(parseInt(numbering1)>parseInt(qty_asli)){
        $("#alert_warning").trigger("click", 'Qty Defect Melebihi Qty Bundle!');
        return false;
    }
    else {

        var input = {
                'qty': numbering1,
                'meja': meja,
                'supplier':supplier,
                'nama_meja': nama_meja,
                'nama_defect': nama_defect,
                'id_defect': id_defect,
                'ror': ror,
                'measurement': measurement,
                'position' : position
        };

        var diff = checkItem(id_defect);
        if (!diff)
        {
            $("#alert_warning").trigger("click", 'Defect '+nama_defect+' sudah diinput!');
            return false;
        }
        defectlist.push(input);
        render();

        $('#i_numbering_1').val('');
        $("#i_defect").val('').change();
        $("#bundle_table_id").val(meja);
    }
});

function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}


function render()
{
    getIndex();
    $('#defectlist').val(JSON.stringify(defectlist));

    var tmpl = $('#defect_table').html();
    Mustache.parse(tmpl);
    var data = { item: defectlist };
    var html = Mustache.render(tmpl, data);
    $('#list_defect').html(html);
    documentFreeze();
    bind();
}

function bind()
{
    $('.btn-delete-item').on('click', deleteItem);
}

function getIndex()
{
    for (idx in defectlist)
    {
        defectlist[idx]['_id'] = idx;
        defectlist[idx]['no'] = parseInt(idx) + 1;
    }
}

function deleteItem()
{
    var i = parseInt($(this).data('id'), 10);

    defectlist.splice(i, 1);
    render();
}

function checkItem(id)
{
    for (var i in defectlist)
    {
        var data = defectlist[i];

        if (data.id_defect == id)
            return false;
    }

    return true;
}

function documentFreeze(){
    var data = Array();

    $('#table-defect tbody tr').each(function(i, v) {
        data[i] = Array();
        $(this).children('td').each(function(ii, vv) {
            data[i][ii] = $(this).text();
        });
    });


    if (data.length > 0) {
        $('#i_meja_qc').attr('disabled', true);
        $('#sub_komponen').attr('disabled', true);
    }else{
        $('#i_meja_qc').removeAttr('disabled');
        $('#sub_komponen').removeAttr('disabled');

        $('#i_meja_qc').val('').trigger('change');
        $('#sub_komponen').val('-').trigger('change');
    }
}

</script>

@endsection
