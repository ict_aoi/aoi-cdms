@extends('layouts.app',['active' => 'qc_check'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">QC Check </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">QC Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group" id="form-filter">
            <label>Scan Barcode Marker</label>
            <input type="text" name="barcode_id" id="barcode_id" class="form-control barcode_id" placeholder="#scan barcode marker">
            {{-- <select data-placeholder="Search Barcode Marker" name="barcode_id" id="barcode_id" class="form-control select-search">
                <option value=""></option>
                @foreach($barcode as $bar)
                    <option value="{{ $bar->barcode_marker }}">{{ $bar->barcode_marker }}</option>
                @endforeach
            </select> --}}
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Size</th>
                        <th>Qty Cutting</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> QC Check Input - <span id="lb_marker">Data Empty!</span></span>
				</legend>
			</div>
            <div class="modal-header">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="headerTable">
						<tbody>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td><span id="lb_style">Data Empty!</span></td>
                                <td><strong>PO Supplier</strong></td>
                                <td><span id="lb_supplier">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>PO Buyer</strong></td>
                                <td><span id="lb_buyer">Data Empty!</span></td>
                                <td><strong>Cut No</strong></td>
                                <td><span id="lb_cutting">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Color</strong></td>
                                <td><span id="lb_color">Data Empty!</span></td>
                                <td><strong>Barcode Marker</strong></td>
                                <td><span id="lb_marker">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Item</strong></td>
                                <td><span id="lb_item">Data Empty!</span></td>
                                <td><strong>Size</strong></td>
                                <td><span id="lb_size">Data Empty!</span></td>
                            </tr>
                        </tbody>
					</table>
                    <input type="text" name="tx_barcode" id="tx_barcode" class="hidden">
                    <input type="text" name="tx_size" id="tx_size" class="hidden">
				</div>
			</div>

            <div class="modal-body">
                <!-- table -->
                <div class="row form-group">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-detail">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Componen</th>
                                    <th>QTY (pcs)</th>
                                    <th width="175px">Table</th>
                                    <th>Bowing</th>
                                    <th>Sliding</th>
                                    <th>Pilling</th>
                                    <th>Snag</th>
                                    <th>Siult</th>
                                    <th>Crease Mark</th>
                                    <th>Foresign Yam</th>
                                    <th>Mising Yam</th>
                                    <th>Shide Bar</th>
                                    <th>Exposure</th>
                                    <th>Hole</th>
                                    <th>Dirly Mark / Rust</th>
                                    <th>Stain</th>
                                    <th>Printing Defect</th>
                                    <th>Streak Line</th>
                                    <th>Dye Spot</th>
                                    <th>Shrinkage</th>
                                    <th>Slide Cutting</th>
                                    <th>Wrong Fice Side</th>
                                    <th>Tag Pin</th>
                                    <th>Poor Cutting</th>
                                    <th>Needle Hole</th>
                                    <th>Fabric Join</th>
                                    <th>Snaging Cut</th>
                                    <th>Wrong Size</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <!-- footer -->
                <div class="row form-group">
                    <button type="button" class="btn btn-default pull-right" id="btn-close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success pull-right" id="btn-save"  onclick="saveqc(this);">SAVE</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $('#barcode_id').focus();

    var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{route('Qccheck.getData')}}",
                data: function(d) {
                     return $.extend({}, d, {
                         "barcode_id"         : $('#barcode_id').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'size', name: 'size'},
                {data: 'qty', name: 'qty'},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

    $('#barcode_id').on('change',function(event){
            event.preventDefault();

            var barcode_id = $('#barcode_id').val();
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : "{{route('Qccheck.getData')}}",
                data:{barcode_id:barcode_id},
                beforeSend:function(){
                    $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                },
                success:function(response){
                    if (response.recordsTotal!=0) {
                        tableL.clear();
                        tableL.draw();
                        $.unblockUI();
                    }else{
                        $("#alert_warning").trigger("click","Barcode Marker Not Found Or Barcode Marker Was Cutting");
                        tableL.clear();
                        tableL.draw();
                        $.unblockUI();
                    }

                },
                error:function(response){
                   console.log(response);
                }
            });
    });


    $('#table-list').on('click','.btn-detail',function(){
            var barcode_id = $(this).data('barcode');
            var size = $(this).data('size');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url : "{{ route('Qccheck.getHeader') }}",
                data:{barcode_id:barcode_id},
                beforeSend:function(){
                   $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                },
                success:function(response){
                    var data = response.data;

                     $('#lb_style').text(response.style_replace);
                    $('#lb_buyer').text(data.po_buyer);
                    $('#lb_color').text(data.color);
                    $('#lb_item').text(data.item_code);
                    $('#lb_cutting').text(data.cut);
                    $('#lb_marker').text(barcode_id);
                    $('#lb_size').text(size);
                    $('#lb_supplier').text(data.po_supplier);
                    $('#tx_barcode').val(barcode_id);
                    $('#tx_size').val(size);
                    tableDetail(data.style,size,barcode_id);
                    $('#modalDetail').modal('show');
                    $.unblockUI();
                },
                error:function(response){
                   myalert('error', response);
                   console.log(response);
                }
            });


    });
});

function tableDetail(style,size,barcode){


    $('#table-detail').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        searching:false,
        paging:false,
        destroy:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{route('Qccheck.getDetail')}}",
            data: function(d) {
                 return $.extend({}, d, {
                     "style"         : style,
                     "size"         : size,
                     "barcode"         : barcode
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            $('td', row).eq(3).css('min-width','175px');
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'qty', name: 'qty'},
            {data: 'table', name: 'table'},
            {data: 'bowing', name: 'bowing'},
            {data: 'sliding', name: 'sliding'},
            {data: 'pilling', name: 'pilling'},
            {data: 'snag', name: 'snag'},
            {data: 'siult', name: 'siult'},
            {data: 'crease_mark', name: 'crease_mark'},
            {data: 'foresign_yam', name: 'foresign_yam'},
            {data: 'mising_yam', name: 'mising_yam'},
            {data: 'shide_bar', name: 'shide_bar'},
            {data: 'exposure', name: 'exposure'},
            {data: 'hole', name: 'hole'},
            {data: 'dirly_mark', name: 'dirly_mark'},
            {data: 'stain', name: 'stain'},
            {data: 'printing_defact', name: 'printing_defact'},
            {data: 'streak_line', name: 'streak_line'},
            {data: 'dye_spot', name: 'dye_spot'},
            {data: 'shrinkage', name: 'shrinkage'},
            {data: 'slide_cutting', name: 'slide_cutting'},
            {data: 'wrong_fice', name: 'wrong_fice'},
            {data: 'tag_pin', name: 'tag_pin'},
            {data: 'poor_cutting', name: 'poor_cutting'},
            {data: 'needle_hole', name: 'needle_hole'},
            {data: 'fabric_join', name: 'fabric_join'},
            {data: 'snaging_cut', name: 'snaging_cut'},
            {data: 'wrong_size', name: 'wrong_size'},
            {data: 'remark', name: 'remark'}
        ]
    });
}

function saveqc(e){
    var data = Array();
    var barcode = $('#tx_barcode').val();
    var size = $('#tx_size').val();
   var formData = new FormData();
    $('#table-detail tbody tr').each(function(i,v){
        data[i]=Array();
        var part;

        $(this).children('td').each(function(ii,vv){

                if (ii==0) {
                   data[i][ii]=$(this).find('label').text();
                   part=$(this).find('label').text();
                }

                if (ii==2) {
                  data[i][ii]=$(this).find('input').val();
                }

                if (ii==3) {
                  data[i][ii]=$(this).find('select').val();
                }

                if (ii>=4 && ii<=29) {
                  data[i][ii]=$(this).find('input').val();
                }
                // if (ii==29) {

                //  console.log( $('input[type=file]')[0].files[0]);

                // }
        });

    });



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'post',
        url : "{{ route('Qccheck.saveQc') }}",
        data:{data:data,barcode:barcode,size:size},
        beforeSend:function(){
           $('#modalDetail').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
        },
        success:function(response){
            var data_response = response.data;
            if (data_response.status == 200) {
                $("#alert_success").trigger("click", data_response.output);

            }else{
                $("#alert_warning").trigger("click",data_response.output);

            }
            $('#modalDetail').unblock();
            $('#modalDetail').modal('hide');

        },
        error:function(response){
           $("#alert_warning").trigger("click",'Save failed, table required');
           $('#modalDetail').unblock();
            $('#modalDetail').modal('hide');
           console.log(response);
        }
    });
}



</script>

@endsection
