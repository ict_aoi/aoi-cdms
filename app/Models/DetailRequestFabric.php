<?php

namespace App\Models;
use App\Uuids;

use Illuminate\Database\Eloquent\Model;

class DetailRequestFabric extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    // protected $fillable = ['plan_id', 'submit_by', 'submit_at', 'approval_whs_by', 'approval_whs_at', 'approval_lab_by', 'approval_lab_at'];

    protected $fillable     = ['req_fabric_id', 'item_code', 'part_no', 'uom','csi', 'approcal_whs_by',
    'approval_whs_at', 'approval_lab_by', 'approval_lab_at', 'created_by','updated_by','deleted_at'];
    protected $dates        = ['created_at','updated_at'];

    public function RequestFabric()
    {
        return $this->belongsTo('App\Models\RequestFabric');
    }
}
