<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarkFabricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remark_fabric', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->char('id_plan', 36);
            $table->date('cutting_date');
            $table->string('style');
            $table->string('articleno');
            $table->string('material');
            $table->string('color');
            $table->string('part_no', 10);
            $table->text('remark')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remark_fabric');
    }
}
