@extends('adibooster.index')

@section('page-css')
<style type="text/css">
    .title-fac{
        color: black;
        font-family:Cooper Black;
        font-size:  40px;
    }

    .highcharts-point {
        stroke: white;
    }

    .main-color .highcharts-graph {
        stroke: red;
    }
    .main-color, .main-color .highcharts-point {
        fill: red;
    }
    .highcharts-graph.highcharts-negative {
        stroke: blue;
    }
    .highcharts-area.highcharts-negative {
        fill: blue;
    }
    .highcharts-point.highcharts-negative {
        fill: blue;
    }
</style>
@endsection

@section('page-header')
<div class="page-header" style="height: 60px;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1px; padding-bottom: 5px;">
        	<center><label class="title-fac">DASHBOARD SPREADING AOI {{$factory_id}} (SHIFT : {{$shift}})</label></center>
        	<input type="text" name="factory_id" id="factory_id" class="hidden" value="{{$factory_id}}">
        </div>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body" style="margin-top: 25px;" >
        <div class="row">
            <div id="chart" style="width:100%; height:550px;"></div>
        </div>
        <br />
        <div class="row">

            <div class="col-md-3">
                <div class="col-md-2" style="background-color: #ba181b; height: 28px;"></div>
                <div class="col-md-10" style=" height: 28px;"><label style="font-size: 22px;"><b>Not On Target</b></label></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-2" style="background-color: #fee440; height: 28px;"></div>
                <div class="col-md-10" style=" height: 28px;"><label style="font-size: 22px;"><b>On Target Last Hour</b></label></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-2" style="background-color: #55a630; height: 28px;"></div>
                <div class="col-md-10" style=" height: 28px;"><label style="font-size: 22px;"><b>On Target</b></label></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-2" style="background-color: rgb(67, 67, 72); height: 28px;"></div>
                <div class="col-md-10" style=" height: 28px;"><label style="font-size: 22px;"><b>Target</b></label></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
Highcharts.chart('chart', {
    chart: {
        zoomType: 'xy'
    },

    title: {
        text: ' '
    },

    xAxis: [{
        categories:[
            <?php
                foreach ($table as $tab) {
                    echo "'".$tab->table_name."',";
                }
             ?>
        ] ,
        crosshair: true
    }],

    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Yards',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }],

    tooltip: {
        shared: true,
        dataLabels: {
            enabled: true
        },
    },

    legend: {
        enabled: false,
    },

    series: [{
        name: 'Spreading',
        type: 'column',
        data: [
            <?php
                foreach ($spread as $key => $sp) {
                    if($spread[$key] < $target[$key]) {
                        if($spread[$key] > $target2[$key]){
                            echo "{y:".$spread[$key].", color: '#fee440'},";
                        }
                        else{
                            echo "{y:".$spread[$key].", color: '#ba181b'},";
                        }
                    } else {
                        echo "{y:".$spread[$key].", color: '#55a630'},";
                    }
                }
             ?>
        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    }, {
        name: 'Target',
        type: 'spline',
        data: [
            <?php
                foreach ($target as $tg) {
                    echo $tg.", ";
                }
             ?>
        ],
        tooltip: {
            valueSuffix: 'Yards',
        },
        dataLabels: {
          enabled: true
        }
    }]
});

$(document).ready(function(){
     var factory_id = $('#factory_id').val();
     setInterval(function(){window.location.href="/adibooster/preparation/"+factory_id },180000);

});


</script>
@endsection
