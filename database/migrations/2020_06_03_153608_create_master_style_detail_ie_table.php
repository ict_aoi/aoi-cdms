<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterStyleDetailIeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_style_detail_ie', function (Blueprint $table) {
            $table->increments('id');
            $table->string('style', 200)->nullable();
            $table->integer('season')->nullable();
            $table->integer('komponen')->nullable();
            $table->string('process', 200)->nullable();
            $table->integer('type_id')->nullable()->default(1);
            $table->integer('user_id')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_style_detail_ie');
    }
}
