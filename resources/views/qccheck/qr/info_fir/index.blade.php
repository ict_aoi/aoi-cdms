@extends('layouts.app',['active' => 'plan_movement'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
    .hidden {
		display: none;
	}
    .huruf-center {
        text-align: center;
    }
    .batas-atas {
        margin-top: 30px;
    }
    .huruf-tebal {
        font-weight: bold;
    }
    .ubah-bg {
        background-color: #b5fffa;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Info FIR {{ $data_id }}</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Info FIR {{ $data_id }}</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th class="dt_col_hide">Id</th>
                        <th>No Roll</th>
                        <th>Material</th>
                        <th>Color</th>
                        <th>Actual LOT</th>
                        <th>PO Supplier</th>
                        <th>Qty Prepared</th>
                        <th>Layer</th>
                        <th>Fabric Inspect</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <input type="hidden" id="barcode_marker" name="barcode_marker" value="{{ $data_id }}" />
</div>
@endsection

@section('page-modal')
	@include('qccheck.info_fir._detail_modal')
@endsection

@section('page-js')
<script>
    $(document).ready( function () {

        $('#barcode_marker').select2({
            placeholder: "Select a State",
            minimumInputLength: 3,
            formatInputTooShort: function () {
                return "Enter Minimum 3 Character";
            },
        });
		
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });
    
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            // serverSide: true, //sync
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
    
        var table = $('#planningTable').DataTable({
            ajax: {
                url: '/info-fir/data-detail',
                data: {
                    barcode_marker: $('#barcode_marker').val(),
                },
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
                {
                    targets: 1,
                    className: 'dt_col_hide',
                },
            ],
            columns: [
                {data: 'barcode_fabric', name: 'barcode_fabric',orderable: false, searchable: false},
                {data: 'id', name: 'id',orderable: false, searchable: false},
                {data: 'no_roll', name: 'no_roll',orderable: false, searchable: false},
                {data: 'item_code', name: 'item_code',orderable: false, searchable: false},
                {data: 'color', name: 'color',orderable: false, searchable: false},
                {data: 'lot', name: 'lot',orderable: false, searchable: false},
                {data: 'po_supplier', name: 'po_supplier',orderable: false, searchable: false},
                {data: 'qty_fabric', name: 'qty_fabric',orderable: false, searchable: false},
                {data: 'actual_layer', name: 'actual_layer',orderable: false, searchable: false},
                {data: 'fabric_inspect', name: 'fabric_inspect',orderable: false, searchable: false},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ],
        });
    
        $('#planningTable').on('click','.ignore-click', function() {
            return false;
        });
        
        var dtable = $('#planningTable').dataTable().api();
    
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
    
        dtable.draw();
    });

    function detail(url) {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#date_of_detail').text(response.date_of);
                $('#batch_number_detail').text(response.batch_number);
                $('#actual_lot_detail').text(response.actual_lot);
                $('#kg_detail').text(response.kg);
                $('#sticker_yard_detail').text(response.sticker_yard);
                $('#width_on_barcode_detail').text(response.width_on_barcode);
                $('#nomor_roll_detail').text(response.nomor_roll);
                $('#yard_actual_detail').text(response.yard_actual);
                $('#yard_diff_detail').text(response.yard_diff);
                $('#cuttable_width_detail').text(response.cuttable_width);
                $('#defect_a_detail').text(response.defect_a);
                $('#defect_b_detail').text(response.defect_b);
                $('#defect_c_detail').text(response.defect_c);
                $('#defect_d_detail').text(response.defect_d);
                $('#defect_e_detail').text(response.defect_e);
                $('#defect_f_detail').text(response.defect_f);
                $('#defect_g_detail').text(response.defect_g);
                $('#defect_h_detail').text(response.defect_h);
                $('#defect_i_detail').text(response.defect_i);
                $('#defect_j_detail').text(response.defect_j);
                $('#defect_k_detail').text(response.defect_k);
                $('#defect_l_detail').text(response.defect_l);
                $('#defect_m_detail').text(response.defect_m);
                $('#defect_n_detail').text(response.defect_n);
                $('#defect_o_detail').text(response.defect_o);
                $('#defect_p_detail').text(response.defect_p);
                $('#defect_q_detail').text(response.defect_q);
                $('#defect_r_detail').text(response.defect_r);
                $('#defect_s_detail').text(response.defect_s);
                $('#defect_t_detail').text(response.defect_t);
                $('#linier_point_detail').text(response.linier_point);
                $('#sq_detail').text(response.sq);
                $('#formula_1_detail').text(response.formula_1);
                $('#formula_2_detail').text(response.formula_2);
                $('#created_by_detail').text(response.created_by);
                $('#comfirm_detail').text(response.comfirm);
                $('#final_result_detail').text(response.final_result);
                $('#note_detail').text(response.note);
                $('#detailModal').modal();
            },
            error: function (response) {
                $.unblockUI();
            }
        });
    }
    
    function isNumberDot(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        if (key.length == 0) return;
            var regex = /^[0-9\b]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
@endsection
