<?php

namespace App\Http\Controllers\Exim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EximDocument;
use App\Models\EximDocumentheader;
use File;
use Excel;
use Carbon\Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use DataTables;

class EximController extends Controller
{
    public function index(){
        return view('exim.index');
    }

    public function uploaddocumentbc(Request $request){
        if($request->hasFile('file_upload')){
            $extension = \File::extension($request->file_upload->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls") {
                $path = $request->file_upload->getRealPath();
                $data = \Excel::selectSheetsByIndex(0)->load($path)->get();
            }
        }else{
            return response()->json('Pilih Dokumen Dahulu !', 422);
        }

        try {
            DB::begintransaction();
            foreach ($data as $dx => $val) {
                if($val->factory_id == '' || $val->factory_id == null){
                    return response()->json('Factory Tidak Boleh Kosong !', 422);
                }
                if($val->document_type == '' || $val->document_type == null){
                    return response()->json('Type Dokumen Tidak Boleh Kosong !', 422);
                }
                if($val->aju_no == '' || $val->aju_no == null){
                    return response()->json('No Aju Tidak Boleh Kosong !', 422);
                }
                if($val->pabean_no == '' || $val->pabean_no == null){
                    return response()->json('No Pabean Tidak Boleh Kosong !', 422);
                }
                if($val->pabean_date == '' || $val->pabean_date == null){
                    return response()->json('Tanggal Pabean Tidak Boleh Kosong !', 422);
                }
                if($val->receive_no == '' || $val->receive_no == null){
                    return response()->json('No Penerimaan Tidak Boleh Kosong !', 422);
                }
                if($val->receive_date == '' || $val->receive_date == null){
                    return response()->json('Tanggal Penerimaan Tidak Boleh Kosong !', 422);
                }
                if($val->subcont_factory == '' || $val->subcont_factory == null){
                    return response()->json('Pemasok Tidak Boleh Kosong !', 422);
                }
                if($val->item_code == '' || $val->item_code == null){
                    return response()->json('Item Code Tidak Boleh Kosong !', 422);
                }
                if($val->item_name == '' || $val->item_name == null){
                    return response()->json('Item Name Tidak Boleh Kosong !', 422);
                }
                if($val->uom == '' || $val->uom == null){
                    return response()->json('UOM Tidak Boleh Kosong !', 422);
                }
                if($val->qty == '' || $val->qty == null){
                    return response()->json('QTY Pabe Tidak Boleh Kosong !', 422);
                }
                if($val->item_value == '' || $val->item_value == null){
                    return response()->json('Item Value Tidak Boleh Kosong !', 422);
                }
                if($val->currency == '' || $val->currency == null){
                    return response()->json('Currency Tidak Boleh Kosong !', 422);
                }
                if($val->type_movement == '' || $val->type_movement == null){
                    return response()->json('Type Movement Tidak Boleh Kosong !', 422);
                }

                if($val->factory_id == 'AOI 1'){
                    $val->factory_id = 1;
                }elseif($val->factory_id == 'AOI 2'){
                    $val->factory_id = 2;
                }else($val->factory_id = 0);

                $pabean_date = date('Y-m-d',strtotime($val->pabean_date));
                $receive_date = date('Y-m-d',strtotime($val->receive_date));
                $product = explode('_',$val->item_code);
                $kk_no = explode('_',$val->item_code);
                if($val->uom != 'PCE'){
                    $kk_no = '-';
                }else{
                    $kk_no = $kk_no[2];
                }

                $data_exist = EximDocument::where('factory_id',$val->factory_id)
                            ->where('document_type',$val->document_type)
                            ->where('aju_no',$val->aju_no)
                            ->where('pabean_no',$val->pabean_no)
                            ->where('pabean_date',$pabean_date)
                            ->where('receive_no',$val->receive_no)
                            ->where('receive_date',$receive_date)
                            ->where('subcont_factory',$val->subcont_factory)
                            ->where('item_code',$val->item_code)
                            ->where('item_name',$val->item_name)
                            ->where('uom',$val->uom)
                            ->where('qty',$val->qty)
                            ->where('item_value',$val->item_value)
                            ->where('currency',$val->currency)
                            ->where('type_movement',strtolower($val->type_movement))
                            ->exists();
                if($data_exist){
                    return response()->json('Data pernah di upload ! '.$val->aju_no.' | '.$val->item_code.' | '.$val->kk_no.' | '.strtoupper($val->type_movement), 422);
                }else{
                    EximDocument::FirstOrCreate([
                        'factory_id'            => $val->factory_id,
                        'document_type'         => trim($val->document_type),
                        'aju_no'                => trim($val->aju_no),
                        'pabean_no'             => trim($val->pabean_no),
                        'pabean_date'           => trim($pabean_date),
                        'receive_no'            => trim($val->receive_no),
                        'receive_date'          => $receive_date,
                        'subcont_factory'       => trim($val->subcont_factory),
                        'item_code'             => trim($val->item_code),
                        'item_name'             => trim($val->item_name),
                        'uom'                   => $val->uom,
                        'qty'                   => $val->qty,
                        'item_value'            => $val->item_value,
                        'currency'              => $val->currency,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                        'created_by'            => \Auth::user()->id,
                        'kk_no'                 => $kk_no,
                        'product'               => $product[0],
                        'quota_used'            => 0,
                        'balanced_quota'        => $val->qty,
                        'type_movement'         => strtolower($val->type_movement)
                    ]);

                    $data_detail = DB::table('document_bc_exim')
                    ->select('subcont_factory','product','factory_id','qty','kk_no','quota_used','document_type','uom','type_movement')
                    ->where('document_type',$val->document_type)
                    ->where('subcont_factory',$val->subcont_factory)
                    ->where('factory_id',$val->factory_id)
                    ->where('product',$product[0])
                    ->where('kk_no',$kk_no)
                    ->where('type_movement',strtolower($val->type_movement))
                    ->distinct()
                    ->get();

                    foreach($data_detail as $x => $xx){
                        if(strpos($xx->product,'-TOP') > 1){
                            $temp = explode('-',$xx->product);
                            $style = $temp[0];
                            $set_type = 'TOP';
                        }elseif(strpos($xx->product,'-BOT') > 1){
                            $temp = explode('-',$xx->product);
                            $style = $temp[0];
                            $set_type = 'BOTTOM';
                        }else{
                            $style = $xx->product;
                            $set_type = 'Non';
                        }

                        $check_header_exists = DB::table('document_bc_exim_header')
                        ->where('subcont_factory',$xx->subcont_factory)
                        ->where('style',$style)
                        ->where('set_type',$set_type)
                        ->where('factory_id',$xx->factory_id)
                        ->where('kk_no',$xx->kk_no)
                        ->where('uom',$xx->uom)
                        ->where('document_type',$xx->document_type)
                        ->where('type_movement',$xx->type_movement)
                        ->exists();

                        if($check_header_exists){
                            DB::table('document_bc_exim_header')
                            ->where('subcont_factory',$xx->subcont_factory)
                            ->where('style',$style)
                            ->where('set_type',$set_type)
                            ->where('factory_id',$xx->factory_id)
                            ->where('kk_no',$xx->kk_no)
                            ->where('uom',$xx->uom)
                            ->where('document_type',$xx->document_type)
                            ->where('type_movement',$xx->type_movement)
                            ->update([
                                'updated_at'        => Carbon::now(),
                                'updated_by'        => \Auth::user()->id
                            ]);
                        }else{
                            EximDocumentheader::FirstOrCreate([
                                'subcont_factory'   => $xx->subcont_factory,
                                'style'             => $style,
                                'set_type'          => $set_type,
                                'factory_id'        => $xx->factory_id,
                                'kk_no'             => $xx->kk_no,
                                'uom'               => $xx->uom,
                                'document_type'     => $xx->document_type,
                                'created_by'        => \Auth::user()->id,
                                'type_movement'     => $xx->type_movement
                            ]);
                        }
                        $get_header_id = DB::table('document_bc_exim_header')
                        ->where('document_type',$val->document_type)
                        ->where('subcont_factory',$val->subcont_factory)
                        ->where('factory_id',$val->factory_id)
                        ->where('style',$style)
                        ->where('kk_no',$kk_no)->get();
                        foreach($get_header_id as $xx => $xy){
                            DB::table('document_bc_exim')
                            ->where('factory_id',$xy->factory_id)
                            ->where('document_type',$xy->document_type)
                            ->where('kk_no',$xy->kk_no)
                            ->where('product',$product[0])
                            ->where('subcont_factory',$val->subcont_factory)
                            ->update([
                                'doc_bc_header_id' => $xy->id
                            ]);

                            $sum_qty = DB::table('document_bc_exim')
                            ->select(DB::raw('sum(qty) as total_qty,
                            sum(quota_used) as quota_used,
                            sum(balanced_quota) as balanced_quota,subcont_factory,product,factory_id,kk_no,uom,document_type'))
                            ->where('document_type',$val->document_type)
                            ->where('subcont_factory',$val->subcont_factory)
                            ->where('factory_id',$val->factory_id)
                            ->where('product',$product[0])
                            ->where('kk_no',$kk_no)
                            ->where('type_movement',$val->type_movement)
                            ->groupBy('product','subcont_factory','factory_id','kk_no','document_type','uom')
                            ->get();

                            foreach($sum_qty as $qt => $qx){
                                if(strpos($qx->product,'-TOP') > 1){
                                    $temp = explode('-',$qx->product);
                                    $style = $temp[0];
                                    $set_type = 'TOP';
                                }elseif(strpos($qx->product,'-BOT') > 1){
                                    $temp = explode('-',$qx->product);
                                    $style = $temp[0];
                                    $set_type = 'BOTTOM';
                                }else{
                                    $style = $qx->product;
                                    $set_type = 'Non';
                                }
                                $x = DB::table('document_bc_exim_header')
                                ->where('subcont_factory',$val->subcont_factory)
                                ->where('style',$style)
                                ->where('set_type',$set_type)
                                ->where('factory_id',$val->factory_id)
                                ->where('kk_no',$kk_no)
                                ->where('document_type',$val->document_type)
                                ->where('type_movement',$val->type_movement)
                                ->update([
                                    'total_qty'         => $qx->total_qty,
                                    'quota_used'        => $qx->quota_used,
                                    'balanced_quota'    => $qx->balanced_quota,
                                ]);
                            }
                        }
                    }
                    // //CREATE SUBCONT FACT FROM FILES AND GENERATE ACRONYM
                    // if($val->uom == 'PCE'){
                    //     $words = explode('.',$val->subcont_factory);
                    //     if(preg_match_all('/\b(\w)/',strtoupper($words[1]),$m)) {
                    //         $acronym = implode('',$m[1]);
                    //     }
                    // }
                    
                    // //CHECK SUBCONT FACTORY IF EXISTS
                    // $check_subcont_name = DB::table('master_subcont')
                    // ->where('subcont_name',$words[1])
                    // ->orWhere('initials',$acronym)
                    // ->exists();
                    
                    // if($check_subcont_name){
                    //     DB::table('master_subcont')->update([
                    //         'updated_at'        => Carbon::now(),
                    //         'updated_by'        => \Auth::user()->id,
                    //         'factory_id'        => \Auth::user()->factory_id
                    //     ]);
                    // }else{
                    //     DB::table('master_subcont')->insert([
                    //         'subcont_name'      => $val->subcont_factory,
                    //         'initials'          => $acronym,
                    //         'division'          => 'PRINTING',
                    //         'created_by'        => \Auth::user()->id,
                    //         'factory_id'        => \Auth::user()->factory_id,
                    //         'is_active'         => 't',
                    //         'created_at'        => Carbon::now()
                    //     ]);
                    // }
                }
            }
            DB::commit();
        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getDataDocbc(Request $request){
        if(request()->ajax()) 
        {
            $from = date_format(date_create(explode('-', preg_replace('/\s+/', '', $request->date_range))[0]),'Y-m-d 00:00:00');
            $to = date_format(date_create(explode('-', preg_replace('/\s+/', '', $request->date_range))[1]),'Y-m-d 23:59:59');

            $data = DB::table('document_bc_exim as dbe')
            ->select('master_factory.factory_alias as factory_name','dbe.document_type','dbe.aju_no','dbe.pabean_no','dbe.pabean_date','dbe.receive_no','dbe.receive_date','dbe.subcont_factory','dbe.item_code','dbe.item_name','dbe.uom','dbe.qty','dbe.item_value','dbe.currency','dbe.updated_at','dbe.kk_no','dbe.product')
            ->leftJoin('master_factory','master_factory.id','=','dbe.factory_id')
            ->whereBetween('dbe.updated_at',[$from,$to])
            ->where('dbe.factory_id',\Auth::user()->factory_id)
            ->whereNull('dbe.deleted_at')
            ->orderBy('dbe.updated_at','desc')
            ->get();
            if($data == null){
                return response()->json('Data Tidak Ditemukan..', 422);
            }else{
                return DataTables::of($data)
            ->make(true);
            }
        }
            
    }

}
