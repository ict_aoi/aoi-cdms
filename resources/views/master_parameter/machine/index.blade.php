@extends('layouts.app',['active' => 'master_machine_type'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Machine Type</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Machine Type</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        @csrf
        <div class="row">
        <form class="form-horizontal" action="{{ route('master_machine_type.add') }}" id="form-add">
            <div class="col-lg-10">
                <label><b>Machine Type</b></label>
                <input type="text" name="machine_type" id="machine_type" class="form-control" placeholder="Machine Type" style="text-transform:uppercase">
            </div>
            <div class="col-lg-1">
                <button type="submit" style="margin-top: 30px; margin-left: 20px;" class="btn btn-primary"><span class="icon-checkmark4"></span> Save</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th style="width: 20px;">No</th>
                            <th>Machine Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master_machine_type.getDataMachinetype') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'machine_type', name: 'machine_type'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
    });


    $('#form-add').submit(function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: $('#form-add').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#form-add').trigger("reset");
                $('#machine_type').val('');
                tableL.draw();
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        })
    });
});
function delType(e){
    var _token = $("input[name='_token]").val();
    var id = e.getAttribute('data-id');
    var tableL = $('#table-list').DataTable();

    bootbox.confirm('Are you sure inactive ?',
        function(result){
            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({
                        url:"master-machine-type/delete/{id}",
                        type: "GET",
                        data:{
                            "id": id,
                        },
                        beforeSend:function(){
                            loading_process();
                        },  
                        success: function(response){    
                            myalert('success','INACTIVE');
                            tableL.ajax.reload();
                        },
                        error: function(response) {
                            myalert('error',response);
                        }
                    });
            }
        }
    );
}
</script>
@endsection