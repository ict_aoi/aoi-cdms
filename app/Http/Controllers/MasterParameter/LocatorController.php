<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

use App\Models\Role;

class LocatorController extends Controller
{

    //MASTER LOCATOR
    public function masterLocator()
    {
        $roles = Role::get();

        return view('master_parameter.locator.index', compact('roles'));
    }

    public function ajaxGetDataMasterLocator(Request $request)
    {
        if ($request->ajax()){
            
            $data = DB::table('master_locator')
                        ->select('master_locator.id', 'master_locator.locator_name', 'master_locator.role', 'master_locator.is_artwork', 'master_locator.is_cutting', 'master_locator.is_setting', 'roles.name', 'roles.display_name', 'roles.description')
                        ->leftJoin('roles', 'roles.id', '=', 'master_locator.role')
                        ->wherenull('master_locator.deleted_at')
                        ->orderBy('master_locator.created_at', 'desc')
                        ->get();

            //datatables
            return DataTables::of($data)
                ->editColumn('is_artwork', function($data){
                    if ($data->is_artwork) {
                        $str = '<span class="label label-success label-rounded">
                                <i class="icon-checkmark2"></i>
                                </span>';
                    }else {
                        $str = '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }

                    return $str;
                })
                ->editColumn('is_cutting', function($data){
                    if ($data->is_cutting) {
                        $str = '<span class="label label-success label-rounded">
                                <i class="icon-checkmark2"></i>
                                </span>';
                    }else {
                        $str = '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }

                    return $str;
                })
                ->editColumn('is_setting', function($data){
                    if ($data->is_setting) {
                        $str = '<span class="label label-success label-rounded">
                                <i class="icon-checkmark2"></i>
                                </span>';
                    }else {
                        $str = '<span class="label label-default label-rounded">
                                <i class="icon-cross3"></i>
                                </span>';
                    }

                    return $str;
                })
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editLocator', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteLocator', $data->id)
                    ]);
                })
                ->rawColumns(['is_artwork', 'is_cutting', 'is_setting', 'action'])
                ->make(true);
        }
    }

    //add new master locator
    public function addLocator(Request $request)
    {

        $this->validate($request, [
            'locator_name' => 'required',
            'role' => 'required'
        ]);

        $locator_name = trim(strtoupper($request->locator_name));

        if ($request->is_artwork=='on') {
            $is_artwork = true;
        }else {
            $is_artwork = false;
        }
        // 
        if ($request->is_cutting=='on') {
            $is_cutting = true;
        }else {
            $is_cutting = false;
        }
        // 
        if ($request->is_setting=='on') {
            $is_setting = true;
        }else {
            $is_setting = false;
        }

        $data = array([
            'locator_name' => $locator_name,
            'role' => $request->role,
            'is_artwork' => $is_artwork,
            'is_cutting' => $is_cutting,
            'is_setting' => $is_setting,
            'created_at' => Carbon::now()
        ]);

        // cek locator exists
        $cekifexist = DB::table('master_locator')
                        ->where('locator_name', $locator_name)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master Locator already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table master_locator
            DB::table('master_locator')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master locator
    public function editLocator($id)
    {
        $master_locator = DB::table('master_locator')
                            ->select('master_locator.id', 'master_locator.locator_name', 'master_locator.role', 'master_locator.is_artwork', 'master_locator.is_cutting', 'master_locator.is_setting', 'roles.name', 'roles.display_name', 'roles.description')
                            ->leftJoin('roles', 'roles.id', '=', 'master_locator.role')
                            ->where('master_locator.id', $id)
                            ->first();

        return response()->json($master_locator, 200);
    }

    //update master locator
    public function updateLocator(Request $request)
    {
        $data = $request->all();

        $locator_name = trim(strtoupper($data['locator_name_update']));
        $is_artwork = $data['is_artwork_update'];
        $is_cutting = $data['is_cutting_update'];
        $is_setting = $data['is_setting_update'];

        //cek exists
        // $cekifexist = DB::table('master_locator')
        //                 ->where('locator_name', $locator_name)
        //                 ->where('is_artwork', $is_artwork)
        //                 ->whereNull('deleted_at')
        //                 ->exists();
        
        // if ($cekifexist) {
        //     return response()->json('Master locator already exists..!', 422);
        // }
        
        try {
            DB::beginTransaction();

                DB::table('master_locator')
                ->where('id', $data['id_update'])
                ->update([
                    'role' => $request->role_update,
                    'is_artwork' => $is_artwork,
                    'is_cutting' => $is_cutting,
                    'is_setting' => $is_setting,
                    'updated_at' => Carbon::now()
                ]);
                
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $roles = Role::get();

        return view('master_parameter.locator.index', compact('roles'));

    }

    //delete master locator
    public function deleteLocator(Request $request)
    {
        $id = $request->id;

        try {
            DB::beginTransaction();
                
                DB::table('master_locator')
                    ->where('id', $id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('master_parameter.locator.index');
    }

    //END OF MASTER LOCATOR
}
