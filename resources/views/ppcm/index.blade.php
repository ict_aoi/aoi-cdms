@extends('layouts.app',['active' => 'master_ppcm'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master PPCM </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Master PPCM</li>
            <li class="active">Upload Data PPCM</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_by_lcdate">
            <div class="col-md-12">
                <div class="alert alert-info alert-styled-right alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">Data Ditampilkan Berdasarkan Tanggal Start Sewing!</span>
                </div>
            </div>
            <div class="col-lg-10">
                <label><b>&nbsp</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <button style="margin-top: 26px;" type="button" class="btn btn-default" id="btn-upload"><span class="icon-cloud-upload2"></span> Upload PPCM</button>
            </div>
        </div>
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Line</th>
                            <th>PO Buyer</th>
                            <th>Style</th>
                            <th>Set Type</th>
                            <th>Season</th>
                            <th>Qty Order</th>
                            <th>Split Qty</th>
                            <th>Start Sewing</th>
                            <th>End Sewing</th>
                            <th>Article</th>
                            <th>Upload Date</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="panel bg-teal-400">
                <center><legend><h4 style="color:white"><b>Upload PPCM</b><h4></legend></center>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="alert alert-info alert-styled-right alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">Format File Yang Diterima Hanya CSV / EXCEL!</span>
                    </div>
                </div>
                <form action="{{ route('MasterPpcm.UploadPpcmNew') }}" method="POST" enctype="multipart/form-data" id="form-upload">
                    @csrf
                    <div class="row form-group">
                        <input type="file" name="file_upload" class="file-styled" id="file_upload">
                    </div>
                    <div class="row form-group">
                        <button class="btn btn-success col-md-12" type="submit" id="btn-save"><span class="icon-cloud-upload2"></span> <b>Upload<b></button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="cls_btn" class="btn btn-grey" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        scrollY:true,
        scrollX:true,
        pageLength:50,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('MasterPpcm.getDataPpcm') }}",
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'area_name', name: 'area_name'},
            {data: 'po_buyer', name: 'po_buyer'},
            {data: 'style', name: 'style'},
            {data: 'set_type', name: 'set_type'},
            {data: 'season', name: 'season'},
            {data: 'qty_order', name: 'qty_order'},
            {data: 'qty_split', name: 'qty_split'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'articleno', name: 'articleno'},
            {data: 'upload', name: 'upload'}
        ]
    });

    tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').on('click',function(){

        if ($('#date_range').val()=='') {
            $("#alert_warning").trigger("click",'Please select date !!!');
            return false;
        }
        tableL.draw();
    });

    $('#modalUpload').on('hidden.bs.modal', function () {
        location.reload();
    })

    $('#btn-upload').on('click',function(){
        $('#modalUpload').modal('show');
    });

    $('#form-upload').submit(function(event){
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['file_upload'];
        var val = [$('#file_upload').val()];

        if($('#file_upload').val() == '' || $('#file_upload').val() == null){
            myalert('error','Pilih File PPCM Dahulu!')
        }
        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },
            success: function(response) {
                $.unblockUI();
                myalert('success','Data Berhasil Diupload');
                $('#modalUpload').modal('hide');
                $('#file_upload').val('');
            },
            error: function(response) {
                $.unblockUI();
                $('#file_upload').val(null);
                if (response.status == 422){
                    myalert('error',response.responseJSON);
                }
            }
        });
    });

})
</script>
@endsection