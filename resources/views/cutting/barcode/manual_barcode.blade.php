@extends('layouts.app',['active' => 'manual_barcode'])
<style type="text/css">
    .slc{
        background-color:#abf6f7 !important;
    }
    .slc2{
        /* background-color:#abf6f7 !important; */
        height: 44px !important;
    }
    .switch {
    position: relative;
    display: inline-block;
    width: 45px;
    height: 24px;
    }

    .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
    }

    .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
    }

    .slider:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    left: 2px;
    right:0px;
    bottom: 2px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
    }

    input:checked + .slider {
    background-color: #2196F3;
    }

    input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
    -webkit-transform: translateX(20px);
    -ms-transform: translateX(20px);
    transform: translateX(20px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }
    .lb_tk{
        margin:2px;
        padding:2px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">RePrint Bundle Manual</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Print Bundle Partial</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat all_content">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active" style="color:white"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">MULTIPLE PO</a></li>
                <li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">MARKER</a></li>
                <li class=""><a href="#basic-tab3" data-toggle="tab" class="legitRipple" aria-expanded="true">PO BUYER</a></li>
			</ul>
			<div class="tab-content">
                <div class="tab-pane active" id="basic-tab1">
                    <form class="form-horizontal" action="{{route('manual_barcode.addNewData')}}" id="form_data_ticket" method="">
                        <fieldset class="content-group">
                            @csrf
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control slc" name="poreference" placeholder="Masukkan PO Buyer" required id="poreference">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control slc" name="cut_num" placeholder="Masukkan No Cutting" id="cut_num">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control slc" name="size" placeholder="Masukkan Size" id="size">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control slc" name="style_num" placeholder="Masukkan Style" id="style_num">
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control slc" name="part_name" placeholder="Masukkan Komponen" id="part_name">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control slc" name="article" placeholder="Masukkan Article" id="article">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control slc" name="start_num" placeholder="Masukkan Sticker Awal" id="start_num">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control slc" name="end_num" placeholder="Masukkan Sticker Akhir" id="end_num">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12" style="padding-top:10px">
                                        <label class="switch">
                                            <input type="checkbox" id="ticket_box" nam="ticket_box">
                                            <span class="slider round"></span>
                                        </label>
                                        <span class="lb_tk pull-left"><b>SERTAKAN TIKET BOX </b><i class="icon-list position-right"></i></span>
                                        <button type="submit" id="btn-add" style="margin-left:10px" class="btn btn-info pull-right margin-kanan">ADD <i class="icon-plus2 position-right"></i></button>
                                        <button type="button" id="print3" style="margin-left:10px" class="btn btn-success pull-right margin-kanan">Print <i class="icon-printer2 position-right"></i></button>
                                        <button type="button" id="cancel-print" class="btn btn-danger pull-right margin-kanan">Cancel <i class="icon-trash position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                        <div class="table-responsive">
                                <table class="table table-basic table-condensed" id="table-list3">
                                    <thead class="bg-grey-700">
                                        <tr>
                                            <th>Season</th>
                                            <th>Style</th>
                                            <th>PO Buyer</th>
                                            <th>Art</th>
                                            <th>Size</th>
                                            <th>PART | Komponen</th>
                                            <th>Cut | Sticker</th>
                                            <th>Box Lim</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="" id="" method="">
						<fieldset class="content-group">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-md-2 text-bold">BARCODE MARKER</label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-btn">	
                                            <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
                                        </span>
                                        <select data-placeholder="MK-XXXXXXXXXXX" class="form-control select-search" style="margin-right: 5px;padding-right: 5px;" name="barcode_marker" id="barcode_marker" required>
                                            <option value=""></option>
                                            @foreach($barcode_marker as $barcode_marker)
                                                <option value="{{ $barcode_marker->barcode_marker }}">{{$barcode_marker->barcode_marker }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div style="margin-top:10px">
                                        <button type="button" id="btn-filter" style="margin-left:10px" class="btn btn-info pull-right margin-kanan">Filter <i class="icon-pointer position-right"></i></button>
                                        <button type="button" id="print" class="btn btn-success pull-right margin-kanan">Print <i class="icon-printer2 position-right"></i></button>
                                    </div>
                                    <input type="hidden" name="detail_id" id="detail_id" />                                    
                                </div>
                                
                            </div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead>
										<tr>
											<th>No</th>
                                            <th><input type="checkbox" onclick="toggle(this)" id="check_all"></th>
											<th>Season</th>
											<th>Style</th>
											<th>PO Buyer</th>
											<th>Article</th>
                                            <th>Color</th>
											<th>Cut | Sticker</th>
                                            <th>Barcode Bundle</th>
											<th>Size</th>
											<th>Komponen</th>
											<th>Cutting Date</th>
											<th>Qty</th>
                                            <th>Job</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
                <div class="tab-pane" id="basic-tab3">
                    <form class="form-horizontal" action="" id="" method="">
                        <fieldset class="content-group">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-md-2 text-bold" style="width:120px">PO Buyer</label>
                                <div class="col-md-6" style="margin-bottom:10px;width:400px">
                                    <div class="input-group">
                                        <span class="input-group-btn">	
                                            <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
                                        </span>
                                        <select data-placeholder="" class="form-control select-search" style="margin-right: 5px;padding-right: 5px;width:270px" name="po_buyer" id="po_buyer" required>
                                            <option value=""></option>
                                            @foreach($po_buyer as $po_buyer)
                                                <option value="{{ $po_buyer->poreference }}">{{$po_buyer->poreference }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <label class="control-label col-md-2 text-bold" style="width:120px">Cut Num</label>
                                <div class="col-md-6" style="margin-bottom:10px;width:350px">
                                    <div class="input-group">
                                        <span class="input-group-btn">	
                                            <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
                                        </span>
                                        <select class="form-control col-md-4 select-search" name="cut_num" id="cut_num" style="width:315px">
                                        <option value=""></option>
                                        <input type="hidden" name="url_cut_num" id="url_cut_num" value ="{{ route('manual_barcode.getCutnum') }}" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <label class="control-label col-md-2 text-bold" style="width:120px">Style</label>
                                <div class="col-lg-10" style="margin-bottom:10px">
                                    <div class="input-group">
                                        <span class="input-group-btn">	
                                            <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
                                        </span>
                                        <select class="form-control col-md-4 select-search" name="style" id="style" style="">
                                        <option value=""></option>
                                            <input type="hidden" name="url_style" id="url_style" value ="{{ route('manual_barcode.getStyle') }}" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div style="margin-top:10px">
                                        <button type="button" id="btn-filter2" style="margin-left:10px" class="btn btn-info pull-right margin-kanan">Filter <i class="icon-pointer position-right"></i></button>
                                        <button type="button" id="print2" class="btn btn-success pull-right margin-kanan">Print <i class="icon-printer2 position-right"></i></button>
                                    </div>
                                    
                            </div>
                        </fieldset>
                    </form>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed" id="table-list2">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th><input type="checkbox" onclick="toggle(this)" id="check_all"></th>
                                            <th>Season</th>
                                            <th>Style</th>
                                            <th>PO Buyer</th>
                                            <th>Article</th>
                                            <th>Color</th>
                                            <th>Cut | Sticker</th>
                                            <th>Barcode Bundle</th>
                                            <th>Size</th>
                                            <th>Komponen</th>
                                            <th>Cutting Date</th>
                                            <th>Qty</th>
                                            <th>Job</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<a href="{{ route('manual_barcode.getDataTicket') }}" id="prev_data_ticket"></a>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });
	//DATATABLES 1
	var barcode_marker =$('#barcode_marker').val();
    var url = "{{route('manual_barcode.getDataDetailbundle')}}";
    var tableL = $('#table-list').DataTable({
        // processing: true,
        serverSide: true,
        deferRender:true,
		scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
				return $.extend({}, d, {
					"barcode_marker"		: $('#barcode_marker').val()
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'selector', name: 'selector',searchable:false,visible:true,orderable:false},//11
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},//2
            {data: 'poreference', name: 'poreference',searchable:true,visible:true,orderable:true},//3
            {data: 'article', name: 'article',searchable:false,visible:true,orderable:true},//4
            {data: 'color_name', name: 'color_name',searchable:false,visible:true,orderable:false},//5
            {data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//6
            {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//6
			{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//7
			{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//8
            {data: 'cutting_date', name: 'cutting_date',searchable:false,visible:true,orderable:true},//9
			{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},//10
			{data: 'job', name: 'job',searchable:false,visible:true,orderable:true},//11
        ]
    });

    //DATATABLES 2
    var po_buyer =$('#po_buyer').val();
    var url_detail = "{{route('manual_barcode.getDetailbypobundle')}}";
    var dtable = $('#table-list2').DataTable({
        // processing: true,
        serverSide: true,
        deferRender:true,
		scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url_detail,
            data: function(d) {
				return $.extend({}, d, {
					"po_buyer"		: $('#po_buyer').val(),
                    "style"		: $('#style').val(),
                    "cut_num"		: $('#cut_num').val(),
				});
            }
        },
		fnCreatedRow: function (row, data, index) {
            var info = dtable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'selector', name: 'selector',searchable:false,visible:true,orderable:false},//11
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:true},//1
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//2
            {data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:true},//3
            {data: 'article', name: 'article',searchable:false,visible:true,orderable:true},//4
            {data: 'color_name', name: 'color_name',searchable:false,visible:true,orderable:false},//5
            {data: 'cut_sticker', name: 'cut_sticker',searchable:true,visible:true,orderable:true},//6
            {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//6
			{data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//7
			{data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//8
            {data: 'cutting_date', name: 'cutting_date',searchable:false,visible:true,orderable:true},//9
			{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:true},//10
			{data: 'job', name: 'job',searchable:false,visible:true,orderable:true},//11
        ]
    });

    $('#btn-filter').click(function(){
        event.preventDefault();
        var barcode_marker    = $('#barcode_marker').val();

        if (barcode_marker=='') {
            $("#alert_warning").trigger("click", 'Pilih Marker Dahulu..!');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url,
                data:{
					barcode_marker:barcode_marker
				},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    barcode_marker = barcode_marker;
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });

    $('#btn-filter2').click(function(){
        event.preventDefault();
        var po_buyer    = $('#po_buyer').val();
        var style    = $('#style').val();
        var cut_num    = $('#cut_num').val();

        if (po_buyer=='') {
            $("#alert_warning").trigger("click", 'Pilih PO Buyer Dahulu..!');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url_detail,
                data:{
					po_buyer:po_buyer,
                    style:style,
                    cut_num:cut_num
				},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    po_buyer = po_buyer;
                    dtable.clear();
                    dtable.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });

    $('#cancel-print').click(function(){
        event.preventDefault();
        var user    = "{{Auth::user()->id}}";
        bootbox.confirm("Are you sure delete this Data ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'GET',
                    url : "/partial-print-ticket/delete-all-detail",
                    data:{
                        user:user
                    },
                    beforeSend: function () {
                        $('#table-list3').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing...</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success:function(response){
                        $('#table-list3').unblock();
                        myalert('success','DETAIL DELETED!');
                        table3.ajax.reload();
                    },
                    error:function(response){
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        $('#table-list3').unblock();
                    }
                })
            }
        });
    });

    $('#print').on('click', function() {
        var data = Array();
        var url_print = '/partial-print-ticket/printPdf';
        $('#table-list tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });
        console.log(data);
        window.open(url_print+'?data='+JSON.stringify(data), '_blank');
    });

    $('#print2').on('click', function() {
        var data = Array();
        var url_print = '/partial-print-ticket/printPdf';
        $('#table-list2 tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    if ($(this).find('#Inputselector').length > 0) {
                        data[i][ii] = $(this).find("#Inputselector").is(':checked');
                    } else {
                        data[i][ii] = $(this).find('input').val();
                    }
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });
        window.open(url_print+'?data='+JSON.stringify(data), '_blank');
    });

    $('#po_buyer').on('change', function () {  
	    var url_style = $('#url_style').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url :  url_style,
            data : {po_buyer:$('#po_buyer').val()},

            success: function(response) {
                console.log(response);
                var data = response.response;
                $('#style').empty();
                $('#style').append('<option></option>');
                
                for (var i = 0; i < data.length; i++) {
                $('#style').append('<option value="'+data[i]['style']+'">'+data[i]['style']+'</option>')
                }
            },
            error: function(response) {
                $.unblockUI();
                alert(response.status,response.responseText);
                
            }
        });
    });

    $('#po_buyer').on('change', function () {  
	var url_cut_num = $('#url_cut_num').val();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
        $.ajax({
            type: 'get',
            url :  url_cut_num,
            data : {po_buyer:$('#po_buyer').val()},

            success: function(response) {
                console.log(response);
                var data = response.response;
                $('#cut_num').empty();
                $('#cut_num').append('<option></option>');
                
                for (var i = 0; i < data.length; i++) {
                $('#cut_num').append('<option value="'+data[i]['cut_num']+'">'+data[i]['cut_num']+'</option>')
                }
            },
            error: function(response) {
                $.unblockUI();
                alert(response.status,response.responseText);
                
            }
        });
    });

    $('#form_data_ticket').submit(function(event) {
        var poreference = $('#poreference').val();
        if(poreference == '' || poreference == null){
            myalert('error','PO Buyer Tidak Boleh Kosong!')
            return false;
        }
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form_data_ticket').attr('action'),
            data: $('#form_data_ticket').serialize(),
            beforeSend: function () {
                $('#form_data_ticket').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing...</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function () {
                $('#form_data_ticket').unblock();
            },
            success: function(response) {
                $('#form_data_ticket').unblock();
                $('input[name=style_num]').val('');
                $('input[name=cut_num]').val('');
                $('input[name=article]').val('');
                $('input[name=part_name]').val('');
                $('input[name=start_num]').val('');
                $('input[name=end_num]').val('');
                table3.ajax.reload();
            },
            error: function(response) {
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#form_data_ticket').unblock();
            }
        })
    });

    $('#table-list3').on('click','.delete_b',function(){
        event.preventDefault();
        var id = $(this).data('id');
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this Size ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "/partial-print-ticket/delete-detail/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'GET',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('#table-list3').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $("#table-list3").unblock();
                    },
                    success: function (response) {
                        console.log(response);
                        $("#table-list3").unblock();
                        myalert('success','Deleted Successfully');
                        table3.ajax.reload();
                    },
                    error: function() {
                        myalert('error','Cannot Delete This Row');
                    }
                });
            }
        });
    });

    var url = $('#prev_data_ticket').attr('href');
    var table3 = $('#table-list3').DataTable({
        pageLength:50,
        ajax: {
            url: url,
        },
        columns: [
            {data: 'season', name: 'season', orderable: true, searchable: true},//1
            {data: 'style', name: 'style', orderable: true, searchable: true},//1
            {data: 'poreference', name: 'poreference', orderable: true, searchable: true},//1
            {data: 'article', name: 'article', orderable: true, searchable: true},//1
            {data: 'size', name: 'size', orderable: true, searchable: true},//1
            {data: 'komponen', name: 'komponen', orderable: true, searchable: true},//1
            {data: 'cut_sticker', name: 'cut_sticker', orderable: true, searchable: true},//1
            {data: 'box_lim', name: 'box_lim', orderable: true, searchable: true},//1
            {data: 'action', name: 'action', orderable: false, searchable: false}//23
        ]
    });

    $('#print3').click(function(){
        event.preventDefault();
        if($('#ticket_box').is(':checked')){
            ticket_box = 1; 
        }else{
            ticket_box = 0; 
        }
        var user = "{{Auth::user()->id}}";
        var factory = "{{Auth::user()->factory_id}}";
        var url_print = '/partial-print-ticket/multiple-print/'+ticket_box+'/'+user+'/'+factory;
        $.ajax({
            url: url_print,
            type: "GET",
            beforeSend: function () {
                $('#table-list3').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $("#table-list3").unblock();
            },
            success: function (response) {
                $('input[name=article]').val('');
                $('input[name=size]').val('');
                $('input[name=part_name]').val('');
                $('input[name=start_num]').val('');
                $('input[name=end_num]').val('');
                $("#table-list3").unblock();
                window.open(url_print,'_blank');
                setTimeout(function(){
                    $('.all_content').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },1000);
                $('#form_data_ticket').unblock();
                setTimeout(function() { sessionDestroy(user); }, 5000);
                // sessionDestroy(user);
            },
            error: function(response) {
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#table-list3').unblock();
            }
        });
    });
});
function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {

        var attr = checkboxes[i].hasAttribute('disabled');

        if (attr === false) {
            checkboxes[i].checked = source.checked;
        }
    }
}
function sessionDestroy(user){
    location.href = "/partial-print-ticket/session-destroy/"+user;
}
</script>
@endsection