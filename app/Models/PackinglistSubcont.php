<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PackinglistSubcont extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'packinglist_subcont';
	protected $fillable = ['no_packinglist','kk_id','no_kk','style','po_buyer','subcont','remark','user_id','factory_id','created_at','updated_at','deleted_at','process','document_type','pabean_no','aju_no','aju_date','user_id_exim','updated_at_exim','status','subcont_id','deleted_by','updated_by','released_by','released_at'];
}
