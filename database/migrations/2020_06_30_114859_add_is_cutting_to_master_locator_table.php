<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCuttingToMasterLocatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_locator', function (Blueprint $table) {
            //
            $table->boolean('is_cutting')->nullable()->default(false);
            $table->boolean('is_setting')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_locator', function (Blueprint $table) {
            //
        });
    }
}
