<div id="revisiModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Spreading Report <span id="revisi_view_barcode"></span></span>
				</legend>
			</div>
            <div class="modal-header">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="headerTable">
						<tbody>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td><span id="revisi_view_style">Data Empty!</span></td>
                                <td><strong>Cut</strong></td>
                                <td><span id="revisi_view_cut">Data Empty!</span></td>
                                <td><strong>Item</strong></td>
                                <td colspan="3"><span id="revisi_view_item_code">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Deliv</strong></td>
                                <td><span id="revisi_view_deliv">Data Empty!</span></td>
                                <td><strong>Marker Width (inch)</strong></td>
                                <td><span id="revisi_view_width">Data Empty!</span></td>
                                <td rowspan="2"><strong>Color</strong></td>
                                <td colspan="3"><span id="revisi_view_color_code">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Article</strong></td>
                                <td><span id="revisi_view_article">Data Empty!</span></td>
                                <td><strong>Marker Length (inch)</strong></td>
                                <td><span id="revisi_view_length">Data Empty!</span></td>
                                <td colspan="3"><span id="revisi_view_color_name">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Marker Pcs</strong></td>
                                <td><span id="revisi_view_marker_pcs">Data Empty!</span></td>
                                <td><strong>Total Length (+allow)</strong></td>
                                <td><span id="revisi_view_allow">Data Empty!</span></td>
                                <td><strong>Part</strong></td>
                                <td><span id="revisi_view_part">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Total Grmt</strong></td>
                                <td><span id="revisi_view_total_garment">Data Empty!</span></td>
                                <td><strong>Layer (Plan)</strong></td>
                                <td><span id="revisi_view_layer">Data Empty!</span> (<span id="revisi_view_layer_plan">Data Empty!</span>)</td>
                                <td><strong>Plan</strong></td>
                                <td><span id="revisi_view_plan">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>PO Buyer</strong></td>
                                <td colspan="2"><span id="revisi_view_list_po">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td colspan="2"><span id="revisi_view_list_ratio">Data Empty!</span></td>
                            </tr>
                        </tbody>
					</table>
				</div>
			</div>
			<div class="modal-body">
                <div class="table-responsive">
					<table class="table table-basic table-condensed" id="fabricRevisiTable">
						<thead>
                            <tr>
                                <th rowspan="2" class="remove-padding-side">No Roll</th>
                                <th rowspan="2" class="remove-padding-side">Lot</th>
                                <th rowspan="2" class="remove-padding-side">Yard In Roll</th>
                                <th rowspan="2" class="remove-padding-side">Lebar Aktual</th>
                                <th colspan="2" class="remove-padding-side"><center>Layer</center></th>
                                <th colspan="2" class="remove-padding-side"><center>Sisa</center></th>
                                <th rowspan="2" class="remove-padding-side">Akumulasi Layer</th>
                                <th rowspan="2" class="remove-padding-side">Reject</th>
                                <th colspan="2" class="remove-padding-side">Sambungan</th>
                                <th rowspan="2" class="remove-padding-side">Aktual Pemakaian</th>
                                <th rowspan="2" class="remove-padding-side">Remark</th>
                                <th rowspan="2" class="remove-padding-side">Catatan</th>
                            </tr>
                            <tr>
                                <th class="remove-padding-side">Suplai</th>
                                <th class="remove-padding-side">Aktual</th>
                                <th class="remove-padding-side">Suplai</th>
                                <th class="remove-padding-side">Aktual</th>
                                <th class="remove-padding-side">Awal</th>
                                <th class="remove-padding-side">Akhir</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
					</table>
				</div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
		</div>
	</div>
</div>