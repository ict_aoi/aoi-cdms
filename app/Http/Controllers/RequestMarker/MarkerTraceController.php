<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use App\Models\CuttingMarker;
use App\Models\User;
use App\Models\ScanCutting;
use App\Models\Spreading\FabricUsed;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Controllers\Controller;

class MarkerTraceController extends Controller
{
    public function index()
    {
        return view('request_marker.marker_trace.index');
    }

    public function scanBarcode(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();

            if($data != null) {
                $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));

                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                $ratio = 0;
                foreach ($data->rasio_markers as $a) {
                    $ratio = $ratio + $a->ratio;
                }
                $total_garment = $ratio * $data->layer;

                $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                $po_buyer = implode(', ', $po_buyer);

                $ratio_size = '';

                foreach ($data->rasio_markers as $aa) {
                    $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                }

                $spreading_check = FabricUsed::where('barcode_marker', $data->barcode_id)->orderBy('created_spreading', 'desc')->get();

                if(count($spreading_check) > 0) {
                    $spreading_status = 'Done';
                    $spreading_date = Carbon::parse($spreading_check->first()->created_at)->format('d M Y / H:i');
                    $spreading_by = User::where('id', $spreading_check->first()->user_id)->first()->name;
                    $spreading_table = DB::table('master_table')->where('id_table', $spreading_check->first()->barcode_table)->first()->table_name;
                } else {
                    $spreading_status = '-';
                    $spreading_date = '-';
                    $spreading_by = '-';
                    $spreading_table = '-';
                }

                $approval_gl_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'GL Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_gl_check != null) {
                    $approve_gl_status = $approval_gl_check->status;
                    $approve_gl_date = Carbon::parse($approval_gl_check->created_at)->format('d M Y / H:i');
                    $approve_gl_by = User::where('id', $approval_gl_check->user_id)->first()->name;
                    $approve_gl_table = '-';
                } else {
                    $approve_gl_status = '-';
                    $approve_gl_date = '-';
                    $approve_gl_by = '-';
                    $approve_gl_table = '-';
                }

                $approval_qc_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'QC Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_qc_check != null) {
                    $approve_qc_status = $approval_qc_check->status;
                    $approve_qc_date = Carbon::parse($approval_qc_check->created_at)->format('d M Y / H:i');
                    $approve_qc_by = User::where('id', $approval_qc_check->user_id)->first()->name;
                    $approve_qc_table = '-';
                } else {
                    $approve_qc_status = '-';
                    $approve_qc_date = '-';
                    $approve_qc_by = '-';
                    $approve_qc_table = '-';
                }

                $cutting_check = ScanCutting::where('barcode_marker', $data->barcode_id)->where('state', 'done')->first();

                if($cutting_check != null) {
                    $get_user_array = $cutting_check->operator->pluck('user_id')->toArray();
                    $operator = User::whereIn('id', $get_user_array)->pluck('name')->toArray();
                    $cutting_status = 'Done';
                    $cutting_date = Carbon::parse($cutting_check->end_time)->format('d M Y / H:i');
                    $cutting_by = implode(', ', $operator);
                    $cutting_table = $cutting_check->table_name;
                } else {
                    $cutting_status = '-';
                    $cutting_date = '-';
                    $cutting_by = '-';
                    $cutting_table = '-';
                }

                $response = [
                    'data' => $data,
                    'layer' => $data->layer_plan.' ('.$data->layer.')',
                    'style' => $data->cutting_plan->style,
                    'article' => $data->cutting_plan->articleno,
                    'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'),
                    'color_name' => substr($get_data_cutting->color_name, 0, 15),
                    'ratio' => $ratio,
                    'total_garment' => $total_garment,
                    'item_code' => $get_data_cutting->material,
                    'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20),
                    'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                    'marker_width' => $data->fabric_width - 0.5,
                    'po_buyer' => $po_buyer,
                    'ratio_size' => $ratio_size,
                    'ket' => null,
                    'spreading_status' => $spreading_status,
                    'approve_gl_status' => $approve_gl_status,
                    'approve_qc_status' => $approve_qc_status,
                    'cutting_status' => $cutting_status,
                    'bundling_status' => '-',
                    'spreading_date' => $spreading_date,
                    'approve_gl_date' => $approve_gl_date,
                    'approve_qc_date' => $approve_qc_date,
                    'cutting_date' => $cutting_date,
                    'bundling_date' => '-',
                    'spreading_by' => $spreading_by,
                    'approve_gl_by' => $approve_gl_by,
                    'approve_qc_by' => $approve_qc_by,
                    'cutting_by' => $cutting_by,
                    'bundling_by' => '-',
                    'spreading_table' => $spreading_table,
                    'cutting_table' => $cutting_table,
                    'bundling_table' => '-',
                ];
                return response()->json($response,200);
            } else {
                return response()->json('marker tidak ditemukan', 422);
            }
        }
    }

    public function POTrace()
    {
        return view('request_marker.po_trace.index');
    }

    // get datatable
    public function dataPO(Request $request)
    {
        $po_buyer = $request->po_buyer;

        if($po_buyer == null){
            $data = array();
        }else{
            $data = DB::table('hs_po_trace')
            ->where('po_buyer', 'like', '%' . $po_buyer . '%')
            ->whereNotNull('size');
        }


        return DataTables::of($data)
        ->addColumn('status',function($data){

            if($data->barcode_id == null){
                $status = 'Marker Belum Dibuat';
                // return '<span class="label bg-grey">'.$status.'</span>';
                return $status;
            }
            else if($data->perimeter == null){
                $status = 'Marker Belum Input Lebar & Perimeter';
                // return '<span class="label bg-danger">'.$status.'</span>';
                return $status;
            }
            else {
                if($data->is_bundling == true){
                    $status = 'Marker Sudah Bundling';
                    // return '<span class="label bg-danger">'.$status.'</span>';
                    return $status;
                }else{
                    if($data->state == 'progress'){
                        $status = 'Marker Proses Cutting';
                        return $status;
                    }
                    else if($data->state == 'done'){
                        $status = 'Marker Sudah Cutting';
                        return $status;
                    }
                    else{
                        if($data->akumulasi_layer == null){
                            $status = 'Marker Belum Spreading';
                            // return '<span class="label bg-orange">'.$status.'</span>';
                            return $status;
                        }
                        else{
                            $status = 'Marker Belum Cutting';
                            return $status;
                        }
                    }
                }
            }
            // if($data->akumulasi_layer == null){
            //     $status = 'Marker Belum Spreading';
            //     // return '<span class="label bg-orange">'.$status.'</span>';
            //     return $status;
            // }
            // else if($data->state == null){
            //     $status = 'Marker Belum Cutting';
            //     // return '<span class="label bg-orange">'.$status.'</span>';
            //     return $status;
            // }
            // else{
            //     if($data->is_bundling == false){
            //         if($data->state == 'progress'){
            //             $status = 'Marker Proses Cutting';
            //             return $status;
            //         }
            //         else if($data->state == 'done'){
            //             $status = 'Marker Sudah Cutting';
            //             return $status;
            //         }
            //         else{
            //             $status = 'Marker Akan Cutting';
            //             return $status;
            //         }
            //     }
            //     else{
            //         $status = 'Marker Sudah Bundling';
            //         return $status;
            //     }
            // }

            return $status;
        })
        ->addColumn('action', function($data) {
            // return '<a href="#" onclick="markerModal(\''.route('poTrace.markerModal',$data->barcode_id).'\')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>';

            return '<a href="#" onclick="markerModal(\''.$data->barcode_id.'\')" class="btn btn-default btn-icon"><i class="icon-file-text"></i></a>';
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public function dataMarker(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();

            if($data != null) {
                $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));

                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                $ratio = 0;
                foreach ($data->rasio_markers as $a) {
                    $ratio = $ratio + $a->ratio;
                }
                $total_garment = $ratio * $data->layer;

                $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                $po_buyer = implode(', ', $po_buyer);

                $ratio_size = '';

                foreach ($data->rasio_markers as $aa) {
                    $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                }

                $spreading_check = FabricUsed::where('barcode_marker', $data->barcode_id)->orderBy('created_spreading', 'desc')->get();

                if(count($spreading_check) > 0) {
                    $spreading_status = 'Done';
                    $spreading_date = Carbon::parse($spreading_check->first()->created_at)->format('d M Y / H:i');
                    $spreading_by = User::where('id', $spreading_check->first()->user_id)->first()->name;
                    $spreading_table = DB::table('master_table')->where('id_table', $spreading_check->first()->barcode_table)->first()->table_name;
                } else {
                    $spreading_status = '-';
                    $spreading_date = '-';
                    $spreading_by = '-';
                    $spreading_table = '-';
                }

                $approval_gl_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'GL Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_gl_check != null) {
                    $approve_gl_status = $approval_gl_check->status;
                    $approve_gl_date = Carbon::parse($approval_gl_check->created_at)->format('d M Y / H:i');
                    $approve_gl_by = User::where('id', $approval_gl_check->user_id)->first()->name;
                    $approve_gl_table = '-';
                } else {
                    $approve_gl_status = '-';
                    $approve_gl_date = '-';
                    $approve_gl_by = '-';
                    $approve_gl_table = '-';
                }

                $approval_qc_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'QC Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_qc_check != null) {
                    $approve_qc_status = $approval_qc_check->status;
                    $approve_qc_date = Carbon::parse($approval_qc_check->created_at)->format('d M Y / H:i');
                    $approve_qc_by = User::where('id', $approval_qc_check->user_id)->first()->name;
                    $approve_qc_table = '-';
                } else {
                    $approve_qc_status = '-';
                    $approve_qc_date = '-';
                    $approve_qc_by = '-';
                    $approve_qc_table = '-';
                }

                $cutting_check = ScanCutting::where('barcode_marker', $data->barcode_id)->where('state', 'done')->first();

                if($cutting_check != null) {
                    $get_user_array = $cutting_check->operator->pluck('user_id')->toArray();
                    $operator = User::whereIn('id', $get_user_array)->pluck('name')->toArray();
                    $cutting_status = 'Done';
                    $cutting_date = Carbon::parse($cutting_check->end_time)->format('d M Y / H:i');
                    $cutting_by = implode(', ', $operator);
                    $cutting_table = $cutting_check->table_name;
                } else {
                    $cutting_status = '-';
                    $cutting_date = '-';
                    $cutting_by = '-';
                    $cutting_table = '-';
                }

                $response = [
                    'data' => $data,
                    'layer' => $data->layer_plan.' ('.$data->layer.')',
                    'style' => $data->cutting_plan->style,
                    'article' => $data->cutting_plan->articleno,
                    'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'),
                    'color_name' => substr($get_data_cutting->color_name, 0, 15),
                    'ratio' => $ratio,
                    'total_garment' => $total_garment,
                    'item_code' => $get_data_cutting->material,
                    'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20),
                    'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                    'marker_width' => $data->fabric_width - 0.5,
                    'po_buyer' => $po_buyer,
                    'ratio_size' => $ratio_size,
                    'ket' => null,
                    'spreading_status' => $spreading_status,
                    'approve_gl_status' => $approve_gl_status,
                    'approve_qc_status' => $approve_qc_status,
                    'cutting_status' => $cutting_status,
                    'bundling_status' => '-',
                    'spreading_date' => $spreading_date,
                    'approve_gl_date' => $approve_gl_date,
                    'approve_qc_date' => $approve_qc_date,
                    'cutting_date' => $cutting_date,
                    'bundling_date' => '-',
                    'spreading_by' => $spreading_by,
                    'approve_gl_by' => $approve_gl_by,
                    'approve_qc_by' => $approve_qc_by,
                    'cutting_by' => $cutting_by,
                    'bundling_by' => '-',
                    'spreading_table' => $spreading_table,
                    'cutting_table' => $cutting_table,
                    'bundling_table' => '-',
                ];
                return response()->json($response,200);
            } else {
                return response()->json('marker tidak ditemukan', 422);
            }
        }
    }


    public function downloadGenerate(Request $request)
    {
        if(request()->ajax())
        {
            $po_buyer = $request->po_buyer;
            if($po_buyer != null && $po_buyer != '') {
                $obj = new StdClass();
                $obj->download_link = route('poTrace.download', $po_buyer);
                return response()->json($obj,200);
            } else {
                return response()->json('Masukkan PO Buyer Terlebih Dahulu',422);
            }
        }
    }

    public function download(Request $request, $po)
    {
        $po_buyer = $request->po;

        if($po_buyer != null && $po_buyer != '') {

            $data = DB::table('hs_po_trace')
            ->where('po_buyer', 'like', '%' . $po_buyer . '%')
            ->whereNotNull('size');

            if(count($data->get()) < 1)
            {
                return response()->json('Data tidak ditemukan.', 422);
            } else {
                $data     = $data->get();
                $filename = 'Report_Monitoring_PO_'.$po_buyer;

                $i = 1;

                $export = \Excel::create($filename, function($excel) use ($data, $i, $po_buyer) {

                    $excel->sheet('report', function($sheet) use ($data, $i, $po_buyer) {

                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                            'H' => '@',
                            'I' => '0',
                            'J' => '0',
                            'K' => '0',
                            'L' => '0',
                            'M' => 'General',
                            'N' => 'General',
                            'O' => '@',
                            'P' => '@',
                            'Q' => '@',
                        ));

                        $sheet->setCellValue('A1', 'CUTTING DATE');
                        $sheet->setCellValue('B1', 'QUEUE');
                        $sheet->setCellValue('C1', 'BARCODE MARKER');
                        $sheet->setCellValue('D1', 'RATIO');
                        $sheet->setCellValue('E1', 'PO BUYER');
                        $sheet->setCellValue('F1', 'SIZE');
                        $sheet->setCellValue('G1', 'PART NO');
                        $sheet->setCellValue('H1', 'LAYER ACTUAL');
                        $sheet->setCellValue('I1', 'LAYER PLAN');
                        $sheet->setCellValue('J1', 'PERIMETER');
                        $sheet->setCellValue('K1', 'AKUMULASI LAYER');
                        $sheet->setCellValue('L1', 'PERIMETER');
                        $sheet->setCellValue('M1', 'CUTTING');
                        $sheet->setCellValue('N1', 'BUNDLING');
                        $sheet->setCellValue('O1', 'TGL CETAK');
                        $sheet->setCellValue('P1', 'TGL SELESAI SPREADING');
                        $sheet->setCellValue('Q1', 'TGL MULAI CUTTING');

                        $index = 0;
                        foreach ($data as $value) {
                            $row = $index + 2;
                            $sheet->setCellValue('A'.$row, $value->cutting_date);
                            $sheet->setCellValue('B'.$row, $value->queu);
                            $sheet->setCellValue('C'.$row, $value->barcode_id);
                            $sheet->setCellValue('D'.$row, $value->ratio_print);
                            $sheet->setCellValue('E'.$row, $value->po_buyer);
                            $sheet->setCellValue('F'.$row, $value->size);
                            $sheet->setCellValue('G'.$row, $value->part_no);
                            $sheet->setCellValue('H'.$row, $value->layer);
                            $sheet->setCellValue('I'.$row, $value->layer_plan);
                            $sheet->setCellValue('J'.$row, $value->perimeter);
                            $sheet->setCellValue('K'.$row, $value->akumulasi_layer);
                            $sheet->setCellValue('L'.$row, $value->perimeter);
                            $sheet->setCellValue('M'.$row, '=IF(EXACT("'.$value->state.'","progress"), "Sedang Cutting", IF(EXACT("'.$value->state.'","done"), "Sudah Cutting", "Belum Cutting"))');
                            // $sheet->setCellValue('M'.$row, $value->state);
                            // $char = '=IF('.$value->is_bundling.'>0, "Sudah Bundling", "Belum Bundling")';
                            $sheet->setCellValue('N'.$row, $value->is_bundling);
                            // $sheet->setCellValue('N'.$row, $value->is_bundling);
                            $sheet->setCellValue('O'.$row, $value->download_at);
                            $sheet->setCellValue('P'.$row, $value->cs);
                            $sheet->setCellValue('Q'.$row, $value->start_time);
                            $index++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->download('xlsx');


                return response()->json(200);
            }
        } else {
            return response()->json('Select tanggal cutting terlebih dahulu!.', 422);
        }
    }

    public function getReviewspreading($id)
    {
        $data = DB::table('ns_spreading_approval')
            ->where('barcode_id', $barcode_id)
            ->first();

        $get_marker = CuttingMarker::where('barcode_id', $data->barcode_id)
            ->where('deleted_at', null)
            ->first();


        $list_po = implode(', ', MarkerDetailPO::select('po_buyer')->where('barcode_id', $id)->groupBy('po_buyer')->pluck('po_buyer')->toArray());
        // $list_po = implode(', ', $get_marker->cutting_plan->po_details->pluck('po_buyer')->toArray());
        $list_ratio = array();
        foreach($get_marker->rasio_markers as $rsio) {
            $list_ratio[] = $rsio->size.'/'.$rsio->ratio;
        }
        $list_ratio = implode(', ', $list_ratio);
        $part_no = explode('+', $data->part_no);
        $data1 = DB::table('jaz_detail_size_per_part')
            ->where('cutting_date', $data->cutting_date)
            ->where('queu', $data->queu)
            ->whereIn('part_no', $part_no)
            ->where('factory_id', \Auth::user()->factory_id)
            ->whereNotNull('deliv_date')
            ->first();

        $obj = new StdClass();
        $obj->barcode_id = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style = $data->style;
        $obj->articleno = $data->articleno;
        $obj->size_category = $data->size_category;
        $obj->top_bot = $data->top_bot;
        $obj->season = $data->season;
        $obj->part_no = $data->part_no;
        $obj->cut = $data->cut;
        $obj->layer = $data->layer;
        $obj->layer_plan = $data->layer_plan;
        $obj->color_name = $data1->color_name;
        $obj->color_code = $data1->color_code;
        $obj->deliv_date = $data1->deliv_date;
        $obj->item_code = $data1->item_code;
        $obj->fabric_width = $data->fabric_width;
        $obj->marker_length = $data->marker_length;
        $obj->qty_ratio = $data->qty_ratio;
        $obj->fabric_width = $data->fabric_width;
        $obj->list_ratio = $list_ratio;
        $obj->list_po = $list_po;
        $obj->data_detail = FabricUsed::where('barcode_marker', $id)->orderBy('created_spreading', 'asc')->get();

		return response()->json($obj,200);
    }

}
