@extends('layouts.app',['active' => 'masterListStyle'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master List Style </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">List Style</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add List Style <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list" style="width:100%;">
                    <thead>
                        <tr>
                            <th style="width:5px;">#</th>
                            <th>SEASON</th>
                            <th style="min-width:170px;">STYLE STANDART</th>
                            <th>STYLE</th>
                            <th>TYPE</th>
                            <th>CATEGORY PRODUCT</th>
                            <th>BISNIS UNIT</th>
                            <th>MAIN MATERIAL</th>
                            <th>DESC MATERIAL</th>
                            <th>CATEGORY FABRIC</th>
                            <th>FABRIC GROUP</th>
                            <th>TINGKAT KESULITAN</th>
                            <th>MP</th>
                            <th>TOTAL PROCESS</th>
                            <th>STATUS TYPE</th>
                            <th>TSME</th>
                            <th>TSLC</th>
                            <th>STRIPES</th>
                            <th>AD</th>
                            <th>STATUS AD</th>
                            <th>Engineering Fabric</th>
                            <th>aSC Priority</th>
                            <th>Min Diameter Hemmning</th>
                            <th>Max Diameter Hemmning</th>
                            <th>Critical Process</th>
                            <th>Update SPT Input</th>
    	                    <th style="width:5px;">ACTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('masterparam.ajaxGetDataListStyle') }}" id="get_data"></a>
@endsection

@section('page-modal')
    @include('master_parameter.style.listing._add_modal')
    @include('master_parameter.style.listing._update_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/fixed_columns.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
        //datatables
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            autoLength: false,
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });
    
        var url = $('#get_data').attr('href');
        var table = $('#table-list').DataTable({
            searching: true, 
            paging: false, 
            scrollY:        "300px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 5
            },
            sDom: 'tF',
            ajax: url,
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false  },
                {data: 'season_name', name: 'season_name'},
                {data: 'style_standart', name: 'style_standart'},
                {data: 'set_type', name: 'set_type'},
                {data: 'product_type', name: 'product_type'},
                {data: 'product_category', name: 'product_category'},
                {data: 'bisnis_unit', name: 'bisnis_unit'},
                {data: 'main_material', name: 'main_material'},
                {data: 'desc_material', name: 'desc_material'},
                {data: 'fabric_category', name: 'fabric_category'},
                {data: 'fabric_group', name: 'fabric_group'},
                {data: 'difficulty_level', name: 'difficulty_level'},
                {data: 'mp', name: 'mp'},
                {data: 'total', name: 'total'},
                {data: 'status_style', name: 'status_style'},
                {data: 'tsme', name: 'tsme'},
                {data: 'tslc', name: 'tslc'},
                {data: 'stripes', name: 'stripes'},
                {data: 'ad', name: 'ad'},
                {data: 'status_ad', name: 'status_ad'},
                {data: 'engineering_fabric', name: 'engineering_fabric'},
                {data: 'asc_priority', name: 'asc_priority'},
                {data: 'min_diameter_hemning', name: 'min_diameter_hemning'},
                {data: 'max_diameter_hemning', name: 'max_diameter_hemning'},
                {data: 'critical_process', name: 'critical_process'},
                {data: 'update_spt_input', name: 'update_spt_input'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
        //end of datatables
    
        //add
        $('.add_new').on('click',function(){
            $('#modal_add_new_').modal('show')
        });
        //end of add

        $('#style_standart').on('change', function(){
            if($(this).val() != null){
                $('#style').val($(this).val());
            }else{
                $('#style').val('');
            }
        });

           $('#modal_add_new_').on('hidden.bs.modal', function(){
               $('#form-add').trigger("reset");

               $('#style_standart').val('').trigger('change');
               $('#season').val('').trigger('change');
               $('#product_type').val('').trigger('change');
               $('#product_category').val('').trigger('change');
               $('#bisnis_unit').val('').trigger('change');
               $('#main_material').val('').trigger('change');
               $('#desc_material').val('').trigger('change');
               $('#fabric_category').val('').trigger('change');
               $('#fabric_group').val('').trigger('change');
               $('#difficulty_level').val('').trigger('change');
               $('#status_style').val('').trigger('change');
               $('#set_type').val('').trigger('change');
               $('#stripes').val('').trigger('change');
               $('#status_ad').val('').trigger('change');
               $('#engineering_fabric').val('').trigger('change');
               $('#asc_priority').val('').trigger('change');
        
               //table.draw();
           });

           $('#modal_edit_').on('hidden.bs.modal', function(){
               $('#form-update').trigger("reset");

               $('#style_standart_update').val('').trigger('change');
               $('#season_update').val('').trigger('change');
               $('#product_type_update').val('').trigger('change');
               $('#product_category_update').val('').trigger('change');
               $('#bisnis_unit_update').val('').trigger('change');
               $('#main_material_update').val('').trigger('change');
               $('#desc_material_update').val('').trigger('change');
               $('#fabric_category_update').val('').trigger('change');
               $('#fabric_group_update').val('').trigger('change');
               $('#difficulty_level_update').val('').trigger('change');
               $('#status_style_update').val('').trigger('change');
               $('#set_type_update').val('').trigger('change');
               $('#stripes_update').val('').trigger('change');
               $('#status_ad_update').val('').trigger('change');
               $('#engineering_fabric_update').val('').trigger('change');
               $('#asc_priority_update').val('').trigger('change');
        
               //table.draw();
           });
    
        //delete
        $("#table-list").on("click", ".delete", function() {
            event.preventDefault();
            var id = $(this).data('id');
            if(id == 'kosong') {
                return false;
            }
            var token = $(this).data("token");
            bootbox.confirm("Are you sure delete this row ?", function (result) {
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $.ajax({
                        url: "master-list-style/delete/"+id,
                        type: "GET",
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        beforeSend: function () {
                            $('.loader-area').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        },
                        complete: function () {
                            $(".loader-area").unblock();
                        },
                        success: function () {
                            myalert('success','Data has been deleted');
                            table.ajax.reload();
                        },
                        error: function(response) {
                            myalert('error','NOT GOOD');
                        }
                    });
                }
            });
        });
        //end of delete
    
        //add new
        $('#form-add').submit(function(event) {
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type : "POST",
                url : $('#form-add').attr('action'),
                data: $('#form-add').serialize(),
                beforeSend: function () {
                    $('.loader-area').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                complete: function () {
                    $('.loader-area').unblock();
                },
                success: function(response) {
                    myalert('success','GOOD');
                    $('#form-add').trigger("reset");
                    //$('#modal_add_new_').trigger('toggle');
                    $('#modal_add_new_').modal('hide');
                    table.ajax.reload();
                },
                error: function(response) {
                    myalert('error','NOT GOOD');
                }
            })
        });

        //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_update', 'style_standart_update', 'style_update', 'season_update', 'product_type_update', 'product_category_update', 'bisnis_unit_update', 'main_material_update', 'desc_material_update', 'fabric_category_update', 'fabric_group_update', 'difficulty_level_update', 'status_style_update', 'mp_update', 'total_update', 'tsme_update', 'tslc_update', 'set_type_update', 'stripes_update', 'ad_update', 'status_ad_update', 'engineering_fabric_update', 'asc_priority_update', 'min_diameter_hemning_update', 'max_diameter_hemning_update', 'critical_process_update', 'update_spt_input_update'];
        var val = [
            $('#id_update').val(), 
            $('#style_standart_update').val(),
            $('#style_update').val(),
            $('#season_update').val(),
            $('#product_type_update').val(),
            $('#product_category_update').val(),
            $('#bisnis_unit_update').val(),
            $('#main_material_update').val(),
            $('#desc_material_update').val(),
            $('#fabric_category_update').val(),
            $('#fabric_group_update').val(),
            $('#difficulty_level_update').val(),
            $('#status_style_update').val(),
            $('#mp_update').val(),
            $('#total_update').val(),
            $('#tsme_update').val(),
            $('#tslc_update').val(),
            $('#set_type_update').val(),
            $('#stripes_update').val(),
            $('#ad_update').val(),
            $('#status_ad_update').val(),
            $('#engineering_fabric_update').val(),
            $('#asc_priority_update').val(),
            $('#min_diameter_hemning_update').val(),
            $('#max_diameter_hemning_update').val(),
            $('#critical_process_update').val(),
            $('#update_spt_input_update').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
                table.ajax.reload();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

});

function edit(url){

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {
        var id = response.id;
        var tsme = response.tsme;
        var tslc = response.tslc;
        var ad = response.ad;
        var min_diameter_hemning = response.min_diameter_hemning;
        var max_diameter_hemning = response.max_diameter_hemning;
        var update_spt_input = response.update_spt_input;

        tsme = tsme ? tsme.replace('.',',') : null;
        tslc = tslc ? tslc.replace('.',',') : null;
        min_diameter_hemning = min_diameter_hemning ? min_diameter_hemning.replace('.',',') : null;
        max_diameter_hemning = max_diameter_hemning ? max_diameter_hemning.replace('.',',') : null;

        ad = ad ? moment(ad).format('DD MMMM, YYYY') : null;
        update_spt_input = update_spt_input ? moment(update_spt_input).format('DD MMMM, YYYY') : null;

        var stripes = response.stripes;

        $('#id_update').val(id);
        $('#style_standart_update').val(response.style_standart).trigger('change');
        $('#style_update').val(response.style);
        $('#season_update').val(response.season).trigger('change');
        $('#product_type_update').val(response.product_type).trigger('change');
        $('#product_category_update').val(response.product_category).trigger('change');
        $('#bisnis_unit_update').val(response.bisnis_unit).trigger('change');
        $('#main_material_update').val(response.main_material).trigger('change');
        $('#desc_material_update').val(response.desc_material);
        $('#fabric_category_update').val(response.fabric_category).trigger('change');
        $('#fabric_group_update').val(response.fabric_group).trigger('change');
        $('#difficulty_level_update').val(response.difficulty_level).trigger('change');
        $('#status_style_update').val(response.status_style).trigger('change');
        $('#mp_update').val(response.mp);
        $('#total_update').val(response.total);
        $('#tsme_update').val(tsme);
        $('#tslc_update').val(tslc);
        $('#min_diameter_hemning_update').val(min_diameter_hemning);
        $('#max_diameter_hemning_update').val(max_diameter_hemning);
        $('#critical_process_update').val(response.critical_process);
        $('#set_type_update').val(response.set_type).trigger('change');
       // $('#stripes_update').val(response.stripes).trigger('change');
       if(stripes != null){
            $.each(stripes.split(","), function(i, v){
                $('#stripes_update option[value='+ v +']').prop('selected', true).trigger('change');
            });
        }else{
            $('#stripes_update').val(null).trigger('change');
        }

        $('#ad_update').val(ad).trigger('change');
        $('#update_spt_input_update').val(update_spt_input).trigger('change');
        $('#status_ad_update').val(response.status_ad).trigger('change');
        $('#engineering_fabric_update').val(response.engineering_fabric).trigger('change');
        $('#asc_priority_update').val(response.asc_priority).trigger('change');

        $('#modal_edit_').modal();

    });
}

</script>
@endsection




