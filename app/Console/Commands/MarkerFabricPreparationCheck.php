<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use App\Http\Controllers\RequestMarker\ReleaseMarkerController;

use App\Models\Scheduler;

class MarkerFabricPreparationCheck extends Command
{
    protected $signature = 'markerFabricPrepareCheck:insert';
    protected $description = 'check balance marker';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','CHECK_FABRIC_PREPARE_90%')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('CHECK FABRIC PREPARE AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            ReleaseMarkerController::checkFabricPrepare();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE CHECK FABRIC PREPARE AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','CHECK_FABRIC_PREPARE_90')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'CHECK_FABRIC_PREPARE_90',
                    'status' => 'ongoing'
                ]);
                $this->info('CHECK FABRIC PREPARE AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ReleaseMarkerController::checkFabricPrepare();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler);
                $this->info('DONE CHECK FABRIC PREPARE AT '.carbon::now());
            }else{
                $this->info('CHECK FABRIC PREPARE SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
