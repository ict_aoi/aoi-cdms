@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Print ID Marker</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Print ID Marker</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Input Cutting Date</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Enter Cutting Date" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Select</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
                        <th>Queue</th>
                        <th>Cutting Date</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO Buyer</th>
                        <th>Size Type</th>
                        <th>Status</th>
                        <th>Last Updated By</th>
                        <th>Detail</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<form action="{{ route('printIDMarker.printIdPdf') }}" method="post" target="_blank" id="get_barcode">
	@csrf
	<input id="list_id_marker" name="list_id_marker" type="hidden" value="[]">
	<input id="print_type_" name="print_type_" type="hidden">
	<div class="" style="">
		<button type="submit" class="btn hidden">Print</button>
	</div>
</form>

@endsection

@section('page-modal')
	@include('request_marker.print_id_marker._detail_modal')
	@include('request_marker.print_id_marker._detail_modal_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/print_id_marker.js') }}"></script>
@endsection
