<?php

namespace App\Http\Controllers\Spreading;

use DB;
use App\Models\CuttingMarker;
use App\Models\Spreading\SpreadingReportTemp;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Spreading\FabricUsed;
use App\Models\TableSpreading;
use App\Models\SpreadingApproval;
use App\Models\CuttingPlan;
use App\Models\SpreadingQc;
use App\Models\Data\DataCutting;
use App\Models\MarkerSpreading;
use App\Models\RasioMarker;
use App\Models\MarkerDetailPO;
use App\Models\CuttingMovement;
use App\Models\MasterDowntimeCategory;
use App\Models\LogSystem;
use App\Models\RequestFabric\ReportRoll;
use Auth;


class ActualSpreadingController extends Controller
{
    public function index()
    {
        $downtime_categorys = MasterDowntimeCategory::where('deleted_at', null)->get();
        return view('spreading.actual_marker.index', compact('downtime_categorys'));
    }

    public function dataCuttingTable()
    {
        if (request()->ajax()) {
            $data = DB::table('master_table')->where('deleted_at', null)->orderBy('id_table', 'asc');

            return datatables()->of($data)
                ->addColumn('action', function ($data) {
                    return '<button class="btn btn-primary btn-xs" data-id="' . $data->id_table . '" data-barcode="' . $data->id_table . '" data-name="' . $data->table_name . '" id="select_cutting_table">Select</button>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function dataSpreadingReportTemp(Request $request)
    {
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;

            if ($barcode_marker != null) {
                $data = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->orderBy('created_at', 'asc');

                return datatables()->of($data)
                    ->editColumn('id', function ($data) {
                        return '<div class="form-group remove-margin"><input type="text" class="form-control" name="id" id="id" value="' . $data->id . '" readonly /></div>';
                    })
                    ->editColumn('qty_fabric', function ($data) {
                        return round($data->qty_fabric, 2);
                    })
                    ->editColumn('actual_width', function ($data) {
                        if ($data->is_commit == 't') {
                            return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="actual_width" onkeypress="isNumberDot(event)" id="actual_width" value="' . $data->actual_width . '" readonly /></div>';
                        } else {
                            return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="actual_width" onkeypress="isNumberDot(event)" id="actual_width" value="' . $data->actual_width . '" /></div>';
                        }
                    })
                    ->editColumn('actual_layer', function ($data) {
                        if($data->actual_layer == null) {
                            if($data->sambungan == null) {
                                return '<center>0</center>';
                            } else {
                                if ($data->is_commit == 't') {
                                    return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;"><input type="text" class="form-control" name="actual_layer" onkeypress="isNumberDot(event)" id="actual_layer" value="' . $data->actual_layer . '" readonly /></div>';
                                } else {
                                    return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;"><input type="text" class="form-control" name="actual_layer" onkeypress="isNumberDot(event)" id="actual_layer" value="' . $data->actual_layer . '" /></div>';
                                }
                            }
                        } else {
                            return '<center>'.$data->actual_layer.'</center>';
                        }
                    })
                    ->editColumn('suplai_sisa', function ($data) {
                        return round($data->suplai_sisa, 2);
                    })
                    ->editColumn('actual_sisa', function ($data) {
                        if($data->reject == null) {
                            return '<center>'.$data->actual_sisa.'</center>';
                        } else {
                            if ($data->is_commit == 't') {
                                return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="actual_sisa" onkeypress="isNumberDot(event)" id="actual_sisa" value="' . $data->actual_sisa . '" readonly /></div>';
                            } else {
                                if($data->actual_sisa > 10) {
                                    return '<center>'.round($data->actual_sisa, 2).'</center>';
                                } else {
                                    return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="actual_sisa" onkeypress="isNumberDot(event)" id="actual_sisa" value="'.$data->actual_sisa.'" /></div>';
                                }
                            }
                        }
                    })
                    ->editColumn('akumulasi_layer', function ($data) {
                        return '<span id="akumulasi_layer">' . $data->akumulasi_layer . '</span>';
                    })
                    ->editColumn('reject', function ($data) {
                        if($data->reject == null) {
                            if($data->sambungan_end == null) {
                                return '<center>0</center>';
                            } else {
                                if ($data->is_commit == 't') {
                                    return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;" ><input type="text" class="form-control" name="reject" onkeypress="isNumberDot(event)" id="reject" value="' . $data->reject . '" readonly /></div>';
                                } else {
                                    return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;" ><input type="text" class="form-control" name="reject" onkeypress="isNumberDot(event)" id="reject" value="' . $data->reject . '" /></div>';
                                }
                            }
                        } else {
                            return '<center>'.$data->reject.'</center>';
                        }
                    })
                    ->editColumn('sambungan', function ($data) {
                        if($data->sambungan == null) {
                            if ($data->is_commit == 't') {
                                return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;"><input type="text" class="form-control" name="sambungan" onkeypress="isNumberDot(event)" id="sambungan" value="' . $data->sambungan . '" readonly /></div>';
                            } else {
                                return '<div class="form-group bg-info remove-margin" style="margin-right: 5px !important;"><input type="text" class="form-control" name="sambungan" onkeypress="isNumberDot(event)" id="sambungan" value="' . $data->sambungan . '" /></div>';
                            }
                        } else {
                            return '<center>'.$data->sambungan.'</center>';
                        }
                    })
                    ->editColumn('sambungan_end', function ($data) {
                        if($data->sambungan_end == null) {
                            if($data->actual_layer == null) {
                                return '<center>0</center>';
                            } else {
                                if ($data->is_commit == 't') {
                                    return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="sambungan_end" onkeypress="isNumberDot(event)" id="sambungan_end" value="' . $data->sambungan_end . '" readonly /></div>';
                                } else {
                                    return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="sambungan_end" onkeypress="isNumberDot(event)" id="sambungan_end" value="' . $data->sambungan_end . '" /></div>';
                                }
                            }
                        } else {
                            return '<center>'.$data->sambungan_end.'</center>';
                        }
                    })
                    ->editColumn('actual_use', function ($data) {
                        return '<span id="actual_use">' . round($data->actual_use, 2) . '</span>';
                    })
                    ->editColumn('remark', function ($data) {
                        return '<span id="remark">' . round($data->remark, 2) . '</span>';
                    })
                    ->editColumn('kualitas_gelaran', function ($data) {
                        if ($data->is_commit == 't') {
                            return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="kualitas_gelaran" id="kualitas_gelaran" value="' . $data->kualitas_gelaran . '" readonly /></div>';
                        } else {
                            return '<div class="form-group bg-info remove-margin"><input type="text" class="form-control" name="kualitas_gelaran" id="kualitas_gelaran" value="' . $data->kualitas_gelaran . '" /></div>';
                        }
                    })
                    ->addColumn('saved', function ($data) {
                        if ($data->is_commit == 't') {
                            return '<span class="label label-success"><i class="icon-checkmark"></i></span>';
                        } else {
                            return '<span class="label label-danger"><i class="icon-cross2"></i></span>';
                        }
                    })
                    ->rawColumns(['id', 'actual_width', 'actual_layer', 'actual_sisa', 'reject', 'sambungan', 'kualitas_gelaran', 'akumulasi_layer', 'actual_use', 'remark', 'saved', 'sambungan_end'])
                    ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('saved', function ($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function checkTableId(Request $request)
    {
        if (request()->ajax()) {
            $barcode_id = $request->table_check;
            if ($barcode_id != null) {
                $data = DB::table('master_table')->where('id_table', $barcode_id)->get()->first();

                if ($data != null) {
                    return response()->json(['status' => 200, 'data' => $data]);
                } else {
                    return response()->json(['status' => 442, 'data' => null]);
                }
            }
        }
    }

    // case normal
    public function checkMarkerId(Request $request)
    {
        if (request()->ajax()) {
            $barcode_id = $request->marker_check;
            if ($barcode_id != null) {

                $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();

                if ($data != null) {

                    if($data->cutting_plan->deleted_at == null){

                        $marker_check = FabricUsed::where('barcode_marker', $barcode_id)->get();

                        if (count($marker_check) > 0) {

                            return response()->json('Marker sudah digunakan!', 422);
                        } else {

                            // $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));
                            $part_no = explode('+', $data->part_no);

                            $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                            if($get_data_cutting == null){
                                return response()->json('CSI Tidak Ditemukan', 422);
                            }

                            $ratio = 0;
                            foreach ($data->rasio_markers as $a) {
                                $ratio = $ratio + $a->ratio;
                            }
                            $total_garment = $ratio * $data->layer;

                            $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                            $po_buyer = implode(', ', $po_buyer);

                            $ratio_size = '';

                            foreach ($data->rasio_markers as $aa) {
                                $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                            }

                            return response()->json(['status' => 200, 'data' => $data, 'style' => $data->cutting_plan->style, 'article' => $data->cutting_plan->articleno, 'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'), 'color_name' => substr($get_data_cutting->color_name, 0, 15), 'ratio' => $ratio, 'total_garment' => $total_garment, 'item_code' => $get_data_cutting->material, 'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20), 'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'), 'marker_width' => $data->fabric_width - 0.5, 'po_buyer' => $po_buyer, 'ratio_size' => $ratio_size, 'ket' => null]);
                        }
                    } else {
                        return response()->json('Planning sudah dihapus!', 422);
                    }

                } else {

                    return response()->json('Marker tidak ditemukan!', 422);
                }
            }
        }
    }

    // case tumpuk
    public function checkMarkerIdTumpuk(Request $request)
    {
        if (request()->ajax()) {
            $barcode_id = $request->marker_check;
            if ($barcode_id != null) {

                $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();

                if ($data != null) {

                    if($data->cutting_plan->deleted_at == null){

                        $marker_check = FabricUsed::where('barcode_marker', $barcode_id)->get();

                        if (count($marker_check) > 0) {
                            return response()->json('Marker sudah digunakan!', 422);
                        } else {

                            $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                            $po_buyer = implode(', ', $po_buyer);

                            $response = [
                                'barcode_id'   => $barcode_id,
                                'cutting_date' => $data->cutting_plan->cutting_date,
                                'style'        => $data->cutting_plan->style,
                                'article'      => $data->cutting_plan->articleno,
                                'po_buyer'     => $po_buyer,
                                'fabric_width' => $data->fabric_width,
                                'cut'          => $data->cut,
                            ];

                            return response()->json($response, 200);
                        }
                    } else {
                        return response()->json('Planning sudah dihapus!', 422);
                    }
                } else {
                    return response()->json('Marker tidak ditemukan!', 422);
                }
            }
        }
    }

    public function checkMarkerIdTumpukSubmit(Request $request)
    {
        if (request()->ajax()) {
            $barcode_ids = $request->barcode_ids;
            if ($barcode_ids != null) {

                $first_barcode = $barcode_ids[0];

                // validation
                $style_check   = '';
                $part_no_check = '';
                $ratio_check   = '';
                $id_plan_check = '';
                foreach ($barcode_ids as $key => $barcode_id) {
                    $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();
                    $get_ratio = '';
                    foreach ($data_marker->rasio_markers as $ratio) {
                        $get_ratio .= $ratio->size . '/' . $ratio->ratio . ', ';
                    }
                    if ($key != 0) {
                        if ($style_check != $data_marker->cutting_plan->style) {
                            return response()->json('Marker tidak dapat ditumpuk karena style berbeda!', 422);
                        }

                        if ($part_no_check != $data_marker->part_no) {
                            return response()->json('Marker tidak dapat ditumpuk karena no part berbeda!', 422);
                        }
                        if ($ratio_check != $get_ratio) {
                            return response()->json('Marker tidak dapat ditumpuk karena rasio berbeda!', 422);
                        }
                        if ($id_plan_check != $data_marker->id_plan) {
                            return response()->json('Marker tidak dapat ditumpuk karena permintan berbeda!', 422);
                        }
                    }
                    $ratio_check   = $get_ratio;
                    $style_check   = $data_marker->cutting_plan->style;
                    $part_no_check = $data_marker->part_no;
                    $id_plan_check = $data_marker->id_plan;
                }

                // update db
                try {
                    DB::beginTransaction();

                    $new_layer = 0;
                    foreach ($barcode_ids as $key => $barcode_id) {
                        $data_marker = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();
                        $new_layer = $data_marker->layer + $new_layer;
                    }

                    $old_marker = implode(',', $barcode_ids);

                    $detail_first_marker = CuttingMarker::where('barcode_id', $first_barcode)->where('deleted_at', null)->first();

                    $update_marker = CuttingMarker::where('barcode_id', $first_barcode)->where('deleted_at', null)->first();
                    $need_total    = ($update_marker->marker_length + 0.75) * $new_layer / 36;
                    $update_marker = CuttingMarker::where('barcode_id', $first_barcode)->where('deleted_at', null)->update(['need_total' => $need_total,'layer' => $new_layer,'old_marker' => $old_marker]);

                    unset($barcode_ids[0]);

                    CuttingMarker::whereIn('barcode_id', $barcode_ids)->where('deleted_at', null)->update(['deleted_at' => Carbon::now(), 'desc' => 'TUMPUK MARKER']);

                    $old_marker_array = $barcode_ids;

                    RasioMarker::whereIn('id_marker', $barcode_ids)->where('deleted_at', null)->update(['deleted_at' => Carbon::now()]);
                    $get_ratio_detail_first = RasioMarker::where('id_marker', $first_barcode)->where('deleted_at', null)->get();

                    $barcode_ids[0] = $first_barcode;

                    $data_input = array();

                    foreach ($get_ratio_detail_first as $data) {
                        $get_new_alocation = MarkerDetailPO::select(DB::raw("po_buyer, size, part_no, sum(qty) as qty, is_sisa"))
                        ->whereIn('barcode_id', $barcode_ids)
                        ->where('size', $data->size)
                        ->where('deleted_at', null)
                        ->groupBy('po_buyer', 'size', 'part_no', 'is_sisa')->get();

                        foreach ($get_new_alocation as $alocation) {
                            $data_input[] = [
                                'barcode_id' => $first_barcode,
                                'po_buyer'   => $alocation->po_buyer,
                                'qty'        => $alocation->qty,
                                'plan_id'    => $detail_first_marker->id_plan,
                                'ratio_id'   => $data->id,
                                'size'       => $alocation->size,
                                'is_sisa'    => $alocation->is_sisa,
                                'part_no'    => $alocation->part_no,
                            ];
                        }
                    }

                    MarkerDetailPO::whereIn('barcode_id', $barcode_ids)->where('deleted_at', null)->update(['deleted_at' => Carbon::now()]);
                    MarkerDetailPO::where('barcode_id', $first_barcode)->where('deleted_at', null)->update(['deleted_at' => Carbon::now()]);

                    foreach ($data_input as $input) {
                        MarkerDetailPO::FirstOrCreate([
                            'barcode_id' => $input['barcode_id'],
                            'po_buyer'   => $input['po_buyer'],
                            'qty'        => $input['qty'],
                            'plan_id'    => $input['plan_id'],
                            'ratio_id'   => $input['ratio_id'],
                            'size'       => $input['size'],
                            'is_sisa'    => $input['is_sisa'],
                            'part_no'    => $input['part_no'],
                            'deleted_at' => null,
                        ]);
                    }

                    LogSystem::FirstOrCreate([
                        'menu'       => 'scan spreading - tumpuk',
                        'user_id'    => \Auth::user()->id,
                        'desc'       => $old_marker . ' to be ' . $first_barcode,
                        'factory_id' => \Auth::user()->factory_id,
                        'type'       => 'update',
                        'created_at' => Carbon::now()
                    ]);

                    $response_data_marker = CuttingMarker::where('barcode_id', $first_barcode)->get()->first();

                    $part_no = explode('+', preg_replace('/\s+/', '', $response_data_marker->part_no));

                    $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $response_data_marker->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                    $ratio = 0;
                    foreach ($response_data_marker->rasio_markers as $a) {
                        $ratio = $ratio + $a->ratio;
                    }
                    $total_garment = $ratio * $response_data_marker->layer;

                    $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $response_data_marker->id_plan)->where('barcode_id', $response_data_marker->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                    $po_buyer = implode(', ', $po_buyer);

                    $response = [
                        'status'        => 200,
                        'data'          => $response_data_marker,
                        'style'         => $response_data_marker->cutting_plan->style,
                        'article'       => $response_data_marker->cutting_plan->articleno,
                        'plan'          => Carbon::parse($response_data_marker->cutting_plan->cutting_date)->format('d-m-Y'),
                        'color_name'    => substr($get_data_cutting->color_name, 0, 15),
                        'ratio'         => $ratio,
                        'total_garment' => $total_garment,
                        'item_code'     => $get_data_cutting->material,
                        'color_code'    => substr($get_data_cutting->color_code_raw_material, 0, 20),
                        'stat_date'     => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                        'marker_width'  => $response_data_marker->fabric_width - 0.5,
                        'po_buyer'      => $po_buyer,
                        'ratio_size'    => $ratio_check,
                        'ket'           => null
                    ];

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($response, 200);
            }
        }
    }

    public function checkFabricId(Request $request)
    {
        if (request()->ajax()) {
            $barcode_meja = $request->barcode_meja;
            $barcode_marker = $request->marker_check;
            $barcode_fabric = $request->fabric_check;
            $total_length_allow = $request->detail_view_allow;
            $total_layer_marker = $request->detail_view_layer;
            if($barcode_marker != null && $barcode_fabric != null){
                $factory = Auth::user()->factory_id;
                if($factory == '1'){
                    // IS FABRIC RECEIVED?
                    $fabric_received = DB::table('report_roll')->where('barcode',strtoupper($barcode_fabric))->whereNull('deleted_at')->first();
                    if($fabric_received == null){
                        return response()->json(['status' => 442, 'report' => 'Fabric belum di terima Cutting..!', 'barcode_id' => $barcode_fabric]);
                    }
                    //IS FABRIC WAS SCAN OUT FROM WMS?
                    $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out'"));
                    if(count($get_data_roll) > 0){
                        $check_fabric_temp = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 'f')->get();
                        // IS FABRIC STILL USED?
                        if(count($check_fabric_temp) > 0){
                            return response()->json(['status' => 442, 'report' => 'Fabrik sedang digunakan!', 'barcode_id' => $barcode_marker]);
                        }else{
                            $check_spreading_process = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 't')->orderBy('actual_sisa', 'desc')->get();
                            if(count($check_spreading_process) > 0){
                                // jika sisa fabric lebih besar dari 0
                                if($check_spreading_process->first()->actual_sisa > 0){
                                    // set total length allow in inch
                                    $total_length_allow_in_inch = $total_length_allow / 36;
                                    // get data marker
                                    $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                    // get data part no
                                    // $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                    $get_data_part_no = explode('+', $data->part_no);
                                    // get data po buyer
                                    $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                    //get info barcode fabric from wms
                                    $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric')"));
    
                                    if(count($barcode_roll_check) > 0){
                                        $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                    ->where('part_no', $get_data_part_no[0])
                                                    ->select('item_id')
                                                    ->distinct()
                                                    ->pluck('item_id')->toArray();
                                        //CEK ITEM ID IN DATA CUTTINGS
                                        $cek_date_rollwhs = DataCutting::where('plan_id',$data->id_plan)
                                                    ->select('cutting_date')
                                                    ->first();
                                        if(!in_array($data->item_id, $cek_item)){
                                            //FOUND DIFFERENT ITEM ID IN CDMS BETWEEN REQUEST MARKER AND DATA CUTTINGS
                                            return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                        }
                                        $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE planning_date='$cek_date_rollwhs->cutting_date' AND item_id = '$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL OR planning_date='$cek_date_rollwhs->cutting_date' AND item_id_plan = '$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                        // end proses check item id baru =================================================================================
                                        // jika get data roll ada
                                        if (count($get_data_roll) > 0) {
                                            // set value supply layer
                                            $suplai_layer = floor($check_spreading_process->first()->actual_sisa / $total_length_allow_in_inch);
                                            // jika supply layer lebih besar dari total layer di marker
                                            if ($suplai_layer > $total_layer_marker) {
                                                // supply layer sama dengan total marker di layer
                                                $suplai_layer = $total_layer_marker;
                                                // jika supply layer lebih kecil dari total layer di marker
                                            }else{
                                                // supply layer sama dengan supply layer
                                                $suplai_layer = $suplai_layer;
                                            }
                                            // set supply sisa
                                            $suplai_sisa = $check_spreading_process->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
    
                                            // case combine plan : get cutting date
                                            if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                    ->whereNull('deleted_at')
                                                    ->pluck('cutting_date')
                                                    ->toArray();
                                            } else {
                                                $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                            }
                                            foreach($get_data_roll as $ax => $az){
                                                if ($az->qty_prepared > 5) {
                                                    if($az->planning_date != null){
                                                        if($data->second_plan == null){
                                                            if (!in_array($az->planning_date, $get_cutting_date) && $az->style != 'additional') {
                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                            }
                                                        }else{
                                                            if ((!in_array($az->planning_date, $get_cutting_date) && $az->planning_date != $data->second_plan) && $az->style != 'additional') {
                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
    
                                            // insert data ke temporary tabel
                                            SpreadingReportTemp::FirstOrCreate([
                                                'barcode_marker'    => $barcode_marker,
                                                'barcode_fabric'    => $barcode_fabric,
                                                'no_roll'           => $check_spreading_process->first()->no_roll,
                                                'qty_fabric'        => $check_spreading_process->first()->actual_sisa,
                                                'actual_width'      => null,
                                                'lot'               => $check_spreading_process->first()->lot,
                                                'suplai_layer'      => $suplai_layer,
                                                'actual_layer'      => null,
                                                'suplai_sisa'       => $suplai_sisa,
                                                'actual_sisa'       => $check_spreading_process->first()->actual_sisa,
                                                'akumulasi_layer'   => 0,
                                                'reject'            => null,
                                                'sambungan'         => null,
                                                'actual_use'        => 0,
                                                'remark'            => null,
                                                'kualitas_gelaran'  => null,
                                                'barcode_table'     => $barcode_meja,
                                                'is_commit'         => 'f',
                                                'user_id'           => Auth::user()->id,
                                                'factory_id'        => Auth::user()->factory_id,
                                                'po_supplier'       => $check_spreading_process->first()->po_supplier,
                                                'supplier_name'     => $check_spreading_process->first()->supplier_name,
                                                'item_code'         => $check_spreading_process->first()->item_code,
                                                'color'             => $check_spreading_process->first()->color,
                                                'sambungan_end'     => null,
                                                'item_id'           => $check_spreading_process->first()->item_id,
                                            ]);
                                            return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                        } else {
                                            return response()->json(['status' => 442, 'report' => 'Fabric tidak ditemukan #2!', 'barcode_id' => $barcode_marker]);
                                        }
                                    } else {
                                        //WMS DATA V2
                                        $barcode_roll_check_2 = DB::connection('wms_live_new')
                                        ->table('detail_material_planning_preparations')
                                        ->where('barcode', strtolower($barcode_fabric))
                                        ->whereNull('deleted_at')
                                        ->first();
                                        //WMS DATA V1
                                        $barcode_roll_check_old = DB::connection('wms_live')
                                        ->select('planning_date')
                                        ->table('detail_material_preparation_fabrics as dmpf')
                                        ->leftJoin('material_preparation_fabrics as mpf','mpf.id','dmpf.material_preparation_fabric_id')
                                        ->where('dmpf.barcode', $barcode_fabric)
                                        ->first();
    
                                        if($barcode_roll_check_2 != null && $barcode_roll_check_old == null){
                                            $po_buyer_exp = explode(',',$barcode_roll_check_2->selected_po_buyer);
                                            $po_buyer_arr = "'".implode("','",$po_buyer_exp)."'";
                                            $cek_is_po_reroute = DB::table('data_cuttings')->select('po_buyer')->whereIn('po_buyer',$po_buyer_arr)->first();
                                            if($cek_is_po_reroute == null){
                                                return response()->json(['status' => 442, 'report' => 'Terdapat Reroute PO', 'barcode_id' => $barcode_fabric]);
                                            }
                                            $cut_date_cdms = DB::table('data_cuttings')->select('cutting_date')->whereIn('po_buyer',$po_buyer_arr)->where('material',strtoupper($barcode_roll_check_2->item_code_book))->where('item_id',$barcode_roll_check_2->item_id_book)->first();
                                            if($barcode_roll_check_2->planning_date != $cut_date_cdms->cutting_date){
                                                return response()->json(['status' => 442, 'report' => 'Terdapat Perbedaan Planning Cutting', 'barcode_id' => $barcode_fabric]);
                                                //CEK DI WMS TABLE material_planning_preparations PADA KOLOM planning_date
                                                //COMPARE DENGAN TABLE data_cuttings DI CDMS PADA KOLOM cutting_date
                                            }
                                        }elseif($barcode_roll_check_2 == null && $barcode_roll_check_old != null){
                                            $cut_date_cdms = DB::table('data_cuttings')->select('cutting_date')->whereIn('po_buyer',$po_buyer_arr)->where('material',strtoupper($barcode_roll_check_old->item_code))->where('item_id',$barcode_roll_check_old->item_id_book)->first();
                                            if($barcode_roll_check_old->planning_date != $cut_date_cdms->cutting_date){
                                                return response()->json(['status' => 442, 'report' => 'Terdapat Perbedaan Planning Cutting', 'barcode_id' => $barcode_fabric]);
                                                //CEK DI WMS TABLE material_planning_preparations PADA KOLOM planning_date
                                                //COMPARE DENGAN TABLE data_cuttings DI CDMS PADA KOLOM cutting_date
                                            }
                                        }elseif($barcode_roll_check_2 != null && $barcode_roll_check_old != null){
                                            return response()->json(['status' => 442, 'report' => 'Terdapat Duplikat Barcode', 'barcode_id' => $barcode_fabric]);
                                        }else{
                                            return response()->json(['status' => 442, 'report' => 'Fabric belum di prepare oleh warehouse!', 'barcode_id' => $barcode_marker]);
                                        }
                                    }
                                } else {
                                    //IF NOT ENOUGH QTY FABRIC LEFT!
                                    return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                }
                            // jika belum pernah digunakan
                            } else {
                                // get data fabrik yang paling kecil sisanya
                                $check_fabric = FabricUsed::where('barcode_fabric', $barcode_fabric)->orderBy('created_spreading', 'desc')->get();
                                // jika fabrik sudah pernah digunakan
                                if ($check_fabric->count() > 0) {
                                    // jika sisa fabric lebih besar dari 0
                                    if ($check_fabric->first()->actual_sisa > 0) {
                                        // set total length allow in inch
                                        $total_length_allow_in_inch = $total_length_allow / 36;
                                        // get data marker
                                        $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                        // get data part no
                                        // $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                        $get_data_part_no = explode('+', $data->part_no);
                                        // get data po buyer
                                        $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                        $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out'"));
                                        if (count($barcode_roll_check) > 0) {
                                            // GET DATA ROLL!
                                            $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND item_id='$data->item_id' OR last_status_movement='out' AND item_id_plan='$data->item_id' GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                            if (count($get_data_roll) > 0) {
                                                //DO NOTHING!
                                            } else {
                                                $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                            ->where('part_no', $get_data_part_no[0])
                                                            ->select('item_id')
                                                            ->distinct()
                                                            ->pluck('item_id')->toArray();
                                                if(!in_array($data->item_id, $cek_item))
                                                {
                                                    return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                }
                                                $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND item_id='$data->item_id' AND deleted_at IS NULL OR item_id_plan='$data->item_id' AND deleted_at IS NULL AND last_status_movement='out' GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
    
                                                if(count($get_data_roll) > 0) {
                                                    //DO NOTHING!
                                                } else {
                                                    $get_data_roll_relax = DB::connection('wms_live')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                                    if (count($get_data_roll_relax) <= 0) {
                                                        return response()->json(['status' => 442, 'report' => 'Fabric belum di scan out warehouse #2!', 'barcode_id' => $barcode_marker]);
                                                    } else {
                                                        return response()->json(['status' => 442, 'report' => 'Fabric tidak ditemukan #3!', 'barcode_id' => $barcode_marker]);
                                                    }
                                                }
                                            }
                                            // jika get data roll ada
                                            if (count($get_data_roll) > 0) {
                                                // set value supply layer
                                                $suplai_layer = floor($check_fabric->first()->actual_sisa / $total_length_allow_in_inch);
                                                // jika supply layer lebih besar dari total layer di marker
                                                if($suplai_layer > $total_layer_marker){
                                                    // supply layer sama dengan total marker di layer
                                                    $suplai_layer = $total_layer_marker;
                                                    // jika supply layer lebih kecil dari total layer di marker
                                                }else{
                                                    // supply layer sama dengan supply layer
                                                    $suplai_layer = $suplai_layer;
                                                }
    
                                                // case combine plan : get cutting date
                                                if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                    $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                        ->whereNull('deleted_at')
                                                        ->pluck('cutting_date')
                                                        ->toArray();
                                                } else {
                                                    $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                }
    
                                                // set supply sisa
                                                $suplai_sisa = $check_fabric->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
                                                foreach($get_data_roll as $xc => $xv){
                                                    if ($xv->qty_prepared > 5) {
                                                        if ($xv->planning_date != null) {
                                                            if($data->second_plan == null) {
                                                                if (!in_array($xv->planning_date, $get_cutting_date) && $xv->style != 'additional') {
                                                                    return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                }
                                                            } else {
                                                                if ((!in_array($xv->planning_date, $get_cutting_date) && $xv->planning_date != $data->second_plan) && $xv->style != 'additional') {
                                                                    return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
    
                                                // insert data ke temporary tabel
                                                SpreadingReportTemp::FirstOrCreate([
                                                    'barcode_marker'    => $barcode_marker,
                                                    'barcode_fabric'    => $barcode_fabric,
                                                    'no_roll'           => $check_fabric->first()->no_roll,
                                                    'qty_fabric'        => $check_fabric->first()->actual_sisa,
                                                    'actual_width'      => null,
                                                    'lot'               => $check_fabric->first()->lot,
                                                    'suplai_layer'      => $suplai_layer,
                                                    'actual_layer'      => null,
                                                    'suplai_sisa'       => $suplai_sisa,
                                                    'actual_sisa'       => $check_fabric->first()->actual_sisa,
                                                    'akumulasi_layer'   => 0,
                                                    'reject'            => null,
                                                    'sambungan'         => null,
                                                    'actual_use'        => 0,
                                                    'remark'            => null,
                                                    'kualitas_gelaran'  => null,
                                                    'barcode_table'     => $barcode_meja,
                                                    'is_commit'         => 'f',
                                                    'user_id'           => Auth::user()->id,
                                                    'factory_id'        => Auth::user()->factory_id,
                                                    'po_supplier'       => $check_fabric->first()->po_supplier,
                                                    'supplier_name'     => $check_fabric->first()->supplier_name,
                                                    'item_code'         => $check_fabric->first()->item_code,
                                                    'color'             => $check_fabric->first()->color,
                                                    'sambungan_end'     => null,
                                                    'item_id'           => $check_fabric->first()->item_id,
                                                ]);
                                                return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                            } else {
                                                return response()->json(['status' => 442, 'report' => 'Fabric tidak ditemukan #4!', 'barcode_id' => $barcode_marker]);
                                            }
                                        } else {
                                            $barcode_roll_check_2 = DB::connection('wms_live')->table('detail_material_preparation_fabrics')->where('barcode', $barcode_fabric)->get();
    
                                            if ($barcode_roll_check_2->count() > 0) {
                                                return response()->json(['status' => 442, 'report' => 'Fabric belum di pindah plan oleh warehouse!', 'barcode_id' => $barcode_marker]);
                                            } else {
                                                return response()->json(['status' => 442, 'report' => 'Fabric belum di prepare oleh warehouse!', 'barcode_id' => $barcode_marker]);
                                            }
                                        }
                                        // jika sudah tidak terdapat sisa fabrik
                                    } else {
                                        // IF NOT ENOUGH LAST FABRIC
                                        return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                    }
                                    // jika fabrik belum pernah digunakan
                                } else {
                                    // set total length allow in inch
                                    $total_length_allow_in_inch = $total_length_allow / 36;
                                    // get data marker
                                    $data = CuttingMarker::where('barcode_id', $barcode_marker)->get()->first();
                                    // get data part no
                                    // $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                    $get_data_part_no = explode('+', $data->part_no);
                                    // // get data po buyer
                                    $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                    $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out'"));
                                    if (count($barcode_roll_check) > 0) {
                                        $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                    ->where('part_no', $get_data_part_no[0])
                                                    ->select('item_id')
                                                    ->distinct()
                                                    ->pluck('item_id')->toArray();
    
                                        $cek_date_rollwhs = DataCutting::where('plan_id',$data->id_plan)
                                                        ->select('cutting_date')
                                                        ->where('part_no',$get_data_part_no[0])
                                                        ->first();
                                                        
                                        if(!in_array($data->item_id, $cek_item))
                                        {
                                            return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                        }
    
                                        // get data roll
                                        $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND item_id='$data->item_id' OR last_status_movement='out' and item_id_plan='$data->item_id' GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
    
                                        if($get_data_roll != null) {
                                            //DO NOTHING!
                                        } else {
                                            //     //ORIGINAL CODE
                                            //     // $get_data_roll_relax = DB::connection('wms_live')->table('integration_whs_to_cutting')
                                            //     // ->select(DB::raw("barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id"))
                                            //     // ->where('barcode', $barcode_fabric)
                                            //     // ->groupBy('barcode', 'nomor_roll', 'actual_lot', 'actual_width', 'planning_date', 'style', 'item_id')
                                            //     // ->get();
                                            //     MODIFIED CODE ADDING LAST STATUS MOVEMENT != OUT
                                            $get_data_roll_relax = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND item_id='$data->item_id' AND deleted_at IS NULL OR last_status_movement='out' AND item_id_plan='$data->item_id' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
    
                                            foreach($get_data_roll_relax as $key => $value){
                                                if ($value->last_status_movement != 'out') {
                                                    // return alert fabrik tidak ditemukan
                                                return response()->json(['status' => 442, 'report' => 'Fabric belum di scan out warehouse #3!', 'barcode_id' => $barcode_marker]);
                                                } else if($get_data_roll_relax == null){
                                                    // return alert fabrik tidak ditemukan
                                                    return response()->json(['status' => 442, 'report' => 'Fabric tidak ditemukan #5! / Terdapat Beda Plan Marker dengan Plan Fabric', 'barcode_id' => $barcode_marker]);
                                                }
                                            }
                                        }
                                        // $get_data_rolls = DB::connection('wms_live_new')->select(DB::raw("SELECT * from integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out'"));
                                        $get_data_rolls = DB::connection('wms_live_new')->select(DB::raw("SELECT document_no,supplier_name,item_code,color,barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND item_id='$data->item_id' AND deleted_at IS NULL OR last_status_movement='out' AND item_id_plan='$data->item_id' AND deleted_at IS NULL GROUP BY document_no,supplier_name,item_code,barcode,color, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                        // jika terdapat roll
                                        if ($get_data_rolls != null) {
                                            foreach($get_data_rolls as $rs => $rsl){
                                                // set value supply layer
                                                $suplai_layer = floor($rsl->qty_prepared / $total_length_allow_in_inch);
                                                // jika supply layer lebih besar dari total layer di marker
                                                if ($suplai_layer > $total_layer_marker) {
                                                    // supply layer sama dengan total marker di layer
                                                    $suplai_layer = $total_layer_marker;
                                                    // jika supply layer lebih kecil dari total layer di marker
                                                }else{
                                                    // supply layer sama dengan supply layer
                                                    $suplai_layer = $suplai_layer;
                                                }
                                                // set value sisa supply
                                                $suplai_sisa = $rsl->qty_prepared - ($suplai_layer * $total_length_allow_in_inch);
                                                // case combine plan : get cutting date
                                                if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                    $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                        ->whereNull('deleted_at')
                                                        ->pluck('cutting_date')
                                                        ->toArray();
                                                } else {
                                                    $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                }
                                                if ($rsl->qty_prepared == null) {
                                                    return response()->json(['status' => 442, 'report' => 'Terjadi kesalahan, silakan kontak ICT!', 'barcode_id' => $barcode_marker]);
                                                }
                                                if($rsl->qty_prepared > 5) {
                                                    if ($rsl->planning_date != null) {
                                                        if($data->second_plan == null) {
                                                            if (!in_array($rsl->planning_date, $get_cutting_date) && $rsl->style != 'additional') {
                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                            }
                                                        } else {
                                                            if ((!in_array($rsl->planning_date, $get_cutting_date) && $rsl->planning_date != $data->second_plan) && $rsl->style != 'additional') {
                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                            }
                                                        }
                                                    }
                                                }
                                                // insert data
                                                SpreadingReportTemp::FirstOrCreate([
                                                    'barcode_marker'        => $barcode_marker,
                                                    'barcode_fabric'        => $barcode_fabric,
                                                    'no_roll'               => $rsl->nomor_roll,
                                                    'qty_fabric'            => $rsl->qty_prepared,
                                                    'actual_width'          => null,
                                                    'lot'                   => $rsl->actual_lot,
                                                    'suplai_layer'          => $suplai_layer,
                                                    'actual_layer'          => null,
                                                    'suplai_sisa'           => $suplai_sisa,
                                                    'actual_sisa'           => $rsl->qty_prepared,
                                                    'akumulasi_layer'       => 0,
                                                    'reject'                => null,
                                                    'sambungan'             => null,
                                                    'actual_use'            => 0,
                                                    'remark'                => null,
                                                    'kualitas_gelaran'      => null,
                                                    'barcode_table'         => $barcode_meja,
                                                    'is_commit'             => 'f',
                                                    'user_id'               => Auth::user()->id,
                                                    'factory_id'            => Auth::user()->factory_id,
                                                    'po_supplier'           => $rsl->document_no,
                                                    'supplier_name'         => $rsl->supplier_name,
                                                    'item_code'             => $rsl->item_code,
                                                    'color'                 => $rsl->color,
                                                    'sambungan_end'         => null,
                                                    'item_id'               => $rsl->item_id,
                                                ]);
                                                return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                            }
                                        } else {
                                            return response()->json(['status' => 442, 'report' => 'Barcode fabrik tidak ditemukan!#1', 'barcode_id' => $barcode_marker]);
                                        }
                                    } else {
                                        $barcode_roll_check_2 = DB::connection('wms_live')->table('detail_material_preparation_fabrics')->where('barcode', $barcode_fabric)->get();
    
                                        if ($barcode_roll_check_2->count() > 0) {
                                            return response()->json(['status' => 442, 'report' => 'Fabric belum di pindah plan oleh warehouse!', 'barcode_id' => $barcode_marker]);
                                        } else {
                                            return response()->json(['status' => 442, 'report' => 'Fabric belum di prepare oleh warehouse!', 'barcode_id' => $barcode_marker]);
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        return response()->json(['status' => 442, 'report' => 'Roll Fabric Belum di Prepare Warehouse!', 'barcode_id' => $barcode_marker]);
                    }
                }else{
                    //FACTORY AOI 2
                    $check_additional = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL"));
                    $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                    if(count($check_additional) == 0){
                        return response()->json(['status' => 442, 'report' => 'Roll Fabric Belum di Prepare Warehouse!', 'barcode_id' => $barcode_marker]);
                    }else{
                        foreach($check_additional as $x => $y){
                            // JIKA BARCODE FABRIC MERUPAKAN ALOKASI ADDITIONAL MAKA EKSEKUSI METHODE INI!
                            if($y->part_no == 'additional'){
                                // IS BARCODE FABRIC STILL USED?
                                $check_fabric_temp = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 'f')->get();
                                if(count($check_fabric_temp) > 0){
                                    return response()->json(['status' => 442, 'report' => 'Fabrik sedang digunakan!', 'barcode_id' => $barcode_marker]);
                                }else{
                                    $check_spreading_process = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 't')->orderBy('actual_sisa', 'desc')->get();
                                    if(count($check_spreading_process) > 0) {
                                        // jika sisa fabric lebih besar dari 0
                                        if ($check_spreading_process->first()->actual_sisa > 0) {
                                            // set total length allow in inch
                                            $total_length_allow_in_inch = $total_length_allow / 36;
                                            $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                            $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                            $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                            $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL"));
                                            if (count($barcode_roll_check) > 0) {
                                                $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                            ->where('part_no', $get_data_part_no[0])
                                                            ->select('item_id')
                                                            ->distinct()
                                                            ->pluck('item_id')->toArray();
                                                if(!in_array($data->item_id, $cek_item))
                                                {
                                                    return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                }
                                                $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode') WHERE item_id='$data->item_id' OR item_id_plan='$data->item_id' AND last_status_movement='out' GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                                // IF ROLL EXISTS AND SUPPLIED TO CUTTING
                                                if(count($get_data_roll) > 0){
                                                    // set value supply layer
                                                    $suplai_layer = floor($check_spreading_process->first()->actual_sisa / $total_length_allow_in_inch);
                                                    // jika supply layer lebih besar dari total layer di marker
                                                    if ($suplai_layer > $total_layer_marker) {
                                                        // supply layer sama dengan total marker di layer
                                                        $suplai_layer = $total_layer_marker;
                                                        // jika supply layer lebih kecil dari total layer di marker
                                                    } else {
                                                        // supply layer sama dengan supply layer
                                                        $suplai_layer = $suplai_layer;
                                                    }
                                                    // set supply sisa
                                                    $suplai_sisa = $check_spreading_process->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
                                                    // case combine plan : get cutting date
                                                    if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                        $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                            ->whereNull('deleted_at')
                                                            ->pluck('cutting_date')
                                                            ->toArray();
                                                    } else {
                                                        $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                    }
                                                    foreach($get_data_roll as $zz => $xx){
                                                        if ($xx->qty_prepared > 5) {
                                                            if ($xx->planning_date != null) {
                                                                if($data->second_plan == null) {
                                                                    if (!in_array($xx->planning_date, $get_cutting_date) && $xx->style != 'additional') {
                                                                        return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                    }
                                                                } else {
                                                                    if ((!in_array($xx->planning_date, $get_cutting_date) && $xx->planning_date != $data->second_plan) && $xx->style != 'additional') {
                                                                        return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // insert data ke temporary tabel
                                                    SpreadingReportTemp::FirstOrCreate([
                                                        'barcode_marker'    => $barcode_marker,
                                                        'barcode_fabric'    => $barcode_fabric,
                                                        'no_roll'           => $check_spreading_process->first()->no_roll,
                                                        'qty_fabric'        => $check_spreading_process->first()->actual_sisa,
                                                        'actual_width'      => null,
                                                        'lot'               => $check_spreading_process->first()->lot,
                                                        'suplai_layer'      => $suplai_layer,
                                                        'actual_layer'      => null,
                                                        'suplai_sisa'       => $suplai_sisa,
                                                        'actual_sisa'       => $check_spreading_process->first()->actual_sisa,
                                                        'akumulasi_layer'   => 0,
                                                        'reject'            => null,
                                                        'sambungan'         => null,
                                                        'actual_use'        => 0,
                                                        'remark'            => null,
                                                        'kualitas_gelaran'  => null,
                                                        'barcode_table'     => $barcode_meja,
                                                        'is_commit'         => 'f',
                                                        'user_id'           => Auth::user()->id,
                                                        'factory_id'        => Auth::user()->factory_id,
                                                        'po_supplier'       => $check_spreading_process->first()->po_supplier,
                                                        'supplier_name'     => $check_spreading_process->first()->supplier_name,
                                                        'item_code'         => $check_spreading_process->first()->item_code,
                                                        'color'             => $check_spreading_process->first()->color,
                                                        'sambungan_end'     => null,
                                                        'item_id'           => $check_spreading_process->first()->item_id,
                                                    ]);
                                                    return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                } else {
                                                    return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#9', 'barcode_id' => $barcode_marker]);
                                                }
                                            } else {
                                                return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#8', 'barcode_id' => $barcode_marker]);
                                            }
                                        } else {
                                            return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                        }
                                    // jika belum pernah digunakan
                                    } else {
                                        // get data fabrik yang paling kecil sisanya
                                        $check_fabric = FabricUsed::where('barcode_fabric', $barcode_fabric)->orderBy('created_spreading', 'desc')->get();
                                        // jika fabrik sudah pernah digunakan
                                        if ($check_fabric->count() > 0) {
                                            // jika sisa fabric lebih besar dari 0
                                            if ($check_fabric->first()->actual_sisa > 0) {
                                                // set total length allow in inch
                                                $total_length_allow_in_inch = $total_length_allow / 36;
                                                // get data marker
                                                $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                                // get data part no
                                                $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                                // get data po buyer
                                                $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                                // $barcode_roll_check = DB::connection('wms_live_new')->table('integration_whs_to_cutting')->where('barcode', $barcode_fabric)->get();
                                                $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL"));
                                                if (count($barcode_roll_check) > 0) {
                                                    $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                                ->where('part_no', $get_data_part_no[0])
                                                                ->select('item_id')
                                                                ->distinct()
                                                                ->pluck('item_id')->toArray();
                                                    if(!in_array($data->item_id, $cek_item))
                                                    {
                                                        return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                    }
                                                    $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' OR item_id_plan='$data->item_id' AND last_status_movement = 'out' AND deleted_at IS NULL GROUP BY barcode,nomor_roll,actual_lot,actual_width,planning_date,style,item_id"));
                                                    // jika get data roll ada
                                                    if (count($get_data_roll) > 0) {
                                                        // set value supply layer
                                                        $suplai_layer = floor($check_fabric->first()->actual_sisa / $total_length_allow_in_inch);
                                                        // jika supply layer lebih besar dari total layer di marker
                                                        if ($suplai_layer > $total_layer_marker) {
                                                            // supply layer sama dengan total marker di layer
                                                            $suplai_layer = $total_layer_marker;
                                                            // jika supply layer lebih kecil dari total layer di marker
                                                        } else {
                                                            // supply layer sama dengan supply layer
                                                            $suplai_layer = $suplai_layer;
                                                        }
                                                        // case combine plan : get cutting date
                                                        if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                            $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                                ->whereNull('deleted_at')
                                                                ->pluck('cutting_date')
                                                                ->toArray();
                                                        } else {
                                                            $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                        }

                                                        // set supply sisa
                                                        $get_data_rolls = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' OR item_id_plan='$data->item_id' AND last_status_movement = 'out' AND deleted_at IS NULL GROUP BY barcode,nomor_roll,actual_lot,actual_width,planning_date,style,item_id"));
                                                        $suplai_sisa = $check_fabric->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
                                                        foreach($get_data_rolls as $mn => $n){
                                                            if ($n->qty_prepared > 5) {
                                                                if ($n->planning_date != null) {
                                                                    if($data->second_plan == null) {
                                                                        if (!in_array($n->planning_date, $get_cutting_date) && $n->style != 'additional') {
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    } else {
                                                                        if ((!in_array($n->planning_date, $get_cutting_date) && $n->planning_date != $data->second_plan) && $n->style != 'additional') {
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        // insert data ke temporary tabel
                                                        SpreadingReportTemp::FirstOrCreate([
                                                            'barcode_marker'    => $barcode_marker,
                                                            'barcode_fabric'    => $barcode_fabric,
                                                            'no_roll'           => $check_fabric->first()->no_roll,
                                                            'qty_fabric'        => $check_fabric->first()->actual_sisa,
                                                            'actual_width'      => null,
                                                            'lot'               => $check_fabric->first()->lot,
                                                            'suplai_layer'      => $suplai_layer,
                                                            'actual_layer'      => null,
                                                            'suplai_sisa'       => $suplai_sisa,
                                                            'actual_sisa'       => $check_fabric->first()->actual_sisa,
                                                            'akumulasi_layer'   => 0,
                                                            'reject'            => null,
                                                            'sambungan'         => null,
                                                            'actual_use'        => 0,
                                                            'remark'            => null,
                                                            'kualitas_gelaran'  => null,
                                                            'barcode_table'     => $barcode_meja,
                                                            'is_commit'         => 'f',
                                                            'user_id'           => Auth::user()->id,
                                                            'factory_id'        => Auth::user()->factory_id,
                                                            'po_supplier'       => $check_fabric->first()->po_supplier,
                                                            'supplier_name'     => $check_fabric->first()->supplier_name,
                                                            'item_code'         => $check_fabric->first()->item_code,
                                                            'color'             => $check_fabric->first()->color,
                                                            'sambungan_end'     => null,
                                                            'item_id'           => $check_fabric->first()->item_id,
                                                        ]);
                                                        return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                    } else {
                                                        return response()->json(['status' => 442, 'report' => 'Fabric tidak ditemukan #9!', 'barcode_id' => $barcode_marker]);
                                                    }
                                                } else {
                                                    return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#10', 'barcode_id' => $barcode_marker]);
                                                }
                                            } else {
                                                return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                            }
                                        }else{
                                            // set total length allow in inch
                                            $total_length_allow_in_inch = $total_length_allow / 36;
                                            // get data marker
                                            $data = CuttingMarker::where('barcode_id', $barcode_marker)->get()->first();
                                            // get data part no
                                            $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                            // // get data po buyer
                                            $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                            $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL"));
                                            if(count($barcode_roll_check) > 0){
                                                $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                            ->where('part_no', $get_data_part_no[0])
                                                            ->select('item_id')
                                                            ->distinct()
                                                            ->pluck('item_id')->toArray();
                                                if(!in_array($data->item_id, $cek_item))
                                                {
                                                    return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                }
                                                // get data roll
                                                $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' OR item_id_plan='$data->item_id' AND last_status_movement = 'out' GROUP BY barcode,nomor_roll,actual_lot,actual_width,planning_date,style,item_id"));
                                                // jika terdapat roll
                                                if ($get_data_roll != null) {
                                                    foreach($get_data_roll as $r => $u){
                                                        // set value supply layer
                                                        $suplai_layer = floor($u->qty_prepared / $total_length_allow_in_inch);
                                                        // jika supply layer lebih besar dari total layer di marker
                                                        if ($suplai_layer > $total_layer_marker) {
                                                            // supply layer sama dengan total marker di layer
                                                            $suplai_layer = $total_layer_marker;
                                                            // jika supply layer lebih kecil dari total layer di marker
                                                        } else {
                                                            // supply layer sama dengan supply layer
                                                            $suplai_layer = $suplai_layer;
                                                        }

                                                        // set value sisa supply
                                                        $suplai_sisa = $u->qty_prepared - ($suplai_layer * $total_length_allow_in_inch);

                                                        // case combine plan : get cutting date
                                                        if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                            $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                                ->whereNull('deleted_at')
                                                                ->pluck('cutting_date')
                                                                ->toArray();
                                                        } else {
                                                            $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                        }

                                                        if ($u->qty_prepared == null) {
                                                            return response()->json(['status' => 442, 'report' => 'Terjadi kesalahan, silakan kontak ICT!', 'barcode_id' => $barcode_marker]);
                                                        }

                                                        if ($u->qty_prepared > 5) {
                                                            if ($u->planning_date != null) {
                                                                if($data->second_plan == null) {
                                                                    if (!in_array($u->planning_date, $get_cutting_date) && $u->style != 'additional') {
                                                                        return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                    }
                                                                } else {
                                                                    if ((!in_array($u->planning_date, $get_cutting_date) && $u->planning_date != $data->second_plan) && $u->style != 'additional') {
                                                                        return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        // insert data
                                                        SpreadingReportTemp::FirstOrCreate([
                                                            'barcode_marker'    => $barcode_marker,
                                                            'barcode_fabric'    => $barcode_fabric,
                                                            'no_roll'           => $u->nomor_roll,
                                                            'qty_fabric'        => $u->qty_prepared,
                                                            'actual_width'      => null,
                                                            'lot'               => $u->actual_lot,
                                                            'suplai_layer'      => $suplai_layer,
                                                            'actual_layer'      => null,
                                                            'suplai_sisa'       => $suplai_sisa,
                                                            'actual_sisa'       => $u->qty_prepared,
                                                            'akumulasi_layer'   => 0,
                                                            'reject'            => null,
                                                            'sambungan'         => null,
                                                            'actual_use'        => 0,
                                                            'remark'            => null,
                                                            'kualitas_gelaran'  => null,
                                                            'barcode_table'     => $barcode_meja,
                                                            'is_commit'         => 'f',
                                                            'user_id'           => Auth::user()->id,
                                                            'factory_id'        => Auth::user()->factory_id,
                                                            'po_supplier'       => $u->document_no,
                                                            'supplier_name'     => $u->supplier_name,
                                                            'item_code'         => $u->item_code,
                                                            'color'             => $u->color,
                                                            'sambungan_end'     => null,
                                                            'item_id'           => $u->item_id,
                                                        ]);
                                                        // return sukses
                                                        return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                    }
                                                } else {
                                                    return response()->json(['status' => 442, 'report' => 'Barcode fabrik tidak ditemukan!#2', 'barcode_id' => $barcode_marker]);
                                                }
                                            } else {
                                                return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#11', 'barcode_id' => $barcode_marker]);
                                            }
                                        }
                                    }
                                }
                            }else{
                                //JIKA BARCODE FABRIC BUKAN ALOKASI ADDITIONAL!
                                $check_fabric_receive = ReportRoll::where('barcode', strtoupper($barcode_fabric))->whereNull('deleted_at')->exists();
                                if($check_fabric_receive){
                                    $check_fabric_temp = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 'f')->get();
                                    if (count($check_fabric_temp) > 0) {
                                        return response()->json(['status' => 442, 'report' => 'Fabrik sedang digunakan!', 'barcode_id' => $barcode_marker]);
                                    }else{
                                        $check_spreading_process = SpreadingReportTemp::where('barcode_fabric', $barcode_fabric)->where('is_commit', 't')->orderBy('actual_sisa', 'desc')->get();
                                        //MENGGUNAKAN FABRIC ROLL SISA SPREADING DARI TRANSAKSI TEMPORARY SEBELUMNYA!
                                        if(count($check_spreading_process) > 0){
                                            // jika sisa fabric lebih besar dari 0
                                            if ($check_spreading_process->first()->actual_sisa > 0){
                                                // set total length allow in inch
                                                $total_length_allow_in_inch = $total_length_allow / 36;
                                                $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                                if(str_contains($data->part_no,'PART')){
                                                    $get_data_part_no = explode('+', $data->part_no);
                                                }else{
                                                    $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                                }
                                                $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                                //GET INFO FROM UNION DATA WMS NEW
                                                $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement = 'out' AND deleted_at IS NULL"));
                                                if(count($barcode_roll_check) > 0){
                                                    $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                                ->where('part_no', $get_data_part_no[0])
                                                                ->select('item_id')
                                                                ->distinct()
                                                                ->pluck('item_id')
                                                                ->toArray();
                                                    if(!in_array($data->item_id, $cek_item))
                                                    {
                                                        return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                    }
                                                    $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' AND last_status_movement='out' GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                                    // IF FABRIC ROLL IS EXISTS AND SUPPLIED TO CUTTING!
                                                    if(count($get_data_roll) > 0){
                                                        // set value supply layer
                                                        $suplai_layer = floor($check_spreading_process->first()->actual_sisa / $total_length_allow_in_inch);
                                                        // jika supply layer lebih besar dari total layer di marker
                                                        if($suplai_layer > $total_layer_marker){
                                                            // supply layer sama dengan total marker di layer
                                                            $suplai_layer = $total_layer_marker;
                                                            // jika supply layer lebih kecil dari total layer di marker
                                                        }else{
                                                            // supply layer sama dengan supply layer
                                                            $suplai_layer = $suplai_layer;
                                                        }
                                                        // set supply sisa
                                                        $suplai_sisa = $check_spreading_process->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
                                                        // case combine plan : get cutting date
                                                        if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                            $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                                ->whereNull('deleted_at')
                                                                ->pluck('cutting_date')
                                                                ->toArray();
                                                        } else {
                                                            $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                        }
                                                        foreach($get_data_roll as $a => $b){
                                                            if($b->qty_prepared > 5){
                                                                if ($b->planning_date != null) {
                                                                    if($data->second_plan == null){
                                                                        if(!in_array($b->planning_date, $get_cutting_date) && $b->style != 'additional'){
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    }else{
                                                                        if ((!in_array($b->planning_date, $get_cutting_date) && $b->planning_date != $data->second_plan) && $b->style != 'additional') {
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        // INSERT TO TEMPORARY TABLE!
                                                        SpreadingReportTemp::FirstOrCreate([
                                                            'barcode_marker'    => $barcode_marker,
                                                            'barcode_fabric'    => $barcode_fabric,
                                                            'no_roll'           => $check_spreading_process->first()->no_roll,
                                                            'qty_fabric'        => $check_spreading_process->first()->actual_sisa,
                                                            'actual_width'      => null,
                                                            'lot'               => $check_spreading_process->first()->lot,
                                                            'suplai_layer'      => $suplai_layer,
                                                            'actual_layer'      => null,
                                                            'suplai_sisa'       => $suplai_sisa,
                                                            'actual_sisa'       => $check_spreading_process->first()->actual_sisa,
                                                            'akumulasi_layer'   => 0,
                                                            'reject'            => null,
                                                            'sambungan'         => null,
                                                            'actual_use'        => 0,
                                                            'remark'            => null,
                                                            'kualitas_gelaran'  => null,
                                                            'barcode_table'     => $barcode_meja,
                                                            'is_commit'         => 'f',
                                                            'user_id'           => Auth::user()->id,
                                                            'factory_id'        => Auth::user()->factory_id,
                                                            'po_supplier'       => $check_spreading_process->first()->po_supplier,
                                                            'supplier_name'     => $check_spreading_process->first()->supplier_name,
                                                            'item_code'         => $check_spreading_process->first()->item_code,
                                                            'color'             => $check_spreading_process->first()->color,
                                                            'sambungan_end'     => null,
                                                            'item_id'           => $check_spreading_process->first()->item_id,
                                                        ]);
                                                        return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                    }else{
                                                        return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#2', 'barcode_id' => $barcode_marker]);
                                                    }
                                                }else{
                                                    return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#3', 'barcode_id' => $barcode_marker]);
                                                }
                                            }else{
                                                return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                            }
                                        }else{
                                            // get data fabrik yang paling kecil sisanya
                                            $check_fabric = FabricUsed::where('barcode_fabric', $barcode_fabric)->orderBy('created_spreading', 'desc')->get();
                                            // jika fabrik sudah pernah digunakan
                                            if($check_fabric->count() > 0){
                                                // jika sisa fabric lebih besar dari 0
                                                if ($check_fabric->first()->actual_sisa > 0) {
                                                    // set total length allow in inch
                                                    $total_length_allow_in_inch = $total_length_allow / 36;
                                                    // get data marker~
                                                    $data = CuttingMarker::where('barcode_id', $barcode_marker)->first();
                                                    // get data part no
                                                    if(str_contains($data->part_no,'PART')){
                                                        $get_data_part_no = explode('+', $data->part_no);
                                                    }else{
                                                        $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                                    }
                                                    // get data po buyer
                                                    $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                                    $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' AND deleted_at IS NULL"));
                                                    if(count($barcode_roll_check) > 0) {
                                                        $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                                    ->where('part_no', $get_data_part_no[0])
                                                                    ->select('item_id')
                                                                    ->distinct()
                                                                    ->pluck('item_id')->toArray();
                                                        // JIKA TERADPAT PERBEDAAN ITEM ID ANTARA WMS DENGAN CDMS
                                                        if(!in_array($data->item_id, $cek_item)){
                                                            return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                        }
                                                        $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' OR item_id_plan='$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                                        // jika get data roll ada
                                                        if (count($get_data_roll) > 0) {
                                                            // set value supply layer
                                                            $suplai_layer = floor($check_fabric->first()->actual_sisa / $total_length_allow_in_inch);
                                                            // jika supply layer lebih besar dari total layer di marker
                                                            if ($suplai_layer > $total_layer_marker) {
                                                                // supply layer sama dengan total marker di layer
                                                                $suplai_layer = $total_layer_marker;
                                                                // jika supply layer lebih kecil dari total layer di marker
                                                            } else {
                                                                // supply layer sama dengan supply layer
                                                                $suplai_layer = $suplai_layer;
                                                            }
                                                            // case combine plan : get cutting date
                                                            if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                                $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                                    ->whereNull('deleted_at')
                                                                    ->pluck('cutting_date')
                                                                    ->toArray();
                                                            } else {
                                                                $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                            }
                                                            // set supply sisa
                                                            $suplai_sisa = $check_fabric->first()->actual_sisa - ($suplai_layer * $total_length_allow_in_inch);
                                                            $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL OR item_id_plan='$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id"));
                                                            foreach($get_data_roll as $x => $z){
                                                                if($z->qty_prepared > 5){
                                                                    if ($z->planning_date != null) {
                                                                        if($data->second_plan == null) {
                                                                            if (!in_array($z->planning_date, $get_cutting_date) && $z->style != 'additional') {
                                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                            }
                                                                        }else{
                                                                            if((!in_array($z->planning_date, $get_cutting_date) && $z->planning_date != $data->second_plan) && $z->style != 'additional') {
                                                                                return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            
                                                            // insert data ke temporary tabel
                                                            SpreadingReportTemp::FirstOrCreate([
                                                                'barcode_marker' => $barcode_marker,
                                                                'barcode_fabric' => $barcode_fabric,
                                                                'no_roll' => $check_fabric->first()->no_roll,
                                                                'qty_fabric' => $check_fabric->first()->actual_sisa,
                                                                'actual_width' => null,
                                                                'lot' => $check_fabric->first()->lot,
                                                                'suplai_layer' => $suplai_layer,
                                                                'actual_layer' => null,
                                                                'suplai_sisa' => $suplai_sisa,
                                                                'actual_sisa' => $check_fabric->first()->actual_sisa,
                                                                'akumulasi_layer' => 0,
                                                                'reject' => null,
                                                                'sambungan' => null,
                                                                'actual_use' => 0,
                                                                'remark' => null,
                                                                'kualitas_gelaran' => null,
                                                                'barcode_table' => $barcode_meja,
                                                                'is_commit' => 'f',
                                                                'user_id' => Auth::user()->id,
                                                                'factory_id' => Auth::user()->factory_id,
                                                                'po_supplier' => $check_fabric->first()->po_supplier,
                                                                'supplier_name' => $check_fabric->first()->supplier_name,
                                                                'item_code' => $check_fabric->first()->item_code,
                                                                'color' => $check_fabric->first()->color,
                                                                'sambungan_end' => null,
                                                                'item_id' => $check_fabric->first()->item_id,
                                                            ]);
                                                            return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                        } else {
                                                            return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#5', 'barcode_id' => $barcode_marker]);
                                                        }
                                                    } else {
                                                        return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#4', 'barcode_id' => $barcode_marker]);
                                                    }
                                                } else {
                                                    return response()->json(['status' => 442, 'report' => 'Qty fabric sudah habis digunakan!', 'barcode_id' => $barcode_marker]);
                                                }
                                                // jika fabrik belum pernah digunakan
                                            }else{
                                                // set total length allow in inch
                                                $total_length_allow_in_inch = $total_length_allow / 36;
                                                // get data marker
                                                $data = CuttingMarker::where('barcode_id', $barcode_marker)->get()->first();
                                                // get data part no
                                                if(str_contains($data->part_no,'PART')){
                                                    $get_data_part_no = explode('+', $data->part_no);
                                                }else{
                                                    $get_data_part_no = explode('+', str_replace(' ', '', $data->part_no));
                                                }
                                                // // get data po buyer
                                                $get_data_po = $data->cutting_plan->po_details->pluck('po_buyer')->toArray();
                                                $barcode_roll_check = DB::connection('wms_live_new')->select(DB::raw("SELECT * FROM integration_to_cutting_f('$barcode_fabric') WHERE last_status_movement='out' and deleted_at IS NULL"));
                                                if(count($barcode_roll_check) > 0){
                                                    $cek_item = DataCutting::where('plan_id',$data->id_plan)
                                                                ->where('part_no', $get_data_part_no[0])
                                                                ->select('item_id')
                                                                ->distinct()
                                                                ->pluck('item_id')->toArray();  
                                                    if(!in_array($data->item_id, $cek_item))
                                                    {
                                                        return response()->json(['status' => 442, 'report' => 'Ada kesalahan input item id pada permintaan marker!', 'barcode_id' => $barcode_marker]);
                                                    }
                                                    
                                                    $get_data_roll = DB::connection('wms_live_new')->select(DB::raw("SELECT barcode, nomor_roll, actual_width, actual_lot, sum(qty_prepared) as qty_prepared, planning_date, style, item_id,document_no,supplier_name,item_code,color FROM integration_to_cutting_f('$barcode_fabric') WHERE item_id='$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL OR item_id_plan='$data->item_id' AND last_status_movement='out' AND deleted_at IS NULL GROUP BY barcode, nomor_roll, actual_width, actual_lot, planning_date, style, item_id,document_no,supplier_name,item_code,color"));
                                                    // jika terdapat roll
                                                    if ($get_data_roll != null) {
                                                        foreach($get_data_roll as $x => $z){
                                                            // set value supply layer
                                                            $suplai_layer = floor($z->qty_prepared / $total_length_allow_in_inch);
                                                            // jika supply layer lebih besar dari total layer di marker
                                                            if($suplai_layer > $total_layer_marker){
                                                                // supply layer sama dengan total marker di layer
                                                                $suplai_layer = $total_layer_marker;
                                                                // jika supply layer lebih kecil dari total layer di marker
                                                            }else{
                                                                // supply layer sama dengan supply layer
                                                                $suplai_layer = $suplai_layer;
                                                            }

                                                            // set value sisa supply
                                                            $suplai_sisa = $z->qty_prepared - ($suplai_layer * $total_length_allow_in_inch);
                                                            // case combine plan : get cutting date
                                                            if($data->cutting_plan->header_id != null && $data->cutting_plan->is_header) {
                                                                $get_cutting_date = CuttingPlan::where('header_id', $data->cutting_plan->id)
                                                                    ->whereNull('deleted_at')
                                                                    ->pluck('cutting_date')
                                                                    ->toArray();
                                                            }else{
                                                                $get_cutting_date[] = $data->cutting_plan->cutting_date;
                                                            }

                                                            if($z->qty_prepared == null){
                                                                return response()->json(['status' => 442, 'report' => 'Terjadi kesalahan, silakan kontak ICT!', 'barcode_id' => $barcode_marker]);
                                                            }
                                                            if ($z->qty_prepared > 5) {
                                                                if ($z->planning_date != null) {
                                                                    if($data->second_plan == null) {
                                                                        if (!in_array($z->planning_date, $get_cutting_date) && $z->style != 'additional') {
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    } else {
                                                                        if ((!in_array($z->planning_date, $get_cutting_date) && $z->planning_date != $data->second_plan) && $z->style != 'additional') {
                                                                            return response()->json(['status' => 442, 'report' => 'plan fabric tidak sesuai dengan plan marker!', 'barcode_id' => $barcode_marker]);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            //INSERT DATA TO TEMPORARY TABLE
                                                            SpreadingReportTemp::FirstOrCreate([
                                                                'barcode_marker'    => $barcode_marker,
                                                                'barcode_fabric'    => $barcode_fabric,
                                                                'no_roll'           => $z->nomor_roll,
                                                                'qty_fabric'        => $z->qty_prepared,
                                                                'actual_width'      => null,
                                                                'lot'               => $z->actual_lot,
                                                                'suplai_layer'      => $suplai_layer,
                                                                'actual_layer'      => null,
                                                                'suplai_sisa'       => $suplai_sisa,
                                                                'actual_sisa'       => $z->qty_prepared,
                                                                'akumulasi_layer'   => 0,
                                                                'reject'            => null,
                                                                'sambungan'         => null,
                                                                'actual_use'        => 0,
                                                                'remark'            => null,
                                                                'kualitas_gelaran'  => null,
                                                                'barcode_table'     => $barcode_meja,
                                                                'is_commit'         => 'f',
                                                                'user_id'           => Auth::user()->id,
                                                                'factory_id'        => Auth::user()->factory_id,
                                                                'po_supplier'       => $z->document_no,
                                                                'supplier_name'     => $z->supplier_name,
                                                                'item_code'         => $z->item_code,
                                                                'color'             => $z->color,
                                                                'sambungan_end'     => null,
                                                                'item_id'           => $z->item_id,
                                                            ]);
                                                        }
                                                        return response()->json(['status' => 200, 'report' => null, 'barcode_id' => $barcode_marker]);
                                                    }else{
                                                        return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#7', 'barcode_id' => $barcode_marker]);
                                                    }
                                                }else{
                                                    return response()->json(['status' => 442, 'report' => 'Barcode Belum Scan Out Warehouse!#6', 'barcode_id' => $barcode_marker]);
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    return response()->json(['status' => 442, 'report' => 'Barcode Belum Diterima Cutting!', 'barcode_id' => $barcode_marker]);
                                }
                            }
                        }
                    }
                }
            }else{
                return response()->json(['status' => 442, 'report' => 'Barcode Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
            }
        }
    }

    public function updateActualWidth(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $actual_width = $request->actual_width;
            if ($id != null && $actual_width != null) {
                $data = SpreadingReportTemp::where('id', $id)->update(['actual_width' => $actual_width]);
                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateActualLayer(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $actual_layer = $request->actual_layer;
            $total_length_allow = $request->detail_view_allow;
            if ($id != null && $actual_layer != null && $barcode_marker != null) {
                $total_length_allow_in_inch = $total_length_allow / 36;
                $actual_use = $total_length_allow_in_inch * $actual_layer;
                $data_query = SpreadingReportTemp::where('id', $id);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if ($data_row != null) {

                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;
                    $reject = $data_row->reject != null ? $data_row->reject : 0;

                    $actual_use = $actual_use + $sambungan_end + $sambungan;
                    $actual_sisa = $data_row->qty_fabric - ($actual_use + $reject);

                    if($actual_sisa > 10) {
                        $remark = 0;
                        $actual_sisa = $actual_sisa;
                    } else {
                        if($actual_sisa > 0) {
                            $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                        } else {
                            $actual_sisa = 0;
                            $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                        }
                    }
                }

                $data = $data_query->update(['actual_layer' => $actual_layer, 'actual_use' => $actual_use, 'actual_sisa' => $actual_sisa, 'remark' => $remark]);

                $data_spreading = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->orderBy('created_at', 'asc')->get();

                $increment_layer_add = 0;

                foreach ($data_spreading as $key => $a) {
                    $akumulasi_layer = 0;
                    for ($i = 0; $i <= $key; $i++) {
                        $akumulasi_layer += $data_spreading[$i]->actual_layer;
                    }

                    if ($key != 0) {
                        if ($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                            $increment_layer_add = $increment_layer_add + 1;
                        }
                    }

                    $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                    SpreadingReportTemp::where('id', $a->id)->update(['akumulasi_layer' => $akumulasi_layer]);
                }
                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateActualSisa(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $actual_sisa_ = $request->actual_sisa;
            $total_length_allow = $request->detail_view_allow;
            if ($id != null && $barcode_marker != null) {

                if($actual_sisa_ == null || $actual_sisa_ == '') {
                    $actual_sisa_ = 0;
                }

                $total_length_allow_in_inch = $total_length_allow / 36;
                $data_query = SpreadingReportTemp::where('id', $id);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if ($data_row != null) {

                    $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                    $actual_use = $total_length_allow_in_inch * $actual_layer;

                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;
                    $reject = $data_row->reject != null ? $data_row->reject : 0;

                    $actual_use = $actual_use + $sambungan_end + $sambungan;
                    $remark = ($actual_use + $reject + $actual_sisa_) - $data_row->qty_fabric;
                }

                $data = $data_query->update(['actual_sisa' => $actual_sisa_, 'remark' => $remark]);
                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateReject(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $reject = $request->reject;
            $total_length_allow = $request->detail_view_allow;
            if ($id != null && $barcode_marker != null) {

                if($reject == null || $reject == '') {
                    $reject = 0;
                }

                $total_length_allow_in_inch = $total_length_allow / 36;
                $data_query = SpreadingReportTemp::where('id', $id);
                $data_row = $data_query->get()->first();

                $remark = 0;

                if ($data_row != null) {

                    $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                    $actual_use = $total_length_allow_in_inch * $actual_layer;

                    $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;
                    $actual_sisa = $data_row->actual_sisa != null ? $data_row->actual_sisa : 0;
                    $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                    $actual_use = $actual_use + $sambungan_end + $sambungan;
                    $actual_sisa = $data_row->qty_fabric - ($actual_use + $reject);

                    if($actual_sisa > 10) {
                        $remark = 0;
                        $actual_sisa = $actual_sisa;
                    } else {
                        if($actual_sisa > 10) {
                            $remark = 0;
                            $actual_sisa = $actual_sisa;
                        } else {
                            if($actual_sisa > 0) {
                                $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                            } else {
                                $actual_sisa = 0;
                                $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                            }
                        }
                    }
                }

                $data = $data_query->update(['reject' => $reject, 'remark' => $remark, 'actual_sisa' => $actual_sisa]);
                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateSambungan(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $sambungan = $request->sambungan;
            $total_length_allow = $request->detail_view_allow;

            if ($id != null && $barcode_marker != null) {

                try {
                    DB::beginTransaction();

                    if($sambungan == null || $sambungan == '') {
                        $sambungan = 0;
                    }

                    $total_length_allow_in_inch = $total_length_allow / 36;
                    $data_query = SpreadingReportTemp::where('id', $id);
                    $data_row = $data_query->get()->first();

                    $remark = 0;

                    if ($data_row != null) {

                        $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                        $actual_use = $total_length_allow_in_inch * $actual_layer;

                        $reject = $data_row->reject != null ? $data_row->reject : 0;
                        $sambungan_end = $data_row->sambungan_end != null ? $data_row->sambungan_end : 0;

                        $actual_use = $actual_use + $sambungan_end + $sambungan;
                        $actual_sisa = $data_row->qty_fabric - ($actual_use + $reject);

                        if($actual_sisa > 10) {
                            $remark = 0;
                            $actual_sisa = $actual_sisa;
                        } else {
                            if($actual_sisa > 0) {
                                $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                            } else {
                                $actual_sisa = 0;
                                $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                            }
                        }
                    }

                    $data = $data_query->update(['sambungan' => $sambungan, 'remark' => $remark, 'actual_sisa' => $actual_sisa, 'actual_use' => $actual_use]);

                    $data_spreading = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->orderBy('created_at', 'asc')->get();

                    $increment_layer_add = 0;

                    foreach ($data_spreading as $key => $a) {
                        $akumulasi_layer = 0;
                        for ($i = 0; $i <= $key; $i++) {
                            $akumulasi_layer += $data_spreading[$i]->actual_layer;
                        }

                        if ($key != 0) {
                            if ($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                                $increment_layer_add = $increment_layer_add + 1;
                            }
                        }

                        $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                        SpreadingReportTemp::where('id', $a->id)->update(['akumulasi_layer' => $akumulasi_layer]);
                    }

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateSambunganEnd(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $sambungan_end = $request->sambungan_end;
            $total_length_allow = $request->detail_view_allow;

            if ($id != null && $barcode_marker != null) {

                try {
                    DB::beginTransaction();

                    if($sambungan_end == null || $sambungan_end == '') {
                        $sambungan_end = 0;
                    }

                    $total_length_allow_in_inch = $total_length_allow / 36;
                    $data_query = SpreadingReportTemp::where('id', $id);
                    $data_row = $data_query->get()->first();

                    $remark = 0;

                    if ($data_row != null) {

                        $actual_layer = $data_row->actual_layer != null ? $data_row->actual_layer : 0;
                        $actual_use = $total_length_allow_in_inch * $actual_layer;

                        $reject = $data_row->reject != null ? $data_row->reject : 0;
                        $sambungan = $data_row->sambungan != null ? $data_row->sambungan : 0;

                        $actual_use = $actual_use + $sambungan_end + $sambungan;
                        $actual_sisa = $data_row->qty_fabric - ($actual_use + $reject);

                        if($actual_sisa > 10) {
                            $remark = 0;
                            $actual_sisa = $actual_sisa;
                        } else {
                            if($actual_sisa > 10) {
                                $remark = 0;
                                $actual_sisa = $actual_sisa;
                            } else {
                                if($actual_sisa > 0) {
                                    $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                                } else {
                                    $actual_sisa = 0;
                                    $remark = ($actual_use + $reject + $actual_sisa) - $data_row->qty_fabric;
                                }
                            }
                        }
                    }

                    $data = $data_query->update(['sambungan_end' => $sambungan_end, 'remark' => $remark, 'actual_use' => $actual_use, 'actual_sisa' => $actual_sisa]);

                    $data_spreading = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->orderBy('created_at', 'asc')->get();

                    $increment_layer_add = 0;

                    foreach ($data_spreading as $key => $a) {
                        $akumulasi_layer = 0;
                        for ($i = 0; $i <= $key; $i++) {
                            $akumulasi_layer += $data_spreading[$i]->actual_layer;
                        }

                        if ($key != 0) {
                            if ($total_length_allow_in_inch <= ($data_spreading[$key]->sambungan + $data_spreading[$key - 1]->sambungan_end)) {
                                $increment_layer_add = $increment_layer_add + 1;
                            }
                        }

                        $akumulasi_layer = $akumulasi_layer + $increment_layer_add;

                        SpreadingReportTemp::where('id', $a->id)->update(['akumulasi_layer' => $akumulasi_layer]);
                    }

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function updateKualitasGelaran(Request $request)
    {
        if (request()->ajax()) {
            $id = $request->id;
            $barcode_marker = $request->barcode_marker;
            $kualitas_gelaran = $request->kualitas_gelaran;
            if ($id != null && $barcode_marker != null) {
                $data_query = SpreadingReportTemp::where('id', $id);
                $data_row = $data_query->get()->first();

                $data = $data_query->update(['kualitas_gelaran' => $kualitas_gelaran]);
                return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data, 'barcode_marker' => null], 442);
            }
        }
    }

    public function deleteReportSpreadingTemp(Request $request)
    {
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            if ($barcode_marker != null) {
                $data = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->delete();
                return response()->json(['data' => $data], 200);
            } else {
                $data = null;
                return response()->json(['data' => $data], 442);
            }
        }
    }

    public function deleteLastFabricScan(Request $request)
    {
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            if ($barcode_marker != null) {
                $data = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->orderBy('created_at', 'desc')->first();
                if ($data != null) {
                    $data->delete();
                    return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
                } else {
                    return response()->json(['status' => 442, 'report' => 'Tidak Ada Fabric Yang Dihapus!', 'barcode_id' => $barcode_marker]);
                }
            } else {
                return response()->json(['status' => 442, 'report' => 'Barcode Tidak ditemukan!', 'barcode_id' => $barcode_marker]);
            }
        }
    }

    public function spreadingReportSave(Request $request)
    {
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            $barcode_table = $request->barcode_table;
            if ($barcode_marker != null) {
                $data = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->get();
                if ($data->count() > 0) {

                    foreach ($data as $a) {
                        if ($a->actual_width == null) {
                            return response()->json(['status' => 442, 'report' => 'Lebar Aktual Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } elseif ($a->actual_layer == null) {
                            return response()->json(['status' => 442, 'report' => 'Aktual Layer Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } elseif ($a->actual_sisa == null) {
                            return response()->json(['status' => 442, 'report' => 'Aktual Sisa Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } else {
                        }
                    }

                    foreach ($data as $a) {
                        SpreadingReportTemp::where('barcode_marker', $barcode_marker)->update([
                            'is_commit' => 't',
                        ]);
                    }

                    return response()->json(['data' => $data, 'barcode_marker' => $barcode_marker], 200);
                } else {
                    return response()->json(['status' => 442, 'report' => 'Belum ada fabric yang diinput!', 'barcode_id' => $barcode_marker]);
                }
            } else {
                return response()->json(['status' => 442, 'report' => 'Barcode Tidak ditemukan!', 'barcode_id' => $barcode_marker]);
            }
        }
    }

    public function spreadingReportInsert(Request $request)
    {
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            $barcode_table = $request->barcode_table;
            $layer = $request->layer;
            if ($barcode_marker != null) {
                $data = SpreadingReportTemp::where('barcode_marker', $barcode_marker)->get();
                if ($data->count() > 0) {

                    foreach ($data as $a) {
                        if ($a->actual_width == null) {
                            return response()->json(['status' => 442, 'report' => 'Lebar Aktual Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } elseif ($a->actual_layer == null) {
                            return response()->json(['status' => 442, 'report' => 'Aktual Layer Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } elseif ($a->actual_sisa == null) {
                            return response()->json(['status' => 442, 'report' => 'Aktual Sisa Tidak Boleh Kosong!', 'barcode_id' => $barcode_marker]);
                        } else {
                        }
                    }

                    try {
                        DB::begintransaction();

                        foreach ($data as $a) {
                            FabricUsed::FirstOrCreate([
                                'barcode_marker'           => $barcode_marker,
                                'barcode_fabric'           => $a->barcode_fabric,
                                'no_roll'                  => $a->no_roll,
                                'qty_fabric'               => $a->qty_fabric,
                                'actual_width'             => $a->actual_width,
                                'lot'                      => $a->lot,
                                'suplai_layer'             => $a->suplai_layer,
                                'actual_layer'             => $a->actual_layer,
                                'suplai_sisa'              => $a->suplai_sisa,
                                'actual_sisa'              => $a->actual_sisa,
                                'akumulasi_layer'          => $a->akumulasi_layer,
                                'reject'                   => $a->reject,
                                'sambungan'                => $a->sambungan,
                                'actual_use'               => $a->actual_use,
                                'remark'                   => $a->remark,
                                'kualitas_gelaran'         => $a->kualitas_gelaran,
                                'barcode_table'            => $a->barcode_table,
                                'user_id'                  => $a->user_id,
                                'created_spreading'        => $a->created_at,
                                'factory_id'               => $a->factory_id,
                                'po_supplier'              => $a->po_supplier,
                                'supplier_name'            => $a->supplier_name,
                                'item_code'                => $a->item_code,
                                'color'                    => $a->color,
                                'sambungan_end'            => $a->sambungan_end,
                                'item_id'                  => $a->item_id,
                                'spreading_report_temp_id' => $a->id,
                            ]);
                        }

                        CuttingMovement::firstOrCreate([
                            'barcode_id' => $barcode_marker,
                            'process_from' => 'preparation',
                            'status_from' => 'completed',
                            'process_to' => 'spreading',
                            'status_to' => 'onprogress',
                            'is_canceled' => false,
                            'user_id' => \Auth::user()->id,
                            'description' => 'input spreading',
                            'ip_address' => $this->getIPClient(),
                            'barcode_type' => 'spreading',
                            'deleted_at' => null
                        ]);

                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }

                    return response()->json(['status' => 200, 'data' => $data, 'barcode_marker' => $barcode_marker], 200);
                } else {
                    return response()->json(['status' => 442, 'report' => 'Belum ada fabric yang diinput!', 'barcode_id' => $barcode_marker]);
                }
            } else {
                return response()->json(['status' => 442, 'report' => 'Barcode Tidak ditemukan!', 'barcode_id' => $barcode_marker]);
            }
        }
    }

    public function approvalSpreading()
    {
        return view('spreading/approval_spreading/approval_gl');
    }

    public function getApprovalSpreading(Request $request)
    {

        // $pcd = Carbon::createFromFormat('d F, Y', $request->pcd)->format('Y-m-d');
        $spread_ = $request->spread;
        if (!is_null($spread_)) {
            $factory_id = Auth::user()->factory_id;
            $spread = Carbon::createFromFormat('d M, Y',$spread_)->format('Y-m-d');
            // $data = DB::table('hs_spreading_approval')
            //     ->where('spread_date', $spread)
            //     // ->orWhere('spread_date2', $spread)
            //     // ->where(function($query)use($spread){
            //     //     $query->orWhere('spread_date',$spread);
            //     //     $query->orWhere('spread_date2',$spread);
            //     // })
            //     ->where('factory_id',\Auth::user()->factory_id);
            $data = DB::select(DB::raw("SELECT * FROM(SELECT 
            a.barcode_id,a.id_plan,b.cutting_date,b.style,b.articleno,b.size_category,b.top_bot,b.season,b.factory_id,a.part_no,a.cut,
            a.layer,a.fabric_width,a.fab_cons,a.marker_length,a.over_consum,a.need_total,a.qty_ratio,a.qty_fabric,a.item_id,a.bay_pass,
            a.gl_status,sa2.name AS gl_user,a.qc_status,sa.name AS qc_user,fu.user_id,c.name,mt.id_table,mt.table_name,b.queu,
            COALESCE(fu.actual_use, srt.actual_use2) AS actual_use,
            COALESCE(fu.spread_date, srt.spread_date2) AS spread_date,
            fu.time_stamp,
            fu.spread_start,
            COALESCE(fu.akumulasi_layer, srt.akumulasi_layer2) AS akumulasi_layer,
            a.layer_plan,
            dc.color
            --CUTTING MARKER
            FROM cutting_marker a
            --CUTTING PLAN
            LEFT JOIN cutting_plans2 b ON a.id_plan = b.id
            --FABRIC USED
            LEFT JOIN(SELECT fus.barcode_marker,max(fus.user_id) AS user_id,fus.barcode_table,sum(fus.actual_use) AS actual_use,date(fus.created_at) AS spread_date,max(fus.created_spreading) AS time_stamp,min(fus.created_spreading) AS spread_start,max(fus.akumulasi_layer) AS akumulasi_layer 
            FROM fabric_useds fus
            where date(fus.created_at) = '$spread' and fus.factory_id='$factory_id'
            GROUP BY fus.barcode_marker, fus.barcode_table,date(fus.created_at)) fu ON a.barcode_id = fu.barcode_marker
            --SPREADING REPORT TEMP
            LEFT JOIN (SELECT srtt.barcode_marker,
            min(srtt.user_id) AS user_id,
            srtt.barcode_table,
            sum(srtt.actual_use) AS actual_use2,
            min(date(srtt.created_at)) AS spread_date2,
            max(srtt.akumulasi_layer) AS akumulasi_layer2
            FROM spreading_report_temp srtt
            WHERE srtt.is_commit = 't' and date(srtt.created_at) = '$spread' and srtt.factory_id='$factory_id'
            GROUP BY srtt.barcode_marker, srtt.barcode_table) srt ON a.barcode_id = srt.barcode_marker
            --USER
            LEFT JOIN users c ON fu.user_id = c.id OR srt.user_id = c.id
            --MASTER TABLE
            LEFT JOIN master_table mt ON fu.barcode_table = mt.id_table OR srt.barcode_table = mt.id_table
            --DATA CUTTING
            LEFT JOIN (SELECT DISTINCT dcs.color_name AS color,dcs.plan_id,dcs.part_no FROM data_cuttings dcs WHERE dcs.plan_id IN(
            (SELECT DISTINCT cm.id_plan FROM(SELECT DISTINCT
            srt1.barcode_marker
            FROM spreading_report_temp srt1
            WHERE srt1.is_commit = 't' and date(srt1.created_at) = '$spread' and srt1.factory_id='$factory_id'
            UNION
            SELECT DISTINCT
            fus.barcode_marker
            FROM fabric_useds fus
            where date(fus.created_at) = '$spread' and fus.factory_id='$factory_id')x
            JOIN cutting_marker cm on cm.barcode_id = x.barcode_marker AND cm.deleted_at IS NULL)
            )) dc ON dc.plan_id = a.id_plan AND dc.part_no = a.part_no
            --SPREADING APPROVAL
            LEFT JOIN (SELECT sap.id_marker,sap.user_id,users.name
            FROM spreading_approval sap
            LEFT JOIN users ON users.id = sap.user_id
            WHERE sap.apv_type = 'QC Spreading') sa ON sa.id_marker = a.barcode_id
            LEFT JOIN 
            (SELECT sap2.id_marker,sap2.user_id,users.name FROM spreading_approval sap2
            LEFT JOIN users ON users.id = sap2.user_id
            WHERE sap2.apv_type = 'GL Spreading') sa2 ON sa2.id_marker = a.barcode_id
            WHERE a.barcode_id IN (SELECT DISTINCT fabric_useds.barcode_marker FROM fabric_useds WHERE date(fabric_useds.created_at) = '$spread' and fabric_useds.factory_id='$factory_id' UNION SELECT DISTINCT spreading_report_temp.barcode_marker FROM spreading_report_temp where date(spreading_report_temp.created_at) = '$spread' AND spreading_report_temp.factory_id='$factory_id'))z WHERE z.spread_date = '$spread' AND z.factory_id = '$factory_id' ORDER BY z.cutting_date,z.style,z.articleno"));
        } else {
            $data = array();
        }

        return datatables::of($data)
            ->editColumn('actual_use', function ($data) {
                return round($data->actual_use, 2);
            })
            ->editColumn('gl_status', function ($data) {
                if($data->gl_status == null){
                    return $data->gl_status;
                }else{
                    return ucwords($data->gl_status).' ('.$data->gl_user.')';
                }
            })
            ->editColumn('qc_status', function ($data) {
                if($data->qc_status == null){
                    return $data->qc_status;
                }
                else{
                    return ucwords($data->qc_status).' ('.$data->qc_user.')';
                }
            })
            ->editColumn('cutting_date', function ($data) {
                return Carbon::parse($data->cutting_date)->format('d-M');
            })
            ->addColumn('action', function ($data) {
                return '<button class="btn btn-info btn-detail"  id="btn-detail" data-id="' . $data->barcode_id . '" onclick="detail(this);"><span class="icon-shutter"></span></button>';
            })
            ->make(true);
    }

    public function ajaxGetSpreadingApv(Request $request)
    {
        $barcode_id = $request->barcode;

        $get       = DB::table('hs_spreading_approval')->where('barcode_id', $barcode_id)->first();
        $po_buyer  = implode(', ', MarkerDetailPO::select('po_buyer')->where('barcode_id', $barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray());
        $item_code = DB::table('ns_master_items')->select('item_code')->where('item_id', $get->item_id)->first();
        $ratio     = DB::table('ns_trig_ratio')->where('id_marker', $get->barcode_id)->first();
        $ms        = MarkerSpreading::where('id_marker', $barcode_id)->first();

        if ($ms != null) {
            $data_response = [
                'barcode_id'    => $get->barcode_id,
                'plan'          => $get->cutting_date,
                'style'         => $get->style,
                'article'       => $get->articleno,
                'part'          => $get->part_no,
                'cut'           => $get->cut,
                'layer'         => $get->layer,
                'layer_plan'    => $get->layer_plan,
                'color'         => $get->color,
                'marker_width'  => $get->fabric_width - 0.5,
                'marker_length' => $get->marker_length,
                'total_length'  => $get->marker_length + 0.75,
                'item_id'       => $get->item_id,
                'item_code'     => $item_code->item_code,
                'gl_status'     => $get->gl_status,
                'qc_status'     => $get->qc_status,
                'name'          => $get->name,
                'table'         => $get->table_name,
                'po'            => $po_buyer,
                'ratio'         => $ratio->ratio,
                'qty_ratio'     => $get->akumulasi_layer,
                'a_length'      => $ms->actual_length_qc,
                'a_width'       => $ms->actual_width_qc,
                's_high'        => $ms->spreading_high_qc,
                'remark_note'   => $ms->remark_qc,
                'is_commit'     => $get->spread_start == null ? 0 : 1,
            ];
        } else {
            $data_response = [
                'barcode_id'    => $get->barcode_id,
                'plan'          => $get->cutting_date,
                'style'         => $get->style,
                'article'       => $get->articleno,
                'part'          => $get->part_no,
                'cut'           => $get->cut,
                'layer'         => $get->layer,
                'layer_plan'    => $get->layer_plan,
                'color'         => $get->color,
                'marker_width'  => $get->fabric_width - 0.5,
                'marker_length' => $get->marker_length,
                'total_length'  => $get->marker_length + 0.75,
                'item_id'       => $get->item_id,
                'item_code'     => $item_code->item_code,
                'gl_status'     => $get->gl_status,
                'qc_status'     => $get->qc_status,
                'name'          => $get->name,
                'table'         => $get->table_name,
                'po'            => $po_buyer,
                'ratio'         => $ratio->ratio,
                'qty'           => $get->qty_ratio,
                'qty_ratio'     => $get->akumulasi_layer,
                'a_length'      => null,
                'a_width'       => null,
                's_high'        => null,
                'remark_note'   => null,
                'is_commit'     => $get->spread_start == null ? 0 : 1,
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function ajaxFabricSpreadingApv(Request $request)
    {
        $barcode_id = $request->barcode;

        $data = DB::table('fabric_useds')
            ->where('barcode_marker', $barcode_id)
            ->whereNull('deleted_at')
            ->orderBy('created_spreading', 'asc');
        return Datatables::of($data)
            ->make(true);
    }

    public function ajaxApproveMarker(Request $request)
    {
        $barcode_id = $request->barcode;
        $status = $request->status;
        $layer = $request->layer;

        try {
            db::begintransaction();

            $get_last_input = FabricUsed::where('barcode_marker', $barcode_id)->orderBy('akumulasi_layer', 'desc')->get();

            if($status == 'approve') {
                if ($get_last_input->first()->akumulasi_layer != $layer) {

                    $id_row_new = '';

                    $data_cutting_all = CuttingMarker::where('barcode_id', 'like', 'MK-' . Carbon::now()->format('ymd') . '%')->orderBy('barcode_id', 'desc')->get();

                    if (count($data_cutting_all) > 0) {
                        $id_row_new = substr($data_cutting_all->first()->barcode_id, 3, 12) + 1;
                        $id_row_new = 'MK-' . $id_row_new;
                    } else {
                        $id_row_new = 'MK-' . Carbon::now()->format('ymd') . '000001';
                    }

                    // get data marker lama

                    $marker_lama = CuttingMarker::where('barcode_id', $barcode_id)->where('deleted_at', null)->first();

                    // get last cut

                    $last_cut_num = CuttingMarker::where('id_plan', $marker_lama->id_plan)->where('part_no', $marker_lama->part_no)->where('deleted_at', null)->orderBy('cut', 'desc')->first();

                    // need total lama

                    $need_total_lama = ($marker_lama->marker_length + 0.75) * $get_last_input->first()->akumulasi_layer / 36;

                    // need total baru

                    $need_total_baru = ($marker_lama->marker_length + 0.75) * ($marker_lama->layer - $get_last_input->first()->akumulasi_layer) / 36;

                    // insert data marker baru

                    DB::table('cutting_marker')->insert([
                        'barcode_id' => $id_row_new,
                        'id_plan' => $marker_lama->id_plan,
                        'part_no' => $marker_lama->part_no,
                        'cut' => $last_cut_num->cut + 1,
                        'color' => $marker_lama->color,
                        'layer' => $marker_lama->layer - $get_last_input->first()->akumulasi_layer,
                        'layer_plan' => $marker_lama->layer - $get_last_input->first()->akumulasi_layer,
                        'fabric_width' => $marker_lama->fabric_width,
                        'fab_cons' => $marker_lama->fab_cons,
                        'perimeter' => $marker_lama->perimeter,
                        'marker_length' => $marker_lama->marker_length,
                        'over_consum' => $marker_lama->over_consum,
                        'need_total' => $need_total_baru,
                        'qty_ratio' => $marker_lama->qty_ratio,
                        'qty_fabric' => $marker_lama->qty_fabric,
                        'fabric_lot' => $marker_lama->fabric_lot,
                        'remark' => $marker_lama->remark,
                        'item_id' => $marker_lama->item_id,
                        'bay_pass' => $marker_lama->bay_pass,
                        'is_new' => 'newreport',
                        'is_body' => $marker_lama->is_body,
                        'ratio_print' => $marker_lama->ratio_print,
                        'set_type' => $marker_lama->set_type,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'created_by'=>\Auth::user()->id
                    ]);

                    // update data marker lama

                    DB::table('cutting_marker')->where('barcode_id', $barcode_id)->where('deleted_at', null)->update([
                        'layer'      => $get_last_input->first()->akumulasi_layer,
                        'need_total' => $need_total_lama,
                        'updated_at' => Carbon::now()
                    ]);

                    // get ratio lama

                    $ratio_lama = RasioMarker::where('id_marker', $barcode_id)->where('deleted_at', null)->get();

                    // duplicate ratio lama

                    foreach ($ratio_lama as $a) {
                        RasioMarker::firstOrCreate([
                            'id_marker' => $id_row_new,
                            'ratio'     => $a->ratio,
                            'size'      => $a->size,
                        ]);
                    }

                    // get ratio baru

                    $ratio_baru = RasioMarker::where('id_marker', $id_row_new)->where('deleted_at', null)->get();

                    // set part no

                    $part_no = explode('+', $marker_lama->part_no);

                    // menentukan urutan PO

                    // $urutan_po_buyer = DB::table('jaz_detail_size_per_part')
                    // ->select(DB::raw("po_buyer, sum(qty) as qty, deliv_date, factory_id"))
                    // ->where('cutting_date', $marker_lama->cutting_plan->cutting_date)
                    // ->where('queu', $marker_lama->cutting_plan->queu)
                    // ->where('factory_id', $marker_lama->cutting_plan->factory_id)
                    // ->where('part_no', $part_no[0])
                    // ->groupBy('po_buyer', 'deliv_date', 'factory_id')
                    // ->orderBy('deliv_date', 'asc')
                    // ->orderBy('qty', 'asc')
                    // ->pluck('po_buyer')->toArray();

                    // foreach ratio baru

                    $test = array();

                    foreach ($ratio_lama as $b) {

                        // set qty max lama

                        $qty_max_lama = $b->ratio * $get_last_input->first()->akumulasi_layer;

                        $test[] = 'lama: ' . $qty_max_lama; // test

                        // set qty max baru

                        $qty_max_baru = $b->ratio * ($layer - $get_last_input->first()->akumulasi_layer);

                        $test[] = 'baru: ' . $qty_max_baru; // test

                        // get po in ratio

                        $po_in_ratio = MarkerDetailPO::where('plan_id', $marker_lama->id_plan)->where('barcode_id', $barcode_id)->where('size', $b->size)->where('is_sisa', 'f')->where('deleted_at', null)->pluck('po_buyer')->toArray();

                        // menentukan urutan po
                        $urutan_po_buyer = DB::table('jaz_detail_size_per_part_2')
                        ->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))
                        ->where('cutting_date', $marker_lama->cutting_plan->cutting_date)
                        ->where('queu', $marker_lama->cutting_plan->queu)
                        ->where('factory_id', $marker_lama->cutting_plan->factory_id)
                        ->where('part_no', $part_no[0])
                        ->where('size_finish_good', $b->size)
                        ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , size_finish_good, deliv_date, qty"))
                        ->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')
                        ->pluck('po_buyer')
                        ->toArray();

                        foreach ($urutan_po_buyer as $c) {

                            // check po in ratio atau tidak

                            if (in_array($c, $po_in_ratio)) {

                                $get_data_ratio = MarkerDetailPO::where('plan_id', $marker_lama->id_plan)->where('barcode_id', $barcode_id)->where('size', $b->size)->where('is_sisa', 'f')->where('po_buyer', $c)->where('deleted_at', null)->first();

                                // jika qty max untuk marker lama lebih besar dari qty ratio maka

                                if ($qty_max_lama >= $get_data_ratio->qty) {

                                    $qty_max_lama = $qty_max_lama - $get_data_ratio->qty;
                                } else {

                                    if ($qty_max_lama > 0) {

                                        MarkerDetailPO::firstOrCreate([
                                            'barcode_id' => $id_row_new,
                                            'po_buyer'   => $c,
                                            'qty'        => $get_data_ratio->qty - $qty_max_lama,
                                            'plan_id'    => $marker_lama->id_plan,
                                            'ratio_id'   => $get_data_ratio->id,
                                            'size'       => $b->size,
                                            'is_sisa'    => 'f',
                                            'part_no'    => $get_data_ratio->part_no,
                                        ]);

                                        $get_data_ratio->update([
                                            'qty'        => $qty_max_lama,
                                            'updated_at' => Carbon::now()
                                        ]);

                                        $qty_max_lama = 0;
                                    } else {

                                        MarkerDetailPO::firstOrCreate([
                                            'barcode_id' => $id_row_new,
                                            'po_buyer'   => $c,
                                            'qty'        => $get_data_ratio->qty,
                                            'plan_id'    => $marker_lama->id_plan,
                                            'ratio_id'   => $get_data_ratio->id,
                                            'size'       => $b->size,
                                            'is_sisa'    => 'f',
                                            'part_no'    => $get_data_ratio->part_no,
                                        ]);

                                        // $get_data_ratio->delete();

                                        $get_data_ratio->update([
                                            'deleted_at' => Carbon::now()
                                        ]);
                                    }
                                }
                            }
                        }
                    }

                    $get_sisa_lama = MarkerDetailPO::where('plan_id', $marker_lama->id_plan)
                    ->where('barcode_id', $barcode_id)
                    ->where('is_sisa', 't')
                    ->where('deleted_at', null)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);


                    CuttingMovement::firstOrCreate([
                        'barcode_id' => $id_row_new,
                        'process_from' => null,
                        'status_from' => null,
                        'process_to' => 'preparation',
                        'status_to' => 'onprogress',
                        'is_canceled' => false,
                        'user_id' => \Auth::user()->id,
                        'description' => 'pecah marker dari ' . $barcode_id,
                        'ip_address' => $this->getIPClient(),
                        'barcode_type' => 'spreading',
                        'deleted_at' => null
                    ]);

                    CuttingMovement::firstOrCreate([
                        'barcode_id' => $barcode_id,
                        'process_from' => 'spreading',
                        'status_from' => 'onprogress',
                        'process_to' => 'spreading',
                        'status_to' => 'completed',
                        'is_canceled' => false,
                        'user_id' => \Auth::user()->id,
                        'description' => 'laporan baru ' . $id_row_new,
                        'ip_address' => $this->getIPClient(),
                        'barcode_type' => 'spreading',
                        'deleted_at' => null
                    ]);
                } else {
                    CuttingMovement::firstOrCreate([
                        'barcode_id' => $barcode_id,
                        'process_from' => 'spreading',
                        'status_from' => 'onprogress',
                        'process_to' => 'spreading',
                        'status_to' => 'completed',
                        'is_canceled' => false,
                        'user_id' => \Auth::user()->id,
                        'description' => 'normal marker',
                        'ip_address' => $this->getIPClient(),
                        'barcode_type' => 'spreading',
                        'deleted_at' => null
                    ]);
                }
            }

            $dtin = array(
                'status' => $status,
                'user_id' => Auth::user()->id,
                'id_marker' => $barcode_id,
                'apv_type' => "GL Spreading",
                'created_at' => Carbon::now()
            );
            DB::table('cutting_marker')->where('barcode_id', $barcode_id)->whereNull('deleted_at')->update(['gl_status' => $status]);
            SpreadingApproval::FirstOrCreate($dtin);

            $data_response = [
                'status' => 200,
                'output' => 'Marker ' . $barcode_id . ' has ' . $status . ' !!'
            ];
            db::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                'status' => 422,
                'output' => $status . ' marker field'
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function approvalSpreadingQc()
    {
        return view('spreading/approval_spreading/approval_qc');
    }

    public function ajaxFabricQcSpreading(Request $request)
    {
        $data = DB::table('ns_detail_qc_spreading_1')->where('barcode_marker', $request->barcode);

        if($data->count() == 0){
            $data = DB::table('hs_detail_qc_spreading_temp')->where('barcode_marker', $request->barcode);
        }

        return datatables::of($data)
            ->editColumn('id', function ($data) {
                return  '<input type="text" class="idt form-control" id="idt" value="' . $data->id . '" readonly></input>';
            })
            ->addColumn('l_actual', function ($data) {
                return $data->qty_fabric + $data->remark;
            })
            ->addColumn('w_actual', function ($data) {
                return $data->actual_width;
            })
            ->addColumn('point_fir', function ($data) {
                $get_fir_point = DB::connection('wms_live')->table('material_checks')->where('barcode_preparation', $data->barcode_fabric)->first();
                if ($get_fir_point == null) {
                    return null;
                } else {
                    return $get_fir_point->total_yds;
                }
            })
            ->editColumn('bau', function ($data) {

                return '<input type="text" class="bau form-control" id="bau"  placeholder="Bau" value="' . $data->bau . '"></input>';
            })
            ->editColumn('short_roll', function ($data) {

                return '<input type="text" class="short_roll form-control" id="short_roll"  placeholder="Short Roll" value="' . $data->short_roll . '"></input>';
            })
            ->editColumn('l_jeblos', function ($data) {
                if ($data->l_jeblos == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="ljeblos" name="ljeblos" id="checkbox" ' . $check . '>';
            })
            ->editColumn('w_jeblos', function ($data) {
                if ($data->w_jeblos == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="wjeblos" name="wjeblos" id="checkbox" ' . $check . '>';
            })
            ->editColumn('rapi', function ($data) {
                if ($data->rapi == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="rapi" name="rapi" id="checkbox" ' . $check . '>';
            })
            ->editColumn('kendur', function ($data) {
                if ($data->kendur == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="kendur" name="kendur" id="checkbox" ' . $check . '>';
            })
            ->editColumn('kencang', function ($data) {
                if ($data->kencang == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="kencang" name="kencang" id="checkbox" ' . $check . '>';
            })
            ->editColumn('sambungan_qc', function ($data) {
                if ($data->sambungan_qc == 1) {
                    $check = "checked";
                } else {
                    $check = "";
                }
                return '<input type="checkbox" class="sambungan_qc" name="sambungan_qc"  id="checkbox" ' . $check . '>';
            })
            ->rawColumns(['id', 'l_actual', 'bau', 'l_jeblos', 'w_jeblos', 'rapi', 'kendur', 'kencang', 'sambungan_qc','short_roll'])
            ->make(true);
    }

    public function ajaxApproveQc(Request $request)
    {
        $data        = $request->data;
        $a_length    = $request->a_length;
        $a_width     = $request->a_width;
        $s_high      = $request->s_high;
        $remark_note = $request->remark_note;
        $barcode     = $request->barcode;
        $status      = $request->status;

        try {
            db::begintransaction();

            $cek = MarkerSpreading::where('id_marker', $barcode)->whereNull('deleted_at')->first();
            if (is_null($cek)) {
                MarkerSpreading::FirstOrCreate(['id_marker' => $barcode, 'actual_length_qc' => $a_length, 'actual_width_qc' => $a_width, 'spreading_high_qc' => $s_high, 'remark_qc' => $remark_note, 'created_at' => Carbon::now()]);
                SpreadingApproval::FirstOrCreate(['status' => $status, 'description' => $remark_note, 'user_id' => Auth::user()->id, 'id_marker' => $barcode, 'created_at' => Carbon::now(), 'apv_type' => "QC Spreading"]);
                DB::table('cutting_marker')->where('barcode_id', $barcode)->update(['qc_status' => $status]);

                foreach ($data as $dt) {

                    $cekfab = FabricUsed::where('id',$dt[0])->whereNull('deleted_at')->first();

                    $ceksqc = SpreadingQc::where('spreading_report_temp_id', $cekfab->spreading_report_temp_id)->first();

                    if(is_null($ceksqc)){
                        $dtinspqc = array(
                            'bau'                      => $dt[7],
                            'l_jeblos'                 => $dt[12],
                            'w_jeblos'                 => $dt[13],
                            'rapi'                     => $dt[14],
                            'kencang'                  => $dt[15],
                            'kendur'                   => $dt[16],
                            'sambungan'                => $dt[17],
                            'short_roll'               => $dt[6],
                            'fabric_used_id'           => $dt[0],
                            'spreading_report_temp_id' => $cekfab->spreading_report_temp_id,
                            'created_at'               => Carbon::now()
                        );

                        SpreadingQc::FirstOrCreate($dtinspqc);
                    }
                    else{
                        $dtinspqc = array(
                            'bau'            => $dt[7],
                            'l_jeblos'       => $dt[12],
                            'w_jeblos'       => $dt[13],
                            'rapi'           => $dt[14],
                            'kencang'        => $dt[15],
                            'kendur'         => $dt[16],
                            'sambungan'      => $dt[17],
                            'fabric_used_id' => $dt[0],
                            'short_roll'     => $dt[6],
                            'updated_at'     => Carbon::now()
                        );

                        SpreadingQc::where('spreading_report_temp_id', $ceksqc->spreading_report_temp_id)->update($dtinspqc);
                    }
                }
            } else {
                MarkerSpreading::where('id_marker', $barcode)->update(['actual_length_qc' => $a_length, 'actual_width_qc' => $a_width, 'spreading_high_qc' => $s_high, 'remark_qc' => $remark_note, 'updated_at' => Carbon::now()]);
                SpreadingApproval::FirstOrCreate(['status' => $status, 'description' => $remark_note, 'user_id' => Auth::user()->id, 'id_marker' => $barcode, 'created_at' => Carbon::now()]);
                DB::table('cutting_marker')->where('barcode_id', $barcode)->update(['qc_status' => $status]);

                foreach ($data as $dt) {
                    $ceksqc = SpreadingQc::where('fabric_used_id', $dt[0])->first();
                    $dtinspqc = array(

                        'bau'        => $dt[7],
                        'l_jeblos'   => $dt[12],
                        'w_jeblos'   => $dt[13],
                        'rapi'       => $dt[14],
                        'kencang'    => $dt[15],
                        'kendur'     => $dt[16],
                        'sambungan'  => $dt[17],
                        'short_roll' => $dt[6],
                        'updated_at' => Carbon::now()
                    );

                    SpreadingQc::where('fabric_used_id', $ceksqc->fabric_used_id)->update($dtinspqc);
                }
            }


            $data_response = [
                'status' => 200,
                'output' => $status . ' marker success'
            ];
            db::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                'status' => 422,
                'output' => $status . ' marker field'
            ];
        }
        return response()->json(['data' => $data_response]);
    }

    public function ajaxSaveQcTemp(Request $request)
    {
        $data        = $request->data;
        $barcode     = $request->barcode;

        try {
            db::begintransaction();

            $cek = MarkerSpreading::where('id_marker', $barcode)->whereNull('deleted_at')->first();

            if (is_null($cek)) {

                foreach ($data as $dt) {
                    $ceksqc = SpreadingQc::where('spreading_report_temp_id',$dt[0])
                                ->whereNull('deleted_at')
                                ->first();

                    if(is_null($ceksqc)){


                        $dtinspqc = array(
                            'bau'                      => $dt[7],
                            'l_jeblos'                 => $dt[12],
                            'w_jeblos'                 => $dt[13],
                            'rapi'                     => $dt[14],
                            'kencang'                  => $dt[15],
                            'kendur'                   => $dt[16],
                            'sambungan'                => $dt[17],
                            'short_roll'               => $dt[6],
                            'spreading_report_temp_id' => $dt[0],
                            'created_at'               => Carbon::now()
                        );

                        SpreadingQc::FirstOrCreate($dtinspqc);
                    }
                    else{

                        $dtinspqc = array(
                            'bau'        => $dt[7],
                            'l_jeblos'   => $dt[12],
                            'w_jeblos'   => $dt[13],
                            'rapi'       => $dt[14],
                            'kencang'    => $dt[15],
                            'kendur'     => $dt[16],
                            'sambungan'  => $dt[17],
                            'short_roll' => $dt[6],
                            'updated_at' => Carbon::now()
                        );

                        SpreadingQc::where('spreading_report_temp_id', $ceksqc->spreading_report_temp_id)->update($dtinspqc);
                    }
                }


                $data_response = [
                    'status' => 200,
                    'output' => 'save marker success'
                ];
            }
            else{

                $data_response = [
                    'status' => 422,
                    'output' => 'marker sudah diapprove'
                ];
            }


            db::commit();
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                'status' => 422,
                'output' => $status . ' marker field'
            ];
        }
        return response()->json(['data' => $data_response]);
    }

    public function updateDowntime(Request $request)
    {
        if (request()->ajax()) {
            $downtime = $request->downtime;
            $keterangan = $request->keterangan;
            $barcode_marker = $request->barcode_marker;
            $downtime_category = $request->downtime_category;

            if ($barcode_marker != null || ($downtime != null || $keterangan != null || $downtime_category != null)) {
                $scan_cutting = CuttingMarker::where('barcode_id', $barcode_marker)->where('deleted_at', null)->update(['downtime' => $downtime, 'keterangan' => $keterangan, 'downtime_category_id' => $downtime_category]);
                return response()->json(['status' => 200, 'message' => 'Data berhasil disimpan!']);
            } else {
                return response()->json(['status' => 200, 'message' => 'Data berhasil disimpan!']);
            }
        }
    }

    private function getIPClient()
    {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
