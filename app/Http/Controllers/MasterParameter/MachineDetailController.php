<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;
use stdClass;

class MachineDetailController extends Controller
{
    public function index(Request $request)
    {
        $machine_type = DB::table('master_machine_type')
                            ->whereNull('deleted_at')
                            ->get();
        return view('master_parameter.machine.detail', compact('machine_type'));
    }

    public function ajaxGetDataMachinedetails(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_machine_details')
                    ->wherenull('deleted_at')
                    ->orderBy('id', 'desc')
                    ->get();

            return DataTables::of($data)
                ->addColumn('machine_type', function($data){
                    $machine_type = DB::table('master_machine_type')->where('id',$data->type_id)->first();
                    if($machine_type->machine_type == null){
                        return '-';
                    }else{
                        return strtoupper($machine_type->machine_type);
                    }
                })
                ->addColumn('action', function($data){ return view('_action', [
                        'id' => $data->id,
                        'edit_modal' => route('machine_details.editMachineDetails', $data->id),
                        'deletes' => route('machine_details.deleteMachineDetails', $data->id)
                    ]);
                })
                ->rawColumns(['action','machine_type'])
                ->make(true);
        }
    }

    public function addMachineDetails(Request $request)
    {

        $this->validate($request, [
            'machine_name' => 'required',
            'machine_code' => 'required',
            'machine_alias' => 'required',
        ]);
        $id_max = DB::table('master_machine_details')->select(DB::raw('max(id) as id'))->first();
        if($id_max  == null ){
            $id_max = 1;
        }else{
            $id_max = $id_max->id + 1;
        }

        $data = array([
            'id' => $id_max,
            'type_id' => (int)$request->machine_type,
            'machine_name' => trim($request->machine_name),
            'code' => trim($request->machine_code),
            'machine_alias' => trim($request->machine_alias),
            'created_at' => Carbon::now(),
            'updated_at' => null,
            'created_by' => \Auth::user()->id
        ]);

        // cek process exists
        $cekifexist = DB::table('master_machine_details')
                        ->where('machine_name', $request->machine_name)
                        ->where('type_id',(int)$request->machine_type)
                        ->where('code', $request->machine_code)
                        ->where('machine_alias', $request->machine_alias)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Machine name already exists..!', 422);
        }

        try {
            DB::begintransaction();
            DB::table('master_machine_details')->insert($data);
            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master process
    public function editMachineDetails($id)
    {
        $data_machine = DB::table('master_machine_details')
                        ->where('id', $id)
                        ->whereNull('deleted_at')
                        ->first();

        $type = DB::table('master_machine_type')->where('id',$data_machine->type_id)->first();

        $data = new StdClass();
        $data->id = $data_machine->id;
        $data->machine_name = $data_machine->machine_name;
        $data->type_id = $data_machine->type_id;
        $data->code = $data_machine->code;
        $data->machine_type = $type->machine_type;
        $data->machine_alias = $data_machine->machine_alias;

        return response()->json($data, 200);
    }

    //update master process
    public function updateMachineDetails(Request $request)
    {
        if(request()->ajax()){
            $data = $request->all();
            $machine_name = $data['machine_name_update'];
            $machine_type =$data['machine_type_update'];
            $machine_code =$data['machine_code_update'];
            $machine_alias =$data['machine_alias_update'];
            try {
                DB::beginTransaction();
                    DB::table('master_machine_details')
                    ->where('id',$data['id_update'])
                    ->update([
                        'machine_name' => $machine_name,
                        'code' => $machine_code,
                        'machine_alias' => $machine_alias,
                        'type_id' => $machine_type,
                        'updated_at' => Carbon::now(),
                        'updated_by' => \Auth::user()->id
                    ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }    
        }
    }

    public function deleteMachineDetails(Request $request)
    {
        if(request()->ajax()) 
        {
            $id = $request->id;
            DB::table('master_machine_details')
            ->where('id', $id)
            ->update([
                'deleted_at' => Carbon::now(),
                'deleted_by' => \Auth::user()->id
            ]);

            return response()->json(200);
        }
        $machine_type = DB::table('master_machine_type')
                            ->whereNull('deleted_at')
                            ->get();
        return view('master_parameter.machine.detail', compact('machine_type'));
    }
}
