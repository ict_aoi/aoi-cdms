<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class EximDocumentheader extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'document_bc_exim_header';
	protected $fillable = ['id','season','style','set_type','factory_id','kk_no','uom','total_qty','quota_used','balanced_quota','created_by','created_at','deleted_by','deleted_at','updated_by','updated_at','document_type','subcont_factory','type_movement'];
}
