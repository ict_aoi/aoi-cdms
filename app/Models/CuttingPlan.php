<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class CuttingPlan extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'cutting_plans2';
	protected $fillable = ['cutting_date','style','articleno','queu','remark','user_id','deleted_at','size_category','created_at','updated_at','mo_created_at','color','material','color_code','top_bot','season','factory_id','is_combine','is_header','header_id'];

	public function file_excels()
    {
        return $this->hasMany('App\Models\RequestMarker\FileExcel', 'cutting_plan_id', 'id');
	}
	
	public function plan_details()
    {
        return $this->hasMany('App\Models\DetailPlan', 'id_plan', 'id');
	}
	
	public function cutting_markers()
    {
        return $this->hasMany('App\Models\CuttingMarker', 'id_plan', 'id');
    }
    
    public function marker_releases()
    {
        return $this->hasMany('App\Models\MarkerReleaseHeader', 'plan_id', 'id');
	}

	public function file_markers()
    {
        return $this->hasMany('App\Models\RequestRasio\FileMarker', 'cutting_plan_id', 'id');
    }
	
	public function po_details()
    {
        return $this->hasMany('App\Models\DetailPlan', 'id_plan', 'id')->where('deleted_at', null);
    }

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory', 'factory_id', 'id');
    }
}
