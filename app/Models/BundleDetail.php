<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BundleDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['barcode_id'];
	protected $table = 'bundle_detail';
    protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['barcode_id', 'bundle_header_id', 'style_detail_id', 'komponen_name', 'start_no', 'end_no', 'qty', 'location', 'factory_id', 'sds_barcode', 'job', 'is_recycle', 'is_out_cutting', 'out_cutting_at', 'is_out_setting', 'out_setting_at', 'job_reroute', 'current_locator_id', 'current_status_to', 'current_description', 'updated_movement_at', 'current_user_id', 'parent_barcode', 'qty_total', 'qty_reject', 'is_reject', 'qc_bundle_id', 'replace_by', 'replace_date'];
}
