<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DetailKontrak extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_kk';
	protected $fillable = ['kk_id','style','po_number','qty_total','created_at','updated_at','deleted_at'];
}
