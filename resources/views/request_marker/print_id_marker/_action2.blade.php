<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if ($completed == 0)
                @if (count($model) > 0)
                    @if (isset($approve))
                        @if ($approved_data < 1)
                            <li class="bg-success"><a href="#" onclick="approve('{!! $approve !!}', '{{ $id }}', '{{ $part_no }}')"><i class="icon-checkmark3"></i> Approve</a></li>
                        @endif
                    @endif
                    @if (isset($reject))
                        <li class="bg-danger"><a href="#" onclick="reject('{!! $reject !!}')"><i class="icon-cross2"></i> Reject</a></li>
                    @endif
                @endif
            @else
                <li><a href="#"> Data Belum Complete</a></li>
            @endif
        </ul>
    </li>
</ul>