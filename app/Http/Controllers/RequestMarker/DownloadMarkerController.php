<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\RequestRasio\FileMarker;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Http\Controllers\Controller;

class DownloadMarkerController extends Controller
{
    public function index()
    {
        return view('request_marker.download_marker.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('factory_id', \Auth::user()->factory_id)
                        ->where('is_header', true)
                        ->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('is_header', true)
                        ->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Inter';
                })
                ->editColumn('articleno', function($data) {
                    if ($data->remark == 'partial') return $data->articleno.' ('.$data->queu.')';
                    else return $data->articleno;
                })
                ->addColumn('po_buyer', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                    }
                })
                ->addColumn('status', function($data) {
                    if (count($data->file_markers) > 0) return '<span class="badge bg-primary">'.count($data->file_markers).' File</span>';
                    else return '<span class="badge bg-danger">Not Yet</span>';
                })
                ->addColumn('last_update', function($data) {
                    if (count($data->file_markers) > 0) return $data->file_markers->first()->updated_at;
                    else return '-';
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.download_marker._action', [
                        'model'      => $data,
                        'download'     => route('downloadMarker.uploadModal',$data->id),
                        'detail'     => route('downloadMarker.getDetailRatio',$data->id)
                    ]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
            }
        }
    }

    public function dataMarker(Request $request)
    {
        if(request()->ajax()) 
        {
            $file_id = $request->file_id;

            if($file_id != NULL) {
                $data = FileMarker::orderBy('created_at', 'desc')->where('cutting_plan_id', $file_id);

                return datatables()->of($data)
                ->editColumn('user_id', function($data) {
                    return $data->user->name;
                })
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->created_at)->format('d/M/Y H:i');
                })
                ->editColumn('terima_user_id', function($data) {
                    if($data->terima_user_id == null) {
                        return '-';
                    } else {
                        return $data->terima_user->name;
                    }
                })
                ->editColumn('terima_at', function($data) {
                    if($data->terima_at == null) {
                        return '-';
                    } else {
                        return Carbon::parse($data->terima_at)->format('d/M/Y H:i');
                    }
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.download_marker._action', [
                        'model' => $data,
                        'terima' => route('downloadMarker.terimaFile',$data->id),
                        'unduh' => route('downloadMarker.download',$data->id),
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function uploadModal($id)
    {
        $upload = CuttingPlan::where('id', $id)
            ->where('deleted_at', null)
            ->first();

        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $upload->cutting_date;
        $obj->style = $upload->style;
        $obj->articleno = $upload->articleno;
        $obj->size_category = $upload->size_category;
		$obj->url_upload = '#';
		
		return response()->json($obj,200);
    }

    public function download($id)
    {
        $data = FileMarker::findOrFail($id);
        return response()->json($data->id, 200);
    }

    public function downloadFile($id)
    {
        $data = FileMarker::findOrFail($id);
        $path = storage_path('app/'.$data->file_path);
        return response()->download($path);
    }

    public function getDetailRatio($id)
    {
        $data = CuttingPlan::where('id', $id)->where('deleted_at', null)->get()->first();
        
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style = $data->style;
        $obj->articleno = $data->articleno;
        $obj->size_category = $data->size_category;
		$obj->url_upload = '#';
		
		return response()->json($obj,200);
    }

    public function getDetailRatioData(Request $request) {
        if(request()->ajax()) 
        {
            $id_plan = $request->plan_id;
            
            if($id_plan != NULL) {
    
                $data = DB::table('cutting_marker')
                    ->where('id_plan',$id_plan)
                    ->whereNull('deleted_at')
                    ->orderBy('part_no')
                    ->orderBy('cut');
        
                return DataTables::of($data)
                ->editColumn('ratio',function($data){
                    $grt = DB::table('ns_trig_ratio')
                        ->select('ratio')
                        ->where('id_marker',$data->barcode_id)
                        ->first();

                    return $grt->ratio;                     
                })
                ->editColumn('fabric_width',function($data){
                    return $data->fabric_width + 0.75;
                })
                ->setRowAttr([
                    'style'=>function($data){
                        if ($data->remark==true) {
                            return  'background-color: #00e6ac;';
                        }
                    }
                ])
                ->rawColumns(['fabric_width','ratio'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function terimaFile(Request $request, $id)
    {
        if(request()->ajax())
        {
            $data = FileMarker::where('id', $id)->update([
                'terima_user_id' => \Auth::user()->id,
                'terima_at' => Carbon::now(),
            ]);

            return response()->json(200);
        }
    }
}
