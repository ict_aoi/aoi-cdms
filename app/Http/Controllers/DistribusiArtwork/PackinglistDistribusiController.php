<?php

namespace App\Http\Controllers\DistribusiArtwork;

use DB;
use Uuid;
use Carbon\Carbon;
use App\Models\DetailPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PackinglistDistribusi;
use App\Models\DetailPackinglistDistribusi;
use App\Models\PackingSubTemp;
use App\Models\ReversedMovement;
use App\Models\DistribusiMovement;
use App\Models\DockkPpcheader;
use App\Models\ReverseDetailPackinglist;
use App\Models\EximDocumentheader;
use stdClass;

class PackinglistDistribusiController extends Controller
{
    public function index()
    {
        $po_buyers = DB::table('detail_cutting_plan')
                        ->join('cutting_plans2', 'cutting_plans2.id', '=', 'detail_cutting_plan.id_plan')
                        ->where('cutting_plans2.factory_id', \Auth::user()->factory_id)
                        ->whereNull('cutting_plans2.deleted_at')
                        ->where(function ($query) {
                            $query->whereNull('detail_cutting_plan.statistical_date')
                            ->orWhere('detail_cutting_plan.statistical_date', '>', Carbon::now()->subDays(60)->format('Y-m-d H:i:s'));
                        })
                        ->get();

                        DB::table('temp_edit_packinglist_inhouse')->where('edited_by',\Auth::user()->id)->delete();
        $po_buyer_array = $po_buyers->pluck('po_buyer')->toArray();
        $style = DB::table('data_cuttings')->select('style')->whereIn('po_buyer',$po_buyer_array)->distinct()->get();
        $subconts = DB::table('master_subcont')->whereNull('deleted_at')->get();
        // $kk_no = DB::table('document_bc_exim')->where('balanced_quota','>',0)->whereNull('deleted_at')->where('kk_no','<>','-')->where('factory_id',\Auth::user()->factory_id)->distinct()->get();
        $kk_no = DB::table('doc_kk_ppc_header')->select('kk_no')->distinct('kk_no')->get();
        $locators = DB::table('master_locator')->whereIn('id',array(8,3))->whereNull('deleted_at')
        ->get();
        return view('distribusi.distribusi_subcont.packinglist.index', compact('subconts', 'po_buyers', 'locators','kk_no','style'));
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $data = DB::table('packinglist_distribusi as pd')
            ->select('pd.no_packinglist', 'ml.locator_name','ms.division','pd.subcont','pd.remark','pd.status')
            ->leftJoin('master_locator as ml', 'pd.process', '=', 'ml.id')
            ->leftJoin('master_subcont as ms','ms.id','=','pd.subcont_id')
            ->where('pd.factory_id', \Auth::user()->factory_id)
            ->where('packinglist_type','subcont')
            ->whereNull('pd.deleted_at')
            ->orderby('pd.created_at','desc');

            return datatables()->of($data)
            ->addColumn('status',function ($data){
                return strtoupper($data->status);
            })
            ->addColumn('action', function($data) {
                return view('distribusi.distribusi_subcont.packinglist._action', [
                    'model' => $data,
                    'edit' => route('packinglistDistribusi.edit', $data->no_packinglist),
                    'delete' => route('packinglistDistribusi.delete', $data->no_packinglist),
                    'print' => route('packinglistDistribusi.print', $data->no_packinglist),
                    'release' => route('packinglistDistribusi.release', $data->no_packinglist),

                ]);
                return 'action';
            })
            ->rawColumns(['action','status'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
        if(request()->ajax()) 
        {
            $kk_id = $request->no_kk_insert;
            $kk_no = DB::table('document_bc_exim')->select('kk_no')->where('id',$kk_id)->first();
            $subcont = $request->subcont_insert;
            $no_packinglist = $request->no_packinglist_insert;
            $po_buyer = $request->po_buyer_insert;
            $locator = $request->locator_insert;
            $remark = $request->remark_insert;
            $style = $request->style;

            $subcont_detail = DB::table('master_subcont')->where('id', $subcont)->first();

            $insert_packinglist = DB::table('packinglist_distribusi')->insert([
                'no_packinglist' => $no_packinglist,
                'no_kk' => $kk_no->kk_no,
                'kk_id' => $kk_id,
                'po_buyer' => implode(',', $po_buyer),
                'subcont' => $subcont_detail->initials,
                'subcont_id' => $subcont,
                'created_by' => \Auth::user()->id,
                'factory_id' => \Auth::user()->factory_id,
                'status' => 'draft',
                'process' => $locator,
                'remark' => $remark,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'style' => $style,
                'packinglist_type' => 'subcont',
                'is_completed'      => false
                // 'season'        => $kk_no->season
            ]);

            return response()->json(200);
        }
    }

    public function getNoPackinglist(Request $request)
    {
        if(request()->ajax()) 
        {
            $subcont = $request->subcont;
            $session_edit = DB::table('temp_edit_packinglist_inhouse')->where('edited_by',\Auth::user()->id)->first();
            if($session_edit == null){
                $year_value = substr(Carbon::now()->format('Y'),2,2);
                $month_value = Carbon::now()->format('m');
                $get_subcont = DB::table('master_subcont')->where('id', $subcont)->first();
                $initials_div = strtoupper(substr($get_subcont->division,0,1));
                $get_last_number = DB::table('packinglist_distribusi')
                ->where('no_packinglist', 'like', '%'.'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'%')
                ->whereNull('deleted_at')
                ->orderBy('no_packinglist', 'desc')->first();
                
                if($get_last_number == null){
                    $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'0001';
                }else{
                    $no_packinglist = (int) substr(str_replace('PL'.\Auth::user()->factory_id.$initials_div,'',$get_last_number->no_packinglist) + 1,4,4);
                    $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.sprintf("%04d", $no_packinglist);
                }
            }else{
                $get_subcont_id = DB::table('packinglist_distribusi')->where('no_packinglist',$session_edit->packinglist_no)->first();
                if($get_subcont_id->subcont_id == $subcont){
                    $no_packinglist = $session_edit->packinglist_no;
                }else{
                    $year_value = substr(Carbon::now()->format('Y'),2,2);
                    $month_value = Carbon::now()->format('m');
                    $get_subcont = DB::table('master_subcont')->where('id', $subcont)->first();
                    $initials_div = strtoupper(substr($get_subcont->division,0,1));
                    $get_last_number = DB::table('packinglist_distribusi')
                    ->where('no_packinglist', 'like', '%'.'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'%')
                    ->whereNull('deleted_at')
                    ->orderBy('no_packinglist', 'desc')->first();
                    
                    if($get_last_number == null){
                        $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'0001';
                    }else{
                        $no_packinglist = (int) substr(str_replace('PL'.\Auth::user()->factory_id.$initials_div,'',$get_last_number->no_packinglist) + 1,4,4);
                        $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.sprintf("%04d", $no_packinglist);
                    }
                }
            }
            

            return response()->json($no_packinglist,200);
        }
    }

    public function getNoPackinglistupdate(Request $request)
    {
        if(request()->ajax()) 
        {
            //ID SUBCONT
            $subcont = $request->subcont;
            //PACKINGLIST NO
            $packinglist_no = $request->packinglist_update;
            
            $year_value = substr(Carbon::now()->format('Y'),2,2);
            $month_value = Carbon::now()->format('m');
            $get_subcont = DB::table('master_subcont')->where('id', $subcont)->first();
            $initials_div = strtoupper(substr($get_subcont->division,0,1));
            $get_last_number = DB::table('packinglist_distribusi')
            ->where('no_packinglist', 'like', '%'.'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'%')
            ->whereNull('deleted_at')
            ->orderBy('no_packinglist', 'desc')->first();
            
            $check_exists = DB::table('packinglist_distribusi')->where('no_packinglist',$packinglist_no)->first();
            if($check_exists != null && $check_exists->subcont == $get_subcont->initials){
                if($get_last_number == null){
                    $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'0001';
                }else{
                    $no_packinglist = $packinglist_no;
                }
            }else{
                if($get_last_number == null){
                    $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.'0001';
                }else{
                    $no_packinglist = (int) substr(str_replace('PL'.\Auth::user()->factory_id.$initials_div,'',$get_last_number->no_packinglist) + 1,4,4);
                    $no_packinglist = 'PL'.\Auth::user()->factory_id.$initials_div.$year_value.$month_value.sprintf("%04d", $no_packinglist);
                }
            }
            
            return response()->json($no_packinglist,200);
        }
    }

    public function delete(Request $request, $id)
    {
        if(request()->ajax()) 
        {
            $no_packinglist = DB::table('packinglist_distribusi')->where('id',$id)->first();
            $check_fillable_detail = DB::table('detail_packinglist_distribusi')->where('id_packinglist',$no_packinglist->no_packinglist)->whereNull('deleted_at')->first();
            if($check_fillable_detail != null){
                return response()->json('No Packinglist '.$no_packinglist->no_packinglist.' Tidak Dapat Dihapus!', 422);
            }else{
                DB::table('packinglist_distribusi')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now(),
                    'deleted_by' => \Auth::user()->id
                ]);
                return response()->json('No Packinglist '.$no_packinglist->no_packinglist.' Telah Dihapus!',200);
            }
        }
    }

    public function print($id)
    {
        $packinglist = DB::table('packinglist_distribusi')->where('id', $id)->first();
        $filename ='packinglist_'.$packinglist->no_packinglist;
        $factory = DB::table('master_factory')->where('id',$packinglist->factory_id)->first();
        $subcont = DB::table('master_subcont')->where('id', $packinglist->subcont_id)->first();

        $size = DB::table('detail_packinglist_distribusi')
        ->select('size')
        ->where('id_packinglist', $packinglist->no_packinglist)
        ->groupBy('size')
        ->orderByRaw('LENGTH(size) asc')
        ->orderBy('size', 'asc')
        ->pluck('size')
        ->toArray();

        $data_details = DB::table('detail_packinglist_distribusi')
        ->select(DB::raw('id_packinglist, style, max(created_at) as created_at, poreference, article, komponen_name,color_name,no_polybag'))
        ->where('id_packinglist', $packinglist->no_packinglist)
        ->groupBy('style', 'poreference', 'article', 'komponen_name', 'id_packinglist', 'no_polybag','color_name')
        ->orderBy('no_polybag', 'poreference')
        ->get();

        $data_qty_array = array();
        $total_qty_all = array();
        
        foreach($data_details as $detail) {
            $data_size_details = array();
            $size_detail = DB::table('detail_packinglist_distribusi')
            ->select(DB::raw('no_polybag, size, sum(qty) as qty'))
            ->where('style', $detail->style)
            ->where('article', $detail->article)
            ->where('poreference', $detail->poreference)
            ->where('komponen_name', $detail->komponen_name)
            ->where('id_packinglist', $packinglist->no_packinglist)
            ->groupBy('no_polybag', 'size')
            ->get();

            $data_size_details['data'] = $size_detail;
            $data_size_details['count'] = $size_detail->count();

            $total_temp = 0;

            foreach($size_detail as $aa) {
                $total_temp = $total_temp + $aa->qty;
            }

            $total_qty_all[] = $total_temp;

            $data_qty_array[] = $data_size_details;
        }
        
        $data_size = array();

        if(count($size) > 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } elseif(count($size) == 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } else {
            for($i = 0; $i < count($size); $i++) {
                $data_size[] = $size[$i];
            }
            $selisih_size = 6 - count($size);
            for($i = 0; $i < $selisih_size; $i++) {
                $data_size[] = 0;
            }
        }

        $paper = array(0,0,750.449,450.85);

        //UPDATED PRINT BY AND PRINT AT
        DB::table('packinglist_distribusi')->where('id',$id)->update([
            'print_at'      => Carbon::now(),
            'printed_by'    => \Auth::user()->id
        ]);

        PackinglistDistribusi::where('no_packinglist',$packinglist->no_packinglist)
        ->update([
                'is_completed' => true,
                'updated_at'    => Carbon::now(),
                'updated_by'    => \Auth::user()->id
            ]);

        $data = [
            'no_packinglist' => $packinglist->no_packinglist,
            'kk' => $packinglist->no_kk,
            'kepada' => $subcont->subcont_name,
            'proses'=>$subcont->division,
            'factory' => $factory->factory_alias,
            'sizes' => $data_size,
            'remark'    => $packinglist->remark,
            'data_details' => $data_details,
            'style_set'      => $packinglist->set_type == 'Non' ? $packinglist->style : $packinglist->style.'-'.substr($packinglist->set_type,0,3),
            'data_size_details' => $data_qty_array,
            'total_qty_all' => $total_qty_all,
            'created_at' => Carbon::parse($packinglist->created_at)->format('d M Y'),
        ];
        
        $pdf = \PDF::loadView('print.packinglist', $data)->setPaper($paper,'potrait');

        return $pdf->stream($filename);
    }

    public function edit($id)
    {
        $packinglist_details = DB::table('packinglist_distribusi')
                        ->where('no_packinglist', $id)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->first();

        $check_edit = DB::table('temp_edit_packinglist_inhouse')->where('packinglist_no',$id)->first();

        if($check_edit != null){
            $edited_by = DB::table('users')->where('id',$check_edit->edited_by)->first();
            return response()->json('Packinglist Masih Dalam Proses Edit Oleh! '.$edited_by->name,422);
        }else{
            $edit_access = DB::table('temp_edit_packinglist_inhouse')->insert([
                'id'                    => Uuid::generate()->string,
                'packinglist_no'        => $id,
                'edited_by'             => \Auth::user()->id,
                'edited_at'             => Carbon::now(),
                'factory_id'            => \Auth::user()->factory_id
            ]);
        }
        $locator_name = DB::table('master_locator')->where('id',$packinglist_details->process)->first();
        $data = new StdClass();
        $data->id = $packinglist_details->id;
        $data->subcont_id = $packinglist_details->subcont_id;
        $data->subcont_name = $packinglist_details->subcont;
        $data->no_packinglist = $packinglist_details->no_packinglist;
        $data->po_buyer = $packinglist_details->po_buyer;
        $data->style = $packinglist_details->set_type == 'Non' ? $packinglist_details->style : $packinglist_details->style.'-'.$packinglist_details->set_type;
        $data->document_no = $packinglist_details->document_no;
        $data->locator_id = $locator_name->id;  
        $data->locator_name = $locator_name->locator_name;  
        $data->remark = $packinglist_details->remark;
        return response()->json($data, 200);
    }

    //UPDATE PACKINGLIST
    public function update(Request $request)
    {
        if(request()->ajax()){
            $data = $request->all();
            $id = $data['id_update'];
            $doc_no = $data['no_kk_insert_update'];
            if($doc_no == '-'){
                return response()->json('Pilih Packinglist Dahulu',422);
            }
            $subcont = $data['subcont_insert_update'];
            $subcont_name = DB::table('master_subcont')->where('id',$subcont)->first();
            $no_packinglist = $data['no_packinglist_insert_update'];
            $locator = $data['locator_insert_update'];
            $po_buyer = $data['po_buyer_insert_update'];
            $style_temp = $data['style_update'];
            $remark = $data['remark_insert_update'];
            if(strpos($style_temp,'-TOP') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'TOP';
            }elseif(strpos($style_temp,'-BOT') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'BOTTOM';
            }else{
                $style = $style_temp;
                $set_type = 'Non';
            }
            $subcont_detail = DB::table('master_subcont')->where('id', $subcont)->first();
            $kk_type = $subcont_detail->division == 'PRINTING' ? 'print' : 'embro';
            $kk_no = DB::table('doc_kk_ppc_header')->where('document_no',$doc_no)->first();

            $kk_id = DB::table('doc_kk_ppc_header')
            ->where('kk_no',$kk_no->kk_no)
            ->where('style',$style)
            ->where('document_no',$doc_no)
            ->where('season',$kk_no->season)
            ->where('set_type',$set_type)
            ->where('factory_id',\Auth::user()->factory_id)
            ->where('kk_type',$kk_type)->first();
            
            try {
                DB::beginTransaction();
                    DB::table('packinglist_distribusi')
                    ->where('id',$id)
                    ->update([
                        'no_packinglist'    => $no_packinglist,
                        'no_kk' => strtoupper($kk_no->kk_no),
                        'kk_id' => $kk_id->id,
                        'subcont' => strtoupper($subcont_name->initials),
                        'subcont_id'    => $subcont,
                        'style' => $style,
                        'set_type' => $set_type,
                        'process' => $locator,
                        'po_buyer' => $data['po_buyer_insert_update'] != 'null' ? implode(',',$data['po_buyer_insert_update']) : null,
                        'updated_by' => \Auth::user()->id,
                        'factory_id' => \Auth::user()->factory_id,
                        'updated_at' => Carbon::now(),
                        'remark' => $remark,
                    ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            } 
        }
    }

    public function release($id){
        if(request()->ajax()) 
        {
            DB::table('packinglist_distribusi')
            ->where('id', $id)
            ->update([
                'status' => 'release',
                'released_by' => \Auth::user()->id,
                'released_at' => Carbon::now()
            ]);

            return response()->json(200);
        }
    }

    public function inhouse_index(){
        $po_buyers = DB::table('detail_cutting_plan')
        ->join('cutting_plans2', 'cutting_plans2.id', '=', 'detail_cutting_plan.id_plan')
        ->where('cutting_plans2.factory_id', \Auth::user()->factory_id)
        ->whereNull('detail_cutting_plan.deleted_at')
        ->where(function ($query) {
            $query->whereNull('detail_cutting_plan.statistical_date')
            ->orWhere('detail_cutting_plan.statistical_date', '>', Carbon::now()->subDays(60)->format('Y-m-d H:i:s'));
        })
        ->get();

        DB::table('temp_edit_packinglist_inhouse')->where('edited_by',\Auth::user()->id)->delete();

        $po_buyer_array = $po_buyers->pluck('po_buyer')->toArray();
        $style = DB::table('doc_kk_ppc_header')->select('style','set_type')->where('balanced_quota','>',0)->whereNull('deleted_at')->distinct('style','set_type')->get();
        $subconts = DB::table('master_subcont')->whereNull('deleted_at')->get();
        $kk_no = DB::table('doc_kk_ppc_header')->select('kk_no','document_no')->whereNull('deleted_at')->where('factory_id',\Auth::user()->factory_id)->distinct('kk_no','document_no')->get();
        $locators = DB::table('master_locator')->whereIn('id',array(8,3))->whereNull('deleted_at')->orderBy('locator_name','ASC')->get();
        return view('distribusi.distribusi_subcont.packinglist_inhouse.inhouse_index', compact('subconts', 'po_buyers', 'locators','kk_no','style'));
    }
    public function storeInhouse(Request $request)
    {
        if(request()->ajax()) 
        {
            $doc_no = $request->no_kk_insert;
            $subcont = $request->subcont_insert;
            $no_packinglist = $request->no_packinglist_insert;
            $po_buyer = $request->po_buyer_insert;
            $locator = $request->locator_insert;
            $remark = $request->remark_insert;
            $style_temp = $request->style;
            if(strpos($style_temp,'-TOP') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'TOP';
            }elseif(strpos($style_temp,'-BOT') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'BOTTOM';
            }else{
                $style = $style_temp;
                $set_type = 'Non';
            }
            $subcont_detail = DB::table('master_subcont')->where('id', $subcont)->first();
            
            $kk_type = $subcont_detail->division == 'PRINTING' ? 'print' : 'embro';
            
            $kk_no = DB::table('doc_kk_ppc_header')
            ->where('document_no',$doc_no)
            ->where('style',$style)
            ->where('set_type',$set_type)
            ->where('kk_type',$kk_type)
            ->first();

            if($kk_no == null){
                return response()->json('No KK Bukan Untuk Divisi '.strtoupper($kk_type),422);
            }
            
            $kk_id = DB::table('doc_kk_ppc_header')
            ->where('kk_no',$kk_no->kk_no)
            ->where('document_no',$doc_no)
            ->where('style',$style)
            ->where('season',$kk_no->season)
            ->where('set_type',$set_type)
            ->where('factory_id',\Auth::user()->factory_id)
            ->where('kk_type',$kk_type)
            ->first();            

            $cek_double_packinglist = DB::table('packinglist_distribusi')->where('no_packinglist',$no_packinglist)->whereNull('deleted_at')->exists();
            if($cek_double_packinglist){
                return response()->json('Packinglist '.$no_packinglist.' Sudah Dibuat!',422);
            }
                DB::table('packinglist_distribusi')->insert([
                    'id' => Uuid::generate()->string,
                    'no_packinglist' => $no_packinglist,
                    'no_kk' => $kk_no->kk_no,
                    'kk_id' => $kk_id->id,
                    'po_buyer' => implode(',', $po_buyer),
                    'subcont' => $subcont_detail->initials,
                    'subcont_id' => $subcont,
                    'created_by' => \Auth::user()->id,
                    'factory_id' => \Auth::user()->factory_id,
                    'status' => 'draft',
                    'process' => $locator,
                    'remark' => $remark,
                    'created_at' => Carbon::now(),
                    'updated_at' => null,
                    'style' => $style,
                    'set_type'  => $set_type,
                    'packinglist_type' => 'inhouse',
                    'document_no'   => $doc_no,
                    'is_completed'  => false,
                    'season'        => $kk_no->season
                ]);
            return response()->json(200);
        }
    }
    public function deleteEditPackinglist(Request $request){
        DB::table('temp_edit_packinglist_inhouse')
        ->where('edited_by',\Auth::user()->id)->delete();
    }
    public function getDataInhouse(Request $request)
    {
        if(request()->ajax()) 
        {
            $packinglist_no = $request->packinglist_no;
            if($packinglist_no != null){
                $data = DB::table('packinglist_distribusi as pd')
                ->select('pd.no_packinglist', 'ml.locator_name','ms.division','pd.subcont','pd.remark','pd.status','pd.id','pd.released_by','pd.style','pd.set_type')
                ->leftJoin('master_locator as ml', 'pd.process', '=', 'ml.id')
                ->leftJoin('master_subcont as ms','ms.id','=','pd.subcont_id')
                ->where('pd.factory_id', \Auth::user()->factory_id)
                ->where('pd.no_packinglist',$packinglist_no)
                ->where('pd.packinglist_type','inhouse')
                ->whereNull('pd.deleted_at')
                ->orderby('pd.created_at','desc')->get();
            }else{
                $data = DB::table('packinglist_distribusi as pd')
                ->select('pd.no_packinglist', 'ml.locator_name','ms.division','pd.subcont','pd.remark','pd.status','pd.id','pd.released_by','pd.style','pd.set_type')
                ->leftJoin('master_locator as ml', 'pd.process', '=', 'ml.id')
                ->leftJoin('master_subcont as ms','ms.id','=','pd.subcont_id')
                ->where('pd.factory_id', \Auth::user()->factory_id)
                ->where('pd.packinglist_type','inhouse')
                ->whereNull('pd.deleted_at')
                ->orderby('pd.created_at','desc')->limit(10)->get();
            }
            

            return datatables()->of($data)
            ->addColumn('status',function ($data){
                if($data->status == 'draft'){
                    return '<td><span class="label bg-danger">'.$data->status.'</span></td>';
                }else{
                    return '<td><span class="label bg-success">'.$data->status.'</span></td>';
                }
            })
            ->addColumn('remark',function ($data){
                return strtoupper($data->remark);
            })
            ->addColumn('style_set',function ($data){
                if($data->set_type == 'Non'){
                    $style_set = $data->style;
                }else{
                    $style_set = $data->style.'-'.$data->set_type;
                }
                return strtoupper($style_set);
            })
            ->addColumn('total_panel',function ($data){
                $c_panel = DB::table('detail_packinglist_distribusi')->where('id_packinglist',$data->no_packinglist)->get()->count('bundle_id');
                return $c_panel.' Panel';
            })
            ->addColumn('released_by',function ($data){
                $released_user = DB::table('users')->where('id',$data->released_by)->first();
                $user = $released_user == null ? '-' : explode(' ',$released_user->name);
                return strtoupper($user[0]);
            })
            ->addColumn('action', function($data) {
                return view('distribusi.distribusi_subcont.packinglist_inhouse._action', [
                    'model' => $data,
                    'edit' => route('packinglistDistribusi.editInhouse', $data->id),
                    'delete' => route('packinglistDistribusi.delete', $data->id),
                    'print' => route('packinglistDistribusi.print', $data->id),
                    'release' => route('packinglistDistribusi.release', $data->id),

                ]);
                return 'action';
            })
            ->rawColumns(['action','status','remark','total_panel','released_by','style_set'])
            ->make(true);
        }
    }
    public function editInhouse($id)
    {
        $packinglist_details = DB::table('packinglist_distribusi')
                        ->where('id', $id)
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->first();
        
        $check_edit = DB::table('temp_edit_packinglist_inhouse')->where('packinglist_no',$packinglist_details->no_packinglist)->first();

        if($check_edit != null){
            $edited_by = DB::table('users')->where('id',$check_edit->edited_by)->first();
            return response()->json('Packinglist Masih Dalam Proses Edit Oleh! '.$edited_by->name,422);
        }else{
            $edit_access = DB::table('temp_edit_packinglist_inhouse')->insert([
                'id'                    => Uuid::generate()->string,
                'packinglist_no'        => $packinglist_details->no_packinglist,
                'edited_by'             => \Auth::user()->id,
                'edited_at'             => Carbon::now(),
                'factory_id'            => \Auth::user()->factory_id
            ]);
        }
        $locator_name = DB::table('master_locator')->where('id',$packinglist_details->process)->first();
        $data = new StdClass();
        $data->id = $packinglist_details->id;
        $data->subcont_id = $packinglist_details->subcont_id;
        $data->subcont_name = $packinglist_details->subcont;
        $data->no_packinglist = $packinglist_details->no_packinglist;
        $data->po_buyer = $packinglist_details->po_buyer;
        $data->style = $packinglist_details->set_type == 'Non' ? $packinglist_details->style : $packinglist_details->style.'-'.$packinglist_details->set_type;
        $data->document_no = $packinglist_details->document_no;
        $data->locator_id = $locator_name->id;  
        $data->locator_name = $locator_name->locator_name;  
        $data->remark = $packinglist_details->remark;
        return response()->json($data, 200);
    }

    //UPDATE PACKINGLIST
    public function updateInhouse(Request $request)
    {
        if(request()->ajax()){
            $data = $request->all();
            $id = $data['id_update'];
            $doc_no = $data['no_kk_insert_update'];//
            $subcont = $data['subcont_insert_update'];//
            $subcont_name = DB::table('master_subcont')->where('id',$subcont)->first();
            $no_packinglist = $data['no_packinglist_insert_update'];//
            $locator = $data['locator_insert_update'];//
            $po_buyer = $data['po_buyer_insert_update'];//
            $style_temp = $data['style_update'];
            $remark = $data['remark_insert_update'];
            if(strpos($style_temp,'-TOP') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'TOP';
            }elseif(strpos($style_temp,'-BOT') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'BOTTOM';
            }else{
                $style = $style_temp;
                $set_type = 'Non';
            }
            $subcont_detail = DB::table('master_subcont')->where('id', $subcont)->first();
            $kk_type = $subcont_detail->division == 'PRINTING' ? 'print' : 'embro';
            $kk_no = DB::table('doc_kk_ppc_header')->where('document_no',$doc_no)->first();
            $kk_id = DB::table('doc_kk_ppc_header')
            ->where('kk_no',$kk_no->kk_no)
            ->where('style',$style)
            ->where('document_no',$doc_no)
            ->where('set_type',$set_type)
            ->where('factory_id',\Auth::user()->factory_id)
            ->where('kk_type',$kk_type)
            ->first();
            
            try {
                DB::beginTransaction();
                    DB::table('packinglist_distribusi')
                    ->where('id',$id)
                    ->update([
                        'no_packinglist'    => $no_packinglist,
                        'no_kk' => strtoupper($kk_no->kk_no),
                        'kk_id' => $kk_id->id,
                        'subcont' => strtoupper($subcont_name->initials),
                        'subcont_id'    => $subcont,
                        'style' => $style,
                        'set_type' => $set_type,
                        'process' => $locator,
                        'po_buyer' => $data['po_buyer_insert_update'] != 'null' ? implode(',',$data['po_buyer_insert_update']) : null,
                        'updated_by' => \Auth::user()->id,
                        'factory_id' => \Auth::user()->factory_id,
                        'updated_at' => Carbon::now(),
                        'remark' => $remark,
                        'document_no' => $kk_id->document_no
                    ]);
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }    
        }
    }
    public function getKk(Request $request)
    {
        $temp = explode('-',$request->style);
        if(in_array('TOP',$temp)){
            $set_type = $temp[1];
            $style = $temp[0];
        }elseif(in_array('BOTTOM',$temp)){
            $set_type = $temp[1];
            $style = $temp[0];
        }else{
            $set_type = 'Non';
            $style = $request->style;
        }
        // $x = count($temp);
        // $set_type = $x == 1 ? 'Non' : $temp[1];

        $kk_no = DB::table('doc_kk_ppc_header')
                            ->select('document_no','kk_no')
                            ->where('style',$style)
                            ->where('set_type',$set_type)
                            ->distinct()
                            ->get();
        return response()->json(['response' => $kk_no],200);
    }
    public function getKkUpdate(Request $request)
    {
        $temp = explode('-',$request->style_update);

        $x = count($temp);
        $style = $temp[0];
        $set_type = $x == 1 ? 'Non' : $temp[1];

        $temp_access = DB::table('temp_edit_packinglist_inhouse')->where('edited_by',\Auth::user()->id)->first();
        $check_exists = DB::table('packinglist_distribusi')->where('no_packinglist',$temp_access->packinglist_no)->first();

        if($temp_access != null && $style  == $check_exists->style && $set_type == $check_exists->set_type){
            $kk_no = DB::table('doc_kk_ppc_header')
                            ->select('document_no','kk_no')
                            ->where('style',$style)
                            ->where('set_type',$set_type)
                            ->distinct()
                            ->get();
        }else{
            $kk_no = DB::table('doc_kk_ppc_header')
                            ->select('document_no','kk_no')
                            ->where('style',$style)
                            ->where('set_type',$set_type)
                            ->distinct()
                            ->get();
        }
        return response()->json(['response' => $kk_no],200);
    }
    public function ajaxGetDataPackinglistinhouse(Request $request)
    {
        $term = trim(strtoupper($request->q));

        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = PackinglistDistribusi::where('no_packinglist', 'like', '%'.$term.'%')->where('packinglist_type','inhouse')->whereNull('deleted_at')->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {

            $formatted_tags[] = [
                'id' => $tag->no_packinglist, 
                'text' => $tag->no_packinglist
            ];
        }

        return \Response::json($formatted_tags);
    }
    public function ajaxGetDataPackinglistsubcont(Request $request)
    {
        $term = trim(strtoupper($request->q));

        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = PackinglistDistribusi::where('no_packinglist', 'like', '%'.$term.'%')->where('packinglist_type','subcont')->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {

            $formatted_tags[] = [
                'id' => $tag->no_packinglist, 
                'text' => $tag->no_packinglist
            ];
        }

        return \Response::json($formatted_tags);
    }
}
