<?php

namespace App\Http\Controllers\ComponentMovement;

use DB;
use StdClass;
use Illuminate\Http\Request;
use App\Models\PackingSubTemp;
use App\Models\PackinglistSubcont;
use App\Models\DistribusiMovement;
use App\Http\Controllers\Controller;

class ScanComponentSubcontController extends Controller
{
    public function index()
    {
        $packinglists = DB::table('packinglist_subcont')->whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->where('status', 'draft')->get();
        return view('component_movement.scan_component_subcont.index', compact('packinglists'));
    }

    public function dataLocator()
    {
        if(request()->ajax())
        {
            $data = DB::table('master_locator')->where('deleted_at', null)->where('is_cutting', false)->where('is_setting', false)->get();

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                if($data->is_cutting) {
                    $is_cutting = 'true';
                } else {
                    $is_cutting = 'false';
                }

                if($data->is_setting) {
                    $is_setting = 'true';
                } else {
                    $is_setting = 'false';
                }

                if($data->is_artwork) {
                    $is_artwork = 'true';
                } else {
                    $is_artwork = 'false';
                }
                return '<button class="btn btn-primary btn-xs" data-id="'.$data->id.'" data-name="'.$data->locator_name.'" data-is_cutting="'.$is_cutting.'" data-is_setting="'.$is_setting.'" data-is_artwork="'.$is_artwork.'" id="select_locator">Select</button>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function dataComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $locator = $request->locator;
            $state = $request->state;
            $polibag = $request->polibag;

            if($packinglist != null && $locator != null && $state != null && $polibag != null) {
                $data = DB::table('detail_packinglist_subtemp')->select('detail_packinglist_subtemp.bundle_id', 'bundle_header.style', 'bundle_header.set_type', 'bundle_header.article', 'bundle_detail.komponen_name', 'bundle_header.poreference', 'bundle_header.size', 'bundle_header.cut_num', 'bundle_detail.start_no', 'bundle_detail.end_no', 'bundle_detail.qty')->join('bundle_detail', 'detail_packinglist_subtemp.bundle_id', '=', 'bundle_detail.barcode_id')->join('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subtemp.id_packinglist', $packinglist)->where('detail_packinglist_subtemp.no_polybag', $polibag)->where('detail_packinglist_subtemp.factory_id', \Auth::user()->factory_id);

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == null) {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->addColumn('action', function($data) {
                    return view('component_movement.scan_component_subcont._action', [
                        'model'      => $data,
                        'hapus'     => route('ScanComponentSubcont.deleteComponent', $data->bundle_id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataComponentPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;

            if($packinglist != null) {
                $data = DB::table('detail_packinglist_subtemp')->select('detail_packinglist_subtemp.bundle_id', 'bundle_header.style', 'bundle_header.set_type', 'bundle_header.article', 'bundle_detail.komponen_name', 'bundle_header.poreference', 'bundle_header.size', 'bundle_header.cut_num', 'bundle_detail.start_no', 'bundle_detail.end_no', 'bundle_detail.qty', 'detail_packinglist_subtemp.no_polybag')->join('bundle_detail', 'detail_packinglist_subtemp.bundle_id', '=', 'bundle_detail.barcode_id')->join('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subtemp.id_packinglist', $packinglist)->where('detail_packinglist_subtemp.factory_id', \Auth::user()->factory_id);

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == null) {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $data = DB::table('packinglist_subcont')->where('no_packinglist', $packinglist)->where('deleted_at', null)->first();
            $kk = DB::table('master_kk')->join('master_subcont', 'master_subcont.id', '=', 'master_kk.subcont_id')->where('master_kk.no_kk', $data->no_kk)->first();

            $obj = new StdClass();
            $obj->packinglist = $data->no_packinglist;
            $obj->kk = $data->no_kk;
            $obj->style = $data->style;
            $obj->po_buyer = $data->po_buyer;
            $obj->subcont = $kk->subcont_name;
            
            return response()->json($obj,200);
        }
    }

    public function dataPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $polibag = $request->polibag;
            $locator = $request->locator;
            $state = $request->state;

            $obj = new StdClass();
            $obj->packinglist = $packinglist;
            $obj->polibag = $polibag;
            $obj->locator = $locator;
            $obj->state = $state;
            
            return response()->json($obj,200);
        }
    }
    
    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $polibag = $request->polibag;
            $locator = $request->locator;
            $state = $request->state;
            $barcode_component = $request->barcode_component;

            if($packinglist != null && $locator != null && $state != null && $polibag != null && $barcode_component != null) {

                $master_locator = DB::table('master_locator')->where('id', $locator)->first();

                $component_check = DistribusiMovement::where('barcode_id', $barcode_component)->where('locator_to', $locator)->where('status_to', 'in')->whereNull('deleted_at')->get();

                if(count($component_check) > 0) {
                    $packinglist_check = DB::table('packinglist_subcont')->where('no_packinglist', $packinglist)->whereNull('deleted_at')->first();

                    if($packinglist_check != null) {
                        $po_buyer_packinglist = explode(',', trim($packinglist_check->po_buyer));

                        $data = DB::table('bundle_detail')->select('bundle_header.poreference', 'bundle_detail.qty')->join('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('bundle_detail.barcode_id', $barcode_component)->first();
                        
                        if($data != null) {
                
                            if(in_array($data->poreference, $po_buyer_packinglist)) {
                                $isset_komponen_packinglist = PackinglistSubcont::where('bundle_id', $barcode_component)->whereNull('deleted_at')->get();
                                $isset_komponen_packinglist_temp = PackingSubTemp::where('bundle_id', $barcode_component)->whereNull('deleted_at')->get();
                                
                                if(count($isset_komponen_packinglist) > 0) {
                                    $packinglist_array = $isset_komponen_packinglist->pluck('id_packinglist')->toArray();
                                    $locator_array = DB::table('packinglist_subcont')->whereIn('no_packinglist', $packinglist_array)->whereNull('deleted_at')->pluck('process')->toArray();
                                    if(in_array($locator, $locator_array)) {
                                        return response()->json('komponent sudah pernah discan',422);
                                    } else {
                                        $data_inserti = PackingSubTemp::firstOrCreate([
                                            'id_packinglist' => $packinglist,
                                            'no_polybag' => $polibag,
                                            'bundle_id' => $barcode_component,
                                            'qty' => $data->qty,
                                            'user_id' => \Auth::user()->id,
                                            'factory_id' => \Auth::user()->factory_id,
                                        ]);
                    
                                        $obj = new StdClass();
                                        $obj->packinglist = $packinglist;
                                        $obj->polibag = $polibag;
                                        $obj->locator = $locator;
                                        $obj->state = $state;
                    
                                        return response()->json($obj,200);
                                    }
                                } elseif(count($isset_komponen_packinglist_temp) > 0) {
                                    return response()->json('komponent sedang proses scan!',422);
                                } else {
                                    $data_inserti = PackingSubTemp::firstOrCreate([
                                        'id_packinglist' => $packinglist,
                                        'no_polybag' => $polibag,
                                        'bundle_id' => $barcode_component,
                                        'qty' => $data->qty,
                                        'user_id' => \Auth::user()->id,
                                        'factory_id' => \Auth::user()->factory_id,
                                    ]);
                
                                    $obj = new StdClass();
                                    $obj->packinglist = $packinglist;
                                    $obj->polibag = $polibag;
                                    $obj->locator = $locator;
                                    $obj->state = $state;
                
                                    return response()->json($obj,200);
                                }
                            } else {
                                return response()->json('PO Buyer pada komponen tidak untuk packinglist '.$packinglist.'!',422);
                            }
                        } else {
                            return response()->json('barcode tidak ditemukan!',422);
                        }
                    } else {

                    }
                } else {
                    return response()->json('barcode belum scan in di locator '.$master_locator->locator_name.'!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat parsing data!',422);
            }
        }
    }

    public function deleteComponent(Request $request, $id)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $polibag = $request->polibag;
            $locator = $request->locator;
            $state = $request->state;

            if($packinglist != null && $locator != null && $state != null && $polibag != null && $id != null) {

                $data = PackingSubTemp::where('bundle_id', $id)->delete(); 
                $obj = new StdClass();
                $obj->packinglist = $packinglist;
                $obj->polibag = $polibag;
                $obj->locator = $locator;
                $obj->state = $state;

                return response()->json($obj,200);
            } else {
                return response()->json('Terjadi kesalahan saat parsing data!',422);
            }
        }
    }

    public function saveComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $state = $request->state;
            $locator_id = $request->locator;
            if($packinglist != null) {
                $data = PackingSubTemp::where('id_packinglist', $packinglist)->get();
                if(count($data) > 0) {
                    try {
                        DB::beginTransaction();

                        $locator = DB::table('master_locator')->where('id', $locator_id)->first();
                        foreach($data as $aa) {
                            PackinglistSubcont::FirstOrCreate([
                                'id_packinglist' => $aa->id_packinglist,
                                'no_polybag' => $aa->no_polybag,
                                'bundle_id' => $aa->bundle_id,
                                'qty' => $aa->qty,
                                'user_id' => $aa->user_id,
                                'factory_id' => $aa->factory_id,
                            ]);
                            DistribusiMovement::FirstOrCreate([
                                'barcode_id'   => $aa->bundle_id,
                                'locator_from' => null,
                                'status_from'  => null,
                                'locator_to'   => $locator_id,
                                'status_to'    => $state,
                                'user_id'      => \Auth::user()->id,
                                'description'  => $state.' SUBCONT from locator '.$locator->locator_name,
                                'ip_address'   => $this->getIPClient(),
                                'deleted_at'   => null,
                                'loc_dist'     => '-'
                            ]);
                        }
                        DB::table('packinglist_subcont')->where('no_packinglist', $packinglist)->whereNull('deleted_at')->update(['status' => 'release']);
                        PackingSubTemp::where('id_packinglist', $packinglist)->delete();
                        $obj = new StdClass();
                        $obj->packinglist = $packinglist;
                        
                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }

                    return response()->json($obj,200);
                } else {
                    return response()->json('barcode tidak ditemukan!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat parsing data!',422);
            }
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
