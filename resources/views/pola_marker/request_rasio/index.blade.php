@extends('layouts.app',['active' => 'request_rasio_download'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Download Permintaan Rasio SSP</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Download Permintaan Rasio SSP</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

@include('includes.notif_mo_update')

<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="{{ route('markerRequestRasio.download') }}" id="download_request_rasio" method="POST">
			<fieldset class="content-group">
				<div class="form-group">

					@if(\Auth::user()->factory_id == '0')

						<label class="control-label col-lg-2 text-bold">Pilih Factory</label>

						<div class="col-lg-10">

							@foreach($factorys as $factory)

							<label class="radio-inline">
								<div><input type="radio" name="factory" value="{{ $factory->id }}" required></div>
								{{ $factory->factory_alias }}
							</label>

							@endforeach

						</div>
					
					@endif

				</div>
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						@csrf
						<div class="input-group">
							<span class="input-group-btn">	
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Download</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/notif_mo_update.js') }}"></script>
@endsection
