<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class SubcontFactoryController extends Controller
{
    //MASTER
    public function index(Request $request)
    {
        return view('master_parameter.subcont_factory.index');
    }

    public function ajaxGetDataMasterSubcontFactory(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_subcont')
                    ->wherenull('deleted_at')
                    ->where('factory_id',\Auth::user()->factory_id)
                    ->orderBy('created_at', 'desc')
                    ->get();

            //datatables
            return DataTables::of($data)
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('subcont_factory.editSubcontFactory', $data->id),
                        'id' => $data->id,
                        'deletes' => route('subcont_factory.deleteSubcontFactory', $data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //add new master
    public function addSubcontFactory(Request $request)
    {

        $this->validate($request, [
            'subcont_fact_name' => 'required',
            'subcont_initial_name' => 'required',
            'division_name'         => 'required'
        ]);

        $subcont_factory_name = trim(strtoupper($request->subcont_fact_name));
        $subcont_initial_name = trim(strtoupper($request->subcont_initial_name));
        $division_name = trim(strtoupper($request->division_name));

        $data = array([
            'subcont_name'      => $subcont_factory_name,
            'initials'          => $subcont_initial_name,
            'division'          =>  $division_name,
            'created_by'        => \Auth::user()->id,
            'factory_id'        => \Auth::user()->factory_id,
            'is_active'         => 't',
            'created_at'        => Carbon::now()
        ]);

        // cek exists
        $cekifexist = DB::table('master_subcont')
                        ->where('subcont_name', $subcont_factory_name)
                        ->where('division',$division_name)
                        ->where('factory_id', \Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Nama Subcont Sudah Ada..!', 422);
        }

        try {
            DB::begintransaction();
            DB::table('master_subcont')->insert($data);
            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master
    public function editSubcontFactory($id)
    {
        $data = DB::table('master_subcont')
                        ->where('id', $id)
                        ->first();
        return response()->json($data, 200);
    }

    //update master
    public function updateSubcontFactory(Request $request)
    {
        $data = $request->all();
        
        $subcont_factory_name   = trim(strtoupper($data['subcont_fact_name_update']));
        $subcont_initial_name   = trim(strtoupper($data['subcont_initial_name_update']));
        $division_name          = trim(strtoupper($data['division_name_update']));

        // cek exists
        $cekifexist = DB::table('master_subcont')
                        ->where('subcont_name', $subcont_factory_name)
                        ->where('initials',$subcont_initial_name)
                        ->where('division',$division_name)
                        ->where('factory_id', \Auth::user()->factory_id)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Nama Subcont Sudah Ada..!', 422);
        }

        try {
            DB::beginTransaction();
                DB::table('master_subcont')
                ->where('id', $data['id_update'])
                ->update([
                    'subcont_name' => $subcont_factory_name,
                    'initials' => $subcont_initial_name,
                    'division' => $division_name,
                    'updated_by' => \Auth::user()->id,
                    'factory_id' => \Auth::user()->factory_id,
                    'updated_at' => Carbon::now()
                ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }    

        return view('master_parameter.subcont_factory.index');

    }

    //delete master
    public function deleteSubcontFactory(Request $request)
    {
        $id = $request->id;
        DB::table('master_subcont')
                ->where('id', $id)
                ->update([
                    'deleted_by' => \Auth::user()->id,
                    'is_active' => 'f',
                    'deleted_at' => Carbon::now()
                ]);

        return view('master_parameter.subcont_factory.index');
    }

    //END OF MASTER
    
}
