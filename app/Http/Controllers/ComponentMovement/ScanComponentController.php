<?php

namespace App\Http\Controllers\ComponentMovement;

use DB;
use StdClass;
use Carbon\Carbon;
use App\Models\DistribusiMovement;
use App\Models\MasterPpcm;
use App\Models\MasterSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScanComponentController extends Controller
{
    private $sds_connection;

    public function __construct()
    {
        $this->sds_connection = DB::connection('sds_live');
    }

    public function index()
    {
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
        return view('component_movement.scan_component.index', compact('locator_set'));
    }

    public function dataLocator()
    {
        if(request()->ajax())
        {
            $get_role = DB::table('role_user')->where('user_id', \Auth::user()->id)->pluck('role_id')->toArray();
            $data = DB::table('master_locator')->where('deleted_at', null)->whereIn('role', $get_role)->get();

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                if($data->is_cutting) {
                    $is_cutting = 'true';
                } else {
                    $is_cutting = 'false';
                }

                if($data->is_setting) {
                    $is_setting = 'true';
                } else {
                    $is_setting = 'false';
                }

                if($data->is_artwork) {
                    $is_artwork = 'true';
                } else {
                    $is_artwork = 'false';
                }
                return '<button class="btn btn-primary btn-xs" data-id="'.$data->id.'" data-name="'.$data->locator_name.'" data-is_cutting="'.$is_cutting.'" data-is_setting="'.$is_setting.'" data-is_artwork="'.$is_artwork.'" id="select_locator">Select</button>';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            $locator_id = $request->locator_id;
            $state = $request->state;

            if($barcode_id != null && $locator_id != null) {

                $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                if($barcode_check != null) {

                    if($locator->is_cutting) {

                        // locator cutting

                        $component_movement_out = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                        if($component_movement_out->count() > 0) {
                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                        } else {

                            $bundle_check = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = DB::table('master_komponen')->where('id', $komponen_detail->komponen)->where('deleted_at', null)->first()->komponen_name;

                            try {
                                DB::beginTransaction();

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => null,
                                    'status_from'  => null,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => '-'
                                ]);

                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => '-'
                                ];

                                if($barcode_check->sds_barcode != null) {
                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                    if($update_sds != null) {
                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                'location' => $convert_locator['locator_sds'],
                                                $convert_locator['column_update'] => Carbon::now(),
                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                'factory' => $convert_locator['factory_sds'],
                                            ]);
                                        } 
                                    }
                                }

                                DB::commit();
 
                            } catch (Exception $e) {
                                DB::rollback();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }

                            return response()->json($response,200);
                        }

                    // } else if($locator->is_setting){

                    //     $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                    //     if($out_cutting_check != null){

                    //         $barcode_bundle_search = DB::table('bundle_detail')->where([
                    //             ['bundle_header_id', $barcode_check->bundle_header_id],
                    //             ['start_no',$barcode_check->start_no],
                    //             ['end_no',$barcode_check->end_no]
                    //         ]);

                    //         $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                    //         $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                    //         $barcode_hasil = $barcode_bundle_search->first();

                    //         $banyak_komponen = $barcode_bundle_search->count();

                    //         $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                    //         $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                    //         $bundle_terima = $movement_check->count();
                    //         $bundle_check  = $movement_check->first();

                    //         $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                    //         $komponen_name = $barcode_check->komponen_name;

                    //         if($bundle_terima<1){
                    //             $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                    //             $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                    //             $ppcm_check = DB::table('master_ppcm')->where([
                    //                 ['master_ppcm.factory_id',$bundle_header->factory_id],
                    //                 ['set_type',$set_type],
                    //                 ['season',$bundle_header->season],
                    //                 ['articleno',$bundle_header->article],
                    //                 ['po_buyer',$bundle_header->poreference],
                    //                 ['master_ppcm.deleted_at', null]
                    //             ])
                    //             ->select('master_ppcm.*','master_setting.area_name')
                    //             ->leftJoin('master_setting', 'master_ppcm.area_id', '=', 'master_setting.id')
                    //             ->first();

                    //             if(!$ppcm_check){
                    //                 return response()->json($barcode_id, 331);
                    //             } else {
                    //                 //master_style_detail
                    //                 $style_detail_id = $barcode_check->style_detail_id;

                    //                 $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                    //                 if(strlen(trim($process_id->process))>0){
                    //                     $array_process = explode(",",$process_id->process);
                    //                 }
                    //                 else{
                    //                     $array_process = array();
                    //                 }

                    //                 if(count($array_process) == 0){

                    //                     DistribusiMovement::FirstOrCreate([
                    //                         'barcode_id'   => $barcode_id,
                    //                         'locator_from' => $out_cutting_check->locator_to,
                    //                         'status_from'  => $out_cutting_check->status_to,
                    //                         'locator_to'   => $locator_id,
                    //                         'status_to'    => $state,
                    //                         'user_id'      => \Auth::user()->id,
                    //                         'description'  => $state.' '.$locator->locator_name,
                    //                         'ip_address'   => $this->getIPClient(),
                    //                         'deleted_at'   => null,
                    //                         'loc_dist'     => $ppcm_check->area_name
                    //                     ]);

                    //                     // if($bundle_terima == $banyak_komponen){
                    //                         // dd($barcode_hasil->qty);
                    //                         $qty_actual = ($ppcm_check->qty_actual == null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                    //                         MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                    //                     // }

                    //                     $response = [
                    //                         'barcode_id'    => $barcode_id,
                    //                         'style'         => $bundle_header->style,
                    //                         'article'       => $bundle_header->article,
                    //                         'po_buyer'      => $bundle_header->poreference,
                    //                         'part'          => $komponen_detail->part,
                    //                         'komponen_name' => $komponen_name,
                    //                         'size'          => $bundle_header->size,
                    //                         'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                    //                         'qty'           => $barcode_check->qty,
                    //                         'cut'           => $bundle_header->cut_num,
                    //                         'process'       => $locator->locator_name,
                    //                         'status'        => $state,
                    //                         'loc_dist'      => $ppcm_check->area_name
                    //                     ];
                    //                     return response()->json($response,200);
                    //                 } else {
                    //                     $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                    //                     $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                    //                     $banyak_process = count($get_process_locator) + 1;

                    //                     array_push($array_locator,10);

                    //                     $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                    //                     $count_process_check = $process_check->count();

                    //                     if($count_process_check == $banyak_process){
                    //                         if($ppcm_check->qty_actual < $ppcm_check->qty_split){

                    //                             $barcode_bundle_search = DB::table('bundle_detail')->where([
                    //                                 ['bundle_header_id', $barcode_check->bundle_header_id],
                    //                                 ['start_no',$barcode_check->start_no],
                    //                                 ['end_no',$barcode_check->end_no]
                    //                             ]);

                    //                             $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                    //                             $barcode_hasil = $barcode_bundle_search->first();

                    //                             $banyak_komponen = $barcode_bundle_search->count();

                    //                             // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                    //                             // $bundle_terima = $movement_check->count();
                    //                             // $bundle_check  = $movement_check->first();

                    //                             $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                    //                             $komponen_name = $barcode_check->komponen_name;

                    //                             $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                    //                             if($check_movement > 0) {
                    //                                 return response()->json('Barcode sudah di scan in!',422);
                    //                             }

                    //                             DistribusiMovement::FirstOrCreate([
                    //                                 'barcode_id'   => $barcode_id,
                    //                                 'locator_from' => $out_cutting_check->locator_to,
                    //                                 'status_from'  => $out_cutting_check->status_to,
                    //                                 'locator_to'   => $locator_id,
                    //                                 'status_to'    => $state,
                    //                                 'user_id'      => \Auth::user()->id,
                    //                                 'description'  => $state.' '.$locator->locator_name,
                    //                                 'ip_address'   => $this->getIPClient(),
                    //                                 'deleted_at'   => null,
                    //                                 'loc_dist'     => $ppcm_check->area_name
                    //                             ]);

                    //                             $qty_actual = ($ppcm_check->qty_actual==null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                    //                             MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);


                    //                             $response = [
                    //                                 'barcode_id'    => $barcode_id,
                    //                                 'style'         => $bundle_header->style,
                    //                                 'article'       => $bundle_header->article,
                    //                                 'po_buyer'      => $bundle_header->poreference,
                    //                                 'part'          => $komponen_detail->part,
                    //                                 'komponen_name' => $komponen_name,
                    //                                 'size'          => $bundle_header->size,
                    //                                 'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                    //                                 'qty'           => $barcode_check->qty,
                    //                                 'cut'           => $bundle_header->cut_num,
                    //                                 'process'       => $locator->locator_name,
                    //                                 'status'        => $state,
                    //                                 'loc_dist'      => $ppcm_check->area_name
                    //                             ];
                    //                             return response()->json($response,200);
                    //                         }
                    //                         else{
                    //                             //qty berlebihan
                    //                             return response()->json($barcode_id, 331);
                    //                         }
                    //                     } else {

                    //                         $get_process_check = $process_check->pluck('locator_to')->toArray();

                    //                         $array_belum_scan = array_diff($array_locator,$get_process_check);

                    //                         $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                    //                         $info_belum_scan = '';
                    //                         foreach ($locator_belum_scan as $key => $value) {
                    //                             $info_belum_scan .= ''.$value->locator_name.', ';
                    //                         }

                    //                         $info = trim($info_belum_scan,", ");

                    //                         return response()->json('Barcode belum discan proses '.$info.'!',422);
                    //                     }
                    //                 }
                    //             }

                    //         } else {

                    //             $style_detail_id = $barcode_check->style_detail_id;

                    //             $process_id = DB::table('master_style_detail')->where('id',$style_detail_id)->first();

                    //             if($process_id->process == null || $process_id->process == '' ){
                    //                 $array_process = array();
                    //             } else {
                    //                 $array_process = explode(",",$process_id->process);
                    //             }

                    //             if(count($array_process) < 1){

                    //                 $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                    //                 if($check_movement > 0) {
                    //                     return response()->json('Barcode sudah di scan in!',422);
                    //                 }
                    //                 // dd(!$bundle_check);
                    //                 if(!$bundle_check){
                    //                     DistribusiMovement::FirstOrCreate([
                    //                         'barcode_id'   => $barcode_id,
                    //                         'locator_from' => $out_cutting_check->locator_to,
                    //                         'status_from'  => $out_cutting_check->status_to,
                    //                         'locator_to'   => $locator_id,
                    //                         'status_to'    => $state,
                    //                         'user_id'      => \Auth::user()->id,
                    //                         'description'  => $state.' '.$locator->locator_name,
                    //                         'ip_address'   => $this->getIPClient(),
                    //                         'deleted_at'   => null,
                    //                         'loc_dist'     => $bundle_check->loc_dist
                    //                     ]);

                    //                     $response = [
                    //                         'barcode_id'    => $barcode_id,
                    //                         'style'         => $bundle_header->style,
                    //                         'article'       => $bundle_header->article,
                    //                         'po_buyer'      => $bundle_header->poreference,
                    //                         'part'          => $komponen_detail->part,
                    //                         'komponen_name' => $komponen_name,
                    //                         'size'          => $bundle_header->size,
                    //                         'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                    //                         'qty'           => $barcode_check->qty,
                    //                         'cut'           => $bundle_header->cut_num,
                    //                         'process'       => $locator->locator_name,
                    //                         'status'        => $state,
                    //                         'loc_dist'      => $bundle_check->loc_dist
                    //                     ];
                    //                     return response()->json($response,200);
                    //                 } else {
                    //                     if($bundle_check->locator_to == $locator_id){

                    //                         $check_record = DistribusiMovement::where([
                    //                             'barcode_id'   => $barcode_id,
                    //                             'locator_from' => $out_cutting_check->locator_to,
                    //                             'status_from'  => $out_cutting_check->status_to,
                    //                             'locator_to'   => $locator_id,
                    //                             'status_to'    => $state,
                    //                             'description'  => $state.' '.$locator->locator_name,
                    //                             'deleted_at'   => null,
                    //                             'loc_dist'     => $bundle_check->loc_dist
                    //                         ])->exists();

                    //                         if(!$check_record){
                    //                             DistribusiMovement::FirstOrCreate([
                    //                                 'barcode_id'   => $barcode_id,
                    //                                 'locator_from' => $out_cutting_check->locator_to,
                    //                                 'status_from'  => $out_cutting_check->status_to,
                    //                                 'locator_to'   => $locator_id,
                    //                                 'status_to'    => $state,
                    //                                 'user_id'      => \Auth::user()->id,
                    //                                 'ip_address'   => $this->getIPClient(),
                    //                                 'description'  => $state.' '.$locator->locator_name,
                    //                                 'deleted_at'   => null,
                    //                                 'loc_dist'     => $bundle_check->loc_dist
                    //                             ]);

                    //                             // if($bundle_terima == $banyak_komponen){
                    //                             //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                    //                             //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                    //                             // }
                    //                             $response = [
                    //                                 'barcode_id'    => $barcode_id,
                    //                                 'style'         => $bundle_header->style,
                    //                                 'article'       => $bundle_header->article,
                    //                                 'po_buyer'      => $bundle_header->poreference,
                    //                                 'part'          => $komponen_detail->part,
                    //                                 'komponen_name' => $komponen_name,
                    //                                 'size'          => $bundle_header->size,
                    //                                 'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                    //                                 'qty'           => $barcode_check->qty,
                    //                                 'cut'           => $bundle_header->cut_num,
                    //                                 'process'       => $locator->locator_name,
                    //                                 'status'        => $state,
                    //                                 'loc_dist'      => $bundle_check->loc_dist
                    //                             ];

                    //                             return response()->json($response,200);
                    //                         }
                    //                         else{
                    //                             return response()->json('Barcode sudah pernah scan!',422);
                    //                         }

                    //                     }
                    //                     else{
                    //                         return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                    //                     }

                    //                 }
                    //             } else {
                    //                 $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                    //                 $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                    //                 $banyak_process = count($get_process_locator) + 1;

                    //                 array_push($array_locator,10);

                    //                 $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                    //                 $count_process_check = $process_check->count();

                    //                 if($count_process_check == $banyak_process){

                    //                     $barcode_bundle_search = DB::table('bundle_detail')->where([
                    //                         ['bundle_header_id', $barcode_check->bundle_header_id],
                    //                         ['start_no',$barcode_check->start_no],
                    //                         ['end_no',$barcode_check->end_no]
                    //                     ]);

                    //                     $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                    //                     $barcode_hasil = $barcode_bundle_search->first();

                    //                     $banyak_komponen = $barcode_bundle_search->count();

                    //                     // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                    //                     // $bundle_terima = $movement_check->count();
                    //                     // $bundle_check  = $movement_check->first();

                    //                     $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                    //                     if($bundle_check->locator_to == $locator_id){

                    //                         $check_record = DistribusiMovement::where([
                    //                             'barcode_id'   => $barcode_id,
                    //                             'locator_from' => $out_cutting_check->locator_to,
                    //                             'status_from'  => $out_cutting_check->status_to,
                    //                             'locator_to'   => $locator_id,
                    //                             'status_to'    => $state,
                    //                             'description'  => $state.' '.$locator->locator_name,
                    //                             'deleted_at'   => null,
                    //                             'loc_dist'     => $bundle_check->loc_dist
                    //                         ])->exists();

                    //                         if(!$check_record){
                    //                             DistribusiMovement::FirstOrCreate([
                    //                                 'barcode_id'   => $barcode_id,
                    //                                 'locator_from' => $out_cutting_check->locator_to,
                    //                                 'status_from'  => $out_cutting_check->status_to,
                    //                                 'locator_to'   => $locator_id,
                    //                                 'status_to'    => $state,
                    //                                 'user_id'      => \Auth::user()->id,
                    //                                 'description'  => $state.' '.$locator->locator_name,
                    //                                 'ip_address'   => $this->getIPClient(),
                    //                                 'deleted_at'   => null,
                    //                                 'loc_dist'     => $bundle_check->loc_dist
                    //                             ]);

                    //                             // $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                    //                             // if($bundle_terima == $banyak_komponen){
                    //                             //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                    //                             //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                    //                             // }
                    //                             $response = [
                    //                                 'barcode_id'    => $barcode_id,
                    //                                 'style'         => $bundle_header->style,
                    //                                 'article'       => $bundle_header->article,
                    //                                 'po_buyer'      => $bundle_header->poreference,
                    //                                 'part'          => $komponen_detail->part,
                    //                                 'komponen_name' => $komponen_name,
                    //                                 'size'          => $bundle_header->size,
                    //                                 'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                    //                                 'qty'           => $barcode_check->qty,
                    //                                 'cut'           => $bundle_header->cut_num,
                    //                                 'process'       => $locator->locator_name,
                    //                                 'status'        => $state,
                    //                                 'loc_dist'      => $bundle_check->loc_dist
                    //                             ];

                    //                             return response()->json($response,200);
                    //                         }
                    //                         else{
                    //                             return response()->json('Barcode sudah pernah scan!',422);
                    //                         }

                    //                     }
                    //                     else{
                    //                         return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                    //                     }
                    //                 }
                    //                 else{

                    //                     $get_process_check = $process_check->pluck('locator_to')->toArray();

                    //                     $array_belum_scan = array_diff($array_locator,$get_process_check);

                    //                     $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                    //                     $info_belum_scan = '';
                    //                     foreach ($locator_belum_scan as $key => $value) {
                    //                         $info_belum_scan .= ''.$value->locator_name.', ';
                    //                     }

                    //                     $info = trim($info_belum_scan,", ");

                    //                     return response()->json('Barcode belum discan proses '.$info.'!',422);
                    //                 }
                    //             }
                    //         }

                    //         $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                    //     }
                    //     else{
                    //         return response()->json('Barcode belum out cutting!',422);
                    //     }

                    // SINKRON SDS /////////////////////////////////////////////////////////////////////////////////////////////////////
                    } else if($locator->is_setting) {

                        $user=\Auth::user()->nik;
                        $factory=\Auth::user()->factory_id;
                        $dstat=1;
                        $alert=NULL;
                        $line_location = null;

                        $chk = $this->sds_connection->select($this->sds_connection->raw("
                            SELECT
                            barcode_id,TRIM(style) AS style,TRIM(set_type) AS set_type,season,TRIM(poreference) AS poreference,article,TRIM(size) AS size,cut_num,start_num,end_num,qty,
                            proc_scan IS NULL AND supply_scan IS NULL AS not_scanned,
                            cut_out,is_wip,supply_unlock,rejects,processing_fact,
                            CASE WHEN is_set THEN CONCAT(TRIM(location_name),' (',TRIM(factory),')') ELSE NULL END AS is_set,
                            CASE WHEN is_supplied THEN supplied_to ELSE NULL END AS is_supplied,
                            CASE
                                WHEN NOT artwork THEN 'Artwork'
                                WHEN NOT he THEN 'Heat Transfer'
                                WHEN NOT pad THEN 'Pad Print'
                                WHEN NOT ppa THEN 'PPA Jahit'
                                WHEN NOT fuse THEN 'Fuse'
                                WHEN NOT auto THEN 'Auto'
                                WHEN NOT bonding THEN 'Bonding'
                                ELSE NULL
                            END AS incomplete_proc
                            FROM bundle_for_set_new
                            WHERE barcode_id = '$barcode_check->sds_barcode'::NUMERIC
                        "));
            
                        if(count($chk) > 0) {
                            
                            $chk = $chk[0];
            
                            if($chk->not_scanned==false){
                                $alert="Bundle sudah dipindai di SDS!";
                                $dstat=0;
                            }else if($chk->processing_fact != $factory){
                                $alert="Bundle merupakan alokasi pabrik lain!";
                                $dstat=0;
                            }else if($chk->is_wip != ''){
                                $alert="Bundle belum Check-Out proses ".($chk->is_wip)."!";
                                $dstat=0;
                            }else if($chk->supply_unlock == true){
                                
                            }else if($chk->is_supplied != ''){
                                $alert="Bundle sudah di supply ke ".($chk->is_supplied)."!";
                                $dstat=0;
                            }else if($chk->is_set != ''){
                                $alert="Bundle sudah menjalani Setting di ".($chk->is_set)."!";
                                $dstat=0;
                            }else if($chk->rejects == true){
                                $alert="Bundle dalam proses ganti reject!";
                                $dstat=0;
                            }else if($chk->cut_out == false){
                                $alert="Bundle belum checkout dari Cutting/Bundling!";
                                $dstat=0;
                            }else if($chk->processing_fact==2 && $chk->incomplete_proc != ''){
                                $alert="Bundle belum menjalani proses ".($chk->incomplete_proc)."!";
                                $dstat=0;
                            }
            
                            if($dstat > 0){
            
                                $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                    SELECT
                                    CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                    FROM set_pairs_alloc_new
                                    WHERE
                                    season='$chk->season'::VARCHAR AND set_type='$chk->set_type'::VARCHAR AND poreference='$chk->poreference'::VARCHAR AND article='$chk->article'::VARCHAR
                                    AND size='$chk->size'::VARCHAR AND cut_num='$chk->cut_num'::INTEGER AND start_num='$chk->start_num'::INTEGER AND end_num='$chk->end_num'::INTEGER
                                "));
            
                                $set_notes=(count($set_notes) > 0 ? $set_notes[0] : NULL);
            
                                if($set_notes != NULL && $set_notes->is_moving==true){
                                    $alert="Pasangan-pasangan bundle ini sedang dalam proses movement di area Setting!";
                                    $dstat=0;
                                }else if($set_notes != NULL && $set_notes->sugg_fact != ''){
                                    $ppcm_id=NULL;
                                    $s_factory=$factory;
                                    if($set_notes->sugg_fact == $factory){
                                        $set_notes=$set_notes->sugg_loc;
                                    }else{
                                        $alert="Garment ini dikerjakan di ".($this->sds_connection->table('factory')->where('factory_id',$set_notes->sugg_fact)->first()->factory_name)."!";
                                        $dstat=0;
                                    }
                                }else if($factory==1){
                                    $ppcm_id=NULL;
                                    $set_notes=NULL;
                                    $s_factory=NULL;
                                }else{
                                    $choose_option = 2;
                                    if($choose_option == 1) {
                                        $set_notes=NULL;
                                        $que = "SELECT set_helper_new AS set_helper FROM set_helper_new(?,?,?,?,?::INTEGER)";
                                        $ppcm_id=$this->db->query($que,array(
                                            $chk->season,
                                            $chk->article,
                                            $chk->set_type,
                                            $chk->poreference,
                                            (int)$factory
                                        ))->result()[0]->set_helper;
                                        $ppcm_id = ($ppcm_id==NULL || $ppcm_id=='' ? NULL : $ppcm_id);
                                        $s_factory=NULL;
                                    } else if($choose_option == 2) {
            
                                        $check_temp_setting_set = $this->sds_connection
                                            ->table('jaz_temp_scan_setting')
                                            ->where('style', $chk->style)
                                            ->where('set_type', $chk->set_type)
                                            ->where('season', $chk->season)
                                            ->where('article', $chk->article)
                                            ->where('poreference', $chk->poreference)
                                            ->where('size', $chk->size)
                                            ->where('cut_num', $chk->cut_num)
                                            ->where('start_num', $chk->start_num)
                                            ->where('end_num', $chk->end_num)
                                            ->where('qty', $chk->qty)
                                            ->where('processing_fact', $factory)
                                            ->get();
            
                                        if(count($check_temp_setting_set) > 0) {
                                            $check_temp_setting_set = $check_temp_setting_set->first();
                                            $ppcm_id = $check_temp_setting_set->location;
                                        } else {
                                            $style_array = array();
                                            if($chk->set_type == 'TOP') {
                                                $style_array[] = $chk->style.'-TOP';
                                            } else if($chk->set_type == 'BOTTOM') {
                                                $style_array[] = $chk->style.'-BOT';
                                            } else if($chk->set_type == 'INNER') {
                                                $style_array[] = $chk->style.'-INN';
                                                $style_array[] = $chk->style.'-TIN';
                                            } else if($chk->set_type == 'OUTER') {
                                                $style_array[] = $chk->style.'-OUT';
                                                $style_array[] = $chk->style.'-TOU';
                                            } else {
                                                $style_array[] = $chk->style;
                                            }
                
                                            $get_ppcm_ids = $this->sds_connection
                                                ->table('jaz_ppcm_upload_new')
                                                ->whereIn('style', $style_array)
                                                ->whereNull('deleted_at')
                                                ->where('poreference', $chk->poreference)
                                                ->where('season', $chk->season)
                                                ->where('article', $chk->article)
                                                ->get();
                
                                            if(count($get_ppcm_ids) > 0) {
                                                $selisih = 10000000;
                                                $ppcm_id = null;
                                                foreach($get_ppcm_ids as $data_ppcm) {
            
                                                    $qty_temp = $this->sds_connection->select($this->sds_connection->raw("select sum(qty) as qty from jaz_temp_scan_setting where location = '$data_ppcm->id'::numeric"))[0]->qty;
            
                                                    $qty_mod = $qty_temp + $data_ppcm->qty_mod;
            
                                                    if($data_ppcm->qty_split > $qty_mod) {
            
                                                        $get_selisih = abs(($data_ppcm->qty_split - $qty_mod) - $chk->qty);
                                                        if($get_selisih < $selisih) {
                                                            $selisih = $get_selisih;
                                                            $ppcm_id = $data_ppcm->id;
                                                        }
            
                                                    }
                                                }
                                            } else {
                                                $ppcm_id = null;
                                            }
                                        }
                                        $s_factory=NULL;
                                        $set_notes=NULL;
                                    }
                                }
            
                                if($dstat > 0){
                                    $check_temp_setting = $this->sds_connection->table('temp_scan_proc')->insert([
                                        'user_id' => $user,
                                        'proc' => 'SET',
                                        'ap_id' => $barcode_check->sds_barcode,
                                        'set_notes'=>$set_notes,
                                        'factory'=>$s_factory,
                                        'ppcm_id'=>$ppcm_id
                                    ]);

                                    $j=0;

                                    // NEW ENTRIES
                                    $bc_list = $this->sds_connection->select($this->sds_connection->raw("
                                        SELECT ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,
                                        TRIM(lc.location) AS location,TRIM(lc.factory) AS factory,
                                        MAX(bi.qty) AS qty,COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id
                                        FROM (
                                        SELECT ap_id,ppcm_id
                                        FROM temp_scan_proc
                                        WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NOT NULL AND NOT check_out
                                        ) ts
                                        JOIN bundle_info_new bi ON ts.ap_id=bi.barcode_id
                                        JOIN ppcm_upload_new pu ON ts.ppcm_id=pu.id
                                        JOIN location lc ON pu.line ~ CONCAT(E'^',REPLACE(TRIM(lc.factory),' ',E'#'),' ',REPLACE(TRIM(lc.location_name),' ','0?'),E'$')
                                        GROUP BY ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,lc.location,lc.factory
                                    "));

                                    foreach($bc_list as $bc){
                                        $this->sds_connection->select($this->sds_connection->raw("UPDATE ppcm_upload_new SET qty_mod=qty_mod+'$bc->qty' WHERE id='$bc->ppcm_id'"));

                                        $cond = explode(',',$bc->bc_id);
                                        $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                            'location'=>$bc->location,
                                            'factory'=>$bc->factory,
                                            'setting'=>Carbon::now(),
                                            'setting_pic'=>$user,
                                            'supply_unlock'=>false
                                        ]);
                                        $line_location = $bc->location;
                                        $j+=$bc->count;
                                    }

                                    $bc_list = $this->sds_connection->select($this->sds_connection->raw("SELECT COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id,ts.set_notes,f.factory_name FROM (SELECT ap_id,set_notes,factory FROM temp_scan_proc WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NULL AND NOT check_out) ts JOIN factory f ON ts.factory=f.factory_id GROUP BY ts.set_notes,f.factory_name"));

                                    foreach($bc_list as $bc){
                                        
                                        $cond = explode(',',$bc->bc_id);
                                        $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                            'location'=>$bc->set_notes,
                                            'factory'=>$bc->factory_name,
                                            'setting'=> Carbon::now(),
                                            'setting_pic'=>$user,
                                            'supply_unlock'=>false
                                        ]);
                                        $line_location = $bc->set_notes;
                                        $j+=$bc->count;
                                    }

                                    $this->sds_connection->table('system_log')->insert([
                                        'user_nik' => $user,
                                        'user_proc' => 'SET',
                                        'log_operation' => 'set_checkin',
                                        'log_text' => 'add item '.$barcode_check->sds_barcode.' to SET check in',
                                    ]);
                                }
                            }
            
                        } else {
                            $alert="Bundle tidak terdaftar!";
                            $dstat=0;
                        }

                        
                        if($dstat == 1) {
                            if($line_location == null) {
                                return response()->json($barcode_id, 331);
                            } else {
                                $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();
                                $komponen_name = $barcode_check->komponen_name;
    
                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $bundle_header->style,
                                    'article'       => $bundle_header->article,
                                    'po_buyer'      => $bundle_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $bundle_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $bundle_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $line_location
                                ];
    
                                $last_movement = DistribusiMovement::where('barcode_id', $barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
    
                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => $last_movement->locator_to,
                                    'status_from'  => $last_movement->status_to,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => $line_location
                                ]);
    
                                $this->sds_connection->table('temp_scan_proc')
                                    ->where('user_id' ,$user)
                                    ->where('proc','SET')
                                    ->where('check_out',false)
                                    ->delete();
    
                                return response()->json($response,200);
                            } 
                        } else {
                            return response()->json($alert,422);
                        }

                    } else if($locator->is_artwork) {
                        
                        // locator artwork

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                
                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                if($barcode_check->sds_barcode != null) {
                                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                    if($update_sds != null) {
                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);

                                                            $type_set = DB::table('types')->where('id', $komponen_detail->type_id)->first();
                                                            if($type_set->type_name == 'Non') {
                                                                $type_set = '0';
                                                            } else {
                                                                $type_set = $type_set->type_name;
                                                            }

                                                            $check_data_exists = $this->sds_connection->table('v_packing_list_new')
                                                                                    ->where('style', $barcode_header->style)
                                                                                    ->where('set_type', $type_set)
                                                                                    ->where('poreference', $barcode_header->poreference)
                                                                                    ->where('article', $barcode_header->article)
                                                                                    ->where('size', $barcode_header->size)
                                                                                    ->where('component', $komponen_name)
                                                                                    ->where('part_num', $komponen_detail->part)
                                                                                    ->where('processing_fact', $barcode_header->factory_id)
                                                                                    ->first();
                                                            
                                                            if($check_data_exists == null) {
                                                                $check_data_exists = $this->sds_connection->table('v_packing_list_new')->insert([
                                                                    'style' => $barcode_header->style,
                                                                    'set_type' => $type_set,
                                                                    'poreference' => $barcode_header->poreference,
                                                                    'article' => $barcode_header->article,
                                                                    'size' => $barcode_header->size,
                                                                    'component' => $komponen_name,
                                                                    'part_num' => $komponen_detail->part,
                                                                    'processing_fact' => $barcode_header->factory_id,
                                                                    'coll' => $barcode_header->color_name,
                                                                    'sum_qty' => $barcode_check->qty,
                                                                    'first_check' => Carbon::now(),
                                                                    'first_user' => \Auth::user()->nik,
                                                                    'qty_aloc' => 0,
                                                                    'last_check' => Carbon::now(),
                                                                    'last_user' => \Auth::user()->nik,
                                                                    'is_out' => false,
                                                                ]);
                                                            } else {
                                                                $sum_qty = $check_data_exists->sum_qty + $barcode_check->qty;
                                                                $check_data_exists = $this->sds_connection->table('v_packing_list_new')->where('id', $check_data_exists->id)->update([
                                                                    'sum_qty' => $sum_qty,
                                                                    'last_check' => Carbon::now(),
                                                                    'last_user' => \Auth::user()->nik,
                                                                ]);
                                                            }
                                                        } 
                                                    }
                                                }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];

                                                if($barcode_check->sds_barcode != null) {
                                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                    if($update_sds != null) {
                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                        } 
                                                    }
                                                }
                
                                                DB::commit();
                 
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    } else {

                        // locator secondary process

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                
                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                if($barcode_check->sds_barcode != null) {
                                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                    if($update_sds != null) {
                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                        } 
                                                    }
                                                }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];

                                                if($barcode_check->sds_barcode != null) {
                                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                    if($update_sds != null) {
                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                        } 
                                                    }
                                                }
                
                                                DB::commit();
                 
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    }
                } else {
                    return response()->json('Barcode tidak dikenali!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat input data!',422);
            }
        }
    }

    public function scanManualSet(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = 11;
            $state = 'in';
            $area_name = $request->area_name;

            if($barcode_id != null && $locator_id != null && $area_name != null) {

                // $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                // $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                // if($barcode_check != null) {

                //     $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                //     if($out_cutting_check != null){

                //         $barcode_bundle_search = DB::table('bundle_detail')->where([
                //             ['bundle_header_id', $barcode_check->bundle_header_id],
                //             ['start_no',$barcode_check->start_no],
                //             ['end_no',$barcode_check->end_no]
                //         ]);

                //         $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                //         $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                //         $barcode_hasil = $barcode_bundle_search->first();

                //         $banyak_komponen = $barcode_bundle_search->count();

                //         $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                //         $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                //         $bundle_terima = $movement_check->count();
                //         $bundle_check  = $movement_check->first();

                //         $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                //         $komponen_name = $barcode_check->komponen_name;

                //         if($bundle_terima < 1){
                //             $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                //             $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                //             $style_detail_id = $barcode_check->style_detail_id;

                //             $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                //             if(strlen(trim($process_id->process))>0){
                //                 $array_process = explode(",",$process_id->process);
                //             }
                //             else{
                //                 $array_process = array();
                //             }

                //             if(count($array_process) == 0){

                //                 DistribusiMovement::FirstOrCreate([
                //                     'barcode_id'   => $barcode_id,
                //                     'locator_from' => $out_cutting_check->locator_to,
                //                     'status_from'  => $out_cutting_check->status_to,
                //                     'locator_to'   => $locator_id,
                //                     'status_to'    => $state,
                //                     'user_id'      => \Auth::user()->id,
                //                     'description'  => $state.' '.$locator->locator_name,
                //                     'ip_address'   => $this->getIPClient(),
                //                     'deleted_at'   => null,
                //                     'loc_dist'     => $area_name
                //                 ]);

                //                 $response = [
                //                     'barcode_id'    => $barcode_id,
                //                     'style'         => $bundle_header->style,
                //                     'article'       => $bundle_header->article,
                //                     'po_buyer'      => $bundle_header->poreference,
                //                     'part'          => $komponen_detail->part,
                //                     'komponen_name' => $komponen_name,
                //                     'size'          => $bundle_header->size,
                //                     'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                //                     'qty'           => $barcode_check->qty,
                //                     'cut'           => $bundle_header->cut_num,
                //                     'process'       => $locator->locator_name,
                //                     'status'        => $state,
                //                     'loc_dist'      => $area_name
                //                 ];
                //                 return response()->json($response,200);
                //             } else {
                //                 $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                //                 $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                //                 $banyak_process = count($get_process_locator) + 1;

                //                 array_push($array_locator,10);

                //                 $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                //                 $count_process_check = $process_check->count();

                //                 if($count_process_check == $banyak_process){
                //                     $barcode_bundle_search = DB::table('bundle_detail')->where([
                //                         ['bundle_header_id', $barcode_check->bundle_header_id],
                //                         ['start_no',$barcode_check->start_no],
                //                         ['end_no',$barcode_check->end_no]
                //                     ]);

                //                     $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                //                     $barcode_hasil = $barcode_bundle_search->first();

                //                     $banyak_komponen = $barcode_bundle_search->count();

                //                     $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                //                     $komponen_name = $barcode_check->komponen_name;

                //                     $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                //                     if($check_movement > 0) {
                //                         return response()->json('Barcode sudah di scan in!',422);
                //                     }

                //                     DistribusiMovement::FirstOrCreate([
                //                         'barcode_id'   => $barcode_id,
                //                         'locator_from' => $out_cutting_check->locator_to,
                //                         'status_from'  => $out_cutting_check->status_to,
                //                         'locator_to'   => $locator_id,
                //                         'status_to'    => $state,
                //                         'user_id'      => \Auth::user()->id,
                //                         'description'  => $state.' '.$locator->locator_name,
                //                         'ip_address'   => $this->getIPClient(),
                //                         'deleted_at'   => null,
                //                         'loc_dist'     => $area_name
                //                     ]);

                //                     $response = [
                //                         'barcode_id'    => $barcode_id,
                //                         'style'         => $bundle_header->style,
                //                         'article'       => $bundle_header->article,
                //                         'po_buyer'      => $bundle_header->poreference,
                //                         'part'          => $komponen_detail->part,
                //                         'komponen_name' => $komponen_name,
                //                         'size'          => $bundle_header->size,
                //                         'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                //                         'qty'           => $barcode_check->qty,
                //                         'cut'           => $bundle_header->cut_num,
                //                         'process'       => $locator->locator_name,
                //                         'status'        => $state,
                //                         'loc_dist'      => $area_name
                //                     ];
                //                     return response()->json($response,200);
                //                 } else {

                //                     $get_process_check = $process_check->pluck('locator_to')->toArray();

                //                     $array_belum_scan = array_diff($array_locator,$get_process_check);

                //                     $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                //                     $info_belum_scan = '';
                //                     foreach ($locator_belum_scan as $key => $value) {
                //                         $info_belum_scan .= ''.$value->locator_name.', ';
                //                     }

                //                     $info = trim($info_belum_scan,", ");

                //                     return response()->json('Barcode belum discan proses '.$info.'!',422);
                //                 }
                //             }
                //         } else {

                //             $style_detail_id = $barcode_check->style_detail_id;

                //             $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                //             if($process_id->process == null || $process_id->process == '' ){
                //                 $array_process = array();
                //             } else {
                //                 $array_process = explode(",",$process_id->process);
                //             }

                //             if(count($array_process) < 1){

                //                 $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                //                 if($check_movement > 0) {
                //                     return response()->json('Barcode sudah di scan in!',422);
                //                 }
                //                 // dd(!$bundle_check);
                //                 if(!$bundle_check){
                //                     DistribusiMovement::FirstOrCreate([
                //                         'barcode_id'   => $barcode_id,
                //                         'locator_from' => $out_cutting_check->locator_to,
                //                         'status_from'  => $out_cutting_check->status_to,
                //                         'locator_to'   => $locator_id,
                //                         'status_to'    => $state,
                //                         'user_id'      => \Auth::user()->id,
                //                         'description'  => $state.' '.$locator->locator_name,
                //                         'ip_address'   => $this->getIPClient(),
                //                         'deleted_at'   => null,
                //                         'loc_dist'     => $bundle_check->loc_dist
                //                     ]);

                //                     $response = [
                //                         'barcode_id'    => $barcode_id,
                //                         'style'         => $bundle_header->style,
                //                         'article'       => $bundle_header->article,
                //                         'po_buyer'      => $bundle_header->poreference,
                //                         'part'          => $komponen_detail->part,
                //                         'komponen_name' => $komponen_name,
                //                         'size'          => $bundle_header->size,
                //                         'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                //                         'qty'           => $barcode_check->qty,
                //                         'cut'           => $bundle_header->cut_num,
                //                         'process'       => $locator->locator_name,
                //                         'status'        => $state,
                //                         'loc_dist'      => $bundle_check->loc_dist
                //                     ];
                //                     return response()->json($response,200);
                //                 } else {
                //                     if($bundle_check->locator_to == $locator_id){

                //                         $check_record = DistribusiMovement::where([
                //                             'barcode_id'   => $barcode_id,
                //                             'locator_from' => $out_cutting_check->locator_to,
                //                             'status_from'  => $out_cutting_check->status_to,
                //                             'locator_to'   => $locator_id,
                //                             'status_to'    => $state,
                //                             'description'  => $state.' '.$locator->locator_name,
                //                             'deleted_at'   => null,
                //                             'loc_dist'     => $bundle_check->loc_dist
                //                         ])->exists();

                //                         if(!$check_record){
                //                             DistribusiMovement::FirstOrCreate([
                //                                 'barcode_id'   => $barcode_id,
                //                                 'locator_from' => $out_cutting_check->locator_to,
                //                                 'status_from'  => $out_cutting_check->status_to,
                //                                 'locator_to'   => $locator_id,
                //                                 'status_to'    => $state,
                //                                 'user_id'      => \Auth::user()->id,
                //                                 'ip_address'   => $this->getIPClient(),
                //                                 'description'  => $state.' '.$locator->locator_name,
                //                                 'deleted_at'   => null,
                //                                 'loc_dist'     => $bundle_check->loc_dist
                //                             ]);

                //                             $response = [
                //                                 'barcode_id'    => $barcode_id,
                //                                 'style'         => $bundle_header->style,
                //                                 'article'       => $bundle_header->article,
                //                                 'po_buyer'      => $bundle_header->poreference,
                //                                 'part'          => $komponen_detail->part,
                //                                 'komponen_name' => $komponen_name,
                //                                 'size'          => $bundle_header->size,
                //                                 'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                //                                 'qty'           => $barcode_check->qty,
                //                                 'cut'           => $bundle_header->cut_num,
                //                                 'process'       => $locator->locator_name,
                //                                 'status'        => $state,
                //                                 'loc_dist'      => $bundle_check->loc_dist
                //                             ];

                //                             return response()->json($response,200);
                //                         }
                //                         else{
                //                             return response()->json('Barcode sudah pernah scan!',422);
                //                         }

                //                     }
                //                     else{
                //                         return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                //                     }

                //                 }
                //             } else {
                //                 $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                //                 $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                //                 $banyak_process = count($get_process_locator) + 1;

                //                 array_push($array_locator,10);

                //                 $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                //                 $count_process_check = $process_check->count();

                //                 if($count_process_check == $banyak_process){

                //                     $barcode_bundle_search = DB::table('bundle_detail')->where([
                //                         ['bundle_header_id', $barcode_check->bundle_header_id],
                //                         ['start_no',$barcode_check->start_no],
                //                         ['end_no',$barcode_check->end_no]
                //                     ]);

                //                     $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                //                     $barcode_hasil = $barcode_bundle_search->first();

                //                     $banyak_komponen = $barcode_bundle_search->count();

                //                     $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                //                     if($bundle_check->locator_to == $locator_id){

                //                         $check_record = DistribusiMovement::where([
                //                             'barcode_id'   => $barcode_id,
                //                             'locator_from' => $out_cutting_check->locator_to,
                //                             'status_from'  => $out_cutting_check->status_to,
                //                             'locator_to'   => $locator_id,
                //                             'status_to'    => $state,
                //                             'description'  => $state.' '.$locator->locator_name,
                //                             'deleted_at'   => null,
                //                             'loc_dist'     => $bundle_check->loc_dist
                //                         ])->exists();

                //                         if(!$check_record){
                //                             DistribusiMovement::FirstOrCreate([
                //                                 'barcode_id'   => $barcode_id,
                //                                 'locator_from' => $out_cutting_check->locator_to,
                //                                 'status_from'  => $out_cutting_check->status_to,
                //                                 'locator_to'   => $locator_id,
                //                                 'status_to'    => $state,
                //                                 'user_id'      => \Auth::user()->id,
                //                                 'description'  => $state.' '.$locator->locator_name,
                //                                 'ip_address'   => $this->getIPClient(),
                //                                 'deleted_at'   => null,
                //                                 'loc_dist'     => $bundle_check->loc_dist
                //                             ]);

                //                             $response = [
                //                                 'barcode_id'    => $barcode_id,
                //                                 'style'         => $bundle_header->style,
                //                                 'article'       => $bundle_header->article,
                //                                 'po_buyer'      => $bundle_header->poreference,
                //                                 'part'          => $komponen_detail->part,
                //                                 'komponen_name' => $komponen_name,
                //                                 'size'          => $bundle_header->size,
                //                                 'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                //                                 'qty'           => $barcode_check->qty,
                //                                 'cut'           => $bundle_header->cut_num,
                //                                 'process'       => $locator->locator_name,
                //                                 'status'        => $state,
                //                                 'loc_dist'      => $bundle_check->loc_dist
                //                             ];

                //                             return response()->json($response,200);
                //                         }
                //                         else{
                //                             return response()->json('Barcode sudah pernah scan!',422);
                //                         }

                //                     }
                //                     else{
                //                         return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                //                     }
                //                 }
                //                 else{

                //                     $get_process_check = $process_check->pluck('locator_to')->toArray();

                //                     $array_belum_scan = array_diff($array_locator,$get_process_check);

                //                     $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                //                     $info_belum_scan = '';
                //                     foreach ($locator_belum_scan as $key => $value) {
                //                         $info_belum_scan .= ''.$value->locator_name.', ';
                //                     }

                //                     $info = trim($info_belum_scan,", ");

                //                     return response()->json('Barcode belum discan proses '.$info.'!',422);
                //                 }
                //             }
                //         }

                //         $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                //     }
                //     else{
                //         return response()->json('Barcode belum out cutting!',422);
                //     }
                // } else {
                //     return response()->json('Barcode tidak dikenali!',422);
                // }

                // SINKRON SDS ///////////////////////////////////////////////////////////////////////////////////////////////////

                $user = \Auth::user()->nik;
                $alert=NULL;
                $dstat=1;

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                $bundle_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                
                $not_allow = $this->sds_connection
                    ->table('temp_scan_proc')
                    ->where('ap_id', $bundle_check->sds_barcode)
                    ->where('proc', 'SET')
                    ->where('check_out', false)
                    ->whereNull('set_notes')
                    ->whereNull('ppcm_id')
                    ->get();

                $area_name = substr($area_name, 4);
                $area_check = substr($area_name, 0, 1);
                if($area_check == '0') {
                    $area_name = 'L.'.substr($area_name, 1);
                } else {
                    $area_name = 'L.'.$area_name;
                }

                if(count($not_allow) > 0){
                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $bundle_check->sds_barcode)->update([
                        'location' => $area_name,
                        'factory'=> 'AOI '.\Auth::user()->factory_id,
                        'setting'=> Carbon::now(),
                        'setting_pic'=> $user,
                        'supply_unlock'=> false
                    ]);
                } else {
                    $alert="terjadi kesalahan, silakan scan ulang!";
                    $dstat=0;
                }

                if($dstat > 0) {
                    $this->sds_connection->table('system_log')->insert([
                        'user_nik' => $user,
                        'user_proc' => 'SET',
                        'log_operation' => 'set_checkin',
                        'log_text' =>  '1 items is checked in.',
                    ]);
                    
                    $this->sds_connection
                        ->table('temp_scan_proc')
                        ->where('ap_id',$bundle_check->sds_barcode)
                        ->where('proc','SET')
                        ->where('check_out',false)
                        ->delete();

                    $bundle_header = DB::table('bundle_header')->where('id', $bundle_check->bundle_header_id)->first();
                    $komponen_detail = DB::table('master_style_detail')->where('id', $bundle_check->style_detail_id)->where('deleted_at', null)->first();
                    $komponen_name = $bundle_check->komponen_name;

                    $last_movement = DistribusiMovement::where('barcode_id', $barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();

                    DistribusiMovement::FirstOrCreate([
                        'barcode_id'   => $barcode_id,
                        'locator_from' => $last_movement->locator_to,
                        'status_from'  => $last_movement->status_to,
                        'locator_to'   => $locator_id,
                        'status_to'    => $state,
                        'user_id'      => \Auth::user()->id,
                        'description'  => $state.' '.$locator->locator_name,
                        'ip_address'   => $this->getIPClient(),
                        'deleted_at'   => null,
                        'loc_dist'     => $area_name
                    ]);

                    $response = [
                        'barcode_id'    => $barcode_id,
                        'style'         => $bundle_header->style,
                        'article'       => $bundle_header->article,
                        'po_buyer'      => $bundle_header->poreference,
                        'part'          => $komponen_detail->part,
                        'komponen_name' => $komponen_name,
                        'size'          => $bundle_header->size,
                        'sticker_no'    => $bundle_check->start_no.' - '.$bundle_check->end_no,
                        'qty'           => $bundle_check->qty,
                        'cut'           => $bundle_header->cut_num,
                        'process'       => $locator->locator_name,
                        'status'        => $state,
                        'loc_dist'      => $area_name
                    ];

                    return response()->json($response,200);
                } else {
                    return response()->json($alert,422);
                }
            }
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }

    private function convertLocator($locator, $state)
    {
        // id base database master_locator
        switch ($locator) {
            case 10:
                $locator_sds = 'BD';
                $column_update = 'distrib_received';
                $column_update_pic = 'bd_pic';
                break;
            case 6:
                $locator_sds = 'PPA';
                if($state == 'in') {
                    $column_update = 'ppa';
                    $column_update_pic = 'ppa_pic';
                } else {
                    $column_update = 'ppa_out';
                    $column_update_pic = 'ppa_out_pic';
                }
                break;
            case 2:
                $locator_sds = 'HE';
                if($state == 'in') {
                    $column_update = 'he';
                    $column_update_pic = 'he_pic';
                } else {
                    $column_update = 'he_out';
                    $column_update_pic = 'he_out_pic';
                }
                break;
            case 4:
                $locator_sds = 'PAD';
                if($state == 'in') {
                    $column_update = 'pad';
                    $column_update_pic = 'pad_pic';
                } else {
                    $column_update = 'pad_out';
                    $column_update_pic = 'pad_out_pic';
                }
                break;
            case 5:
                $locator_sds = 'WP';
                if($state == 'in') {
                    $column_update = 'bobok';
                    $column_update_pic = 'bobok_pic';
                } else {
                    $column_update = 'bobok_out';
                    $column_update_pic = 'bobok_out_pic';
                }
                break;
            case 1:
                $locator_sds = 'FUSE';
                if($state == 'in') {
                    $column_update = 'fuse';
                    $column_update_pic = 'fuse_pic';
                } else {
                    $column_update = 'fuse_out';
                    $column_update_pic = 'fuse_out_pic';
                }
                break;
            case 3:
                $locator_sds = 'ART';
                if($state == 'in') {
                    $column_update = 'artwork';
                    $column_update_pic = 'artwork_pic';
                } else {
                    $column_update = 'artwork_out';
                    $column_update_pic = 'artwork_out_pic';
                }
                break;
            default:
                $locator_sds = null;
                $column_update = null;
                $column_update_pic = null;
        }

        if(\Auth::user()->factory_id == '1') {
            $factory_sds = 'AOI 1';
        } else {
            $factory_sds = 'AOI 2';
        }

        $convert_locator = [
            'locator_sds' => $locator_sds,
            'column_update' => $column_update,
            'column_update_pic' => $column_update_pic,
            'factory_sds' => $factory_sds,
        ];

        return $convert_locator;
    }

    public function bundleCheck()
    {
        return view('component_movement.bundle_check.index');
    }

    public function bundleInfo(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_input = $request->barcode_input;
            $data = DB::table('bundle_detail')->selectRaw('bundle_detail.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, bundle_header.style, bundle_header.season, bundle_header.poreference, bundle_header.article, bundle_header.size, bundle_header.cut_num, bundle_detail.start_no, bundle_detail.end_no, bundle_detail.qty')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'bundle_detail.style_detail_id')->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')->where('bundle_detail.barcode_id', $barcode_input)->first();

            if($data != null) {

                if($data->type_name == 'Non') {
                    $style = $data->style;
                } else {
                    $style = $data->style.$data->type_name;
                }

                if($data->process == null || $data->process == '' || $data->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $data->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();
                    $process = implode(', ', $process_get);
                }

                $cutting_out = DB::table('distribusi_movements')->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')->where('distribusi_movements.barcode_id', $barcode_input)->where('distribusi_movements.locator_to', 10)->where('distribusi_movements.status_to', 'out')->whereNull('distribusi_movements.deleted_at')->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }

                $obj = new StdClass();
                $obj->komponen = $data->part.' - '.$data->komponen_name;
                $obj->style = $style.' ('.$data->season.')';
                $obj->po_buyer = $data->poreference;
                $obj->article = $data->article;
                $obj->size = $data->size;
                $obj->cut_sticker = $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                $obj->qty = $data->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                
                return response()->json($obj,200);
            } else {
                return response()->json('bundle tidak ditemukan',422);
            }
        }
    }

    public function deleteScanProcSet(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            if($barcode_id != null) {
                $data_cdms = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $this->sds_connection
                    ->table('temp_scan_proc')
                    ->where('ap_id',$data_cdms->sds_barcode)
                    ->where('proc','SET')
                    ->where('check_out',false)
                    ->delete();
                return response()->json(200);
            } else {
                return response()->json('terjadi kesalahan saat process data!',422);
            }
        }
    }
}
