<!-- MODAL ADD -->
<div id="modal_detail_" class="modal fade" role="dialog" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
		<div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Detail Komponen</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="list" class="col-lg-12">
                            <form class="form-horizontal" action="{{ route('masterparam.addKomponenProcess') }}" id="form-add" enctype="multipart/form-data">
                            @csrf
                            <div class="row loader-area">
                                <input type="text" class="form-control hidden" id="id_style_detail" name="id_style_detail" value="">
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <label for="komponen_name">Komponen:</label>
                                        <input type="text" id="komponen_name" name="komponen_name" class="form-control" value="">
                                    </div>
                                    <div class="col-sm-5">
                                        <label for="process_id">Process:</label>
                                        <select data-placeholder="select a state.." class="form-control select2 select-process" name="process_id" id="process_id" required>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2 pull-right">
                                        <label for="">Action</label>
                                        <div class="input-group-btn"><br>
                                            <button type="submit" class="btn btn-primary" id="btnAdd">Add & Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <span id="spStyle"></span>
                            <hr>
                            <div class="table-responsive">
                                <table class="table datatable-save-state" id="table-list-komponen-process">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Style</th>
                                            <th>Komponen</th>
                                            <th>Process</th>
                                            <th width="10px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-number="1">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL ADD -->

<!-- MODAL ADD -->
<div id="modal_detail_process_" class="modal fade" role="dialog" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
		<div class="modal-content modal-md">
            <div class="modal-header">
                {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Detail Komponen Process</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="list_process" class="col-lg-12">
                            <form class="form-horizontal" action="{{ route('masterparam.addKomponenProcessDetail') }}" id="form-adds" enctype="multipart/form-data">
                                @csrf
                                <div class="row loader-area">
                                    <input type="text" class="form-control hidden" id="id_style_transactions" name="id_style_transactions" value="">
                                    <input type="text" class="form-control hidden" id="process_id_" name="process_id_" value="">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label for="komponen_name_">Komponen:</label>
                                            <input type="text" id="komponen_name_" name="komponen_name_" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="process_name_">Process:</label>
                                            <input type="text" id="process_name_" name="process_name_" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-4" id="dvRemarkRnd">
                                            <label for="remark_rnd">Remark RnD:</label>
                                            <select class="form-control select2 select-remark-rnd" name="remark_rnd" id="remark_rnd">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4 hidden">
                                            <label for="column_name">Column Report RnD:</label>
                                            <select class="form-control select2 select-column-name" name="id_column_report" id="id_column_report">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label for="id_detail_process">Detail Process:</label>
                                            <select class="form-control select2 select-process-detail" name="id_detail_process" id="id_detail_process" required>
                                                <option value=""></option>
                                            </select>
                                            <input type="hidden" name="is_artwork" id="is_artwork" class="form-control">
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="gsd_name">GSD:</label>
                                            <textarea class="form-control" name="gsd_name" id="gsd_name" cols="30" rows="3" readonly></textarea>
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="lc" id="lbl_lc">LC:</label>
                                            <input type="text" id="lc" name="lc" class="form-control number text-right" autocomplete="off">
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="gasa" id="lbl_gasa">Gasa:</label>
                                            <input type="text" id="gasa" name="gasa" class="form-control number text-right" autocomplete="off">
                                        </div>
                                        <div class="col-sm-1">
                                            <label for="total" id="lbl_total">Total:</label>
                                            <input type="text" id="total" name="total" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2 pull-right">
                                            <label for=""></label>
                                            <div class="input-group-btn"><br>
                                                <button type="submit" class="btn btn-primary" id="btnAdds">Add & Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                Style: <span id="spStyle_"></span>
                                <hr>
                                <div class="table-responsive">
                                    <table class="table datatable-save-state" id="table-list-komponen-process-detail_">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                {{-- <th>Style</th> --}}
                                                <th>Komponen</th>
                                                <th>Process</th>
                                                <th>Detail Process</th>
                                                <th>LC</th>
                                                <th>Gasa</th>
                                                <th>Total</th>
                                                <th>Remark RnD</th>
                                                <th width="10px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-number="2">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL ADD -->