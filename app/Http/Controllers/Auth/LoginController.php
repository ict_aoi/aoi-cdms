<?php namespace App\Http\Controllers\Auth;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';

    protected $username;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('login');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'nik';
        request()->merge([$fieldType => $login]);
        return $fieldType;
    }

    public function username()
    {
        return $this->username;
    }

    public function login(Request $request){
    	$this->validate($request, [
            'login' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->credentials($request);

        if(isset($credentials['nik'])){
            $count = User::where('nik',$credentials['nik'])->where('status', 'internal')->where('last_login_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->count();
        }else if(isset($credentials['email'])){
            $count = User::where('email',$credentials['email'])->where('status', 'internal')->where('last_login_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->count();
        }

        if($count == 1){
            if (Auth::validate($credentials)) {
                $user = Auth::getLastAttempted();
                Auth::login($user, $request->has('remember'));
                $role = Auth::user()->roles->pluck('name')->toArray();
                // dd($role);
                // dd((in_array("operator-scan", $role)));
                if (in_array("operator-scan", $role))
                {
                    // echo "Match found";
                    return redirect('/scan-distribusi');
                }
                else
                {
                    // echo "Match not found";
                    return redirect()->intended('/home');
                }

            }

            if(isset($credentials['nik'])){
                return redirect('/login')
                ->withInput($request->only($credentials['nik'], 'remember'))
                ->withErrors([
                    'nik' => 'NIK atau Password anda salah',
                ]);
            }else if(isset($credentials['email'])){
                return redirect('/login')
                ->withInput($request->only($credentials['email'], 'remember'))
                ->withErrors([
                    'email' => 'Email atau Password anda salah',
                ]);
            }

        }elseif($count == 0){
            Session::flash("flash_notification", ["message"=> "NIK anda belum terdaftar atau sudah expired, silahkan hubungi ICT"]);
            return  redirect()
            ->back()
            ->withInput($request->only($this->username(), 'remember'));
        }else{
            Session::flash("flash_notification", ["message"=> "NIK anda punya 2 user, silahkan login menggunakan email"]);
            return  redirect()
            ->back()
            ->withInput($request->only($this->username(), 'remember'));
        }
    }
}
