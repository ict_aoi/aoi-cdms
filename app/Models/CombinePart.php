<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class CombinePart extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'combine_part';
	protected $fillable = ['plan_id', 'deleted_at', 'combine_id'];
}
