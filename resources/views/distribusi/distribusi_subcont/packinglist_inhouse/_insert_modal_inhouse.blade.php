<div id="insertPackinglistModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Create New Packinglist Inhouse</span>
                </legend>
			</div>
            <form method="POST" action="{{ route('packinglistDistribusi.storeInhouse') }}" accept-charset="UTF-8" id="form_insert">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">PO Buyer</label>
                                <div class="col-lg-12">
                                    <select multiple data-placeholder="Select PO Buyer..." class="form-control select-search" name="po_buyer_insert[]" id="po_buyer_insert" required>
                                        <option value=""></option>
                                        @foreach($po_buyers as $po_buyer)
                                            <option value="{{ $po_buyer->po_buyer }}">{{ $po_buyer->po_buyer }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Subcont Tujuan</label>
                                <div class="col-lg-12">
                                    <select data-placeholder="Select a subcont..." class="form-control select-search" name="subcont_insert" id="subcont_insert" required>
                                        <option value=""></option>
                                            @foreach($subconts as $subcont)
                                                <option value="{{ $subcont->id }}">{{ $subcont->initials.'-'.$subcont->division }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Packinglist No</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="no_packinglist_insert" id="no_packinglist_insert" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Kontrak Kerja No</label>
                                <div class="col-lg-12">
                                    <select data-placeholder="Select KK No..." class="form-control select-search" name="no_kk_insert" id="no_kk_insert" required>
                                        <option value=""></option>
                                    </select>
                                    <input type="hidden" name="url_get_kk" id="url_get_kk" value ="{{ route('packinglistDistribusi.getKk') }}" class="form-control" required="" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Locator</label>
                                <div class="col-lg-12">
                                    <select data-placeholder ="Select a locator..." class="form-control select-search" name="locator_insert" id="locator_insert" required>
                                        <option value=""></option>
                                        @foreach($locators as $locator)
                                            <option value="{{ $locator->id }}">{{ $locator->locator_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Remark</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="remark_insert" id="remark_insert">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Style</label>
                                <div class="col-lg-12">
                                    <select data-placeholder="Select a Style" class="form-control select-search" name="style" id="style" required>
                                        <option value=""></option>
                                        @foreach($style as $style)
                                            {{$style_set = $style->set_type != 'Non' ? $style->style.'-'.$style->set_type : $style->style}}
                                            <option value="{{ $style_set }}">{{ $style_set }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-insert-packinglist">Submit</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
            </form>
		</div>
	</div>
</div>