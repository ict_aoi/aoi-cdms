<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Identitas Marker</title>
    <style>
        html, body, body div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, figure, footer, header, hgroup, menu, nav, section, time, mark, audio, video {
            margin: 0;
            padding: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            background: transparent;
        }

        .column {
            float: left;
            width: 50%;
            padding: 10px;
        }

        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        table {
            width: 530px;
            table-layout:fixed;
        }

        .container {
            display: table;
            width: 100%;
        }

        .left-half {
            position: absolute;
            left: 0px;
            margin-top: 20px;
            margin-left: 20px;
            width: 50%;
        }

        .right-half {
            position: absolute;
            right: -20px;
            width: 50%;
        }

        .page-break {
            page-break-after: always;
        }

        table {
            border-collapse: collapse;
        }

        .wrap-text {
            white-space: nowrap;
            overflow: hidden;
        }

        .bold {
            font-weight: bold;
        }

        .fsize-8 {
            font-size: 8px;
        }

        .fsize-10 {
            font-size: 10px;
        }

        .fsize-12 {
            font-size: 12px;
        }

        .fsize-14 {
            font-size: 14px;
        }

        .fsize-20 {
            font-size: 17px;
        }

        .fsize-25 {
            /* font-size: 25px; */
            font-size: 20px;
        }

        .fsize-30 {
            font-size: 30px;
        }

        .h15 {
            height: 15px;
        }

        .h18 {
            height: 18px;
        }

        .h20 {
            height: 20px;
        }

        .h23 {
            height: 23px;
        }

        .h25 {
            height: 25px;
        }

        .h30 {
            height: 30px;
        }

        .h40 {
            height: 40px;
        }

        .h50 {
            height: 50px;
        }

        .h80 {
            height: 80px;
        }

        .h120 {
            height: 120px;
        }

        .h200 {
            height: 200px;
        }

        .talign-left {
            text-align: left;
        }

        .talign-center {
            text-align: center;
        }

        .talign-right {
            text-align: right;
        }

        .valign-center {
            vertical-align: middle;
        }

        .valign-top {
            vertical-align: top;
        }

        .valign-bottom {
            vertical-align: bottom;
        }

        .pt-5 {
            padding-top: 5px;
        }

        .pb-5 {
            padding-bottom: 5px;
        }

        .pt-20 {
            padding-top: 20px;
        }

        .pl-3 {
            padding-left: 3px;
        }

        .pl-20 {
            padding-left: 20px;
        }

        .pb-20 {
            padding-bottom: 20px;
        }

        .pt-30 {
            padding-top: 30px;
        }

        .pl-30 {
            padding-left: 30px;
        }

        .barcode {
            /* width: 250px; */
            /* height: 50px; */
        }

        .logo {
            width: 120px;
            margin-bottom: 5px;
        }

    </style>
</head>
<body>
    @php
        if($print_type == 'single') {
            $count_data = count($data_marker_all);
        } else {
            $count_data = count($barcode_ids);
        }
        $inc = 1;
        $dbl = 1;
        $pss = '';
    @endphp

    @if($print_type == 'single')
        @foreach($data_marker_all as $dt)

            @if($inc % 4 == 1)
                <section class="container">
            @endif

            @php
                if($dbl % 2 == 1) {
                    $pss = 'left';
                } else {
                    $pss = 'right';
                }
            @endphp

            @if($inc % 2 == 1)
                <div class="{{ $pss }}-half">
            @endif
                    @if(count($dt->rasio_markers) <= 11)
                        <div class="pt-10 pl-10 pb-20">
                            <table border="1" class="pt-5">
                                <tr class="talign-center">
                                    <td colspan="4" class="h15 valign-center">Laporan Spreading</td>
                                    <td rowspan="2" colspan="5" class="valign-center talign-center"><img class="barcode" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($dt->barcode_id, 'C128', 1, 50) }}" alt="barcode" /><p>{{ $dt->barcode_id }}</p></td>
                                    <td rowspan="2" colspan="3" class="valign-center talign-center"><img src="" alt=""></td>
                                    <td rowspan="2" colspan="3" class="valign-center talign-center"><img src="https://chart.googleapis.com/chart?chs=100x74&cht=qr&chl={{ route('QR.scan', $dt->barcode_id) }}" alt=""></td>
                                </tr>
                                <tr class="talign-center">
                                    <td colspan="4" class="valign-center">
                                        <p class="pt-5 pb-5 fsize-10">PT APPAREL ONE INDONESIA</p>
                                        <!-- <img src="{{ public_path('images/general/logo_aoi.png') }}" alt="logo" class="logo" /> -->
                                        <img src="{{ url('images/general/logo_aoi.png') }}" alt="logo" class="logo" />
                                    </td>
                                </tr>

                                @php

                                    $get_total_marker = App\Models\CuttingMarker::where('id_plan', $dt->id_plan)->where('part_no', $dt->part_no)->where('is_new', null)->where('deleted_at', null)->orderBy('cut', 'asc')->get();

                                    $total_marker = count($get_total_marker);
                                    $marker_ke = 0;
                                    $no_urut = 0;
                                    foreach($get_total_marker as $get_data_) {
                                        $no_urut++;
                                        if($get_data_->barcode_id == $dt->barcode_id) {
                                            // $marker_ke = $no_urut;
                                            $marker_ke = $get_data_->suggest_cut;
                                            break;
                                        }
                                    }
                                    $show_marker_per = $marker_ke.' / '.$total_marker;

                                    $queue_list = array();

                                    if($dt->combine_id == null) {
                                        $queue_list[] =  $dt->cutting_plan->queu;
                                        $get_plan_id[] = $dt->id_plan;
                                    } else {
                                        $get_plan_id = App\Models\CombinePart::where('combine_id', $dt->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();

                                        foreach($get_plan_id as $plan_id) {
                                            $queue_list[] = App\Models\CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first()->queu;
                                        }
                                    }

                                    $po = '';

                                    // $get_list_po = App\Models\MarkerDetailPO::select('po_buyer')->where('barcode_id', $dt->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                                    $get_list_po = App\Models\MarkerDetailPO::select('ratio_detail.po_buyer', 'statistical_date')
                                    ->leftJoin('detail_cutting_plan', 'detail_cutting_plan.po_buyer', '=', 'ratio_detail.po_buyer')
                                    ->where('barcode_id', $dt->barcode_id)->groupBy('ratio_detail.po_buyer', 'statistical_date')->orderBy('statistical_date')
                                    ->pluck('ratio_detail.po_buyer')->toArray();

                                    $part_no = explode('+', $dt->part_no);

                                    $data_plan = App\Models\CuttingPlan::orderBy('queu', 'asc')
                                                ->whereIn('id', $get_plan_id)
                                                ->where('is_header', true)
                                                ->where('deleted_at', null)
                                                ->get();

                                    $plan_list = array();
                                    foreach($data_plan as $dp){
                                        if($dp->header_id != null && $dp->is_header) {
                                            $get_plan_id = App\Models\CuttingPlan::where('header_id', $dp->id)
                                                ->whereNull('deleted_at')
                                                ->pluck('id')
                                                ->toArray();
                                            $plan_list = array_merge($plan_list, $get_plan_id);
                                        } else {
                                            $plan_list[] = $dp->id;
                                        }
                                    }

                                    $get_data_po_detail = \DB::table('jaz_detail_size_per_part_2')
                                    ->whereIn('plan_id', $plan_list)
                                    // ->whereIn('queu', $queue_list)
                                    ->whereIn('po_buyer', $get_list_po)
                                    ->whereIn('part_no', $part_no)
                                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                    $po = '';

                                    if(count($get_list_po) > 0) {
                                        for($ii = 0; $ii < count($get_list_po); $ii++) {
                                            $get_dest = \DB::table('jaz_detail_size_per_part_2')
                                            // ->where('cutting_date', $dt->cutting_plan->cutting_date)
                                            // ->whereIn('queu', $queue_list)
                                            ->whereIn('plan_id', $plan_list)
                                            ->where('po_buyer', $get_list_po[$ii])
                                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                            $po .= $get_list_po[$ii].' ('.$get_dest->first()->destination.'), ';
                                        }
                                    } else {
                                        $po = 'Tidak ada PO Buyer';
                                    }

                                    if(strlen($po) > 200) {
                                        $po = substr($po, 0, 200);
                                    } else {
                                        $po = $po;
                                    }

                                    if($get_data_po_detail->first() == null) {
                                        dd($dt->barcode_id, $dt->cutting_plan->cutting_date,$dt->cutting_plan->queu,$get_list_po,$dt->part_no);
                                    }

                                    if(strlen($get_data_po_detail->first()->color_name) > 15) {
                                        $color_name = substr($get_data_po_detail->first()->color_name, 0, 15);
                                    } else {
                                        $color_name = $get_data_po_detail->first()->color_name;
                                    }

                                    if(strlen($get_data_po_detail->first()->item_code) > 10) {
                                        $item_code = substr($get_data_po_detail->first()->item_code, 0, 10);
                                    } else {
                                        $item_code = $get_data_po_detail->first()->item_code;
                                    }
                                @endphp

                                <tr>
                                    <td class="fsize-12 valign-center pl-3 h25">STYLE</td>
                                    <td colspan="5" class="bold talign-center valign-center fsize-25">{{ $dt->cutting_plan->style }}</td>
                                    <td class="fsize-12 valign-center pl-3">CUT</td>
                                    <td class="bold talign-center valign-center fsize-25">{{ $dt->cut }}</td>
                                    <td class="fsize-12 valign-center pl-3">ITEM</td>
                                    <td colspan="3" class="bold valign-center talign-center fsize-20">{{ $item_code }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Stats Date</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ \Carbon\Carbon::parse($get_data_po_detail->first()->deliv_date)->format('d-m') }}</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3">Marker Width</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->fabric_width - 0.5 }}</td>
                                    <td rowspan="2" class="fsize-10 valign-center pl-3">COLOR</td>
                                    @if(strlen($get_data_po_detail->first()->color_code) > 20)
                                    <td colspan="3" class="bold valign-center talign-center" style="font-size:10px">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @else
                                    <td colspan="3" class="fsize-12 bold valign-center talign-center">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Article</td>
                                    @if(strlen($dt->cutting_plan->articleno) > 15)
                                    <td colspan="2" class="bold valign-center pl-3 talign-center" style="font-size:10px">{{ $dt->cutting_plan->articleno }}</td>
                                    @else
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->cutting_plan->articleno }}</td>
                                    @endif
                                    <td colspan="2" class="fsize-10 valign-center pl-3">Marker Length</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->marker_length }}</td>
                                    <td colspan="3" class="fsize-12 bold valign-center talign-center">{{ $color_name }}</td>
                                </tr>
                                @php
                                    $marker_pcs = 0;
                                    foreach($dt->rasio_markers as $a) {
                                        $marker_pcs += $a->ratio;
                                    }

                                    $total_garment = $marker_pcs * $dt->layer;
                                @endphp
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Marker Pcs</td>
                                    <td colspan="2" class="bold valign-center talign-center">{{ $marker_pcs }}</td>
                                    <td colspan="2" class="fsize-8 valign-center pl-3">Total Length (Allowence)</td>
                                    <td colspan="2" class="talign-center">{{ $dt->marker_length + 0.75 }}</td>
                                    <td rowspan="2" class="fsize-10 valign-center pl-3">PART</td>
                                    <td rowspan="2" class="fsize-12 bold valign-center talign-center">{{ str_replace('+', ', ', $dt->part_no) }}</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">Plan</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">{{ \Carbon\Carbon::parse($dt->cutting_plan->cutting_date)->format('d-m-y') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Total Garment</td>
                                    <td colspan="2" class="bold valign-center pl-3 talign-center">{{ $total_garment }}</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Layer</td>
                                    <td colspan="2" class="bold valign-center talign-center">{{ $dt->layer }}</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">Queue</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">{{ $dt->cutting_plan->queu }}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="fsize-10 valign-center pl-3 h23">{{ $show_marker_per }}</td>
                                </tr>
                                <tr>
                                    <td class="fsize-10 valign-center pl-3 h50">PO</td>
                                    <td colspan="11" class="fsize-14 pl-3" style="width:100px; word-wrap:break-word;">{{ $po }}</td>
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h25">Size</td>
                                    @for($i = 0; $i < 11; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center">{{ $dt->rasio_markers[$i]->size }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h30">Ratio</td>
                                    @for($i = 0; $i < 11; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center">{{ $dt->rasio_markers[$i]->ratio }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                            </table>
                        </div>
                    @else
                        <div class="pt-10 pl-10 pb-20">
                            <table border="1" class="pt-5">
                                <tr class="talign-center">
                                    <td colspan="7" class="h15 valign-center">Laporan Spreading</td>
                                    <td rowspan="2" colspan="7" class="valign-center talign-center"><img class="barcode" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($dt->barcode_id, 'C128', 1, 50) }}" alt="barcode" /><p>{{ $dt->barcode_id }}</p></td>
                                    <td rowspan="2" colspan="5" class="valign-center talign-center"><img src="https://chart.googleapis.com/chart?chs=110x110&cht=qr&chl={{ route('QR.scan', $dt->barcode_id) }}" alt=""></td>
                                </tr>
                                <tr class="talign-center">
                                    <td colspan="7" class="valign-center">
                                        <p class="pt-5 pb-5 fsize-10">PT APPAREL ONE INDONESIA</p>
                                        <img src="{{ public_path('images/general/logo_aoi.png') }}" alt="logo" class="logo" />
                                    </td>
                                </tr>

                                @php
                                    $queue_list = array();

                                    if($dt->combine_id == null) {
                                        $queue_list[] =  $dt->cutting_plan->queu;
                                        $get_plan_id[] = $dt->id_plan;
                                    } else {
                                        $get_plan_id = App\Models\CombinePart::where('combine_id', $dt->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                                        foreach($get_plan_id as $plan_id) {
                                            $queue_list[] = App\Models\CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first()->queu;
                                        }
                                    }

                                    $po = '';

                                    // $get_list_po = App\Models\MarkerDetailPO::select('po_buyer')->where('barcode_id', $dt->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                                    $get_list_po = App\Models\MarkerDetailPO::select('ratio_detail.po_buyer', 'statistical_date')
                                    ->leftJoin('detail_cutting_plan', 'detail_cutting_plan.po_buyer', '=', 'ratio_detail.po_buyer')
                                    ->where('barcode_id', $dt->barcode_id)->groupBy('ratio_detail.po_buyer', 'statistical_date')->orderBy('statistical_date')
                                    ->pluck('ratio_detail.po_buyer')->toArray();

                                    $part_no = explode('+', $dt->part_no);

                                    $data_plan = App\Models\CuttingPlan::orderBy('queu', 'asc')
                                                ->whereIn('id', $get_plan_id)
                                                ->where('is_header', true)
                                                ->where('deleted_at', null)
                                                ->get();

                                    $plan_list = array();
                                    foreach($data_plan as $dp){
                                        if($dp->header_id != null && $dp->is_header) {
                                            $get_plan_id = App\Models\CuttingPlan::where('header_id', $dp->id)
                                                ->whereNull('deleted_at')
                                                ->pluck('id')
                                                ->toArray();
                                            $plan_list = array_merge($plan_list, $get_plan_id);
                                        } else {
                                            $plan_list[] = $dp->id;
                                        }
                                    }

                                    $get_data_po_detail = \DB::table('jaz_detail_size_per_part_2')
                                    ->whereIn('plan_id', $plan_list)
                                    // ->whereIn('queu', $queue_list)
                                    ->whereIn('po_buyer', $get_list_po)
                                    ->whereIn('part_no', $part_no)
                                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));


                                    $po = '';

                                    if(count($get_list_po) > 0) {
                                        for($ii = 0; $ii < count($get_list_po); $ii++) {
                                            $get_dest = \DB::table('jaz_detail_size_per_part_2')
                                            // ->where('cutting_date', $dt->cutting_plan->cutting_date)
                                            // ->whereIn('queu', $queue_list)
                                            ->whereIn('plan_id', $plan_list)
                                            ->where('po_buyer', $get_list_po[$ii])
                                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                            $po .= $get_list_po[$ii].' ('.$get_dest->first()->destination.'), ';
                                        }
                                    } else {
                                        $po = 'Tidak ada PO Buyer';
                                    }

                                    if(strlen($po) > 200) {
                                        $po = substr($po, 0, 200);
                                    } else {
                                        $po = $po;
                                    }

                                    if($get_data_po_detail->first() == null) {
                                        dd($dt->barcode_id, $dt->cutting_plan->cutting_date,$dt->cutting_plan->queu,$get_list_po,$dt->part_no);
                                    }

                                    if(strlen($get_data_po_detail->first()->color_name) > 15) {
                                        $color_name = substr($get_data_po_detail->first()->color_name, 0, 15);
                                    } else {
                                        $color_name = $get_data_po_detail->first()->color_name;
                                    }

                                    if(strlen($get_data_po_detail->first()->item_code) > 10) {
                                        $item_code = substr($get_data_po_detail->first()->item_code, 0, 10);
                                    } else {
                                        $item_code = $get_data_po_detail->first()->item_code;
                                    }
                                @endphp

                                <tr>
                                    <td colspan="3" class="fsize-12 valign-center pl-3 h25">STYLE</td>
                                    <td colspan="7" class="bold talign-center valign-center fsize-20">{{ $dt->cutting_plan->style }}</td>
                                    <td class="fsize-12 valign-center pl-3">CUT</td>
                                    <td class="bold talign-center valign-center fsize-25">{{ $dt->cut }}</td>
                                    <td colspan="2" class="fsize-12 valign-center pl-3 talign-center">ITEM</td>
                                    <td colspan="5" class="bold valign-center talign-center fsize-20">{{ $item_code }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Stats Date</td>
                                    <td colspan="4" class="bold valign-center pl-3 fsize-12 talign-center">{{ \Carbon\Carbon::parse($get_data_po_detail->first()->deliv_date)->format('d-m') }}</td>
                                    <td colspan="3" class="fsize-10 valign-center pl-3">Marker Width</td>
                                    <td colspan="3" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->fabric_width - 0.5 }}</td>
                                    <td rowspan="2" colspan="2" class="fsize-10 valign-center pl-3 talign-center">COLOR</td>
                                    @if(strlen($get_data_po_detail->first()->color_code) > 20)
                                    <td colspan="4" class="bold valign-center talign-center" style="font-size:10px">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @else
                                    <td colspan="4" class="fsize-12 bold valign-center talign-center">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @endif
                                    
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Article</td>
                                    @if(strlen($dt->cutting_plan->articleno) > 15)
                                    <td colspan="4" class="bold valign-center pl-3 talign-center" style="font-size:10px">{{ $dt->cutting_plan->articleno }}</td>
                                    @else
                                    <td colspan="4" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->cutting_plan->articleno }}</td>
                                    @endif
                                    
                                    <td colspan="3" class="fsize-10 valign-center pl-3">Marker Length</td>
                                    <td colspan="3" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->marker_length }}</td>
                                    <td colspan="4" class="fsize-12 bold valign-center talign-center">{{ $color_name }}</td>
                                </tr>
                                @php
                                    $marker_pcs = 0;
                                    foreach($dt->rasio_markers as $a) {
                                        $marker_pcs += $a->ratio;
                                    }

                                    $total_garment = $marker_pcs * $dt->layer;
                                @endphp
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Marker Pcs</td>
                                    <td colspan="4" class="bold valign-center talign-center">{{ $marker_pcs }}</td>
                                    <td colspan="3" class="fsize-8 valign-center pl-3">Total Length (Allowence)</td>
                                    <td colspan="3" class="talign-center">{{ $dt->marker_length + 0.75 }}</td>
                                    <td rowspan="2" colspan="1" class="fsize-10 valign-center pl-3 talign-center">PART</td>
                                    <td rowspan="2" colspan="2" class="fsize-12 bold valign-center talign-center">{{ str_replace('+', ', ', $dt->part_no) }}</td>
                                    <td class="fsize-10 valign-center pl-3">Plan</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 talign-center">{{ \Carbon\Carbon::parse($dt->cutting_plan->cutting_date)->format('d-m-y') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Total Garment</td>
                                    <td colspan="4" class="bold valign-center pl-3 talign-center">{{ $total_garment }}</td>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Layer</td>
                                    <td colspan="3" class="bold valign-center talign-center">{{ $dt->layer }}</td>
                                    <td class="fsize-10 valign-center pl-3">Queue</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 talign-center">{{ $dt->cutting_plan->queu }}</td>
                                </tr>
                                <tr>
                                    <td colspan="19">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="fsize-10 valign-center pl-3 h50">PO</td>
                                    <td colspan="18" class="fsize-14 pl-3" style="width:100px; word-wrap:break-word;">{{ $po }}</td>
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h25">Size</td>
                                    @for($i = 0; $i < 18; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center fsize-12">{{ $dt->rasio_markers[$i]->size }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h30">Ratio</td>
                                    @for($i = 0; $i < 18; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center fsize-12">{{ $dt->rasio_markers[$i]->ratio }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                            </table>
                        </div>
                    @endif

            @if($inc < $count_data)
                @if($inc % 2 == 0)
                    </div>
                @endif
            @elseif($inc == $count_data)
                </div>
            @else
            @endif

            @if($inc % 4 == 0)
                </section>
            @endif

            @if($inc % 4 == 0)
                @if($inc < $count_data)
                    <div class="page-break"></div>
                @endif
            @endif

            @php
                $inc++;
                if($inc % 2 == 0) {
                    $dbl++;
                }
            @endphp

        @endforeach

    @else

        @foreach($barcode_ids as $dta)

            @if($inc % 4 == 1)
                <section class="container">
            @endif

            @php
                if($dbl % 2 == 1) {
                    $pss = 'left';
                } else {
                    $pss = 'right';
                }

                $dt = App\Models\CuttingMarker::where('barcode_id', $dta)->where('deleted_at', null)->first();
            @endphp

            @if($inc % 2 == 1)
                <div class="{{ $pss }}-half">
            @endif
                    @if(count($dt->rasio_markers) <= 11)
                        <div class="pt-10 pl-10 pb-20">
                            <table border="1" class="pt-5">
                                <tr class="talign-center">
                                    <td colspan="4" class="h15 valign-center">Laporan Spreading</td>
                                    <td rowspan="2" colspan="5" class="valign-center talign-center"><img class="barcode" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($dt->barcode_id, 'C128', 1, 50) }}" alt="barcode" /><p>{{ $dt->barcode_id }}</p></td>
                                    <td rowspan="2" colspan="3" class="valign-center talign-center"><img src="https://chart.googleapis.com/chart?chs=100x74&cht=qr&chl={{ route('QR.scan', $dt->barcode_id) }}" alt=""></td>
                                </tr>
                                <tr class="talign-center">
                                    <td colspan="4" class="valign-center">
                                        <p class="pt-5 pb-5 fsize-10">PT APPAREL ONE INDONESIA</p>
                                        <img src="{{ public_path('images/general/logo_aoi.png') }}" alt="logo" class="logo" />
                                    </td>
                                </tr>

                                @php
                                    $get_total_marker = App\Models\CuttingMarker::where('id_plan', $dt->id_plan)->where('part_no', $dt->part_no)->where('is_new', null)->where('deleted_at', null)->orderBy('cut', 'asc')->get();

                                    $total_marker = count($get_total_marker);
                                    $marker_ke = 0;
                                    $no_urut = 0;
                                    foreach($get_total_marker as $get_data_) {
                                        $no_urut++;
                                        if($get_data_->barcode_id == $dt->barcode_id) {
                                            // $marker_ke = $no_urut;
                                            $marker_ke = $get_data_->suggest_cut;
                                            break;
                                        }
                                    }
                                    $show_marker_per = $marker_ke.' / '.$total_marker;

                                    $queue_list = array();

                                    if($dt->combine_id == null) {
                                        $queue_list[] =  $dt->cutting_plan->queu;
                                        $get_plan_id[] = $dt->id_plan;
                                    } else {
                                        $get_plan_id = App\Models\CombinePart::where('combine_id', $dt->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                                        foreach($get_plan_id as $plan_id) {
                                            $queue_list[] = App\Models\CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first()->queu;
                                        }
                                    }

                                    $po = '';

                                    $get_list_po = App\Models\MarkerDetailPO::select('ratio_detail.po_buyer', 'statistical_date')
                                    ->leftJoin('detail_cutting_plan', 'detail_cutting_plan.po_buyer', '=', 'ratio_detail.po_buyer')
                                    ->where('barcode_id', $dt->barcode_id)->groupBy('ratio_detail.po_buyer', 'statistical_date')->orderBy('statistical_date')
                                    ->pluck('ratio_detail.po_buyer')->toArray();

                                    $part_no = explode('+', $dt->part_no);

                                    $data_plan = App\Models\CuttingPlan::orderBy('queu', 'asc')
                                                ->whereIn('id', $get_plan_id)
                                                ->where('is_header', true)
                                                ->where('deleted_at', null)
                                                ->get();

                                    $plan_list = array();
                                    foreach($data_plan as $dp){
                                        if($dp->header_id != null && $dp->is_header) {
                                            $get_plan_id = App\Models\CuttingPlan::where('header_id', $dp->id)
                                                ->whereNull('deleted_at')
                                                ->pluck('id')
                                                ->toArray();
                                            $plan_list = array_merge($plan_list, $get_plan_id);
                                        } else {
                                            $plan_list[] = $dp->id;
                                        }
                                    }

                                    $get_data_po_detail = \DB::table('jaz_detail_size_per_part_2')
                                    ->whereIn('plan_id', $plan_list)
                                    // ->whereIn('queu', $queue_list)
                                    ->whereIn('po_buyer', $get_list_po)
                                    ->whereIn('part_no', $part_no)
                                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                    $po = '';

                                    if(count($get_list_po) > 0) {
                                        for($ii = 0; $ii < count($get_list_po); $ii++) {
                                            $get_dest = \DB::table('jaz_detail_size_per_part_2')
                                            // ->where('cutting_date', $dt->cutting_plan->cutting_date)
                                            // ->whereIn('queu', $queue_list)
                                            ->whereIn('plan_id', $plan_list)
                                            ->where('po_buyer', $get_list_po[$ii])
                                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                            $po .= $get_list_po[$ii].' ('.$get_dest->first()->destination.'), ';
                                        }
                                    } else {
                                        $po = 'Tidak ada PO Buyer';
                                    }

                                    if(strlen($po) > 200) {
                                        $po = substr($po, 0, 200);
                                    } else {
                                        $po = $po;
                                    }

                                    if($get_data_po_detail->first() == null) {
                                        dd($dt->barcode_id, $dt->cutting_plan->cutting_date,$dt->cutting_plan->queu,$get_list_po,$dt->part_no);
                                    }

                                    if(strlen($get_data_po_detail->first()->color_name) > 15) {
                                        $color_name = substr($get_data_po_detail->first()->color_name, 0, 15);
                                    } else {
                                        $color_name = $get_data_po_detail->first()->color_name;
                                    }

                                    if(strlen($get_data_po_detail->first()->item_code) > 10) {
                                        $item_code = substr($get_data_po_detail->first()->item_code, 0, 10);
                                    } else {
                                        $item_code = $get_data_po_detail->first()->item_code;
                                    }
                                @endphp

                                <tr>
                                    <td class="fsize-12 valign-center pl-3 h25">STYLE</td>
                                    <td colspan="5" class="bold talign-center valign-center fsize-25">{{ $dt->cutting_plan->style }}</td>
                                    <td class="fsize-12 valign-center pl-3">CUT</td>
                                    <td class="bold talign-center valign-center fsize-25">{{ $dt->cut }}</td>
                                    <td class="fsize-12 valign-center pl-3">ITEM</td>
                                    <td colspan="3" class="bold valign-center talign-center fsize-20">{{ $item_code }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Stats Date</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ \Carbon\Carbon::parse($get_data_po_detail->first()->deliv_date)->format('d-m') }}</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3">Marker Width</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->fabric_width - 0.5 }}</td>
                                    <td rowspan="2" class="fsize-10 valign-center pl-3">COLOR</td>
                                    @if(strlen($get_data_po_detail->first()->color_code) > 20)
                                        <td colspan="3" class="bold valign-center talign-center" style="font-size:10px">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @else
                                        <td colspan="3" class="fsize-12 bold valign-center talign-center">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Article</td>
                                    @if(strlen($dt->cutting_plan->articleno) > 15)
                                    <td colspan="2" class="bold valign-center pl-3 talign-center" style="font-size:10px">{{ $dt->cutting_plan->articleno }}</td>
                                    @else
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->cutting_plan->articleno }}</td>
                                    @endif
                                    <td colspan="2" class="fsize-10 valign-center pl-3">Marker Length</td>
                                    <td colspan="2" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->marker_length }}</td>
                                    <td colspan="3" class="fsize-12 bold valign-center talign-center">{{ $color_name }}</td>
                                </tr>
                                @php
                                    $marker_pcs = 0;
                                    foreach($dt->rasio_markers as $a) {
                                        $marker_pcs += $a->ratio;
                                    }

                                    $total_garment = $marker_pcs * $dt->layer;
                                @endphp
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Marker Pcs</td>
                                    <td colspan="2" class="bold valign-center talign-center">{{ $marker_pcs }}</td>
                                    <td colspan="2" class="fsize-8 valign-center pl-3">Total Length (Allowence)</td>
                                    <td colspan="2" class="talign-center">{{ $dt->marker_length + 0.75 }}</td>
                                    <td rowspan="2" class="fsize-10 valign-center pl-3">PART</td>
                                    <td rowspan="2" class="fsize-12 bold valign-center talign-center">{{ str_replace('+', ', ', $dt->part_no) }}</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">Plan</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">{{ \Carbon\Carbon::parse($dt->cutting_plan->cutting_date)->format('d-m-y') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Total Garment</td>
                                    <td colspan="2" class="bold valign-center pl-3 talign-center">{{ $total_garment }}</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 h23">Layer</td>
                                    <td colspan="2" class="bold valign-center talign-center">{{ $dt->layer }}</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">Queue</td>
                                    <td class="fsize-10 valign-center pl-3 talign-center">{{ $dt->cutting_plan->queu }}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="fsize-10 valign-center pl-3 h23">{{ $show_marker_per }}</td>
                                </tr>
                                <tr>
                                    <td class="fsize-10 valign-center pl-3 h50">PO</td>
                                    <td colspan="11" class="fsize-14 pl-3" style="width:100px; word-wrap:break-word;">{{ $po }}</td>
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h25">Size</td>
                                    @for($i = 0; $i < 11; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center">{{ $dt->rasio_markers[$i]->size }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h30">Ratio</td>
                                    @for($i = 0; $i < 11; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center">{{ $dt->rasio_markers[$i]->ratio }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                            </table>
                        </div>
                    @else
                        <div class="pt-10 pl-10 pb-20">
                            <table border="1" class="pt-5">
                                <tr class="talign-center">
                                    <td colspan="7" class="h15 valign-center">Laporan Spreading</td>
                                    <td rowspan="2" colspan="7" class="valign-center talign-center"><img class="barcode" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($dt->barcode_id, 'C128', 1, 50) }}" alt="barcode" /><p>{{ $dt->barcode_id }}</p></td>
                                    <td rowspan="2" colspan="5" class="valign-center talign-center"><img src="https://chart.googleapis.com/chart?chs=85x85&cht=qr&chl={{ route('QR.scan', $dt->barcode_id) }}" alt=""></td>
                                </tr>
                                <tr class="talign-center">
                                    <td colspan="7" class="valign-center">
                                        <p class="pt-5 pb-5 fsize-10">PT APPAREL ONE INDONESIA</p>
                                        <img src="{{ public_path('images/general/logo_aoi.png') }}" alt="logo" class="logo" />
                                    </td>
                                </tr>

                                @php
                                    $queue_list = array();

                                    if($dt->combine_id == null) {
                                        $queue_list[] =  $dt->cutting_plan->queu;
                                        $get_plan_id[] = $dt->id_plan;
                                    } else {
                                        $get_plan_id = App\Models\CombinePart::where('combine_id', $dt->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                                        foreach($get_plan_id as $plan_id) {
                                            $queue_list[] = App\Models\CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first()->queu;
                                        }
                                    }

                                    $po = '';

                                    // $get_list_po = App\Models\MarkerDetailPO::select('po_buyer')->where('barcode_id', $dt->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();
                                    $get_list_po = App\Models\MarkerDetailPO::select('ratio_detail.po_buyer', 'statistical_date')
                                    ->leftJoin('detail_cutting_plan', 'detail_cutting_plan.po_buyer', '=', 'ratio_detail.po_buyer')
                                    ->where('barcode_id', $dt->barcode_id)->groupBy('ratio_detail.po_buyer', 'statistical_date')->orderBy('statistical_date')
                                    ->pluck('ratio_detail.po_buyer')->toArray();

                                    $part_no = explode('+', $dt->part_no);

                                    $data_plan = App\Models\CuttingPlan::orderBy('queu', 'asc')
                                                ->whereIn('id', $get_plan_id)
                                                ->where('is_header', true)
                                                ->where('deleted_at', null)
                                                ->get();

                                    $plan_list = array();
                                    foreach($data_plan as $dp){
                                        if($dp->header_id != null && $dp->is_header) {
                                            $get_plan_id = App\Models\CuttingPlan::where('header_id', $dp->id)
                                                ->whereNull('deleted_at')
                                                ->pluck('id')
                                                ->toArray();
                                            $plan_list = array_merge($plan_list, $get_plan_id);
                                        } else {
                                            $plan_list[] = $dp->id;
                                        }
                                    }

                                    $get_data_po_detail = \DB::table('jaz_detail_size_per_part_2')
                                    ->whereIn('plan_id', $plan_list)
                                    // ->whereIn('queu', $queue_list)
                                    ->whereIn('po_buyer', $get_list_po)
                                    ->whereIn('part_no', $part_no)
                                    ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                    $po = '';

                                    if(count($get_list_po) > 0) {
                                        for($ii = 0; $ii < count($get_list_po); $ii++) {
                                            $get_dest = \DB::table('jaz_detail_size_per_part_2')
                                            // ->where('cutting_date', $dt->cutting_plan->cutting_date)
                                            // ->whereIn('queu', $queue_list)
                                            ->whereIn('plan_id', $plan_list)
                                            ->where('po_buyer', $get_list_po[$ii])
                                            ->orderBy(DB::raw("case when po_buyer like '%-S%' then 1 else 2 end , deliv_date, qty"));

                                            $po .= $get_list_po[$ii].' ('.$get_dest->first()->destination.'), ';
                                        }
                                    } else {
                                        $po = 'Tidak ada PO Buyer';
                                    }

                                    if(strlen($po) > 200) {
                                        $po = substr($po, 0, 200);
                                    } else {
                                        $po = $po;
                                    }

                                    if($get_data_po_detail->first() == null) {
                                        dd($dt->barcode_id, $dt->cutting_plan->cutting_date,$dt->cutting_plan->queu,$get_list_po,$dt->part_no);
                                    }

                                    if(strlen($get_data_po_detail->first()->color_name) > 15) {
                                        $color_name = substr($get_data_po_detail->first()->color_name, 0, 15);
                                    } else {
                                        $color_name = $get_data_po_detail->first()->color_name;
                                    }

                                    if(strlen($get_data_po_detail->first()->item_code) > 10) {
                                        $item_code = substr($get_data_po_detail->first()->item_code, 0, 10);
                                    } else {
                                        $item_code = $get_data_po_detail->first()->item_code;
                                    }
                                @endphp

                                <tr>
                                    <td colspan="3" class="fsize-12 valign-center pl-3 h25">STYLE</td>
                                    <td colspan="7" class="bold talign-center valign-center fsize-20">{{ $dt->cutting_plan->style }}</td>
                                    <td class="fsize-12 valign-center pl-3">CUT</td>
                                    <td class="bold talign-center valign-center fsize-20">{{ $dt->cut }}</td>
                                    <td colspan="2" class="fsize-12 valign-center pl-3 talign-center">ITEM</td>
                                    <td colspan="5" class="bold valign-center talign-center fsize-20">{{ $item_code }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Stats Date</td>
                                    <td colspan="4" class="bold valign-center pl-3 fsize-12 talign-center">{{ \Carbon\Carbon::parse($get_data_po_detail->first()->deliv_date)->format('d-m') }}</td>
                                    <td colspan="3" class="fsize-10 valign-center pl-3">Marker Width</td>
                                    <td colspan="3" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->fabric_width - 0.5 }}</td>
                                    <td rowspan="2" colspan="2" class="fsize-10 valign-center pl-3 talign-center">COLOR</td>
                                    @if(strlen($get_data_po_detail->first()->color_code) > 20)
                                    <td colspan="4" class="bold valign-center talign-center" style="font-size:10px">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @else
                                    <td colspan="4" class="fsize-12 bold valign-center talign-center">{{ $get_data_po_detail->first()->color_code }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Article</td>
                                    @if(strlen($dt->cutting_plan->articleno) > 15)
                                    <td colspan="4" class="bold valign-center pl-3 talign-center" style="font-size:10px">{{ $dt->cutting_plan->articleno }}</td>
                                    @else
                                    <td colspan="4" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->cutting_plan->articleno }}</td>
                                    @endif
                                    
                                    <td colspan="3" class="fsize-10 valign-center pl-3">Marker Length</td>
                                    <td colspan="3" class="bold valign-center pl-3 fsize-12 talign-center">{{ $dt->marker_length }}</td>
                                    <td colspan="4" class="fsize-12 bold valign-center talign-center">{{ $color_name }}</td>
                                </tr>
                                @php
                                    $marker_pcs = 0;
                                    foreach($dt->rasio_markers as $a) {
                                        $marker_pcs += $a->ratio;
                                    }

                                    $total_garment = $marker_pcs * $dt->layer;
                                @endphp
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Marker Pcs</td>
                                    <td colspan="4" class="bold valign-center talign-center">{{ $marker_pcs }}</td>
                                    <td colspan="3" class="fsize-8 valign-center pl-3">Total Length (Allowence)</td>
                                    <td colspan="3" class="talign-center">{{ $dt->marker_length + 0.75 }}</td>
                                    <td rowspan="2" colspan="1" class="fsize-10 valign-center pl-3 talign-center">PART</td>
                                    <td rowspan="2" colspan="2" class="fsize-12 bold valign-center talign-center">{{ str_replace('+', ', ', $dt->part_no) }}</td>
                                    <td class="fsize-10 valign-center pl-3">Plan</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 talign-center">{{ \Carbon\Carbon::parse($dt->cutting_plan->cutting_date)->format('d-m-y') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Total Garment</td>
                                    <td colspan="4" class="bold valign-center pl-3 talign-center">{{ $total_garment }}</td>
                                    <td colspan="3" class="fsize-10 valign-center pl-3 h23">Layer</td>
                                    <td colspan="3" class="bold valign-center talign-center">{{ $dt->layer }}</td>
                                    <td class="fsize-10 valign-center pl-3">Queue</td>
                                    <td colspan="2" class="fsize-10 valign-center pl-3 talign-center">{{ $dt->cutting_plan->queu }}</td>
                                </tr>
                                <tr>
                                    <td colspan="19">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="fsize-10 valign-center pl-3 h50">PO</td>
                                    <td colspan="18" class="fsize-14 pl-3" style="width:100px; word-wrap:break-word;">{{ $po }}</td>
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h25">Size</td>
                                    @for($i = 0; $i < 18; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center fsize-12">{{ $dt->rasio_markers[$i]->size }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                                <tr>
                                    <td width="42px" class="fsize-10 valign-center pl-3 h30">Ratio</td>
                                    @for($i = 0; $i < 18; $i++)

                                        @if($i < count($dt->rasio_markers))
                                    <td width="42px" class="valign-center talign-center fsize-12">{{ $dt->rasio_markers[$i]->ratio }}</td>
                                        @else
                                        <td width="42px">&nbsp;</td>
                                        @endif
                                    @endfor
                                </tr>
                            </table>
                        </div>
                    @endif

            @if($inc < $count_data)
                @if($inc % 2 == 0)
                    </div>
                @endif
            @elseif($inc == $count_data)
                </div>
            @else
            @endif

            @if($inc % 4 == 0)
                </section>
            @endif

            @if($inc % 4 == 0)
                @if($inc < $count_data)
                    <div class="page-break"></div>
                @endif
            @endif

            @php
                $inc++;
                if($inc % 2 == 0) {
                    $dbl++;
                }
            @endphp

        @endforeach

    @endif
</body>
</html>
