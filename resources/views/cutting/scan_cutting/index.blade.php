@extends('layouts.app',['active' => 'request_rasio_upload'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    .control-label {
        padding-top: 9px !important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Scan Cutting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Scan Cutting</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control" style="opacity: 1;" id="table_cutting_check" name="table_cutting_check" placeholder="#Scan Barcode Table" readonly="readonly" />
                    <input type="hidden" id="barcode_cutting_table" name="barcode_cutting_table" />
                    <input type="hidden" id="id_cutting_table" name="id_cutting_table" />
                    <input type="hidden" id="id_scan" name="id_scan" />
                    <input type="hidden" id="state" name="state" />
                    <input type="hidden" id="barcode_marker" name="barcode_marker" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Cutting Table" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <br />
            <div id="operator-view">
                <div class="row">
                    <div class="col-md-8">
                        <input type="text" class="form-control" style="opacity: 1;" id="cutter_check" name="cutter_check" placeholder="#Scan Barcode ID Cutter" readonly="readonly" />
                    </div>
                    <div class="col-md-4">
                        <input type="text" value="Barcode Cutter" class="form-control bg-info" readonly="readonly" />
                    </div>
                </div>
            </div>
            <br />
            <div id="marker-view"></div>
            <br />
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="marker_check" name="marker_check" placeholder="#Scan Barcode Marker" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
            <div class="row" style="margin-top:15px;">
                <button class="btn btn-success pull-right col-md-4" id="btn-cut-start">Start</button>
                <button class="btn btn-warning pull-right col-md-4 hidden" id="btn-cut-end">End</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Proses Terbaru</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Detail Info</th>
                        <th>Table</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('spreading.actual_marker._modal_table')
    @include('cutting.scan_cutting._downtime_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/scan-cutting.js') }}"></script>
@endsection
