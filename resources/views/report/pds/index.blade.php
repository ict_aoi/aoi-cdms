@extends('layouts.app',['active' => 'pds_report'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - PDS</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - PDS</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
            <li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">Report PDS</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<fieldset class="">
						@csrf
						<div class="form-group">
							<label class="control-label col-lg-2 text-bold">Output Date</label>
							<div class="col-lg-10">							
								<div class="input-group">
									<div class="col-md-8">
										<select class="form-control select-search" name="months" id="months">
											<option value="1">JANUARY</option>
											<option value="2">FEBRUARY</option>
											<option value="3">MARCH</option>
											<option value="4">APRIL</option>
											<option value="5">MAY</option>
											<option value="6">JUNE</option>
											<option value="7">JULY</option>
											<option value="8">AUGUST</option>
											<option value="9">SEPTEMBER</option>
											<option value="10">OCTOBER</option>
											<option value="11">NOVEMBER</option>
											<option value="12">DECEMBER</option>
										</select>
									</div>
									<div class="col-md-4">
										<select class="form-control select-search" id="years" name="years">
											<option value="{{ Carbon\Carbon::now()->subYear(1)->format('Y') }}">{{ Carbon\Carbon::now()->subYear(1)->format('Y') }}</option>
											<option value="{{ Carbon\Carbon::now()->format('Y') }}">{{ Carbon\Carbon::now()->format('Y') }}</option>
										</select>
									</div>
									<span class="input-group-btn">
										<button class="btn btn-primary legitRipple" type="button" id="downloads">Download</button>
									</span>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
	$('#downloads').on('click',function(){
		var months        	= $('#months').val();
		var years      		= $("#years").val();
		var factory_id		= "{{Auth::user()->factory_id}}";
		var newWindow 		= window.open("","_blank");
		
		newWindow.location.href = '/report/download-pds/'+months+'/'+years+'/'+factory_id;
	});
});
</script>
@endsection