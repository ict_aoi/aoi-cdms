<?php

namespace App\Http\Controllers\RequestFabric;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\Spreading\FabricUsed;
use App\Models\DetailPlan;
use App\Models\RequestFabric;
use App\Models\DetailRequestFabric;
use App\Models\MarkerReleaseHeader;
use App\Models\RequestFabric\ReportRoll;
use App\Http\Controllers\Controller;

class PermintaanFabricSubmitController extends Controller
{
    public function index()
    {
        return view('request_fabric.request_fabric_submit.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            $factory_id = \Auth::user()->factory_id;
            if($cutting_date != NULL) {
                $data = DB::select(db::raw("SELECT cutting_plans2.id, cutting_date, style, articleno, size_category,
                request_fabrics.queue, request_fabrics.created_by, request_fabrics.created_at from cutting_plans2
                LEFT JOIN request_fabrics on request_fabrics.plan_id = cutting_plans2.id
                where cutting_date = '$cutting_date'
                and cutting_plans2.deleted_at is null
                and cutting_plans2.factory_id = '$factory_id'
                "));
                // CuttingPlan::orderBy('queu', 'asc')
                //     ->where('cutting_date', $cutting_date)
                //     ->where('deleted_at', null);

                return datatables()->of($data)
                // ->editColumn('cutting_date', function($data) {
                //     return Carbon::parse($data->cutting_date)->format('d-M-Y');
                // })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->addColumn('po', function($data) {

                    $view = implode(', ', DetailPlan::where('id_plan',$data->id)->pluck('po_buyer')->toArray());
                    // $view = DetailPlan::find($data->id);
                    return $view;
                })
                ->editColumn('queue', function($data){
                    if($data->queue != null) {
                        return $data->queue;
                    } else {
                        return '<div id="queue_'.$data->id.'">
                            <input type="text"
                                data-id="'.$data->id.'"
                                class="form-control queue_unfilled">
                            </input>
                        </div>';
                    }
                })
                ->addColumn('action', function($data) {
                    if(is_null($data->queue)){
                        // TOMBOL AKSI TIDAK AKTIF KETIKA QUEUE BELUM ADA ''
                    }
                    else{
                        return view('request_fabric.request_fabric_submit._action', [
                            'model'          => $data,
                            'delete_queue' => route('reqFabricSubmit.deleteQueue',$data->id),
                            'detail_queue' => route('reqFabricSubmit.detailQueue',$data->id)
                        ]);
                    }
                })
                ->rawColumns(['queue', 'action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->make(true);
            }
        }
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax())
        {
            // dd($request);
            $req_fabric_id = $request->req_fabric_id;
            $factory_id = \Auth::user()->factory_id;
            if($req_fabric_id != NULL) {
                $data = DB::select(db::raw("SELECT * from hs_req_fabrics
                where id = '$req_fabric_id'
                "));
                return datatables()->of($data)
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->make(true);
            }
        }
    }

    public function detailQueue($id)
    {
        $reqFabric      = RequestFabric::where('plan_id',$id)
                            ->whereNull('deleted_at')
                            ->first();
        $obj             = new StdClass();
        $obj->id         = $reqFabric->id;
        $obj->plan_id    = $reqFabric->plan_id;
        // $obj->url_update = route('production.update',$production->id);

		return response()->json($obj,200);
    }

    public function deleteQueue($id)
    {
        $carbon     = Carbon::now();
        $reqFab     = RequestFabric::where('plan_id',$id);
        $reqFabrics = $reqFab->first();

        $reqFab->update([
            'before_queue' => $reqFabrics->queue.", ",
            'queue'        => null,
            'updated_by'   => \Auth::user()->id,
        ]);
        return response()->json(200);
    }

    public function addQueue(Request $request)
    {
        $factory_id = \Auth::user()->factory_id;
        $plan_id    = $request->plan_id;
        $queue      = $request->queue;
        $before_queue = $request->before_queue;

        // $cekqueue = RequestFabric::where('queue', $queue)
        //     ->where('factory_id', $factory_id)
        //     ->whereNull('deleted_at')
        //     ->first();
        //VALIDASI CEK QUEUE DI LEPAS KAARENA INGIN RESET QUEUE SETIAP GANTI CUTTING DATE BARU

        $cekreq = RequestFabric::where('plan_id',$plan_id)
        ->where('factory_id',$factory_id)
        ->whereNull('deleted_at')
        ->first();
        //PENGECEKAN PLAN ID TERHADAP QUEUE DI ATAS ADA ATAU TIDAK
        if(is_null($cekreq)){
            try {
                DB::beginTransaction();
                    $request_fab = RequestFabric::Create([
                        'plan_id'    => $plan_id,
                        'queue'      => $queue,
                        'factory_id' => $factory_id,
                        'created_by' => \Auth::user()->id,
                        'updated_by' => \Auth::user()->id,
                    ]);//KETIKA SUDAH DI CEK QUEUE BELUM ADA DAN PLAN ID BELUM DI INSERT KE TABEL MAKA AKAN PROSES INSERT PADA TABEL REQUEST FABRIC
                    $check_plan = CuttingPlan::where('id', $plan_id)->first();

                    if($check_plan->header_id != null & $check_plan->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $plan_id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();
                        $cutting_dates = CuttingPlan::select('cutting_date')
                            ->whereIn('id', $get_plan_id)
                            ->whereNull('deleted_at')
                            ->groupBy('cutting_date')
                            ->pluck('cutting_date')
                            ->toArray();
                    } else {
                        $get_plan_id = array($plan_id);
                        $cutting_dates = array($check_plan->cutting_date);
                    }

                    $check_marker = DB::select(db::raw("SELECT id_plan, part_no, combine_id, item_id, sum(need_total) as need_total from cutting_marker
                        where id_plan = '$plan_id'
                        GROUP BY id_plan, part_no, combine_id, item_id
                        ORDER BY part_no
                    "));

                    if(is_null($check_marker)){
                        $data_response = [
                            'status' => 422,
                            'output' => 'Permintaan marker belum dibuat !'
                        ];
                    }
                    else{
                        foreach ($check_marker as $key => $value) {

                            if($value->combine_id == null){

                                $part_no_array = explode('+', $value->part_no);

                                $data_uom_cons = DB::table('data_cuttings')
                                    ->where('plan_id', $value->id_plan)
                                    ->whereIn('part_no', $part_no_array)
                                    ->first()->uom;

                                $data_cons = DB::table('data_cuttings')
                                    ->whereIn('plan_id', $get_plan_id)
                                    ->whereIn('part_no', $part_no_array)
                                    ->sum('fbc');

                                if($data_uom_cons == 'M') {
                                    $data_cons = (float) $data_cons * 1.0936;
                                }

                                $data_qty_kebutuhan = $value->need_total;
                            } else {

                                $part_no_array = explode('+', $value->part_no);

                                $plan_id_list = CombinePart::where('combine_id', $value->combine_id)
                                    ->where('deleted_at', null)
                                    ->pluck('plan_id')
                                    ->toArray();

                                foreach($plan_id_list as $aa) {
                                    if(!in_array($aa, $get_plan_id)) {
                                        $get_plan_id[] = $aa;
                                    }
                                }

                                $data_uom_cons = DB::table('data_cuttings')
                                    ->whereIn('plan_id', $get_plan_id)
                                    ->whereIn('part_no', $part_no_array)
                                    ->first()->uom;

                                $data_cons = DB::table('data_cuttings')
                                    ->whereIn('plan_id', $get_plan_id)
                                    ->whereIn('part_no', $part_no_array)
                                    ->sum('fbc');

                                if($data_uom_cons == 'M') {
                                    $data_cons = (float) $data_cons * 1.0936;
                                }

                                $data_qty_kebutuhan = $value->need_total;

                            }

                            $data = [
                                'qty_csi'     => $data_cons,
                                'qty_need'    => $data_qty_kebutuhan,
                                //'over_consum' => $data_cons - $data_qty_kebutuhan,
                            ];

                            $detail_req_fab = DetailRequestFabric::FirstOrCreate([
                                'req_fabric_id' => $request_fab->id,
                                'item_code'     => $value->item_id,
                                'part_no'       => $value->part_no,
                                'uom'           => $data_uom_cons,
                                'csi'           => $data_cons,
                                'created_by'    => \Auth::user()->id,
                                'updated_by'    => \Auth::user()->id,
                            ]);

                        }
                        DB::commit();
                        $data_response = [
                            'status' => 200,
                            'output' => 'Insert queue berhasil !'
                        ];
                    }
            } catch (Exception $er) {
                db::rollback();
                $message = $er->getMessage();
                ErrorHandler::db($message);

                $data_response = [
                    'status' => 422,
                    'output' => 'Insert queue gagal'
                ];
            }
        }else{
                // $max_queue = RequestFabric::whereNull('deleted_at')
                //     ->where('factory_id', $factory_id)
                //     ->max('queue');

                // $data_response = [
                //     'status' => 422,
                //     'output' => 'Queue plan di tanggal ini sudah ada! Max Queue '.$max_queue.' '
                // ];
                // if(is_null($cekreq->queue)){
                    $req_fabric        = RequestFabric::find($cekreq->id);
                    $req_fabric->queue = $queue;
                    $req_fabric->save();

                    DB::commit();
                    $data_response = [
                        'status' => 200,
                        'output' => 'Update queue berhasil !'
                    ];
                //}
                // else{
                //     $data_response = [
                //         'status' => 422,
                //         'output' => 'Queue plan tersebut sudah ada !'
                //     ];
                // }
        }
        //ok
        return response()->json(['data' => $data_response]);
    }



    public function editQueue(Request $request)
    {
        $factory_id = \Auth::user()->factory_id;
        $plan_id    = $request->plan_id;
        $queue      = $request->queue;

        $cekqueue = RequestFabric::where('queue', $queue)
            ->where('factory_id', $factory_id)
            ->whereNull('deleted_at')
            ->first();

        if(is_null($cekqueue)){
            $cekreq = RequestFabric::where('plan_id',$plan_id)
                // ->where('queue',$queue)
                ->where('factory_id',$factory_id)
                ->whereNull('deleted_at')
                ->first();

            try {
                if (is_null($cekreq)) {

                    $before_queue = $cekreq->queue;
                    $before       = $before_queue.", ";

                    $cekreq->queue = $queue;
                    $cekreq->before_queue = $before;
                    $cekreq->save();

                    $data_response = [
                        'status' => 200,
                        'output' => 'Edit queue berhasil !'
                    ];
                }
            } catch (Exception $er) {
                db::rollback();
                $message = $er->getMessage();
                ErrorHandler::db($message);

                $data_response = [
                    'status' => 422,
                    'output' => 'Edit queue gagal'
                ];
            }
        }
        else{

            $max_queue = RequestFabric::whereNull('deleted_at')
                ->where('factory_id', $factory_id)
                ->max('queue');

            $data_response = [
                'status' => 422,
                'output' => 'Queue plan tersebut sudah ada! Max Queue '.$max_queue.' '
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    public function submitRequest(Request $request, $id)
    {
        if(request()->ajax())
        {
            $query = CuttingPlan::where('id', $id)
                ->whereNull('deleted_at');

            $is_exists = $query->exists();

            if($is_exists) {
                RequestFabric::firstOrCreate([
                    'plan_id' => $id,
                    'submit_by' => \Auth::user()->id,
                    'submit_at' => Carbon::now(),
                ]);
            }

            return response()->json(200);
        }
    }
}
