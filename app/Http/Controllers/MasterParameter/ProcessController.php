<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;

class ProcessController extends Controller
{
    //MASTER PROCESS
    public function masterProcess(Request $request)
    {
        $master_locator = DB::table('master_locator')
                            ->whereNull('deleted_at')
                            ->get();
        return view('master_parameter.process.index', compact('master_locator'));
    }

    public function ajaxGetDataMasterProcess(Request $request)
    {
        if ($request->ajax()){
            $data = DB::table('master_process')
                    ->select('master_process.id','master_process.process_name', 'master_process.locator_id', 'master_locator.locator_name')
                    ->join('master_locator', 'master_locator.id', '=', 'master_process.locator_id')
                    ->wherenull('master_process.deleted_at')
                    ->wherenull('master_locator.deleted_at')
                    ->orderBy('master_process.created_at', 'desc')
                    ->get();

            //datatables
            return DataTables::of($data)
                // ->editColumn('is_artwork', function($data){
                //     if ($data->is_artwork) {
                //         $str = '<span class="label label-success label-rounded">
                //                 <i class="icon-checkmark2"></i>
                //                 </span>';
                //     }else {
                //         $str = '<span class="label label-default label-rounded">
                //                 <i class="icon-cross3"></i>
                //                 </span>';
                //     }

                //     return $str;
                // })
                ->addColumn('action', function($data){ return view('_action', [
                        'edit_modal' => route('masterparam.editProcess', $data->id),
                        'id' => $data->id,
                        'deletes' => route('masterparam.deleteProcess', $data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    //add new master process
    public function addProcess(Request $request)
    {

        $this->validate($request, [
            'process_name' => 'required',
            'locator_id' => 'required'
        ]);

        // if ($request->is_artwork=='on') {
        //     $is_artwork = true;
        // }else {
        //     $is_artwork = false;
        // }

        $process_name = trim(strtoupper($request->process_name));

        $data = array([
            'locator_id' => $request->locator_id,
            'process_name' => $process_name,
            // 'is_artwork' => $is_artwork,
            'created_at' => Carbon::now()
        ]);

        // cek process exists
        $cekifexist = DB::table('master_process')
                        ->where('process_name', $process_name)
                        ->whereNull('deleted_at')
                        ->exists();
        
        if ($cekifexist) {
            return response()->json('Master Process already exists..!', 422);
        }

        try {
            DB::begintransaction();

            //insert table process
            DB::table('master_process')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master process
    public function editProcess($id)
    {
        $master_process = DB::table('master_process')
                        ->select('master_process.id','master_process.process_name', 'master_process.locator_id', 'master_locator.locator_name')
                        ->join('master_locator', 'master_locator.id', '=', 'master_process.locator_id')
                        ->where('master_process.id', $id)
                        ->first();

        return response()->json($master_process, 200);
    }

    //update master process
    public function updateProcess(Request $request)
    {
        $data = $request->all();

        $locator_id = $data['locator_id_update'];
        $process_name = trim(strtoupper($data['process_name_update']));

        // if ($request->is_artwork_update=='on') {
        //     $is_artwork = true;
        // }else {
        //     $is_artwork = false;
        // }

        // cek exists
        // $cekifexist = DB::table('master_process')
        //                 ->where('process_name', $process_name)
        //                 ->whereNull('deleted_at')
        //                 ->exists();
        
        // if ($cekifexist) {
        //     return response()->json('Master process already exists..!', 422);
        // }

        try {
            DB::beginTransaction();
                DB::table('master_process')
                ->where('id', $data['id_update'])
                ->update([
                    'locator_id' => $locator_id,
                    'process_name' => $process_name,
                    // 'is_artwork' => $is_artwork,
                    'updated_at' => Carbon::now()
                ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }    

        
        $master_locator = DB::table('master_locator')
                        ->whereNull('deleted_at')
                        ->get();

        return view('master_parameter.process.index', compact('master_locator'));

    }

    //delete master process
    public function deleteProcess(Request $request)
    {
        $id = $request->id;
        DB::table('master_process')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

        return view('master_parameter.process.index');
    }

    //END OF MASTER PROCESS
}
