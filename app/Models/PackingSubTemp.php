<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PackingSubTemp extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_packinglist_subtemp';
	protected $fillable = ['id_packinglist','no_polybag','bundle_id','qty','user_id','factory_id','created_at','updated_at','deleted_at','size','komponen_name','poreference','article','cut_num','start_no','end_no','style','set_type','color_name','season'];
}
