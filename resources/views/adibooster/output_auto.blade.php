@extends('adibooster.index')
@section('page-css')
<style type="text/css">
.header{
    font-family:arial;
    font-size:  50px;
}
.text_header{
    font-weight: bold;
    text-align: center;
}
.time_update{
    text-align: center;
    font-weight: bold;
    text-align: center;
    font-size: 25px;
}
.content_header_1{
    text-align: left;
    font-weight: bold;
    font-size:18px;
    padding-left: 70px;
}
.content_header_2{
    text-align: right;
    font-weight: bold;
    font-size:18px;
    padding-right: 70px;
}
.table_1{
    background-color: orange;
    opacity: 0.8;
    color:white;
    font-size:20px;
    text-align:center;
    font-weight:bold;
    border:2.6px solid white;
}
.table_content_2{
    margin-top:10px;
    width:40%;
    margin-left: auto;
    margin-right: auto;
}
.table_content_3{
    margin-top:10px;
    width:85%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    padding-right:10px;
    padding-left:10px;
}
.table_content_4{
    margin-top:0px;
    width:85%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    padding-right:10px;
    padding-left:10px;
}
.text_content{
    font-size:20px;
    font-weight:bold;
    border:3px solid white;
}
.text_content2{
    font-size:17px;
    font-weight:bold;
    border:1px solid white;
}
.text_content3{
    font-size:17px;
    font-weight:bold;
    border:1px solid white;
    background-color: #00FFFF;
}
.filled_output{
    background-color:grey;
    opacity: 0.5;
    border:2.6px solid white;
}
.filled_output2{
    font-size:18px;
    color:white,
    text-align:center;
    font-weight:bold;
}
.table-responsive{
    height:240px; 
    width:100%;
    overflow-y: hidden;
    margin-left: auto;
    margin-right: auto;
}
</style>
@endsection
@section('page-content')
<meta http-equiv="refresh" content="300"> <!-- Refresh every 15 minutes -->
<table style="width: 100%;margin-top:10px">
    <thead class="bg-blue-700">
        <tr class="header">
            <th class="text_header" colspan="6">DASHBOARD AUTO AOI {{$factory_id}}</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr class="bg-blue-700">
            <td colspan="6" class="time_update">Last Updated At : {{\Carbon\Carbon::now()->format('H:i:s')}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="bg-blue-700">
            <td class="content_header_1" colspan="2">Date &nbsp&nbsp&nbsp{{\Carbon\Carbon::parse($today)->format('d-m-Y')}}</td>
            <td></td>
            <td></td>
            <td class="content_header_2" colspan="2">Man Power &nbsp&nbsp&nbsp {{$mp}}</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
<table class="table_content_2">
    <tbody>
        <tr>
            <td></td>
            <td class="table_1">Achievement</td>
        </tr>
        <tr height="50">
            <td class="table_1">Target Output (Pcs)</td>
            <td class="filled_output"><p style="font-size:20px;color:white;font-weight:bold;text-align:center">{{$target_output}}</p></td>
        </tr>
        <tr height="50">
            <td class="table_1">Actual Output (Pcs)</td>
            <td class="filled_output"><p style="font-size:20px;color:white;font-weight:bold;text-align:center">{{number_format($output->output_auto)}}</p></td>
        </tr>
    </tbody>
</table>
<table class="table_content_3" width="100%">
    <tr class="bg-blue text_content" height="30">
        <td style="border:3px solid white" width="20%">Style</td>
        <td style="border:3px solid white" width="10%">Qty Order</td>
        <td style="border:3px solid white" width="10%">Actual Qty</td>
    </tr>
</table>
<div class="table-responsive">
    <table class="table_content_4" width="100%">
        <tbody>
            @foreach($data as $key => $val)
            <tr>
                <td class="bg-blue text_content2" width="20%">
                    {{$val->style_set}}
                </td>
                <td class="text_content3" width="10%">
                    {{number_format($val->ordered_qty)}}
                </td>
                <td class="text_content3" width="10%">
                    {{number_format($val->output_auto)}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('page-js')
<script type="text/javascript">
    var $el = $(".table-responsive");
        function anim() {
            var st = $el.scrollTop();
            var sb = $el.prop("scrollHeight")-$el.innerHeight();
            $el.animate({scrollTop: st<sb/2 ? sb : 0}, 37000, anim);
        }
    function stop(){
        $el.stop();
    }
        anim();
        $el.hover(stop, anim);
</script>
@endsection