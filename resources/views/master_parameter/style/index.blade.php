@extends('layouts.app', ['active' => 'masterStyle'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">STYLE & KOMPONEN</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Home</a></li>
            <li class="active">Style & Komponen</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('masterparam.ajaxGetDataMasterStyle') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_lcdate">
				<label><b>Choose Order Date (month/day/year)</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group" id="filter_by_style">
				<label><b>Choose Style</b></label>
                    <div class="input-group col-md-12">
                        <select data-placeholder="Select a State..." name="style" id="style" class="form-control select-search">
                            <option value=""></option>
                            @foreach($styles as $list)
                                <option value="{{ $list->id }}" data-style="{{ $list->style_standart }}" data-season="{{ $list->season }}" data-types="{{ $list->set_type }}">{{ $list->style }}</option>
                            @endforeach
                        </select>
                        {{-- <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div> --}}
                    </div>
			</div>
            <div class="form-group hidden">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="style">Filter by Style</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="lc">Filter by LC Date</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading hidden">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add Style <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_list">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>STYLE</th>
                        <th>SEASON</th>
                        <th>KOMPONEN</th>
                        <th>TYPE</th>
                        <th style="width:10px;">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
    @include('master_parameter.style._index_modal')
    @include('master_parameter.style._update_modal')
@endsection

@section('page-js')
<script type="text/javascript">

    $('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });


    $('#style').on('change', function(){
        if($(this).val() != ''){
            $('#form_filter').submit();
        }
    });

//$( document ).ready(function() {

    var url = $('#form_filter').attr('action');
    //datatables
  /*  $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        //serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });*/

    var table = $('#table_list').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        //serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: url,
        data: {
            date_range: $('#date_range').val(), 
            style: $('#style').val(), 
            style_standart: $('#style option:selected').data('style'),
            season: $('#style option:selected').data('season'),
            set_type: $('#style option:selected').data('types'),
            radio_status: $('input[name=radio_status]:checked').val()
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'season_name', name: 'season_name'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'type_name', name: 'type_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var date_range = $('#date_range').val();
        var style  = $('#style').val();
        var style_standart  = $('#style option:selected').data('style');
        var season  = $('#style option:selected').data('season');
        var set_type  = $('#style option:selected').data('types');
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        if(date_range == '' && style == '') {
            myalert('error','Please select filter first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {
                date_range: date_range, 
                style: style, 
                radio_status: radio_status,
                style_standart: style_standart,
                season: season,
                set_type: set_type
            },
            beforeSend: function() {
                $('#table_list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table_list').unblock();
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table_list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //add new
    $('.add_new').on('click',function(){
        $('#modal_add_new_').modal('show');
    });
    //end of add new
    
    $('.syncStyle').on('click',function(){
        $('#modal_sync_').modal('show');
    });

    $('#modal_add_new_').on('shown.bs.modal', function(){
        $('input[name=season]').focus();
    });

    $('#modal_edit_').on('shown.bs.modal', function(){
        $('input[name=seasonUpdate]').focus();
    });

    $('#modal_add_new_').on('hidden.bs.modal', function(){
        $('#form-add').trigger("reset");
        $('#season').val(null).trigger('change');
        $('#komponen').val(null).trigger('change');

        $('#form_filter').submit();
        //table.draw();
    });

    $('#modal_edit_').on('hidden.bs.modal', function(){
        $('#form-update').trigger("reset");
        $('#season').val(null).trigger('change');
        $('#komponenIdUpdate').val(null).trigger('change');

        $('#form_filter').submit();
    });

    //open modal upload (api)
    $('#table_list').on('click', '.upload', function() {
        var str = $(this).data('value');
        var season = $(this).data('seasonid');
        var season_name = $(this).data('seasonname');
        var type_id = $(this).data('typeid');
        var type_name = $(this).data('types');

        $('input[name=modalstyle]').val(str);
        $('input[name=season]').val(season);
        $('input[name=season_name]').val(season_name);
        $('input[name=type_name]').val(type_name);
        $('input[name=type_id]').val(type_id);
        $('#modal_add_new_').modal('show');
    });

    //delete
    $('#table_list').on('click', '.deleteStyle', function() {
        event.preventDefault();
       
        var style = $(this).data('style');
        var komponen = $(this).data('komponen');
        var type_id = $(this).data('type');
        var season = $(this).data('seasonid');

        if(style == 'kosong' || komponen == 'kosong' || type_id == 'kosong' || season == 'kosong') {
            alert('asas');
            return false;
        }
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this row ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-style/delete-style/"+style+"/"+komponen+"/"+type_id+"/"+season,
                    type: "GET",
                    data: {
                        "style": style,
                        "komponen": komponen,
                        "type_id": type_id,
                        "season": season,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Deleted Successfully');
                        //table.ajax.reload();

                        $('#form_filter').submit();
                    }
                });
            }
        });
    });
    //end of delete

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['modalstyle', 'season', 'komponen', 'type_id'];
        var val = [
            $('#modalstyle').val(), 
            $('#season').val(), 
            $('#komponen').val(),
            $('#type_id').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
               // $('#season').val(null).trigger('change').focus();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    //update
    $('#form-update').submit(function(event) {
        event.preventDefault();

        if($('#totalUpdate').val() < 1){
            myalert('error', 'total must be > 0..!');
            return false;
        }

        var formData = new FormData($(this)[0]);
        var parameter = ['idUpdate', 'styleUpdate', 'seasonUpdate', 'komponenUpdate', 'type_id_update'];
        var val = [
            $('#idUpdate').val(), 
            $('#styleUpdate').val(), 
            $('#seasonUpdate').val(), 
            $('#komponenIdUpdate').val(),
            $('#type_id_update').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            	$.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_lcdate').addClass('hidden');
        }
        else if (this.value == 'lc') {
            if($('#filter_by_lcdate').hasClass('hidden')) {
                $('#filter_by_lcdate').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
    });

    function edit(url){

        $.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {

            var process = response.process;

            $('#idUpdate').val(response.id);
            $('#styleUpdate').val(response.style);
            $('#seasonUpdate').val(response.season);
            $('#season_name_update').val(response.season_name);

            $('#komponenIdUpdate').val(response.komponen).trigger('change');
            $('#komponenNameUpdate').val(response.komponen_name);

            $('#type_id_update').val(response.type_id);
            $('#type_name_update').val(response.type_name);

            /*if(process != null){
                $.each(process.split(","), function(i, v){
                    $('#process_id_update option[value='+ v +']').prop('selected', true).trigger('change');
                });
            }else{
                $('#process_id_update').val(null).trigger('change');
            }*/

			$('#modal_edit_').modal();

		});
    }

//});

</script>
@endsection
