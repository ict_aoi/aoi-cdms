<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MarkerReleaseHeader extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $table = 'marker_release_headers';
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['plan_id','part_no','deleted_at','cutting_date','factory_id','lc_date','style','item_id','combine_id', 'item_code', 'item_id_book', 'item_code_book'];
    
    public function cutting_plan()
    {
        return $this->belongsTo('App\Models\CuttingPlan', 'plan_id', 'id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\MarkerReleaseDetail', 'header_id', 'id')->orderBy('created_at', 'desc');
    }

    public function detail_approve()
    {
        return $this->hasMany('App\Models\MarkerReleaseDetail', 'header_id', 'id')->where('is_confirm', true)->orderBy('created_at', 'desc');
    }
    
    public function factory()
    {
        return $this->belongsTo('App\Models\Factory', 'factory_id', 'id');
    }
}