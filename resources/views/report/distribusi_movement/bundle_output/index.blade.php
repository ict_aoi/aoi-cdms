@extends('layouts.app',['active' => 'bundle_output_report'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - Bundle Output</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - Bundle Output</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">LAPORAN SCAN OUT PROCESS</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="true">UNCOMPLETE SCAN OUT PROCESS</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<form class="form-horizontal" action="{{ route('reportComponentMovement.downloadBundleOutput') }}" id="form_filter_date" method="POST">
						<fieldset class="content-group">
                            @csrf
                            <div class="form-group">
								<div class="col-lg-12">
									<div class="alert bg-info alert-styled-left">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">Laporan Hasil Scan Out Process (SET GARMENT) - 1 Tanggal = Data Scan SHIFT 1 + SHIFT 2</span>
									</div>
								</div>
								<label class="control-label col-lg-1 text-bold">Locator</label>
								<div class="col-lg-4">
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-sync"></i></button>
										</span>
										<select data-placeholder="Select a locator..." class="form-control select-search" name="locator" id="locator" required>
                                            <option value=""></option>
                                                @foreach($locators as $locator)
                                                    <option value="{{ $locator->id }}">{{ $locator->locator_name }}</option>
                                                @endforeach
                                        </select>
									</div>
                                </div>
								<label class="control-label col-lg-2 text-bold">Tanggal Scan</label>
								<div class="col-lg-5">							
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="scan_date" placeholder="Masukkan Tanggal Spreading" id="scan_date" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="tab-pane" id="basic-tab2">
					<form class="form-horizontal" action="{{ route('reportComponentMovement.downloadBundleOutputDaily') }}" id="act_cut2" method="POST">
						<fieldset class="content-group">
                            @csrf
                            <div class="form-group">
							<div class="col-lg-12">
									<div class="alert bg-danger alert-styled-left">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">Detail Bundle Uncomplete Scan Out Process! (SHIFT1 + SHIFT2)</span>
									</div>
								</div>
								<label class="control-label col-lg-1 text-bold">Locator</label>
								<div class="col-lg-4">
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-sync"></i></button>
										</span>
										<select data-placeholder="Select a locator..." class="form-control select-search" name="locator_2" id="locator_2" required>
                                            <option value=""></option>
                                                @foreach($locators as $locator)
                                                    <option value="{{ $locator->id }}">{{ $locator->locator_name }}</option>
                                                @endforeach
                                        </select>
									</div>
                                </div>
								<label class="control-label col-lg-2 text-bold">Tanggal Scan</label>
								<div class="col-lg-5">							
									<div class="input-group">
										<span class="input-group-btn">	
											<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
										</span>
										<input type="text" class="form-control daterange-basic" name="scan_date_2" id="scan_date_2" placeholder="Masukkan Tanggal Spreading" id="scan_date2" required="">
										<span class="input-group-btn">
											<button class="btn btn-primary legitRipple" type="submit">Download</button>
										</span>
									</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        // $('#locator').on('change', function() {
        //     $("#state").html('');
        //     $("#state").append($('<option>').attr('value', 'out').text('OUT')).append($('<option>').attr('value', 'in').text('IN'));
        //     if(this.value == 10) {
        //         $("#state option[value='in']").remove();
        //     }
        //     if(this.value == 11) {
        //         $("#state option[value='out']").remove();
        //     }
        // });
		// $('#locator_2').on('change', function() {
        //     $("#state_2").html('');
        //     $("#state_2").append($('<option>').attr('value', 'out').text('OUT')).append($('<option>').attr('value', 'in').text('IN'));
        //     if(this.value == 10) {
        //         $("#state_2 option[value='in']").remove();
        //     }
        //     if(this.value == 11) {
        //         $("#state_2 option[value='out']").remove();
        //     }
        // });
    });
</script>
@endsection