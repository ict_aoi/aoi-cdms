<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TempdataTicket extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_data_ticket';
	protected $dates = ['created_at'];
	protected $fillable = ['season', 'style', 'set_type', 'poreference', 'article', 'cut_num', 'size', 'part_id', 'part_num', 'part_name', 'note', 'start_num', 'end_num', 'depth', 'processing_fact', 'ed_pcd', 'lot', 'coll', 'qc_op', 'bd_op', 'cutter', 'add_note', 'box_lim', 'replacement', 'input_time', 'user_nik','created_by','updated_at'];
}
