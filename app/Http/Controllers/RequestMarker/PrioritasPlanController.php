<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\InfoSync;
use App\Models\DetailPlan;
use App\Models\CuttingPlan;
use App\Models\RatioMarker;
use App\Models\CombinePart;
use App\Models\CuttingMarker;
use App\Models\RatioDetailTemp;
use App\Models\CuttingMovement;
use App\Models\Data\DataCutting;

class PrioritasPlanController extends Controller
{
    // view prioritas plan
    public function index(){
        return view('request_marker.prioritas_plan.index');
    }

    // datatable prioritas plan
    public function ajaxGetPlaning(Request $request){
        $date_cutting = $request->date_cutting;

        if (!empty($date_cutting)) {
            $data = DB::table('jaz_cutting_plan_new_update_3')
                ->where('cutting_date',$date_cutting)
                ->where('factory_id',Auth::user()->factory_id)
                ->orderBy('queu')
                ->orderBy('style');

        } else {
            $data = array();
        }

        return DataTables::of($data)
        ->editColumn('style',function($data){
            return $data->style.' '.$data->top_bot;
        })
        ->addColumn('po_buyer',function($data){
            if($data->plan_id == null) {
                return '';
            } else {
                $get_plan = CuttingPlan::where('id', $data->plan_id)
                    ->whereNull('deleted_at')
                    ->first();

                if($get_plan == null) {
                    return '';
                } else {
                    if($get_plan->header_id == null) {
                        $get_po = DetailPlan::where('id_plan', $data->plan_id)
                            ->whereNull('deleted_at')
                            ->get();
                    } else {
                        $get_plan_id = CuttingPlan::where('header_id', $get_plan->header_id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        $get_po = DetailPlan::whereIn('id_plan', $get_plan_id)
                            ->whereNull('deleted_at')
                            ->get();
                    }

                    $return_po = array();

                    foreach($get_po as $po) {
                        $return_po[] = $po->po_buyer.' ('.$po->destination.' - '.Carbon::parse($po->cutting_plan->cutting_date)->format('d/M').')';
                    }

                    return implode(', ', $return_po);
                }
            }
            $po = DB::table('ns_trig_po_update')
                ->where('cutting_date',$data->cutting_date)
                ->where('style',$data->style)
                ->where('articleno',$data->articleno)
                ->where('size_category',$data->size_category)
                ->where('queu',$data->queu)
                ->first();

            return $pob =  isset($po->po_buyer) ? trim($po->po_buyer) : '';
        })
        ->editColumn('size_category',function($data){
            if ($data->size_category=='A') {
                return 'Asian';
            } elseif ($data->size_category=='J') {
                return 'Japan';
            }else{
                return 'Intl';
            }
        })
        ->addColumn('queu', function($data){
            $cek = DB::table('ns_trig_po_update')
                ->where('cutting_date',$data->cutting_date)
                ->where('style',$data->style)
                ->where('articleno',$data->articleno)
                ->where('size_category',$data->size_category)
                ->where('queu',$data->queu)
                ->first();

            if (isset($cek->queu) != null) {
                if($cek->queu == 0) {
                    return 'Combine';
                } else {
                    return '<div id="edqueu_'.$cek->id.'">
                        <input type="text"
                            data-date="'.$data->cutting_date.'"
                            data-style="'.$data->style.'"
                            data-article="'.$data->articleno.'"
                            data-color="'.$data->color_name.'"
                            data-size="'.$data->size_category.'"
                            data-mo_created="'.$data->mo_updated_at.'"
                            data-material="'.$data->material.'"
                            data-color_code="'.$data->color_code.'"
                            data-gtb="'.$data->get_top_bot.'"
                            data-tb="'.$data->top_bot.'"
                            data-season="'.$data->season.'"
                            data-id="'.$cek->id.'"
                            class="form-control edqueu_unfilled" value="'.$cek->queu.'" >
                        </input>
                    </div>';
                }
            } else {
                return '<div id="queu_'.$data->style.'">
                    <input type="text"
                        data-date="'.$data->cutting_date.'"
                        data-style="'.$data->style.'"
                        data-article="'.$data->articleno.'"
                        data-color="'.$data->color_name.'"
                        data-size="'.$data->size_category.'"
                        data-mo_created="'.$data->mo_updated_at.'"
                        data-material="'.$data->material.'"
                        data-color_code="'.$data->color_code.'"
                        data-gtb = "'.$data->get_top_bot.'"
                        data-tb="'.$data->top_bot.'"
                        data-season="'.$data->season.'"
                        class="form-control queu_unfilled" >
                    </input>
                </div>';
            }
        })
        ->addColumn('action',function($data){
            $color = str_replace(' ', '-', $data->color_name);
            $color_code = str_replace(' ', '-', $data->color_code);
            if (isset($data->plan_id) != null) {
                $cutting_plan = CuttingPlan::where('id', $data->plan_id)
                    ->whereNull('deleted_at')
                    ->first();

                if($cutting_plan->is_header) {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'combine'  => route('prioritasPlan.modalCombinePlan', $data->plan_id),
                        'split'  => '#',
                        'delete'  => '#',
                    ]);
                } else {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'delete'  => '#',
                    ]);
                }
            } else {
                if ($data->get_top_bot == 2 && $data->top_bot == null) {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'split'  => '#',
                        'split_set'  => '#',
                    ]);
                } else if($data->get_top_bot == 2 && $data->top_bot != null) {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'split'  => '#',
                        'merge_set'  => '#',
                    ]);
                } else {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'split'  => '#',
                    ]);
                }
            }

        })
        ->rawcolumns(['po_buyer','queu','action'])
        ->make(true);
    }

    // action download prioritas plan
    public function downloadPrioritas(Request $request)
    {
        $this->validate($request, [
            'date_cutting_download' => 'required',
        ]);

        $cutting_date = $request->date_cutting_download;

        $data = DB::table('jaz_report_prioritas_cutting')
            ->where('cutting_date', $cutting_date)
            ->whereNotNull('queu')
            ->where('deleted_at', null)
            ->orderBy('queu', 'asc');

        if(count($data->get()) < 1) {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Prioritas_Cutting_'.$cutting_date;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report_prioritas', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'QUEUE',
                        'CUTTING DATE',
                        'STYLE',
                        'PO BUYER',
                        'ARTICLE',
                        'SIZE CATEGORY',
                        'COLOR',
                        'QTY'
                    ));

                    $data->chunk(100, function($rows) use ($sheet, $i) {
                        foreach ($rows as $row) {
                            if($row->top_bot != null) {
                                $style = $row->style.'-'.$row->top_bot;
                            } else {
                                $style = $row->style;
                            }
                            $data_excel = array(
                                $row->queu,
                                Carbon::parse($row->cutting_date)->format('d-m-Y'),
                                $style,
                                $row->po_buyer,
                                $row->articleno,
                                $row->size_category,
                                $row->color,
                                $row->qty
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    // action sinkron data_cuttings dengan erp per day
    public function dailySyncDay(Request $request)
    {
        $cutting_date = date('Y-m-d', strtotime($request->cutting_date));

        // set factory
        if(\Auth::user()->factory_id == 2) {
            $factory_id = '1000012';
        } elseif(\Auth::user()->factory_id == 1) {
            $factory_id = '1000005';
        } elseif(\Auth::user()->factory_id == 3) {
            $factory_id = '1000048';
        }  else {
            $warehouse = 0;
        }

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')
            ->table('jz_csi_details')
            ->where('datestartschedule', $cutting_date)
            ->where('m_warehouse_id', $factory_id)
            ->get();

        // get data cutting
        $cdms_cutting_datas =  DataCutting::where('cutting_date', $cutting_date)
            ->where('warehouse', \Auth::user()->factory_id)
            ->get();

        $is_exists = '';

        // start datebase excecution
        try
        {
            DB::beginTransaction();
            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data)
            {
                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                    $warehouse = 3;
                    //UPDATE FOR AOI 3
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                    ->where('documentno', $erp_csi_data->documentno)
                    ->where('style', $erp_csi_data->style)
                    ->where('po_buyer', $erp_csi_data->po_buyer)
                    ->where('articleno', $erp_csi_data->kst_articleno)
                    ->where('size_finish_good', $erp_csi_data->size_fg)
                    ->where('size_category', $size_category)
                    ->where('part_no', $erp_csi_data->part_no)
                    ->where('warehouse', $warehouse)
                    // ->where('color_name',$erp_csi_data->color)
                    ->where('item_id',$erp_csi_data->item_id)
                    ->where('product',$erp_csi_data->product);

                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {
                    // check data if update data
                    // $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                    if((int)$erp_csi_data->qtyordered != (int)$is_exists->ordered_qty) {

                        // set data to update
                        // $data_for_update['ordered_qty'] = (int)$erp_csi_data->qtyordered;
                        // update data cutting
                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);

                        DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                        ->where('documentno', $erp_csi_data->documentno)
                        ->where('style', $erp_csi_data->style)
                        ->where('po_buyer', $erp_csi_data->po_buyer)
                        ->where('articleno', $erp_csi_data->kst_articleno)
                        ->where('size_finish_good', $erp_csi_data->size_fg)
                        ->where('size_category', $size_category)
                        ->where('part_no', $erp_csi_data->part_no)
                        ->where('warehouse', $warehouse)
                        ->where('item_id',$erp_csi_data->item_id)
                        ->where('product',$erp_csi_data->product)->update([
                            'is_recycle'        => $erp_csi_data->recycle,
                            'ordered_qty'       => (int)$erp_csi_data->qtyordered
                        ]);
                        
                    }
                    // else{
                    //     // insert table info status update
                    //     InfoSync::FirstOrCreate([
                    //         'type' => 'update',
                    //         'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                    //         'style' => $is_exists->style,
                    //         'articleno' => $is_exists->articleno,
                    //         'po_buyer' => $is_exists->po_buyer,
                    //         'documentno' => $is_exists->documentno,
                    //         'desc' => 'update is_recycle',
                    //         'factory_id' => $warehouse,
                    //     ]);

                    //     $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                    // }

                    // $data_is_exists->update($data_for_update);
                    DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                    ->where('documentno', $erp_csi_data->documentno)
                    ->where('style', $erp_csi_data->style)
                    ->where('po_buyer', $erp_csi_data->po_buyer)
                    ->where('articleno', $erp_csi_data->kst_articleno)
                    ->where('size_finish_good', $erp_csi_data->size_fg)
                    ->where('size_category', $size_category)
                    ->where('part_no', $erp_csi_data->part_no)
                    ->where('warehouse', $warehouse)
                    ->where('item_id',$erp_csi_data->item_id)
                    ->where('product',$erp_csi_data->product)->update([
                        'is_recycle'        => $erp_csi_data->recycle
                    ]);
                    InfoSync::FirstOrCreate([
                        'type' => 'update',
                        'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                        'style' => $is_exists->style,
                        'articleno' => $is_exists->articleno,
                        'po_buyer' => $is_exists->po_buyer,
                        'documentno' => $is_exists->documentno,
                        'desc' => 'update is_recycle',
                        'factory_id' => $warehouse,
                    ]);
                } 
                else {
                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => (int)$erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    InfoSync::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data)
            {
                $check_count_data = 0;
                // set size_category
                $size_category = substr($cdms_cutting_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                        $warehouse = 3;
                        //UPDATE FOR AOI 3
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();

                    // insert table info
                    InfoSync::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            return response()->json('Sync Success . . . ');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json('Sync Field . . . ');
        }
    }

    // action sikron data_cuttings dengan erp h + 3
    public function dailySync()
    {
        // set date range
        $from = date(Carbon::now()->subDays(2)->format('Y-m-d'));
        $to = date(Carbon::now()->addDays(3)->format('Y-m-d'));

        // set factory
        if(\Auth::user()->factory_id == 2) {
            $factory_id = '1000012';
        } elseif(\Auth::user()->factory_id == 1) {
            $factory_id = '1000005';
        } elseif(\Auth::user()->factory_id == 3) {
            $factory_id = '1000048';
        }  else {
            $factory_id = '0000000';
        }

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')
            ->table('jz_csi_details')
            ->whereBetween('datestartschedule', [$from, $to])
            ->where('m_warehouse_id', $factory_id)
            ->get();

        // get data cutting
        $cdms_cutting_datas =  DataCutting::whereBetween('cutting_date', [$from, $to])
            ->where('warehouse', \Auth::user()->factory_id)
            ->get();

        $is_exists = '';

        // start datebase excecution
        try
        {
            DB::beginTransaction();
            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data)
            {
                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                    $warehouse = 3;
                    //UPDATE FOR AOI 3
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                    ->where('documentno', $erp_csi_data->documentno)
                    ->where('style', $erp_csi_data->style)
                    ->where('po_buyer', $erp_csi_data->po_buyer)
                    ->where('articleno', $erp_csi_data->kst_articleno)
                    ->where('size_finish_good', $erp_csi_data->size_fg)
                    ->where('size_category', $size_category)
                    ->where('part_no', $erp_csi_data->part_no)
                    ->where('warehouse', $warehouse)
                    ->where('item_id',$erp_csi_data->item_id)
                    ->where('product',$erp_csi_data->product);

                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {
                    // check data if update data
                    if((int)$erp_csi_data->qtyordered != (int)$is_exists->ordered_qty) {

                        // set data to update
                        // $data_for_update['ordered_qty'] = $erp_csi_data->qtyordered;
                        // $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);

                        // update data cutting
                        // $data_is_exists->update($data_for_update);
                        DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                        ->where('documentno', $erp_csi_data->documentno)
                        ->where('style', $erp_csi_data->style)
                        ->where('po_buyer', $erp_csi_data->po_buyer)
                        ->where('articleno', $erp_csi_data->kst_articleno)
                        ->where('size_finish_good', $erp_csi_data->size_fg)
                        ->where('size_category', $size_category)
                        ->where('part_no', $erp_csi_data->part_no)
                        ->where('warehouse', $warehouse)
                        ->where('item_id',$erp_csi_data->item_id)
                        ->where('product',$erp_csi_data->product)->update([
                            'ordered_qty'       => (int)$erp_csi_data->qtyordered,
                            'is_recycle'        => $erp_csi_data->recycle
                        ]);
                    }
                    DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                        ->where('documentno', $erp_csi_data->documentno)
                        ->where('style', $erp_csi_data->style)
                        ->where('po_buyer', $erp_csi_data->po_buyer)
                        ->where('articleno', $erp_csi_data->kst_articleno)
                        ->where('size_finish_good', $erp_csi_data->size_fg)
                        ->where('size_category', $size_category)
                        ->where('part_no', $erp_csi_data->part_no)
                        ->where('warehouse', $warehouse)
                        ->where('item_id',$erp_csi_data->item_id)
                        ->where('product',$erp_csi_data->product)->update([
                            'is_recycle'        => $erp_csi_data->recycle
                        ]);
                    // else{
                    //     // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'update is_recycle',
                            'factory_id' => $warehouse,
                        ]);

                    //     $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                    //     $data_is_exists->update($data_for_update);
                    // }
                } else {
                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => (int)$erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    InfoSync::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data)
            {
                $check_count_data = 0;

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                        $warehouse_erp = 3;
                        //UPDATE FOR AOI 3
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();

                    // insert table info
                    InfoSync::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            return response()->json('Sync Success . . . ');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json('Sync Field . . . ');
        }
    }

    // action sikron data_cuttings dengan erp h + 7
    public function dailySync7()
    {
        // set date range
        $from = date(Carbon::now()->subDays(2)->format('Y-m-d'));
        $to = date(Carbon::now()->addDays(5)->format('Y-m-d'));

        // set factory
        if(\Auth::user()->factory_id == 2) {
            $factory_id = '1000012';
        } elseif(\Auth::user()->factory_id == 1) {
            $factory_id = '1000005';
        } elseif(\Auth::user()->factory_id == 3) {
            $factory_id = '1000048';
        }  else {
            $factory_id = '0000000';
        }

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')
            ->table('jz_csi_details')
            ->whereBetween('datestartschedule', [$from, $to])
            ->where('m_warehouse_id', $factory_id)
            ->get();

        // get data cutting
        $cdms_cutting_datas =  DataCutting::whereBetween('cutting_date', [$from, $to])
            ->where('warehouse', \Auth::user()->factory_id)
            ->get();

        $is_exists = '';

        // start datebase excecution
        try
        {
            DB::beginTransaction();

            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data)
            {

                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                    $warehouse = 3;
                    //UPDATE FOR AOI 3
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                    ->where('documentno', $erp_csi_data->documentno)
                    ->where('style', $erp_csi_data->style)
                    ->where('po_buyer', $erp_csi_data->po_buyer)
                    ->where('articleno', $erp_csi_data->kst_articleno)
                    ->where('size_finish_good', $erp_csi_data->size_fg)
                    ->where('size_category', $size_category)
                    ->where('part_no', $erp_csi_data->part_no)
                    ->where('warehouse', $warehouse)
                    ->where('item_id',$erp_csi_data->item_id)
                    ->where('product',$erp_csi_data->product);

                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {

                    // check data if update data
                    if((int)$erp_csi_data->qtyordered != (int)$is_exists->ordered_qty) {

                        // set data to update
                        // $data_for_update['ordered_qty'] = $erp_csi_data->qtyordered;
                        // $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);
                        DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                        ->where('documentno', $erp_csi_data->documentno)
                        ->where('style', $erp_csi_data->style)
                        ->where('po_buyer', $erp_csi_data->po_buyer)
                        ->where('articleno', $erp_csi_data->kst_articleno)
                        ->where('size_finish_good', $erp_csi_data->size_fg)
                        ->where('size_category', $size_category)
                        ->where('part_no', $erp_csi_data->part_no)
                        ->where('warehouse', $warehouse)
                        ->where('item_id',$erp_csi_data->item_id)
                        ->where('product',$erp_csi_data->product)->update([
                            'ordered_qty'       => (int)$erp_csi_data->qtyordered,
                            'is_recycle'        => $erp_csi_data->recycle
                        ]);

                        // update data cutting
                        // $data_is_exists->update($data_for_update);
                    }
                    // else{
                    //     // insert table info status update
                    //     InfoSync::FirstOrCreate([
                    //         'type' => 'update',
                    //         'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                    //         'style' => $is_exists->style,
                    //         'articleno' => $is_exists->articleno,
                    //         'po_buyer' => $is_exists->po_buyer,
                    //         'documentno' => $is_exists->documentno,
                    //         'desc' => 'update is_recycle',
                    //         'factory_id' => $warehouse,
                    //     ]);

                    //     $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                    //     $data_is_exists->update($data_for_update);
                    // }
                    DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                        ->where('documentno', $erp_csi_data->documentno)
                        ->where('style', $erp_csi_data->style)
                        ->where('po_buyer', $erp_csi_data->po_buyer)
                        ->where('articleno', $erp_csi_data->kst_articleno)
                        ->where('size_finish_good', $erp_csi_data->size_fg)
                        ->where('size_category', $size_category)
                        ->where('part_no', $erp_csi_data->part_no)
                        ->where('warehouse', $warehouse)
                        ->where('item_id',$erp_csi_data->item_id)
                        ->where('product',$erp_csi_data->product)->update([
                            'is_recycle'        => $erp_csi_data->recycle
                        ]);

                    InfoSync::FirstOrCreate([
                        'type' => 'update',
                        'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                        'style' => $is_exists->style,
                        'articleno' => $is_exists->articleno,
                        'po_buyer' => $is_exists->po_buyer,
                        'documentno' => $is_exists->documentno,
                        'desc' => 'update is_recycle',
                        'factory_id' => $warehouse,
                    ]);
                } else {

                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => (int)$erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    InfoSync::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data)
            {
                $check_count_data = 0;

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } elseif($erp_csi_data->m_warehouse_id == '1000048') {
                        $warehouse_erp = 3;
                        //UPDATE FOR AOI 3
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();

                    // insert table info
                    InfoSync::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            return response()->json('Sync Success . . . ');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json('Sync Field . . . ');
        }
    }

    // action delete prioritas
    public function deletedAll(Request $request)
    {
        $getplan = CuttingPlan::where('cutting_date',$request->cutting_date)
            ->where('style',$request->style)
            ->where('articleno',$request->articleno)
            ->where('size_category',$request->size_category)
            ->where('factory_id',Auth::user()->factory_id)
            ->where('queu',$request->queu)
            ->whereNull('deleted_at')->first();

        try {
            if($getplan->header_id != null && $getplan->is_header) {
                $get_plan_id = CuttingPlan::where('header_id', $getplan->header_id)
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->toArray();

                DetailPlan::whereIn('id_plan', $get_plan_id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);

                DB::table('data_cuttings')
                    ->whereIn('plan_id', $get_plan_id)
                    ->update([
                        'queu' => null,
                        'plan_id' => null,
                    ]);

                CuttingPlan::whereIn('id', $get_plan_id)
                    ->update([
                        'deleted_at' => Carbon::now(),
                        'is_combine' => null,
                        'is_header' => null,
                        'header_id' => null,
                    ]);
            } else {
                DetailPlan::where('id_plan', $getplan->id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);

                DB::table('data_cuttings')
                    ->where('plan_id', $getplan->id)
                    ->update([
                        'queu' => null,
                        'plan_id' => null,
                    ]);

                CuttingPlan::where('id', $getplan->id)
                    ->update([
                        'deleted_at' => Carbon::now(),
                        'is_combine' => null,
                        'is_header' => null,
                        'header_id' => null,
                    ]);
            }

        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
        }
    }

    // datatable list po
    public function getPO(Request $request)
    {
        $data = DB::table('ns_detail_po_2')
            ->where('cutting_date',$request->cutting_date)
            ->where('style',$request->style)
            ->where('articleno',$request->articleno)
            ->where('size_category',$request->size_category)
            ->where('queu',$request->queu)
            ->where('top_bot',$request->top_bot);

        return DataTables::of($data)
        ->addColumn('checkbox',function($data){
            if ($data->queu==null) {
                return '<input type="checkbox" class="setpo" name="selector[]" id="Inputselector" value="'.$data->po_buyer.'" data-stat="'.$data->statistical_date.'" data-qty="'.$data->qty.'" >';
            }else{
                return '';
            }
        })
        ->editColumn('queu',function($data){
                return $data->queu;
        })
        ->rawColumns(['checkbox','queu'])
        ->make(true);
    }

    // action update queue pertama kali case partial
    public function partialIn(Request $request)
    {
        $data = $request->data;
        $pcd = $request->date;
        $style = $request->style;
        $article = $request->article;
        $sizectg = $request->sizectg;
        $queue = $request->queue;
        $mo_created = $request->mo_created;
        $color = $request->color;
        $material = $request->material;
        $color_code = $request->color_code;
        $top_bot = $request->top_bot;
        $season = $request->season;
        $factory_id = Auth::user()->factory_id;
        $get_top_bot = $request->gtopbot;

        $cekqueu = CuttingPlan::where('cutting_date',$pcd)
            ->where('queu',$queue)
            ->where('factory_id',$factory_id)
            ->whereNull('deleted_at')
            ->count();

        try {
            if ($cekqueu==0) {

                switch ($request->gtopbot) {
                case '2':
                    $set_tb = $request->top_bot;
                    break;

                case '1':
                    $getTB= DB::table('data_cuttings')
                        ->select('style')
                        ->where('cutting_date',$pcd)
                        ->where('style','LIKE',$style.'%')
                        ->where('articleno',$article)
                        ->where('size_category',$sizectg)
                        ->where('warehouse',$factory_id)
                        ->groupBy('style')
                        ->first();

                    $set_tb = substr($getTB->style, -3,3);
                    break;

                default:
                    $set_tb = null;
                    break;
                }

                CuttingPlan::FirstOrCreate([
                    'cutting_date'=>$pcd,
                    'style'=>$style,
                    'articleno'=>$article,
                    'size_category'=>$sizectg,
                    'queu'=>$queue,
                    'remark'=>'partial',
                    'mo_created_at'=>$mo_created,
                    'color'=>$color,
                    'material'=>$material,
                    'color_code'=>$color_code,
                    'user_id'=>Auth::user()->id,
                    'top_bot'=>$set_tb,
                    'season'=>$season,
                    'created_at'=>Carbon::now(),
                    'factory_id'=>$factory_id,
                    'is_combine' => false,
                    'is_header' => true
                ]);

                $getidplan = CuttingPlan::where('cutting_date',$pcd)
                    ->where('style',$style)
                    ->where('articleno',$article)
                    ->where('size_category',$sizectg)
                    ->where('queu',$queue)
                    ->where('factory_id',$factory_id)
                    ->whereNull('deleted_at')
                    ->first();

                foreach ($data as $dt) {

                    $gp = DB::table('ns_detail_po_2')
                        ->where('cutting_date',$pcd)
                        ->where('style',$style)
                        ->where('articleno',$article)
                        ->where('size_category',$sizectg)
                        ->where('factory_id',$factory_id)
                        ->where('po_buyer',$dt['pobuyer'])
                        ->whereNull('queu')
                        ->first();

                    DetailPlan::FirstOrCreate([
                        'id_plan'=>$getidplan->id,
                        'po_buyer'=>$dt['pobuyer'],
                        'statistical_date'=>$dt['stat_date'],
                        'queu'=>$queue,
                        'qty'=>$dt['qty'],
                        'job'=>$gp->job_no,
                        'destination'=>$gp->destination,
                        'created_at'=>carbon::now(),
                    ]);

                    DB::table('data_cuttings')
                        ->where('cutting_date',$pcd)
                        ->where('warehouse',$factory_id)
                        ->where('size_category',$sizectg)
                        ->where('top_bot',$top_bot)
                        ->where('po_buyer',$dt['pobuyer'])
                        ->update([
                            'queu' => $queue,
                            'plan_id' => $getidplan->id
                        ]);
                }

                $data_response = [
                    'status' => 200,
                    'output' => 'Insert queue success !'
                ];
            }else{
                $data_response = [
                    'status' => 422,
                    'output' => 'Queue already axist'
                ];
            }
        } catch (Exception $e) {
            db::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'Insert queue failed'
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    // action update queue pertama kali case normal // non partial
    public function allIn(Request $request)
    {
        $factory_id = Auth::user()->factory_id;

        $cekplan = CuttingPlan::where('cutting_date',$request->cutting_date)
            ->where('queu',$request->queu)
            ->where('factory_id',$factory_id)
            ->whereNull('deleted_at')
            ->first();

        try {
            if (is_null($cekplan)) {

                switch ($request->gtopbot) {
                case '2':
                    $set_tb = $request->top_bot;
                    break;

                case '1':
                    $getTB= DB::table('data_cuttings')
                        ->select('style')
                        ->where('cutting_date',$request->cutting_date)
                        ->where('style','LIKE',$request->style.'%')
                        ->where('articleno',$request->articleno)
                        ->where('size_category',$request->size_category)
                        ->where('warehouse',$factory_id)
                        ->groupBy('style')
                        ->first();

                    $set_tb = substr($getTB->style, -3,3);
                    break;

                default:
                    $set_tb = null;
                    break;
                }

                CuttingPlan::FirstOrCreate([
                    'cutting_date'=>$request->cutting_date,
                    'style'=>$request->style,
                    'articleno'=>$request->articleno,
                    'size_category'=>$request->size_category,
                    'queu'=>$request->queu,
                    'remark'=>'all',
                    'user_id'=>Auth::user()->id,
                    'mo_created_at'=>$request->mo_created,
                    'color'=>$request->color,
                    'material'=>$request->material,
                    'color_code'=>$request->color_code,
                    'top_bot'=>$set_tb,
                    'season'=>$request->season,
                    'created_at'=>Carbon::now(),
                    'factory_id'=>$factory_id,
                    'is_combine' => false,
                    'is_header' => true
                ]);

                $getplan = CuttingPlan::where('cutting_date',$request->cutting_date)
                    ->where('style',$request->style)
                    ->where('articleno',$request->articleno)
                    ->where('queu',$request->queu)
                    ->where('factory_id',$factory_id)
                    ->whereNull('deleted_at')
                    ->first();

                $getPo = DB::table('ns_detail_po_2')
                    ->where('cutting_date',$request->cutting_date)
                    ->where('style',$request->style)
                    ->where('articleno',$request->articleno)
                    ->where('size_category',$request->size_category)
                    ->where('factory_id',$factory_id)
                    ->whereNull('queu')
                    ->get();

                foreach ($getPo as $gp) {
                    DetailPlan::FirstOrCreate([
                        "po_buyer"=>$gp->po_buyer,
                        "id_plan"=>$getplan->id,
                        "queu"=>$request->queu,
                        'statistical_date'=>$gp->statistical_date,
                        'qty'=>$gp->qty,
                        'job' => $gp->job_no,
                        'destination' => $gp->destination,
                    ]);

                    DB::table('data_cuttings')
                        ->where('cutting_date',$request->cutting_date)
                        ->where('size_category',$request->size_category)
                        ->where('top_bot',$request->top_bot)
                        ->where('po_buyer',$gp->po_buyer)
                        ->where('articleno',$getplan->articleno)
                        ->where('warehouse',$factory_id)
                        ->update([
                            'queu' => $request->queu,
                            'plan_id' => $getplan->id
                        ]);
                }

                $this->set_info($request->cutting_date);

                $data_response = [
                    'status' => 200,
                    'output' => 'Insert queue success !'
                ];

            } else {
                $data_response = [
                    'status' => 422,
                    'output' => 'Queue already exists !!!'
                ];
            }
        } catch (Exception $er) {
            db::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'Insert queue failed'
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    // action update queue sebelumnya
    public function edQueu(Request $request)
    {
        $factory_id = Auth::user()->factory_id;
        $id = $request->id;
        $date = $request->date;
        $queu = $request->queu;

        $cekplan = CuttingPlan::where('cutting_date',$date)
            ->where('queu',$queu)
            ->where('factory_id',$factory_id)
            ->whereNull('deleted_at')
            ->first();

        try {
            if (is_null($cekplan)) {

                CuttingPlan::where('id',$id)
                    ->whereNull('deleted_at')
                    ->update([
                        'queu'=>$queu,
                    ]);

                DetailPlan::where('id_plan',$id)
                    ->whereNull('deleted_at')
                    ->update([
                        'queu'=>$queu,
                        'id_plan'=>$id
                    ]);

                $gpc = CuttingPlan::where('id',$id)
                ->whereNull('deleted_at')
                ->first();

                $gpo = DetailPlan::where('id_plan',$id)
                    ->whereNull('deleted_at')
                    ->get();

                foreach ($gpo as $gp) {
                    DB::table('data_cuttings')
                        ->where('cutting_date',$date)
                        ->where('po_buyer',$gp->po_buyer)
                        ->where('articleno',$gpc->articleno)
                        ->update([
                            'queu'=>$queu,
                            'plan_id'=>$id
                        ]);
                }


                $data_response = [
                    'status' => 200,
                    'output' => 'Edit queue success !'
                ];
            } else {
                $data_response = [
                    'status' => 422,
                    'output' => 'Queue already exists !!!'
                ];
            }
        } catch (Exception $er) {
            db::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);

            $data_response = [
                'status' => 422,
                'output' => 'Edit queue failed'
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    // action split set antara top dan bottom etc
    public function splitTopBot(Request $request)
    {
        $pcd = $request->pcd;
        $style = $request->style;
        $article = $request->article;
        $size_category = $request->size_category;
        $factory_id = Auth::user()->factory_id;

        try {
            DB::begintransaction();
            $getDC = DB::table('data_cuttings')
                ->select('style')
                ->where('cutting_date',$pcd)
                ->where('style','LIKE',$style.'%')
                ->where('articleno',$article)
                ->where('size_category',$size_category)
                ->where('warehouse',$factory_id)
                ->groupBy('style')
                ->get();

            foreach ($getDC as $dc) {
                $sub = substr($dc->style, -3,3);
                if ($sub=='TOP') {

                    DB::table('data_cuttings')
                        ->where('cutting_date',$pcd)
                        ->where('style',$dc->style)
                        ->where('articleno',$article)
                        ->where('size_category',$size_category)
                        ->where('warehouse',$factory_id)
                        ->update(['top_bot'=>'TOP']);
                }else{
                    DB::table('data_cuttings')
                        ->where('cutting_date',$pcd)
                        ->where('style',$dc->style)
                        ->where('articleno',$article)
                        ->where('size_category',$size_category)
                        ->where('warehouse',$factory_id)
                        ->update(['top_bot'=>'BOT']);
                }
            }

            $data_response = [
                'status' => 200,
                'output' => 'Split Top and Bottom success'
            ];

            DB::commit();
       } catch (Exception $e) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
            $data_response = [
                'status' => 422,
                'output' => 'Split Top and Bottom field'
            ];
       }

       return response()->json(['data' => $data_response]);
    }

    // action delete
    public function delpart(Request $request)
    {
        $cekplan = CuttingPlan::where('cutting_date',$request->pcd)
            ->where('style',$request->style)
            ->where('articleno',$request->article)
            ->where('queu',$request->queu)
            ->where('factory_id',Auth::user()->factory_id)
            ->whereNull('deleted_at')
            ->first();

        if ($cekplan!=null) {
            DetailPlan::where('id_plan',$cekplan->id)
                ->update([
                    'deleted_at'=>Carbon::now()
                ]);

            DB::table('data_cuttings')
                ->where('po_buyer',$request->po_buyer)
                ->update([
                    'queu' => null,
                    'plan_id' => null
                ]);

            CuttingPlan::where('id',$cekplan->id)
                ->update([
                    'deleted_at' => Carbon::now(),
                    'is_combine' => null,
                    'is_header' => null,
                    'header_id' => null,
                ]);
        }
    }

    // action untuk menggabungkan top dan bottom
    public function margeTopBot(Request $request){

        $pcd = $request->pcd;
        $style = $request->style;
        $article = $request->article;
        $size_ctg = $request->size_category;
        $factory_id = Auth::user()->factory_id;

        try {
           DB::begintransaction();
            $getDC = DB::table('data_cuttings')
                ->select('style')
                ->where('cutting_date',$pcd)
                ->where('style','LIKE',$style.'%')
                ->where('articleno',$article)
                ->where('size_category',$size_ctg)
                ->where('warehouse',$factory_id)
                ->groupBy('style')
                ->get();

            foreach ($getDC as $dc) {
                $sub = substr($dc->style, -3,3);
                if ($sub=='TOP') {
                    DB::table('data_cuttings')
                        ->where('cutting_date',$pcd)
                        ->where('style',$dc->style)
                        ->where('articleno',$article)
                        ->where('size_category',$size_ctg)
                        ->where('warehouse',$factory_id)
                        ->update([
                            'top_bot' => null
                        ]);

                } else {

                    DB::table('data_cuttings')
                        ->where('cutting_date',$pcd)
                        ->where('style',$dc->style)
                        ->where('articleno',$article)
                        ->where('size_category',$size_ctg)
                        ->where('warehouse',$factory_id)
                        ->update([
                            'top_bot' => null
                        ]);
                }
            }

            $data_response = [
                'status' => 200,
                'output' => 'Marge Top and Bottom success'
            ];
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
            $data_response = [
                'status' => 422,
                'output' => 'Marge Top and Bottom field'
            ];
        }

        return response()->json(['data' => $data_response]);
    }

    // view modal combine
    public function modalCombinePlan(Request $request, $id)
    {
        if(request()->ajax())
        {
            $get_plan = CuttingPlan::where('id', $id)->first();

            $obj = new StdClass();
            $obj->id = $id;

            if($get_plan != null) {
                $obj->style = $get_plan->style;
                $obj->article = $get_plan->article;
            } else {
                $obj->style = null;
                $obj->article = null;
            }

            return response()->json($obj,200);
        }
    }

    // datatable combine plan
    public function getDataCombinePlan(Request $request){
        if(request()->ajax())
        {
            $date_cutting = is_null($request->cutting_date) ? null : date('Y-m-d', strtotime($request->cutting_date));
            $combine_plan_id = $request->combine_planning_id;
            $check_plan_exists = CuttingPlan::where('header_id', $combine_plan_id)->whereNull('deleted_at')->pluck('id')->toArray();

            // dd($check_plan_exists);

            if (!empty($date_cutting)) {
                $data = DB::table('jaz_cutting_plan_new_update_3')
                    ->where('cutting_date',$date_cutting)
                    ->where('factory_id',Auth::user()->factory_id)
                    ->where(function($where_raw) use($check_plan_exists) {
                        $where_raw->whereIn('plan_id', $check_plan_exists)
                            ->orWhere('queu', null);
                    })
                    ->orderBy('style');

            } else {
                $data = array();
            }

            return DataTables::of($data)
            ->editColumn('style',function($data){
                return $data->style.' '.$data->top_bot;
            })
            ->editColumn('size_category',function($data){
                if ($data->size_category=='A') {
                    return 'Asian';
                } elseif ($data->size_category=='J') {
                    return 'Japan';
                }else{
                    return 'Intl';
                }
            })
            ->editColumn('status',function($data){
                if($data->plan_id != null) {
                    return '<span class="badge badge-primary">Combine</span>';
                }
            })
            ->addColumn('action',function($data){
                $check_plan = CuttingPlan::where('id', $data->plan_id)->whereNull('deleted_at')->first();
                if($check_plan == null) {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'combine_plan_action' => '#',
                    ]);
                } else {
                    return view('request_marker.prioritas_plan._action', [
                        'model' => $data,
                        'cancel_combine' => route('prioritasPlan.canceCombinePlanAction', $data->plan_id),
                    ]);
                }
            })
            ->rawcolumns(['status','action'])
            ->make(true);
        }
    }

    // action combine plan
    public function combinePlanAction(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            $style = $request->style;
            $articleno = $request->articleno;
            $size_category = $request->size_category;
            $queu = $request->queu;
            $plan_id = $request->plan_id;
            $mo_created = $request->mo_created;
            $color = $request->color;
            $color_code = $request->color_code;
            $material = $request->material;
            $top_bot = $request->top_bot;
            $season = $request->season;
            $factory_id = $request->factory_id;
            $combine_planning_id = $request->combine_planning_id;

            try {
                switch ($request->gtopbot) {
                case '2':
                    $set_tb = $request->top_bot;
                    break;

                case '1':
                    $getTB= DB::table('data_cuttings')
                        ->select('style')
                        ->where('cutting_date',$request->cutting_date)
                        ->where('style','LIKE',$request->style.'%')
                        ->where('articleno',$request->articleno)
                        ->where('size_category',$request->size_category)
                        ->where('warehouse',$factory_id)
                        ->groupBy('style')
                        ->first();

                    $set_tb = substr($getTB->style, -3,3);
                    break;

                default:
                    $set_tb = null;
                    break;
                }

                CuttingPlan::FirstOrCreate([
                    'cutting_date'=>$request->cutting_date,
                    'style'=>$request->style,
                    'articleno'=>$request->articleno,
                    'size_category'=>$request->size_category,
                    'queu'=>0,
                    'remark'=>'combine',
                    'user_id'=>Auth::user()->id,
                    'mo_created_at'=>$request->mo_created,
                    'color'=>$request->color,
                    'material'=>$request->material,
                    'color_code'=>$request->color_code,
                    'top_bot'=>$set_tb,
                    'season'=>$request->season,
                    'created_at'=>Carbon::now(),
                    'factory_id'=>$factory_id,
                    'is_combine' => true,
                    'is_header' => false,
                    'header_id' => $combine_planning_id
                ]);

                $getplan = CuttingPlan::where('cutting_date',$request->cutting_date)
                    ->where('style',$request->style)
                    ->where('articleno',$request->articleno)
                    ->where('queu', 0)
                    ->where('factory_id',$factory_id)
                    ->whereNull('deleted_at')
                    ->first();

                $getPo = DB::table('ns_detail_po_2')
                    ->where('cutting_date',$request->cutting_date)
                    ->where('style',$request->style)
                    ->where('articleno',$request->articleno)
                    ->where('size_category',$request->size_category)
                    ->where('factory_id',$factory_id)
                    ->whereNull('queu')
                    ->get();

                foreach ($getPo as $gp) {
                    DetailPlan::FirstOrCreate([
                        "po_buyer"=>$gp->po_buyer,
                        "id_plan"=>$getplan->id,
                        "queu"=>0,
                        'statistical_date'=>$gp->statistical_date,
                        'qty'=>$gp->qty,
                        'job' => $gp->job_no,
                        'destination' => $gp->destination,
                    ]);

                    DB::table('data_cuttings')
                        ->where('cutting_date',$request->cutting_date)
                        ->where('size_category',$request->size_category)
                        ->where('top_bot',$request->top_bot)
                        ->where('po_buyer',$gp->po_buyer)
                        ->where('warehouse',$factory_id)
                        ->update([
                            'queu' => 0,
                            'plan_id' => $getplan->id
                        ]);

                }

                CuttingPlan::where('id', $combine_planning_id)
                    ->update([
                        'is_combine' => true,
                        'header_id' => $combine_planning_id
                    ]);

            } catch (Exception $er) {
                db::rollback();
                $message = $er->getMessage();
                ErrorHandler::db($message);

                return response()->json('Combine Plan Gagal!',422);
            }

            return response()->json(200);
        }
    }

    // action cancel combine plan
    public function canceCombinePlanAction(Request $request, $id)
    {
        if(request()->ajax())
        {
            $combine_plan_id = $request->combine_planning_id;
            try {
                DB::table('data_cuttings')
                    ->where('plan_id', $id)
                    ->update([
                        'queu' => null,
                        'plan_id' => null
                    ]);

                CuttingPlan::where('id', $id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);

                DetailPlan::where('id_plan', $id)
                    ->update([
                        'deleted_at' => Carbon::now()
                    ]);

                $check_cutting_plan = CuttingPlan::where('header_id', $combine_plan_id)
                    ->whereNull('deleted_at')
                    ->count();

                if($check_cutting_plan < 2) {
                    CuttingPlan::where('id', $combine_plan_id)
                        ->update([
                            'is_combine' => false,
                            'is_header' => true,
                            'header_id' => null
                        ]);
                }

            } catch (Exception $er) {
                db::rollback();
                $message = $er->getMessage();
                ErrorHandler::db($message);

                return response()->json('Combine Plan Gagal!',422);
            }

            return response()->json(200);
        }
    }

    // private function

    // action update database infos
    private function set_info($pcd){
        $chek = 0;
        $cekdatcut = DB::table('jaz_cutting_plan_new')
            ->where('cutting_date',$pcd)
            ->get();


        foreach ($cekdatcut as $dc) {
            $cekplan = CuttingPlan::where('cutting_date',$dc->cutting_date)
                ->where('style',$dc->style)
                ->where('articleno',$dc->articleno)
                ->where('size_category',$dc->size_category)
                ->where('factory_id',Auth::user()->factory_id)
                ->count();

            if ($cekplan==1) {
                $chek = $chek;
            }else{
                $chek++;
            }
        }

        if ($chek!=0) {
            InfoSync::FirstOrCreate([
                'type' => "create",
                'cutting_date' => $pcd,
                'style' => "-",
                'articleno' => "-",
                'documentno' => "-",
                'po_buyer' => "-",
                'desc' => "Plan date ".$pcd." create",
                'created_at' => Carbon::now(),
                'is_integrated' => false
            ]);
        }
    }
}
