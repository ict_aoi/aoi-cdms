@extends('adibooster.index')
@section('page-css')
<style type="text/css">

.title-fac{
	color: white;
	font-family:Cooper Black;
	font-size:  40px;
}

body {
    background-color: #333333;
}
.panel-body {
    background-color: #333333;
}

</style>
@endsection
@section('page-header')
<div class="page-header" style="height: 60px;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1px; padding-bottom: 5px;">
        	<center><label class="title-fac">DASHBOARD WIP SUPERMARKET AOI {{ $factory_id }}</label></center>
        	<input type="text" name="factory_id" id="factory_id" class="hidden" value="{{ $factory_id }}">
        </div>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
		    <div class="panel-body" >
		        <div id="chart1" style="width:100%; height:300px;"></div>
		        <div id="chart2" style="width:100%; height:300px;"></div>
		    </div>
		</div>
@endsection

@section('page-js')
<script type="text/javascript">
    Highcharts.chart('chart1', {
        chart: {
            backgroundColor: 'rgba(0,0,0,0)',
            type: 'column',
            height: 280,
        },
        title: {
            text: ' '
        },
        xAxis: {
            categories: [
                    <?php
                        foreach($lines_1 as $line) {
                            echo "'".$line->line_name."', ";
                        }
                    ?>
                ],
            labels: {
                style: {
                    color: '#ffffff',
                    textOutline: false 
                }
            },
            title: {
                style: {
                    color: '#ffffff',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    textOutline: false 
                }            
            }
        },
        yAxis: {
            title: {
                text: 'Total days',
                style: {
                    color: '#ffffff',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    textOutline: false 
                }    
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#ffffff',
                    textOutline: false 
                }
            },
        },
        legend: {
            enabled: true,
            itemStyle: {
                color: '#ffffff'
            }   
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }
        },
        series: [
            {
                name: 'Unready',
                data: [
                    <?php
                        foreach($component_unready_1 as $unready) {
                            echo $unready.', ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }, {
                name: 'Ready',
                data: [
                    <?php
                        foreach($component_ready_1 as $ready) {
                            echo $ready.', ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }, {
                name: 'Min',
                type: 'spline',
                data: [
                    <?php
                        foreach($lines_1 as $line) {
                            echo '1, ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: false,
                },
                marker: {
                    enabled: false
                }
            }, {
                name: 'Max',
                type: 'spline',
                data: [
                    <?php
                        foreach($lines_1 as $line) {
                            echo '3, ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: false,
                },
                marker: {
                    enabled: false
                }
            },
        ],
        colors: ['#ff8c00', '#28B463', '#ED3528', '#4a5cff']
    });

    Highcharts.chart('chart2', {
        chart: {
            backgroundColor: 'rgba(0,0,0,0)',
            type: 'column',
            height: 280,
        },
        title: {
            text: ' '
        },
        xAxis: {
            categories: [
                    <?php
                        foreach($lines_2 as $line) {
                            echo "'".$line->line_name."', ";
                        }
                    ?>
                ],
            labels: {
                style: {
                    color: '#ffffff',
                    textOutline: false 
                }
            },
            title: {
                style: {
                    color: '#ffffff',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    textOutline: false 
                }            
            }
        },
        yAxis: {
            title: {
                text: 'Total days',
                style: {
                    color: '#ffffff',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    textOutline: false 
                }    
            },
            labels: {
                format: '{value}',
                style: {
                    color: '#ffffff',
                    textOutline: false 
                }
            },
        },
        legend: {
            enabled: true,
            itemStyle: {
                color: '#ffffff'
            }   
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }
        },
        series: [
            {
                name: 'Unready',
                data: [
                    <?php
                        foreach($component_unready_2 as $unready) {
                            echo $unready.', ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }, {
                name: 'Ready',
                data: [
                    <?php
                        foreach($component_ready_2 as $ready) {
                            echo $ready.', ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    style: {
                        textOutline: false,
                    },
                }
            }, {
                name: 'Min',
                type: 'spline',
                data: [
                    <?php
                        foreach($lines_2 as $line) {
                            echo '1, ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: false,
                },
                marker: {
                    enabled: false
                }
            }, {
                name: 'Max',
                type: 'spline',
                data: [
                    <?php
                        foreach($lines_2 as $line) {
                            echo '3, ';
                        }
                    ?>
                ],
                dataLabels: {
                    enabled: false,
                },
                marker: {
                    enabled: false
                }
            },
        ],
        colors: ['#ff8c00', '#28B463', '#ED3528', '#4a5cff']
    });

$(document).ready(function(){
     var factory_id = $('#factory_id').val();     	
     setInterval(function(){window.location.href="/adibooster/setting-area/"+factory_id },300000);

});


</script>
@endsection