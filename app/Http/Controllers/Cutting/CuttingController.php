<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;


use App\Models\Cutting;

class CuttingController extends Controller
{
    public function index(){

    	return view('cutting/cutting')->with('user_id',Auth::user()->id);
    }

    public function ajaxDailyCutting(Request $request){
    	$now = Carbon::now();
    	$date = Carbon::parse($now)->format('Y-m-d');
    	$data= DB::table('cutting')
    					->join('cutting_marker','cutting_marker.barcode_id','=','cutting.id_marker')
    					->join('cutting_plans2','cutting_plans2.id','=','cutting_marker.id_plan')
    					->join('users','users.id','=','cutting.user_id')
    					->where('cutting.user_id',$request->user_id)
    					->whereNull('cutting.deleted_at')
    					->where(function($query) use ($date){
    						$query->whereDate('cutting.created_at',$date)
    								->orWhereDate('cutting.finished_at',$date);
    					})
    					->whereNull('cutting_marker.deleted_at')
    					->whereNull('cutting_plans2.deleted_at')
    					->select('cutting.id_marker','cutting_plans2.cutting_date','cutting_plans2.style','cutting_plans2.articleno','cutting_marker.color','cutting.created_at','cutting.finished_at')
    					->orderBy('created_at','desc');
    	return DataTables::of($data)
    						->addColumn('status',function($data){
    							if ($data->finished_at==null) {
    								$badge='<span class="label label-warning label-rounded">ON PROGRESS</span>';
    							}else{
    								$badge='<span class="label label-success label-rounded">FINISHED</span>';
    							}
    							return $badge;
    						})
    						->rawColumns(['status'])
    						->make(true);
    }

    public function ajaxSetStatus(Request $request){
    	$barcode_id = $request->barcodeid;
    	$user_id = Auth::user()->id;
    	$cekuser = Cutting::where('user_id',$user_id)->whereNull('finished_at')->first();
    	if ($cekuser!=null && $cekuser->id_marker!=$barcode_id) {
    		$data_response = [
	            'status' => 422,
	            'output' => 'not finished cutting. Marker ID '.$cekuser->id_marker
	          ];
    	}else if ($cekuser==null || ($cekuser!=null && $cekuser->id_marker==$barcode_id)){
    			try {
		    		db::begintransaction();
		    			$cek = Cutting::where('id_marker',$barcode_id)
		    						->whereNull('deleted_at');
		    			
		    			if ($cek->first()==null) {
		    				$cekmarker = DB::table('cutting_marker')
		    									->select('barcode_id')
		    									->where('barcode_id',$barcode_id)
		    									->whereNull('deleted_at')->first();
		    				if ($cekmarker==null) {
		    					$data_response = [
		                            'status' => 422,
		                            'output' => 'Marker ID not found'
		                          ];
		    				}else{
		    					$dt_in = array(
		    						'id_marker'=>$barcode_id,
									'user_id'=>$user_id,
									'created_at'=>Carbon::now()
		    					);

		    					Cutting::FirstOrCreate($dt_in);
		    					$data_response = [
		                            'status' => 200,
		                            'output' => 'Marker '.$barcode_id.' start to cutting !!'
		                          ];
		    				}
		    			}else if ($cek->first()!=null) {
		    				$cek2 = $cek->whereNull('finished_at')->first();
		    				if ($cek2!=null) {
		    					Cutting::where('id',$cek2->id)->update(['finished_at'=>Carbon::now()]);
		    					$data_response = [
		                            'status' => 200,
		                            'output' => 'Marker '.$barcode_id.' finished cutting !!'
		                          ];
		    				}else{
		    					$data_response = [
		                            'status' => 422,
		                            'output' => 'It is already done'
		                          ];
		    				}
		    			}
		    		db::commit();
		    	} catch (Exception $ex) {
		    		 db::rollback();
		          $message = $ex->getMessage();
		                  \ErrorHandler($message);
		    	}
    	}
    	

    	return response()->json(['data' => $data_response]);
    }
}
 