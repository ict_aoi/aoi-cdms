@extends('layouts.app',['active' => 'scan_component_subcont'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
    .control-label {
        padding-top: 9px !important;
    }
    .margin-atas {
        margin-top: 20px;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Scan Component Subcont In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Scan Component Subcont In</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-12">
                    <center><h5>Input Data</h5></center>
                </div>
            </div>
            <div class="row margin-atas">
                <div class="col-md-3">
                    <input type="text" value="Packingnlist Number" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-7">
                    <select data-placeholder="Select a No Packinglist..." name="packinglist_input" id="packinglist_input" class="form-control select-search">
                        <option value=""></option>
                        @foreach($packinglists as $list)
                            <option value="{{ $list->no_packinglist }}">{{ $list->no_packinglist }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary btn-sm pull-right">Create PL</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="KK No" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="kk_input" name="kk_input" placeholder="#No Data!" />
                </div>
                <div class="col-md-2">
                    <input type="text" value="Style" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="style_input" name="style_input" placeholder="#No Data!" />
                </div>
                <div class="col-md-2">
                    <input type="text" value="PO Buyer" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="po_buyer_input" name="po_buyer_input" placeholder="#No Data!" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="Subcont" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="subcont_input" name="subcont_input" placeholder="#No Data!" />
                </div>
                <div class="col-md-2">
                    <input type="text" value="Process" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-2">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="process_input" name="process_input" placeholder="#No Data!" />
                </div>
                <div class="col-md-4">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <input type="text" value="Scan Barcode Bundle" class="form-control bg-info" readonly="readonly" />
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" style="opacity: 1;" id="input_barcode" name="input_barcode" placeholder="#Scan Barcode" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Proses Terbaru</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="componentTable">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Style | Article</th>
                        <th>Part - Komponen</th>
                        <th>PO</th>
                        <th>Size</th>
                        <th>Cut / Sticker No</th>
                        <th>Qty</th>
                        <th>Polibag</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <a type="button" class="btn btn-primary" id="save_component">Submit</a>
    </div>
</div>
<a href="{{ route('ScanComponentSubcont.dataLocator') }}" id="dataLocatorLink"></a>
<a href="{{ route('ScanComponentSubcont.detailPackinglist') }}" id="detailPackinglistLink"></a>
<a href="{{ route('ScanComponentSubcont.dataPackinglist') }}" id="dataPackinglistLink"></a>
<a href="{{ route('ScanComponentSubcont.dataComponent') }}" id="dataComponentLink"></a>
<a href="{{ route('ScanComponentSubcont.dataComponentPackinglist') }}" id="dataComponentPackinglistLink"></a>
<a href="{{ route('ScanComponentSubcont.scanComponent') }}" id="scanComponentLink"></a>
<a href="{{ route('ScanComponentSubcont.saveComponent') }}" id="saveComponentLink"></a>
@endsection

@section('page-modal')
	@include('component_movement.scan_component_subcont._modal_locator')
	@include('component_movement.scan_component_subcont._modal_scan')
@endsection

@section('page-js')
    <script>
        $(document).ready( function () {

            $(".file-styled").uniform({
                fileButtonClass: 'action btn bg-warning'
            });

            $.extend( $.fn.dataTable.defaults, {
                stateSave: true,
                autoWidth: false,
                autoLength: false,
                processing: true,
                // serverSide: true, //sync
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                }
            });

            $('#locator_check').on('click', function() {
                $('#locatorModal').modal();
                get_data_locator();
            });

            $('#locatorTable').on('click', '#select_locator', function() {
                var id_locator = $(this).data('id');
                var locator_name = $(this).data('name');
                var is_cutting = $(this).data('is_cutting');
                var is_setting = $(this).data('is_setting');
                var is_artwork = $(this).data('is_artwork');

                $("#state").html('');

                $("#state").append($('<option>').attr('value', 'out').text('OUT')).append($('<option>').attr('value', 'in').text('IN'));

                if(is_cutting) {
                    $("#state option[value='in']").remove();
                }

                if(is_setting) {
                    $("#state option[value='out']").remove();
                }

                $('#locator_check').val(locator_name);
                $('#locator_id').val(id_locator);
                $('#barcode_input').focus();

                $('#locatorModal').modal('hide');
            })

            $('#locatorModal').on('hidden.bs.modal', function(){
                // $('#marker_check').focus();  
            });

            $('#scanModal').on('hidden.bs.modal', function(){
                get_data_component_packinglist($('#packinglist_input').val());
            });

            $('#packinglist_input').on('change', function() {
                var packinglist_no = $('#packinglist_input').val();
        
                $.ajax({
                    type: 'get',
                    url : $('#detailPackinglistLink').attr('href'),
                    data: {
                        packinglist_no: packinglist_no,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#kk_input').val(response.kk);
                        $('#style_input').val(response.style);
                        $('#po_buyer_input').val(response.po_buyer);
                        $('#subcont_input').val(response.subcont);
                        get_data_component_packinglist(response.packinglist);
                        $.unblockUI();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });

            $('#scan_barcode').on('click', function() {
                var locator = $('#locator_id').val();
                var state = $('#state').val();
                var packinglist = $('#packinglist_input').val();
                var polibag = $('#polibag_input').val();

                if(!locator){
                    $("#alert_warning").trigger("click", 'Locator belum dipilih!');
                    return false;
                }
                if(!state){
                    $("#alert_warning").trigger("click", 'State belum dipilih!');
                    return false;
                }
                if(!packinglist){
                    $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                    return false;
                }
                if(!polibag){
                    $("#alert_warning").trigger("click", 'No polibag belum diisi!');
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url : $('#dataPackinglistLink').attr('href'),
                    data: {
                        locator: locator,
                        state: state,
                        packinglist: packinglist,
                        polibag: polibag,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#scanModal').modal();
                        get_data_component(response.locator, response.state, response.packinglist, response.polibag);
                        $.unblockUI();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });

            $('#scan_component').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var locator = $('#locator_id').val();
                    var state = $('#state').val();
                    var packinglist = $('#packinglist_input').val();
                    var polibag = $('#polibag_input').val();
                    var barcode_component = $(this).val();
                    
                    if(!locator){
                        $("#alert_warning").trigger("click", 'Locator belum dipilih!');
                        return false;
                    }
                    if(!state){
                        $("#alert_warning").trigger("click", 'State belum dipilih!');
                        return false;
                    }
                    if(!packinglist){
                        $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                        return false;
                    }
                    if(!polibag){
                        $("#alert_warning").trigger("click", 'No polibag belum diisi!');
                        return false;
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url : $('#scanComponentLink').attr('href'),
                        data: {
                            locator: locator,
                            state: state,
                            packinglist: packinglist,
                            polibag: polibag,
                            barcode_component: barcode_component,
                        },
                        beforeSend: function() {
                            $('#scanTable').block({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        success: function(response) {
                            $('#scanTable').unblock();
                            $('#scan_component').val('');
                            $('#scan_component').focus();
                            get_data_component(response.locator, response.state, response.packinglist, response.polibag);
                        },
                        error: function(response) {
                            $('#scanTable').unblock();
                            $('#scan_component').val('');
                            $('#scan_component').focus();
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });

            $('#save_component').on('click', function() {
                var packinglist_no = $('#packinglist_input').val();
                var locator = $('#locator_id').val();
                var state = $('#state').val();

                if(!packinglist_no){
                    $("#alert_warning").trigger("click", 'Packinglist belum dipilih!');
                    return false;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url : $('#saveComponentLink').attr('href'),
                    data: {
                        packinglist_no: packinglist_no,
                        locator: locator,
                        state: state,
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function(response) {
                        $('#kk_input').val('');
                        $('#style_input').val('');
                        $('#po_buyer_input').val('');
                        $('#subcont_input').val('');
                        $('#polibag_input').val('');
                        $('#packinglist_input').val(null).trigger('change');
                        get_data_component_packinglist(response.packinglist);
                        $.unblockUI();
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            })

            $('#barcode_input').keypress(function(event){
                if(event.which == 13) {
                    event.preventDefault();
                    var barcode_input = $(this).val();
                    var locator_id = $('#locator_id').val();
                    var state = $('#state').val();

                    validationInputBarcode(barcode_input, locator_id, state, function() {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
    
                        $.ajax({
                            type: 'post',
                            url: '/scan-component/scan-component',
                            data: {
                                barcode_input: barcode_input,
                                locator_id: locator_id,
                                state: state,
                            },
                            beforeSend: function() {
                                $('#loading_view').block({
                                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: '10px 15px',
                                        color: '#fff',
                                        width: 'auto',
                                        '-webkit-border-radius': 2,
                                        '-moz-border-radius': 2,
                                        backgroundColor: '#333'
                                    }
                                });
                            },
                            success: function(response) {
                                $('#loading_view').unblock();
                                $('#barcode_input').val('').focus();
                                appendBarcode(response);
                            },
                            error: function(response) {
                                $('#loading_view').unblock();
                                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                                $('#barcode_input').val('').focus();
                            }
                        });
                    });
                }
            });
        });

        function get_data_locator() {       
            var table = $('#locatorTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: $('#dataLocatorLink').attr('href'),
                },
                fnCreatedRow: function (row, data, index) {
                    var info = table.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'locator_name', name: 'locator_name', searchable:true, visible:true, orderable:false},
                    {data: 'action', name: 'action', searchable:false, orderable:false},
                ],
            });

            $('#tableTable').on('click','.ignore-click', function() {
                return false;
            });
        }

        function get_data_component(locator, state, packinglist, polibag) {
            var table_component = $('#scanTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: $('#dataComponentLink').attr('href'),
                    data: {
                        locator: locator,
                        state: state,
                        packinglist: packinglist,
                        polibag: polibag
                    }
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: 'bundle_id', name: 'bundle_id', searchable:true, visible:true, orderable:false},
                    {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                    {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                    {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                    {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                    {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                    {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false},
                    {data: 'action', name: 'action', searchable:false, orderable:false},
                ],
            });

            $('#scanTable').on('click','.ignore-click', function() {
                return false;
            });
        }

        function get_data_component_packinglist(packinglist) {
            var table_component = $('#componentTable').DataTable({
                destroy: true,
                bFilter: false,
                paging: false,
                ordering: false,
                info: false,
                ajax: {
                    url: $('#dataComponentPackinglistLink').attr('href'),
                    data: {
                        packinglist: packinglist,
                    }
                },
                columnDefs: [
                    {
                        className: 'dt-center'
                    },
                    {
                        targets: 2,
                        className: 'text-center',
                    }
                ],
                columns: [
                    {data: 'bundle_id', name: 'bundle_id', searchable:true, visible:true, orderable:false},
                    {data: 'style', name: 'style', searchable:true, visible:true, orderable:false},
                    {data: 'komponen_name', name: 'komponen_name', searchable:true, visible:true, orderable:false},
                    {data: 'poreference', name: 'poreference', searchable:true, visible:true, orderable:false},
                    {data: 'size', name: 'size', searchable:true, visible:true, orderable:false},
                    {data: 'cut_num', name: 'cut_num', searchable:true, visible:true, orderable:false},
                    {data: 'qty', name: 'qty', searchable:true, visible:true, orderable:false},
                    {data: 'no_polybag', name: 'no_polybag', searchable:false, orderable:false},
                ],
            });

            $('#componentTable').on('click','.ignore-click', function() {
                return false;
            });
        }

        function hapus(url) {

            var locator = $('#locator_id').val();
            var state = $('#state').val();
            var packinglist = $('#packinglist_input').val();
            var polibag = $('#polibag_input').val();
            
            if(!locator){
                $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
                return false;
            }
            if(!state){
                $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
                return false;
            }
            if(!packinglist){
                $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
                return false;
            }
            if(!polibag){
                $("#alert_warning").trigger("click", 'Terjadi kesalahan parsing data!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url : url,
                data: {
                    locator: locator,
                    state: state,
                    packinglist: packinglist,
                    polibag: polibag
                },
                beforeSend: function() {
                    $('#scanTable').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    $("#alert_success").trigger("click",'komponen berhasil di hapus');
                    get_data_component(response.locator, response.state, response.packinglist, response.polibag);
                },
                error: function(response) {
                    $('#scanTable').unblock();
                    $('#scan_component').val('');
                    $('#scan_component').focus();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }

        function appendBarcode(data) {
            $('#lastActivityTable > tbody').prepend(
                $('<tr>').append(
                    $('<td>').text(data.barcode_id)
                ).append(
                    $('<td>').text(data.style+' | '+data.article)
                ).append(
                    $('<td>').text(data.part+' - '+data.komponen_name)
                ).append(
                    $('<td>').text(data.po_buyer)
                )
                .append(
                    $('<td>').text(data.size)
                ).append(
                    $('<td>').text(data.cut+' / '+data.sticker_no)
                ).append(
                    $('<td>').text(data.qty)
                ).append(
                    $('<td>').text(data.process)
                ).append(
                    $('<td>').text(data.status)
                ).append(
                    $('<td>').text(data.loc_dist)
                )
            )
        }

        function validationInputBarcode(barcode_input, locator_id, state, callbackFucntion) {
            if(!locator_id) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            if(!state) {
                $("#alert_warning").trigger("click", 'Set locator terlebih dahulu');
                $('#barcode_input').val('').focus();
                return false;
            }

            callbackFucntion(barcode_input, locator_id, state);
        }

    </script>
@endsection