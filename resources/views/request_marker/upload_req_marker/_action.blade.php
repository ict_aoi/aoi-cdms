<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="#" class="btn-detrat" id="btn-detrat" data-id="{{ $data->id }}" onclick="detrat(this);" ><i class="icon-list"></i> Detail Ratio</a></li>
            @endif
            @if (isset($check_fb))
                <li><a href="#" onclick="getCheckFB('{!! $check_fb !!}')" ><i class="icon-file-text"></i> Check FB</a></li>
            @endif
            @if (isset($check_alokasi))
                <li><a href="#" onclick="getCheckAlokasi('{!! $check_alokasi !!}')" ><i class="icon-download"></i> Check Alokasi</a></li>
            @endif
            @if (isset($delete))
                <li><a href="#" class="btn-delrat" onclick="delrat(this);" id="btn-delrat" data-id="{{ $data->id }}"><i class="icon-trash"></i> Delete</a></li>
            @endif
        </ul>
    </li>
</ul>