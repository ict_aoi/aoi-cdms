@extends('layouts.app',['active' => 'report_fabric_width'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Laporan Penerimaan Fabrik</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Laporan Penerimaan Fabrik</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Masukkan Tanggal Cutting" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" type="submit">Pilih</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
        </form>
        <button type="button" class="btn btn-success btn-sm pull-right add-margin-right" id="download_report"><i class="icon-download"></i> Download</button>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
						<th>Queue</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO Buyer</th>
                        <th>Action</th>
                        {{-- <th>Material</th>
                        <th>Color</th>
                        <th>Part No</th>
                        <th>CSI Qty</th>
                        <th>SSP Qty</th>
                        <th>Over Consum</th>
                        <th>Already Prepared</th>
                        <th>WHS Balance</th>
                        <th>Total Balance</th>
                        <th>Remark</th> --}}
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<form action="{{ route('reportFabric.downloadReport') }}" method="post" target="_blank" id="download_report_submit">
	@csrf
	{{-- <input id="data_download" name="data_download" type="hidden" value="[]"> --}}
	<input id="data_cutting_date" name="data_cutting_date" type="hidden" value="">
	<div class="" style="">
		<button type="submit" class="btn hidden">download</button>
	</div>
</form>

@endsection

@section('page-modal')
	@include('request_fabric.report_fabric._detail_modal')
	@include('request_fabric.report_fabric._detail_modal_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/report_fabric.js') }}"></script>
@endsection
