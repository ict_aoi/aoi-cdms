<!-- MODAL ADD -->
<div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('masterparam.addDetailProcess') }}" id="form-add">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> ADD NEW DETAIL PROCESS</span> <!-- title -->
                          	</legend>

                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Detail Process Name:</label>
                              	<div class="col-lg-9">
									  <textarea class="form-control" name="detail_process_name" id="detail_process_name" cols="30" rows="3" required></textarea>
                              	</div>
                          	</div>
                          	<div class="form-group">
                              	<label class="col-lg-3 control-label text-semibold">Gsd Name:</label>
                              	<div class="col-lg-9">
									  <textarea class="form-control" name="gsd_name" id="gsd_name" cols="30" rows="3" required></textarea>
                              	</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Process:</label>
								<div class="col-lg-9">
									<select data-placeholder="Select a state" name="process_id" id="process_id" class="form-control select-search">
										<option value=""></option>
										@foreach ($process as $item)
											<option value="{{$item->id}}">{{$item->process_name}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-3 control-label text-semibold">Column Report RnD:</label>
								<div class="col-lg-9">
									<select class="form-control select2 select-column-name" name="id_column_report" id="id_column_report">
										<option value=""></option>
									</select>
								</div>
							</div>
							<div class="form-group hidden" id="dvColumnReport2">
								<label class="col-lg-3 control-label text-semibold">Column Report RnD Type 2:</label>
								<div class="col-lg-9">
									<select class="form-control select2 select-column-name2" name="id_column_report2" id="id_column_report2">
										<option value=""></option>
									</select>
								</div>
							</div>
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD -->