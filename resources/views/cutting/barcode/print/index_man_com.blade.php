<?php
    if(isset($ttle)){
    
    }else if(isset($data) && ($data != NULL)){
        $style=trim($data[0]->style);
        $cut_num=$data[0]->cut_num;
        $ttle="BARCODE_".$style."_CUT-".$cut_num;
    }else if(isset($box_id) && ($box_id != NULL)){
        $info=current(current($box_id));
        $style=trim($info->style);
        $cut_num=$info->cut_num;
        $ttle="BOX_".$style."_CUT-".$cut_num;
    }else{
        $style="NONE";
        $cut_num=0;
        $ttle="NO-RESULT_".\Carbon\Carbon::now()->format('Y-m-d_H-i-s');
    }
?>

<html>
    <head>
        <style>
            body,
            html{
                width:420mm;
                margin: 0px;
                padding: 0px;
                font-family:"Times New Roman", Times, serif;
                font-size:10px;
            }
            
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            
            .page {
                    min-height: 297mm;
                    min-width: 420mm;/*420mm;*/
                    padding: 5mm 5mm;
                    margin: 1mm auto;
                    border: 1px #D3D3D3 solid;
                    background: white;
            }
            
            /*.ldscape {
                    -webkit-transform: rotate(-90deg); 
                    -moz-transform:rotate(-90deg);
                    filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            }*/
            
            div {
                margin:0;
                padding:0;
            }
            table {
                border-collapse:collapse;
                margin:0;
                padding:0;
            }
            
            table.outer {
                border:1px dotted #333;
            }
            table,table tr {
                padding:0;
                margin:0;
            }
            table tr td {
                /*border:1px solid #000;*/
                padding:0;
                margin:0;
            }
            td.main {
                padding:0;
                margin:0;
                width:100mm;/*100mm;*/
                height:74mm;/*74mm;*/
            }
            tr.bold td {
                font-weight:bold;
            }

            td.col-1 {
                width:8mm;
            }
            td.col-2 {
                width:23mm;
            }
            td.col-3 {
                width:20mm;
            }
            td.col-4 {
                width:15mm;
            }
            td.col-5 {
                width:34mm;
            }
                    
            td.box-1 {
                width:10mm;
            }
            td.box-2 {
                width:10mm;
            }
            td.box-3 {
                width:10mm;
            }
            td.box-4 {
                width:76mm;
            }
            
            .smallfont {
                font-size:0.9em;
            }
            .evensmallerfont {
                font-size:0.6em;
            }
            
            .center {
                text-align:center;
            }
            .left {
                text-align:left;
            }
            .right {
                text-align:right;
            }
            
            .barcode{
                position:absolute;
                bottom:4mm;
                right:8mm;
                border: 0px solid #00F;
                overflow: hidden; 
                width: 35mm;
                /*height: 10mm;
                -webkit-transform: rotate(-90deg);
                -moz-transform:rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
                filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);*/
                text-align:center;
            }
            td.bc-cell {
                position:relative;
                padding:2mm;
            }
            
            tr.box, tr td.box {
                font-weight:bold;
            }
            td.box-title {
                font-size:1.5em;
            }
            td.small-font {
                font-size:0.9em;
            }
            td.med-large-font {
                font-size:1.5em;
            }
            td.med-large-font-2 {
                font-size:1.3em;
                
            }
            table.box-table {
                font-family:Arial, Helvetica, sans-serif;
                font-size:11px;
            }
            table.box-table, table.box-table tr.content td{
                border:1px solid #333;
            }
            table.box-table tr.content td {
                padding:0 0.5em;
            }
            table.box-table tr.tr-title {
                border:1px solid #333;
                vertical-align: top;
            }
            table.box-table tr.box-header {
                vertical-align: bottom;
            }
            table.box-table tr td.bot-cel {
                border-bottom: 1pt solid #333;
            }

            table { page-break-inside:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
            

            @page {
                /*size: A3;*/
                margin: 0;
            }
            @media print {
                html, body {
                    width: 420mm;/*420mm;*/
                    height: 297mm;        
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                    overflow:hidden;
                }
            }
        </style>
    </head>
<body>
    <?php if(isset($data) && (count($data)>0)){ ?>
        <div class='page'>
            <div class='ldscape'>
                <div>
                    <table>
                        <tr>
                            <?php
                            $i=0;
                            $pg=1;
                            $curpo='';
                            $cursize='';
                            $curnum=0;
                            foreach($data as $tmp){
                                $poreference=trim($tmp->poreference);
                                $size=trim($tmp->size);
                                $job=trim($tmp->job);
                                $bd_op=trim($tmp->bd_op);
                                $cutter=trim($tmp->cutter);
                                $qc_op=trim($tmp->qc_op);
                                $ship_to=trim($tmp->ship_to);
                                $coll=trim($tmp->coll);
                                $sttype=trim($tmp->style).(trim($tmp->set_type)=='0' ? NULL : trim($tmp->set_type)).($tmp->season=='0' ? NULL : "(".($tmp->season).")");
                                
                                $addnote=explode(';',$tmp->add_note);
                            
                                if(true/*$curpo==$poreference && $cursize==$size && $curnum==$tmp->start_num*/){
                                    if($i>3){ 
                                        $i=1;
                                        $pg=$pg+1;
                                        ?></tr><tr><?php
                                    }else{
                                        $i=$i+1;
                                    }
                                    if($pg>3){
                                        $pg=1;
                                        $i=1;
                                    ?></tr></table></div>
                                    </div>
                                    </div>
                                    <div class='page'>
                                        <div class='ldscape'>
                                            <div>
                                                <table>
                                                    <tr><?php
                                    }
                                }else{
                                    $i=0;
                                    $pg=1;
                                    $curpo=$poreference;
                                    $cursize=$size;
                                    $curnum=$tmp->start_num;
                                    ?></tr></table></div>
                                    </div></div>
                                    <div class='page'><div class='ldscape'>
                                        <div><table><tr><?php
                                }
                                
                                $note=trim($tmp->note);
                                if($note==''){
                                    $comp=trim($tmp->komponen);
                                }else{
                                    $comp=trim($tmp->komponen)." (".$note.")";
                                }
                                
                                // FONT SIZE ADJUSTMENT
                                // adjust font-size if character > 26
                                if(strlen($job) > 26){
                                    $jobcls='smallfont';
                                }else{
                                    $jobcls='';
                                }
                                if(strlen($comp) > 30){
                                    $compcls='evensmallerfont';
                                }else if(strlen($comp) > 24){
                                    $compcls='smallfont';
                                }else{
                                    $compcls='';
                                }
                                if(strlen($bd_op) > 26){
                                    $bdcls='smallfont';
                                }else{
                                    $bdcls='';
                                }
                                if(strlen($ship_to) > 17){
                                    $shcls='smallfont';
                                }else{
                                    $shcls='';
                                }
                                if(strlen($coll) > 32){
                                    $clcls='evensmallerfont';
                                }else if(strlen($coll) > 26){
                                    $clcls='smallfont';
                                }else{
                                    $clcls='';
                                }
                                if(strlen($cutter) > 26){
                                    $cutcls='smallfont';
                                }else{
                                    $cutcls='';
                                }
                                if(strlen($qc_op) > 26){
                                    $qccls='smallfont';
                                }else{
                                    $qccls='';
                                }?>
                                    <td class='main'>
                                        <table class='outer'>
                                            @if($tmp->barcode_cdms != null)
                                            <tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'class='center'><?=date('j F',strtotime($tmp->ed_pcd));?></td>
                                                <td class='col-slit'></td>
                                                <td class='col-3 col3-pad' colspan="3" style="padding-top:4px">
                                                    <img style="width:100%;height:32px" id="id<?=$tmp->barcode_cdms;?>" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($tmp->barcode_cdms, 'C128') }}" alt="barcode"/>
                                                </td>
                                                <td class='col-4'></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class='col-1'></td>
                                                @if($tmp->is_recycle == 'Y')
                                                <td class='col-2' style="font-weight:bold;"><i><u>{{'RECYC/AOI'.$tmp->factory_id}}</u></i></td>
                                                @else
                                                <td class='col-2'></td>
                                                @endif
                                                <td class='col-slit'></td>
                                                <td colspan="3" style="text-align:center">{{ isset($tmp) ? $tmp->barcode_cdms : null }}</td>
                                                <td></td>
                                            </tr>
                                            @else
                                            <tr style="margin-top:20px">
                                                <td class='col-1'></td>
                                                <td class='col-2'class='center'><?=date('j F',strtotime($tmp->ed_pcd));?></td>
                                                <td class='col-slit'></td>
                                                <td class='col-3 col3-pad' colspan="3" style="padding-top:4px">
                                                    <div style="width:100%;height:32px;border:none;"></div>
                                                </td>
                                                <td class='col-4'></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'></td>
                                                <td class='col-slit'></td>
                                                <td colspan="3" style="text-align:center">&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            @endif
                                            <tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'>CUT</td>
                                                <td class='col-slit'></td>
                                                <td colspan='2' class='center'><?=$tmp->cut_num;?></td>
                                                <td colspan='3' class='center'><?php echo (isset($addnote[0])&&($addnote[0]!=NULL)&&(trim($addnote[0])!='')) ? trim($addnote[0]) : '' ;?></td>
                                            </tr><tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'>DELV</td>
                                                <td class='col-slit'></td>
                                                <td colspan='2'  class='col3-pad'><?=date('d-M-y',strtotime($tmp->podd));?></td>
                                                <td colspan='3' class='center'><?php echo (isset($addnote[1])&&($addnote[1]!=NULL)&&(trim($addnote[1])!='')) ? trim($addnote[1]) : '' ;?></td>
                                            </tr><tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'>KOMPONEN</td>
                                                <td class='col-slit'></td>
                                                <td colspan='5' class='<?=$compcls;?> col3-pad'>:<i><?=$comp;?></i></td>
                                            </tr><tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'>PART</td>
                                                <td class='col-slit'></td>
                                                <td colspan='5' class='col3-pad'>:<?=$tmp->part;?></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>JOB</td>
                                                <td class='col-slit'></td>
                                                <td colspan='5' class='<?=$jobcls;?>  col3-pad'><?=$job;?></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>STYLE</td>
                                                <td class='col-slit slit slit-top'></td>
                                                <td colspan='3' class='col3-pad stcls'>:<?=$sttype;?></td>
                                                <td class='col-slit slit slit-top'></td>
                                                <td></td>
                                            </tr><tr class='bold'>
                                                <td class='col-1'></td>
                                                <td class='col-2'>PO / SO</td>
                                                <td class='col-slit slit'></td>
                                                <td colspan='3' class='col3-pad'><?=$poreference;?>/<span class='<?=$shcls;?>'><?=$ship_to;?></span></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>ARTICLE</td>
                                                <td class='col-slit slit'></td>
                                                <td colspan='3' class='col3-pad'><?=$tmp->article;?></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>COLL</td>
                                                <td class='col-slit slit'></td>
                                                <td class='<?=$clcls;?> col3-pad' colspan='3'><?=$coll;?></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>SIZE</td>
                                                <td class='col-slit'></td>
                                                <td colspan='3' class='col-3 col3-pad'>:<?=$size;?></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>QTY</td>
                                                <td class='col-slit'></td>
                                                <td colspan='3' class='col-3 col3-pad'>:<?=$tmp->qty;?></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>NO STICKER</td>
                                                <td class='col-slit'></td>
                                                <td colspan='3' class='col-3 col3-pad'>:<?=(($tmp->start_no==$tmp->end_no) ? $tmp->start_no : ($tmp->start_no)."-".($tmp->end_no));?></td>
                                                <td class='col-slit slit'></td>
                                                <td></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>TGL</td>
                                                <td class='col-slit'></td>
                                                <td class='col-3 col3-pad'>:<?=date('j/m');?></td>
                                                <td colspan='4' rowspan='4' class='bc-cell col-4'>
                                                    <div class="barcode">
                                                        <img style="width:130px;height:30px" id="id<?=$tmp->barcode_id;?>" src="data:image/png;base64,{{ DNS1D::getBarcodePNG($tmp->barcode_id, 'C128') }}" alt="barcode"   />
                                                        <span class="barcode_number">{{ isset($tmp) ? $tmp->barcode_id : null }}</span >
                                                    </div>
                                                </td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'></td>
                                                <td class='col-slit slit'></td>
                                                <td class='<?=$bdcls;?> col3-pad'></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'></td>
                                                <td class='col-slit slit'></td>
                                                <td class='<?=$cutcls;?> col3-pad'></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'>&nbsp;</td>
                                                <td class='col-slit slit slit-bottom'></td>
                                                <td class='<?=$qccls;?> col3-pad'></td>
                                            </tr><tr>
                                                <td class='col-1'></td>
                                                <td class='col-2'></td>
                                                <td class='col-slit'></td>
                                                <td class='col-3 col3-pad'></td>
                                                <td class='col-4'></td>
                                                <td class='col-5'></td>
                                                <td class='col-slit'></td>
                                                <td class='col-6'></td>
                                            </tr>
                                        </table>
                                    </td>
                            <?php }
                            if($pg<3){
                                $i=1;
                                $pg+=1; ?>
                        </tr>
                    </table>
                </div>
            <div>
        <table>
            <tr>
                <td>
                    <?php }else{ ?>
                    </tr></table></div></div></div>
                    <div class='page'><div class='ldscape'><div><table><tr><td>
                    <?php
                    $i=1;
                    $pg=1;
                    }
    }else{
        $i=1;
        $pg=1;
        ?><div class='page'><div class='ldscape'><div><table><tr><td><?php
    }
    if(isset($checklist) && ($checklist != NULL)){
        $po = [];
        $size = [];
        $cut = [];
        foreach($data as $dp){
            $po[] = $dp->poreference;
            $size[] = $dp->size;
            $cut[] = $dp->cut_num;
        }
        $po_arr = array_unique($po);
        $size_arr = array_unique($size);
        $cut_arr = array_unique($cut);
        $po_im = "'".implode("','",$po_arr)."'";
        $size_im = "'".implode("','",$size_arr)."'";
        $cut_im = "'".implode("','",$cut_arr)."'";
        $max_komponen = \DB::select(DB::raw("SELECT max(y.x_komp) as max_c from 
        (SELECT x.poreference,x.style,x.set_type,x.size,x.cut_num,count(x.sticker) as x_komp,x.sticker from (
        SELECT poreference,style,set_type,komponen,size,cut_num,concat(start_no,'-',end_no) as sticker from temp_print_ticket where poreference in($po_im)and size in($size_im)and cut_num in($cut_im)ORDER BY style,set_type,size,start_no)x
        GROUP BY x.poreference,x.style,x.set_type,x.size,x.cut_num,x.sticker)y"));
        foreach($checklist as $ticket){
            $info=current($ticket);
            if($pg<4){
                if($i<3){ ?>
                    </td><td>
                <?php }else{
                    $i=1;
                    $pg+=1; ?>
                    </td></tr></table></div>
                    <div><table><tr><td>
                <?php }
            }else{
                if($i<3){ ?>
                    </td><td>
                <?php }else{ ?>
                    </td></tr></table></div></div></div>
                    <div class='page'><div class='ldscape'><div><table><tr><td>
                    <?php
                    $i=1;
                    $pg=1;
                }
            }
            $count=0;
            $style=trim($info->style).(trim($info->set_type)=='0' ? NULL : trim($info->set_type)).($info->season=='0' ? NULL : "(".($info->season).")");
            $po=trim($info->poreference);
            $size=trim($info->size);
            $infocoll=trim($info->coll).(trim($info->article)=='0' ? NULL : " (".trim($info->article).")");
            ?>
            <table class='box-table'>
            <tr class='tr-title'><td colspan='5' class='box small-font'>PT. APPAREL ONE INDONESIA</td><td colspan='5' class='box box-title'>CHECKLIST KOMPONEN</td></tr>
            <tr><td colspan='10'>_</td></tr>
            <tr class='box-header'><td colspan='2' class='box bot-cel'>STYLE</td><td colspan='2' class='box med-large-font bot-cel'>:<?=$style;?></td><td></td><td colspan='4' class="box med-large-font bot-cel">No. Stiker : <?=$info->start_no."-".$info->end_no;?></td><td></td></tr>
            <tr class='box-header'><td colspan='2' class='box bot-cel'>COLOUR</td><td colspan='2' class='box <?=(strlen($infocoll) > 26 ? NULL : 'med-large-font');?> bot-cel'><?=$infocoll;?></td><td colspan='6'></td></tr>
            <tr class='box-header'><td colspan='2' class='box bot-cel'>PO</td><td colspan='2' class='box med-large-font bot-cel'><?=$po;?>/<?=$info->ship_to;?></td><td colspan='6'></td></tr>
            <tr class='center content'><td colspan='4'></td><td colspan='3' class='box med-large-font-2'>CUTTING</td><td colspan='3' class='box med-large-font-2'>DISTRIBUSI</td></tr>
            <tr class='center content box'><td class='box-1'>SIZE</td><td class='box-2'>CUT</td><td class='box-3'>QTY</td><td class='box-4'>KOMPONEN</td><td class='box-3'>JML</td><td class='box-1'>STATUS</td><td class='box-2'>KET</td><td class='box-3'>JML</td><td class='box-1'>STATUS</td><td class='box-2'>KET</td></tr>
            <?php
            foreach($ticket as $box){
                $note=trim($box->note);
                $part_name=trim($box->komponen);
                if($note==''){
                    $comp=$part_name;
                }else{
                    $comp=$part_name." (".$note.")";
                }
                if(strlen($comp) > 30){
                    $cls='smallfont';
                }else{
                    $cls='';
                } 
                ?><tr class='content'>
                    <td class='box-1 center'><?=$box->size;?></td>
                    <td class='box-2 center'><?=$box->cut_num;?></td>
                    <td class='box-3 right'><?=$box->qty;?></td>
                    <td class='box-4 <?=$cls;?>'><?=$comp;?></td>
                    <td class='box-3'></td>
                    <td class='box-1'></td>
                    <td class='box-2'></td>
                    <td class='box-3'></td>
                    <td class='box-1'></td>
                    <td class='box-2'></td>
                </tr><?php
                $count+=1;
            }
            $num = $max_komponen[0]->max_c + 2 >= 25 && $max_komponen[0]->max_c ? 25 : $max_komponen[0]->max_c + 2;
            while($count<$num){
            // while($count<25){
                $count+=1; ?>
                <tr class='content'>
                    <td class='box-1'></td>
                    <td class='box-2'></td>
                    <td class='box-3'></td>
                    <td class='box-4'>-</td>
                    <td class='box-3'></td>
                    <td class='box-1'></td>
                    <td class='box-2'></td>
                    <td class='box-3'></td>
                    <td class='box-1'></td>
                    <td class='box-2'></td>
                </tr>
            <?php } ?></table><?php
            $i+=1;
        }
    } ?>
            </table>
        </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
</body>
</html>
