<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MarkerReleaseDetail extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $table = 'marker_release_details';
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['header_id','balance','release_by','is_confirm','confirm_by','confirm_at','deleted_at','remark','supply_whs','actual_marker_prod'];
    
    public function header()
    {
        return $this->belongsTo('App\Models\MarkerReleaseHeader', 'header_id', 'id');
    }

    public function user_release()
    {
        return $this->belongsTo('App\Models\User', 'release_by', 'id');
    }

    public function user_confirm()
    {
        return $this->belongsTo('App\Models\User', 'confirm_by', 'id');
    }
}
