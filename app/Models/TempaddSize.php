<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TempaddSize extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_add_size';
	protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['season','style','size','cut_num','user_nik','factory_id','created_by','updated_by','deleted_at','deleted_by','set_type','article','coll','qc_op','bd_op','cutter','add_note','bd_lim','input_time','box_lim'];
}
