<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterStyleTransactionsDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_style_transactions_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_style_transactions')->nullable();   
            $table->integer('id_detail_process')->nullable();
            $table->float('ts')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_style_transactions_detail');
    }
}
