<div id="editModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Pilih Komponen QC</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								<span id="detail_style_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Season</strong></span>
							</div>
							<div class="col-md-8">
								<span id="detail_season_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
                        <div class="row">
							<div class="col-md-4">
								<span><strong>Nama Komponen</strong></span>
							</div>
							<div class="col-md-8">
								<span id="detail_komponen_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
            <form method="POST" action="#" accept-charset="UTF-8" id="form_update">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="display-block text-semibold">Check QC</label>
                                <label class="radio-inline">
                                    <input type="radio" name="is_check_qc" id="is_check_qc_yes" value='yes'>
                                    Ya
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="is_check_qc" id="is_check_qc_no" value='no'>
                                    Tidak
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-lg-12 text-semibold">Input Name Alias</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="name_alias" id="name_alias">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="detail_style" id="detail_style">
                    <input type="hidden" name="detail_season" id="detail_season">
                    <input type="hidden" name="detail_komponen" id="detail_komponen">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-update-komponen">Update</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
            </form>
		</div>
	</div>
</div>