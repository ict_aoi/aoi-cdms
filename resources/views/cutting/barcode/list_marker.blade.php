<div class="table-responsive">
    <table class="table" id="table-style">
        <thead>
            <tr>
                <th>Barcode Marker</th>
                <th>Cut</th>
                <th>Layer</th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val->barcode_id }}</td>
                    <td>{{ $val->cut }}</td>
                    <td>{{ $val->layer }}</td>
                    <td>
                        <button type="button" id="choose-marker"
                                data-barcode="{{ $val->barcode_id }}"
                                data-cut="{{ $val->cut }}"
                                data-layer="{{ $val->layer }}"
                                data-plan="{{ $val->id_plan }}"
                                class="btn btn-default">
                                SELECT
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    </div>
    <script type="text/javascript">
        var tables = $('#table-style').DataTable({  
            "lengthChange": false,
            "pageLength": 5,
            "columnDefs": [{
               // "targets" : [0],
               // "visible" : false,
                "searchable" : false
            }]
        });
    </script>
    