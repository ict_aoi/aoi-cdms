@extends('layouts.app', ['active' => 'qc_component'])

@section('page-css')
<style>
	.add-margin-top {
		margin-top: 30px;
	}
    .full-width {
        width:100%!important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">QC Component</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Home</a></li>
            <li class="active">QC Component</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('ComponenQC.getData') }}" id="form_filter">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label><b>Choose Style</b></label>
                        <div class="input-group col-md-12 full-width">
                            <select data-placeholder="Select a style..." name="style" id="style" class="form-control select-search">
                                <option value=""></option>
                                @foreach($styles as $list)
                                    <option value="{{ $list->style }}">{{ $list->style }}</option>
                                @endforeach
                            </select>
                            {{-- <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label><b>Choose Season</b></label>
                        <div class="input-group col-md-12 full-width">
                            <select data-placeholder="Select a season..." name="season" id="season" class="form-control select-search">
                                <option value=""></option>
                                @foreach($seasons as $season)
                                    <option value="{{ $season->id }}">{{ $season->season_name }}</option>
                                @endforeach
                            </select>
                            {{-- <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary add-margin-top">Select</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_list">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>STYLE</th>
                        <th>TYPE</th>
                        <th>PART</th>
                        <th>KOMPONEN</th>
                        <th>QC CHECK</th>
                        <th>NAME ALIAS</th>
                        <th style="width:10px;">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
    @include('qccheck.qc_component._edit_modal')
@endsection

@section('page-js')
<script type="text/javascript">

    $('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });

$( document ).ready(function() {

    var url = $('#form_filter').attr('action');
    //datatables
    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#table_list').DataTable({
        ajax: url,
        data: {
            style: $('#style').val(),
            season: $('#season').val(),
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'type_name', name: 'type_name'},
            {data: 'part', name: 'part'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'is_check_qc', name: 'is_check_qc'},
            {data: 'qc_name_komponen', name: 'qc_name_komponen'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var style  = $('#style').val();
        var season  = $('#season').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {
                style: style,
                season: season,
            },
            beforeSend: function() {
                $('#table_list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table_list').unblock();
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table_list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    $('input[type=radio][name=is_check_qc]').on('change', function() {
        console.log(this.value);
        if(this.value == 'no') {
            if(!$('#name_alias').is('[readonly]')) {
                $('#name_alias').attr('readonly', true);
                $('#name_alias').val(null);
            }
        } else {
            $('#name_alias').removeAttr('readonly');
        }
    });

    $('#form_update').submit(function(event) {
        event.preventDefault();

        $.ajax({
            url: '/qc-component/update-data',
            data: new FormData($('#form_update')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#editModal').modal('hide');
                if(response.status == 200) {
                    $("#alert_success").trigger("click", 'Marker berhasil direlease!');
                } else {
                    $("#alert_warning").trigger("click", response.responseJSON);
                }
            },
            error: function (response) {
                $.unblockUI();
            }
        });
    });

    $('#is_check_qc_no').on('change', function() {
        if($('#is_check_qc_no').is('[checked]')) {
            if(!$('#name_alias').is('[readonly]')) {
                $('#name_alias').attr('readonly', true);
                $('#name_alias').val(null);
            } else {
                $('#name_alias').val(null);
            }
        } else {
            if(!$('#name_alias').is('[readonly]')) {
                $('#name_alias').removeAttr('readonly', true);
                $('#name_alias').val(null);
            } else {
                $('#name_alias').val(null);
            }
        }
    })

    $('#editModal').on('hidden.bs.modal', function(){
        $('#form_filter').submit();
        $('#is_check_qc_yes').removeAttr('checked');
        $('#is_check_qc_no').removeAttr('checked');
        $('#name_alias').val(null);
    });
});
    
    function editModal(url) {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#editModal').modal();
                $('#detail_style_view').text(response.style);
                $('#detail_season_view').text(response.season);
                $('#detail_komponen_view').text(response.komponen);
                $('#detail_style').val(response.style);
                $('#detail_season').val(response.season_id);
                $('#detail_komponen').val(response.komponen_id);
                if(response.is_check_qc) {
                    if($('#is_check_qc_no').is('[checked]')) {
                        $('#is_check_qc_no').removeAttr('checked');
                    }
                    if(!$('#is_check_qc_yes').is('[checked]')) {
                        $('#is_check_qc_yes').prop('checked',true);
                    }
                    $('#name_alias').val(response.qc_name_komponen);
                } else {
                    if($('#is_check_qc_yes').is('[checked]')) {
                        $('#is_check_qc_yes').removeAttr('checked');
                    }
                    if(!$('#is_check_qc_no').is('[checked]')) {
                        $('#is_check_qc_no').prop('checked',true);
                    }
                    if(!$('#name_alias').is('[readonly]')) {
                        $('#name_alias').attr('readonly', true);
                        $('#name_alias').val(null);
                    }
                }
            },
            error: function (response) {
                $.unblockUI();
            }
        });
    }


</script>
@endsection
