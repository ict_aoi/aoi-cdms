<?php

namespace App\Http\Controllers\Operator;

use DB;
use DataTables;
use Carbon\Carbon;
use App\Models\DistribusiMovement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class SwitchLocatorController extends Controller
{
    public function index()
    {
        $po_buyer = DB::table('bundle_header')
        ->select(DB::raw('poreference'))
        ->where('factory_id',\Auth::user()->factory_id)
        ->where('statistical_date','>',Carbon::now()->subDays(60)->format('Y-m-d'))
        ->distinct('poreference')
        ->get();
        $line = DB::table('master_setting')->select('area_name')->where('factory_id',\Auth::user()->factory_id)->wherenull('deleted_at')->get();
        return view('distribusi.switch_locator.index', compact('line','po_buyer','style','set_type'));
    }

    public function getDataSupply(Request $request){
        $poreference    = $request->po_buyer;
        $factory_id     = \Auth::user()->factory_id;
        $cut_num = $request->cut_num;
        if ($request->ajax()){
            if($poreference != null && $cut_num){
                $data = DB::select(DB::RAW("SELECT bh.id as header_id,bh.season,bh.poreference,bh.style,vms.type_name,bh.article,bh.size,bh.cut_num,date(dm.created_at) as supply_date,dm.loc_dist as supply_line,u1.name as user
                FROM bundle_header bh
                JOIN bundle_detail bd on bd.bundle_header_id = bh.id
                JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                JOIN users u1 on u1.id = dm.user_id
                where bh.poreference='$poreference'
                and bh.factory_id = '$factory_id'
                and bh.cut_num='$cut_num'
                and dm.locator_to='16'
                GROUP BY bh.id,bh.season,bh.poreference,bh.style,bh.article,vms.type_name,bh.size,bh.cut_num,date(dm.created_at),dm.loc_dist,u1.name
                UNION
                SELECT shm.id as header_id,shm.season,shm.poreference,shm.style,vms.type_name,shm.article,shm.size,shm.cut_num,date(dm.created_at) as supply_date,dm.loc_dist as supply_line,u1.name as user
                FROM sds_header_movements shm
                JOIN sds_detail_movements sdm on sdm.sds_header_id = shm.id
                JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
                JOIN v_master_style vms on vms.id = sdm.style_id
                JOIN users u1 on u1.id = dm.user_id
                where shm.poreference='$poreference'
                and shm.factory_id='$factory_id'
                and shm.cut_num='$cut_num'
                and dm.locator_to='16'
                GROUP BY shm.id,shm.season,shm.poreference,shm.style,vms.type_name,shm.size,shm.article,shm.cut_num,date(dm.created_at),dm.loc_dist,u1.name"));
            }elseif($poreference != null){
                $data = DB::select(DB::RAW("SELECT bh.id as header_id,bh.season,bh.poreference,bh.style,vms.type_name,bh.article,bh.size,bh.cut_num,date(dm.created_at) as supply_date,dm.loc_dist as supply_line,u1.name as user
                FROM bundle_header bh
                JOIN bundle_detail bd on bd.bundle_header_id = bh.id
                JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                JOIN users u1 on u1.id = dm.user_id
                where bh.poreference='$poreference'
                and bh.factory_id = '$factory_id'
                and dm.locator_to='16'
                GROUP BY bh.id,bh.season,bh.poreference,bh.style,bh.article,vms.type_name,bh.size,bh.cut_num,date(dm.created_at),dm.loc_dist,u1.name
                UNION
                SELECT shm.id as header_id,shm.season,shm.poreference,shm.style,vms.type_name,shm.article,shm.size,shm.cut_num,date(dm.created_at) as supply_date,dm.loc_dist as supply_line,u1.name as user
                FROM sds_header_movements shm
                JOIN sds_detail_movements sdm on sdm.sds_header_id = shm.id
                JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
                JOIN v_master_style vms on vms.id = sdm.style_id
                JOIN users u1 on u1.id = dm.user_id
                where shm.poreference='$poreference'
                and shm.factory_id='$factory_id'
                and dm.locator_to='16'
                GROUP BY shm.id,shm.season,shm.poreference,shm.style,vms.type_name,shm.size,shm.article,shm.cut_num,date(dm.created_at),dm.loc_dist,u1.name"));
            }else{
                $data = [];
            }
            return DataTables::of($data)
            ->editColumn('supply_line',function ($data)
            {
                if($data->supply_line == null){
                    return '-';
                }else{
                    return $data->supply_line;
                }
            })
            ->editColumn('style_set',function ($data)
            {
                if($data->type_name == 'Non'){
                    return $data->style;
                }else{
                    return $data->style.'-'.$data->type_name;
                }
            })
            ->editColumn('supply_by',function ($data)
            {
                if($data->user == null){
                    return '-';
                }else{
                    return $data->user;
                }
            })
            ->editColumn('supply_date',function ($data)
            {
                if($data->supply_date == null){
                    return '-';
                }else{
                    return Carbon::parse($data->supply_date)->format('d-M-Y');
                }
            })
            ->addColumn('action', function($data){ 
                if($data->supply_date == null){
                    return '<p style="color:red" class="icon-lock2"></p>';
                }else{
                    return view('_action', [
                        'edit_modal' => route('switch_locator.edit', $data->header_id),
                        'id' => $data->header_id,
                    ]);
                }
            })
            ->rawColumns(['style_set','supply_line','supply_by','supply_date','action'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function edit($id)
    {
        $barcode_cdms = DB::table('bundle_detail')->where('bundle_header_id',$id)->pluck('barcode_id')->toArray();
        $barcode_sds = DB::table('sds_detail_movements')->where('sds_header_id',$id)->pluck('sds_barcode')->toArray();
        $barcode_merge = array_merge($barcode_cdms,$barcode_sds);
        $line = DB::table('distribusi_movements')
                        ->select(DB::raw('loc_dist'))
                        ->whereIn('barcode_id', $barcode_merge)
                        ->where('locator_to','16')
                        ->where('status_to','out')
                        ->distinct('loc_dist')
                        ->first();
        $barcode_arr = DB::table('distribusi_movements')
        ->select(DB::raw('barcode_id'))
        ->whereIn('barcode_id', $barcode_merge)
        ->where('locator_to','16')
        ->where('status_to','out')
        ->pluck('barcode_id')->toArray();

        $data =[
            'loc_dist'      => $line,
            'barcode_id'    => $barcode_arr
        ];
        
        return response()->json($data, 200);
    }
    public function update(Request $request)
    {
        $data           = $request->all();
        $line_update    = $request->line_update;
        $line           = $data['line_update'];
        $barcode_id     =  explode(',',$data['barcode_id']);
        foreach($barcode_id as $b){
            $dist_move = DistribusiMovement::where('barcode_id',$b)->where('locator_to','16')->where('status_to','out')->first();
            if($dist_move->loc_dist == $line_update){
                //DO NOTHING CAUSE SAME AS LINE UPDATE
            }else{
                try {
                    DB::beginTransaction();
                        DistribusiMovement::where('barcode_id', $b)
                        ->where('description', 'out SUPPLY')
                        ->update([
                            'loc_dist' => $line_update,
                            'updated_at' => Carbon::now()
                        ]);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                } 
            }
        }
    }
}
