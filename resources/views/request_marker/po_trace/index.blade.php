@extends('layouts.app',['active' => 'po_trace'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }

    .change-bg-header {
        background-color: aqua;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">PO Buyer Trace</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">PO Buyer Trace</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="control-label text-bold col-md-1">PO Buyer</label>
                <div class="col-md-11">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="po_buyer" name="po_buyer" placeholder="#Input PO Buyer" autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-warning col-md-12 col-sm-12 col-xs-12" id="download-po-data">Download <i class="icon-file-excel position-left"></i></button>
            </div>
        </div>

        <div class="alert alert-info alert-styled-left alert-bordered">
            <span class="text-semibold">Informasi!</span> Jika hasil tidak ditemukan, planning permintaan belum dibuat atas PO tersebut.
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="traceTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cutting Date</th>
                        <th>Queue</th>
                        <th>Barcode Marker</th>
                        <th>Cut</th>
                        <th>Ratio</th>
                        <th>PO Buyer</th>
                        <th>Size</th>
                        <th>No Part</th>
                        <th>Layer</th>
                        <th>Aktual Layer</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('poTrace.downloadGenerate') }}" id="generateDownloadLink"></a>
@endsection

@section('page-modal')
	@include('request_marker.po_trace._marker_modal')
@endsection


@section('page-js')
    <script>
        $(document).ready( function () {

            var traceTable =  $('#traceTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength:100,
                scroller:true,
                deferRender:true,
                ajax: {
                    type: 'GET',
                    url: '/po-trace/data-po',
                    data: function(d) {
                        return $.extend({}, d, {
                            "po_buyer": $('#po_buyer').val(),
                        });
                }
                },
                fnCreatedRow: function (row, data, index) {
                    var info = traceTable.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                    // $('td', row).eq(7).css('min-width', '120px');
                    // $('td', row).eq(9).css('min-width', '200px');
                    // // $('td', row).eq(10).css('min-width', '200px');
                    // $('td', row).eq(15).css('min-width', '200px');
                },
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'cutting_date', name: 'cutting_date',searchable:true,visible:true,orderable:true},
                    {data: 'queu', name: 'queu',searchable:false,visible:true,orderable:false},
                    {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:false},
                    {data: 'cut', name: 'cut',searchable:true,visible:true,orderable:false},
                    {data: 'ratio_print', name: 'ratio_print',searchable:true,visible:true,orderable:true},
                    {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
                    {data: 'size', name: 'size',searchable:false,visible:true,orderable:true},
                    {data: 'part_no', name: 'part_no',searchable:true,visible:true,orderable:true},
                    {data: 'layer', name: 'layer',searchable:false,visible:true,orderable:true},
                    {data: 'akumulasi_layer', name: 'akumulasi_layer',searchable:false,visible:true,orderable:true},
                    {data: 'status', name: 'status',searchable:false,visible:true,orderable:false},
                    {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
                ]
            });

            var dtable = $('#traceTable').dataTable().api();
            $(".dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
            });
            dtable.draw();

            $('#po_buyer').keyup(function(e){
                if(e.keyCode == 13)
                {
                    dtable.draw();
                }
            });

            $('#download-po-data').on('click', function() {
                var po_buyer = $('#po_buyer').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "post",
                    url: $('#generateDownloadLink').attr('href'),
                    data: {
                        po_buyer: po_buyer,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function (response) {
                        $.unblockUI();
                        window.open(response.download_link, '_blank');
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            });
        });

        function markerModal(id) {
            $('#markerModal').modal();
            get_data_marker(id);
        }

        function get_data_marker(id) {
            event.preventDefault();

            var barcode_id = id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url: 'po-trace/data-marker',
                data: {
                    barcode_id: barcode_id,
                },
                beforeSend: function() {
                    $('.modal-content').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('.modal-content').unblock();
                    $('#detail_view_barcode_marker').text(response.data.barcode_id);
                    $('#detail_view_style').text(response.style);
                    $('#detail_view_cut').text(response.data.cut);
                    $('#detail_view_layer').text(response.layer);
                    $('#detail_view_length').text(response.data.marker_length);
                    $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                    $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                    $('#detail_view_part').text(response.data.part_no);
                    $('#detail_view_plan').text(response.plan);
                    $('#detail_view_color_name').text(response.color_name);
                    $('#detail_view_color_code').text(response.color_code);
                    $('#detail_view_item').text(response.item_code);
                    $('#detail_view_deliv').text(response.stat_date);
                    $('#detail_view_width').text(response.marker_width);
                    $('#detail_view_article').text(response.article);
                    $('#detail_view_marker_pcs').text(response.ratio);
                    $('#detail_view_total_garment').text(response.total_garment);
                    $('#detail_view_ratio_size').text(response.ratio_size);
                    $('#detail_view_po_buyer').text(response.po_buyer);
                    $('#spreading_status').text(response.spreading_status);
                    $('#spreading_date').text(response.spreading_date);
                    $('#spreading_by').text(response.spreading_by);
                    $('#spreading_table').text(response.spreading_table);
                    $('#approve_gl_status').text(response.approve_gl_status);
                    $('#approve_gl_date').text(response.approve_gl_date);
                    $('#approve_gl_by').text(response.approve_gl_by);
                    $('#approve_qc_status').text(response.approve_qc_status);
                    $('#approve_qc_date').text(response.approve_qc_date);
                    $('#approve_qc_by').text(response.approve_qc_by);
                    $('#cutting_status').text(response.cutting_status);
                    $('#cutting_date').text(response.cutting_date);
                    $('#cutting_by').text(response.cutting_by);
                    $('#cutting_table').text(response.cutting_table);
                    $('#bundling_status').text(response.bundling_status);
                    $('#bundling_date').text(response.bundling_date);
                    $('#bundling_by').text(response.bundling_by);
                    $('#bundling_table').text(response.bundling_table);
                    $('#barcode_marker').val('');
                    $('#barcode_marker').focus();
                },
                error: function(response) {
                    $('.panel').unblock();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        }
    </script>
@endsection
