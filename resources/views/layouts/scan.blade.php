<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>CDMS | Cutting Distribution Management System</title>
        @yield('page-css')

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->

	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        <link rel="icon" href="{{ asset('/images/logo2.png')  }}" type="image/x-icon">
        <style>
            .god {
                background:  linear-gradient(45deg, #555755, #a6aaa8 80%) no-repeat center center fixed;

                /* background:  linear-gradient(45deg, #172A3A, #073B4C 80%) no-repeat center center fixed; */
                /* D0E3C4
                5F7470 */
                /* C8CC92 */
                /* background:  linear-gradient(45deg, #F08A4B, #FFEE88 80%) no-repeat center center fixed; */
                /* A4B8C4 */
                /* FF934F */
                /* FFEE88 */

                /* background:  linear-gradient(45deg, #091C87, #5975FF 80%) no-repeat center center fixed; */

                /* background:  #091C87; */

                /* #ff9595 */
                /* #833ab4 */

                background-size: cover;
            }

            .padding-footer{
                padding-left: 20px !important;
                color: white !important;
            }

            .padding-footer a{
                color: white !important;
                font-weight: 700;
            }
        </style>
    </head>
    <body class="login-container god">
        @include('includes.top_notif')
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                        <div class="panel panel-body">
                        @yield('page-content')
                        @yield('page-modal')
                        <script src="{{ mix('js/backend.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        <script src="{{ mix('js/datepicker.js') }}"></script>
                        @yield('page-js')
                        {{-- <div class="panel panel-body">
                            <a href="#" class="btn btn-primary col-md-12" id="cutting">Cutting</a>
                        </div> --}}
                        </div>
                        <div class="footer text-muted padding-footer">

                            &copy; 2019. <a href="route('home')">CDMS | Cutting Distribution Management System</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
