@extends('layouts.app', ['active' => 'masterKomponenProcess'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">KOMPONEN & PROCESS</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home4 position-left"></i> Home</a></li>
            <li class="active">Komponen & Process</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <form action="{{ route('masterparam.ajaxGetDataMasterKomponenProcess') }}" id="form_filter">
            <div class="form-group hidden" id="filter_by_lcdate">
				<label><b>Choose Order Date (month/day/year)</b></label>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Filter</button>
                    </div>
				</div>
			</div>
            <div class="form-group" id="filter_by_style">
				<label><b>Choose Style</b></label>
                    <div class="input-group col-md-12">
                        <select name="style" id="style" class="form-control select-style">
                        </select>
                        {{-- <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div> --}}
                    </div>
			</div>
            <div class="form-group hidden">
                <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="style">Filter by Style</label>
                <label class="radio-inline"><input type="radio" name="radio_status" value="lc">Filter by LC Date</label>
            </div>
        </form>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading hidden">
        <div class="form-group">
            <button type="button" class="btn btn-xs btn-primary add_new">Add Style <span class="icon-plus2"></span></button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-save-state" id="table_list">
                <thead>
                    <tr>
                        <th style="width:10px;">#</th>
                        <th>STYLE</th>
                        <th>SEASON</th>
                        <th>KOMPONEN</th>
                        <th>TYPE</th>
                        {{--  <th>PROCESS</th>  --}}
                        {{--  <th style="width:10px;">ACTION</th>  --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('masterparam.ajaxGetDataKomponenProcess') }}" id="get_detail_process"></a>
<a href="{{ route('masterparam.ajaxGetDataStyle') }}" id="get_data_style"></a>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessJson') }}" id="get_detail_process_"></a>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessDetail') }}" id="get_detail_process_detail"></a>
<a href="{{ route('masterparam.ajaxGetDataKomponenProcessDetailJson') }}" id="get_detail_process_2"></a>
@endsection

@section('page-modal')
    @include('master_parameter.komponen_process._index_modal')
@endsection

@section('page-js')
<script type="text/javascript">

    $('button[data-number=1]').click(function(){
        $('#modal_detail_').modal('hide');
    });

    /*$('#style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
    });*/

    $('.select-style').select2({
        placeholder: "Select a State",
        minimumInputLength: 3,
        formatInputTooShort: function () {
            return "Enter Minimum 3 Character";
        },
        ajax: {
            type: 'GET',
            url: $('#get_data_style').attr('href'),
            dataType: 'json',
           // delay: 250,
            data: function (params) {
                return {
                    term: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data,
                    more: false
                };
            }
        }
    });


    $('#style').on('change', function(){
        if($(this).val() != ''){
            $('#form_filter').submit();
        }
    });

//$( document ).ready(function() {

    var url = $('#form_filter').attr('action');
    var _token = $("input[name='csrf-token']").val();
    //datatables
   /* $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });*/

    var table = $('#table_list').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: url,
        data: {
            date_range: $('#date_range').val(), 
            style: $('#style').val(), 
            radio_status: $('input[name=radio_status]:checked').val()
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'season_name', name: 'season_name'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'type_name', name: 'type_name'}
           // {data: 'process_name', name: 'process_name'},
           // {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
    //end of datatables

    //filter (api)
    $('#form_filter').submit(function(event){
        event.preventDefault();
        var date_range = $('#date_range').val();
        var style  = $('#style').val();
        var radio_status = $('input[name=radio_status]:checked').val();

        //check location
        if(date_range == '' && style == '') {
            myalert('error','Please select filter first');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url : url,
            data: {date_range: date_range, style: style, radio_status: radio_status},
            beforeSend: function() {
                $('#table_list').block({
                	message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#table_list').unblock();
                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#table_list').unblock();
                myalert('error',response['responseJSON']);
            }
        });
    });

    //choose filter
    $('input[type=radio][name=radio_status]').change(function() {
        if (this.value == 'style') {
            if($('#filter_by_style').hasClass('hidden')) {
                $('#filter_by_style').removeClass('hidden');
            }

            $('#filter_by_lcdate').addClass('hidden');
        }
        else if (this.value == 'lc') {
            if($('#filter_by_lcdate').hasClass('hidden')) {
                $('#filter_by_lcdate').removeClass('hidden');
            }

            $('#filter_by_style').addClass('hidden');
        }
    });

    function edit(url){

        $.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {

            var process = response.process;

            $('#styleUpdate').val(response.style);
            $('#seasonUpdate').val(response.season);
            $('#totalUpdate').val(response.total);

            $('#komponenIdUpdate').val(response.komponen);
            $('#komponenNameUpdate').val(response.komponen_name);

            $('#type_id_update').val(response.type_id);

            if(process != null){
                $.each(process.split(","), function(i, v){
                    $('#process_id_update option[value='+ v +']').prop('selected', true).trigger('change');
                });
            }else{
                $('#process_id_update').val(null).trigger('change');
            }

			$('#modal_edit_').modal();

		});
    }

    function detailKomponen(e){
        var id = $(e).data('id');
        var komponen = $(e).data('komponen');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_process').attr('href'),
            data: {'id': id, '_token': _token},
            beforeSend: function(){
                $('#modal_detail_ > .modal-content').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function() {
                $('#modal_detail_').modal('show');
            },
            success: function(response) {
                $('#modal_detail_ > .modal-content').unblock();
                //$('#list').html(response);
                $('#id_style_detail').val(response.details.id);
                $('#spStyle').text('Style : '+response.details.style);
                $('#komponen_name').val(response.details.komponen_name);

                $('.select-process').children('option:not(:first)').remove();

                $.each(response.process, function(i, v){

                    $(".select-process").select2({
                        data: [
                            {id: v.id, text: v.process_name}
                        ]
                    });  
                    
                });

                table_komponen.draw();
            },
            error: function(response) {
                console.log(response);
            }
        });
    }


    var ids = $('#id_style_detail').val();
    var url_process = $('#get_detail_process_').attr('href');

    $('button[data-number=2]').click(function(){
        $('#modal_detail_process_').modal('hide');
    });

    var table_komponen = $('#table-list-komponen-process').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            url: url_process,
           // type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "id": $('#id_style_detail').val(),
                    "_token": $("input[name='csrf-token']").val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
            {data: 'style', name: 'style'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'process_name', name: 'process_name'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    //add new
    $('#form-add').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_style_detail', 'process_id'];
        var val = [
            $('#id_style_detail').val(), 
            $('#process_id').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-add').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#process_id').val('').trigger('change');
                table_komponen.draw();
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        })
    });

    function deleteKomponenProcess(e){
        event.preventDefault();
        var id = $(e).data('id');
        var process_name = $(e).data('process');
        if(id == '') {
            return false;
        }
        bootbox.confirm("Are you sure delete "+process_name+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-komponen-process/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": $("input[name='csrf-token']").val(),
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table_komponen.ajax.reload();
                    }
                });
            }
        });
    }

    function detailKomponenProcess(e){
        var id = $(e).data('id');
        var komponen_name = $(e).data('komponen');
        var process_name = $(e).data('process');
        var process_id = $(e).data('processid');
        var style = $(e).data('style');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'GET',
            url: $('#get_detail_process_detail').attr('href'),
            data: {'id': id, process_id: process_id, '_token': _token},
            beforeSend: function(){
                $('#modal_detail_process_ > .modal-content').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function() {
                $('#modal_detail_process_').modal('show');
            },
            success: function(response) {
                console.log(response);
                $('#id_style_transactions').val(response.id_style_transactions);
                $('#modal_detail_process_ > .modal-content').unblock();
                //$('#list_process').html(response);
                $('#komponen_name_').val(komponen_name);
                $('#process_name_').val(process_name);
                $('#spStyle_').text(style);

                $('.select-process-detail').children('option:not(:first)').remove();
                $('.select-column-name').children('option:not(:first)').remove();
                $('.select-remark-rnd').children('option:not(:first)').remove();

                $.each(response.process_detail, function(i, v){

                    $(".select-process-detail").select2({
                        placeholder:'select a state..',
                        data: [
                            {
                                id: v.id,
                                text: v.detail_process_name
                            }
                        ]
                    });  
                    
                });

                $.each(response.column_reports, function(i, v){

                    $(".select-column-name").select2({
                        placeholder:'select a state..',
                        data: [
                            {
                                id: v.id,
                                text: v.column_name
                            }
                        ]
                    });  
                    
                });

                $.each(response.remark_rnd, function(i, v){

                    $(".select-remark-rnd").select2({
                        placeholder:'select a state..',
                        data: [
                            {
                                id: v,
                                text: v
                            }
                        ]
                    });  
                    
                });

                //if process printing / he pilih remark rnd
                if(response.is_printing || response.is_he){
                    $('#dvRemarkRnd').removeClass('hidden');
                }else{
                    $('#dvRemarkRnd').addClass('hidden');
                }

                table_komponens.draw();
            },
            error: function(response) {
                console.log(response);
            }
        });
    }
    ///////// detail process
    $('.number').keypress(function (event) {
        var $this = $(this);
    
        if ((event.which != 44 || $this.val().indexOf(',') != -1) &&
                ((event.which < 48 || event.which > 57) &&
                        (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which == 188) && (text.indexOf(',') == -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf(',')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf(',') + 3));
                }
            }, 1);
        }

       /* if ((text.indexOf(',') != -1) &&
                (text.substring(text.indexOf(',')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }*/
    });

    $('.number').bind("paste", function (e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        if ($.isNumeric(text)) {
            if ((text.substring(text.indexOf(',')).length > 3) && (text.indexOf(',') > -1)) {
                e.preventDefault();
                $(this).val(text.substring(0, text.indexOf(',') + 3));
            }
        } else {
            e.preventDefault();
        }
    });

    $('#id_detail_process').change(function(){
        
        if($(this).val() != ''){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url : "master-komponen-process/listing/"+$(this).val(),
                contentType: false,
                processData: false,
                success: function(response) {
                    $('#gsd_name').val(response.gsd_name);
                    $('#is_artwork').val(response.is_artwork);
                },
                error: function(response) {
                    console.log(response);
                }
            })
            
        }else{
            $('#gsd_name').val('');
            $('#is_artwork').val('');
        }
    });

    var table_komponens = $('#table-list-komponen-process-detail_').DataTable({
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            url: $('#get_detail_process_2').attr('href'),
            //type: 'post',
            data: function (d) {
                return $.extend({},d,{
                    "id": $('#id_style_transactions').val(),
                    "_token": $("input[name='csrf-token']").val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false },
           // {data: 'style', name: 'style'},
            {data: 'komponen_name', name: 'komponen_name'},
            {data: 'process_name', name: 'process_name'},
            {data: 'detail_process_name', name: 'detail_process_name', orderable: false, searchable: false},
            {data: 'lc', name: 'lc'},
            {data: 'gasa', name: 'gasa'},
            {data: 'total', name: 'total'},
            {data: 'remark_rnd', name: 'remark_rnd'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });


    //add new
    $('#form-adds').submit(function(event) {
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['id_style_transactions', 'id_detail_process', 'lc', 'gasa', 'total', 'remark_rnd'];
        var val = [
            $('#id_style_transactions').val(), 
            $('#id_detail_process').val(),
            $('#lc').val(),
            $('#gasa').val(),
            $('#total').val(),
            $('#remark_rnd').val()
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-adds').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
            	$('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
                $('#id_detail_process').val(null).trigger('change');
                $('#remark_rnd').val(null).trigger('change');
                $('#lc').val('');
                $('#gasa').val('');
                $('#total').val('');
                table_komponens.draw();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });

    function deleteKomponenProcessDetail(e){
        event.preventDefault();
        var id = $(e).data('id');
        var process_name = $(e).data('processdetail');
        if(id == '') {
            return false;
        }
        bootbox.confirm("Are you sure delete "+process_name+" ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "master-komponen-process-detail/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": $("input[name='csrf-token']").val(),
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        myalert('success','Data has been deleted');
                        table_komponens.ajax.reload();
                    }
                });
            }
        });
    }

    function editKomponenProcessDetail(e){
        event.preventDefault();
        var id = $(e).data('id');
        var process_name = $(e).data('processdetail');
        if(id == '') {
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "master-komponen-process-detail/delete/"+id,
            type: "GET",
            data: {
                "id": id,
                "_method": 'UPDATE',
                "_token": $("input[name='csrf-token']").val(),
            },
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $(".loader-area").unblock();
            },
            success: function (response) {
                $('#remark_rnd').val(response.remark_rnd).trigger('change');
                $('#id_detail_process').val(response.id_detail_process).trigger('change');
                $('#gsd_name').val(response.gsd_name);
                $('#lc').val(response.lc);
                $('#gasa').val(response.gasa);
                $('#total').val(response.total);
            }
        });
            
    }

//});

</script>
@endsection
