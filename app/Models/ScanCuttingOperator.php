<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class ScanCuttingOperator extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'scan_cutting_operators';
	protected $fillable = ['scan_cutting_id', 'user_id', 'deleted_at', 'name', 'nik'];

	public function scan_cutting()
    {
        return $this->belongsTo('App\Models\ScanCutting', 'scan_cutting_id', 'id');
    }
}
