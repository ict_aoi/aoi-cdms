<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class EximDocument extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'document_bc_exim';
	protected $fillable = ['id','factory_id','document_type','aju_no','pabean_no','pabean_date','receive_no','receive_date','subcont_factory','item_code','item_name','uom','qty','item_value','currency','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by','kk_no','product','quota_used','balanced_quota','doc_bc_header_id','type_movement'];
}
