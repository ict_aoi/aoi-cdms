@extends('adibooster.index')

@section('page-css')
<style type="text/css">
    .left{
        padding-left: 2px;
        padding-right: 2px;
        width:49%;
        float:left;
    }
    .right{
        padding-left: 2px;
        padding-right: 2px;
        width:49%;
        float:right;
    }
    .title-fac{
        color: black;
        font-family:Cooper Black;
        font-size:  40px;
    }

    td{
        font-size: 10px;
    }
    th{
        font-size: 12px;
        font-weight: bold;
    }
    .center-row {
		text-align: center !important;
	}
</style>
@endsection

@section('page-header')
<div class="page-header" style="height: 60px;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1px; padding-bottom: 5px;">
        	<center><label class="title-fac">DASHBOARD CUTTING PREPARATION AOI {{$factory_id}}</label></center>
        	<input type="text" name="factory_id" id="factory_id" class="hidden" value="{{$factory_id}}">
        </div>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="left" >
        <div class="row">
            <center><h4><b>{{$today_show}}</b></h4></center>
            <input type="text" name="dtoday" id="dtoday" class="hidden" value="{{$today}}">
        </div>
        <div class="row">
            <div class="panel panel-flat">
                <div class="panel-body" >
                    <div class="table-responsive" style="height: 100%;">
                        <table class="table table-basic table-condensed" id="today">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <th rowspan="2" class="text-center">Queue</th>
                                    <th rowspan="2" class="text-center">Style Code</th>
                                    <th rowspan="2" class="text-center">Color</th>
                                    <th colspan="2" class="center-row text-center">Preparation Status</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Marker</th>
                                    <th class="text-center">Fabric</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5"><center>Tidak ada data!</center></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right" >
        <div class="row">
            <center><h4><b>{{$tomorrow_show}}</b></h4></center>
            <input type="text" name="dtomorrow" id="dtomorrow" class="hidden" value="{{$tomorrow}}">
        </div>
        <div class="row">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="table-responsive" style="height: 100%;">
                        <table class="table table-basic table-condensed" id="tomorrow">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="text-center" >Queue</th>
                                    <th rowspan="2" class="text-center">Style Code</th>
                                    <th rowspan="2" class="text-center">Color</th>
                                    <th colspan="2" class="center-row text-center">Preparation Status</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Marker</th>
                                    <th class="text-center">Fabric</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">

    var factory_id = $('#factory_id').val();

    // Today
    var data1;
    var data1_length = 0;
    var data1_per_page = 18;
    var data1_page = 0;
    var data1_state = 0;

    // Tomorrow
    var data2;
    var data2_length = 0;
    var data2_per_page = 18;
    var data2_page = 0;
    var data2_state = 0;

    $(document).ready(function(){

        $.ajax({
            type: 'get',
            url: "{{ route('adibooster.pretoday') }}",
            data: {
                factory_id: $('#factory_id').val(),
                today:$('#dtoday').val(),
            },
            success: function (response) {
                data1_length = response.length;
                data1_page = Math.ceil(response.length / data1_per_page);
                data1 = response;
                appendData1();
            },
            error: function (response) {

            }
        });

        $.ajax({
            type: 'get',
            url: "{{ route('adibooster.pretomorrow') }}",
            data: {
                factory_id: $('#factory_id').val(),
                tomorrow:$('#dtomorrow').val(),
            },
            success: function (response) {
                data2_length = response.length;
                data2_page = Math.ceil(response.length / data2_per_page);
                data2 = response;
                appendData2();
            },
            error: function (response) {

            }
        });

        setInterval(function()
        {
            // today

            $.ajax({
                type: 'get',
                url: "{{ route('adibooster.pretoday') }}",
                data: {
                    factory_id: $('#factory_id').val(),
                    today:$('#dtoday').val(),
                },
                success: function (response) {
                    data1_length = response.length;
                    data1_page = Math.ceil(response.length / data1_per_page);
                    data1 = response;
                },
                error: function (response) {

                }
            });

            // tomorrow

            $.ajax({
                type: 'get',
                url: "{{ route('adibooster.pretomorrow') }}",
                data: {
                    factory_id: $('#factory_id').val(),
                    tomorrow:$('#dtoday').val(),
                },
                success: function (response) {
                    data2_length = response.length;
                    data2_page = Math.ceil(response.length / data2_per_page);
                    data2 = response;
                },
                error: function (response) {

                }
            });

            console.log('Update data');
        }, 300000);

        setInterval(function()
        {
            appendData1();
            appendData2();

            console.log('Update tampilan');
        }, 15000);

        // setInterval(function(){ window.location.href="route('adibooster.spreading')"; }, 3000);

         setInterval(function(){window.location.href="/adibooster/spreading/"+factory_id },180000);

    });

    function appendData1() {
        if(data1_length > 0) {

            $('#today > tbody').html('');

            var i_min = data1_state * data1_per_page;
            var i_max = i_min + data1_per_page;

            if(i_max > data1_length) {
                i_max = data1_length;
            }

            for(var i = i_min; i < i_max; i++){
                $('#today > tbody').append(
                    $('<tr>').append(
                        $('<td>').text(data1[i].queu)
                    ).append(
                        $('<td>').text(data1[i].style)
                    ).append(
                        $('<td>').text(data1[i].color_name)
                    ).append(
                        $('<td>').text(data1[i].marker)
                    ).append(
                        $('<td>').text(data1[i].fabric)
                    )
                );

                if(data1[i].marker == 'Not Request Yet') {
                    $('#today tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data1[i].marker == 'On Progress') {
                    $('#today tr').last().children().eq(3).css({'backgroundColor': '#fee440'});
                } else if(data1[i].marker == 'Not Release Yet') {
                    $('#today tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data1[i].marker == 'Done') {
                    $('#today tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#55a630'});
                } else if(data1[i].marker == 'Waiting For MM Approval') {
                    $('#today tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#277da1'});
                }

                if(data1[i].fabric == 'Not Prepare Yet') {
                    $('#today tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data1[i].fabric.substring(0,11) == 'On Progress') {
                    $('#today tr').last().children().eq(4).css({'backgroundColor': '#fee440'});
                } else if(data1[i].fabric == 'Done') {
                    $('#today tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#55a630'});
                } else if(data1[i].fabric == 'Missing Part') {
                    $('#today tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#808080'});
                } else if(data1[i].fabric == 'Waiting For MM Approval') {
                    $('#today tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#277da1'});
                }
            }

            data1_state++;

            if(data1_state >= data1_page) {
                data1_state = 0;
            }
        } else {
            $('#today > tbody').html('');

            $('#today > tbody').append(
                $('<tr>').append(
                    $('<td>').attr('colspan', 5).append(
                        $('<center>').text('Tidak ada data!')
                    )
                )
            );
        }
    }

    function appendData2() {
        if(data2_length > 0) {

            $('#tomorrow > tbody').html('');

            var i_min = data2_state * data2_per_page;
            var i_max = i_min + data2_per_page;

            if(i_max > data2_length) {
                i_max = data2_length;
            }

            for(var i = i_min; i < i_max; i++){
                $('#tomorrow > tbody').append(
                    $('<tr>').append(
                        $('<td>').text(data2[i].queu)
                    ).append(
                        $('<td>').text(data2[i].style)
                    ).append(
                        $('<td>').text(data2[i].color_name)
                    ).append(
                        $('<td>').text(data2[i].marker)
                    ).append(
                        $('<td>').text(data2[i].fabric)
                    )
                );

                if(data2[i].marker == 'Not Request Yet') {
                    $('#tomorrow tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data2[i].marker == 'On Progress') {
                    $('#tomorrow tr').last().children().eq(3).css({'backgroundColor': '#fee440'});
                } else if(data2[i].marker == 'Not Release Yet') {
                    $('#tomorrow tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data2[i].marker == 'Done') {
                    $('#tomorrow tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#55a630'});
                } else if(data2[i].marker == 'Waiting For MM Approval') {
                    $('#tomorrow tr').last().children().eq(3).css({'color': 'white', 'backgroundColor': '#277da1'});
                }

                if(data2[i].fabric == 'Not Prepare Yet') {
                    $('#tomorrow tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#ba181b'});
                } else if(data2[i].fabric.substring(0,11) == 'On Progress') {
                    $('#tomorrow tr').last().children().eq(4).css({'backgroundColor': '#fee440'});
                } else if(data2[i].fabric == 'Done') {
                    $('#tomorrow tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#55a630'});
                }else if(data1[i].fabric == 'Missing Part') {
                    $('#today tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#808080'});
                }else if(data2[i].fabric == 'Waiting For MM Approval') {
                    $('#tomorrow tr').last().children().eq(4).css({'color': 'white', 'backgroundColor': '#277da1'});
                }
            }

            data2_state++;

            if(data2_state >= data2_page) {
                data2_state = 0;
            }
        } else {
            $('#tomorrow > tbody').html('');

            $('#tomorrow > tbody').append(
                $('<tr>').append(
                    $('<td>').attr('colspan', 5).append(
                        $('<center>').text('Tidak ada data!')
                    )
                )
            );
        }
    }
</script>
@endsection
