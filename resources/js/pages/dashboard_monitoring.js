$(document).ready( function () {
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        autoLength: false,
        processing: true,
        pageLength: 100,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/dashboard/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 5,
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData == 'Prioritas Plan' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    } else if( cellData == 'SSP Upload' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    } else if( cellData == 'Marker Request' ) {
                        $(td).css({'color': 'black', 'backgroundColor': 'yellow'});
                    } else if( cellData == 'Fabric Width' ) {
                        $(td).css({'color': 'black', 'backgroundColor': 'yellow'});
                    }  else if( cellData == 'Marker Upload' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'green'});
                    } else if( cellData == 'Over Consum' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'blue'});
                    } else if( cellData == 'Marker Plot' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'green'});
                    } else {
                        $(td).css({'color': 'black', 'backgroundColor': 'white'});
                    }
                }
            },
            {
                targets: 6,
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData == 'Prioritas Plan' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    } else if( cellData == 'Prepare' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'});
                    } else if( cellData == 'Relax' ) {
                        $(td).css({'color': 'black', 'backgroundColor': 'yellow'});
                    } else if( cellData == 'Scan Out' ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'green'});
                    } else {
                        $(td).css({'color': 'white', 'backgroundColor': 'blue'});
                    }
                }
            }
        ],
        columns: [
            {data: 'queu', name: 'queu',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'color_name', name: 'color_name',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'marker', name: 'marker',searchable:false,orderable:false},
            {data: 'fabric', name: 'fabric',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();

        console.log(cutting_date);

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/dashboard/data-planning',
            data: {
                cutting_date: cutting_date,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    // Upload File Marker
    $('#form_upload_file').submit(function(event) {
        event.preventDefault();

        var file_excel = $('#file_marker').val();

        if(!file_excel){
            $("#alert_warning").trigger("click", 'File marker masih kosong!');
            return false;
        }

        $.ajax({
            type: "POST",
            url: $('#form_upload_file').attr('action'),
            data:new FormData($("#form_upload_file")[0]),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $('#form_upload_file').trigger("reset");
                $('#uploadTable').DataTable().ajax.reload();
                $('#file_marker').val(null);
                $("#alert_success").trigger("click", 'Data Berhasil disimpan');
            },
            error: function (response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#form_upload_file').modal();
            }
        });
    });

    $('#uploadModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        $('#file_marker').val(null);
    });

    // download report

    $('#download_report').on('click', function() {
        var cutting_date = $('#cutting_date').val();
        var data = Array();
        $('#planningTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                data[i][ii] = $(this).text();
            });
        });

        if(data.length > 1) {
            $('#data_download').val(JSON.stringify(data));
            $('#data_cutting_date').val(cutting_date);
            $('#download_report_submit').submit();
        }
    });
});