<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ReversedMovement extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'reversed_movement_packinglist';
	protected $fillable = ['id','barcode_id', 'locator_from', 'status_from', 'locator_to', 'status_to', 'user_id', 'description', 'ip_address', 'deleted_at', 'loc_dist','bundle_table','factory_order','movement_id'];
}
