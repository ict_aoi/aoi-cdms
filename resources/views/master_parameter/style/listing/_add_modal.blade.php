<!-- MODAL ADD -->
<div id="modal_add_new_" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" action="{{ route('masterparam.addListStyle') }}" id="form-add">
            <div class="modal-content">
              	<div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                  	<div class="panel-body loader-area">
                      	<fieldset>
                          	<legend class="text-semibold">
                              	<i class="icon-file-text2 position-left"></i>
                              	<span id="title"> ADD NEW LIST STYLE</span> <!-- title -->
							</legend>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Style Standart:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="style_standart" id="style_standart" class="form-control select-search2" required>
												<option value=""></option>
												@foreach ($styles as $item)  
													<option value="{{ $item->style }}">{{ $item->style }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Set Type:</label>
										<div class="col-lg-9">
											<input type="hidden" name="style" id="style" class="form-control" required>
											<select data-placeholder="Select a State..." name="set_type" id="set_type" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($types as $item)  
													<option value="{{ $item->type_name }}">{{ $item->type_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Bisnis Unit:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="bisnis_unit" id="bisnis_unit" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($bisnis_unit as $item)  
													<option value="{{ $item->bisnis_unit_name }}">{{ $item->bisnis_unit_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Main Material:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="main_material" id="main_material" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($main_material as $item)  
													<option value="{{ $item->main_material_name }}">{{ $item->main_material_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Fabric Category:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="fabric_category" id="fabric_category" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($fabric_category as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Difficulty Level:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="difficulty_level" id="difficulty_level" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($difficulty_level as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Stripes:</label>
										<div class="col-lg-9">
											<select multiple data-placeholder="Select a State..." name="stripes" id="stripes" class="form-control select-search">
												<option value=""></option>
												@foreach ($stripes as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Status AD:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="status_ad" id="status_ad" class="form-control select-search">
												<option value=""></option>
												@foreach ($status_ad as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">aSC Priority:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="asc_priority" id="asc_priority" class="form-control select-search">
												<option value=""></option>
												@foreach ($asc_priority as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Update SPT Input:</label>
										<div class="col-lg-9">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" class="form-control pickadate" placeholder="Update SPT Input" id="update_spt_input" name="update_spt_input">
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">season:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="season" id="season" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($seasons as $item)  
													<option value="{{ $item->id }}">{{ $item->season_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Product Type:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="product_type" id="product_type" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($product_type as $item)  
													<option value="{{ $item->product_type_name }}">{{ $item->product_type_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Product Category:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="product_category" id="product_category" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($product_category as $item)  
													<option value="{{ $item->product_category_name }}">{{ $item->product_category_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Desc Material:</label>
										<div class="col-lg-9">
											<textarea class="form-control" name="desc_material" id="desc_material" cols="30" rows="2"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Fabric Group:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="fabric_group" id="fabric_group" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($fabric_group as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Status Style:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="status_style" id="status_style" class="form-control select-search" required>
												<option value=""></option>
												@foreach ($status_style as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">AD:</label>
										<div class="col-lg-9">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" class="form-control pickadate" placeholder="Update AD" id="ad" name="ad">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Engineering Fabric:</label>
										<div class="col-lg-9">
											<select data-placeholder="Select a State..." name="engineering_fabric" id="engineering_fabric" class="form-control select-search">
												<option value=""></option>
												@foreach ($engineering_fabric as $item)  
													<option value="{{ $item }}">{{ $item }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Critical Process:</label>
										<div class="col-lg-9">
											<textarea class="form-control" name="critical_process" id="critical_process" cols="30" rows="2"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">MP:</label>
										<div class="col-lg-9">
											<input type="text" name="mp" id="mp" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Total Process:</label>
										<div class="col-lg-9">
											<input type="text" name="total" id="total" class="form-control text-right" onkeypress="return isNumberKey(event);" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">tsme:</label>
										<div class="col-lg-9">
											<input type="text" name="tsme" id="tsme" class="form-control text-right number2" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">tslc:</label>
										<div class="col-lg-9">
											<input type="text" name="tslc" id="tslc" class="form-control text-right number2" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Min Diameter Hemning:</label>
										<div class="col-lg-9">
											<input type="text" name="min_diameter_hemning" id="min_diameter_hemning" class="form-control text-right number2" autocomplete="off" required>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="col-lg-3 control-label text-semibold">Max Diameter Hemning:</label>
										<div class="col-lg-9">
											<input type="text" name="max_diameter_hemning" id="max_diameter_hemning" class="form-control text-right number2" autocomplete="off">
										</div>
									</div>
								</div>
							</div>
                          	
                          	<hr>
                          	<div class="form-group text-center">
                          		<button type="submit" class="btn btn-success">Submit <i class="icon-floppy-disk position-right"></i></button>
                          		<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                          	</div>
                      	</fieldset>
                  	</div>
              	</div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL ADD -->