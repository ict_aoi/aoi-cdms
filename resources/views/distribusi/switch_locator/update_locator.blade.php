<!-- MODAL EDIT -->
<div id="modal_edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('subcont_factory.updateSubcontFactory') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
            <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                <div class="panel-body loader-area">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> UPDATE SUPPLY LINE</span>
                            </legend>
                            <input type="hidden" class="form-control" name="id_update" id="id_update" placeholder="" readonly>
                        <div class="form-group">
                        <label class="control-label col-lg-2 text-bold">Line</label>
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-btn">	
                                        <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
                                    </span>
                                    <select class="form-control select-search" name="po_buyer" id="po_buyer" required>
                                        <option value=""></option>
                                        @foreach($po_buyer as $po_buyer)
                                            <option value="{{ $po_buyer->poreference }}">{{$po_buyer->poreference }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Update <i class="icon-floppy-disk position-right"></i></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
<!-- /MODAL EDIT -->