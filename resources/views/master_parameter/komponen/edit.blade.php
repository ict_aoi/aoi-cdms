@extends('layouts.app', ['active' => 'masterLocator'])

@section('page-content')
<br>
<div class="breadcrumb-line breadcrumb-line-component">
    <ul class="breadcrumb">
        <li><a href="{{ route('masterparam.masterLocator') }}"><i class="icon-home2 position-left"></i>Edit Locator</a></li>
    </ul>
</div>

<section class="panel">
	<div class="panel-body loader-area">
        <form action="{{ route('masterparam.updateLocator') }}" id="main-form" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="text" name="id" class="hidden" value="{{ $master_locator->id }}" readonly>
            <div class="form-group">
                <label class="col-sm-2 control-label text-semibold text-uppercase">Name :</label>
                <div class="col-sm-5">
                    <input type="text" name="locator_name" placeholder="Name" class="form-control text-uppercase" id="locator_name" value="{{ $master_locator->locator_name }}">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    <div class="text-center loader-area">
                        <button type="button" class="btn btn-success save-data" name="update">SAVE <i class="icon-floppy-disk position-right"></i></button>
                        <a class="btn btn-default" href="javascript:history.back()">Close <i class="icon-reload-alt position-right"></i></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function () {

    var fd = $("#main-form").serializeArray();
    var url = $("#main-form").attr("href");
    $(".save-data").on('click', function(event) {
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#main-form').attr('action'),
            data: $('#main-form').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error','NOT GOOD');
            }
        })
    });

});
</script>
@endsection
