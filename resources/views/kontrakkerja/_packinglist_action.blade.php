
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if(isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-file-text"></i> Print</a></li>
            @endif
        </ul>
    </li>
</ul>
