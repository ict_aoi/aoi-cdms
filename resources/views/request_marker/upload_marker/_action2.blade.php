@if (isset($download))
    <a href='#' onclick="unduh('{!! $download !!}')"  class="btn btn-primary btn-icon"><i class="icon-folder-download3"></i></a>
@endif
@if (isset($delete))
    <a href='#' onclick="hapus('{!! $delete !!}')"  class="btn btn-danger btn-icon"><i class="icon-close2"></i></a>
@endif