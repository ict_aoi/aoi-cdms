@extends('layouts.app', ['active' => 'report_fabric_prepare'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Preparation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Daily Material Preparation</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			{!!
				Form::open(array(
					'class' 	=> 'form-horizontal',
					'role' 		=> 'form',
					'url' 		=> '',
					'method' 	=> 'get',
					'target' 	=> '_blank'
				))
			!!}

			@include('form.select', [
				'field' 		=> 'warehouse',
				'label' 		=> 'Warehouse',
				'default' 		=> auth::user()->warehouse,
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'options' 		=> [
					'1000001' 	=> 'Warehouse Fabric AOI 1',
					'1000011' 	=> 'Warehouse Fabric AOI 2',
				],
				'class' 		=> 'select-search',
				'attributes' 	=> [
					'id' 		=> 'select_warehouse'
				]
			])

			@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'Planning Date From (00:00)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->subDays(7)->format('m/d/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])

			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'Planning Date To (23:59)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				// 'default'		=> \Carbon\Carbon::now()->addDays(30)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])
			{!! Form::hidden('active_tab_export','[]' , array('id' => 'active_tab_export')) !!}

			{{-- <button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button> --}}
		{!! Form::close() !!}
		</div>
	</div>

	<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
		<ul class="nav navbar-nav visible-xs-block">
			<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-filter">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#reguler" data-toggle="tab" onclick="changeTab('reguler')">Reguler <i class="icon-stack3 position-left"></i></a></li>
			</ul>
		</div>
	</div>

	<div class="tabbable">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="reguler">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover" id="reguler_table">
								<thead>
									<tr>
										<th>No</th>
										<th>ID</th>
										<th>Warehouse Preparation</th>
										<th>Planning Date</th>
										<th>Piping</th>
										<th>Po Supplier</th>
										<th>Supplier Name</th>
										<th>Item Code</th>
										<th>Item Code Source</th>
										<th>Po Buyer</th>
										<th>Article No</th>
										<th>Style</th>
										<th>Uom</th>
										<th>Total Qty Prepared</th>
										<th>Total Qty Outstanding</th>
										<th>Total Qty Relax</th>
										<th>Lot</th>
										<th>Machine</th>
										<th>Status</th>
										<th>Last locator</th>
										<th>Remark</th>
										<th>Remark Planning</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::hidden('active_tab','reguler' , array('id' => 'active_tab')) !!}
	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script>
        $(function()
        {

            var active_tab = $('#page').val();

            var active_tab = $('#active_tab').val();
            if(active_tab == 'reguler')
            {
                $('#active_tab_export').val('reguler')
                regulerTables();

                var dtablRegulerTable = $('#reguler_table').dataTable().api();
                $(".dataTables_filter input")
                    .unbind() // Unbind previous default bindings
                    .bind("keyup", function (e) { // Bind our desired behavior
                        // If the user pressed ENTER, search
                        if (e.keyCode == 13) {
                            // Call the API search function
                            dtablRegulerTable.search(this.value).draw();
                        }
                        // Ensure we clear the search if they backspace far enough
                        if (this.value == "") {
                            dtablRegulerTable.search("").draw();
                        }
                        return;
                });
				dtablRegulerTable.on('preDraw',function(){
					Pace.start();
					$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					})
				.on('draw.dt',function(){
					Pace.stop();
					$.unblockUI();
				});
                dtablRegulerTable.draw();
                $('#select_warehouse').on('change',function(){
                    dtablRegulerTable.draw();
                });

                $('#start_date').on('change',function(){
                    dtablRegulerTable.draw();
                });

                $('#end_date').on('change',function(){
                    dtablRegulerTable.draw();
                });

            }

            $('#active_tab').on('change',function()
            {
                $('#active_tab_export').val('reguler')
                regulerTables();

                var dtablRegulerTable = $('#reguler_table').dataTable().api();
                $(".dataTables_filter input")
                    .unbind() // Unbind previous default bindings
                    .bind("keyup", function (e) { // Bind our desired behavior
                        // If the user pressed ENTER, search
                        if (e.keyCode == 13) {
                            // Call the API search function
                            dtablRegulerTable.search(this.value).draw();
                        }
                        // Ensure we clear the search if they backspace far enough
                        if (this.value == "") {
                            dtablRegulerTable.search("").draw();
                        }
                        return;
                });
                dtablRegulerTable.draw();
                $('#select_warehouse').on('change',function(){
                    dtablRegulerTable.draw();
                });

                $('#start_date').on('change',function(){
                    dtablRegulerTable.draw();
                });

                $('#end_date').on('change',function(){
                    dtablRegulerTable.draw();
                });


            });

        });


        function regulerTables()
        {
            $('#reguler_table').DataTable().destroy();
            $('#reguler_table tbody').empty();

            var regulerTable =  $('#reguler_table').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength:100,
                ajax: {
                    type: 'GET',
                    url: '/report-fabric-prepare/data-reguler',
                    data: function(d) {
                        return $.extend({}, d, {
                            "warehouse" 	: $('#select_warehouse').val(),
                            "start_date" 	: $('#start_date').val(),
                            "end_date" 		: $('#end_date').val(),
                        });
                    }
                },

                fnCreatedRow: function (row, data, index) {
                    var info = regulerTable.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'id', name: 'id',visible:false},
                    {data: 'warehouse_id', name: 'warehouse_id'},
                    {data: 'planning_date', name: 'planning_date',searchable:true},
                    {data: 'is_piping', name: 'is_piping',searchable:false},
                    {data: 'document_no', name: 'document_no',orderable:true},
                    {data: 'supplier_name', name: 'supplier_name'},
                    {data: 'item_code', name: 'item_code',orderable:true},
                    {data: 'item_code_source', name: 'item_code_source'},
                    {data: 'po_buyer', name: 'po_buyer'},
                    {data: 'article_no', name: 'article_no',orderable:true,searchable:true},
                    {data: '_style', name: '_style',orderable:true,searchable:true},
                    {data: 'uom', name: 'uom'},
                    {data: 'total_reserved_qty', name: 'total_reserved_qty',orderable:true,searchable:false},
                    {data: 'total_qty_outstanding', name: 'total_qty_outstanding',orderable:true,searchable:false},
                    {data: 'total_qty_rilex', name: 'total_qty_rilex',searchable:false,orderable:true},
                    {data: 'actual_lot', name: 'actual_lot',searchable:false,orderable:true},
                    {data: 'machine', name: 'machine',searchable:false,orderable:true},
                    {data: 'status', name: 'status',searchable:false,orderable:true},
                    {data: 'last_locator_code', name: 'last_locator_code',searchable:true,orderable:true},
                    {data: 'remark', name: 'remark',searchable:false},
                    {data: 'remark_planning', name: 'remark_planning',searchable:false},
                    {data: 'action', name: 'action',searchable:false,orderable:false},
                ]
            });
        }

    </script>
@endsection
