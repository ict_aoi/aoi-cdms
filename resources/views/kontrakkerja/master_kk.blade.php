@extends('layouts.app',['active' => 'master_kk'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Kontrak Kerja </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Master Kontrak Kerja</li>
            <li class="active">Create Kontrak Kerja</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            @csrf
            <div class="col-lg-4">
                <label><b>Nomor Kontrak Kerja</b></label>
                <input type="text" name="no_kk" id="no_kk" class="form-control" placeholder="Nomor Kontrak Kerja" required="">
            </div>
            <div class="col-lg-4">
                <label><b>Subcont</b></label>
                <select data-placeholder="Search Subcont" name="subcont_id" id="subcont_id" class="form-control select-search" required="">
                    <option value=""></option>
                    @foreach($subcont as $sub)
                        <option value="{{ $sub->id }}">{{ $sub->subcont_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-2">
                <label><b>Total Qty</b></label>
                <input type="number" name="total" id="total" class="form-control" required="">
            </div>
            <div class="col-lg-2">

                <button type="button" style="margin-top: 25px;" class="btn btn-primary" id="btn-save" name="btn-save">SIMPAN</button>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-lg-4">
                <label><b>PO Buyer</b></label>
                <select data-placeholder="Search Po Buyer" name="po_buyer" id="po_buyer" class="form-control select-search">
                    <option value=""></option>
                    @foreach($polist as $po)
                        <option value="{{ $po->po_buyer.'&'.$po->style }}">{{ $po->po_buyer }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>PO Buyer</th>
                            <th>Style</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@section('page-js')
<script type="text/javascript">

$(document).ready(function(){
    var dataset = [];

    var i = 1;

    $('#po_buyer').on('change',function(){
        var str = $('#po_buyer').val();
        var po = str.split('&')[0];
        var style = str.split('&')[1];

        dataset.push({
            'idx': i,
            'po_buyer':po,
            'style' : style
        });
        i++;
        table.clear();
        table.rows.add(dataset);
        table.draw();
    });

    var table = $('#table-list').DataTable({
            processing: true,
            serverSide: false,
            deferRender:true,
            searching:false,
            data : dataset,
            columns:[
                {title:'#',data:'idx'},
                {title:'PO Buyer',data:'po_buyer'},
                {title:'Style',data:'style'}
            ]
    });


    $('#btn-save').on('click',function(event){

        if (dataset.length > 0) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url : "{{ route('MasterKK.create_kk') }}",
                data: {
                    no_kk: $('#no_kk').val(),
                    subcont_id: $('#subcont_id').val(),
                    total: $('#total').val(),
                    dataset : dataset
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(response) {
                    var data_response = response.data;
                    $.unblockUI();
                    if (data_response.status == 200) {
                        $("#alert_success").trigger("click", data_response.output);
                        window.location.reload();
                        
                    }else{
                        $("#alert_warning").trigger("click",data_response.output);
                       
                    }
                    
                },
                error: function(response) {
                    var data_response = response.data;
                    $("#alert_warning").trigger("click",data_response.output);
                    $.unblockUI();
                }
            });
        }else{
            $("#alert_warning").trigger("click","Data Po Buyer Cannot Empty");
        }

        
    });

   
    
});


</script>
@endsection

