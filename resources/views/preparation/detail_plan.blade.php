@extends('layouts.app',['active' => 'cuttingplan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Set Plan </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Preparation</li>
            <li class="active">Set Plan</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group " id="filter_by_lcdate">
            <div class="row">
                <div class="col-lg-1">
                    <label style="padding-top: 10px;">PCD : </label>
                </div>
                <div class="col-lg-1">
                    <input type="text" class="form-control" name="cutting-date" id="cutting-date" value="{{$date_cutting}}" readonly="">
                </div>
                <div class="col-lg-1">
                    <label style="padding-top: 10px;">Style : </label>
                </div>
                <div class="col-lg-2">
                   <input type="text" class="form-control" name="style" id="style" value="{{$style}}" readonly="">
                </div>
                <div class="col-lg-1">
                    <label style="padding-top: 10px;">Article : </label>
                </div>
                <div class="col-lg-2">
                  <input type="text" class="form-control" name="article" id="article" value="{{$article}}" readonly="">
                  <input type="text" class="form-control hidden" name="size" id="size" value="{{$size_category}}">
                  <input type="text" class="form-control hidden" name="mo_created" id="mo_created" value="{{$mo_created}}" >
                </div>
                <div class="col-lg-2">
                    <input type="text" class="form-control" name="color" id="color" value="{{$color}}" readonly="">
                </div>
                 <div class="col-lg-2">
                        <a href="#" class="btn btn-default" id="btn-add"><i class="icon-plus2 position-left"></i>Add Queu</a>
                </div>
                
                
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Queu</th>
                        <th>Document No</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalAddQueu"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-2">
                        <label>Date</label>
                        <input class="form-control" type="text" name="datem" id="datem" readonly="" >
                    </div>
                    <div class="col-lg-2">
                        <label>Style</label>
                        <input class="form-control" type="text" name="stylem" id="stylem" readonly="">
                    </div>
                    <div class="col-lg-3">
                        <label>Article</label>
                        <input class="form-control" type="text" name="articlem" id="articlem" readonly="">
                        <input class="form-control" type="text" name="colorm" id="colorm" readonly="">
                    </div>
                    <div class="col-lg-2">
                        <label>Size Ctg</label>
                        <input class="form-control" type="text" name="sizem" id="sizem" readonly="">
                        <input class="form-control hidden" type="text" name="mo_createdm" id="mo_createdm">
                    </div>
                    <div class="col-lg-2">
                        <label>Set Queu</label>
                        <input type="text" name="queu" id="queu" class="form-control" required="" placeholder="Queu Number">
                    </div>
                    <div class="col-lg-1">
                        <label style="color: white;">button</label><br>
                        <button class="btn btn-primary btn-save" type="button" id="btnsave"><span class="icon-stack-plus"></span></button>
                    </div>
                    
                </div>
                <hr>
                 <!-- <div class="row">
                    <div class="col-lg-4">
                        <label><b>Queu</b></label>
                        <input type="text" name="queu" class="form-control" id="queu">
                    </div>
                    <div class="col-lg-4">
                        <label><b>Remark</b></label>
                        <input type="text" name="remark" class="form-control" id="remark">
                    </div>
                    <div class="col-lg-4">
                       
                        <button style="margin-top: 20px;" type="button" id="btn-submit" name="btn-submit" class="btn btn-primary"><span class="icon-stack-plus"></span></button>
                    </div>
                </div> -->

                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-list-md">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="toggle(this)"></th>
                                    <th>Po Buyer</th>
                                    <th>style</th>
                                    <th>Article</th>
                                </tr>
                            </thead>
                        </table>
                    </div>  
                </div>
            </div>
           

        </div>
    </div>
</div>

<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="table-list-mo">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Po Buyer</th>
                            </tr>
                        </thead>
                    </table>
                </div>                
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $('#btn-add').click(function(){
        $('#modalAddQueu').modal('show');
        $('#datem').val($('#cutting-date').val());
        $('#stylem').val($('#style').val());
        $('#articlem').val($('#article').val());
        $('#sizem').val($('#size').val());
        $('#colorm').val($('#color').val());
        $('#mo_createdm').val($('#mo_created').val());

        var tablemd = $('#table-list-md').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.getMo') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "cutting_date"         : $('#datem').val(),
                         "style"         : $('#stylem').val(),
                         "articleno"         : $('#articlem').val(),
                         "color"         : $('#colorm').val(),
                         "size_category":$('#sizem').val(),
                         "mo_created":$('#mo_createdm').val()
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tablemd.page.info();
                var value = index+1+info.start;
                $('td', row).eq(1).css('min-width', '100px');
            },
            columns: [
                // {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox',sortable: false, orderable: false, searchable: false},
                {data: 'po_buyer', name: 'po_buyer'},
                {data: 'style', name: 'style'},
                {data: 'articleno', name: 'articleno'}
            ]
        });

        
    });

    var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.ajaxGetQueu') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "cutting_date"         : $('#cutting-date').val(),
                         "style"         : $('#style').val(),
                         "articleno"         : $('#article').val(),
                         "size_category":$('#size').val()
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(2).css('width','100px');
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'queu', name: 'queu'},
                {data: 'po_buyer', name: 'po_buyer'},
                {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
            ]
        });

    // $('#btn-submit').click(function(){
    //     event.preventDefault();

    //     $.ajaxSetup({
    //         headers:{
    //             'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
    //     $.ajax({
    //         type:'GET',
    //         url : "{{ route('preparation.insertQueu') }}",
    //         data:{cutting_date:$('#datem').val(),style:$('#stylem').val(),articleno:$('#articlem').val(),size_category:$('#sizem').val(),queu:$('#queu').val(),remark:$('#remark').val()},
    //         beforeSend:function(){
    //             loading_process();
    //         },
    //         success:function(response){
    //             $('#modalAddQueu').modal('hide');
    //             $('#table-list').unblock();

    //             tableL.clear();
    //             tableL.draw();
    //         },
    //         error:function(response){
    //            console.log('error');
    //         }
    //     });
    // });

    $('#table-list').on('click','.btn-detail',function(){
        $('#modalDetail').modal('show');

        var id_plan = $(this).data('id');
        var tablemo = $('#table-list-mo').DataTable({
            processing: true,
            destroy : true,
            serverSide: true,
            deferRender:true,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: "{{ route('preparation.ajaxGetDetailMo') }}",
                data: function(d) {
                     return $.extend({}, d, {
                         "id_plan"         :id_plan
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = tablemo.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);

            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'po_buyer', name: 'po_buyer'}
            ]
        });


        
    });

    // $('#modalDetail').on('hidden.bs.modal', function(){
    //         // $('#form_filter_date').submit();
    //         // $('#file_marker').val(null);
    //         $('#table-list-mo').clear();
    // });
});

function deleteQueu(e){
    var _token = $("input[name='_token]").val();
    var id = e.getAttribute('data-id');

    bootbox.confirm("Are you sure delete this ?",
        function(result){

            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });


                $.ajax({
                        url:"{{ route('preparation.deleteQueu') }}",
                        type: "POST",
                        data:{
                            "id": id,                           
                            "_token": _token,
                        },
                        beforeSend:function(){
                            loading_process();
                        },
                        success: function(response){
                            // myalert('success', 'Invoice cancel successfully');
                            
                            $('#table-list').unblock();
                            window.location.reload();
                        },
                        error: function(response) {
                            // if(response['status'] == 422) {
                            //     myalert('error', 'Oopss Something wrong..!');
                            // }

                            console.log("error");
                        }
                    });

            }
        });
}

function toggle(source) {
    checkboxes = document.getElementsByName('selector[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
}

$('#btnsave').on('click',function(){
    var data = [];

    var queu = $('#queu').val();
    var date = $('#datem').val();
    var style = $('#stylem').val();
    var color = $('#colorm').val();
    var article = $('#articlem').val();
    var sizectg = $('#sizem').val();
    var mo_created = $('#mo_createdm').val();

    $('.setmo:checked').each(function(){
        data.push({
            docmo :$(this).val()
        });
    });

    if (data.length > 0) {

        if ($('#queu').val()=='') {
            myalert('warning','Queu Cannot empty');
            return false;
        }

        $.ajaxSetup({
            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
        });
        $.ajax({
            type:'POST',
            url : "{{ route('preparation.partialQueu') }}",
            data:{data:data,date:date,style:style,article:article,sizectg:sizectg,queu:queu,mo_created:mo_created,color:color},
            beforeSend:function(){
                loading_md();
            },
            success:function(response){
               $('modalAddQueu').modal('hide');
               window.location.reload();
            },
            error:function(response){
                console.log('error');
            }
        });
    }else{
        myalert('warning','Select mo first');
    }
});

function deleteMoDet(e){
    var _token = $("input[name='_token]").val();
    var id = e.getAttribute('data-id');
    var documentno = e.getAttribute('data-mo');

    bootbox.confirm("Are you sure delete this ?",
        function(result){

            if (result) {
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                        }
                });


                $.ajax({
                        url:"{{ route('preparation.deleteMoDet') }}",
                        type: "POST",
                        data:{
                            "id": id,
                            "documentno":documentno,                           
                            "_token": _token,
                        },
                        success: function(response){
                            // myalert('success', 'Invoice cancel successfully');
                            
                            $('#modalDetail').modal('hide');
                        },
                        error: function(response) {
                            // if(response['status'] == 422) {
                            //     myalert('error', 'Oopss Something wrong..!');
                            // }

                            console.log("error");
                        }
                    });

            }
        });
}
</script>
@endsection