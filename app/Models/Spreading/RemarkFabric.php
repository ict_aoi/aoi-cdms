<?php

namespace App\Models\Spreading;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RemarkFabric extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'remark_fabric';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['id_plan', 'cutting_date', 'style', 'articleno', 'material', 'color', 'part_no', 'remark', 'deleted_at', 'status'];
}