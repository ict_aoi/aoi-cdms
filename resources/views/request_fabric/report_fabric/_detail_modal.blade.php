<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Fabric</span>
				</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Cutting Date</strong></span>
							</div>
							<div class="col-md-8">
								<span id="cutting_date_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>Style</strong></span>
							</div>
							<div class="col-md-8">
								<span id="style_detail_view"></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<span><strong>Article</strong></span>
							</div>
							<div class="col-md-8">
								<span id="articleno_detail_view"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<span><strong>PO Buyer</strong></span>
							</div>
							<div class="col-md-8">
								<span id="po_detail_view"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-striped table-condensed" id="detailTable">
						<thead>
							<tr>
								{{-- <th>ID</th> --}}
                                <th>Material</th>
                                <th>Item ID</th>
								<th>Color</th>
								<th>Part No</th>
								<th>CSI Qty</th>
                                <th>SSP Qty</th>
								{{-- <th>Already Prepared</th> --}}
								<th>Qty Prepared</th>
								<th>Fabric Width</th>
								<th>Remark</th>
							</tr>
						</thead>
					</table>
				</div>

                <h5>Status Prepare Material</h5>
                <div class="table-responsive">
					<table class="table table-basic table-striped table-condensed" id="prepareTable">
						<thead>
							<tr>
								{{-- <th>ID</th> --}}
                                <th>Part</th>
                                <th>Qty CSI (YDS)</th>
								<th>Qty Prepare</th>
								<th>Balance</th>
								<th>%</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="planning_id" id="planning_id" />
				<input type="hidden" name="id_plan" id="id_plan" />
                <input type="hidden" name="po_buyer" id="po_buyer" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
