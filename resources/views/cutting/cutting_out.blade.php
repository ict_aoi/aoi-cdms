@extends('layouts.app',['active' => 'cutting_out'])
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting OUT</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Cutting OUT</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-md-2">
                <label style="padding-top: 10px;"><b>Scan ID Marker</b></label>
            </div>
            <div class="col-md-10">
                <input type="text" name="barcode_id" id="barcode_id" class="form-control" placeholder="Barcode Marker" autofocus="">
            </div>
        </div>
    </div>
</div>
@endsection
