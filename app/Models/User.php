<?php namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'name', 'email', 'password','photo','sex','nik','photo','email_verified_at', 'factory_id', 'last_login_at', 'status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory', 'factory_id', 'id');
    }

    public function page_access()
    {
        return $this->hasMany('App\Models\TempPageAccess', 'user_id', 'id');
    }

    public function cutting_marker()
    {
        return $this->hasMany('App\Models\CuttingMarker', 'user_updated_id', 'id');
    }

    public function file_excel()
    {
        return $this->hasMany('App\Models\RequestRatio\FileExcel', 'user_id', 'id');
    }

    public function file_marker()
    {
        return $this->hasMany('App\Models\RequestRatio\FileMarker', 'user_id', 'id');
    }

    public function file_excel2()
    {
        return $this->hasMany('App\Models\RequestMarker\FileExcel', 'user_id', 'id');
    }

    public function user_release()
    {
        return $this->hasMany('App\Models\MarkerReleaseDetail', 'release_by', 'id');
    }

    public function user_confirm()
    {
        return $this->hasMany('App\Models\MarkerReleaseDetail', 'confirm_by', 'id');
    }

    public function distribusi_movement()
    {
        return $this->hasMany('App\Models\DistribusiMovement', 'user_id', 'id');
    }
    public function report_roll()
    {
        return $this->hasMany('App\Models\RequestFabric\ReportRoll','user_id','id');
    }

    public function temp_sds_removal()
    {
        return $this->hasMany('App\Models\TempTicketSdsRemove', 'created_by', 'id');
    }
}
