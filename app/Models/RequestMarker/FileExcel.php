<?php

namespace App\Models\RequestMarker;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class FileExcel extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'file_ssps';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['cutting_plan_id', 'file_name', 'file_link', 'deleted_at', 'user_id', 'terima_user_id', 'terima_at'];

    public function cutting_plan()
    {
        return $this->belongsTo('App\Models\CuttingPlan', 'cutting_plan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function terima_user()
    {
        return $this->belongsTo('App\Models\User', 'terima_user_id', 'id');
    }
}
