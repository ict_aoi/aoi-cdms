@extends('layouts.app',['active' => 'document_bc'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Document BC IN & OUT </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Document BC IN & OUT</li>
            <li class="active">Upload Document BC</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_by_lcdate">
            <div class="col-lg-9">
                <label><b>Choose BC Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="text" class="form-control daterange-basic" name="date_range" id="date_range">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <button style="margin-top: 26px;" type="button" class="btn btn-default" id="btn-upload"><span class="icon-cloud-upload2"></span> Upload Document</button>
            </div>
        </div>
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Factory</th>
                            <th>Document Type</th>
                            <th>No Aju</th>
                            <th>No Pabean</th>
                            <th>Tanggal Pabean</th>
                            <th>No Penerimaan</th>
                            <th>Tanggal Penerimaan</th>
                            <th>Subcont</th>
                            <th>Item Code</th>
                            <th>Item Name</th>
                            <th>Uom</th>
                            <th>Qty</th>
                            <th>Item Value</th>
                            <th>Currency</th>
                            <th>Tanggal Upload</th>
                            <th>No KK</th>
                            <th>Product</th>
                        </tr>
                    </thead>
                </table>
            </div>  
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4>Upload Document BC</h4></center>
            </div>
            <div class="modal-body">
                <form action="{{ route('Exim.uploaddocumentbc') }}" method="POST" enctype="multipart/form-data" id="form-upload">
                    @csrf
                    <div class="row form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-1">
                            <label style="margin-top: 10px;"><b>File : </b></label>
                        </div>
                        <div class="col-lg-2">
                            <input type="file" name="file_upload" class="form-control" id="file_upload">
                        </div>
                        <div class="col-lg-1">
                            <button class="btn btn-success" type="submit" id="btn-save"><span class="icon-cloud-upload2"></span> Upload Document</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
  
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        pageLength:50,
        deferRender:true,
        scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('Exim.getDataDocbc') }}",
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'factory_name', name: 'factory_name'},//1
            {data: 'document_type', name: 'document_type'},//2
            {data: 'aju_no', name: 'aju_no'},//3
            {data: 'pabean_no', name: 'pabean_no'},//4
            {data: 'pabean_date', name: 'pabean_date'},//5
            {data: 'receive_no', name: 'receive_no'},//6
            {data: 'receive_date', name: 'receive_date'},//7
            {data: 'subcont_factory', name: 'subcont_factory'},//8
            {data: 'item_code', name: 'item_code'},//9
            {data: 'item_name', name: 'item_name'},//10
            {data: 'uom', name: 'uom'},//11
            {data: 'qty', name: 'qty'},//12
            {data: 'item_value', name: 'item_value'},//13
            {data: 'currency', name: 'currency'},//14
            {data: 'updated_at', name: 'updated_at'},//15
            {data: 'kk_no', name: 'kk_no'},//15
            {data: 'product', name: 'product'}//15
        ]
    });
    tableL.ajax.reload();

    tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').on('click',function(){

        if ($('#date_range').val()=='') {
            $("#alert_warning").trigger("click",'Please select date !!!');
            return false;
        }
        tableL.draw();
    });


    $('#btn-upload').on('click',function(){
        $('#modalUpload').modal('show');
    });

    $('#form-upload').submit(function(event){
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['file_upload'];
        var val = [$('#file_upload').val()];

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },
            success: function(response) {
                $('#table-list').unblock();
                myalert('success','Data Berhasil Diupload');                
                $('#modalUpload').modal('hide');
                $('#file_upload').val('');
                $.unblockUI();
            },
            error: function(response) {
                $('#table-list').unblock();
                var responses = response['statusText'] != 'Forbidden' ? response['responseJSON'] : 'Terjadi kesalahan atau cek hak akses anda..!';
                myalert('error', responses);
                $('#modalUpload').modal('hide');
                $('#file_upload').val('');
                $.unblockUI();
                console.log(response);
            }
        });

    });


})
</script>
@endsection