@extends('layouts.app',['active' => 'permintaan_fabric_submit'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Permintaan Fabric</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Permintaan Fabric</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
	<div class="panel-body">
		<form class="form-horizontal" action="#" id="form_filter_date" method="POST">
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2 text-bold">Input Cutting Date</label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-btn">
								<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
							</span>
							<input type="text" class="form-control pickadate" name="cutting_date" placeholder="Enter Cutting Date" id="cutting_date">
							<span class="input-group-btn">
								<button class="btn btn-primary legitRipple" id='btn-filter' type="submit">Select</button>
							</span>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="planningTable">
                <thead>
                    <tr>
						<th>#</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO</th>
                        <th>Size Type</th>
                        <th>Queue</th>
                        {{-- <th>Release Marker</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('reqFabricSubmit.dataPlanning') }}" id="dataPlanningLink"></a>
<a href="{{ route('reqFabricSubmit.dataDetail') }}" id="dataDetailLink"></a>
<a href="{{ route('reqFabricSubmit.addQueue') }}" id="QueueLink"></a>
{{-- <a href="{{ route('reqFabricSubmit.editQueue') }}" id="edQueueLink"></a> --}}
@endsection

@section('page-modal')
	@include('request_fabric.request_fabric_submit._detail')
@endsection

@section('page-js')
<script>
    $(document).ready(function(){
        $.extend( $.fn.dataTable.defaults, {
            stateSave: true,
            autoWidth: false,
            autoLength: false,
            processing: true,
            // serverSide: true, //sync
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            }
        });

        var table = $('#planningTable').DataTable({
            ajax: {
                type: 'get',
                url: $('#dataPlanningLink').attr('href'),
                data: {
                    cutting_date: $('#cutting_date').val(),
                },
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style',searchable:true,orderable:true},
                {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
                {data: 'po', name: 'po',searchable:true},
                {data: 'size_category', name: 'size_category'},
                {data: 'queue', name: 'queue',searchable:false, orderable:true},
                // {data: 'release_marker', name: 'release_marker'},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ],
        });

        $('#planningTable').on('blur','.queue_unfilled', function(){

            var id = $(this).data('id');
            var queu = $(this).val();
            // console.log(queu);
            if (queu!="" || queu.trim()!="") {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url : $('#QueueLink').attr('href'),
                    data: {plan_id:id,queue:queu},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        var data_response = response.data;

                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);

                        }else{
                            $("#alert_warning").trigger("click",data_response.output);

                        }
                        $('#queu_'+id).html(response);
                        $('#btn-filter').click();
                    },
                    eror:function(response){
                        myalert('error',response);
                        return false;
                        $('#btn-filter').click();
                    }

                });
            }
        });

        $('#planningTable').on('click','.ignore-click', function() {
            return false;
        });

        $('#form_filter_date').submit(function(event){
            event.preventDefault();
            var cutting_date = $('#cutting_date').val();

            if(!cutting_date){
                $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'get',
                url : $('#dataPlanningLink').attr('href'),
                data: {
                    cutting_date: cutting_date,
                },
                beforeSend: function() {
                    $('#planningTable').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('#planningTable').unblock();

                    table.clear();
                    table.rows.add(response['data']);
                    table.draw();
                },
                error: function(response) {
                    $('#planningTable').unblock();
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });
    });

    function tableDetail() {
        var table2 = $('#detailRequestFabricTable').DataTable({
            ajax: {
                type: 'get',
                url: $('#dataDetailLink').attr('href'),
                data: {
                    req_fabric_id: $('#req_fabric_id').val(),
                },
            },
            fnCreatedRow: function (row, data, index) {
                var info = table2.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columnDefs: [
                {
                    className: 'dt-center'
                },
            ],
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'style', name: 'style', searchable:true,orderable:true},
                {data: 'articleno', name: 'articleno', searchable:true,orderable:true},
                {data: 'item_code', name: 'item_code', searchable:true,orderable:true},
                {data: 'part_no', name: 'part_no', searchable:true,orderable:true},
                {data: 'uom', name: 'uom', searchable:true,orderable:true},
                {data: 'csi', name: 'csi', searchable:true,orderable:true},
                {data: 'ssp', name: 'ssp', searchable:true,orderable:true},
                {data: 'bm', name: 'bm', searchable:true,orderable:true},
                {data: 'created_by', name: 'created_by', searchable:true,orderable:true},
                {data: 'created_at', name: 'created_at', searchable:true,orderable:true},
            ],
        });
    }

    function detail_queue(url)
    {
        $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();

            }
        })
        .done(function (response) {
            // $('#update_production').attr('action', response.url_update);
            $('#req_fabric_id').val(response.id);
            console.log(response.id);
            $('#detailRequestFabricModal').modal();
            $('#detailRequestFabricTable').DataTable().destroy();
            tableDetail();
            // $('#detailRequestFabricTable').DataTable().ajax.reload();
            // table2.draw();
            // $('#detailRequestFabricTable').DataTable().ajax.reload();
            console.log($('#req_fabric_id').val());
        });
    }

    function delete_queue(url)
    {
        bootbox.confirm("Apakah anda yakin akan menghapus data ini ?", function (result) {
            if(result){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "delete",
                    url: url,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function (response) {
                        $.unblockUI();
                    },
                    error: function (response) {
                        $.unblockUI();
                    }
                }).done(function ($result) {
                    $("#alert_success").trigger("click", 'Data Berhasil hapus');
                    // $('#planningTable').DataTable().ajax.reload();
                    $('#btn-filter').click();
                });
            }
        });
    }

</script>
@endsection
