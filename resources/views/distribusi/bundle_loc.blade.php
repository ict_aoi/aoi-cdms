@extends('layouts.scan',['active' => 'dashboard-scan'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title text-center title-distribusi">
            <h1 class="white-text"><span class="text-semibold" style="color:white">BUNDLE LOCATOR</span> </h1>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Bundle Locator</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<fieldset class="content-group">
    <div class="row text-center">
        <div class="col-md-4">
            <a href="{{ route('distribusi.index') }}" class="btn btn-sm btn-info btn-custom-header pull-left"> BACK </a>
        </div>
        <div class="col-md-4 pull-right">
            <a href="#"onclick="event.preventDefault(); document.getElementById('logout-form').submit();"class="btn btn-sm btn-danger pull-right btn-custom-header">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</fieldset>

<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_input" name="barcode_input" placeholder="#Scan Barcode Number" autofocus autocomplete="off" />
                </div>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-hover table-condensed" id="table-list">
                <thead>
                    <tr class="bg-teal-700">
                        <th>No</th>
                        <th>Is Supplied</th>
                        <th>Last Status</th>
                        <th>Location</th>
                        <th>Style</th>
                        <th>PO Buyer</th>
                        <th>Article</th>
                        <th>Cut | Sticker</th>
                        <th>Size</th>
                        <th>Komponen</th>
                        <th>Barcode Number</th>
                        <th>Qty</th>
                        <th>User</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('distribusi.getDataBundleLoc') }}" id="bundleLocLink"></a>
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        setTimeout(function(){
            location.reload();
        },180000);
        $.extend( $.fn.dataTable.defaults, {
            destroy:true,
            scrollX:true,
            scrollY:true,
            searching:false,
            paginate:false,
            pageLenght: false,
            autoWidth:false,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
            }
        });
        $('#barcode_input').keypress(function(event){
            if(event.which == 13) {
                event.preventDefault();
                var barcode_input = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'get',
                    url: $('#bundleLocLink').attr('href'),
                    data: {
                        barcode_input: barcode_input,
                    },
                    beforeSend: function(response) {
                        $.blockUI({
                            message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait',
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent',
                            },
                            theme: true,
                            baseZ: 2000
                        });
                        
                    },
                    success: function(response) {
                        bundle_loc_data(barcode_input);
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        $('#barcode_input').val('').focus();
                    }
                });
            }
        });
    });
    function bundle_loc_data(barcode_input){
        var tableL = $('#table-list').DataTable({
            processing: true,
            serverSide: true,
            deferRender:true,
            destroy:true,
            scrollX:true,
            scrollY:true,
            autoWidth:false,
            dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax: {
                type: 'GET',
                url: '/scan-distribusi/data-bundle-loc',
                data: function(d) {
                    return $.extend({}, d, {
                        "barcode_input"		: barcode_input
                    });
                },
            },
            fnCreatedRow: function (row, data, index) {
                var info = tableL.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},//0
                {data: 'is_supplied', name: 'is_supplied',searchable:false,visible:true,orderable:false},//12
                {data: 'current_description', name: 'current_description',searchable:true,visible:true,orderable:true},//12
                {data: 'line', name: 'line',searchable:true,visible:true,orderable:true},//12
                {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//2
                {data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:false},//3
                {data: 'article', name: 'article',searchable:false,visible:true,orderable:false},//4
                {data: 'cut_sticker', name: 'cut_sticker',searchable:false,visible:true,orderable:true},//5
                {data: 'size', name: 'size',searchable:true,visible:true,orderable:true},//6
                {data: 'komponen_name', name: 'komponen_name',searchable:true,visible:true,orderable:true},//7
                {data: 'barcode_id', name: 'barcode_id',searchable:true,visible:true,orderable:true},//8
                {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},//11
                {data: 'user_scan', name: 'user_scan',searchable:true,visible:true,orderable:true},//13
            ]
        });
        tableL.on('preDraw',function(){
            Pace.start();
        })
        .on('draw.dt',function(){
            Pace.stop();
            $.unblockUI();
        });

    }
</script>
@endsection
