<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Cutting extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'cutting';
	protected $fillable = ['id_marker','user_id','created_at','updated_at','deleted_at','finished_at'];
}
