<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingMarker;
use App\Models\CuttingPlan;
use App\Models\RasioMarker;
use App\Models\TempPageAccess;
use App\Models\CombinePart;
use App\Models\Factory;
use App\Models\MarkerReleaseHeader;
use App\Models\MarkerReleaseDetail;
use App\Http\Controllers\Controller;

class LaporanBaruMarkerController extends Controller
{
    public function index()
    {
        TempPageAccess::where('user_id', \Auth::user()->id)->where('page', 'actual-marker')->delete();
        $factorys = Factory::where('deleted_at', null)->where('status', 'internal')->orderBy('factory_alias', 'asc')->get();
        return view('request_marker.laporan_baru_marker.index', compact('factorys'));
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;

            $factory = isset($request->factory) ? $request->factory : \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('factory_id', $factory)->where('deleted_at', null);
                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')->where('cutting_date', $cutting_date)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M-Y');
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asian';
                    else return 'Inter';
                })
                ->addColumn('poreference', function($data) {
                    $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    return implode(', ', $po_array);
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('status', function($data) {
                    $po_array = $data->po_details->pluck('po_buyer')->toArray();
                    $check_plan = DB::table('data_cuttings')->where('plan_id', $data->id)->whereIn('po_buyer', $po_array)->get();
                    if (count($check_plan) < 1) {
                        return '<span class="badge bg-danger">Geser Plan</span>';
                    } else {
                        $check_marker = CuttingMarker::where('id_plan', $data->id)->whereNotNull('is_new')->whereNull('deleted_at')->get();
                        if(count($check_marker) > 0) {
                            return '<span class="badge bg-warning">Ada</span>';
                        } else {
                            return '<span class="badge bg-primary">Tidak ada</span>';
                        }
                    }
                })
                ->addColumn('last_updated', function($data) {
                    $get_marker = CuttingMarker::where('id_plan', $data->id)->whereNotNull('ratio_updated_at')->orderBy('ratio_updated_at', 'desc')->first();
                    if($get_marker != null) {
                        return $get_marker->user->name;
                    } else {
                        return '-';
                    }
                })
                ->addColumn('detail', function($data) {
                    return view('request_marker.laporan_baru_marker._action', [
                        'model'      => $data,
                        'detail'     => route('laporanBaruMarker.detailModal',$data->id)
                    ]);
                })
                ->rawColumns(['detail', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;

            if($planning_id != NULL) {
                $data = CuttingMarker::select('color', 'part_no', 'id_plan')->orderBy('part_no', 'asc')->where('id_plan', $planning_id)->where('deleted_at', null)->groupBy('color', 'part_no', 'id_plan');

                return datatables()->of($data)
                ->addColumn('detail', function($data) {
                    return view('request_marker.laporan_baru_marker._action', [
                        'model'      => $data,
                        'detailDetail'     => route('laporanBaruMarker.detailDetailModal', ['id' => $data->id_plan, 'part_no' => $data->part_no])
                    ]);
                })
                ->addColumn('status', function($data) {
                    $data_is_null = 0;
                    $data_approved = 0;
                    $data_marker = CuttingMarker::where('id_plan', $data->id_plan)->where('part_no', $data->part_no)->where('color', $data->color)->where('deleted_at', null)->get();
                    foreach($data_marker as $a) {
                        if($a->approved == null) {
                            if($a->marker_length == null) {
                                $data_is_null++;
                            }
                        } else {
                            $data_approved++;
                        }
                    }

                    if($data_approved > 0) {
                        return '<span class="badge bg-success">Approved</span>';
                    } elseif($data_is_null > 0) {
                        return '<span class="badge bg-danger">Not Yet</span>';
                    } else {
                        return '<span class="badge bg-primary">Completed</span>';
                    }
                })
                ->addColumn('total_cutting', function($data) {
                    $data_marker = CuttingMarker::where('id_plan', $data->id_plan)->where('part_no', $data->part_no)->where('color', $data->color)->where('deleted_at', null)->get();
                    return count($data_marker);
                })
                ->rawColumns(['status','detail','action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('status', function($data) {
                    return null;
                })
                ->addColumn('detail', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['status','detail','action'])
                ->make(true);
            }
        }
    }

    public function dataDetailMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            if($planning_id != NULL) {
                $data = CuttingMarker::orderBy('cut', 'asc')->where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                return datatables()->of($data)
                ->editColumn('id_row', function($data) {
                    return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="id_marker-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="id_marker" value="'.$data->barcode_id.'" readonly /></div></div>';
                })
                ->addColumn('rasio', function($data) {
                    $list_ratio = array();
                    foreach($data->rasio_markers as $a) {
                        $list_ratio[] = $a->size.'-'.$a->ratio;
                    }
                    $data_temp = implode(', ', $list_ratio);

                    if($data->is_new == null) {
                        return $data_temp.'  (LAPORAN NORMAL)';
                    } else {
                        return '<textarea class="form-input" id="rasio">'.$data_temp.'</textarea>';
                    }
                })
                ->editColumn('layer', function($data) {
                    if($data->is_new == null) {
                        return $data->layer;
                    } else {
                        return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="layer-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="layer" value="'.$data->layer.'" /></div></div>';
                    }
                })
                ->addColumn('lebar_fb', function($data) {
                    $add_class = '';
                    if($data->bay_pass == 't') {
                        $add_class = ' bg-warning-300';
                    }
                    if($data->is_new == null) {
                        return $data->fabric_width;
                    } else {
                        return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control'.$add_class.'" name="fabric_width-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="fabric_width" value="'.$data->fabric_width.'" /></div></div>';
                    }
                })
                ->addColumn('lebar_marker', function($data) {
                    if($data->fabric_width != null && $data->fabric_width != 0) {
                        return $data->fabric_width - 0.5;
                    } else {
                        return $data->fabric_width;
                    }
                })
                ->addColumn('perimeter', function($data) {
                    if($data->is_new == null) {
                        return $data->perimeter;
                    } else {
                        return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control perimeter" name="perimeter-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="perimeter" value="'.$data->perimeter.'" /></div></div>';
                    }
                })
                ->addColumn('actual_marker', function($data) {
                    if($data->is_new == null) {
                        return $data->marker_length;
                    } else {
                        return '<div class="form-group"><div class="col-lg-10"><input type="text" class="form-control" name="marker_length-'.$data->barcode_id.'" onkeypress="isNumberDot(event)" id="marker_length" value="'.$data->marker_length.'" /></div></div>';
                    }
                })
                ->addColumn('action', function($data) {
                    if($data->is_new == null) {
                        return '<button class="btn btn-primary btn-sm" id="add_row" data-id="'.$data->barcode_id.'"><i class="icon icon-plus3"></i></button>';
                    } else {
                        return '<button class="btn btn-primary btn-sm" id="add_row" data-id="'.$data->barcode_id.'"><i class="icon icon-plus3"></i></button> <button class="btn btn-danger btn-sm" id="delete_row" data-id="'.$data->barcode_id.'"><i class="icon icon-cross"></i></button>';
                    }
                })
                ->rawColumns(['id_row', 'perimeter', 'rasio', 'actual_marker', 'layer', 'lebar_fb', 'action','last_edit'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataLebarFB(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            if($planning_id != NULL) {
                $data = array();

                $data_marker_query = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                $data_marker_row = $data_marker_query->get()->first();

                $part_no_array = explode('+', $part_no);

                // $data_lebar = DB::connection('wms_live')
                // ->table('integration_whs_to_cutting')
                // ->select(DB::raw("actual_width, sum(qty_prepared) as alokasi_fb"))
                // ->where('style', $data_marker_row->cutting_plan->style)
                // ->where('planning_date', $data_marker_row->cutting_plan->cutting_date)->where('article_no', $data_marker_row->cutting_plan->articleno)
                // ->whereIn('part_no', $part_no_array)
                // ->groupBy('actual_width')
                // ->get();
                $style          = $data_marker_row->cutting_plan->style;
                $cutting_date   = $data_marker_row->cutting_plan->cutting_date;
                $article        = $data_marker_row->cutting_plan->articleno;
                $part           = "'".implode("','",$part_no_array)."'";
                $data_lebar = DB::connection('wms_live_new')->select(DB::raw("SELECT actual_width, sum(qty_prepared) as alokasi_fb FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$article' AND part_no IN($part) GROUP BY actual_width"));

                foreach($data_lebar as $a) {
                    $data_qty_kebutuhan = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->where('fabric_width', $a->actual_width)->sum('need_total');
                    $data[] = [
                        'lebar_fb' => $a->actual_width,
                        'alokasi_fb' => $a->alokasi_fb,
                        'need_total' => $data_qty_kebutuhan,
                    ];
                }

                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('lebar_fb', function($data) {
                    return null;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataSizeBalance(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $data_temp = array();
            $data = array();
            $data_rasio_size = array();

            if($planning_id != NULL) {
                $check_marker = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->where('combine_id', null)->get();

                if($check_marker->count() > 0) {
                    $data_marker = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->get();

                    $data_per_size = DB::table('jaz_detail_size_per_po')->select(DB::raw("size_finish_good, sum(qty) as qty"))->where('cutting_date', $data_marker->first()->cutting_plan->cutting_date)->where('queu', $data_marker->first()->cutting_plan->queu)->where('factory_id', $data_marker->first()->cutting_plan->factory_id)->whereIn('po_buyer', $data_marker->first()->cutting_plan->po_details->pluck('po_buyer')->toArray())->groupBy('size_finish_good')->get();
                } else {
                    $data_marker = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->get();

                    $plan_id_list = CombinePart::where('combine_id', $data_marker->first()->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();

                    $po_list = array();
                    $queu_list = array();

                    foreach($plan_id_list as $plan_id) {
                        $get_plan = CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first();
                        $queu_list[] = $get_plan->queu;
                        $get_po_array = $get_plan->po_details->pluck('po_buyer')->toArray();
                        foreach($get_po_array as $po_buyer) {
                            $po_list[] = $po_buyer;
                        }
                    }


                    $data_per_size = DB::table('jaz_detail_size_per_po')->select(DB::raw("size_finish_good, sum(qty) as qty"))->where('cutting_date', $data_marker->first()->cutting_plan->cutting_date)->whereIn('queu', $queu_list)->where('factory_id', $data_marker->first()->cutting_plan->factory_id)->whereIn('po_buyer', $po_list)->groupBy('size_finish_good')->get();
                }

                $data_temp[0][0] = 'Size';
                $data_temp[1][0] = 'Total Kebutuhan';
                $data_temp[2][0] = 'Total Marker';

                $size_plan = 0;
                $size_actual = 0;

                for($i1 = 0; $i1 < 16; $i1++) {
                    if(isset($data_per_size[$i1])) {
                        $data_temp[0][$i1 + 1] = $data_per_size[$i1]->size_finish_good;
                        $data_temp[1][$i1 + 1] = $data_per_size[$i1]->qty;
                        $size_plan = $data_per_size[$i1]->qty + $size_plan;
                    } else {
                        $data_temp[0][$i1 + 1] = null;
                        $data_temp[1][$i1 + 1] = null;
                    }
                }

                foreach($data_marker as $a) {
                    foreach($a->rasio_markers as $b) {
                        $data_rasio_size[] = [
                            'size' => $b->size,
                            'rasio' => $b->ratio * $a->layer,
                        ];
                    }
                }

                for($i2 = 1; $i2 < 16; $i2++) {
                    $jumlah_qty = 0;
                    foreach($data_rasio_size as $a) {
                        if($data_temp[0][$i2] != null) {
                            if($a['size'] == $data_temp[0][$i2]) {
                                $jumlah_qty += $a['rasio'];
                            }
                        }
                    }
                    $size_actual = $jumlah_qty + $size_actual;
                    if($jumlah_qty == 0) {
                        $jumlah_qty = null;
                    }
                    $data_temp[2][$i2] = $jumlah_qty;
                }

                $data_temp[0][16] = 'Total';
                $data_temp[1][16] = $size_plan;
                $data_temp[2][16] = $size_actual;

                for($i3 = 0; $i3 < 3; $i3++) {
                    if($i3 == 2) {
                        for($i4 = 0; $i4 < 17; $i4++) {
                            if($i4 == 0) {
                                $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                            } else {
                                if($data_temp[$i3][$i4] != null) {
                                    if($data_temp[$i3][$i4] > $data_temp[$i3-1][$i4]) {
                                        $data_temp[$i3][$i4] = '<div class="bg-danger">'.$data_temp[$i3][$i4].'</div>';
                                    } elseif($data_temp[$i3][$i4] < $data_temp[$i3-1][$i4]) {
                                        $data_temp[$i3][$i4] = '<div class="bg-orange-300">'.$data_temp[$i3][$i4].'</div>';
                                    } else {
                                        $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                    }
                                } else {
                                    $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                }
                            }
                        }
                    } else {
                        for($i5 = 0; $i5 < 16; $i5++) {
                            $data_temp[$i3][$i5] = $data_temp[$i3][$i5];
                        }
                    }

                    $data[] = [
                        'column1' => $data_temp[$i3][0],
                        'column2' => $data_temp[$i3][1],
                        'column3' => $data_temp[$i3][2],
                        'column4' => $data_temp[$i3][3],
                        'column5' => $data_temp[$i3][4],
                        'column6' => $data_temp[$i3][5],
                        'column7' => $data_temp[$i3][6],
                        'column8' => $data_temp[$i3][7],
                        'column9' => $data_temp[$i3][8],
                        'column10' => $data_temp[$i3][9],
                        'column11' => $data_temp[$i3][10],
                        'column12' => $data_temp[$i3][11],
                        'column13' => $data_temp[$i3][12],
                        'column14' => $data_temp[$i3][13],
                        'column15' => $data_temp[$i3][14],
                        'column16' => $data_temp[$i3][15],
                        'column17' => $data_temp[$i3][16],
                    ];
                }

                return datatables()->of($data)
                ->rawColumns(['column1', 'column2', 'column3', 'column4', 'column5', 'column6', 'column7', 'column8', 'column9', 'column10', 'column11', 'column12', 'column13', 'column14', 'column15', 'column16', 'column17'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('lebar_fb', function($data) {
                    return null;
                })
                ->addColumn('alokasi_fb', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataOverConsum(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $data = array();

            if($planning_id != NULL) {

                $check_marker = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->where('combine_id', null)->get();

                if($check_marker->count() > 0) {
                    $data_marker_query = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                    $data_marker = $data_marker_query->get()->first();

                    $part_no_array = explode('+', $part_no);

                    $data_uom_cons = DB::table('data_cuttings')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no_array)->first()->uom;
                    $data_cons = DB::table('data_cuttings')->where('plan_id', $data_marker->cutting_plan->id)->whereIn('part_no', $part_no_array)->sum('fbc');
                    if($data_uom_cons == 'M') {
                        $data_cons = $data_cons * 1.0936;
                    }

                    $data_qty_kebutuhan = $data_marker_query->sum('need_total');
                } else {
                    $data_marker_query = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null);

                    $data_marker = $data_marker_query->get()->first();

                    $part_no_array = explode('+', $part_no);

                    $plan_id_list = CombinePart::where('combine_id', $data_marker->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();

                    $queu_list = array();

                    foreach($plan_id_list as $plan_id) {
                        $get_plan = CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first();
                        $queu_list[] = $get_plan->queu;
                        $plan_id_list[] = $get_plan->id;
                    }

                    $data_uom_cons = DB::table('data_cuttings')->whereIn('plan_id', $plan_id_list)->whereIn('part_no', $part_no_array)->first()->uom;
                    $data_cons = DB::table('data_cuttings')->whereIn('plan_id', $plan_id_list)->whereIn('part_no', $part_no_array)->sum('fbc');
                    if($data_uom_cons == 'M') {
                        $data_cons = $data_cons * 1.0936;
                    }

                    $data_qty_kebutuhan = $data_marker_query->sum('need_total');
                }

                $data[] = [
                    'qty_csi' => $data_cons,
                    'qty_need' => $data_qty_kebutuhan,
                    'over_consum' => $data_cons - $data_qty_kebutuhan,
                ];

                return datatables()->of($data)
                ->rawColumns(['qty_csi', 'qty_need', 'over_consum'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->addColumn('qty_csi', function($data) {
                    return null;
                })
                ->addColumn('qty_need', function($data) {
                    return null;
                })
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function detailModal($id)
    {
        $get_data = CuttingPlan::where('id', $id)->where('deleted_at', null)->firstOrFail();
        $data_is_null = 0;
        $data_marker = CuttingMarker::where('id_plan', $get_data->id)->where('deleted_at', null)->get();
        foreach($data_marker as $a) {
            if($a->marker_length == null && $a->perimeter == null) {
                $data_is_null++;
            }
        }
        if($data_is_null > 0) {
            $can_download = false;
        } else {
            $can_download = true;
        }
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $get_data->cutting_date;
        $obj->style = $get_data->style;
        $obj->articleno = $get_data->articleno;
        $obj->size_category = $get_data->size_category;
        $obj->poreference = implode(', ', $get_data->po_details->pluck('po_buyer')->toArray());
        $obj->url_upload = '#';
        $obj->download = $can_download;

		return response()->json($obj,200);
    }

    public function detailDetailModal($id, $part_no)
    {
        $upload = CuttingMarker::where('id_plan', $id)->where('part_no', $part_no)->where('deleted_at', null)->firstOrFail();

        $check_page_access_user = TempPageAccess::where('page', 'actual-marker')->where('cutting_date', $upload->cutting_plan->cutting_date)->where('part_no', $upload->part_no)->where('user_id', \Auth::user()->id)->get();

        $po_list = array();

        if($upload->combine_id == null) {
            $po_list = implode(', ', $upload->cutting_plan->po_details->pluck('po_buyer')->toArray());
        } else {
            $get_plan_id = CombinePart::where('combine_id', $upload->combine_id)->where('deleted_at', null)->get();
            foreach($get_plan_id as $plan_id) {
                $get_po_array = CuttingPlan::where('id', $plan_id->plan_id)->where('deleted_at', null)->first()->po_details->pluck('po_buyer')->toArray();
                $po_list = array_merge($po_list, $get_po_array);
            }
        }

        if($check_page_access_user->count() > 0) {

            $obj = new StdClass();
            $obj->id_plan = $upload->id_plan;
            $obj->cutting_date = $upload->cutting_plan->cutting_date;
            $obj->style = $upload->cutting_plan->style;
            $obj->articleno = $upload->cutting_plan->articleno;
            $obj->size_category = $upload->cutting_plan->size_category;
            $obj->part_no = $upload->part_no;
            $obj->color = $upload->color;
            $obj->user = null;
            $obj->po_buyer = $po_list;
            $obj->url_upload = route('laporanBaruMarker.updateDataMarker');

            return response()->json($obj,200);

        } else {

            $check_page_access = TempPageAccess::where('page', 'actual-marker')->where('cutting_date', $upload->cutting_plan->cutting_date)->where('part_no', $upload->part_no)->get();

            if($check_page_access->count() > 0) {

                $obj = new StdClass();
                $obj->id_plan = $upload->id_plan;
                $obj->cutting_date = $upload->cutting_plan->cutting_date;
                $obj->style = $upload->cutting_plan->style;
                $obj->articleno = $upload->cutting_plan->articleno;
                $obj->size_category = $upload->cutting_plan->size_category;
                $obj->part_no = $upload->part_no;
                $obj->color = $upload->color;
                $obj->po_buyer = $po_list;
                $obj->user = $check_page_access->first()->user->name.' ('.$check_page_access->first()->user->nik.')';
                return response()->json($obj,200);

            } else {

                TempPageAccess::FirstOrCreate([
                    'page' => 'actual-marker',
                    'cutting_date' => $upload->cutting_plan->cutting_date,
                    'part_no' => $upload->part_no,
                    'queu' => $upload->cutting_plan->queu,
                    'user_id' => \Auth::user()->id,
                ]);

                $obj = new StdClass();
                $obj->id_plan = $upload->id_plan;
                $obj->cutting_date = $upload->cutting_plan->cutting_date;
                $obj->style = $upload->cutting_plan->style;
                $obj->articleno = $upload->cutting_plan->articleno;
                $obj->size_category = $upload->cutting_plan->size_category;
                $obj->part_no = $upload->part_no;
                $obj->color = $upload->color;
                $obj->user = null;
                $obj->po_buyer = $po_list;
                $obj->url_upload = route('laporanBaruMarker.updateDataMarker');

                return response()->json($obj,200);
            }
        }
    }

    public function updateDataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $data = $request->data;
            $planning_id = $request->planning_id;
            $part_no = '';

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    $data_cutting = CuttingMarker::findOrFail($data[$i][0]);
                    $data_cutting->perimeter = $data[$i][9];
                    $data_cutting->marker_length = $data[$i][10];
                    $data_cutting->over_consum = $data[$i][11];
                    $data_cutting->ratio_updated_at = Carbon::now();
                    $data_cutting->user_updated_id = \Auth::user()->id;
                    $data_cutting->save();
                }
                $part_no = $data[0][6];
            }

            return response()->json(['id_plan' => $planning_id, 'part_no' => $part_no],200);
        }
    }

    public function approveDataMarker(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->id_plan;
            $part_no = $request->part_no;

            $data_cutting = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->update(['approved' => Carbon::now()]);

            return response()->json(['id_plan' => $planning_id, 'part_no' => $part_no],200);
        }
    }

    public function addRow(Request $request)
    {
        if(request()->ajax())
        {
            $marker_id = $request->id;
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;
            $id_row_new = '';

            $data_cutting_all = CuttingMarker::where('barcode_id', 'like', 'MK-'.Carbon::now()->format('ymd').'%')->orderBy('barcode_id', 'desc')->get();
            if(count($data_cutting_all) > 0) {
                $id_row_new = substr($data_cutting_all->first()->barcode_id, 3, 12) + 1;
                $id_row_new = 'MK-'.$id_row_new;
            } else {
                $id_row_new = 'MK-'.Carbon::now()->format('ymd').'000001';
            }

            $data_cutting = '';

            if($marker_id == 'header_id') {
                $data_cutting = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->get();

                if(count($data_cutting) > 0) {
                    $data_cutting_updates = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('cut', '>=', 1)->get();

                    if(count($data_cutting_updates) > 0) {
                        foreach($data_cutting_updates as $updt) {
                            CuttingMarker::where('barcode_id', $updt->barcode_id)->update(['cut' => $updt->cut + 1]);
                        }
                    }

                    $add_row = CuttingMarker::insert([
                        'barcode_id' => $id_row_new,
                        'cut' => 1,
                        'id_plan' => $planning_id,
                        'color' => $color,
                        'layer' => 0,
                        'part_no' => $part_no,
                        'fabric_width' => null,
                        'qty_fabric' => null,
                        'perimeter' => null,
                        'marker_length' => null,
                        'over_consum' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $data_cutting->first()->item_id,
                        'ratio_updated_at' => Carbon::now(),
                        'user_updated_id' => \Auth::user()->id,
                    ]);
                }
            } else {
                $data_cutting = CuttingMarker::where('barcode_id', $marker_id)->first();

                if($data_cutting != null) {
                    $data_cutting_updates = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('cut', '>=', $data_cutting->cut + 1)->get();

                    if(count($data_cutting_updates) > 0) {
                        foreach($data_cutting_updates as $updt) {
                            CuttingMarker::where('barcode_id', $updt->barcode_id)->update(['cut' => $updt->cut + 1]);
                        }
                    }

                    $add_row = CuttingMarker::insert([
                        'barcode_id' => $id_row_new,
                        'cut' => $data_cutting->cut + 1,
                        'id_plan' => $data_cutting->id_plan,
                        'color' => $data_cutting->color,
                        'layer' => null,
                        'part_no' => $data_cutting->part_no,
                        'fabric_width' => null,
                        'qty_fabric' => null,
                        'perimeter' => null,
                        'marker_length' => null,
                        'over_consum' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $data_cutting->item_id,
                        'ratio_updated_at' => Carbon::now(),
                        'user_updated_id' => \Auth::user()->id,
                    ]);
                }
            }


            return response()->json(['marker_id' => $marker_id, 'add_row' => $data_cutting, 'planning_id' => $planning_id, 'part_no' => $part_no ,'color' => $color],200);
        }
    }

    public function deleteRow(Request $request)
    {
        if(request()->ajax())
        {
            $marker_id = $request->id;
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;
            $id_row_new = '';

            $data_cutting = CuttingMarker::where('barcode_id', $marker_id)->get()->first();

            if($data_cutting != null) {
                $data_cutting_updates = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('cut', '>', $data_cutting->cut)->get();

                if(count($data_cutting_updates) > 0) {
                    foreach($data_cutting_updates as $updt) {
                        CuttingMarker::where('barcode_id', $updt->barcode_id)->update(['cut' => $updt->cut - 1]);
                    }
                }

                $delete_row = CuttingMarker::where('barcode_id', $marker_id)->delete();
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function updateLayer(Request $request)
    {
        if(request()->ajax())
        {
            $marker_id = $request->id;
            $layer = $request->layer;
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;

            $data_check = CuttingMarker::where('barcode_id', $marker_id)->get()->first();
            if($data_check->marker_length != null && $data_check->marker_length > 0) {
                $need_total = ($data_check->marker_length + 0.75) * $layer / 36;
                $layer_update = CuttingMarker::where('barcode_id', $marker_id)->update([
                    'layer' => $layer,
                    'need_total' => $need_total,
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            } else {
                $layer_update = CuttingMarker::where('barcode_id', $marker_id)->update([
                    'layer' => $layer,
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function updateFabricWidth(Request $request)
    {
        if(request()->ajax())
        {
            $marker_id = $request->id;
            $fabric_width = $request->fabric_width;
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;

            $fabric_width_check = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('fabric_width', $fabric_width)->get()->first();

            if($fabric_width_check != null) {
                $data_cutting = CuttingMarker::where('barcode_id', $marker_id)->update([
                    'fabric_width' => $fabric_width,
                    'qty_fabric' => $fabric_width_check->qty_fabric,
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            } else {
                $data_cutting = CuttingMarker::where('barcode_id', $marker_id)->update([
                    'fabric_width' => $fabric_width,
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function updatePerimeter(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;
            $data_array = $request->data_array;

            foreach($data_array as $a) {
                $data_cutting = CuttingMarker::where('barcode_id', $a[0])->whereNull('deleted_at')->update([
                    'perimeter' => $a[1],
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function updateMarkerLength(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;
            $data_array = $request->data_array;

            foreach($data_array as $a) {
                $data_cutting = CuttingMarker::where('barcode_id', $a[0])->whereNull('deleted_at');
                $need_total = ($a[1] + 0.75) * $data_cutting->get()->first()->layer / 36;
                $data_cutting->update([
                    'marker_length' => $a[1],
                    'need_total' => $need_total,
                    'ratio_updated_at' => Carbon::now(),
                    'user_updated_id' => \Auth::user()->id,
                ]);
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function updateRasio(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;

            $id = $request->id;
            $data_rasio = $request->data_rasio;

            $get_rasio = RasioMarker::where('id_marker', $id)->where('deleted_at', null)->get();
            $get_ratio_array = $get_rasio->pluck('size')->toArray();

            foreach($data_rasio as $a) {
                if($a != '') {
                    $a_array = explode('-', $a);
                    if(in_array($a_array[0], $get_ratio_array) == false) {
                        RasioMarker::FirstOrCreate([
                            'id_marker' => $id,
                            'size' => $a_array[0],
                            'ratio' => $a_array[1],
                        ]);
                    } else {
                        RasioMarker::where('id_marker', $id)->where('size', $a_array[0])->update([ 'ratio' => $a_array[1] ]);
                    }
                }
            }

            foreach($get_rasio as $b) {
                if(in_array($b->size.'-'.$b->ratio, $data_rasio) == false) {
                    RasioMarker::where('id_marker', $id)->where('size', $b->size)->where('ratio', $b->ratio)->delete();
                }
            }

            $data_cutting = CuttingMarker::where('barcode_id', $id)->update([
                'ratio_updated_at' => Carbon::now(),
                'user_updated_id' => \Auth::user()->id,
            ]);

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function resetData(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;
            $color = $request->color;
            $data = $request->data;

            foreach($data as $a) {
                $data_cutting = CuttingMarker::where('barcode_id', $a[0])->whereNotNull('is_new')->whereNull('deleted_at');
                $data_cutting->update(['perimeter' => null, 'marker_length' => null, 'need_total' => null]);
            }

            return response()->json(['planning_id' => $planning_id, 'part_no' => $part_no , 'color' => $color],200);
        }
    }

    public function removePageAccess(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;
            $part_no = $request->part_no;

            $marker_check = CuttingMarker::where('id_plan', $planning_id)->where('part_no', $part_no)->where('deleted_at', null)->firstOrFail();

            TempPageAccess::where('cutting_date', $marker_check->cutting_plan->cutting_date)->where('queu', $marker_check->cutting_plan->queu)->where('part_no', $part_no)->where('user_id', \Auth::user()->id)->where('page', 'actual-marker')->delete();

            return response()->json(200);
        }
    }

    public function downloadReport($id)
    {
        if($id != null) {

            $data = array();
            $get_plan = CuttingPlan::where('id', $id)->where('deleted_at', null)->first();
            $part_no = CuttingMarker::select('part_no')->where('id_plan', $id)->where('deleted_at', null)->groupBy('part_no')->orderBy('part_no', 'asc')->pluck('part_no')->toArray();

            foreach($part_no as $part) {
                $data_part = array();
                $array_part = explode('+', $part);
                $data_marker = CuttingMarker::where('id_plan', $id)->where('deleted_at')->where('part_no', $part)->first();
                $data_part['marker_by'] = $data_marker->user_updated_id == null ? null : $data_marker->user->name;
                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $get_plan->id)->whereIn('part_no', $array_part);
                $data_part['part_no'] = $part;
                $data_part['factory'] = $get_plan->factory->factory_alias;
                $data_part['pcd'] = $get_plan->cutting_date;
                $data_part['lc_date'] = $get_data_cutting->first()->lc_date;
                $data_part['style'] = $get_plan->style;
                $data_part['season'] = $get_plan->season;
                $data_part['po'] = implode(', ', $get_plan->po_details->pluck('po_buyer')->toArray());
                $data_part['item_code'] = $get_data_cutting->first()->material;
                $data_part['color_name'] = $get_data_cutting->first()->color_name;
                $get_lebar_marker = CuttingMarker::select('fabric_width')->where('id_plan', $id)->where('part_no', $part)->where('deleted_at', null)->groupBy('fabric_width')->orderBy('fabric_width', 'asc')->pluck('fabric_width')->toArray();
                $list_lebar_marker = array();
                foreach($get_lebar_marker as $lebar) {
                    $list_lebar_marker[] = $lebar - 0.5;
                }
                $data_part['cw'] = implode('/', $list_lebar_marker);

                $detail_data_supply_whs = $get_data_cutting->whereIn('part_no', $array_part)->first()->uom;
                if($get_data_cutting->whereIn('part_no', $array_part)->first()->uom == 'M') {
                    $data_part['supply_whs'] = $get_data_cutting->whereIn('part_no', $array_part)->sum('fbc') * 1.0936;
                } else {
                    $data_part['supply_whs'] = $get_data_cutting->whereIn('part_no', $array_part)->sum('fbc');
                }
                $data_part['marker_cons'] = CuttingMarker::where('id_plan', $id)->where('part_no', $part)->where('deleted_at', null)->sum('need_total');
                $data_part['balance'] = $data_part['supply_whs'] - $data_part['marker_cons'];
                $data_part['persen'] = 100 - ($data_part['marker_cons'] / $data_part['supply_whs'] * 100);
                $data[] = $data_part;
            }

            if($get_plan != null) {
                $filename = 'Report_Overconsumtion_'.$get_plan->cutting_date.'-'.$get_plan->style.'-'.$get_plan->article;

                $i = 1;

                $export = \Excel::create($filename, function($excel) use ($data, $i) {
                    $excel->sheet('report', function($sheet) use($data, $i) {
                        $header = array('MARKER BY', 'FACTORY', 'PCD', 'LC', 'SEASON', 'STYLE', 'PO', 'PART NO', 'ITEM/CW AD', 'COLOR', 'MARKER PROD', 'SUPPLY WHS', 'ACTUAL MARKER PROD', 'BALANCE', 'PERCENTAGE');

                        $sheet->appendRow($header);

                        foreach ($data as $key => $row)
                        {
                            $data_excel = array(
                                $row['marker_by'],
                                $row['factory'],
                                Carbon::parse($row['pcd'])->format('d-M-y'),
                                Carbon::parse($row['lc_date'])->format('d-M-y'),
                                $row['season'],
                                $row['style'],
                                $row['po'],
                                $row['part_no'],
                                $row['item_code'],
                                $row['color_name'],
                                $row['cw'],
                                $row['supply_whs'],
                                $row['marker_cons'],
                                $row['balance'],
                                $row['persen'],
                            );
                            $sheet->appendRow($data_excel);
                        }
                    });
                })->download('xlsx');
            } else {
                // return error;
            }
        } else {
            // return error;
        }
    }

    public function downloadRatio($id)
    {

        $part_no_all = CuttingMarker::select('part_no')->where('id_plan', $id)->where('deleted_at', null)->groupBy('part_no')->orderBy('part_no', 'asc')->pluck('part_no')->toArray();

        $po_name = DB::table('detail_cutting_plan')->where('id_plan',$id)->pluck('po_buyer')->toArray();
        foreach($po_name as $po => $x){
            $x = substr($x,5);
            $y[] = $x;
        }
        $po_name_file = (implode(',',$y));

        $data_plan = CuttingPlan::where('id', $id)->first();
        $filename = 'Q-'.$data_plan->queu.'.'.'Report Ratio '.$data_plan->cutting_date.' '.$data_plan->style.' '.$data_plan->articleno.' #PO:'.$po_name_file;;
        $i = 1;

        $export = \Excel::create($filename, function($excel) use ($part_no_all, $id, $i, $data_plan) {
            foreach($part_no_all as $part_no) {

                $data_marker = CuttingMarker::where('id_plan', $id)->where('deleted_at', null)->where('part_no', $part_no)->orderBy('cut', 'asc')->get();

                $excel->sheet('PART '.$part_no, function($sheet) use($data_marker, $i, $data_plan, $part_no) {
                    $header = array('Name Marker', 'Size', 'Ratio', 'Season', 'Width', 'Layer');
                    $sheet->appendRow($header);
                    foreach($data_marker as $marker) {
                        $list_ratio = array();
                        $first_size = '';
                        $first_ratio = '';

                        foreach($marker->rasio_markers as $key => $row) {
                            if($key == 0) {
                                $first_size = $row->size;
                                $first_ratio = $row->ratio;
                            }
                            $list_ratio[] = $row->size.'-'.$row->ratio;
                        }

                        $data_temp = implode(' ', $list_ratio);
                        $header = array($data_temp);
                        array_push($header, $first_size, $first_ratio, $data_plan->season, $marker->fabric_width - 0.5, $marker->layer);
                        $sheet->appendRow($header);

                        foreach($marker->rasio_markers as $key => $row) {
                            if($key != 0) {
                                $data_excel = array(
                                    '',
                                    $row->size,
                                    $row->ratio,
                                );
                                $sheet->appendRow($data_excel);
                            }
                        }

                        $header = array('');
                        $sheet->appendRow($header);
                    }
                    $header = array('');
                    $sheet->appendRow($header);

                    $header = array('Fabric Info', 'Lebar Fabric', 'WHS Prepared');
                    $sheet->appendRow($header);

                    $part_no_array = explode('+', $part_no);

                    // $data_lebar = DB::connection('wms_live')->table('integration_whs_to_cutting')->select(DB::raw("actual_width, sum(qty_prepared) as alokasi_fb"))
                    // ->where('style', $data_plan->style)
                    // ->where('planning_date', $data_plan->cutting_date)
                    // ->where('article_no', $data_plan->articleno)
                    // ->whereIn('part_no', $part_no_array)->groupBy('actual_width')->get();

                    $style          = $data_plan->style;
                    $cutting_date   = $data_plan->cutting_date;
                    $article        = $data_plan->articleno;
                    $part           = "'".implode("','",$part_no_array)."'";
                    $data_lebar = DB::connection('wms_live_new')->select(DB::raw("SELECT actual_width, sum(qty_prepared) as alokasi_fb FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$article' AND part_no IN($part) GROUP BY actual_width"));

                    if(count($data_lebar) > 0) {
                        foreach($data_lebar as $lebar) {
                            $data_excel = array(
                                '',
                                $lebar->actual_width,
                                $lebar->alokasi_fb,
                            );
                            $sheet->appendRow($data_excel);
                        }
                    } else {
                        $data_excel = array(
                            'tidak ada data!',
                        );
                        $sheet->appendRow($data_excel);
                    }

                    $header = array('');
                    $sheet->appendRow($header);

                    $header = array('');
                    $sheet->appendRow($header);

                    $data_uom_cons = DB::table('data_cuttings')->where('plan_id', $data_plan->id)->whereIn('part_no', $part_no_array)->first()->uom;

                    $data_cons = DB::table('data_cuttings')->where('plan_id', $data_plan->id)->whereIn('part_no', $part_no_array)->sum('fbc');

                    if($data_uom_cons == 'M') {
                        $data_cons = $data_cons * 1.0936;
                    }

                    $header = array('Total CSI Cons','','','','',$data_cons);
                    $sheet->appendRow($header);
                });
            }
        })->download('xlsx');
    }

    public function ReleaseMarker(Request $request)
    {
        if(request()->ajax()) {
            $plan_id = $request->planning_id;
            $part_no = $request->part_no;
            $remark = $request->remark;
            $overconsum = $request->overconsum;
            $qty_csi = $request->qty_csi;
            $qty_need = $request->qty_need;

            $marker_check = CuttingMarker::where('id_plan', $plan_id)->where('part_no', $part_no)->where(function ($q) {
                $q->orWhere('perimeter', null)->orWhere('marker_length', null);
            })->where('deleted_at', null)->get();

            if(count($marker_check) > 0) {
                $response = [
                    'responseJSON' => 'Perimeter atau panjang marker belum lengkap!',
                    'status' => 422,
                ];
                return response()->json($response);
            } else {
                $release_check = MarkerReleaseHeader::where('plan_id', $plan_id)->where('part_no', $part_no)->where('deleted_at', null)->get();
                $data_detail = CuttingMarker::where('id_plan', $plan_id)->where('part_no', $part_no)->where('deleted_at', null)->first();
                $part_no = explode('+', $data_detail->part_no);
                $po_buyer = $data_detail->cutting_plan->po_details->pluck('po_buyer')->toArray();
                $data_detail_lc = DB::table('data_cuttings')->where('plan_id', $data_detail->cutting_plan->id)->whereIn('part_no', $part_no)->whereIn('po_buyer', $po_buyer)->first();

                if($release_check->count() > 0) {
                    MarkerReleaseDetail::FirstOrCreate([
                        'header_id' => $release_check->first()->id,
                        'balance' => $overconsum,
                        'release_by' => \Auth::user()->id,
                        'is_confirm' => false,
                        'remark' => $remark,
                        'supply_whs' => $qty_csi,
                        'actual_marker_prod' => $qty_need,
                    ]);
                } else {
                    $insert_header = MarkerReleaseHeader::FirstOrCreate([
                        'plan_id' => $plan_id,
                        'part_no' => $data_detail->part_no,
                        'cutting_date' => $data_detail->cutting_plan->cutting_date,
                        'factory_id' => $data_detail->cutting_plan->factory_id,
                        'lc_date' => $data_detail_lc->lc_date,
                        'style' => $data_detail->cutting_plan->style,
                        'item_id' => $data_detail->item_id, // sementara, seharusnya ambil data dari $data_detail_lc->lc_date
                        'combine_id' => $data_detail->combine_id,
                    ]);

                    MarkerReleaseDetail::FirstOrCreate([
                        'header_id' => $insert_header->id,
                        'balance' => $overconsum,
                        'release_by' => \Auth::user()->id,
                        'is_confirm' => false,
                        'remark' => $remark,
                        'supply_whs' => $qty_csi,
                        'actual_marker_prod' => $qty_need,
                    ]);
                }
                $response = [
                    'status' => 200,
                ];
                return response()->json($response);
            }
        }
    }
}
