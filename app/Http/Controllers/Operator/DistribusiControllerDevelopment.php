<?php

namespace App\Http\Controllers\Operator;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;
use Auth;
use App\Models\User;
use App\Models\Locator;
use App\Models\BundleTable;
use App\Models\MasterPpcm;
use App\Models\MasterSetting;
use App\Models\DistribusiMovement;
use DataTables;

class DistribusiControllerDevelopment extends Controller
{

    private $sds_connection;

    public function __construct()
    {
        $this->middleware('auth');
        $this->sds_connection = DB::connection('sds_live');
    }

    public function index()
    {
        User::where('id', \Auth::user()->id)->update(['last_login_at' => Carbon::now()]);
        return view('distribusi.index');
    }

    public function bundleLocator()
    {
        return view('distribusi.bundle_loc');
    }
    public function getDataBundleLoc(Request $request)
    {
        if ($request->ajax()){
            $barcode_input = $request->barcode_input;
            $po_cdms = DB::table('bundle_detail as bd')->select('bh.poreference','bh.cut_num','bh.size','bd.start_no')->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')->where('bd.barcode_id',$barcode_input)->first();
            $po_sds_cdms = DB::table('bundle_detail as bd')->select('bh.poreference','bh.cut_num','bh.size','bd.start_no')->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')->where('bd.sds_barcode',$barcode_input)->first();
            $po_sds_only = DB::table('sds_detail_movements as sdm')->select('shm.poreference','shm.cut_num','shm.size','sdm.start_no')->leftJoin('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')->where('sdm.sds_barcode',$barcode_input)->first();
            
            $x = array_filter([$po_cdms,$po_sds_cdms,$po_sds_only]);
            $zz = array_merge($x);
            $poreference = "'".$zz[0]->poreference."'";
            $cut_num = "'".$zz[0]->cut_num."'";
            $size = "'".$zz[0]->size."'";
            $start_no = "'".$zz[0]->start_no."'";
            $datax = DB::SELECT(DB::RAW("SELECT bd.barcode_id,bd.komponen_name,bh.style,vms.type_name,vms.season_name as season,bh.poreference,bh.article,bh.size,vms.part,bh.cut_num,bd.start_no,bd.end_no,bd.qty,bd.current_description,bd.updated_movement_at,
            u.name AS pic,
            bh.factory_id,
            max(dm.loc_dist) FILTER (where dm.locator_to='11') as loc_setting,
            max(dm.loc_dist) FILTER (where dm.locator_to='16' and dm.status_to = 'out') as loc_supply
            FROM
            bundle_header bh
            JOIN bundle_detail bd ON bd.bundle_header_id = bh.id
            JOIN v_master_style vms ON vms.id = bd.style_detail_id
            JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
            JOIN users u ON u.id = bd.current_user_id 
            WHERE
            bh.poreference=$poreference AND bh.cut_num = $cut_num AND bh.size = $size AND bd.start_no = $start_no
            GROUP BY
            bh.cut_num,bh.style,bh.size,bd.komponen_name,bd.barcode_id,vms.type_name,vms.season_name,bh.poreference,bh.article,vms.part,bd.start_no,bd.end_no,bd.qty,bd.current_description,bd.updated_movement_at,u.name,bh.factory_id
            UNION ALL
            SELECT sdm.sds_barcode AS barcode_id,sdm.komponen_name,shm.style,vms.type_name,vms.season_name as season,shm.poreference,shm.article,shm.size,vms.part,shm.cut_num,sdm.start_no,sdm.end_no,sdm.qty,sdm.current_description,sdm.updated_movement_at,u.name AS pic,shm.factory_id,
            max(dm.loc_dist) FILTER (where dm.locator_to='11') as loc_setting,
            max(dm.loc_dist) FILTER (where dm.locator_to='16' and dm.status_to='out') as loc_supply
            FROM
            sds_header_movements shm
            JOIN sds_detail_movements sdm ON sdm.sds_header_id = shm.id
            JOIN v_master_style vms ON vms.ID = sdm.style_id
            JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
            JOIN users u ON u.id = sdm.current_user_id 
            WHERE
            shm.poreference = $poreference AND shm.cut_num = $cut_num AND shm.size = $size AND sdm.start_no = $start_no
            GROUP BY shm.cut_num,shm.style,shm.size,sdm.komponen_name,sdm.sds_barcode,vms.type_name,vms.season_name,shm.poreference,shm.article,vms.part,sdm.start_no,sdm.end_no,sdm.qty,sdm.current_description,sdm.updated_movement_at,u.name,
            shm.factory_id"));
            
            return DataTables::of($datax)
            ->addColumn('cut_sticker',function ($datax)
            {
                return $datax->cut_num.' | '.$datax->start_no.'-'.$datax->end_no;
            })
            ->addColumn('style',function ($datax)
            {
                if($datax->type_name == 'Non'){
                    $style = $datax->season.'-'.$datax->style;
                }else{
                    $style = $datax->season.'-'.$datax->style.'-'.$datax->type_name;
                }
                return $style;
            })
            ->addColumn('current_description',function ($datax)
            {
                if($datax->current_description == null){
                    $current_description = '-';
                }else{
                    $current_description = $datax->current_description;
                }
                return $current_description;
            })
            ->addColumn('line',function ($datax)
            {
                if($datax->current_description == 'in SETTING'){
                    $line = $datax->loc_setting;
                }elseif($datax->current_description == 'out SUPPLY'){
                    $line = $datax->loc_supply;
                }else{
                    $line =  '-';
                }
                return str_replace('LINE','L.',$line);
            })
            ->addColumn('factory_id',function ($datax)
            {
                if($datax->factory_id == 1){
                    $factory_id = 'AOI 1';
                }else{
                    $factory_id = 'AOI 2';
                }
                return $factory_id;
            })
            ->addColumn('is_supplied',function ($datax)
            {
                if($datax->loc_supply != null){
                    return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                }else{
                    return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                }
            })
            ->addColumn('user_scan',function($datax){
                return strtoupper($datax->pic);
            })
            ->rawColumns(['style','factory_id','user_scan','current_description','is_supplied'])
            ->make(true);
        }else {
            $datax = array();
            return DataTables::of($datax)
            ->make(true);
        }
    }

    public function menu($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.action',compact('locator'));
            }else{
                return abort(405);
            }
        }else{
            return abort(405);
        }
    }

    public function checkIn($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();

        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_in',compact('locator','locator_set'));
            }else{
                return abort(405);
            }
        }else{
            return abort(405);
        }
    }

    public function moveLocSet($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.move_loc',compact('locator','locator_set'));
            }else{
                return abort(405);
            }

        }else{
            return abort(405);
        }
    }

    public function checkOut($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        if(\Auth::user()->factory_id == 2){
            $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('area_name', 'asc')->get();
        }else{
            $locator_set = MasterSetting::select('alias')->whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('alias', 'asc')->distinct('alias')->get();
        }
        $bundle_table = BundleTable::select('id_table','name')->where('factory_id',\Auth::user()->factory_id)->orderBy('id_table')->get();
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_out',compact('locator','locator_set','bundle_table'));
            }else{
                return abort(405);
            }
        }else{
            return abort(405);
        }
    }

    public function checkBundle($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();

        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_bundle',compact('locator','locator_set'));
            }else{
                return abort(405);
            }
        }else{
            return abort(405);
        }
    }

    public function bundleInfo(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_input = $request->barcode_input;

            $barcode_cdms = DB::table('bundle_detail')
            ->selectRaw('bundle_detail.barcode_id,bundle_detail.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, bundle_header.style, bundle_header.season, bundle_header.poreference, bundle_header.article, bundle_header.size, bundle_header.cut_num, bundle_detail.start_no, bundle_detail.end_no, bundle_detail.qty,bundle_header.factory_id')
            ->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'bundle_detail.style_detail_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('bundle_detail.barcode_id', $barcode_input)
            ->first();

            $barcode_sds_cdms = DB::table('bundle_detail')
            ->selectRaw('bundle_detail.barcode_id,bundle_detail.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, bundle_header.style, bundle_header.season, bundle_header.poreference, bundle_header.article, bundle_header.size, bundle_header.cut_num, bundle_detail.start_no, bundle_detail.end_no, bundle_detail.qty,bundle_header.factory_id')
            ->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'bundle_detail.style_detail_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('bundle_detail.sds_barcode', $barcode_input)
            ->first();

            $barcode_sds_only = DB::table('sds_detail_movements')
            ->selectRaw('sds_detail_movements.sds_barcode,sds_detail_movements.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, sds_header_movements.style, sds_header_movements.season, sds_header_movements.poreference, sds_header_movements.article, sds_header_movements.size, sds_header_movements.cut_num, sds_detail_movements.start_no, sds_detail_movements.end_no, sds_detail_movements.qty,sds_header_movements.factory_id')
            ->leftJoin('sds_header_movements', 'sds_header_movements.id', '=', 'sds_detail_movements.sds_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'sds_detail_movements.style_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('sds_detail_movements.sds_barcode', $barcode_input)
            ->first();

            if($barcode_cdms != null) {
                if($barcode_cdms->type_name == 'Non') {
                    $style = $barcode_cdms->style;
                } else {
                    $style = $barcode_cdms->style.$barcode_cdms->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_cdms->process == null || $barcode_cdms->process == '' || $barcode_cdms->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_cdms->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();
                    $process = implode(', ', $process_get);
                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_cdms->barcode_id)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }

                $obj = new StdClass();
                $obj->komponen = $barcode_cdms->part.' - '.$barcode_cdms->komponen_name;
                $obj->style = $style.' ('.$barcode_cdms->season.')';
                $obj->po_buyer = $barcode_cdms->poreference;
                $obj->article = $barcode_cdms->article;
                $obj->size = $barcode_cdms->size;
                $obj->cut_sticker = $barcode_cdms->cut_num.' / '.$barcode_cdms->start_no.' - '.$barcode_cdms->end_no;
                $obj->qty = $barcode_cdms->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_cdms->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms != null && $barcode_sds_only == null){
                if($barcode_sds_cdms->type_name == 'Non') {
                    $style = $barcode_sds_cdms->style;
                } else {
                    $style = $barcode_sds_cdms->style.$barcode_sds_cdms->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_sds_cdms->process == null || $barcode_sds_cdms->process == '' || $barcode_sds_cdms->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_sds_cdms->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();
                    $process = implode(', ', $process_get);
                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_sds_cdms->barcode_id)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }

                $obj = new StdClass();
                $obj->komponen = $barcode_sds_cdms->part.' - '.$barcode_sds_cdms->komponen_name;
                $obj->style = $style.' ('.$barcode_sds_cdms->season.')';
                $obj->po_buyer = $barcode_sds_cdms->poreference;
                $obj->article = $barcode_sds_cdms->article;
                $obj->size = $barcode_sds_cdms->size;
                $obj->cut_sticker = $barcode_sds_cdms->cut_num.' / '.$barcode_sds_cdms->start_no.' - '.$barcode_sds_cdms->end_no;
                $obj->qty = $barcode_sds_cdms->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_sds_cdms->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms == null && $barcode_sds_only != null){
                if($barcode_sds_only->type_name == 'Non') {
                    $style = $barcode_sds_only->style;
                } else {
                    $style = $barcode_sds_only->style.$barcode_sds_only->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_sds_only->process == null || $barcode_sds_only->process == '' || $barcode_sds_only->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_sds_only->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();

                    $process = implode(', ', $process_get);

                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_only->sds_barcode)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_only->sds_barcode)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_sds_only->sds_barcode)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }
                $obj = new StdClass();
                $obj->komponen = $barcode_sds_only->part.' - '.$barcode_sds_only->komponen_name;
                $obj->style = $style.' ('.$barcode_sds_only->season.')';
                $obj->po_buyer = $barcode_sds_only->poreference;
                $obj->article = $barcode_sds_only->article;
                $obj->size = $barcode_sds_only->size;
                $obj->cut_sticker = $barcode_sds_only->cut_num.' / '.$barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no;
                $obj->qty = $barcode_sds_only->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_sds_only->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms != null && $barcode_sds_only != null){
                return response()->json('Duplikat Detail Barcode!',422);
            }else{
                return response()->json('bundle tidak ditemukan',422);
            }
        }
    }
    
    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            $locator_id = $request->locator_id;
            $state = $request->state;
            $bundle_table = $request->mj_bundle;
            $line_supply = $request->line_supply;

            if($barcode_id != null && $locator_id != null){
                $barcode_check      = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode', $barcode_id)->first();
                $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
                $barcode_sds_bi     = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_id)->first();
                if($barcode_check != null){
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->whereNull('deleted_at')->first();
                    $update_sds         = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                    if($barcode_header->factory_id != \Auth::user()->factory_id){
                        return response()->json('Barcode Milik Factory AOI '.$barcode_header->factory_id,422);
                    }
                    if($komponen_detail != null){
                        $convert_locator = $this->convertLocator($locator_id, $state);
                        $bundle_info = DistribusiMovement::select('bundle_table')->where('barcode_id',$barcode_id)->where('locator_to','10')->first();
                        if($locator_id == '10'){
                            if($bundle_info != null){
                                return response()->json('Barcode Sudah Scan di Meja -'.$bundle_info->bundle_table,422);
                            }else{
                                try {
                                    DB::beginTransaction();
                                    // UPDATE DB SDS BUNDLE INFO NEW
                                    $this->sds_connection->table('bundle_info_new')
                                    ->where('barcode_id', $barcode_check->sds_barcode)
                                    ->update([
                                        'location'                              => $convert_locator['locator_sds'],
                                        $convert_locator['column_update']       => Carbon::now(),
                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                        'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                    ]);
                                    // INSERT IN TABLE TRANSACTION CDMS
                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => null,
                                        'status_from'  => null,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => '-',
                                        'bundle_table' => $bundle_table
                                    ]);

                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                        'current_locator_id'    => $locator_id,
                                        'current_status_to'     => $state,
                                        'current_description'   => $state.' '.$locator->locator_name,
                                        'updated_movement_at'   => Carbon::now(),
                                        'current_user_id'       => \Auth::user()->id,
                                        'is_out_cutting'        => 'Y',
                                        'out_cutting_at'        => Carbon::now()
                                    ]);
                                    DB::commit();
                                }catch(Exception $e){
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $barcode_check->komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $bundle_table
                                ];
                                return response()->json($response,200);
                            }
                        }elseif($locator->is_setting){
                            $nik_user   = DB::table('users')->where('id',\Auth::user()->id)->first();
                            $nik_sds    = DB::connection('sds_live')->table('m_user')->where('nik',$nik_user->nik)->first();
                            if($nik_sds == null){
                                return response()->json('ANDA TIDAK MEMILIKI AKSES DI SDS!',422);
                            }
                            $last_loc = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                            if($bundle_info == null){
                                return response()->json('Barcode Belum Scan Out Cutting!',422);
                            }elseif($last_loc != null){
                                return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_loc->loc_dist,422);
                            }else{
                                $process    = DB::table('v_master_style')->where('id',$barcode_check->style_detail_id)->first();
                                //JIKA TERDAPAT PROSES SECONDARY MAKA AKAN DI LAKUKAN PENGECEKAN SUDAH SELESAI ATAU BELUM
                                $last_move = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                $type_name = DB::table('types')->where('id',$komponen_detail->type_id)->first();
                                // $get_locator_set_cdms = DB::table('distribusi_movements as dm')
                                // ->select('dm.barcode_id','bh.season','bh.style','bh.poreference','bh.article','vms.type_name','dm.loc_dist','bh.size')
                                // ->Join('bundle_detail as bd','bd.barcode_id','=','dm.barcode_id')
                                // ->Join('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                // ->Join('v_master_style as vms','vms.id','=','bd.style_detail_id')
                                // ->where('bh.poreference',$barcode_header->poreference)
                                // ->where('bh.style',$barcode_header->style)
                                // ->where('bh.season',$barcode_header->season)
                                // ->where('bh.article',$barcode_header->article)
                                // ->where('vms.type_name',$type_name->type_name)
                                // ->where('bh.cut_num',$barcode_header->cut_num)
                                // ->where('bh.size',$barcode_header->size)
                                // ->where('bd.start_no',$barcode_check->start_no)
                                // ->where('dm.locator_to','11')
                                // ->where('dm.status_to','in')
                                // ->OrderBy('dm.created_at','DESC')
                                // ->first();

                                $get_locator_set_cdms = DB::select(DB::raw("SELECT dm.loc_dist FROM distribusi_movements dm JOIN bundle_detail bd on bd.barcode_id = dm.barcode_id JOIN bundle_header bh on bh.id = bd.bundle_header_id JOIN v_master_style vms on vms.id = bd.style_detail_id WHERE bh.poreference = '$barcode_header->poreference' AND bh.style = '$barcode_header->style' AND bh.season = '$barcode_header->season' AND bh.article = '$barcode_header->article' AND vms.type_name = '$type_name->type_name' AND bh.cut_num = '$barcode_header->cut_num' AND bh.size = '$barcode_header->size' AND bd.start_no = '$barcode_check->start_no' AND dm.locator_to='11' AND dm.status_to='in' ORDER BY dm.created_at DESC limit 1"));

                                $type_name = $type_name->type_name == 'Non' ? 0 : $type_name->type_name;

                                // $get_locator_set_sds = DB::connection('sds_live')
                                // ->table('bundle_info_new as bi')
                                // ->select('bi.location')
                                // ->leftJoin('art_desc_new as ad',function($join){
                                //     $join->on('ad.season','=','bi.season');
                                //     $join->on('ad.style','=','bi.style');
                                //     $join->on('ad.part_num','=','bi.part_num');
                                //     $join->on('ad.part_name','=','bi.part_name');
                                // })
                                // ->where('bi.poreference',$barcode_header->poreference)
                                // ->where('bi.style',$barcode_header->style)
                                // ->where('bi.season',$barcode_header->season)
                                // ->where('bi.article',$barcode_header->article)
                                // ->where('ad.set_type',$type_name)             
                                // ->where('bi.cut_num',$barcode_header->cut_num)
                                // ->where('bi.size',$barcode_header->size)
                                // ->where('bi.start_num',$barcode_check->start_no)
                                // ->whereNotNull('bi.setting')
                                // ->orderBy('bi.setting','DESC')
                                // ->first();


                                $get_locator_set_sds = DB::connection('sds_live')->select(DB::raw("SELECT bi.location FROM bundle_info_new bi JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name WHERE bi.poreference='$barcode_header->poreference' AND bi.style = '$barcode_header->style' AND bi.article = '$barcode_header->article' AND ad.set_type = '$type_name' AND bi.cut_num = '$barcode_header->cut_num' AND bi.size = '$barcode_header->size' AND bi.start_num = '$barcode_check->start_no' AND bi.setting IS NOT NULL ORDER BY bi.setting DESC limit 1"));
                                

                                $linkage_bi = count($get_locator_set_cdms) == 0 ? $get_locator_set_sds : $get_locator_set_cdms;
                                if($process->process != null){
                                    $proc_id        = explode(',',$process->process);
                                    $loc_id_temp    = DB::table('master_process')->select('locator_id')->whereIn('id',$proc_id)->whereNull('deleted_at')->distinct('locator_id')->get();
                                    //JIKA TERDAPAT PROSES MAKA AKAN DI CEK SCAN IN OUTNYA
                                    foreach($loc_id_temp as $lit){
                                        $locator_names = DB::table('master_locator')->where('id',$lit->locator_id)->whereNull('deleted_at')->first();
                                        $in_status     = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','in')->first();
                                        $out_status    = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','out')->first();
                                        if($in_status == null){
                                            return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                        }
                                        if($out_status == null){
                                            return response()->json('Bundle Belum Scan Out'.' - '.$locator_names->locator_name,422);
                                        }
                                    }
                                    if(count($linkage_bi) == 0){
                                        return response()->json($barcode_id, 331);
                                    }else{
                                        // $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds : $get_locator_set_cdms;
                                        $loc_dist = 'L.1';
                                        try{
                                            DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $last_move->locator_to,
                                                    'status_from'  => $last_move->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => trim($loc_dist)
                                                ]);
                                                DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                    'current_locator_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id',$barcode_check->sds_barcode)
                                                ->update([
                                                    'location'      => $loc_dist,
                                                    'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                    'setting'       => Carbon::now(),
                                                    'setting_pic'   => \Auth::user()->nik,
                                                    'supply_unlock' => false
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_check->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $loc_dist
                                            ];
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }else{
                                    if(count($linkage_bi) == 0){
                                        return response()->json($barcode_id, 331);
                                    }else{
                                        // $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds : $get_locator_set_cdms;
                                        try{
                                            DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $last_move->locator_to,
                                                    'status_from'  => $last_move->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $loc_dist
                                                ]);
                                                DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                    'current_locator_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id',$barcode_check->sds_barcode)
                                                ->update([
                                                    'location'      => $loc_dist,
                                                    'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                    'setting'       => Carbon::now(),
                                                    'setting_pic'   => $nik_user->nik,
                                                    'supply_unlock' => false
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_check->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $loc_dist
                                            ];
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }
                        }elseif($locator->is_artwork){
                            $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->whereNull('deleted_at')->first();
                            $komponen_name      = $barcode_check->komponen_name;
                            $get_process        = explode(',', $komponen_detail->process);
                            if($bundle_info == null){
                                return response()->json('Barcode Belum Scan Out Cutting!',422);
                            }elseif($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }else{
                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                if(in_array($locator_id, $get_locator_process)) {
                                    if($state == 'in'){
                                        $check_sds      = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->first();
                                        $check_in_cdms  = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                        if($check_sds->artwork != null){
                                            return response()->json('BARCODE SUDAH SCAN IN ARTWORK!',422);    
                                        }else{
                                            return response()->json('MODULE INI SUDAH TIDAK BERLAKU, GUNAKAN MODULE TERBARU!',422);
                                        }
                                        if($check_in_cdms != null){
                                            return response()->json('BARCODE SUDAH SCAN IN ARTWORK!',422);
                                        }else{
                                            return response()->json('MODULE INI SUDAH TIDAK BERLAKU, GUNAKAN MODULE TERBARU!',422);
                                        }
                                    }else{
                                        //STATE OUT
                                        if($bundle_info != null){
                                            $cdms_in_artwork        = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',3)->where('status_to','in')->first();
                                            $cdms_out_artwork       = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',3)->where('status_to','out')->first();
                                            $status_art_in_sds      = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->whereNotNull('artwork_pic')->first();
                                            $status_art_out_sds     = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->whereNotNull('artwork_out_pic')->first();
                                            $cek_user_cdms          = DB::table('users')->where('nik',$status_art_in_sds->artwork_pic)->first();
                                            if($status_art_in_sds != null && $cdms_in_artwork == null){
                                                if($cek_user_cdms == null){
                                                    return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#01-UA',422);
                                                }
                                                $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'        => $barcode_check->barcode_id,
                                                        'locator_from'      => $last_loc->locator_to,
                                                        'status_from'       => $last_loc->status_to,
                                                        'locator_to'        => 3,
                                                        'status_to'         => 'in',
                                                        'user_id'           => $cek_user_cdms->id,
                                                        'description'       => 'in ARTWORK',
                                                        'ip_address'        => $this->getIPClient(),
                                                        'deleted_at'        => null,
                                                        'loc_dist'          => '-'
                                                    ]);
                                                    DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',3)->where('status_to','in')->update([
                                                        'created_at'        => $status_art_in_sds->artwork,
                                                        'updated_at'        => $status_art_in_sds->artwork,
                                                    ]);
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                            }elseif($status_art_in_sds == null && $cdms_in_artwork == null){
                                                return response()->json('Barcode Belum Scan In ARTWORK!',422);
                                            }
                                            if($status_art_out_sds == null && $cdms_out_artwork == null){
                                                $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'        => $barcode_id,
                                                        'locator_from'      => $last_loc->locator_to,
                                                        'status_from'       => $last_loc->status_to,
                                                        'locator_to'        => $locator_id,
                                                        'status_to'         => $state,
                                                        'user_id'           => $cek_user_cdms->id,
                                                        'description'       => $state.' '.$locator->locator_name,
                                                        'ip_address'        => $this->getIPClient(),
                                                        'deleted_at'        => null,
                                                        'loc_dist'          => '-'
                                                    ]);

                                                    DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',3)->where('status_to','in')->update([
                                                        'created_at'        => Carbon::now(),
                                                        'updated_at'        => Carbon::now(),
                                                    ]);
        
                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id'    => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => $cek_user_cdms->id
                                                    ]);
                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                        'location'                              => $convert_locator['locator_sds'],
                                                        $convert_locator['column_update']       => Carbon::now(),
                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                        'factory'                               => $convert_locator['factory_sds'],
                                                    ]);
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                            }elseif($status_art_out_sds != null && $cdms_out_artwork == null){
                                                $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'        => $barcode_id,
                                                        'locator_from'      => $last_loc->locator_to,
                                                        'status_from'       => $last_loc->status_to,
                                                        'locator_to'        => $locator_id,
                                                        'status_to'         => $state,
                                                        'user_id'           => \Auth::user()->id,
                                                        'description'       => $state.' '.$locator->locator_name,
                                                        'ip_address'        => $this->getIPClient(),
                                                        'deleted_at'        => null,
                                                        'loc_dist'          => '-'
                                                    ]);
        
                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id'    => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id
                                                    ]);
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                            }elseif($status_art_out_sds != null && $cdms_out_artwork != null){
                                                return response()->json('Barcode Sudah Scan Out ARTWORK!',422);
                                            }
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_check->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => '-'
                                            ];
                                            return response()->json($response,200);
                                        }else{
                                            return response()->json('Barcode Belum Scan Out Cutting!',422);
                                        }
                                    }
                                }else{
                                    return response()->json('Bundle tidak perlu melalui proses ini!',422);
                                }
                            }
                        }elseif($locator_id == '7'){
                            $is_ppa2 = DB::table('master_style_detail')->where('id',$barcode_check->style_detail_id)->first();
                            if($is_ppa2 != null){
                                if($is_ppa2->is_ppa2 == false){
                                    return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                }
                            }else{
                                return response()->json('Master Style Dihapus Dari Sistem!',422);
                            }
                            $check_in_status    = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                            $check_out_status   = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                            if($state == 'in') {
                                if($check_in_status != null){
                                    return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                }else{
                                    $check_setting = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                    if($check_setting == null){
                                        return response()->json('Harap Scan Setting dahulu!',422);
                                    }else{
                                        try {
                                            DB::beginTransaction();
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => null
                                            ]);

                                            DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                'current_locator_id' => $locator_id,
                                                'current_status_to' => $state,
                                                'current_description' => $state.' '.$locator->locator_name,
                                                'updated_movement_at' => Carbon::now(),
                                                'current_user_id' => \Auth::user()->id
                                            ]);

                                            $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => '-'
                                            ];

                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }else{
                                if($check_in_status == null){
                                    return response()->json('Bundle Belum Scan In PPA2!',422);
                                }elseif($check_out_status != null){
                                    return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                }else{
                                    $barcode = DB::select("SELECT ms.season_name,bh.poreference,bh.style,t.type_name,bd.barcode_id,bd.bundle_header_id,bd.komponen_name,msd.is_ppa2,bd.style_detail_id
                                    FROM bundle_detail bd
                                    LEFT JOIN bundle_header bh on bh.id = bd.bundle_header_id
                                    LEFT JOIN master_style_detail msd on msd.id = bd.style_detail_id
                                    LEFT JOIN master_seasons ms on ms.id = msd.season
                                    LEFT JOIN types t on t.id = msd.type_id
                                    WHERE bd.bundle_header_id='$barcode_check->bundle_header_id' AND msd.is_ppa2 = true AND bd.start_no='$barcode_check->start_no' AND bd.end_no='$barcode_check->end_no'");
                                    $barcode_group = [];
                                    foreach($barcode as $b){
                                        $barcode_group[] = $b->barcode_id;
                                    }
                                    $barcode_group = implode(',',$barcode_group);
                                    $barcode_finish = explode(',',$barcode_group);
                                    $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                    $check_barcode = implode(',',$is_all_in);
                                    foreach($barcode_finish as $bf){
                                        $unscanned = DB::table('bundle_detail')->where('barcode_id',$bf)->first();
                                        if(strpos($check_barcode,$bf) === false){
                                            return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                        }
                                    }
                                    try {
                                        DB::beginTransaction();
                                        $description = $state.' '.$locator->locator_name;
                                        for ($i = 0; $i < count($barcode_finish); $i++) {
                                            $data_insert[] = [
                                                'id'           => Uuid::generate()->string,
                                                'barcode_id'   => $barcode_finish[$i],
                                                'deleted_at'   => null,
                                                'description'  => $description,
                                                'ip_address'   => $this->getIPClient(),
                                                'loc_dist'     => null,
                                                'locator_from' => intval($out_cutting_check->locator_to),
                                                'locator_to'   => intval($locator_id),
                                                'status_from'  => $out_cutting_check->status_to,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'created_at'   => Carbon::now(),
                                                'updated_at'   =>Carbon::now()
                                            ];
                                        }
                                        DistribusiMovement::insert($data_insert);

                                        DB::table('bundle_detail')->whereIn('barcode_id',$barcode_finish)->update([
                                            'current_locator_id'    => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => \Auth::user()->id
                                        ]);

                                        $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                        $response = [
                                            'barcode_id'        => $barcode_id,
                                            'style'             => $barcode_header->style,
                                            'article'           => $barcode_header->article,
                                            'po_buyer'          => $barcode_header->poreference,
                                            'part'              => $komponen_detail->part,
                                            'komponen_name'     => $komponen_name,
                                            'size'              => $barcode_header->size,
                                            'sticker_no'        => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                            'qty'               => $barcode_check->qty,
                                            'cut'               => $barcode_header->cut_num,
                                            'process'           => $locator->locator_name,
                                            'status'            => $state,
                                            'loc_dist'          => '-'
                                        ];
                                        DB::commit();

                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                    return response()->json($response,200);
                                }
                            }
                        }else{
                            if($bundle_info != null){
                                //LOCATOR SUPPLY
                                if($locator_id == '16'){
                                    $out_supply_check = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                                    if($out_supply_check != null){
                                        return response()->json('Barcode sudah scan out Supply Ke '.$out_supply_check->loc_dist.'!',422);
                                    }else{
                                        $cek_out_cutting    = DB::table('dd_bundle_check')->where('barcode_id', $barcode_id)->first();
                                        $get_info_barcode   = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->get();
                                        foreach($get_info_barcode as $gib){
                                            $setting_sds    = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$gib->sds_barcode)->whereNotNull('setting_pic')->first();
                                            $check_move     = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                            $out_cut        = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                            $last_move      = DistribusiMovement::where('barcode_id',$gib->barcode_id)->orderBy('created_at','DESC')->first();
                                            if($out_cut == null){
                                                return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_check->komponen_name.' || Cut -'.$barcode_header->cut_num.' || Size '.$barcode_header->size.' Belum Scan Cutting!',422);
                                            }
                                            $komponen_detail    = DB::table('master_style_detail')->where('id', $gib->style_detail_id)->whereNull('deleted_at')->first();
                                            $get_process        = explode(',', $komponen_detail->process);
                                            if($get_process[0] != null || $get_process[0] != '') {
                                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                                if(in_array(3,$get_locator_process)){
                                                    $sds_in_art     = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$gib->sds_barcode)->whereNotNull('artwork_pic')->first();
                                                    $sds_out_art    = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$gib->sds_barcode)->whereNotNull('artwork_out_pic')->first();
                                                    $in_art_cdms    = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                                    $out_art_cdms   = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','3')->where('status_to','out')->first();
                                                    //JIKA ADA PROSES ARTWORK DAN DI SCAN LEWAT SDS SAJA
                                                    if($sds_in_art != null && $in_art_cdms == null){
                                                        $cek_user_cdms = DB::table('users')->where('nik',$sds_in_art->artwork_pic)->first();
                                                        if($cek_user_cdms == null){
                                                            return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#03-UA',422);
                                                        }
                                                        $last_loc = DistribusiMovement::where('barcode_id',$gib->barcode_id)->orderBy('created_at','DESC')->first();
                                                        try {
                                                            DB::beginTransaction();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'        => $gib->barcode_id,
                                                                'locator_from'      => $last_loc->locator_to,
                                                                'status_from'       => $last_loc->status_to,
                                                                'locator_to'        => 3,
                                                                'status_to'         => 'in',
                                                                'user_id'           => $cek_user_cdms->id,
                                                                'description'       => 'in ARTWORK',
                                                                'ip_address'        => $this->getIPClient(),
                                                                'deleted_at'        => null,
                                                                'loc_dist'          => '-'
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','3')->where('status_to','in')->update([
                                                            'created_at'        => $sds_in_art->artwork,
                                                            'updated_at'        => $sds_in_art->artwork,
                                                        ]);
                                                    }elseif($sds_in_art == null && $in_art_cdms == null){
                                                        return response()->json('Barcode Belum Scan In Artwork!',422);
                                                    }
                                                    //OUT CONDITION ARTWORK
                                                    if($sds_out_art != null && $out_art_cdms == null){
                                                        $cek_user_cdms = DB::table('users')->where('nik',$sds_out_art->artwork_out_pic)->first();
                                                        if($cek_user_cdms == null){
                                                            return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#04-UA',422);
                                                        }
                                                        $last_loc = DistribusiMovement::where('barcode_id',$gib->barcode_id)->orderBy('created_at','DESC')->first();
                                                        try {
                                                            DB::beginTransaction();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'        => $gib->barcode_id,
                                                                'locator_from'      => $last_loc->locator_to,
                                                                'status_from'       => $last_loc->status_to,
                                                                'locator_to'        => 3,
                                                                'status_to'         => 'out',
                                                                'user_id'           => $cek_user_cdms->id,
                                                                'description'       => 'out ARTWORK',
                                                                'ip_address'        => $this->getIPClient(),
                                                                'deleted_at'        => null,
                                                                'loc_dist'          => '-'
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','3')->where('status_to','out')->update([
                                                            'created_at'        => $sds_out_art->artwork_out,
                                                            'updated_at'        => $sds_out_art->artwork_out,
                                                        ]);
                                                    }elseif($sds_out_art == null && $out_art_cdms == null){
                                                        return response()->json('Barcode Belum Scan Out Artwork!',422);
                                                    }
                                                }
                                            }
                                            if($setting_sds != null && $check_move == null){
                                                $check_user_setting = DB::table('users')->where('nik',$setting_sds->setting_pic)->first();
                                                if($check_user_setting == null){
                                                    return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#02-UA',422);
                                                }else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'        => $gib->barcode_id,
                                                            'locator_from'      => $last_move->locator_to,
                                                            'status_from'       => $last_move->status_to,
                                                            'locator_to'        => 11,
                                                            'status_to'         => 'in',
                                                            'user_id'           => $check_user_setting->id,
                                                            'description'       => 'in SETTING',
                                                            'ip_address'        => $this->getIPClient(),
                                                            'deleted_at'        => null,
                                                            'loc_dist'          => trim($setting_sds->location)
                                                        ]);
                                                        DB::commit();
                                                    } catch (Exception $e) {
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','11')->where('status_to','in')->update([
                                                        'created_at'        => $setting_sds->setting,
                                                        'updated_at'        => $setting_sds->setting,
                                                    ]);
                                                }
                                            }elseif($setting_sds == null && $check_move == null){
                                                return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_check->komponen_name.' || Cut -'.$barcode_header->cut_num.' || Size '.$barcode_header->size.' Belum Scan Setting!',422);
                                            }
                                        }
                                        $list_bundle_komponen = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->pluck('barcode_id')->toArray();
                                        try{
                                            DB::beginTransaction();
                                            DB::table('bundle_detail')
                                            ->whereIn('barcode_id',$list_bundle_komponen)
                                            ->update([
                                                'is_out_setting' => 'Y',
                                                'out_setting_at' => Carbon::now()
                                            ]);
                                            $description = $state.' '.$locator->locator_name;
                                            for($i = 0; $i < count($list_bundle_komponen); $i++){
                                                $data_insert[] = [
                                                    'id'            => Uuid::generate()->string,
                                                    'barcode_id'    => $list_bundle_komponen[$i],
                                                    'deleted_at'    => null,
                                                    'description'   => $description,
                                                    'ip_address'    => $this->getIPClient(),
                                                    'loc_dist'      => $line_supply,
                                                    'locator_from'  => intval($last_move->locator_to),
                                                    'locator_to'    => intval($locator_id),
                                                    'status_from'   => $last_move->status_to,
                                                    'status_to'     => $state,
                                                    'user_id'       => \Auth::user()->id,
                                                    'created_at'    => Carbon::now(),
                                                    'updated_at'    => Carbon::now()
                                                ];
                                            }
                                            DistribusiMovement::insert($data_insert);
                                            DB::table('bundle_detail')->whereIn('barcode_id',$list_bundle_komponen)->update([
                                                'current_locator_id'    => $locator_id,
                                                'current_status_to'     => $state,
                                                'current_description'   => $state.' '.$locator->locator_name,
                                                'updated_movement_at'   => Carbon::now(),
                                                'current_user_id'       => \Auth::user()->id
                                            ]);
                                            DB::commit();
                                        }catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        $response = [
                                            'barcode_id'    => $barcode_id,
                                            'style'         => $barcode_header->style,
                                            'article'       => $barcode_header->article,
                                            'po_buyer'      => $barcode_header->poreference,
                                            'part'          => $komponen_detail->part,
                                            'komponen_name' => $barcode_check->komponen_name,
                                            'size'          => $barcode_header->size,
                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                            'qty'           => $barcode_check->qty,
                                            'cut'           => $barcode_header->cut_num,
                                            'process'       => $locator->locator_name,
                                            'status'        => $state,
                                            'loc_dist'      => $line_supply
                                        ];
                                        return response()->json($response,200);
                                    }
                                }else{
                                    //SCAN ALL LOCATOR SECONDARY (HE-FUSE-PPA1-AUTO-FUSE-PAD-LASER)
                                    $get_process = explode(',', $komponen_detail->process);
                                    if($get_process[0] == null || $get_process[0] == '') {
                                        return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                    }
                                    $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                    if(in_array($locator_id, $get_locator_process)){
                                        $last_loc           = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                        $required_in_out    = [1,2,3,4,5,6,7];
                                        $in_sec             =  DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$locator_id)->where('status_to','in')->first();
                                        $out_sec            =  DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();
                                        $las_loc_name       =  DB::table('master_locator')->where('id',$last_loc->locator_to)->whereNull('deleted_at')->first();
                                        if(in_array($last_loc->locator_to,$required_in_out) && $last_loc->status_to == 'in' && $last_loc->locator_to != $locator_id){
                                            return response()->json('Harap Scan Out '.$las_loc_name->locator_name.' Dahulu!',422);
                                        }elseif($in_sec != null && $state == 'in'){
                                            return response()->json('Barcode Sudah Scan In '.$locator->locator_name.'!',422);
                                        }elseif($out_sec != null && $state == 'out'){
                                            return response()->json('Barcode Sudah Scan Out '.$locator->locator_name.'!',422);
                                        }elseif($in_sec == null && $state == 'out'){
                                            return response()->json('Barcode Belum Scan In '.$locator->locator_name.'!',422);
                                        }else{
                                            if($state == 'in'){
                                                if(in_array(3,$get_locator_process)){
                                                    $in_art_cdms = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                                    $out_art_cdms = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to','3')->where('status_to','out')->first();
                                                    $status_art_in_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->whereNotNull('artwork_pic')->first();
                                                    $status_art_out_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->whereNotNull('artwork_out_pic')->first();
                                                    if($status_art_in_sds != null && $in_art_cdms == null){
                                                        $cek_user_cdms = DB::table('users')->where('nik',$status_art_in_sds->artwork_pic)->first();
                                                        if($cek_user_cdms == null){
                                                            return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#01-UA',422);
                                                        }
                                                        $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                                        try {
                                                            DB::beginTransaction();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'        => $barcode_check->barcode_id,
                                                                'locator_from'      => $last_loc->locator_to,
                                                                'status_from'       => $last_loc->status_to,
                                                                'locator_to'        => 3,
                                                                'status_to'         => 'in',
                                                                'user_id'           => $cek_user_cdms->id,
                                                                'description'       => 'in ARTWORK',
                                                                'ip_address'        => $this->getIPClient(),
                                                                'deleted_at'        => null,
                                                                'loc_dist'          => '-'
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to','3')->where('status_to','in')->update([
                                                            'created_at'        => $status_art_in_sds->artwork,
                                                            'updated_at'        => $status_art_in_sds->artwork,
                                                        ]);
                                                    }elseif($status_art_in_sds == null && $in_art_cdms == null){
                                                        return response()->json('Bundle Belum Scan In Artwork!',422);
                                                    }

                                                    if($status_art_out_sds != null && $out_art_cdms == null){
                                                        $cek_user_cdms = DB::table('users')->where('nik',$status_art_out_sds->artwork_out_pic)->first();
                                                        if($cek_user_cdms == null){
                                                            return response()->json('TERJADI KESALAHAN, HARAP HUBUNGI ICT!#01-UA',422);
                                                        }
                                                        $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                                        try {
                                                            DB::beginTransaction();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'        => $barcode_check->barcode_id,
                                                                'locator_from'      => $last_loc->locator_to,
                                                                'status_from'       => $last_loc->status_to,
                                                                'locator_to'        => 3,
                                                                'status_to'         => 'out',
                                                                'user_id'           => $cek_user_cdms->id,
                                                                'description'       => 'out ARTWORK',
                                                                'ip_address'        => $this->getIPClient(),
                                                                'deleted_at'        => null,
                                                                'loc_dist'          => '-'
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',3)->where('status_to','out')->update([
                                                            'created_at'        => $status_art_out_sds->artwork_out,
                                                            'updated_at'        => $status_art_out_sds->artwork_out,
                                                        ]);
                                                    }elseif($status_art_out_sds == null && $out_art_cdms == null){
                                                        return response()->json('Bundle Belum Scan Out Artwork!',422);
                                                    }
                                                }
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $barcode_check->current_locator_id,
                                                        'status_from'  => $barcode_check->current_status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'user_id'      => \Auth::user()->id,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'ip_address'   => $this->getIPClient(),
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => '-'
                                                    ]);

                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id'        => $locator_id,
                                                        'current_status_to'         => $state,
                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'       => Carbon::now(),
                                                        'current_user_id'           => \Auth::user()->id
                                                    ]);
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_check->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                        'qty'           => $barcode_check->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];

                                                    //UPDATE IN DB SDS
                                                    if($barcode_check->sds_barcode != null) {
                                                        $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                        if($update_sds != null) {
                                                            $convert_locator = $this->convertLocator($locator_id, $state);
                                                            if($update_sds->{$convert_locator['column_update']} == null) {
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => $convert_locator['factory_sds'],
                                                                ]);
                                                            }
                                                        }
                                                    }
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                                return response()->json($response,200);
                                            }else{
                                                //SCAN OUT SECONDARY PROCESS
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'        => $barcode_id,
                                                        'locator_from'      => $last_loc->locator_to,
                                                        'status_from'       => $last_loc->status_to,
                                                        'locator_to'        => $locator_id,
                                                        'status_to'         => $state,
                                                        'user_id'           => \Auth::user()->id,
                                                        'description'       => $state.' '.$locator->locator_name,
                                                        'ip_address'        => $this->getIPClient(),
                                                        'deleted_at'        => null,
                                                        'loc_dist'          => '-'
                                                    ]);

                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id'        => $locator_id,
                                                        'current_status_to'         => $state,
                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'       => Carbon::now(),
                                                        'current_user_id'           => \Auth::user()->id
                                                    ]);

                                                    $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                    $response = [
                                                        'barcode_id'        => $barcode_id,
                                                        'style'             => $barcode_header->style,
                                                        'article'           => $barcode_header->article,
                                                        'po_buyer'          => $barcode_header->poreference,
                                                        'part'              => $komponen_detail->part,
                                                        'komponen_name'     => $barcode_check->komponen_name,
                                                        'size'              => $barcode_header->size,
                                                        'sticker_no'        => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                        'qty'               => $barcode_check->qty,
                                                        'cut'               => $barcode_header->cut_num,
                                                        'process'           => $locator->locator_name,
                                                        'status'            => $state,
                                                        'loc_dist'          => '-'
                                                    ];

                                                    ///////////////////----UPDATED IN DB SDS-------////////////////
                                                    if($barcode_check->sds_barcode != null) {
                                                        $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                        if($update_sds != null) {
                                                            $convert_locator = $this->convertLocator($locator_id, $state);
                                                            if($update_sds->{$convert_locator['column_update']} == null) {
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => $convert_locator['factory_sds'],
                                                                ]);
                                                            }
                                                        }
                                                    }
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                                return response()->json($response,200);
                                            }
                                        }
                                    }else{
                                        return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                    }
                                }
                            } else {
                                return response()->json('Barcode Belum Scan Out Cutting!',422);
                            }
                        }
                    }else{
                        return response()->json('KOMPONEN DI HAPUS DARI SISTEM!',422);
                    }
                }elseif($barcode_sds != null && $barcode_sds_only == null){
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->whereNull('deleted_at')->first();
                    $update_sds         = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->first();
                    if($barcode_header->factory_id != \Auth::user()->factory_id){
                        return response()->json('Barcode Milik Factory AOI '.$barcode_header->factory_id,422);
                    }
                    if($komponen_detail != null){
                        $bundle_info_sds = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',10)->first();
                        if($locator->is_setting){
                            $nik_user   = DB::table('users')->where('id',\Auth::user()->id)->first();
                            $nik_sds    = DB::connection('sds_live')->table('m_user')->where('nik',$nik_user->nik)->first();
                            if($nik_sds == null){
                                return response()->json('ANDA TIDAK MEMILIKI AKSES DI SDS!',422);
                            }
                            $last_loc = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                            if($bundle_info_sds == null){
                                return response()->json('Barcode Belum Scan Out Cutting!',422);
                            }elseif($last_loc != null){
                                return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_loc->loc_dist,422);
                            }else{
                                $process    = DB::table('v_master_style')->where('id',$barcode_sds->style_detail_id)->first();
                                //JIKA TERDAPAT PROSES SECONDARY MAKA AKAN DI LAKUKAN PENGECEKAN SUDAH SELESAI ATAU BELUM
                                $last_move = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                $type_name = DB::table('types')->where('id',$komponen_detail->type_id)->first();
                                                                
                                $get_locator_set_cdms = DB::table('distribusi_movements as dm')
                                ->select('dm.barcode_id','bh.season','bh.style','bh.poreference','bh.article','vms.type_name','dm.loc_dist','bh.size')
                                ->Join('bundle_detail as bd','bd.barcode_id','=','dm.barcode_id')
                                ->Join('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                ->Join('v_master_style as vms','vms.id','=','bd.style_detail_id')
                                ->where('bh.poreference',$barcode_header->poreference)
                                ->where('bh.style',$barcode_header->style)
                                ->where('bh.season',$barcode_header->season)
                                ->where('bh.article',$barcode_header->article)
                                ->where('vms.type_name',$type_name->type_name)
                                ->where('bh.cut_num',$barcode_header->cut_num)
                                ->where('bh.size',$barcode_header->size)
                                ->where('bd.start_no',$barcode_sds->start_no)
                                ->where('dm.locator_to','11')
                                ->where('dm.status_to','in')
                                ->OrderBy('dm.created_at','DESC')
                                ->first();

                                $type_name = $type_name->type_name == 'Non' ? 0 : $type_name->type_name;

                                $get_locator_set_sds = DB::connection('sds_live')->table('bundle_info_new as bi')
                                ->leftJoin('art_desc_new as ad',function($join){
                                    $join->on('ad.season','=','bi.season');
                                    $join->on('ad.style','=','bi.style');
                                    $join->on('ad.part_num','=','bi.part_num');
                                    $join->on('ad.part_name','=','bi.part_name');
                                })
                                ->where('bi.poreference',$barcode_header->poreference)
                                ->where('bi.style',$barcode_header->style)
                                ->where('bi.season',$barcode_header->season)
                                ->where('bi.article',$barcode_header->article)
                                ->where('ad.set_type',$type_name)             
                                ->where('bi.cut_num',$barcode_header->cut_num)
                                ->where('size',$barcode_header->size)
                                ->where('bi.start_num',$barcode_sds->start_no)
                                ->whereNotNull('setting')
                                ->orderBy('setting','DESC')
                                ->first();

                                $linkage_bi = $get_locator_set_cdms == null ? $get_locator_set_sds : $get_locator_set_cdms;
                            
                                if($process->process != null){
                                    $proc_id        = explode(',',$process->process);
                                    $loc_id_temp    = DB::table('master_process')->select('locator_id')->whereIn('id',$proc_id)->whereNull('deleted_at')->distinct('locator_id')->get();
                                    foreach($loc_id_temp as $lit){
                                        $locator_names = DB::table('master_locator')->where('id',$lit->locator_id)->whereNull('deleted_at')->first();
                                        $loc_arr[] = $lit->locator_id;
                                        if($barcode_header->factory_id == '1'){
                                            if(in_array(3,$loc_arr)){
                                                $scan_sds_art = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->first();
                                                if($scan_sds_art->artwork == null){
                                                    return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                                }
                                                if($scan_sds_art->artwork_out == null){
                                                    return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                                }
                                            }
                                        }else{
                                            $in_status     = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','in')->first();
                                            $out_status    = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','out')->first();
                                            if($in_status == null){
                                                return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                            }
                                            if($out_status == null){
                                                return response()->json('Bundle Belum Scan Out'.' - '.$locator_names->locator_name,422);
                                            }
                                        }
                                    }
                                    if($linkage_bi == null){
                                        return response()->json($barcode_sds->barcode_id, 331);
                                    }else{
                                        $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds->location : $get_locator_set_cdms->loc_dist;
                                        try{
                                            DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_sds->barcode_id,
                                                    'locator_from' => $last_move->locator_to,
                                                    'status_from'  => $last_move->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $loc_dist
                                                ]);
                                                DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                    'current_locator_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id',$barcode_sds->sds_barcode)
                                                ->update([
                                                    'location'      => $loc_dist,
                                                    'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                    'setting'       => Carbon::now(),
                                                    'setting_pic'   => \Auth::user()->nik,
                                                    'supply_unlock' => false
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_sds->barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_sds->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                'qty'           => $barcode_sds->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $loc_dist
                                            ];
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }else{
                                    if($linkage_bi == null){
                                        return response()->json($barcode_sds->barcode_id, 331);
                                    }else{
                                        $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds->location : $get_locator_set_cdms->loc_dist;
                                        try{
                                            DB::beginTransaction();
                                            DB::disableQueryLog();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_sds->barcode_id,
                                                    'locator_from' => $last_move->locator_to,
                                                    'status_from'  => $last_move->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $loc_dist
                                                ]);
                                                DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                    'current_locator_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id',$barcode_sds->sds_barcode)
                                                ->update([
                                                    'location'      => $loc_dist,
                                                    'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                    'setting'       => Carbon::now(),
                                                    'setting_pic'   => $nik_user->nik,
                                                    'supply_unlock' => false
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_sds->barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_sds->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                'qty'           => $barcode_sds->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $loc_dist
                                            ];
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }
                        }elseif($locator_id == '16'){
                            $out_supply_check = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                            if($out_supply_check != null){
                                return response()->json('Barcode sudah scan out Supply Ke '.$out_supply_check->loc_dist.'!',422);
                            }else{
                                $cek_out_cutting = DB::table('dd_bundle_check')->where('barcode_id', $barcode_sds->barcode_id)->first();
                                $get_info_barcode = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->get();
                                foreach($get_info_barcode as $gib){
                                    $check_move = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                    $out_cut    = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                    $last_move = DistribusiMovement::where('barcode_id',$gib->barcode_id)->orderBy('created_at','DESC')->first();
                                    if($out_cut == null){
                                        return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_sds->komponen_name.' || Cut -'.$barcode_header->cut_num.' || Size '.$barcode_header->size.' Belum Scan Cutting!',422);
                                    }elseif($check_move == null){
                                        return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_sds->komponen_name.' || Cut -'.$barcode_header->cut_num.' || Size '.$barcode_header->size.' Belum Scan Setting!',422);
                                    }
                                }
                                $list_bundle_komponen = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->pluck('barcode_id')->toArray();
                                try{
                                    DB::beginTransaction();
                                    DB::table('bundle_detail')
                                    ->whereIn('barcode_id',$list_bundle_komponen)
                                    ->update([
                                        'is_out_setting' => 'Y',
                                        'out_setting_at' => Carbon::now()
                                    ]);
                                    $description = $state.' '.$locator->locator_name;
                                    for($i = 0; $i < count($list_bundle_komponen); $i++){
                                        $data_insert[] = [
                                            'id'            => Uuid::generate()->string,
                                            'barcode_id'    => $list_bundle_komponen[$i],
                                            'deleted_at'    => null,
                                            'description'   => $description,
                                            'ip_address'    => $this->getIPClient(),
                                            'loc_dist'      => $line_supply,
                                            'locator_from'  => intval($last_move->locator_to),
                                            'locator_to'    => intval($locator_id),
                                            'status_from'   => $last_move->status_to,
                                            'status_to'     => $state,
                                            'user_id'       => \Auth::user()->id,
                                            'created_at'    => Carbon::now(),
                                            'updated_at'    => Carbon::now()
                                        ];
                                    }
                                    DistribusiMovement::insert($data_insert);
                                    DB::table('bundle_detail')->whereIn('barcode_id',$list_bundle_komponen)->update([
                                        'current_locator_id'    => $locator_id,
                                        'current_status_to'     => $state,
                                        'current_description'   => $state.' '.$locator->locator_name,
                                        'updated_movement_at'   => Carbon::now(),
                                        'current_user_id'       => \Auth::user()->id
                                    ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $barcode_sds->komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                    'qty'           => $barcode_sds->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $line_supply
                                ];
                                return response()->json($response,200);
                            }
                        
                        }elseif($locator_id == '7'){
                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->where('deleted_at', null)->first();
                            $komponen_name = $barcode_sds->komponen_name;
                            $out_cutting_check = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                            $is_ppa2 = DB::table('master_style_detail')->where('id',$barcode_sds->style_detail_id)->first();
                            if($is_ppa2 != null){
                                if($is_ppa2->is_ppa2 == false){
                                    return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                }
                            }else{
                                return response()->json('Master Style Dihapus Dari Sistem!',422);
                            }
                            $check_in_status = DistribusiMovement::where('barcode_id', $barcode_sds->barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                            $check_out_status = DistribusiMovement::where('barcode_id', $barcode_sds->barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                            if($state == 'in') {
                                if($check_in_status != null){
                                    return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                }else{
                                    $check_setting = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                    if($check_setting == null){
                                        return response()->json('Harap Scan Setting dahulu!',422);
                                    }else{
                                        try {
                                            DB::beginTransaction();
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => '-'
                                            ]);
    
                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                'current_locator_id' => $locator_id,
                                                'current_status_to' => $state,
                                                'current_description' => $state.' '.$locator->locator_name,
                                                'updated_movement_at' => Carbon::now(),
                                                'current_user_id' => \Auth::user()->id
                                            ]);
    
                                            $barcode_header = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
    
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_sds->komponen,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                'qty'           => $barcode_sds->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => '-'
                                            ];
    
                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }else{
                                if($check_in_status == null){
                                    return response()->json('Bundle Belum Scan In PPA2!',422);
                                }elseif($check_out_status != null){
                                    return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                }else{
                                    $barcode = DB::select("SELECT ms.season_name,bh.poreference,bh.style,t.type_name,bd.barcode_id,bd.bundle_header_id,bd.komponen_name,msd.is_ppa2,bd.style_detail_id
                                    FROM bundle_detail bd
                                    LEFT JOIN bundle_header bh on bh.id = bd.bundle_header_id
                                    LEFT JOIN master_style_detail msd on msd.id = bd.style_detail_id
                                    LEFT JOIN master_seasons ms on ms.id = msd.season
                                    LEFT JOIN types t on t.id = msd.type_id
                                    WHERE bd.bundle_header_id='$barcode_sds->bundle_header_id' AND msd.is_ppa2 = true AND bd.start_no='$barcode_sds->start_no' AND bd.end_no='$barcode_sds->end_no'");
                                    $barcode_group = [];
                                    foreach($barcode as $b){
                                        $barcode_group[] = $b->barcode_id;
                                    }
                                    $barcode_group = implode(',',$barcode_group);
                                    $barcode_finish = explode(',',$barcode_group);
                                    $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                    $check_barcode = implode(',',$is_all_in);
                                    foreach($barcode_finish as $bf){
                                        $unscanned = DB::table('bundle_detail')->where('barcode_id',$bf)->first();
                                        if(strpos($check_barcode,$bf) === false){
                                            return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                        }
                                    }
                                    try {
                                        DB::beginTransaction();
                                        $description = $state.' '.$locator->locator_name;
                                        for ($i = 0; $i < count($barcode_finish); $i++) {
                                            $data_insert[] = [
                                                'id'           => Uuid::generate()->string,
                                                'barcode_id'   => $barcode_finish[$i],
                                                'deleted_at'   => null,
                                                'description'  => $description,
                                                'ip_address'   => $this->getIPClient(),
                                                'loc_dist'     => $line_supply,
                                                'locator_from' => intval($out_cutting_check->locator_to),
                                                'locator_to'   => intval($locator_id),
                                                'status_from'  => $out_cutting_check->status_to,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'created_at'   => Carbon::now(),
                                                'updated_at'   =>Carbon::now()
                                            ];
                                        }
                                        DistribusiMovement::insert($data_insert);
    
                                        DB::table('bundle_detail')->whereIn('barcode_id',$barcode_finish)->update([
                                            'current_locator_id'    => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => \Auth::user()->id
                                        ]);
    
                                        $barcode_header = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
    
                                        $response = [
                                            'barcode_id'        => $barcode_sds->barcode_id,
                                            'style'             => $barcode_header->style,
                                            'article'           => $barcode_header->article,
                                            'po_buyer'          => $barcode_header->poreference,
                                            'part'              => $komponen_detail->part,
                                            'komponen_name'     => $komponen_name,
                                            'size'              => $barcode_header->size,
                                            'sticker_no'        => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                            'qty'               => $barcode_sds->qty,
                                            'cut'               => $barcode_header->cut_num,
                                            'process'           => $locator->locator_name,
                                            'status'            => $state,
                                            'loc_dist'          => '-'
                                        ];
                                        DB::commit();
    
                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                    return response()->json($response,200);
                                }
                            }
                        }else{
                            $cek_art = $this->sds_connection->table('art_desc_new')
                            ->where('style',$update_sds->style)
                            ->where('season',$update_sds->season)
                            ->where('part_name',$update_sds->part_name)
                            ->where('part_num',$update_sds->part_num)
                            ->first();
                            $convert_locator = $this->convertLocator($locator_id, $state);
                            $convert_locator_in = $this->convertLocator($locator_id, 'in');
                            //LOCATOR CUTTING!
                            if($convert_locator['art_desc']=='bd'){
                                $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                ->where('barcode_id', $barcode_id)
                                ->first();
                                if($bundle_info_sds != null){
                                    return response()->json('Barcode Sudah Scan di Meja -'.$bundle_info_sds->bundle_table,422);
                                }else{
                                    try{
                                        DB::beginTransaction();
                                        DB::disableQueryLog();
                                            $this->sds_connection->table('bundle_info_new')
                                            ->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                'location'                              => $convert_locator['locator_sds'],
                                                $convert_locator['column_update']       => Carbon::now(),
                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                            ]);
                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                'is_out_cutting'            => 'Y',
                                                'out_cutting_at'            => Carbon::now(),
                                                'current_locator_id'        => $locator_id,
                                                'current_status_to'         => $state,
                                                'current_description'       => $state.' '.$locator->locator_name,
                                                'updated_movement_at'       => Carbon::now(),
                                                'current_user_id'           => \Auth::user()->id
                                            ]);
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                'locator_from' => null,
                                                'status_from'  => null,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => '-',
                                                'bundle_table' => $bundle_table
                                            ]);
                                        DB::commit();
                                    }catch(Exception $e){
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                    $response = [
                                        'barcode_id'    => $barcode_sds->barcode_id,
                                        'style'         => $barcode_header->style,
                                        'article'       => $barcode_header->article,
                                        'po_buyer'      => $barcode_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $barcode_sds->komponen_name,
                                        'size'          => $barcode_header->size,
                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                        'qty'           => $barcode_sds->qty,
                                        'cut'           => $barcode_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $bundle_table
                                    ];
                                    return response()->json($response,200);
                                }
                                
                            }else{
                                if($bundle_info_sds == null){
                                    return response()->json('Barcode belum scan out cutting!',422);
                                }else{
                                    $get_process = explode(',', $komponen_detail->process);
                                    if($get_process[0] == null || $get_process[0] == '') {
                                        return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                    }
                                    $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                    if(in_array($locator_id, $get_locator_process)){
                                        //ALL LOCATOR SECONDARY
                                        $last_move      = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                        $in_check       = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$locator_id)->where('status_to','in')->first();
                                        $out_check      = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();
                                        $las_loc_name   = DB::table('master_locator')->where('id',$last_move->locator_to)->whereNull('deleted_at')->first();
                                        $in_check_sds   = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->whereNotnull($convert_locator_in['column_update'] )->first();
                                        $out_check_sds  = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->whereNotnull($convert_locator['column_update'] )->first();
                                        $required_in_out    = [1,2,3,4,5,6,7];
                                        if(in_array($last_move->locator_to,$required_in_out) && $last_move->status_to == 'in' && $out_check_sds != null && $last_move->locator_to != $locator_id){
                                            return response()->json('Harap Scan Out '.$las_loc_name->locator_name.' Dahulu#!',422);
                                        }elseif($in_check != null && $in_check_sds != null && $state == 'in'){
                                            return response()->json('Barcode Sudah Scan In '.$locator->locator_name.'!',422);
                                        }elseif($out_check != null && $out_check_sds != null && $state == 'out'){
                                            return response()->json('Barcode Sudah Scan Out '.$locator->locator_name.'!',422);
                                        }elseif($in_check == null && $in_check_sds == null && $state == 'out'){
                                            return response()->json('Barcode Belum Scan In '.$locator->locator_name.'!',422);
                                        }else{
                                            $scan_artwork_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->first();
                                            if(in_array(3,$get_locator_process)){
                                                $sds_artwork = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->first();
                                                if($sds_artwork->artwork == null && $locator_id != 3){
                                                    return response()->json('Barcode Belum Scan In ARTWORK!',422);
                                                }
                                                if($sds_artwork->artwork_out == null && $locator_id != 3){
                                                    return response()->json('Barcode Belum Scan Out ARTWORK!',422);
                                                }
                                            }
                                            //PPA 1
                                            if($convert_locator['art_desc']=='ppa')
                                            {
                                                if($state == 'out'){
                                                    try{
                                                        DB::beginTransaction();
                                                            $this->sds_connection->table('bundle_info_new')
                                                            ->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'        => $barcode_sds->barcode_id,
                                                                'locator_from'      => $last_move->locator_to,
                                                                'status_from'       => $last_move->status_to,
                                                                'locator_to'        => $locator_id,
                                                                'status_to'         => $state,
                                                                'user_id'           => \Auth::user()->id,
                                                                'description'       => $state.' '.$locator->locator_name,
                                                                'ip_address'        => $this->getIPClient(),
                                                                'deleted_at'        => null,
                                                                'loc_dist'          => '-',
                                                                'bundle_table'      => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }else{
                                                    //STATE IN
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                            }
                                            //LOCATOR HE SDS
                                            elseif($convert_locator['art_desc']=='he')
                                            {
                                                if($state == 'out'){
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                //STATE IN
                                                else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();    
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                            }
                                            //LOCATOR PAD PRINT SDS
                                            elseif($convert_locator['art_desc']=='pad')
                                            {
                                                if($state == 'out'){
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();    
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                //STATE IN
                                                else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();    
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                            }
                                            //LOCATOR BOBOK / AUTO
                                            elseif($convert_locator['art_desc']=='bobok')
                                            {
                                                if($state == 'out'){
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();    
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_num.' - '.$barcode_sds->end_num,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                //STATE IN
                                                else{
                                                    if(in_array(3,$get_locator_process)){
                                                        $in_art     = DistribusiMovement::select('barcode_id')->where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',3)->where('status_to','in')->first();
                                                        $out_art    = DistribusiMovement::select('barcode_id')->where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',3)->where('status_to','out')->first();
                                                        if($in_art == null || $out_art == null){
                                                            return response()->json('HARAP SCAN ARTWORK DAHULU!',422);
                                                        }
                                                    }else{
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();    
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => $convert_locator['factory_sds'],
                                                                ]);
                                                                DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                    'current_locator_id'        => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_sds->barcode_id,
                                                                    'locator_from' => $last_move->locator_to,
                                                                    'status_from'  => $last_move->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_sds->barcode_id,
                                                            'style'         => $barcode_header->style,
                                                            'article'       => $barcode_header->article,
                                                            'po_buyer'      => $barcode_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $barcode_sds->komponen_name,
                                                            'size'          => $barcode_header->size,
                                                            'sticker_no'    => $barcode_sds->start_num.' - '.$barcode_sds->end_num,
                                                            'qty'           => $barcode_sds->qty,
                                                            'cut'           => $barcode_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                            }
                                            //LOCATOR FUSE
                                            elseif($convert_locator['art_desc']=='fuse')
                                            {
                                                if($state == 'out'){
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location' => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update'] => Carbon::now(),
                                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                'factory' => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                            }
                                            //LOCATOR ARTWORK SDS
                                            elseif($convert_locator['art_desc']=='artwork')
                                            {
                                                if($state == 'out'){
                                                    if($scan_artwork_sds->artwork_out != null){
                                                        return response()->json('Barcode Sudah Scan Out Artwork!',422);
                                                    }
                                                    if($scan_artwork_sds->artwork != null){
                                                        $cek_user = DB::table('users')->where('nik',$scan_artwork_sds->artwork_pic)->first();
                                                        if($cek_user == null){
                                                            return response()->json('Terjadi Kesalahan Harap Hubungi ICT!#D0USR001',422);
                                                        }else{
                                                            DistribusiMovement::insert([
                                                                'id'           => Uuid::generate()->string,
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move->locator_to,
                                                                'status_from'  => $last_move->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => 'in',
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => 'in'.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'created_at'   => $scan_artwork_sds->artwork,
                                                                'updated_at'   => $scan_artwork_sds->artwork,
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        }
                                                    }
                                                    $last_move2      = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_sds->sds_barcode)->update([
                                                                'location'                              => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                'factory'                               => $convert_locator['factory_sds'],
                                                            ]);
                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                                                'current_locator_id'        => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_sds->barcode_id,
                                                                'locator_from' => $last_move2->locator_to,
                                                                'status_from'  => $last_move2->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'bundle_table' => null
                                                            ]);
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_sds->barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $barcode_sds->komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                                        'qty'           => $barcode_sds->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }else{
                                                    return response()->json('MODULE INI SUDAH TIDAK BERLAKU, GUNAKAN MODULE TERBARU!',422);
                                                }
                                                
                                            }
                                        }
                                    }else{
                                        return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                    }
                                }
                            }
                        }
                    }else{
                        return response()->json('KOMPONEN DI HAPUS DARI SISTEM!',422);
                    }
                }elseif($barcode_sds == null && $barcode_sds_bi != null && $barcode_sds_only != null){
                    $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                ->where('barcode_id', $barcode_id)
                                ->where('processing_fact',\Auth::user()->factory_id)
                                ->first();
                    $convert_locator = $this->convertLocator($locator_id, $state);
                    $bundle_out_cut     = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                    if($locator_id == '10'){
                        $user_check_sds = DB::connection('sds_live')->table('m_user')->where('nik',\Auth::user()->nik)->first();
                        if($user_check_sds != null){
                            if($bundle_out_cut != null){
                                return response()->json('Barcode Sudah Scan di Meja - '.$bundle_out_cut->bundle_table,422);
                            }else{
                                if($sds_header->set_type == 'TOP'){
                                    $set_type = 'TOP';
                                }elseif($sds_header->set_type == 'BOTTOM'){
                                    $set_type = 'BOTTOM';
                                }else{
                                    $set_type = 'Non';
                                }
                                $header_exists = DB::table('sds_header_movements')
                                ->where('poreference',$sds_header->poreference)
                                ->where('style',$sds_header->style)
                                ->where('set_type',$set_type)
                                ->where('season',$sds_header->season)
                                ->where('cut_num',$sds_header->cut_num)
                                ->where('size',$sds_header->size)
                                ->where('factory_id',$sds_header->processing_fact)
                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                ->exists();

                                $cut_date_sds_ver = DB::connection('sds_live')->table('bundle_info_new as bi')->leftJoin('cut_data_new as cd','cd.id','=','bi.cut_id')->where('bi.barcode_id',$barcode_id)->first();
                                $conv_cut_date = Carbon::parse($cut_date_sds_ver->ed_pcd)->format('Y-m-d');
    
                                $cut_plan_id    = DB::table('detail_cutting_plan')->where('po_buyer',$sds_header->poreference)->whereNull('deleted_at')->first();
                                $cut_plan_date  = DB::table('cutting_plans2')->where('id',$cut_plan_id->id_plan)->whereNull('deleted_at')->first();
                                $cut_date       = $cut_plan_date == null ? $conv_cut_date : $cut_plan_date->cutting_date;
    
                                $get_style_process = DB::table('master_style_detail as msd')
                                ->select('msd.id as style_id','msd.process as process_id','msd.part')
                                ->leftJoin('types as t','t.id','=','msd.type_id')
                                ->leftJoin('master_komponen as mk','mk.id','=','msd.komponen')
                                ->leftJoin('master_seasons as ms','ms.id','=','msd.season')
                                ->where('msd.style',$sds_header->style)
                                ->where('t.type_name',$set_type)
                                ->where('ms.season_name',$sds_header->season)
                                ->where('msd.part',$sds_header->part_num)
                                ->where('mk.komponen_name',$sds_header->part_name)
                                ->whereNull('msd.deleted_at')
                                ->first();
    
                                if($get_style_process != null){
                                    $style_id = $get_style_process == null ? null : $get_style_process->style_id;
                                    $process_id = $get_style_process == null ? null : $get_style_process->process_id;
                                    if(!$header_exists){
                                        try{
                                            DB::beginTransaction();
                                            DB::disableQueryLog();
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->update([
                                                    'location'                              => $convert_locator['locator_sds'],
                                                    $convert_locator['column_update']       => Carbon::now(),
                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                ]);
                                                DB::table('sds_header_movements')->insert([
                                                    'id'            => Uuid::generate()->string,
                                                    'season'        => $sds_header->season,
                                                    'style'         => trim($sds_header->style),
                                                    'set_type'      => trim($set_type),
                                                    'poreference'   => $sds_header->poreference,
                                                    'article'       => $sds_header->article,
                                                    'cut_num'       => $sds_header->cut_num,
                                                    'size'          => $sds_header->size,
                                                    'factory_id'    => (int)$sds_header->processing_fact,
                                                    'cutting_date'  => $cut_date,
                                                    'color_name'    => trim($sds_header->coll),
                                                    'created_by'    => \Auth::user()->id,
                                                    'created_at'    => Carbon::now(),
                                                    'updated_at'    => Carbon::now(),
                                                    'updated_by'    => null,
                                                    'sticker'       => $sds_header->start_num.'-'.$sds_header->end_num
                                                ]);
                                                $get_header_id = DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                ->first()->id;
                                                $is_recycle = DB::table('data_cuttings')
                                                ->where('po_buyer',$sds_header->poreference)
                                                ->where('part_no',$sds_header->part_num)
                                                ->where('size_finish_good',$sds_header->size)->first();
                                                $recycle = $is_recycle == null ? null : $is_recycle->is_recycle;
                                                $detail_exists = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->exists();
                                                if(!$detail_exists){
                                                    DB::table('sds_detail_movements')->insert([
                                                        'sds_header_id'         => $get_header_id,
                                                        'sds_barcode'           => $barcode_id,
                                                        'komponen_name'         => trim($sds_header->part_name),
                                                        'part_num'              => $sds_header->part_num,
                                                        'start_no'              => $sds_header->start_num,
                                                        'end_no'                => $sds_header->end_num,
                                                        'qty'                   => $sds_header->qty,
                                                        'factory_id'            => $sds_header->processing_fact,
                                                        'job'                   => $sds_header->style."::".$sds_header->poreference."::".$sds_header->article,
                                                        'is_recycle'            => $recycle,
                                                        'is_out_cutting'        => 'Y',
                                                        'is_out_cutting_at'     => Carbon::now(),
                                                        'out_setting_at'        => null,
                                                        'is_out_setting'        => null,
                                                        'current_location_id'   => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id,
                                                        'secondary_process'     => trim($sds_header->note),
                                                        'style_id'              => $style_id,
                                                        'process_id'            => $process_id
                                                    ]);
                                                }else{
                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                        'current_location_id'       => $locator_id,
                                                        'current_status_to'         => $state,
                                                        'current_user_id'           => \Auth::user()->id,
                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'       => Carbon::now()
                                                    ]);
                                                }
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => null,
                                                    'status_from'  => null,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-',
                                                    'bundle_table' => $bundle_table
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $sds_header->style,
                                                'article'       => $sds_header->article,
                                                'po_buyer'      => $sds_header->poreference,
                                                'part'          => $get_style_process->part,
                                                'komponen_name' => $sds_header->part_name,
                                                'size'          => $sds_header->size,
                                                'sticker_no'    => $sds_header->start_num.' - '.$sds_header->end_num,
                                                'qty'           => $sds_header->qty,
                                                'cut'           => $sds_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_table
                                            ];
                                            return response()->json($response,200);
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                    }else{
                                        try{
                                            DB::beginTransaction();
                                            DB::disableQueryLog();
                                                $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->update([
                                                    'location'                              => $convert_locator['locator_sds'],
                                                    $convert_locator['column_update']       => Carbon::now(),
                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                ]);
                                                DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)->update([
                                                    'updated_at'        => Carbon::now(),
                                                    'updated_by'        => \Auth::user()->id
                                                ]);
                                                $get_header_id = DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                ->first()->id;
                                                $is_recycle = DB::table('data_cuttings')
                                                ->where('po_buyer',$sds_header->poreference)
                                                ->where('part_no',$sds_header->part_num)
                                                ->where('size_finish_good',$sds_header->size)
                                                ->first();
                                                $recycle = $is_recycle == null ? null : $is_recycle->is_recycle;
                                                $detail_exists = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->exists();
                                                if(!$detail_exists){
                                                    DB::table('sds_detail_movements')->insert([
                                                        'sds_header_id'         => $get_header_id,
                                                        'sds_barcode'           => $barcode_id,
                                                        'komponen_name'         => trim($sds_header->part_name),
                                                        'part_num'              => $sds_header->part_num,
                                                        'start_no'              => $sds_header->start_num,
                                                        'end_no'                => $sds_header->end_num,
                                                        'qty'                   => $sds_header->qty,
                                                        'factory_id'            => $sds_header->processing_fact,
                                                        'job'                   => $sds_header->style."::".$sds_header->poreference."::".$sds_header->article,
                                                        'is_recycle'            => $recycle,
                                                        'is_out_cutting'        => 'Y',
                                                        'is_out_cutting_at'     => Carbon::now(),
                                                        'out_setting_at'        => null,
                                                        'is_out_setting'        => null,
                                                        'current_location_id'   => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id,
                                                        'secondary_process'     => trim($sds_header->note),
                                                        'style_id'              => $style_id,
                                                        'process_id'            => $process_id
                                                    ]);
                                                }else{
                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                        'current_location_id'           => $locator_id,
                                                        'current_status_to'             => $state,
                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                        'current_user_id'               => \Auth::user()->id,
                                                        'updated_movement_at'           => Carbon::now()
                                                    ]);
                                                }
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => null,
                                                    'status_from'  => null,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-',
                                                    'bundle_table' => $bundle_table
                                                ]);
                                            DB::commit();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $sds_header->style,
                                                'article'       => $sds_header->article,
                                                'po_buyer'      => $sds_header->poreference,
                                                'part'          => $get_style_process->part,
                                                'komponen_name' => $sds_header->part_name,
                                                'size'          => $sds_header->size,
                                                'sticker_no'    => $sds_header->start_num.' - '.$sds_header->end_num,
                                                'qty'           => $sds_header->qty,
                                                'cut'           => $sds_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_table
                                            ];
                                            return response()->json($response,200);
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                    }
                                }else{
                                    return response()->json('MASTER STYLE BELUM DIINPUT DI CDMS!',422);
                                }
                            }
                        }else{
                            return response()->json('Anda Tidak Mempunyai Akses SDS!',422);
                        }
                    }else{
                        if($bundle_out_cut != null){
                            if($barcode_sds_only != null){
                                $last_move_sds          = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                $barcode_header_sds     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                                $komponen_detail_sds    = DB::table('master_style_detail')->where('id', $barcode_sds_only->style_id)->whereNull('deleted_at')->first();
                                if($komponen_detail_sds != null){
                                    if($locator_id =='11'){
                                        $nik_user   = DB::table('users')->where('id',\Auth::user()->id)->first();
                                        $nik_sds    = DB::connection('sds_live')->table('m_user')->where('nik',$nik_user->nik)->first();
                                        if($nik_sds == null){
                                            return response()->json('ANDA TIDAK MEMILIKI AKSES DI SDS!',422);
                                        }
                                        $last_sett_sds = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                        if($last_sett_sds != null){
                                            return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_sett_sds->loc_dist,422);
                                        }else{
                                            $process_sds    = DB::table('v_master_style')->where('id',$barcode_sds_only->style_id)->first();
                                            //JIKA TERDAPAT PROSES SECONDARY MAKA AKAN DI LAKUKAN PENGECEKAN SUDAH SELESAI ATAU BELUM
                                            $type_name_sds = DB::table('types')->where('id',$komponen_detail_sds->type_id)->first();
                                            $linkage_bi_sds = DB::table('distribusi_movements as dm')
                                            ->select('dm.barcode_id','shm.season','shm.style','shm.poreference','shm.article','vms.type_name','dm.loc_dist','shm.size')
                                            ->Join('sds_detail_movements as sdm','sdm.sds_barcode','=','dm.barcode_id')
                                            ->Join('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')
                                            ->Join('v_master_style as vms','vms.id','=','sdm.style_id')
                                            ->where('shm.poreference',$barcode_header_sds->poreference)
                                            ->where('shm.style',$barcode_header_sds->style)
                                            ->where('shm.season',$barcode_header_sds->season)
                                            ->where('shm.article',$barcode_header_sds->article)
                                            ->where('vms.type_name',$type_name_sds->type_name)
                                            ->where('dm.locator_to','11')
                                            ->where('dm.status_to','in')
                                            ->OrderBy('dm.created_at','ASC')
                                            ->first();
                                            if($process_sds->process != null){
                                                $proc_id_sds        = explode(',',$process_sds->process);
                                                $loc_id_temp_sds    = DB::table('master_process')->select('locator_id')->whereIn('id',$proc_id_sds)->whereNull('deleted_at')->distinct('locator_id')->get();
                                                foreach($loc_id_temp_sds as $vx){
                                                    $last_loc_sds_check = DB::table('master_locator')->where('id',$vx->locator_id)->whereNull('deleted_at')->first();
                                                    $in_status     = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$vx->locator_id)->where('status_to','in')->first();
                                                    $out_status    = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$vx->locator_id)->where('status_to','out')->first();
                                                    if($in_status == null){
                                                        return response()->json('Bundle Belum Scan In'.' - '.$last_loc_sds_check->locator_name,422);
                                                    }
                                                    if($out_status == null){
                                                        return response()->json('Bundle Belum Scan Out'.' - '.$last_loc_sds_check->locator_name,422);
                                                    }
                                                }

                                                if($linkage_bi_sds == null){
                                                    return response()->json($barcode_id, 331);
                                                }else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_id,
                                                                'locator_from' => $last_move_sds->locator_to,
                                                                'status_from'  => $last_move_sds->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => $linkage_bi_sds->loc_dist
                                                            ]);
                                                            DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                'current_location_id'   => $locator_id,
                                                                'current_status_to'     => $state,
                                                                'current_description'   => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'   => Carbon::now(),
                                                                'current_user_id'       => \Auth::user()->id
                                                            ]);
                                                            $this->sds_connection->table('bundle_info_new')
                                                            ->where('barcode_id',$barcode_id)
                                                            ->update([
                                                                'location'      => $linkage_bi_sds->loc_dist,
                                                                'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                                'setting'       => Carbon::now(),
                                                                'setting_pic'   => \Auth::user()->nik,
                                                                'supply_unlock' => false
                                                            ]);
                                                        DB::commit();
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $linkage_bi_sds->loc_dist
                                                        ];
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    return response()->json($response,200);
                                                }
                                            }else{
                                                if($linkage_bi_sds == null){
                                                    return response()->json($barcode_id, 331);
                                                }else{
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::disableQueryLog();
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_id,
                                                                'locator_from' => $last_move_sds->locator_to,
                                                                'status_from'  => $last_move_sds->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => $linkage_bi_sds->loc_dist
                                                            ]);
                                                            DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                'current_location_id'   => $locator_id,
                                                                'current_status_to'     => $state,
                                                                'current_description'   => $state.' '.$locator->locator_name,
                                                                'updated_movement_at'   => Carbon::now(),
                                                                'current_user_id'       => \Auth::user()->id
                                                            ]);
                                                            $this->sds_connection->table('bundle_info_new')
                                                            ->where('barcode_id',$barcode_id)
                                                            ->update([
                                                                'location'      => $linkage_bi_sds->loc_dist,
                                                                'factory'       => 'AOI '.\Auth::user()->factory_id,
                                                                'setting'       => Carbon::now(),
                                                                'setting_pic'   => $nik_user->nik,
                                                                'supply_unlock' => false
                                                            ]);
                                                        DB::commit();
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $linkage_bi_sds->loc_dist
                                                        ];
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    return response()->json($response,200);
                                                }
                                            }
                                        }
                                    }elseif($locator_id == '16'){
                                        $barcode_header_sds = DB::table('sds_header_movements')->where('id',$barcode_sds_only->sds_header_id)->first();
                                        $out_supply_check = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                                        if($out_supply_check != null){
                                            return response()->json('Barcode sudah scan out Supply Ke '.$out_supply_check->loc_dist.'!',422);
                                        }else{
                                            $cek_out_cutting = DB::table('dd_bundle_check')->where('barcode_id', $barcode_id)->first();
                                            $get_info_barcode = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->get();
                                            foreach($get_info_barcode as $gib){
                                                $check_move = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                                $out_cut    = DistribusiMovement::where('barcode_id',$gib->barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                                $last_move = DistribusiMovement::where('barcode_id',$gib->barcode_id)->orderBy('created_at','DESC')->first();
                                                if($out_cut == null){
                                                    return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_sds_only->komponen_name.' || Cut -'.$barcode_header_sds->cut_num.' || Size '.$barcode_header_sds->size.' Belum Scan Cutting!',422);
                                                }elseif($check_move == null){
                                                    return response()->json('Barcode '.$gib->barcode_id.' || '.$barcode_sds_only->komponen_name.' || Cut -'.$barcode_header_sds->cut_num.' || Size '.$barcode_header_sds->size.' Belum Scan Setting!',422);
                                                }
                                            }
                                            $list_bundle_komponen = DB::table('dd_bundle_check')->where('bundle_header_id',$cek_out_cutting->bundle_header_id)->pluck('barcode_id')->toArray();
                                            try{
                                                DB::beginTransaction();
                                                DB::table('sds_detail_movements')
                                                ->whereIn('sds_barcode',$list_bundle_komponen)
                                                ->update([
                                                    'is_out_setting' => 'Y',
                                                    'out_setting_at' => Carbon::now()
                                                ]);
                                                $description = $state.' '.$locator->locator_name;
                                                for($i = 0; $i < count($list_bundle_komponen); $i++){
                                                    $data_insert[] = [
                                                        'id'            => Uuid::generate()->string,
                                                        'barcode_id'    => $list_bundle_komponen[$i],
                                                        'deleted_at'    => null,
                                                        'description'   => $description,
                                                        'ip_address'    => $this->getIPClient(),
                                                        'loc_dist'      => $line_supply,
                                                        'locator_from'  => intval($last_move->locator_to),
                                                        'locator_to'    => intval($locator_id),
                                                        'status_from'   => $last_move->status_to,
                                                        'status_to'     => $state,
                                                        'user_id'       => \Auth::user()->id,
                                                        'created_at'    => Carbon::now(),
                                                        'updated_at'    => Carbon::now()
                                                    ];
                                                }
                                                DistribusiMovement::insert($data_insert);
                                                DB::table('sds_detail_movements')->whereIn('sds_barcode',$list_bundle_komponen)->update([
                                                    'current_location_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
                                                DB::commit();
                                            }catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header_sds->style,
                                                'article'       => $barcode_header_sds->article,
                                                'po_buyer'      => $barcode_header_sds->poreference,
                                                'part'          => $komponen_detail_sds->part,
                                                'komponen_name' => $barcode_sds_only->komponen_name,
                                                'size'          => $barcode_header_sds->size,
                                                'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                'qty'           => $barcode_sds_only->qty,
                                                'cut'           => $barcode_header_sds->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $line_supply
                                            ];
                                            return response()->json($response,200);
                                        }
                                    }elseif($locator_id == '7'){
                                        $out_cutting_check = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                        $is_ppa2 = DB::table('master_style_detail')->where('id',$barcode_sds_only->style_id)->first();
                                        if($is_ppa2 != null){
                                            if($is_ppa2->is_ppa2 == false){
                                                return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                            }
                                        }else{
                                            return response()->json('Master Style Dihapus Dari Sistem!',422);
                                        }
                                        $check_in_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                                        $check_out_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                                        if($state == 'in') {
                                            if($check_in_status != null){
                                                return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                            }else{
                                                $check_setting = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                                if($check_setting == null){
                                                    return response()->json('Harap Scan Setting dahulu!',422);
                                                }else{
                                                    try {
                                                        DB::beginTransaction();
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => null
                                                        ]);
                
                                                        DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                            'current_location_id'   => $locator_id,
                                                            'current_status_to'     => $state,
                                                            'current_description'   => $state.' '.$locator->locator_name,
                                                            'updated_movement_at'   => Carbon::now(),
                                                            'current_user_id'       => \Auth::user()->id
                                                        ]);
                
                                                        $barcode_header = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header->style,
                                                            'article'       => $barcode_header->article,
                                                            'po_buyer'      => $barcode_header->poreference,
                                                            'part'          => $barcode_sds_only->part_num,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                
                                                        DB::commit();
                                                    } catch (Exception $e) {
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                    return response()->json($response,200);
                                                }
                                            }
                                        }else{
                                            if($check_in_status == null){
                                                return response()->json('Bundle Belum Scan In PPA2!',422);
                                            }elseif($check_out_status != null){
                                                return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                            }else{
                                                $barcode = DB::select("SELECT ms.season_name,shm.poreference,shm.style,t.type_name,sdm.barcode_id,sdm.sds_header_id,sdm.komponen_name,msd.is_ppa2,sdm.style_id
                                                FROM sds_detail_movements sdm
                                                LEFT JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
                                                LEFT JOIN master_style_detail msd on msd.id = sdm.style_id
                                                LEFT JOIN master_seasons ms on ms.id = msd.season
                                                LEFT JOIN types t on t.id = msd.type_id
                                                WHERE sdm.sds_header_id='$barcode_sds_only->sds_header_id' AND msd.is_ppa2 = true AND sdm.start_no='$barcode_sds_only->start_no' AND sdm.end_no='$barcode_sds_only->end_no'");
                                                $barcode_group = [];
                                                foreach($barcode as $b){
                                                    $barcode_group[] = $b->barcode_id;
                                                }
                                                $barcode_group = implode(',',$barcode_group);
                                                $barcode_finish = explode(',',$barcode_group);
                                                $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                                $check_barcode = implode(',',$is_all_in);
                                                foreach($barcode_finish as $bf){
                                                    $unscanned = DB::table('sds_detail_movements')->where('sds_barcode',$bf)->first();
                                                    if(strpos($check_barcode,$bf) === false){
                                                        return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                                    }
                                                }
                                                try {
                                                    DB::beginTransaction();
                                                    $description = $state.' '.$locator->locator_name;
                                                    for ($i = 0; $i < count($barcode_finish); $i++) {
                                                        $data_insert[] = [
                                                            'id'           => Uuid::generate()->string,
                                                            'barcode_id'   => $barcode_finish[$i],
                                                            'deleted_at'   => null,
                                                            'description'  => $description,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'loc_dist'     => null,
                                                            'locator_from' => intval($out_cutting_check->locator_to),
                                                            'locator_to'   => intval($locator_id),
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'created_at'   => Carbon::now(),
                                                            'updated_at'   => Carbon::now()
                                                        ];
                                                    }
                                                    DistribusiMovement::insert($data_insert);
                
                                                    DB::table('sds_detail_movements')->whereIn('barcode_id',$barcode_finish)->update([
                                                        'current_location_id'    => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id
                                                    ]);
                
                                                    $barcode_header = DB::table('sds_header_id')->where('id', $sds_detail->sds_header_id)->first();
                
                                                    $response = [
                                                        'barcode_id'        => $barcode_id,
                                                        'style'             => $barcode_header->style,
                                                        'article'           => $barcode_header->article,
                                                        'po_buyer'          => $barcode_header->poreference,
                                                        'part'              => $barcode_sds_only->part_num,
                                                        'komponen_name'     => $barcode_sds_only->komponen_name,
                                                        'size'              => $barcode_header->size,
                                                        'sticker_no'        => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                        'qty'               => $barcode_sds_only->qty,
                                                        'cut'               => $barcode_header->cut_num,
                                                        'process'           => $locator->locator_name,
                                                        'status'            => $state,
                                                        'loc_dist'          => '-'
                                                    ];
                                                    DB::commit();
                
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                                return response()->json($response,200);
                                            }
                                        }
                                    }else{
                                        $get_process = explode(',', $komponen_detail_sds->process);
                                        if($get_process[0] == null || $get_process[0] == '') {
                                            return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                        }
                                        $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                        if(in_array($locator_id, $get_locator_process)){
                                            $required_in_out_sds    = [1,2,3,4,5,6,7];
                                            $in_sec_sds             = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$locator_id)->where('status_to','in')->first();
                                            $out_sec_sds            = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();
                                            $las_loc_name           = DB::table('master_locator')->where('id',$last_move_sds->locator_to)->whereNull('deleted_at')->first();
                                            if(in_array($last_move_sds->locator_to,$required_in_out_sds) && $last_move_sds->status_to == 'in' && $last_move_sds->locator_to != $locator_id){
                                                return response()->json('Harap Scan Out '.$las_loc_name->locator_name.' Dahulu!',422);
                                            }elseif($in_sec_sds != null && $state == 'in'){
                                                return response()->json('Barcode Sudah Scan In '.$locator->locator_name.'!',422);
                                            }elseif($out_sec_sds != null && $state == 'out'){
                                                return response()->json('Barcode Sudah Scan Out '.$locator->locator_name.'!',422);
                                            }elseif($in_sec_sds == null && $state == 'out'){
                                                return response()->json('Harap Scan In '.$locator->locator_name.'!',422);
                                            }else{
                                                if(in_array(3,$get_locator_process)){
                                                    $in_artwork = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',3)->where('status_to','in')->first();
                                                    $out_artwork = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',3)->where('status_to','out')->first();
                                                    if($in_artwork == null){
                                                        return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                    }
                                                    if($out_artwork == null){
                                                        return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                    }
                                                }
                                                //PPA 1
                                                if($convert_locator['art_desc']=='ppa')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        //STATE IN
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $last_move_sds->locator_to,
                                                                    'status_from'  => $last_move_sds->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                                //HE
                                                elseif($convert_locator['art_desc']=='he')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        //STATE IN
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $last_move_sds->locator_to,
                                                                    'status_from'  => $last_move_sds->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                                //PAD
                                                elseif($convert_locator['art_desc']=='pad')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        //STATE IN
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $last_move_sds->locator_to,
                                                                    'status_from'  => $last_move_sds->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                                //BOBOK / AUTO
                                                elseif($convert_locator['art_desc']=='bobok')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        //STATE IN
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $last_move_sds->locator_to,
                                                                    'status_from'  => $last_move_sds->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                                //FUSE
                                                elseif($convert_locator['art_desc']=='fuse')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        //STATE IN
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $last_move_sds->locator_to,
                                                                    'status_from'  => $last_move_sds->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'bundle_table' => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                }
                                                //FUSE
                                                elseif($convert_locator['art_desc']=='artwork')
                                                {
                                                    if($state == 'out'){
                                                        try{
                                                            DB::beginTransaction();
                                                            DB::disableQueryLog();
                                                                $this->sds_connection->table('bundle_info_new')
                                                                ->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => 'AOI '.\Auth::user()->factory_id,
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'        => $barcode_id,
                                                                    'locator_from'      => $last_move_sds->locator_to,
                                                                    'status_from'       => $last_move_sds->status_to,
                                                                    'locator_to'        => $locator_id,
                                                                    'status_to'         => $state,
                                                                    'user_id'           => \Auth::user()->id,
                                                                    'description'       => $state.' '.$locator->locator_name,
                                                                    'ip_address'        => $this->getIPClient(),
                                                                    'deleted_at'        => null,
                                                                    'loc_dist'          => '-',
                                                                    'bundle_table'      => null
                                                                ]);
                                                            DB::commit();
                                                        }catch(Exception $e){
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $barcode_header_sds->style,
                                                            'article'       => $barcode_header_sds->article,
                                                            'po_buyer'      => $barcode_header_sds->poreference,
                                                            'part'          => $komponen_detail_sds->part,
                                                            'komponen_name' => $barcode_sds_only->komponen_name,
                                                            'size'          => $barcode_header_sds->size,
                                                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                                            'qty'           => $barcode_sds_only->qty,
                                                            'cut'           => $barcode_header_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        return response()->json('MODULE INI SUDAH TIDAK BERLAKU, GUNAKAN MODULE TERBARU!',422);
                                                    }
                                                }
                                                else{
                                                    return response()->json('TERJADI KESALAHAN HARAP REFRESH HALAMAN INI!',422);
                                                }
                                            }
                                        }else{
                                            return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                        }
                                    }
                                }else{
                                    return response()->json('KOMPONEN DI HAPUS DARI SISTEM!',422);
                                }
                            }else{
                                return response()->json('Barcode Belum Scan Out Cutting!#D001',422);
                            }
                        }else{
                            return response()->json('Barcode Belum Scan Out Cutting!',422);
                        }
                    }
                }elseif($barcode_sds_only != null && $barcode_sds != null){
                    return response()->json('Terdapat Duplikat Data Pada Detail Barcode!',422);
                }else{
                    return response()->json('Barcode Tidak Dikenali!',422);
                }
            }else{
                return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
            }
        }
    }

    public function scanComponent_nonsds(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            $locator_id = $request->locator_id;
            $state = $request->state;

            if($barcode_id != null && $locator_id != null) {

                $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                if($barcode_check != null) {

                    if($locator->is_cutting) {

                        // locator cutting

                        $component_movement_out = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                        if($component_movement_out->count() > 0) {
                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                        } else {

                            $bundle_check = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = DB::table('master_komponen')->where('id', $komponen_detail->komponen)->where('deleted_at', null)->first()->komponen_name;

                            try {
                                DB::beginTransaction();

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => null,
                                    'status_from'  => null,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => '-'
                                ]);

                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => '-'
                                ];

                                // if($barcode_check->sds_barcode != null) {
                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                //     if($update_sds != null) {
                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                //                 'location' => $convert_locator['locator_sds'],
                                //                 $convert_locator['column_update'] => Carbon::now(),
                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                //                 'factory' => $convert_locator['factory_sds'],
                                //             ]);
                                //         }
                                //     }
                                // }

                                DB::commit();

                            } catch (Exception $e) {
                                DB::rollback();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }

                            return response()->json($response,200);
                        }

                            } else if($locator->is_setting){

                                $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                                if($out_cutting_check != null){

                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                                    $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                                    $bundle_terima = $movement_check->count();
                                    $bundle_check  = $movement_check->first();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    $komponen_name = $barcode_check->komponen_name;

                                    if($bundle_terima<1){
                                        $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                        $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                                        $ppcm_check = DB::table('master_ppcm')->where([
                                            ['master_ppcm.factory_id',$bundle_header->factory_id],
                                            ['set_type',$set_type],
                                            ['season',$bundle_header->season],
                                            ['articleno',$bundle_header->article],
                                            ['po_buyer',$bundle_header->poreference],
                                            ['master_ppcm.deleted_at', null]
                                        ])
                                        ->select('master_ppcm.*','master_setting.area_name')
                                        ->leftJoin('master_setting', 'master_ppcm.area_id', '=', 'master_setting.id')
                                        ->first();

                                        if(!$ppcm_check){
                                            return response()->json($barcode_id, 331);
                                        } else {
                                            //master_style_detail
                                            $style_detail_id = $barcode_check->style_detail_id;

                                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                                            if(strlen(trim($process_id->process))>0){
                                                $array_process = explode(",",$process_id->process);
                                            }
                                            else{
                                                $array_process = array();
                                            }

                                            if(count($array_process) == 0){

                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $ppcm_check->area_name
                                                ]);

                                                // if($bundle_terima == $banyak_komponen){
                                                    // dd($barcode_hasil->qty);
                                                    $qty_actual = ($ppcm_check->qty_actual == null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                                                    MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                // }

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $bundle_header->style,
                                                    'article'       => $bundle_header->article,
                                                    'po_buyer'      => $bundle_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $bundle_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $bundle_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => $ppcm_check->area_name
                                                ];
                                                return response()->json($response,200);
                                            } else {
                                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                                $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                                                $banyak_process = count($get_process_locator) + 1;

                                                array_push($array_locator,10);

                                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                                $count_process_check = $process_check->count();

                                                if($count_process_check == $banyak_process){
                                                    if($ppcm_check->qty_actual < $ppcm_check->qty_split){

                                                        $barcode_bundle_search = DB::table('bundle_detail')->where([
                                                            ['bundle_header_id', $barcode_check->bundle_header_id],
                                                            ['start_no',$barcode_check->start_no],
                                                            ['end_no',$barcode_check->end_no]
                                                        ]);

                                                        $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                                        $barcode_hasil = $barcode_bundle_search->first();

                                                        $banyak_komponen = $barcode_bundle_search->count();

                                                        // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                                                        // $bundle_terima = $movement_check->count();
                                                        // $bundle_check  = $movement_check->first();

                                                        $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                                        $komponen_name = $barcode_check->komponen_name;

                                                        $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                                                        if($check_movement > 0) {
                                                            return response()->json('Barcode sudah di scan in!',422);
                                                        }

                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $ppcm_check->area_name
                                                        ]);

                                                        $qty_actual = ($ppcm_check->qty_actual==null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                                                        MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);


                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $ppcm_check->area_name
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        //qty berlebihan
                                                        return response()->json($barcode_id, 331);
                                                    }
                                                } else {

                                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                                    $info_belum_scan = '';
                                                    foreach ($locator_belum_scan as $key => $value) {
                                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                                    }

                                                    $info = trim($info_belum_scan,", ");

                                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                                }
                                            }
                                        }

                                    } else {

                                        $style_detail_id = $barcode_check->style_detail_id;

                                        $process_id = DB::table('master_style_detail')->where('id',$style_detail_id)->first();

                                        if($process_id->process == null || $process_id->process == '' ){
                                            $array_process = array();
                                        } else {
                                            $array_process = explode(",",$process_id->process);
                                        }

                                        if(count($array_process) < 1){

                                            $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                                            if($check_movement > 0) {
                                                return response()->json('Barcode sudah di scan in!',422);
                                            }
                                            // dd(!$bundle_check);
                                            if(!$bundle_check){
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $bundle_check->loc_dist
                                                ]);

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $bundle_header->style,
                                                    'article'       => $bundle_header->article,
                                                    'po_buyer'      => $bundle_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $bundle_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $bundle_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => $bundle_check->loc_dist
                                                ];
                                                return response()->json($response,200);
                                            } else {
                                                if($bundle_check->locator_to == $locator_id){

                                                    $check_record = DistribusiMovement::where([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $out_cutting_check->locator_to,
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => $bundle_check->loc_dist
                                                    ])->exists();

                                                    if(!$check_record){
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $bundle_check->loc_dist
                                                        ]);

                                                        // if($bundle_terima == $banyak_komponen){
                                                        //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                                                        //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                        // }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $bundle_check->loc_dist
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah pernah scan!',422);
                                                    }

                                                }
                                                else{
                                                    return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                                }

                                            }
                                        } else {
                                            $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                            $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                                            $banyak_process = count($get_process_locator) + 1;

                                            array_push($array_locator,10);

                                            $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                            $count_process_check = $process_check->count();

                                            if($count_process_check == $banyak_process){

                                                $barcode_bundle_search = DB::table('bundle_detail')->where([
                                                    ['bundle_header_id', $barcode_check->bundle_header_id],
                                                    ['start_no',$barcode_check->start_no],
                                                    ['end_no',$barcode_check->end_no]
                                                ]);

                                                $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                                $barcode_hasil = $barcode_bundle_search->first();

                                                $banyak_komponen = $barcode_bundle_search->count();

                                                // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                                                // $bundle_terima = $movement_check->count();
                                                // $bundle_check  = $movement_check->first();

                                                $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                                if($bundle_check->locator_to == $locator_id){

                                                    $check_record = DistribusiMovement::where([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $out_cutting_check->locator_to,
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => $bundle_check->loc_dist
                                                    ])->exists();

                                                    if(!$check_record){
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $bundle_check->loc_dist
                                                        ]);

                                                        // $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                        // if($bundle_terima == $banyak_komponen){
                                                        //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                                                        //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                        // }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $bundle_check->loc_dist
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah pernah scan!',422);
                                                    }

                                                }
                                                else{
                                                    return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                                }
                                            }
                                            else{

                                                $get_process_check = $process_check->pluck('locator_to')->toArray();

                                                $array_belum_scan = array_diff($array_locator,$get_process_check);

                                                $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                                $info_belum_scan = '';
                                                foreach ($locator_belum_scan as $key => $value) {
                                                    $info_belum_scan .= ''.$value->locator_name.', ';
                                                }

                                                $info = trim($info_belum_scan,", ");

                                                return response()->json('Barcode belum discan proses '.$info.'!',422);
                                            }
                                        }
                                    }

                                    $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                                }
                                else{
                                    return response()->json('Barcode belum out cutting!',422);
                                }
                                ////////////////////////////////////////////////////////////////////////////////////////////////////
                    }else if($locator->is_artwork) {

                        // locator artwork

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);

                                                //             $type_set = DB::table('types')->where('id', $komponen_detail->type_id)->first();
                                                //             if($type_set->type_name == 'Non') {
                                                //                 $type_set = '0';
                                                //             } else {
                                                //                 $type_set = $type_set->type_name;
                                                //             }

                                                //             $check_data_exists = $this->sds_connection->table('v_packing_list_new')
                                                //                                     ->where('style', $barcode_header->style)
                                                //                                     ->where('set_type', $type_set)
                                                //                                     ->where('poreference', $barcode_header->poreference)
                                                //                                     ->where('article', $barcode_header->article)
                                                //                                     ->where('size', $barcode_header->size)
                                                //                                     ->where('component', $komponen_name)
                                                //                                     ->where('part_num', $komponen_detail->part)
                                                //                                     ->where('processing_fact', $barcode_header->factory_id)
                                                //                                     ->first();

                                                //             if($check_data_exists == null) {
                                                //                 $check_data_exists = $this->sds_connection->table('v_packing_list_new')->insert([
                                                //                     'style' => $barcode_header->style,
                                                //                     'set_type' => $type_set,
                                                //                     'poreference' => $barcode_header->poreference,
                                                //                     'article' => $barcode_header->article,
                                                //                     'size' => $barcode_header->size,
                                                //                     'component' => $komponen_name,
                                                //                     'part_num' => $komponen_detail->part,
                                                //                     'processing_fact' => $barcode_header->factory_id,
                                                //                     'coll' => $barcode_header->color_name,
                                                //                     'sum_qty' => $barcode_check->qty,
                                                //                     'first_check' => Carbon::now(),
                                                //                     'first_user' => \Auth::user()->nik,
                                                //                     'qty_aloc' => 0,
                                                //                     'last_check' => Carbon::now(),
                                                //                     'last_user' => \Auth::user()->nik,
                                                //                     'is_out' => false,
                                                //                 ]);
                                                //             } else {
                                                //                 $sum_qty = $check_data_exists->sum_qty + $barcode_check->qty;
                                                //                 $check_data_exists = $this->sds_connection->table('v_packing_list_new')->where('id', $check_data_exists->id)->update([
                                                //                     'sum_qty' => $sum_qty,
                                                //                     'last_check' => Carbon::now(),
                                                //                     'last_user' => \Auth::user()->nik,
                                                //                 ]);
                                                //             }
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];
                                                DB::commit();

                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    } else {

                        // locator secondary process

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();

                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    }
                } else {
                    return response()->json('Barcode tidak dikenali!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat input data!',422);
            }
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }


    public function scanManualSet(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = 11;
            $state = 'in';
            $area_name = $request->area_name;
            //SCAN WITH SELECT AREA SETTING USING THIS CODE NOT scanComponent function
            if($barcode_id != null && $locator_id != null && $area_name != null) {
                $user = \Auth::user()->nik;
                $locator = DB::table('master_locator')->where('id', $locator_id)->first();
                $barcode_cdms   = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $barcode_sds    = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
                $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();

                if($barcode_cdms != null){
                    $last_move          = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_cdms->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_cdms->style_detail_id)->whereNull('deleted_at')->first();
                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($area_check == '0') {
                        $area_name = 'L.'.substr($area_name, 1);
                    } else {
                        $area_name = 'L.'.$area_name;
                    }
                    try{
                        DB::beginTransaction();
                        DB::disableQueryLog();
                            DistribusiMovement::FirstOrCreate([
                                'barcode_id'   => $barcode_id,
                                'locator_from' => $last_move->locator_to,
                                'status_from'  => $last_move->status_to,
                                'locator_to'   => $locator_id,
                                'status_to'    => $state,
                                'user_id'      => \Auth::user()->id,
                                'description'  => $state.' '.$locator->locator_name,
                                'ip_address'   => $this->getIPClient(),
                                'deleted_at'   => null,
                                'loc_dist'     => $area_name
                            ]);
                            DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                'current_locator_id'    => $locator_id,
                                'current_status_to'     => $state,
                                'current_description'   => $state.' '.$locator->locator_name,
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_cdms->sds_barcode)
                            ->update([
                                'location'      => $area_name,
                                'factory'       => 'AOI '.\Auth::user()->factory_id,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    $response = [
                        'barcode_id'    => $barcode_id,
                        'style'         => $barcode_header->style,
                        'article'       => $barcode_header->article,
                        'po_buyer'      => $barcode_header->poreference,
                        'part'          => $komponen_detail->part,
                        'komponen_name' => $barcode_cdms->komponen_name,
                        'size'          => $barcode_header->size,
                        'sticker_no'    => $barcode_cdms->start_no.' - '.$barcode_cdms->end_no,
                        'qty'           => $barcode_cdms->qty,
                        'cut'           => $barcode_header->cut_num,
                        'process'       => $locator->locator_name,
                        'status'        => $state,
                        'loc_dist'      => $request->area_name
                    ];
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds != null){
                    $last_move          = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->whereNull('deleted_at')->first();
                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($area_check == '0') {
                        $area_name = 'L.'.substr($area_name, 1);
                    } else {
                        $area_name = 'L.'.$area_name;
                    }
                    try{
                        DB::beginTransaction();
                        DB::disableQueryLog();
                            DistribusiMovement::FirstOrCreate([
                                'barcode_id'   => $barcode_sds->barcode_id,
                                'locator_from' => $last_move->locator_to,
                                'status_from'  => $last_move->status_to,
                                'locator_to'   => $locator_id,
                                'status_to'    => $state,
                                'user_id'      => \Auth::user()->id,
                                'description'  => $state.' '.$locator->locator_name,
                                'ip_address'   => $this->getIPClient(),
                                'deleted_at'   => null,
                                'loc_dist'     => $area_name
                            ]);
                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                'current_locator_id'    => $locator_id,
                                'current_status_to'     => $state,
                                'current_description'   => $state.' '.$locator->locator_name,
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_id)
                            ->update([
                                'location'      => $area_name,
                                'factory'       => 'AOI '.\Auth::user()->factory_id,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $barcode_sds->barcode_id,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $komponen_detail->part,
                            'komponen_name' => $barcode_sds->komponen_name,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                            'qty'           => $barcode_sds->qty,
                            'cut'           => $barcode_header->cut_num,
                            'process'       => $locator->locator_name,
                            'status'        => $state,
                            'loc_dist'      => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
                    $last_move          = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                    $barcode_header     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds_only->style_id)->whereNull('deleted_at')->first();
                    if(\Auth::user()->factory_id == '2'){
                        $area_name = substr($area_name, 4);
                        $area_check = substr($area_name, 0, 1);
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                        } else {
                            $area_name = 'L.'.$area_name;
                        }
                    }else{
                        $area_name = $area_name;
                    }
                    try{
                        DB::beginTransaction();
                        DB::disableQueryLog();
                            DistribusiMovement::FirstOrCreate([
                                'barcode_id'   => $barcode_id,
                                'locator_from' => $last_move->locator_to,
                                'status_from'  => $last_move->status_to,
                                'locator_to'   => $locator_id,
                                'status_to'    => $state,
                                'user_id'      => \Auth::user()->id,
                                'description'  => $state.' '.$locator->locator_name,
                                'ip_address'   => $this->getIPClient(),
                                'deleted_at'   => null,
                                'loc_dist'     => $area_name
                            ]);
                            DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                'current_location_id'    => $locator_id,
                                'current_status_to'     => $state,
                                'current_description'   => $state.' '.$locator->locator_name,
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_id)
                            ->update([
                                'location'      => $area_name,
                                'factory'       => 'AOI '.\Auth::user()->factory_id,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $barcode_id,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $komponen_detail->part,
                            'komponen_name' => $barcode_sds_only->komponen_name,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                            'qty'           => $barcode_sds_only->qty,
                            'cut'           => $barcode_header->cut_num,
                            'process'       => $locator->locator_name,
                            'status'        => $state,
                            'loc_dist'      => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_sds != null && $barcode_sds_only != null){
                    return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
                }else{
                    return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                }
            }else{
                return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
            }
        }
    }

    public function scanManualSet_nonsds(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = 11;
            $state = 'in';
            $area_name = $request->area_name;

            if($barcode_id != null && $locator_id != null && $area_name != null) {

                $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                if($barcode_check != null) {

                    $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                    if($out_cutting_check != null){

                        $barcode_bundle_search = DB::table('bundle_detail')->where([
                            ['bundle_header_id', $barcode_check->bundle_header_id],
                            ['start_no',$barcode_check->start_no],
                            ['end_no',$barcode_check->end_no]
                        ]);

                        $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                        $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                        $barcode_hasil = $barcode_bundle_search->first();

                        $banyak_komponen = $barcode_bundle_search->count();

                        $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                        $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                        $bundle_terima = $movement_check->count();
                        $bundle_check  = $movement_check->first();

                        $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                        $komponen_name = $barcode_check->komponen_name;

                        if($bundle_terima < 1){
                            $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                            $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                            $style_detail_id = $barcode_check->style_detail_id;

                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                            if(strlen(trim($process_id->process))>0){
                                $array_process = explode(",",$process_id->process);
                            }
                            else{
                                $array_process = array();
                            }

                            if(count($array_process) == 0){

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => $out_cutting_check->locator_to,
                                    'status_from'  => $out_cutting_check->status_to,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => $area_name
                                ]);

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $bundle_header->style,
                                    'article'       => $bundle_header->article,
                                    'po_buyer'      => $bundle_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $bundle_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $bundle_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $area_name
                                ];
                                return response()->json($response,200);
                            } else {
                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                                $banyak_process = count($get_process_locator) + 1;

                                array_push($array_locator,10);

                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                $count_process_check = $process_check->count();

                                if($count_process_check == $banyak_process){
                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    $komponen_name = $barcode_check->komponen_name;

                                    $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                                    if($check_movement > 0) {
                                        return response()->json('Barcode sudah di scan in!',422);
                                    }

                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => $out_cutting_check->locator_to,
                                        'status_from'  => $out_cutting_check->status_to,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => $area_name
                                    ]);

                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $bundle_header->style,
                                        'article'       => $bundle_header->article,
                                        'po_buyer'      => $bundle_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $komponen_name,
                                        'size'          => $bundle_header->size,
                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                        'qty'           => $barcode_check->qty,
                                        'cut'           => $bundle_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $area_name
                                    ];
                                    return response()->json($response,200);
                                } else {

                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                    $info_belum_scan = '';
                                    foreach ($locator_belum_scan as $key => $value) {
                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                    }

                                    $info = trim($info_belum_scan,", ");

                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                }
                            }
                        } else {

                            $style_detail_id = $barcode_check->style_detail_id;

                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                            if($process_id->process == null || $process_id->process == '' ){
                                $array_process = array();
                            } else {
                                $array_process = explode(",",$process_id->process);
                            }

                            if(count($array_process) < 1){

                                $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                                if($check_movement > 0) {
                                    return response()->json('Barcode sudah di scan in!',422);
                                }
                                // dd(!$bundle_check);
                                if(!$bundle_check){
                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => $out_cutting_check->locator_to,
                                        'status_from'  => $out_cutting_check->status_to,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => $bundle_check->loc_dist
                                    ]);

                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $bundle_header->style,
                                        'article'       => $bundle_header->article,
                                        'po_buyer'      => $bundle_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $komponen_name,
                                        'size'          => $bundle_header->size,
                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                        'qty'           => $barcode_check->qty,
                                        'cut'           => $bundle_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $bundle_check->loc_dist
                                    ];
                                    return response()->json($response,200);
                                } else {
                                    if($bundle_check->locator_to == $locator_id){

                                        $check_record = DistribusiMovement::where([
                                            'barcode_id'   => $barcode_id,
                                            'locator_from' => $out_cutting_check->locator_to,
                                            'status_from'  => $out_cutting_check->status_to,
                                            'locator_to'   => $locator_id,
                                            'status_to'    => $state,
                                            'description'  => $state.' '.$locator->locator_name,
                                            'deleted_at'   => null,
                                            'loc_dist'     => $bundle_check->loc_dist
                                        ])->exists();

                                        if(!$check_record){
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'ip_address'   => $this->getIPClient(),
                                                'description'  => $state.' '.$locator->locator_name,
                                                'deleted_at'   => null,
                                                'loc_dist'     => $bundle_check->loc_dist
                                            ]);

                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $bundle_header->style,
                                                'article'       => $bundle_header->article,
                                                'po_buyer'      => $bundle_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $bundle_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $bundle_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_check->loc_dist
                                            ];

                                            return response()->json($response,200);
                                        }
                                        else{
                                            return response()->json('Barcode sudah pernah scan!',422);
                                        }

                                    }
                                    else{
                                        return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                    }

                                }
                            } else {
                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                                $banyak_process = count($get_process_locator) + 1;

                                array_push($array_locator,10);

                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                $count_process_check = $process_check->count();

                                if($count_process_check == $banyak_process){

                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    if($bundle_check->locator_to == $locator_id){

                                        $check_record = DistribusiMovement::where([
                                            'barcode_id'   => $barcode_id,
                                            'locator_from' => $out_cutting_check->locator_to,
                                            'status_from'  => $out_cutting_check->status_to,
                                            'locator_to'   => $locator_id,
                                            'status_to'    => $state,
                                            'description'  => $state.' '.$locator->locator_name,
                                            'deleted_at'   => null,
                                            'loc_dist'     => $bundle_check->loc_dist
                                        ])->exists();

                                        if(!$check_record){
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => $bundle_check->loc_dist
                                            ]);

                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $bundle_header->style,
                                                'article'       => $bundle_header->article,
                                                'po_buyer'      => $bundle_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $bundle_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $bundle_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_check->loc_dist
                                            ];

                                            return response()->json($response,200);
                                        }
                                        else{
                                            return response()->json('Barcode sudah pernah scan!',422);
                                        }

                                    }
                                    else{
                                        return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                    }
                                }
                                else{

                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                    $info_belum_scan = '';
                                    foreach ($locator_belum_scan as $key => $value) {
                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                    }

                                    $info = trim($info_belum_scan,", ");

                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                }
                            }
                        }

                        $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                    }
                    else{
                        return response()->json('Barcode belum out cutting!',422);
                    }
                } else {
                    return response()->json('Barcode tidak dikenali!',422);
                }
            }
        }
    }

    //QUERY AJAX SELECT OPTION PILIH MEJA BUNDLE
    public function ajaxGetDataBundletableJson(Request $request)
    {
        if ($request->has('q')) {
            $term = trim(strtoupper($request->q));
            if (empty($term)) {
                return \Response::json([]);
            }
            $tags = BundleTable::where('factory_id',\Auth::user()->factory_id)
            ->Where('id_table', 'like', '%'.$term.'%')
            ->wherenull('deleted_at')
            ->get();
    
            return response()->json($tags);
        }
    }

    private function convertLocator($locator, $state)
    {
        // id base database master_locator
        switch ($locator) {
            case 10:
                //cutting
                $art_desc = 'bd';
                $locator_sds = 'BD';
                $column_update = 'distrib_received';
                $column_update_pic = 'bd_pic';
                break;
            case 6:
                $art_desc = 'ppa';
                $locator_sds = 'PPA';
                if($state == 'in') {
                    $column_update = 'ppa';
                    $column_update_pic = 'ppa_pic';
                } else {
                    $column_update = 'ppa_out';
                    $column_update_pic = 'ppa_out_pic';
                }
                break;
            case 2:
                $art_desc = 'he';
                $locator_sds = 'HE';
                if($state == 'in') {
                    $column_update = 'he';
                    $column_update_pic = 'he_pic';
                } else {
                    $column_update = 'he_out';
                    $column_update_pic = 'he_out_pic';
                }
                break;
            case 4:
                $art_desc = 'pad';
                $locator_sds = 'PAD';
                if($state == 'in') {
                    $column_update = 'pad';
                    $column_update_pic = 'pad_pic';
                } else {
                    $column_update = 'pad_out';
                    $column_update_pic = 'pad_out_pic';
                }
                break;
            case 5:
                //bobok
                $art_desc = 'bobok';
                $locator_sds = 'WP';
                if($state == 'in') {
                    $column_update = 'bobok';
                    $column_update_pic = 'bobok_pic';
                } else {
                    $column_update = 'bobok_out';
                    $column_update_pic = 'bobok_out_pic';
                }
                break;
            case 1:
                $art_desc = 'fuse';
                $locator_sds = 'FUSE';
                if($state == 'in') {
                    $column_update = 'fuse';
                    $column_update_pic = 'fuse_pic';
                } else {
                    $column_update = 'fuse_out';
                    $column_update_pic = 'fuse_out_pic';
                }
                break;
            case 3:
                //artwork
                $art_desc = 'artwork';
                $locator_sds = 'ART';
                if($state == 'in') {
                    $column_update = 'artwork';
                    $column_update_pic = 'artwork_pic';
                } else {
                    $column_update = 'artwork_out';
                    $column_update_pic = 'artwork_out_pic';
                }
                break;
            default:
                $art_desc          = null;
                $locator_sds       = null;
                $column_update     = null;
                $column_update_pic = null;
        }

        if(\Auth::user()->factory_id == '1') {
            $factory_sds = 'AOI 1';
        } else {
            $factory_sds = 'AOI 2';
        }

        $convert_locator = [
            'art_desc'          => $art_desc,
            'locator_sds'       => $locator_sds,
            'column_update'     => $column_update,
            'column_update_pic' => $column_update_pic,
            'factory_sds'       => $factory_sds,
        ];

        return $convert_locator;
    }

    public function SetLocatorLine(Request $request){
        
        // $locator_sets = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
        $locator_sets = MasterSetting::whereNull('deleted_at')
        ->where('factory_id', Auth::user()->factory_id)
        ->whereNotNull('display_order')
        ->orderBy('display_order', 'asc')
        ->orderBy('order_name', 'asc')
        ->get();

        $barcode_id = $request->barcode_id;
        if(Auth::user()->factory_id == 1){
            return view('distribusi._view_line_set_new',compact('barcode_id','locator_sets'));
        }else{
            return view('distribusi._view_line_set_new',compact('barcode_id','locator_sets'));
        }
    }

    public function moveLocatorLine(Request $request){
        
        $locator_sets = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
        $locator_set = MasterSetting::whereNull('deleted_at')
        ->where('factory_id', Auth::user()->factory_id)
        ->orderBy('display_order', 'asc')
        ->orderBy('order_name', 'asc')
        ->get();

        $barcode_id = $request->barcode_id;
        if(Auth::user()->factory_id == 2){
            return view('distribusi.move_locator_set',compact('barcode_id','locator_sets'));
        }else{
            return view('distribusi.move_locator_set2',compact('barcode_id','locator_set'));
        }
    }

    public function scanMoveBundle(Request $request){
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = $request->locator_id;
            $state = $request->state;
            $condition = $request->condition;
            if($condition == '1'){
                if($barcode_id != null && $locator_id != null && $state != null){
                    $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
                    $barcode_cdms       = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                    $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
                    $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                    if($barcode_cdms != null){
                        $is_setting         = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                        $is_supply          = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                        if($is_supply != null){
                            return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                        }elseif($is_setting == null){
                            return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
                        }else{
                            $obj = new StdClass();
                            $obj->barcode_id = $barcode_id;
                            $obj->condition = $condition;
                            return response()->json($obj, 200);
                        }
                    }elseif($barcode_cdms == null && $barcode_sds != null){
                        $is_setting         = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                        $is_supply          = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                        if($is_supply != null){
                            return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                        }elseif($is_setting == null){
                            return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
                        }else{
                            $obj = new StdClass();
                            $obj->barcode_id = $barcode_id;
                            $obj->condition = $condition;
                            return response()->json($obj, 200);
                        }
                    }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
                        $is_setting         = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                        $is_supply          = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                        if($is_supply != null){
                            return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                        }elseif($is_setting == null){
                            return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
                        }else{
                            $obj = new StdClass();
                            $obj->barcode_id = $barcode_id;
                            $obj->condition = $condition;
                            return response()->json($obj, 200);
                        }
                    }elseif($barcode_sds != null && $barcode_sds_only != null){
                        return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
                    }else{
                        return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                    }
                }else{
                    return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                }
            }else{
                if($barcode_id != null && $locator_id != null && $state != null){
                    $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
                    $barcode_cdms       = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                    $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
                    $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                    if($barcode_cdms != null){
                        $data_detail      = DB::table('bundle_detail')->where('bundle_header_id',$barcode_cdms->bundle_header_id);
                        $data_barcode       = $data_detail->get();
                        foreach($data_barcode as $db){
                            $is_setting         = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                            $is_supply          = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                            if($is_supply != null){
                                return response()->json('Barcode '.$db->barcode_id.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                            }elseif($is_setting == null){
                                return response()->json('Scan Barcode '.$db->barcode_id.' dengan Module Yang Sudah Ada!',422);
                            }else{
                                $obj = new StdClass();
                                $obj->barcode_id = $barcode_id;
                                $obj->condition = $condition;
                                return response()->json($obj, 200);
                            }
                        }
                    }elseif($barcode_cdms == null && $barcode_sds != null){
                        $data_detail      = DB::table('bundle_detail')->where('bundle_header_id',$barcode_sds->bundle_header_id);
                        $data_barcode       = $data_detail->get();
                        foreach($data_barcode as $db){
                            $is_setting         = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                            $is_supply          = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                            if($is_supply != null){
                                return response()->json('Barcode '.$db->barcode_id.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                            }elseif($is_setting == null){
                                return response()->json('Scan Barcode '.$db->barcode_id.' dengan Module Yang Sudah Ada!',422);
                            }else{
                                $obj = new StdClass();
                                $obj->barcode_id = $barcode_id;
                                $obj->condition = $condition;
                                return response()->json($obj, 200);
                            }
                        }
                    }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
                        $data_detail        = DB::table('sds_detail_movements')->where('bundle_header_id',$barcode_sds_only->sds_header_id);
                        $data_barcode       = $data_detail->get();
                        foreach($data_barcode as $db){
                            $is_setting         = DistribusiMovement::where('barcode_id',$db->sds_barcode)->where('locator_to','11')->where('status_to','in')->first();
                            $is_supply          = DistribusiMovement::where('barcode_id',$db->sds_barcode)->where('locator_to','16')->where('status_to','out')->first();
                            if($is_supply != null){
                                return response()->json('Barcode '.$db->sds_barcode.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
                            }elseif($is_setting == null){
                                return response()->json('Scan Barcode '.$db->sds_barcode.' dengan Module Yang Sudah Ada!',422);
                            }else{
                                $obj = new StdClass();
                                $obj->barcode_id = $barcode_id;
                                $obj->condition = $condition;
                                return response()->json($obj, 200);
                            }
                        }
                    }elseif($barcode_sds != null && $barcode_sds_only != null){
                        return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
                    }else{
                        return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                    }
                }else{
                    return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                }
            }
        }
    }

    public function selectLocatorMove(Request $request)
    {
        $barcode_id = $request->barcode_id;
        $locator_id = 11;
        $state = 'move';
        $area_name = $request->area_name;
        $condition = $request->condition;
        //SCAN WITH SELECT AREA SETTING USING THIS CODE NOT scanComponent function
        if($condition == '1'){
            if($barcode_id != null && $locator_id != null && $area_name != null) {
                $user = \Auth::user()->nik;
                $barcode_cdms   = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $barcode_sds    = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
                $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
    
                if($barcode_cdms != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_cdms->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_cdms->style_detail_id)->whereNull('deleted_at')->first();
                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($barcode_header->factory_id == 2){
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name = $request->area_name;
                        $line_num = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->where('barcode_id',$barcode_cdms->barcode_id)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_cdms->sds_barcode)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    $response = [
                        'barcode_id'    => $barcode_id,
                        'style'         => $barcode_header->style,
                        'article'       => $barcode_header->article,
                        'po_buyer'      => $barcode_header->poreference,
                        'part'          => $komponen_detail->part,
                        'komponen_name' => $barcode_cdms->komponen_name,
                        'size'          => $barcode_header->size,
                        'sticker_no'    => $barcode_cdms->start_no.' - '.$barcode_cdms->end_no,
                        'qty'           => $barcode_cdms->qty,
                        'cut'           => $barcode_header->cut_num,
                        'from'          => $line_num,
                        'to'            => $request->area_name
                    ];
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->whereNull('deleted_at')->first();
                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($barcode_header->factory_id ==2){
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name  = $request->area_name;
                        $line_num   = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_sds->sds_barcode)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $barcode_sds->barcode_id,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $komponen_detail->part,
                            'komponen_name' => $barcode_sds->komponen_name,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                            'qty'           => $barcode_sds->qty,
                            'cut'           => $barcode_header->cut_num,
                            'from'          => $line_num,
                            'to'            => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    
                    $barcode_header     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                    $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds_only->style_id)->whereNull('deleted_at')->first();
                    if($barcode_header->factory_id == '2'){
                        $area_name = substr($area_name, 4);
                        $area_check = substr($area_name, 0, 1);
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name  = $request->area_name;
                        $line_num   = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_id)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $barcode_id,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $komponen_detail->part,
                            'komponen_name' => $barcode_sds_only->komponen_name,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                            'qty'           => $barcode_sds_only->qty,
                            'cut'           => $barcode_header->cut_num,
                            'from'          => $line_num,
                            'to'            => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_sds != null && $barcode_sds_only != null){
                    return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
                }else{
                    return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                }
            }else{
                return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
            }
        }elseif($condition == '2'){
            if($barcode_id != null && $locator_id != null && $area_name != null) {
                $user = \Auth::user()->nik;
                $barcode_cdms   = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $barcode_sds    = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
                $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                if($barcode_cdms != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_cdms->bundle_header_id)->first();
                    $data_detail        = DB::table('bundle_detail')->where('bundle_header_id',$barcode_header->id);
                    $barcode_group      = $data_detail->pluck('barcode_id')->toArray();
                    $disp_barcode       = implode(',',$barcode_group);
                    $barcode_group_sds  = $data_detail->pluck('sds_barcode')->toArray();
                    $barcode_groups     = $data_detail->get();
                    $style_detail_group = $data_detail->pluck('style_detail_id')->toArray();
                    $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
                    $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
                    $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
                    if($barcode_header->factory_id == 2){
                        $area_name = substr($area_name, 4);
                        $area_check = substr($area_name, 0, 1);
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name = $request->area_name;
                        $line_num = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->whereIn('barcode_id',$barcode_group)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->whereIn('barcode_id',$barcode_group_sds)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    $response = [
                        'barcode_id'    => $disp_barcode,
                        'style'         => $barcode_header->style,
                        'article'       => $barcode_header->article,
                        'po_buyer'      => $barcode_header->poreference,
                        'part'          => $disp_part,
                        'komponen_name' => $disp_komponen,
                        'size'          => $barcode_header->size,
                        'sticker_no'    => $barcode_cdms->start_no.' - '.$barcode_cdms->end_no,
                        'qty'           => $barcode_cdms->qty,
                        'cut'           => $barcode_header->cut_num,
                        'from'          => $line_num,
                        'to'            => $request->area_name
                    ];
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
                    $data_detail        = DB::table('bundle_detail')->where('bundle_header_id',$barcode_header->id);
                    $barcode_group      = $data_detail->pluck('barcode_id')->toArray();
                    $disp_barcode       = implode(',',$barcode_group);
                    $barcode_group_sds  = $data_detail->pluck('sds_barcode')->toArray();
                    $barcode_groups     = $data_detail->get();
                    $style_detail_group = $data_detail->pluck('style_detail_id')->toArray();
                    $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
                    $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
                    $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
                    if($barcode_header->factory_id ==2){
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name  = $request->area_name;
                        $line_num   = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->where('barcode_id',$barcode_group)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->where('barcode_id',$barcode_group_sds)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $disp_barcode,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $disp_part,
                            'komponen_name' => $disp_komponen,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                            'qty'           => $barcode_sds->qty,
                            'cut'           => $barcode_header->cut_num,
                            'from'          => $line_num,
                            'to'            => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
                    $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds_only->sds_barcode)->where('locator_to','11')->where('status_to','in')->first();
                    $loc_dist           = explode('.',$last_loc->loc_dist);
                    $barcode_header     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                    $data_detail        = DB::table('sds_detail_movements')->where('sds_header_id',$barcode_header->id);
                    $barcode_group      = $data_detail->pluck('sds_barcode')->toArray();
                    $disp_barcode       = implode(',',$barcode_group);
                    $barcode_groups     = $data_detail->get();
                    $style_detail_group = $data_detail->pluck('style_id')->toArray();
                    $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
                    $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
                    $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
                    if($barcode_header->factory_id == '2'){
                        $area_name = substr($area_name, 4);
                        $area_check = substr($area_name, 0, 1);
                        if($area_check == '0') {
                            $area_name = 'L.'.substr($area_name, 1);
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        } else {
                            $area_name = 'L.'.$area_name;
                            $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
                        }
                    }else{
                        $area_name  = $request->area_name;
                        $line_num   = $last_loc->loc_dist;
                    }
                    try{
                        DB::beginTransaction();
                            DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
                                'loc_dist'     => $area_name,
                                'ip_address'   => $this->getIPClient(),
                                'user_id'      => \Auth::user()->id,
                                'created_at'   => Carbon::now()
                            ]);
                            
                            DB::table('bundle_detail')->whereIn('barcode_id',$barcode_group)->update([
                                'updated_movement_at'   => Carbon::now(),
                                'current_user_id'       => \Auth::user()->id
                            ]);
    
                            $this->sds_connection->table('bundle_info_new')
                            ->whereIn('barcode_id',$barcode_group)
                            ->update([
                                'location'      => $area_name,
                                'setting'       => Carbon::now(),
                                'setting_pic'   => $user,
                                'supply_unlock' => false
                            ]);
                        DB::commit();
                        $response = [
                            'barcode_id'    => $disp_barcode,
                            'style'         => $barcode_header->style,
                            'article'       => $barcode_header->article,
                            'po_buyer'      => $barcode_header->poreference,
                            'part'          => $disp_part,
                            'komponen_name' => $disp_komponen,
                            'size'          => $barcode_header->size,
                            'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                            'qty'           => $barcode_sds_only->qty,
                            'cut'           => $barcode_header->cut_num,
                            'from'          => $line_num,
                            'to'            => $request->area_name
                        ];
                    }catch(Exception $e){
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                    return response()->json($response,200);
                }elseif($barcode_sds != null && $barcode_sds_only != null){
                    return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
                }else{
                    return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
                }
            }else{
                return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
            }
        }else{
            return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
        }
    }
}

