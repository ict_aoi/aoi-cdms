<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');;
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

// MANAGEMENT PERMISSION

Route::prefix('/permission')->middleware(['permission:menu-permission'])->group(function(){
    Route::get('', 'PermissionController@index')->name('permission.index');
    Route::get('data', 'PermissionController@data')->name('permission.data');
    Route::post('store', 'PermissionController@store')->name('permission.store');
    Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
    Route::put('update/{id}', 'PermissionController@update')->name('permission.update');
    Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
});

// MANAGEMENT ROLE

Route::prefix('/role')->middleware(['permission:menu-role'])->group(function(){
    Route::get('', 'RoleController@index')->name('role.index');
    Route::get('data', 'RoleController@data')->name('role.data');
    Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
    Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
    Route::put('update/{id}', 'RoleController@update')->name('role.update');
    Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
});

// MANAGEMENT USER

Route::prefix('/user')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'UserController@index')->name('user.index');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
    Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
    Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::post('active/{id}', 'UserController@active')->name('user.active');
    Route::delete('delete/{id}', 'UserController@destroy')->name('user.destroy');
});

// DOWNLOAD REQUEST SSP

Route::prefix('/download-request-rasio')->middleware(['permission:menu-request-rasio-download'])->group(function(){
    Route::get('', 'PolaMarker\RequestRasioController@indexDownload')->name('markerRequestRasio.indexDownload');
    Route::post('download', 'PolaMarker\RequestRasioController@download')->name('markerRequestRasio.download');
});

// PERMINTAAN MARKER MANUAL CUTTING
Route::prefix('/permintaan-marker')->middleware(['permission:menu-cutting-preparation'])->group(function(){
    Route::get('', 'RequestMarker\PermintaanMarkerController@index')->name('permintaanMarker.index');
    Route::post('/upload-excel', 'RequestMarker\PermintaanMarkerController@uploadExcel')->name('permintaanMarker.uploadExcel');
    Route::get('/get-check-alokasi/{id}', 'RequestMarker\PermintaanMarkerController@getCheckAlokasi')->name('permintaanMarker.getCheckAlokasi');
    Route::post('/delete-check-alokasi', 'RequestMarker\PermintaanMarkerController@deleteCheckAlokasi')->name('permintaanMarker.deleteCheckAlokasi');
    Route::get('/get-data', 'RequestMarker\PermintaanMarkerController@getData')->name('permintaanMarker.getData');
    Route::get('/get-data-detail', 'RequestMarker\PermintaanMarkerController@getDataDetail')->name('permintaanMarker.getDataDetail');
    Route::post('/get-size-balance', 'RequestMarker\PermintaanMarkerController@getSizeBalance')->name('permintaanMarker.getSizeBalance');
    Route::get('/get-check-fb/{id}', 'RequestMarker\PermintaanMarkerController@getCheckFB')->name('permintaanMarker.getCheckFB');
    Route::post('/delete-ratio', 'RequestMarker\PermintaanMarkerController@deleteRatio')->name('permintaanMarker.deleteRatio');
    Route::post('/delete-part-no', 'RequestMarker\PermintaanMarkerController@deletePartNo')->name('permintaanMarker.deletePartNo');
});

// PRIORITAS PLANNING CUTTING

Route::prefix('/prioritas-plan')->middleware(['permission:menu-cutting-preparation'])->group(function(){
    Route::get('', 'RequestMarker\PrioritasPlanController@index')->name('prioritasPlan.index');
    Route::get('/get-data', 'RequestMarker\PrioritasPlanController@ajaxGetPlaning')->name('prioritasPlan.ajaxGetPlaning');
    Route::get('/combine-plan/{id}', 'RequestMarker\PrioritasPlanController@modalCombinePlan')->name('prioritasPlan.modalCombinePlan');
    Route::post('/combine-plan-action', 'RequestMarker\PrioritasPlanController@combinePlanAction')->name('prioritasPlan.combinePlanAction');
    Route::get('/get-data-combine-plan', 'RequestMarker\PrioritasPlanController@getDataCombinePlan')->name('prioritasPlan.getDataCombinePlan');
    Route::post('/download-prioritas', 'RequestMarker\PrioritasPlanController@downloadPrioritas')->name('prioritasPlan.downloadPrioritas');
    Route::get('/get-po', 'RequestMarker\PrioritasPlanController@getPO')->name('prioritasPlan.getPO');
    Route::get('/delete-all', 'RequestMarker\PrioritasPlanController@deletedAll')->name('prioritasPlan.deletedAll');
    Route::post('/set-plan/daily-sync-day', 'RequestMarker\PrioritasPlanController@dailySyncDay')->name('prioritasPlan.dailySyncDay');
    Route::post('/set-plan/daily-sync', 'RequestMarker\PrioritasPlanController@dailySync')->name('prioritasPlan.dailySync');
    Route::post('/set-plan/daily-sync-7', 'RequestMarker\PrioritasPlanController@dailySync7')->name('prioritasPlan.dailySync7');
    Route::post('/get-po/partial-in', 'RequestMarker\PrioritasPlanController@partialIn')->name('prioritasPlan.partialIn');
    Route::post('/all-in', 'RequestMarker\PrioritasPlanController@allIn')->name('prioritasPlan.allIn');
    Route::post('/edit-queu', 'RequestMarker\PrioritasPlanController@edQueu')->name('prioritasPlan.edQueu');
    Route::post('/split-topbot', 'RequestMarker\PrioritasPlanController@splitTopBot')->name('prioritasPlan.splitTopBot');
    Route::post('/get-po/delete-partial', 'RequestMarker\PrioritasPlanController@delpart')->name('prioritasPlan.delpart');
    Route::post('/marge-topbot', 'RequestMarker\PrioritasPlanController@margeTopBot')->name('prioritasPlan.margeTopBot');
    Route::post('/combine-plan-action', 'RequestMarker\PrioritasPlanController@combinePlanAction')->name('prioritasPlan.combinePlanAction');
    Route::post('/delete-combine-plan-action/{id}', 'RequestMarker\PrioritasPlanController@canceCombinePlanAction')->name('prioritasPlan.canceCombinePlanAction');
});

// PRIORITAS PLANNING CUTTING // migrasi

Route::prefix('/preparation')->middleware(['permission:menu-cutting-preparation'])->group(function(){
    Route::get('', 'Preparation\PreparationController@index')->name('preparation.index');
    Route::get('/get-data', 'Preparation\PreparationController@ajaxGetPlaning')->name('preparation.ajaxGetPlaning');
    Route::post('/download-prioritas', 'Preparation\PreparationController@downloadPrioritas')->name('preparation.downloadPrioritas');
    Route::get('/combine-plan/{id}', 'Preparation\PreparationController@combinePlan')->name('preparation.combinePlan');
    // Route::get('/set-plan/date/{date}/style/{style}/article/{article}/color/{color}/size/{size_category}/mo_created/{mo_created}/', 'Preparation\PreparationController@setPlan')->name('preparation.setPlan');getPO


    Route::post('/all-in', 'Preparation\PreparationController@allIn')->name('preparation.allIn');
    Route::get('/delete-all', 'Preparation\PreparationController@deletedAll')->name('preparation.deletedAll');
    Route::get('/get-po', 'Preparation\PreparationController@getPO')->name('preparation.getPO');
    Route::post('/get-po/partial-in', 'Preparation\PreparationController@partialIn')->name('preparation.partialIn');
    Route::post('/get-po/delete-partial', 'Preparation\PreparationController@delpart')->name('preparation.delpart');
    Route::post('/edit-queu', 'Preparation\PreparationController@edQueu')->name('preparation.edQueu');


    // Route::post('/set-plan/in-queu', 'Preparation\PreparationController@insertQueu')->name('preparation.inQueu');
    // Route::get('/set-plan/getMo', 'Preparation\PreparationController@getMo')->name('preparation.getMo');
    // Route::post('/set-plan/delete-queu', 'Preparation\PreparationController@deleteQueu')->name('preparation.deleteQueu');

    Route::get('/upload-permintaan', 'Preparation\PreparationController@upload_permintaan')->name('preparation.upload');
    Route::get('/upload-permintaan/get-data', 'Preparation\PreparationController@ajaxGetPlan')->name('preparation.ajaxGetPlan');
    Route::get('/upload-permintaan/detrat', 'Preparation\PreparationController@ajaxGetDetRat')->name('preparation.ajaxGetDetRat');
    Route::post('upload-permintaan/shoot-width','Preparation\PreparationController@shootFabricWidth')->name('preparation.shootFabricWidth');
    Route::post('/upload-permintaan/delrat', 'Preparation\PreparationController@delrat')->name('preparation.delrat');
    Route::get('/upload-permintaan/upload', 'Preparation\PreparationController@uploadRequestMarker')->name('preparation.uploadRequestMarker');
    Route::post('/upload-permintaan/uploadExcel', 'Preparation\PreparationController@uploadExcel')->name('preparation.uploadExcel');
    Route::post('/upload-permintaan/uploadxls', 'Preparation\PreparationController@uploadxls')->name('preparation.uploadxls');
    Route::post('/upload-permintaan/delpartno', 'Preparation\PreparationController@delpartno')->name('preparation.delpartno');

    // request marker menu-actual-marker
    Route::get('/upload-permintaan-manual', 'Preparation\PreparationController@uploadManual')->name('preparation.uploadManual');
    Route::post('/upload-permintaan-manual/uploadxlsmanual', 'Preparation\PreparationController@uploadxlsmanual')->name('preparation.uploadxlsmanual');

    Route::post('/set-plan/sync', 'Preparation\PreparationController@sync')->name('preparation.sync');#sync data infos

    Route::post('/set-plan/daily-sync', 'Preparation\PreparationController@dailySync')->name('preparation.dailySync');#sync data infos
    Route::post('/set-plan/daily-sync-7', 'Preparation\PreparationController@dailySync7')->name('preparation.dailySync7');#sync data infos

    Route::post('/split-topbot', 'Preparation\PreparationController@splitTopBot')->name('preparation.splitTopBot');#splittopbot
    Route::post('/marge-topbot', 'Preparation\PreparationController@margeTopBot')->name('preparation.margeTopBot');#splittopbot

    Route::get('/fabric-prepared', 'Preparation\PreparationController@fabricPrepared')->name('preparation.fabricPrepared');
    Route::get('/get-fabric-prepared', 'Preparation\PreparationController@getFabricPrepared')->name('preparation.getFabricPrepared');
    Route::get('/export-fabric-prepared', 'Preparation\PreparationController@exportFabricPrepared')->name('preparation.exportFabricPrepared');

    Route::post('/get-size-balance', 'Preparation\PreparationController@getSizeBalance')->name('preparation.getSizeBalance');
    Route::get('/get-check-fb/{id}', 'Preparation\PreparationController@getCheckFB')->name('preparation.getCheckFB');

    Route::get('/get-check-alokasi/{id}', 'Preparation\PreparationController@getCheckAlokasi')->name('preparation.getCheckAlokasi');
    Route::post('/delete-check-alokasi', 'Preparation\PreparationController@deleteCheckAlokasi')->name('preparation.deleteCheckAlokasi');
});

// Route::prefix('/permintaan-marker')->middleware(['permission:menu-cutting-preparation'])->group(function() {
//     Route::get('/get-check-alokasi/{id}', 'RequestMarker\PermintaanMarker@getCheckAlokasi')->name('permintaanMarker.getCheckAlokasi');
//     Route::post('/delete-check-alokasi', 'RequestMarker\PermintaanMarker@deleteCheckAlokasi')->name('permintaanMarker.deleteCheckAlokasi');
// });

// UPLOAD FILE SSP

Route::prefix('/upload-file-ssp')->middleware(['permission:menu-upload-file-ssp'])->group(function(){
    Route::get('', 'RequestMarker\UploadFileSSPController@index')->name('UploadFileSSP.index');
    Route::get('data', 'RequestMarker\UploadFileSSPController@data')->name('UploadFileSSP.data');
    Route::get('dialog/upload/{id}', 'RequestMarker\UploadFileSSPController@uploadFile')->name('UploadFileSSP.uploadFile');
    Route::post('data-file', 'RequestMarker\UploadFileSSPController@dataFile')->name('UploadFileSSP.dataFile');
    Route::delete('data-file/delete/{id}', 'RequestMarker\UploadFileSSPController@dataFileDelete')->name('UploadFileSSP.dataFileDelete');
    Route::post('data/file-upload', 'RequestMarker\UploadFileSSPController@dataFileUpload')->name('UploadFileSSP.dataFileUpload');
    Route::get('data-file/download/{id}', 'RequestMarker\UploadFileSSPController@dataFileDownload')->name('UploadFileSSP.dataFileDownload');
    Route::get('data-file/download/{id}/view', 'RequestMarker\UploadFileSSPController@dataFileDownloadView')->name('UploadFileSSP.dataFileDownloadView');
});

// DOWNLOAD FILE SSP

Route::prefix('/download-file-ssp')->middleware(['permission:menu-download-file-ssp'])->group(function(){
    Route::get('', 'RequestMarker\DownloadFileSSPController@index')->name('downloadFileSSP.index');
    Route::get('data', 'RequestMarker\DownloadFileSSPController@data')->name('downloadFileSSP.data');
    Route::get('dialog/upload/{id}', 'RequestMarker\DownloadFileSSPController@uploadFile')->name('downloadFileSSP.uploadFile');
    Route::post('data-file', 'RequestMarker\DownloadFileSSPController@dataFile')->name('downloadFileSSP.dataFile');
    Route::post('data/file-upload', 'RequestMarker\DownloadFileSSPController@dataFileUpload')->name('downloadFileSSP.dataFileUpload');
    Route::get('data-file/download/{id}', 'RequestMarker\DownloadFileSSPController@dataFileDownload')->name('downloadFileSSP.dataFileDownload');
    Route::get('data-file/view/{id}/download', 'RequestMarker\DownloadFileSSPController@dataFileDownloadView')->name('downloadFileSSP.dataFileDownloadView');
    Route::post('terima-file/{id}', 'RequestMarker\DownloadFileSSPController@terimaFile')->name('downloadFileSSP.terimaFile');
});

// NOTIF

Route::get('notif/data', 'General\NotifController@data')->name('notif.data');

// ACTUAL MARKER

Route::prefix('/actual-marker')->middleware(['permission:menu-actual-marker'])->group(function(){
    Route::get('', 'RequestMarker\ActualMarkerController@index')->name('actualMarker.index');
    Route::get('data-planning', 'RequestMarker\ActualMarkerController@dataPlanning')->name('actualMarker.dataPlanning');
    Route::get('data-marker', 'RequestMarker\ActualMarkerController@dataMarker')->name('actualMarker.dataMarker');
    Route::get('data-detail-marker', 'RequestMarker\ActualMarkerController@dataDetailMarker')->name('actualMarker.dataDetailMarker');
    Route::get('data-lebar-fabrik', 'RequestMarker\ActualMarkerController@dataLebarFB')->name('actualMarker.dataLebarFB');
    Route::get('data-size-balance', 'RequestMarker\ActualMarkerController@dataSizeBalance')->name('actualMarker.dataSizeBalance');
    Route::get('data-over-consum', 'RequestMarker\ActualMarkerController@dataOverConsum')->name('actualMarker.dataOverConsum');
    Route::get('/detail-modal/{id}', 'RequestMarker\ActualMarkerController@detailModal')->name('actualMarker.detailModal');
    Route::post('/post-data-marker', 'RequestMarker\ActualMarkerController@updateDataMarker')->name('actualMarker.updateDataMarker');
    Route::get('/detail-marker/{id}/detail/{part_no}', 'RequestMarker\ActualMarkerController@detailDetailModal')->name('actualMarker.detailDetailModal');
    Route::post('/approve-data-marker', 'RequestMarker\ActualMarkerController@approveDataMarker')->name('actualMarker.approveDataMarker');
    Route::post('/add-row', 'RequestMarker\ActualMarkerController@addRow')->name('actualMarker.addRow');
    Route::post('/delete-row', 'RequestMarker\ActualMarkerController@deleteRow')->name('actualMarker.deleteRow');
    Route::post('/update-layer', 'RequestMarker\ActualMarkerController@updateLayer')->name('actualMarker.updateLayer');
    Route::post('/update-fabric-width', 'RequestMarker\ActualMarkerController@updateFabricWidth')->name('actualMarker.updateFabricWidth');
    Route::post('/update-perimeter', 'RequestMarker\ActualMarkerController@updatePerimeter')->name('actualMarker.updatePerimeter');
    Route::post('/update-marker-length', 'RequestMarker\ActualMarkerController@updateMarkerLength')->name('actualMarker.updateMarkerLength');
    Route::post('/update-rasio', 'RequestMarker\ActualMarkerController@updateRasio')->name('actualMarker.updateRasio');
    Route::post('/reset-data-marker', 'RequestMarker\ActualMarkerController@resetData')->name('actualMarker.resetData');
    Route::post('/remove-page-access', 'RequestMarker\ActualMarkerController@removePageAccess')->name('actualMarker.removePageAccess');
    Route::get('/download-report/{id}', 'RequestMarker\ActualMarkerController@downloadReport')->name('actualMarker.downloadReport');
    Route::get('/download-ratio/{id}', 'RequestMarker\ActualMarkerController@downloadRatio')->name('actualMarker.downloadRatio');
    Route::post('/release-marker', 'RequestMarker\ActualMarkerController@ReleaseMarker')->name('actualMarker.ReleaseMarker');
    Route::post('/add-note', 'RequestMarker\ActualMarkerController@addNote')->name('actualMarker.addNote');
    // Route::get('/download-report-cutdate/{cut_date}/{factory_id}', 'RequestMarker\ActualMarkerController@downloadReportBydate')->name('actualMarker.downloadReportBydate');
    // Route::get('/download-ratio-cutdate/{cut_date}/{factory_id}', 'RequestMarker\ActualMarkerController@downloadRatioBydate')->name('actualMarker.downloadRatioBydate');
});

// UPLOAD MARKER

Route::prefix('/upload-marker')->middleware(['permission:menu-upload-marker'])->group(function(){
    Route::get('', 'RequestMarker\UploadMarkerController@index')->name('uploadMarker.index');
    Route::get('data-planning', 'RequestMarker\UploadMarkerController@dataPlanning')->name('uploadMarker.dataPlanning');
    Route::get('data-marker', 'RequestMarker\UploadMarkerController@dataMarker')->name('uploadMarker.dataMarker');
    Route::get('/upload-modal/{id}', 'RequestMarker\UploadMarkerController@uploadModal')->name('uploadMarker.uploadModal');
    Route::post('/upload', 'RequestMarker\UploadMarkerController@upload')->name('uploadMarker.upload');
    Route::get('/download/{id}', 'RequestMarker\UploadMarkerController@download')->name('uploadMarker.download');
    Route::get('/download-file/{id}', 'RequestMarker\UploadMarkerController@downloadFile')->name('uploadMarker.downloadFile');
    Route::delete('delete/{id}', 'RequestMarker\UploadMarkerController@delete')->name('uploadMarker.delete');
});

// DOWNLOAD MARKER

Route::prefix('/download-marker')->middleware(['permission:menu-download-marker'])->group(function(){
    Route::get('', 'RequestMarker\DownloadMarkerController@index')->name('downloadMarker.index');
    Route::get('data-planning', 'RequestMarker\DownloadMarkerController@dataPlanning')->name('downloadMarker.dataPlanning');
    Route::get('data-marker', 'RequestMarker\DownloadMarkerController@dataMarker')->name('downloadMarker.dataMarker');
    Route::get('/detail-modal/{id}', 'RequestMarker\DownloadMarkerController@uploadModal')->name('downloadMarker.uploadModal');
    Route::get('/download/{id}', 'RequestMarker\DownloadMarkerController@download')->name('downloadMarker.download');
    Route::get('/download-file/{id}', 'RequestMarker\DownloadMarkerController@downloadFile')->name('downloadMarker.downloadFile');
    Route::get('/detail-marker/{id}', 'RequestMarker\DownloadMarkerController@getDetailRatio')->name('downloadMarker.getDetailRatio');
    Route::get('/detail-marker-ratio/data-ratio', 'RequestMarker\DownloadMarkerController@getDetailRatioData')->name('downloadMarker.getDetailRatioData');
    Route::post('terima-file/{id}', 'RequestMarker\DownloadMarkerController@terimaFile')->name('downloadMarker.terimaFile');
});

// PRINT ID MARKER

Route::prefix('/print-id-marker')->middleware(['permission:menu-print-id-marker'])->group(function(){
    Route::get('', 'RequestMarker\PrintIDMarkerController@index')->name('printIDMarker.index');
    Route::get('data-planning', 'RequestMarker\PrintIDMarkerController@dataPlanning')->name('printIDMarker.dataPlanning');
    Route::get('data-marker', 'RequestMarker\PrintIDMarkerController@dataMarker')->name('printIDMarker.dataMarker');
    Route::get('data-detail-marker', 'RequestMarker\PrintIDMarkerController@dataDetailMarker')->name('printIDMarker.dataDetailMarker');
    Route::get('/detail-modal/{id}', 'RequestMarker\PrintIDMarkerController@detailModal')->name('printIDMarker.detailModal');
    Route::post('/post-data-marker', 'RequestMarker\PrintIDMarkerController@updateDataMarker')->name('printIDMarker.updateDataMarker');
    Route::get('/detail-marker/{id}/detail/{part_no}', 'RequestMarker\PrintIDMarkerController@detailDetailModal')->name('printIDMarker.detailDetailModal');
    Route::post('/approve-data-marker', 'RequestMarker\PrintIDMarkerController@approveDataMarker')->name('printIDMarker.approveDataMarker');
    Route::post('print-id', 'RequestMarker\PrintIDMarkerController@printId')->name('printIDMarker.printId');
    Route::post('print-id-pdf', 'RequestMarker\PrintIDMarkerController@printIdPdf')->name('printIDMarker.printIdPdf');
    Route::post('/realokasi', 'RequestMarker\PrintIDMarkerController@realokasi')->name('printIDMarker.realokasi');
});

// REPORT FABRIC

Route::prefix('/report-fabric')->middleware(['permission:menu-report-fabric'])->group(function(){
    Route::get('', 'RequestFabric\ReportFabricController@index')->name('reportFabric.index');
    Route::get('compare', 'RequestFabric\ReportFabricController@indexOutstanding')->name('reportFabric.indexOutstanding');
    Route::get('data-planning', 'RequestFabric\ReportFabricController@dataPlanning')->name('reportFabric.dataPlanning');
    Route::get('data-planning-outstanding', 'RequestFabric\ReportFabricController@dataPlanningOutstanding')->name('reportFabric.dataPlanningOutstanding');
    Route::get('/detail-modal/{id}', 'RequestFabric\ReportFabricController@detailModal')->name('reportFabric.detailModal');
    Route::get('data-detail', 'RequestFabric\ReportFabricController@dataDetail')->name('reportFabric.dataDetail');
    Route::get('data-prepare', 'RequestFabric\ReportFabricController@dataPrepare')->name('reportFabric.dataPrepare');
    Route::post('/download-report', 'RequestFabric\ReportFabricController@downloadReport')->name('reportFabric.downloadReport');
    Route::post('/download-report-outstanding', 'RequestFabric\ReportFabricController@downloadReportOutstanding')->name('reportFabric.downloadReportOutstanding');
});


Route::prefix('/report-ci')->middleware(['permission:menu-permintaan-fabric-submit'])->group(function(){
    Route::get('', 'RequestFabric\ReportFabricController@reportCuttingInstruction')->name('reportCI.index');
    Route::get('data-ci', 'RequestFabric\ReportFabricController@dataCuttingInstruction')->name('reportCI.dataCuttingInstruction');
    Route::get('detail/{id}', 'RequestFabric\ReportFabricController@detailCuttingInstruction')->name('reportCI.detailCuttingInstruction');
    Route::get('detail/{id}/data', 'RequestFabric\ReportFabricController@dataDetailCuttingInstruction')->name('reportCI.dataDetailCuttingInstruction');
    // Route::get('export-to-excel', 'RequestFabric\ReportFabricController@exportCuttingInstruction')->name('reportCI.exportCuttingInstruction');
    Route::get('export/{id}', 'RequestFabric\ReportFabricController@exportDetailCuttingInstruction')->name('reportCI.exportDetailCuttingInstruction');

    // Route::get('/detail-modal/{id}', 'RequestFabric\ReportFabricController@detailModal')->name('reportFabric.detailModal');
    // Route::get('data-detail', 'RequestFabric\ReportFabricController@dataDetail')->name('reportFabric.dataDetail');
    // Route::post('/download-report', 'RequestFabric\ReportFabricController@downloadReport')->name('reportFabric.downloadReport');
});

Route::prefix('/report-fabric-prepare')->middleware(['permission:menu-permintaan-fabric-submit'])->group(function(){
    Route::get('', 'RequestFabric\ReportFabricController@reportFabricPrepare')->name('reportFabricPrepare.index');
    Route::get('data-reguler', 'RequestFabric\ReportFabricController@dataReguler')->name('reportFabricPrepare.dataReguler');
    //Route::get('detail/{id}', 'RequestFabric\ReportFabricController@detail')->name('reportFabricPrepare.detail');
    //Route::get('detail/{id}/data', 'RequestFabric\ReportFabricController@dataDetailPrepare')->name('reportFabricPrepare.dataDetailPrepare');
    Route::get('detail/{id}/{wms_version}', 'RequestFabric\ReportFabricController@detail')->name('reportFabricPrepare.detail');
    Route::get('dataDetail/{id}/{wms_version}/data', 'RequestFabric\ReportFabricController@dataDetailPrepare')->name('reportFabricPrepare.dataDetailPrepare');
});

// REPORT ROLL

Route::prefix('/report-roll')->middleware(['permission:menu-report-roll'])->group(function(){
    Route::get('', 'RequestFabric\ReportRollController@index')->name('reportRoll.index');
    Route::get('data-planning', 'RequestFabric\ReportRollController@dataPlanning')->name('reportRoll.dataPlanning');
    Route::get('data-detail', 'RequestFabric\ReportRollController@dataDetail')->name('reportRoll.dataDetail');
    Route::get('/part-detail/{id}/detail/{part_no}', 'RequestFabric\ReportRollController@detailModal')->name('reportRoll.detailModal');
    Route::get('data-part', 'RequestFabric\ReportRollController@dataPart')->name('reportRoll.dataPart');
    Route::get('/part-modal/{id}', 'RequestFabric\ReportRollController@partModal')->name('reportRoll.partModal');
    Route::post('/accept-roll', 'RequestFabric\ReportRollController@acceptRoll')->name('reportRoll.acceptRoll');
    Route::post('/accept-rolls', 'RequestFabric\ReportRollController@acceptRolls')->name('reportRoll.acceptRolls');
    Route::get('/reduce-qty/{id}/{barcode}', 'RequestFabric\ReportRollController@reduceModal')->name('reportRoll.reduceModal');
    Route::get('/cancel-received/{barcode}', 'RequestFabric\ReportRollController@cancelReceived')->name('reportRoll.cancelReceived');
    Route::post('/reduce-pengurangan', 'RequestFabric\ReportRollController@reducePengurangan')->name('reportRoll.reducePengurangan');
    Route::post('/download-excel', 'RequestFabric\ReportRollController@downloadExcel')->name('reportRoll.downloadExcel');
});

// DASHBOARD

Route::prefix('/dashboard')->group(function(){
    Route::get('', 'DashboardController@index')->name('dashboard.index');
    Route::get('data-planning', 'DashboardController@dataPlanning')->name('dashboard.dataPlanning');
    Route::post('download-report', 'DashboardController@downloadReport')->name('dashboard.downloadReport');
});

//master spreading,table,setting table
Route::prefix('/master-param')->middleware(['permission:menu-master-parameter'])->group(function(){
     Route::get('', 'MasterParameter\MasterController@index')->name('masterparam');
    Route::get('get-data-spreading', 'MasterParameter\MasterController@getMasterSpreading')->name('masterparam.getMasterSpreading');
    Route::post('in-spreading', 'MasterParameter\MasterController@inSpreading')->name('masterparam.inSpreading');
    Route::post('inactive-spreading', 'MasterParameter\MasterController@inactiveSpreading')->name('masterparam.inactiveSpreading');
    Route::get('master-set-table', 'MasterParameter\MasterController@masterSetTable')->name('masterparam.masterSetTable');
    Route::post('insert-table-spreading','MasterParameter\MasterController@insertTableSpreading')->name('masterparam.insertTableSpreading');
    Route::get('get-table-spreading', 'MasterParameter\MasterController@getTableSpreading')->name('masterparam.getTableSpreading');
    Route::post('delete-table-spreading', 'MasterParameter\MasterController@delTableSpreading')->name('masterparam.delTableSpreading');
    Route::post('update-table-spreading', 'MasterParameter\MasterController@editWorkHour')->name('masterparam.editWorkHour');
    Route::get('master-table','MasterParameter\MasterController@masterTable')->name('masterparam.masterTable');
    Route::get('get-data-table','MasterParameter\MasterController@getTable')->name('masterparam.getTable');
    Route::post('add-table','MasterParameter\MasterController@newTable')->name('masterparam.newTable');
    Route::post('delete-table','MasterParameter\MasterController@delTab')->name('masterparam.delTab');
    Route::get('print-table','MasterParameter\MasterController@printTable')->name('masterparam.printTable');

    // master locator
    Route::get('/master-locator', 'MasterParameter\LocatorController@masterLocator')->name('masterparam.masterLocator');
    Route::get('/master-locator/get-data', 'MasterParameter\LocatorController@ajaxGetDataMasterLocator')->name('masterparam.ajaxGetDataMasterLocator');
    Route::post('/master-locator/add', 'MasterParameter\LocatorController@addLocator')->name('masterparam.addLocator');
    Route::get('/master-locator/edit/{id}', 'MasterParameter\LocatorController@editLocator')->name('masterparam.editLocator');
    Route::post('/master-locator/update', 'MasterParameter\LocatorController@updateLocator')->name('masterparam.updateLocator');
    Route::get('/master-locator/delete/{id}', 'MasterParameter\LocatorController@deleteLocator')->name('masterparam.deleteLocator');

    // master process
    Route::get('/master-process', 'MasterParameter\ProcessController@masterProcess')->name('masterparam.masterProcess');
    Route::get('/master-process/get-data', 'MasterParameter\ProcessController@ajaxGetDataMasterProcess')->name('masterparam.ajaxGetDataMasterProcess');
    Route::post('/master-process/add', 'MasterParameter\ProcessController@addProcess')->name('masterparam.addProcess');
    Route::get('/master-process/edit/{id}', 'MasterParameter\ProcessController@editProcess')->name('masterparam.editProcess');
    Route::post('/master-process/update', 'MasterParameter\ProcessController@updateProcess')->name('masterparam.updateProcess');
    Route::get('/master-process/delete/{id}', 'MasterParameter\ProcessController@deleteProcess')->name('masterparam.deleteProcess');

    // master komponen
    Route::get('/master-komponen', 'MasterParameter\KomponenController@masterKomponen')->name('masterparam.masterKomponen');
    Route::get('/master-komponen/get-data', 'MasterParameter\KomponenController@ajaxGetDataMasterKomponen')->name('masterparam.ajaxGetDataMasterKomponen');
    Route::post('/master-komponen/add', 'MasterParameter\KomponenController@addKomponen')->name('masterparam.addKomponen');
    Route::get('/master-komponen/edit/{id}', 'MasterParameter\KomponenController@editKomponen')->name('masterparam.editKomponen');
    Route::post('/master-komponen/update', 'MasterParameter\KomponenController@updateKomponen')->name('masterparam.updateKomponen');
    Route::get('/master-komponen/delete/{id}', 'MasterParameter\KomponenController@deleteKomponen')->name('masterparam.deleteKomponen');

    // master style
    Route::get('/master-style', 'MasterParameter\StyleController@masterStyle')->name('masterparam.masterStyle');
    Route::get('/master-style/get-data', 'MasterParameter\StyleController@ajaxGetDataMasterStyle')->name('masterparam.ajaxGetDataMasterStyle');
    Route::post('/master-style/add', 'MasterParameter\StyleController@addStyle')->name('masterparam.addStyle');
    Route::get('/master-style/edit-style/{style}/{komponen}', 'MasterParameter\StyleController@editStyle')->name('masterparam.editStyle');
    Route::post('/master-style/update', 'MasterParameter\StyleController@updateStyle')->name('masterparam.updateStyle');
    Route::get('/master-style/delete-style/{style}/{komponen}/{type_id}/{season}', 'MasterParameter\StyleController@deleteStyle')->name('masterparam.deleteStyle');

    // master style production
    Route::get('/master-style-production', 'MasterParameter\StyleController@masterStyleProduction')->name('masterparam.masterStyleProduction');
    Route::get('/master-style-production/get-data', 'MasterParameter\StyleController@ajaxGetDataMasterStyleProduction')->name('masterparam.ajaxGetDataMasterStyleProduction');
    Route::post('/master-style-production/add', 'MasterParameter\StyleController@addStyleProduction')->name('masterparam.addStyleProduction');
    Route::get('/master-style-production/edit-style/{style}/{komponen}', 'MasterParameter\StyleController@editStyleProduction')->name('masterparam.editStyleProduction');
    Route::post('/master-style-production/update', 'MasterParameter\StyleController@updateStyleProduction')->name('masterparam.updateStyleProduction');
    Route::get('/master-style-production/delete-style/{style}/{komponen}/{id}', 'MasterParameter\StyleController@deleteStyleProduction')->name('masterparam.deleteStyleProduction');

    Route::get('/data-komponen', 'MasterParameter\StyleController@ajaxGetDataKomponen')->name('masterparam.ajaxGetDataKomponen');
    Route::get('/master-style-production/get-list-data', 'MasterParameter\StyleController@ajaxGetDataGetListStyle')->name('masterparam.ajaxGetDataGetListStyle');
    // style sync
    Route::get('/style-production/sync', 'MasterParameter\StyleController@styleSync')->name('masterparam.styleSync');
    Route::post('/style-production/sync-sds', 'MasterParameter\StyleController@styleSyncSDS')->name('masterparam.styleSyncSDS');

    // master detail process
    Route::get('/master-detail-process', 'MasterParameter\DetailProcessController@masterDetailProcess')->name('masterparam.masterDetailProcess');
    Route::get('/master-detail-process/get-data', 'MasterParameter\DetailProcessController@ajaxGetDataMasterDetailProcess')->name('masterparam.ajaxGetDataMasterDetailProcess');
    Route::post('/master-detail-process/add', 'MasterParameter\DetailProcessController@addDetailProcess')->name('masterparam.addDetailProcess');
    Route::get('/master-detail-process/edit/{id}', 'MasterParameter\DetailProcessController@editDetailProcess')->name('masterparam.editDetailProcess');
    Route::post('/master-detail-process/update', 'MasterParameter\DetailProcessController@updateDetailProcess')->name('masterparam.updateDetailProcess');
    Route::get('/master-detail-process/delete/{id}', 'MasterParameter\DetailProcessController@deleteDetailProcess')->name('masterparam.deleteDetailProcess');

    //master machines type
    Route::get('/master-machine-type', 'MasterParameter\MachineTypeController@index')->name('master_machine_type.index');
    Route::get('/master-machine-type/get-data', 'MasterParameter\MachineTypeController@getDataMachinetype')->name('master_machine_type.getDataMachinetype');
    Route::post('/master-machine-type/add', 'MasterParameter\MachineTypeController@add')->name('master_machine_type.add');
    Route::get('/master-machine-type/delete/{id}', 'MasterParameter\MachineTypeController@delete')->name('master_machine_type.delete');

    Route::get('/master-machine-detail', 'MasterParameter\MachineDetailController@index')->name('machine_details.index');
    Route::get('/master-machine-detail/get-data', 'MasterParameter\MachineDetailController@ajaxGetDataMachinedetails')->name('machine_details.ajaxGetDataMachinedetails');
    Route::post('/master-machine-detail/add', 'MasterParameter\MachineDetailController@addMachineDetails')->name('machine_details.addMachineDetails');
    Route::get('/master-machine-detail/edit/{id}', 'MasterParameter\MachineDetailController@editMachineDetails')->name('machine_details.editMachineDetails');
    Route::post('/master-machine-detail/update', 'MasterParameter\MachineDetailController@updateMachineDetails')->name('machine_details.updateMachineDetails');
    Route::get('/master-machine-detail/delete/{id}', 'MasterParameter\MachineDetailController@deleteMachineDetails')->name('machine_details.deleteMachineDetails');
    // master komponen add process
    Route::get('/master-komponen-process', 'MasterParameter\KomponenProcessController@masterKomponenProcess')->name('masterparam.masterKomponenProcess');
    Route::get('/master-komponen-process/get-data', 'MasterParameter\KomponenProcessController@ajaxGetDataMasterKomponenProcess')->name('masterparam.ajaxGetDataMasterKomponenProcess');
    Route::post('/master-komponen-process/add', 'MasterParameter\KomponenProcessController@addKomponenProcess')->name('masterparam.addKomponenProcess');
    Route::post('/master-komponen-process-detail/add', 'MasterParameter\KomponenProcessController@addKomponenProcessDetail')->name('masterparam.addKomponenProcessDetail');
    Route::get('/master-komponen-process/delete/{id}', 'MasterParameter\KomponenProcessController@deleteKomponenProcess')->name('masterparam.deleteKomponenProcess');
    Route::get('/master-komponen-process/listing/{id}', 'MasterParameter\KomponenProcessController@listingKomponenProcess')->name('masterparam.listingKomponenProcess');

    Route::get('/get-komponen-process', 'MasterParameter\KomponenProcessController@ajaxGetDataKomponenProcess')->name('masterparam.ajaxGetDataKomponenProcess');
    Route::get('/get-komponen-process-json', 'MasterParameter\KomponenProcessController@ajaxGetDataKomponenProcessJson')->name('masterparam.ajaxGetDataKomponenProcessJson');

    Route::get('/get-komponen-process-detail', 'MasterParameter\KomponenProcessController@ajaxGetDataKomponenProcessDetail')->name('masterparam.ajaxGetDataKomponenProcessDetail');
    Route::get('/get-komponen-process-detail-json', 'MasterParameter\KomponenProcessController@ajaxGetDataKomponenProcessDetailJson')->name('masterparam.ajaxGetDataKomponenProcessDetailJson');
    Route::get('/master-komponen-process-detail/delete/{id}', 'MasterParameter\KomponenProcessController@deleteKomponenProcessDetail')->name('masterparam.deleteKomponenProcessDetail');

    Route::get('/get-data-style', 'MasterParameter\KomponenProcessController@ajaxGetDataStyle')->name('masterparam.ajaxGetDataStyle');

    // master bisnis unit
    Route::get('/master-bisnis-unit', 'MasterParameter\BisnisUnitController@index')->name('masterparam.masterBisnisUnit');
    Route::get('/master-bisnis-unit/get-data', 'MasterParameter\BisnisUnitController@ajaxGetDataMasterBisnisUnit')->name('masterparam.ajaxGetDataMasterBisnisUnit');
    Route::post('/master-bisnis-unit/add', 'MasterParameter\BisnisUnitController@addBisnisUnit')->name('masterparam.addBisnisUnit');
    Route::get('/master-bisnis-unit/edit/{id}', 'MasterParameter\BisnisUnitController@editBisnisUnit')->name('masterparam.editBisnisUnit');
    Route::post('/master-bisnis-unit/update', 'MasterParameter\BisnisUnitController@updateBisnisUnit')->name('masterparam.updateBisnisUnit');
    Route::get('/master-bisnis-unit/delete/{id}', 'MasterParameter\BisnisUnitController@deleteBisnisUnit')->name('masterparam.deleteBisnisUnit');

    // master list style
    Route::get('/master-list-style', 'MasterParameter\ListStyleController@index')->name('masterparam.masterListStyle');
    Route::get('/master-list-style/get-data', 'MasterParameter\ListStyleController@ajaxGetDataListStyle')->name('masterparam.ajaxGetDataListStyle');
    Route::post('/master-list-style/add', 'MasterParameter\ListStyleController@addListStyle')->name('masterparam.addListStyle');
    Route::get('/master-list-style/edit/{id}', 'MasterParameter\ListStyleController@editListStyle')->name('masterparam.editListStyle');
    Route::post('/master-list-style/update', 'MasterParameter\ListStyleController@updateListStyle')->name('masterparam.updateListStyle');
    Route::get('/master-list-style/delete/{id}', 'MasterParameter\ListStyleController@deleteListStyle')->name('masterparam.deleteListStyle');

    // master defect name
    Route::get('/master-defect-name', 'MasterParameter\MasterDefectNameController@index')->name('masterparam.masterDefectName');
    Route::get('/master-defect-name/get-data', 'MasterParameter\MasterDefectNameController@getDataDefectName')->name('masterparam.getDataDefectName');
    Route::post('/master-defect-name/add', 'MasterParameter\MasterDefectNameController@addDefectName')->name('masterparam.addDefectName');
    Route::get('/master-defect-name/edit/{id}', 'MasterParameter\MasterDefectNameController@editDefectName')->name('masterparam.editDefectName');
    Route::post('/master-defect-name/update', 'MasterParameter\MasterDefectNameController@updateDefectName')->name('masterparam.updateDefectName');
    Route::get('/master-defect-name/delete/{id}', 'MasterParameter\MasterDefectNameController@deleteDefectName')->name('masterparam.deleteDefectName');
    Route::get('/master-defect-name/downloadReport', 'MasterParameter\MasterDefectNameController@downloadMasterdefectname')->name('masterparam.downloadMasterdefectname');
    Route::get('/master-defect-name/session-done', 'MasterParameter\MasterDefectNameController@deleteEditSession')->name('masterparam.deleteEditSession');
});

// INPUT ACTUAL SPREADING

Route::prefix('/actual-spreading')->middleware(['permission:menu-actual-spreading-input'])->group(function(){
    Route::get('', 'Spreading\ActualSpreadingController@index')->name('actualSpreading.index');
    Route::get('data-cutting-table', 'Spreading\ActualSpreadingController@dataCuttingTable')->name('actualSpreading.dataCuttingTable');
    Route::post('data-spreading-report-temp', 'Spreading\ActualSpreadingController@dataSpreadingReportTemp')->name('actualSpreading.dataSpreadingReportTemp');
    Route::post('check-table-id', 'Spreading\ActualSpreadingController@checkTableId')->name('actualSpreading.checkTableId');
    Route::post('check-marker-id', 'Spreading\ActualSpreadingController@checkMarkerId')->name('actualSpreading.checkMarkerId');
    Route::post('check-marker-id-tumpuk', 'Spreading\ActualSpreadingController@checkMarkerIdTumpuk')->name('actualSpreading.checkMarkerIdTumpuk');
    Route::post('check-marker-id-tumpuk-submit', 'Spreading\ActualSpreadingController@checkMarkerIdTumpukSubmit')->name('actualSpreading.checkMarkerIdTumpukSubmit');
    Route::post('check-marker-id-scan2', 'Spreading\ActualSpreadingController@checkMarkerIdScan2')->name('actualSpreading.checkMarkerIdScan2');
    Route::post('check-fabric-id', 'Spreading\ActualSpreadingController@checkFabricId')->name('actualSpreading.checkFabricId');
    Route::post('update-actual-width', 'Spreading\ActualSpreadingController@updateActualWidth')->name('actualSpreading.updateActualWidth');
    Route::post('update-actual-layer', 'Spreading\ActualSpreadingController@updateActualLayer')->name('actualSpreading.updateActualLayer');
    Route::post('update-actual-sisa', 'Spreading\ActualSpreadingController@updateActualSisa')->name('actualSpreading.updateActualSisa');
    Route::post('update-reject', 'Spreading\ActualSpreadingController@updateReject')->name('actualSpreading.updateReject');
    Route::post('update-sambungan', 'Spreading\ActualSpreadingController@updateSambungan')->name('actualSpreading.updateSambungan');
    Route::post('update-sambungan-end', 'Spreading\ActualSpreadingController@updateSambunganEnd')->name('actualSpreading.updateSambunganEnd');
    Route::post('update-kualitas-gelaran', 'Spreading\ActualSpreadingController@updateKualitasGelaran')->name('actualSpreading.updateKualitasGelaran');
    Route::post('delete-report-spreading-temp', 'Spreading\ActualSpreadingController@deleteReportSpreadingTemp')->name('actualSpreading.deleteReportSpreadingTemp');
    Route::post('delete-last-fabric-scan', 'Spreading\ActualSpreadingController@deleteLastFabricScan')->name('actualSpreading.deleteLastFabricScan');
    Route::post('spreading-report-save', 'Spreading\ActualSpreadingController@spreadingReportSave')->name('actualSpreading.spreadingReportSave');
    Route::post('spreading-report-insert', 'Spreading\ActualSpreadingController@spreadingReportInsert')->name('actualSpreading.spreadingReportInsert');
    Route::post('update-downtime', 'Spreading\ActualSpreadingController@updateDowntime')->name('actualSpreading.updateDowntime');
    //Route::get('', 'Spreading\ActualSpreadingController@index')->name('actualSpreading.index');
});

// APPROVAL GL

Route::prefix('/approve-spreading-gl')->middleware(['permission:menu-approval-spreading-gl'])->group(function(){
    Route::get('', 'Spreading\ActualSpreadingController@approvalSpreading')->name('ApprovalSpreadingGL.approvalSpreading');
    Route::get('get-data', 'Spreading\ActualSpreadingController@getApprovalSpreading')->name('ApprovalSpreadingGL.getApprovalSpreading');
    Route::get('ajaxgetspreadingapv', 'Spreading\ActualSpreadingController@ajaxGetSpreadingApv')->name('ApprovalSpreadingGL.ajaxGetSpreadingApv');
    Route::get('ajaxfabricspreadingapv', 'Spreading\ActualSpreadingController@ajaxFabricSpreadingApv')->name('ApprovalSpreadingGL.ajaxFabricSpreadingApv');
    Route::post('ajaxApproveMarker', 'Spreading\ActualSpreadingController@ajaxApproveMarker')->name('ApprovalSpreadingGL.ajaxApproveMarker');
});

// APPROVAL QC
Route::prefix('/approve-spreading-qc')->middleware(['permission:menu-approval-spreading-qc'])->group(function(){
    Route::get('', 'Spreading\ActualSpreadingController@approvalSpreadingQc')->name('ApprovalSpreadingQC.approvalSpreadingQc');
    Route::get('get-data', 'Spreading\ActualSpreadingController@getApprovalSpreading')->name('ApprovalSpreadingQC.getApprovalSpreading');
    Route::get('ajaxFabricQcSpreading', 'Spreading\ActualSpreadingController@ajaxFabricQcSpreading')->name('ApprovalSpreadingQC.ajaxFabricQcSpreading');
    Route::post('ajaxApproveQc', 'Spreading\ActualSpreadingController@ajaxApproveQc')->name('ApprovalSpreadingQC.ajaxApproveQc');
    Route::post('ajaxSaveQcTemp', 'Spreading\ActualSpreadingController@ajaxSaveQcTemp')->name('ApprovalSpreadingQC.ajaxSaveQcTemp');
    Route::get('ajaxgetspreadingapv', 'Spreading\ActualSpreadingController@ajaxGetSpreadingApv')->name('ApprovalSpreadingQC.ajaxGetSpreadingApv');
    Route::get('ajaxfabricspreadingapv', 'Spreading\ActualSpreadingController@ajaxFabricSpreadingApv')->name('ApprovalSpreadingQC.ajaxFabricSpreadingApv');
    Route::post('ajaxApproveMarker', 'Spreading\ActualSpreadingController@ajaxApproveMarker')->name('ApprovalSpreadingQC.ajaxApproveMarker');
});

// Review Spreading Result

Route::prefix('/review-spreading-result')->middleware(['permission:menu-review-spreading-result'])->group(function(){
    Route::get('', 'Spreading\ReviewSpreadingResultController@index')->name('reviewSpreadingResult.index');
    Route::get('get-data', 'Spreading\ReviewSpreadingResultController@getData')->name('reviewSpreadingResult.getData');
    Route::get('get-data-detail/{id}', 'Spreading\ReviewSpreadingResultController@getDetailData')->name('reviewSpreadingResult.getDetailData');
    Route::get('get-data-revisi/{id}', 'Spreading\ReviewSpreadingResultController@getRevisiData')->name('reviewSpreadingResult.getRevisiData');
    Route::post('update-actual-layer', 'Spreading\ReviewSpreadingResultController@updateActualLayer')->name('reviewSpreadingResult.updateActualLayer');
    Route::post('update-actual-sisa', 'Spreading\ReviewSpreadingResultController@updateActualSisa')->name('reviewSpreadingResult.updateActualSisa');
    Route::post('update-reject', 'Spreading\ReviewSpreadingResultController@updateReject')->name('reviewSpreadingResult.updateReject');
    Route::post('update-sambungan', 'Spreading\ReviewSpreadingResultController@updateSambungan')->name('reviewSpreadingResult.updateSambungan');
    Route::post('update-sambungan-end', 'Spreading\ReviewSpreadingResultController@updateSambunganEnd')->name('reviewSpreadingResult.updateSambunganEnd');
    Route::post('update-kualitas-gelaran', 'Spreading\ReviewSpreadingResultController@updateKualitasGelaran')->name('reviewSpreadingResult.updateKualitasGelaran');
    Route::post('gen-download-link', 'Spreading\ReviewSpreadingResultController@downloadGenerate')->name('reviewSpreadingResult.downloadGenerate');
    Route::get('download/{cutting_date}', 'Spreading\ReviewSpreadingResultController@download')->name('reviewSpreadingResult.download');
});

// Report

Route::prefix('/report')->middleware(['permission:menu-report'])->group(function(){

    // Report Rekonsiliasi
    Route::get('spreading/rekonsiliasi-report', 'Report\SpreadingReportController@indexRekonsiliasi')->name('reportSpreading.indexRekonsiliasi');
    Route::post('spreading/download-rekonsiliasi-report', 'Report\SpreadingReportController@downloadRekonsiliasi')->name('reportSpreading.downloadRekonsiliasi');
    Route::post('spreading/download-rekonsiliasi-report-po', 'Report\SpreadingReportController@downloadRekonsiliasiPO')->name('reportSpreading.downloadRekonsiliasiPO');

    // Report Fabric Per Roll
    Route::get('spreading/fabric-roll-report', 'Report\SpreadingReportController@indexFabricRoll')->name('reportSpreading.indexFabricRoll');
    Route::post('spreading/download-fabric-roll-report', 'Report\SpreadingReportController@downloadFabricRoll')->name('reportSpreading.downloadFabricRoll');
    Route::post('spreading/download-fabric-roll-report-po-buyer', 'Report\SpreadingReportController@downloadFabricRollPOBuyer')->name('reportSpreading.downloadFabricRollPOBuyer');
    Route::post('spreading/download-fabric-roll-report-po-supplier', 'Report\SpreadingReportController@downloadFabricRollPOSupplier')->name('reportSpreading.downloadFabricRollPOSupplier');
    Route::post('spreading/download-fabric-roll-report-item-code', 'Report\SpreadingReportController@downloadFabricRollItemCode')->name('reportSpreading.downloadFabricRollItemCode');
    Route::get('spreading/get-fabric-roll-report', 'Report\SpreadingReportController@getDownloadPerRoll')->name('reportSpreading.getDownloadPerRoll');
    Route::get('spreading/download-fabric-roll-report-plan/{id}', 'Report\SpreadingReportController@downloadFabricRollPlan')->name('reportSpreading.downloadFabricRollPlan');

    // Report Data Potong
    Route::get('spreading/data-potong-report', 'Report\SpreadingReportController@indexDataPotong')->name('reportSpreading.indexDataPotong');
    Route::post('spreading/download-data-potong-report', 'Report\SpreadingReportController@downloadDataPotong')->name('reportSpreading.downloadDataPotong');
    Route::post('spreading/download-data-potong-report-po', 'Report\SpreadingReportController@downloadDataPotongPO')->name('reportSpreading.downloadDataPotongPO');
    Route::get('spreading/get-data-potong-report', 'Report\SpreadingReportController@getDownloadDataPotong')->name('reportSpreading.getDownloadDataPotong');
    Route::get('spreading/download-data-potong-report-plan/{id}', 'Report\SpreadingReportController@downloadDataPotongPlan')->name('reportSpreading.downloadDataPotongPlan');

    // Report Data Potong
    Route::get('marker/report-serah-terima', 'Report\SpreadingReportController@indexSerahTerima')->name('reportSpreading.indexSerahTerima');
    Route::post('marker/download-report-serah-terima', 'Report\SpreadingReportController@downloadSerahTerima')->name('reportSpreading.downloadSerahTerima');
    Route::get('marker/get-data-marker-report', 'Report\SpreadingReportController@getSerahTerima')->name('reportSpreading.getSerahTerima');
    Route::get('marker/download-report-serah-terima/{id}', 'Report\SpreadingReportController@downloadSerahTerimaPlan')->name('reportSpreading.downloadSerahTerimaPlan');


    // report spreading qc
    Route::get('spreading/qc-spreading', 'Report\SpreadingReportController@reportQcSpreading')->name('reportSpreading.reportQcSpreading');
    Route::get('spreading/download-weekly', 'Report\SpreadingReportController@downloadWeekly')->name('reportSpreading.downloadWeekly');
    Route::post('spreading/report-qc-spreading', 'Report\SpreadingReportController@ajaxGetReportQcSpreading')->name('reportSpreading.ajaxGetReportQcSpreading');

    // report qc check
    Route::get('qc/report-qc','Report\QccheckController@index')->name('reportQc.reportQc');
    Route::get('qc/export-qc','Report\QccheckController@exportDataQc')->name('reportQc.exportQc');
    Route::get('qc/export-qc-cutting','Report\QccheckController@exportDataQcCutting')->name('reportQc.exportQcCutting');
    // Route::get('qc/download-report','Report\QccheckController@reportQcCuttingnew')->name('reportQc.reportQcCuttingnew');

    // report RnD
    Route::get('rnd','Report\RndController@index')->name('reportRnd.index');
    Route::post('rnd','Report\RndController@export')->name('reportRnd.export');

    // report KITT PPA2
    Route::get('kitt-ppa2','Report\KittController@ppa2')->name('reportKitt.ppa2');
    Route::post('kitt-ppa2','Report\KittController@exportPpa2')->name('reportKitt.exportPpa2');

    // Report Downtime Cutting
    Route::get('cutting/downtime-cutting-report', 'Report\CuttingReportController@indexDowntime')->name('reportCutting.indexDowntime');
    Route::post('cutting/download-downtime-cutting-report', 'Report\CuttingReportController@downloadDowntime')->name('reportCutting.downloadDowntime');
    Route::get('cutting/get-data', 'Report\CuttingReportController@getDataBundleOutput')->name('reportCutting.getDataBundleOutput');
    Route::post('cutting/report-bundle-output', 'Report\CuttingReportController@downloadBundleOutput')->name('reportCutting.downloadBundleOutput');

    // Report Perimeter Cutting
    Route::get('cutting/peimeter-cutting-report', 'Report\CuttingReportController@indexPerimeter')->name('reportCutting.indexPerimeter');
    Route::post('cutting/download-peimeter-cutting-report', 'Report\CuttingReportController@downloadPerimeter')->name('reportCutting.downloadPerimeter');

    // Report Scan Cutting
    Route::get('cutting/scan-cutting-report', 'Report\CuttingReportController@indexCutting')->name('reportCutting.indexCutting');
    Route::post('cutting/download-scan-cutting-report', 'Report\CuttingReportController@downloadCutting')->name('reportCutting.downloadCutting');

    // Report Hasil Bundling
    // Route::get('cutting/laporan-hasil-bundle', 'Report\CuttingReportController@indexHasilBundle')->name('reportCutting.indexHasilBundle');
    // Route::post('cutting/download-hasil-bundle', 'Report\CuttingReportController@downloadHasilBundle')->name('reportCutting.downloadHasilBundle');

    // Report Downtime Spreading
    Route::get('spreading/downtime-spreading-report', 'Report\SpreadingReportController@indexDowntime')->name('reportSpreading.indexDowntime');
    Route::post('spreading/download-downtime-spreading-report', 'Report\SpreadingReportController@downloadDowntime')->name('reportSpreading.downloadDowntime');

    // Report Saving Fabric (SSP)
    Route::get('saving_fabric_ssp/saving-fabric-ssp', 'Report\SavingfabricController@index')->name('savingfabricssp.index');
    Route::get('saving_fabric_ssp/export-data-saving','Report\SavingfabricController@exportDataSaving')->name('savingfabricssp.exportDataSaving');

    // Report Output Spreading
    Route::get('spreading/output-spreading-report', 'Report\SpreadingReportController@indexOutput')->name('reportSpreading.indexOutput');
    Route::post('spreading/download-output-spreading-report', 'Report\SpreadingReportController@downloadOutput')->name('reportSpreading.downloadOutput');
    Route::post('spreading/download-spreading-day-table', 'Report\SpreadingReportController@downloadSpreadingdaytable')->name('reportSpreading.downloadSpreadingdaytable');
    Route::post('spreading/download-spreading-detail-table', 'Report\SpreadingReportController@downloadSpreadingdetailtable')->name('reportSpreading.downloadSpreadingdetailtable');

    // Report Bundle Movement
    Route::get('component-movement/bundle-movement', 'Report\DistribusiMovementReportController@indexBundleMovement')->name('reportComponentMovement.indexBundleMovement');
    Route::post('component-movement/download-bundle-movement', 'Report\DistribusiMovementReportController@downloadBundleMovement')->name('reportComponentMovement.downloadBundleMovement');
    // Route::post('component-movement/download-bundle-detail-movement', 'Report\DistribusiMovementReportController@downloadBundledetailMovement')->name('reportComponentMovement.downloadBundledetailMovement');
    Route::get('component-movement/getDataDetailmovement', 'Report\DistribusiMovementReportController@getDataDetailmovement')->name('reportComponentMovement.getDataDetailmovement');
    Route::get('component-movement/export-excel', 'Report\DistribusiMovementReportController@exportExcel')->name('reportComponentMovement.exportExcel');
    Route::get('getStyle', 'Report\DistribusiMovementReportController@getStyle')->name('reportComponentMovement.getStyle');
    Route::get('getCutnum', 'Report\DistribusiMovementReportController@getCutnum')->name('reportComponentMovement.getCutnum');
    Route::post('component-movement/download-supply-movement', 'Report\DistribusiMovementReportController@downloadSupplymovement')->name('reportComponentMovement.downloadSupplymovement');
    Route::post('component-movement/download-supply-set', 'Report\DistribusiMovementReportController@downloadSupplyset')->name('reportComponentMovement.downloadSupplyset');

    // Report Bundle output
    Route::get('component-movement/bundle-output', 'Report\DistribusiMovementReportController@indexBundleOutput')->name('reportComponentMovement.indexBundleOutput');
    Route::post('component-movement/download-bundle-output', 'Report\DistribusiMovementReportController@downloadBundleOutput')->name('reportComponentMovement.downloadBundleOutput');
    Route::post('component-movement/download-bundle-output-daily', 'Report\DistribusiMovementReportController@downloadBundleOutputDaily')->name('reportComponentMovement.downloadBundleOutputDaily');

    //Report PDS
    Route::get('/report-pds', 'Report\PdsController@index')->name('pds_report.index');
    Route::get('/download-pds/{months}/{years}/{factory_id}', 'Report\PdsController@downloadPds')->name('pds_report.downloadPds');
    //Report PO SUMMARY
    Route::get('/po-summary', 'Report\PoSummaryController@index')->name('po_summary.index');
    Route::get('/getSummaryPO', 'Report\PoSummaryController@getDataSummaryPo')->name('po_summary.getDataSummaryPo');
    //Report PO MONITORING
    Route::get('/po-monitoring', 'Report\PoMonitoringController@index')->name('po_monitoring.index');
    Route::get('/getDataMonitoringpo', 'Report\PoMonitoringController@getDataMonitoring')->name('po_monitoring.getDataMonitoring');
});

//REPORT ARTWORK MOVEMENT
Route::prefix('/report-artwork')->middleware(['permission:report-artwork-movement'])->group(function(){
    Route::get('/artwork-move', 'DistribusiArtwork\DistribusiArtworkreportController@indexArtworkmovement')->name('distribusi_artwork.indexArtworkmovement');
    Route::get('/data-artworkmove', 'DistribusiArtwork\DistribusiArtworkreportController@getDataArtworkmovement')->name('distribusi_artwork.getDataArtworkmovement');
    Route::get('/data-artworkmovedetail', 'DistribusiArtwork\DistribusiArtworkreportController@getDataMovemendetail')->name('distribusi_artwork.getDataMovemendetail');
    Route::post('/download-movement', 'DistribusiArtwork\DistribusiArtworkreportController@downloadArtworkmovement')->name('distribusi_artwork.downloadArtworkmovement');
});

//SWITCH SUPPLY LINE
Route::prefix('/switch-locator')->middleware(['permission:menu-switch-locator-supply'])->group(function(){
    Route::get('/switch-locator', 'Operator\SwitchLocatorController@index')->name('switch_locator.index');
    Route::get('/switch-locator/getDataSupply', 'Operator\SwitchLocatorController@getDataSupply')->name('switch_locator.getDataSupply');
    Route::get('getStyle', 'Operator\SwitchLocatorController@getStyle')->name('switch_locator.getStyle');
    Route::get('getCutnum', 'Operator\SwitchLocatorController@getCutnum')->name('switch_locator.getCutnum');
    Route::get('/edit/{id}', 'Operator\SwitchLocatorController@edit')->name('switch_locator.edit');
    Route::post('/update', 'Operator\SwitchLocatorController@update')->name('switch_locator.update');
});
//END OF SWITCH LINE
Route::prefix('/cutting')->middleware(['permission:menu-cutting'])->group(function(){
    Route::get('', 'Cutting\CuttingController@index')->name('cutting.index');
    Route::get('/get-daily-cutting', 'Cutting\CuttingController@ajaxDailyCutting')->name('cutting.ajaxDailyCutting');
    Route::post('/set-status-cutting', 'Cutting\CuttingController@ajaxSetStatus')->name('cutting.ajaxSetStatus');
});

// Review Spreading Result

Route::prefix('/laporan-baru')->middleware(['permission:menu-print-id-marker'])->group(function(){
    Route::get('', 'Spreading\LaporanBaruController@index')->name('laporanBaru.index');
    Route::get('data-marker', 'Spreading\LaporanBaruController@dataMarker')->name('laporanBaru.dataMarker');
    Route::post('print-id', 'Spreading\LaporanBaruController@printId')->name('laporanBaru.printId');
    Route::post('print-id-pdf', 'Spreading\LaporanBaruController@printIdPdf')->name('laporanBaru.printIdPdf');
    Route::get('detail-laporan-baru/{id}', 'Spreading\LaporanBaruController@detailLaporanBaru')->name('laporanBaru.detailLaporanBaru');
    Route::post('change-ratio', 'Spreading\LaporanBaruController@changeRatio')->name('laporanBaru.changeRatio');
});

// SPREADING -> CUTTING INSTRUCTION

Route::prefix('/cutting-instruction-spreading')->middleware(['permission:menu-cutting-instruction-spreading'])->group(function(){
    Route::get('', 'Spreading\CuttingInstructionController@index')->name('SpreadingCI.index');
    Route::get('data-planning', 'Spreading\CuttingInstructionController@dataPlanning')->name('SpreadingCI.dataPlanning');
    Route::get('data-detail', 'Spreading\CuttingInstructionController@dataDetail')->name('SpreadingCI.dataDetail');
    Route::get('/detail-modal/{id}', 'Spreading\CuttingInstructionController@detailModal')->name('SpreadingCI.detailModal');
});

// END - SPREADING -> CUTTING INSTRUCTION

// SPREADING -> QTY FABRIC CHECK

Route::prefix('/qty-fabric-check')->middleware(['permission:menu-qty-fabric-check'])->group(function(){
    Route::get('', 'Spreading\QtyFabricCheckController@index')->name('QtyFabricCheck.index');
    Route::post('/check-fabric', 'Spreading\QtyFabricCheckController@checkFabric')->name('QtyFabricCheck.checkFabric');
});

// END - SPREADING -> QTY FABRIC CHECK

Route::prefix('/factory')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'MasterFactoryController@index')->name('factory.index');
    Route::post('/store', 'MasterFactoryController@store')->name('factory.store');
});


//dashboard start
Route::get('/adibooster/preparation/{factory}', 'Adibooster\DashboardController@preparation')->name('adibooster.preparation');
Route::get('/adibooster/preparation-today', 'Adibooster\DashboardController@today')->name('adibooster.pretoday');
Route::get('/adibooster/preparation-tomorrow', 'Adibooster\DashboardController@tomorrow')->name('adibooster.pretomorrow');
Route::get('/adibooster/spreading/{factory}', 'Adibooster\DashboardController@spreading')->name('adibooster.spreading');
Route::get('/adibooster/bundling/{factory}', 'Adibooster\DashboardController@bundling')->name('adibooster.bundling');
Route::get('/adibooster/get-data-cutting', 'Adibooster\DashboardController@getDataCutting')->name('adibooster.getDataCutting');
Route::get('/adibooster/setting-area/{factory}', 'Adibooster\DashboardController@settingArea')->name('adibooster.settingArea');
// Route::get('/adibooster/supply/{factory}', 'Adibooster\DashboardController@supplyMove')->name('adibooster.supplyMove');
Route::get('/adibooster/auto/{factory}', 'Adibooster\DashboardController@auto')->name('adibooster.auto');
Route::get('/adibooster/print/{factory}', 'Adibooster\DashboardController@print')->name('adibooster.print');
Route::get('/adibooster/priority/{factory}', 'Adibooster\DashboardController@priority')->name('adibooster.priority');
Route::get('/adibooster/get-data-priority/{factory_id}', 'Adibooster\DashboardController@getDataPriority')->name('adibooster.getDataPriority');

//dashboard end

// Fabric Report Outstanding

Route::prefix('/report-fabric-outstanding')->middleware(['permission:menu-upload-permintaan'])->group(function(){
    Route::get('', 'RequestMarker\ReportFabricController@index')->name('reportFabricOS.index');
    Route::get('/get-data', 'RequestMarker\ReportFabricController@getData')->name('reportFabricOS.getData');
});

// End Fabric Report Outstanding

// permintaan fabric -> kebutuhan fabrik

Route::prefix('/fabric-need-info')->middleware(['permission:menu-info-kebutuhan-fabric'])->group(function(){
    Route::get('', 'RequestFabric\FabricNeedController@index')->name('fabricNeed.index');
    Route::get('data-planning', 'RequestFabric\FabricNeedController@dataPlanning')->name('fabricNeed.dataPlanning');
    Route::get('data-marker', 'RequestFabric\FabricNeedController@dataMarker')->name('fabricNeed.dataMarker');
    Route::get('data-detail-marker', 'RequestFabric\FabricNeedController@dataDetailMarker')->name('fabricNeed.dataDetailMarker');
    Route::get('data-lebar-fabrik', 'RequestFabric\FabricNeedController@dataLebarFB')->name('fabricNeed.dataLebarFB');
    Route::get('data-size-balance', 'RequestFabric\FabricNeedController@dataSizeBalance')->name('fabricNeed.dataSizeBalance');
    Route::get('data-over-consum', 'RequestFabric\FabricNeedController@dataOverConsum')->name('fabricNeed.dataOverConsum');
    Route::get('/detail-modal/{id}', 'RequestFabric\FabricNeedController@detailModal')->name('fabricNeed.detailModal');
    Route::post('/post-data-marker', 'RequestFabric\FabricNeedController@updateDataMarker')->name('fabricNeed.updateDataMarker');
    Route::get('/detail-marker/{id}/detail/{part_no}', 'RequestFabric\FabricNeedController@detailDetailModal')->name('fabricNeed.detailDetailModal');
    Route::post('/approve-data-marker', 'RequestFabric\FabricNeedController@approveDataMarker')->name('fabricNeed.approveDataMarker');
    Route::post('gen-download-link', 'RequestFabric\FabricNeedController@downloadGenerate')->name('fabricNeed.downloadGenerate');
    Route::get('download/{planning_id}', 'RequestFabric\FabricNeedController@download')->name('fabricNeed.download');
});

// END permintaan fabric -> kebutuhan fabrik

// permintaan marker -> geser planning

Route::prefix('/plan-movement')->middleware(['permission:menu-geser-planning'])->group(function(){
    Route::get('', 'RequestMarker\PlanMovementController@index')->name('planMovement.index');
    Route::get('data-planning', 'RequestMarker\PlanMovementController@dataPlanning')->name('planMovement.dataPlanning');
    Route::get('data-detail/{id}', 'RequestMarker\PlanMovementController@dataDetail')->name('planMovement.dataDetail');
    Route::get('move-plan', 'RequestMarker\PlanMovementController@movePlan')->name('planMovement.movePlan');
    Route::post('delete-plan', 'RequestMarker\PlanMovementController@deletePlan')->name('planMovement.deletePlan');
});

// END permintaan marker -> geser planning

// Cutting -> Scan Cutting

Route::prefix('/scan-cutting')->middleware(['permission:menu-scan-cutting'])->group(function(){
    Route::get('', 'Cutting\ScanCuttingController@index')->name('ScanCutting.index');
    Route::get('data-cutting-table', 'Cutting\ScanCuttingController@dataCuttingTable')->name('ScanCutting.dataCuttingTable');
    Route::post('check-table', 'Cutting\ScanCuttingController@checkTable')->name('ScanCutting.checkTable');
    Route::post('check-operator', 'Cutting\ScanCuttingController@checkOperator')->name('ScanCutting.checkOperator');
    Route::post('check-marker', 'Cutting\ScanCuttingController@checkMarker')->name('ScanCutting.checkMarker');
    Route::post('save-cutting', 'Cutting\ScanCuttingController@checkMarker')->name('ScanCutting.saveCutting');
    Route::post('delete-operator', 'Cutting\ScanCuttingController@deleteOperator')->name('ScanCutting.deleteOperator');
    Route::post('update-downtime', 'Cutting\ScanCuttingController@updateDowntime')->name('ScanCutting.updateDowntime');
    Route::post('cancel-cutting', 'Cutting\ScanCuttingController@cancelCutting')->name('ScanCutting.cancelCutting');
    Route::post('delete-marker', 'Cutting\ScanCuttingController@deleteMarker')->name('ScanCutting.deleteMarker');
    Route::post('start-cutting', 'Cutting\ScanCuttingController@startCutting')->name('ScanCutting.startCutting');
    Route::post('end-cutting', 'Cutting\ScanCuttingController@endCutting')->name('ScanCutting.endCutting');
});

// END Cutting -> Scan Cutting

// Cutting -> Cutting Result

Route::prefix('/cutting-result')->middleware(['permission:menu-cutting-result'])->group(function(){
    Route::get('', 'Cutting\CuttingResultController@index')->name('CuttingResult.index');
    Route::get('data-cutting-result', 'Cutting\CuttingResultController@getData')->name('CuttingResult.getData');
});

// END Cutting -> Cutting Result

// Marker -> Release Marker

Route::prefix('/release-marker')->middleware(['permission:menu-release-marker'])->group(function(){
    Route::get('', 'RequestMarker\ReleaseMarkerController@index')->name('ReleaseMarker.index');
    Route::get('data-release', 'RequestMarker\ReleaseMarkerController@getData')->name('ReleaseMarker.getData');
    Route::get('detail/{id}', 'RequestMarker\ReleaseMarkerController@getDetail')->name('ReleaseMarker.getDetail');
    Route::get('data-release-detail', 'RequestMarker\ReleaseMarkerController@getDetailTable')->name('ReleaseMarker.getDetailTable');
});

// END Marker -> Release Marker

// Marker -> qc check

Route::prefix('/qc-check')->middleware(['permission:menu-qc-check'])->group(function(){
    Route::get('', 'Qccheck\QcController@index')->name('Qccheck.index');
    Route::get('/get-data', 'Qccheck\QcController@getData')->name('Qccheck.getData');
    Route::get('/get-header', 'Qccheck\QcController@getHeader')->name('Qccheck.getHeader');
    Route::get('/get-detail', 'Qccheck\QcController@getDetail')->name('Qccheck.getDetail');
    Route::post('/insert-qc', 'Qccheck\QcController@saveQc')->name('Qccheck.saveQc');

    // QR CODE
    Route::get('qr/{id}', 'Qccheck\QcController@indexQR')->name('Qcheck.indexQR');
    Route::get('qc-bundle', 'Qccheck\QcController@qcBundle')->name('Qccheck.qcBundle');
    Route::get('get-bundle', 'Qccheck\QcController@getBundle')->name('Qccheck.getBundle');
    Route::get('get-locator', 'Qccheck\QcController@getLocatorId')->name('Qccheck.getLocatorId');
    Route::get('get-defect-list/{id}', 'Qccheck\QcController@getDefectList')->name('Qccheck.getDefectList');
    Route::post('/save-qc', 'Qccheck\QcController@simpanQc')->name('Qccheck.simpanQc');

    Route::get('qc-approve', 'Qccheck\QcController@qcApprove')->name('Qccheck.qcApprove');
    Route::get('get-data-defect', 'Qccheck\QcController@getQcDefect')->name('Qccheck.getQcDefect');
    Route::post('/approve-reject', 'Qccheck\QcController@approveReject')->name('Qccheck.approveReject');
    // Route::get('get-bundle-approve/{id}', 'Qccheck\QcController@getBundleApprove')->name('Qccheck.getBundleApprove');

    Route::get('review-qc-v4', 'Qccheck\QcController@reviewQcV4')->name('Qccheck.reviewQcV4');
    Route::get('review-qc-v4/get-data', 'Qccheck\QcController@getDataReviewQcV4')->name('Qccheck.getDataReviewQcV4');
    Route::get('review-qc-v4/get-data/download/', 'Qccheck\QcController@downloadReviewQcV4')->name('Qccheck.downloadReviewQcV4');

});

Route::prefix('/qc-reject')->middleware(['permission:menu-qc-reject'])->group(function(){
    Route::get('', 'Qccheck\QcController@qcReject')->name('Qccheck.qcReject');
    Route::get('get-data-reject', 'Qccheck\QcController@getQcReject')->name('Qccheck.getQcReject');
    Route::post('/ganti-reject', 'Qccheck\QcController@gantiReject')->name('Qccheck.gantiReject');
    // Route::get('get-data-defect', 'Qccheck\QcController@getQcDefect')->name('Qccheck.getQcDefect');
    // Route::post('/approve-reject', 'Qccheck\QcController@approveReject')->name('Qccheck.approveReject');
});


// END Marker -> qc check

// approve qc check
Route::prefix('/approve-qc-check')->middleware(['permission:menu-approve-qc-check'])->group(function(){
    Route::get('', 'Qccheck\QcController@approveQcCheck')->name('Qccheck.approveQcCheck');
    Route::get('/get-barcode', 'Qccheck\QcController@getBarcodeMarker')->name('Qccheck.qcaApvBarcode');
    Route::get('/get-data', 'Qccheck\QcController@getDataQc')->name('Qccheck.qcaApvGetQc');
    Route::post('/set-approve', 'Qccheck\QcController@setApproveQc')->name('Qccheck.qcaApvsetQc');
});
// end approve qc check

// QC Component

Route::prefix('/qc-component')->middleware(['permission:menu-qc-component'])->group(function(){
    Route::get('', 'Qccheck\ComponenQCController@index')->name('ComponenQC.index');
    Route::get('get-data', 'Qccheck\ComponenQCController@getData')->name('ComponenQC.getData');
    Route::get('edit-modal/{style}/{season}/{komponen}', 'Qccheck\ComponenQCController@editModal')->name('ComponenQC.editModal');
    Route::post('/update-data', 'Qccheck\ComponenQCController@updateData')->name('ComponenQC.updateData');
});

// END QC Component

// GENERATE BARCODE
Route::prefix('/cutting-barcode')->middleware(['permission:menu-cutting-barcode'])->group(function(){
    //
    Route::get('', 'Cutting\CuttingBarcodeController@index')->name('cuttingbarcode.index');
    Route::get('/get-size', 'Cutting\CuttingBarcodeController@ajaxGetSize')->name('cuttingbarcode.ajaxGetSize');
    Route::post('/set-marker', 'Cutting\CuttingBarcodeController@ajaxSetMarker')->name('cuttingbarcode.ajaxSetMarker');
    Route::get('/get-marker', 'Cutting\CuttingBarcodeController@ajaxGetDataMarker')->name('cuttingbarcode.ajaxGetDataMarker');
    Route::post('/generate', 'Cutting\CuttingBarcodeController@generateBarcode')->name('cuttingbarcode.generateBarcode');
    Route::get('/print-preview/{barcode_marker}', 'Cutting\CuttingBarcodeController@printPreview')->name('cuttingbarcode.printPreview');
    Route::get('/get-po-size', 'Cutting\CuttingBarcodeController@getPoSize')->name('cuttingbarcode.getPoSize');
    Route::post('/add-po-ratio', 'Cutting\CuttingBarcodeController@addPoRatio')->name('cuttingbarcode.addPoRatio');
    Route::post('/update-qty-ratio', 'Cutting\CuttingBarcodeController@updateQtyRatio')->name('cuttingbarcode.updateQtyRatio');
    Route::post('/update-queue-ratio', 'Cutting\CuttingBarcodeController@updateQueueRatio')->name('cuttingbarcode.updateQueueRatio');
    Route::post('/update-size-ratio', 'Cutting\CuttingBarcodeController@updateSizeRatio')->name('cuttingbarcode.updateSizeRatio');
    // Route::post('/update-cutinfo-ratio', 'Cutting\CuttingBarcodeController@updateCutInfoRatio')->name('cuttingbarcode.updateCutInfoRatio');
    Route::post('/update-po-ratio', 'Cutting\CuttingBarcodeController@updatePoRatio')->name('cuttingbarcode.updatePoRatio');

    // update checklist komponen break
    Route::post('/checklist-komponen-break', 'Cutting\CuttingBarcodeController@updateKomponenBreak')->name('cuttingbarcode.updateKomponenBreak');
    Route::post('/sinkron-sds', 'Cutting\CuttingBarcodeController@sinkronSDS')->name('cuttingbarcode.sinkronSDS');
    Route::get('/migrate-sds', 'Cutting\CuttingBarcodeController@migrateSDS')->name('cuttingbarcode.migrateSDS');
});

// END GENERATE BARCODE

// GENERATE BARCODE V2 MANUAL CREATION
Route::prefix('/manual-creation')->middleware(['permission:menu-cutting-barcode'])->group(function(){
    Route::get('/', 'Cutting\ManualCreateController@index')->name('manual_create.index');
    Route::get('data-style', 'Cutting\ManualCreateController@getDataStyle')->name('manual_create.getDataStyle');
    Route::get('/syp-view', 'Cutting\ManualCreateController@sypView')->name('manual_create.sypView');
    Route::get('/get-data-manual', 'Cutting\ManualCreateController@getDataManualCreate')->name('manual_create.getDataManualCreate');
    Route::get('/sync-barcode/{id}', 'Cutting\ManualCreateController@sycnBarcodeBundle')->name('manual_create.sycnBarcodeBundle');
    Route::get('/parse-barcode/{id}', 'Cutting\ManualCreateController@parseBarcode')->name('manual_create.parseBarcode');
    Route::get('/print-ticket/{barcode_id}', 'Cutting\ManualCreateController@printBundle')->name('manual_create.printBundle');
    Route::post('/add-data-size', 'Cutting\ManualCreateController@addDatasize')->name('manual_create.addDatasize');
    Route::post('/add-detail-size', 'Cutting\ManualCreateController@addDetailsize')->name('manual_create.addDetailsize');
    Route::get('get-data-size', 'Cutting\ManualCreateController@getDataSize')->name('manual_create.getDataSize');
    Route::get('get-summary-size', 'Cutting\ManualCreateController@summarySizeQty')->name('manual_create.summarySizeQty');
    Route::get('get-data-detail', 'Cutting\ManualCreateController@getDataDetail')->name('manual_create.getDataDetail');
    Route::get('/delete/{id}', 'Cutting\ManualCreateController@deleteTempSize')->name('manual_create.deleteTempSize');
    Route::get('/delete-detail/{id}', 'Cutting\ManualCreateController@deleteDetailsize')->name('manual_create.deleteDetailsize');
    Route::get('session-edit', 'Cutting\ManualCreateController@sessionEdit')->name('manual_create.sessionEdit');
    Route::get('check-reg-szie', 'Cutting\ManualCreateController@checkRegSize')->name('manual_create.checkRegSize');
    Route::get('update-que', 'Cutting\ManualCreateController@updateQue')->name('manual_create.updateQue');
    Route::get('data-size', 'Cutting\ManualCreateController@ajaxGetDataSize')->name('manual_create.ajaxGetDataSize');
    Route::get('data-po-buyer', 'Cutting\ManualCreateController@ajaxGetDataPobuyer')->name('manual_create.ajaxGetDataPobuyer');
    Route::get('get-art', 'Cutting\ManualCreateController@ajaxGetDataArt')->name('manual_create.ajaxGetDataArt');
    Route::post('create-head', 'Cutting\ManualCreateController@createHead')->name('manual_create.createHead');
    Route::post('/generate-barcode', 'Cutting\ManualCreateController@generateBarcode')->name('manual_create.generateBarcode');
    Route::post('delete-detail', 'Cutting\ManualCreateController@deleteDetail')->name('manual_create.deleteDetail');
    Route::post('delete-header', 'Cutting\ManualCreateController@deleteHeader')->name('manual_create.deleteHeader');
    Route::post('/update-qty', 'Cutting\ManualCreateController@updateQty')->name('manual_create.updateQty');
});
// END MANUAL CREATION TICKET BUNDLE

// PRINT TICKET PARTIAL
Route::prefix('/partial-print-ticket')->middleware(['permission:menu-cutting-barcode'])->group(function(){
    Route::get('', 'Cutting\BarcodeManualController@index')->name('manual_barcode.index');
    Route::get('/getDataDetailbundle', 'Cutting\BarcodeManualController@getDataDetailbundle')->name('manual_barcode.getDataDetailbundle');
    Route::get('/printPdf', 'Cutting\BarcodeManualController@printPdf')->name('manual_barcode.printPdf');
    Route::get('/getStyle', 'Cutting\BarcodeManualController@getStyle')->name('manual_barcode.getStyle');
    Route::get('/getCutnum', 'Cutting\BarcodeManualController@getCutnum')->name('manual_barcode.getCutnum');
    Route::get('/getDetailbypobundle', 'Cutting\BarcodeManualController@getDetailbypobundle')->name('manual_barcode.getDetailbypobundle');
    Route::post('/add-new-data', 'Cutting\BarcodeManualController@addNewData')->name('manual_barcode.addNewData');
    Route::get('/get-data-ticket', 'Cutting\BarcodeManualController@getDataTicket')->name('manual_barcode.getDataTicket');
    Route::get('/delete-detail/{id}', 'Cutting\BarcodeManualController@deleteBarcode')->name('manual_barcode.deleteBarcode');
    Route::get('/delete-all-detail', 'Cutting\BarcodeManualController@deleteAllDetail')->name('manual_barcode.deleteAllDetail');
    Route::get('/multiple-print/{ticket_box}/{user}/{factory}', 'Cutting\BarcodeManualController@printTicketAll')->name('manual_barcode.printTicketAll');
    Route::get('/session-destroy/{user}', 'Cutting\BarcodeManualController@sessionDestroy')->name('manual_barcode.sessionDestroy');
});
// END PRINT TICKET PARTIAL

// movement component -> scan component
Route::prefix('/scan-component')->middleware(['permission:menu-scan-component'])->group(function(){
    // component movement

    // menampilkan halaman scan componen
    Route::get('', 'ComponentMovement\ScanComponentController@index')->name('ScanComponent.index');
    // get data locator
    Route::get('/data-locator', 'ComponentMovement\ScanComponentController@dataLocator')->name('ScanComponent.dataLocator');
    // scan action
    Route::post('/scan-component', 'ComponentMovement\ScanComponentController@scanComponent')->name('ScanComponent.scanComponent');
    // open modal list line jika komponen tidak ada di ppcm
    Route::post('/scan-manual-set', 'ComponentMovement\ScanComponentController@scanManualSet')->name('ScanComponent.scanManualSet');
    // delete data jika modal list line ditutup
    Route::post('/delete-scan-proc', 'ComponentMovement\ScanComponentController@deleteScanProcSet')->name('ScanComponent.deleteScanProcSet');

    // bundle check
    Route::get('bundle-check', 'ComponentMovement\ScanComponentController@bundleCheck')->name('bundleCheck.index');
    // bundle check action
    Route::post('info-bundle', 'ComponentMovement\ScanComponentController@bundleInfo')->name('bundleCheck.bundleInfo');
});
// END movement component -> scan component

// master setting
Route::prefix('/master-setting')->middleware(['permission:menu-master-setting'])->group(function(){
    Route::get('','MasterParameter\MasterSettingArea@index')->name('Asetting.index');
    Route::get('/get-data','MasterParameter\MasterSettingArea@getData')->name('Asetting.getData');
    Route::post('/add-data','MasterParameter\MasterSettingArea@addArea')->name('Asetting.addAread');
    Route::get('/get-edit','MasterParameter\MasterSettingArea@getDataEdit')->name('Asetting.getDataEdit');
    Route::post('/edit-data','MasterParameter\MasterSettingArea@setEditData')->name('Asetting.setEditData');
    Route::post('/delete-data','MasterParameter\MasterSettingArea@deleteArea')->name('Asetting.deleteArea');
});
// end master setting

// master subcont factory
Route::prefix('/master-subcont-factory')->middleware(['permission:master-subcont-factory'])->group(function(){
    Route::get('', 'MasterParameter\SubcontFactoryController@index')->name('subcont_factory.subcontfactory');
    Route::get('/get-data', 'MasterParameter\SubcontFactoryController@ajaxGetDataMasterSubcontFactory')->name('subcont_factory.ajaxGetDataMasterSubcontFactory');
    Route::post('/add', 'MasterParameter\SubcontFactoryController@addSubcontFactory')->name('subcont_factory.addSubcontFactory');
    Route::get('/edit/{id}', 'MasterParameter\SubcontFactoryController@editSubcontFactory')->name('subcont_factory.editSubcontFactory');
    Route::post('/update', 'MasterParameter\SubcontFactoryController@updateSubcontFactory')->name('subcont_factory.updateSubcontFactory');
    Route::get('/delete/{id}', 'MasterParameter\SubcontFactoryController@deleteSubcontFactory')->name('subcont_factory.deleteSubcontFactory');
});
// end master subcont factory

// master ppcm
Route::prefix('/master-ppcm')->middleware(['permission:menu-master-ppcm'])->group(function(){
    Route::get('','Ppcm\MasterPpcmController@index')->name('MasterPpcm.index');
    Route::post('/upload-data','Ppcm\MasterPpcmController@uploadPpcm')->name('MasterPpcm.uploadPpcm');
    Route::get('/get-data','Ppcm\MasterPpcmController@getDataPpcm')->name('MasterPpcm.getDataPpcm');
    Route::post('/upload-ppcm','Ppcm\MasterPpcmController@UploadPpcmNew')->name('MasterPpcm.UploadPpcmNew');
});

// end master ppcm
// UPLOAD DOC KK PPC
Route::prefix('/menu-ppc-distribusi')->middleware(['permission:menu-master-ppcm'])->group(function(){
    Route::get('','Ppcm\DockkController@index')->name('DocKK.index');
    Route::post('/upload-kk','Ppcm\DockkController@UploadKK')->name('DocKK.UploadKK');
    Route::get('/get-data','Ppcm\DockkController@getDataDockk')->name('DocKK.getDataDockk');
    Route::get('/edit-data','Ppcm\DockkController@editDataDockk')->name('DocKK.editDataDockk');
    Route::get('/edit/{id}','Ppcm\DockkController@editQty')->name('DocKK.editQty');
    Route::post('/update','Ppcm\DockkController@updateQty')->name('DocKK.updateQty');
});

// master exim
Route::prefix('/menu-exim')->middleware(['permission:menu-exim'])->group(function(){
    Route::get('','Exim\EximController@index')->name('Exim.index');
    Route::post('/upload-data','Exim\EximController@uploaddocumentbc')->name('Exim.uploaddocumentbc');
    Route::get('/get-data','Exim\EximController@getDataDocbc')->name('Exim.getDataDocbc');
});

// end master exim

// master kontrak kerja & subcont
Route::prefix('/master-kontrak')->middleware(['permission:menu-kontrak-kerja'])->group(function(){
    //createkk
    Route::get('','KontrakKerja\MasterKkController@masterKontrak')->name('MasterKK.masterKontrak');
    Route::post('/create','KontrakKerja\MasterKkController@create_kk')->name('MasterKK.create_kk');
    Route::get('/report','KontrakKerja\MasterKkController@LaporanKK')->name('MasterKK.LaporanKK');
    Route::get('/get-data','KontrakKerja\MasterKkController@getDataKK')->name('MasterKK.getDataKK');
    Route::get('/get-head','KontrakKerja\MasterKkController@getHeadKK')->name('MasterKK.getHeadKK');
    Route::get('/export','KontrakKerja\MasterKkController@exportLaporanKK')->name('MasterKK.exportLaporanKK');

    //packinglist
    Route::get('/packing-list','KontrakKerja\MasterKkController@packingListKK')->name('MasterKK.packingListKK');
    Route::get('/packing-list/ajaxGetStyle','KontrakKerja\MasterKkController@ajaxGetStyle')->name('MasterKK.ajaxGetStyle');
    Route::get('/packing-list/get-data','KontrakKerja\MasterKkController@packinglistGetData')->name('MasterKK.packinglistGetData');
    Route::get('/packing-list/print/{id}','KontrakKerja\MasterKkController@print')->name('MasterKK.packinglistPrint');
    Route::get('/packing-list/ajaxGetPo','KontrakKerja\MasterKkController@ajaxGetPo')->name('MasterKK.ajaxGetPo');
    Route::get('/packing-list/generatePackingNumber','KontrakKerja\MasterKkController@generatePackingNumber')->name('MasterKK.generatePackingNumber');
    Route::post('/packing-list/create','KontrakKerja\MasterKkController@createPackinglistKK')->name('MasterKK.createPackinglistKK');

});

Route::prefix('/scan-subcont')->middleware(['permission:menu-scan-subcont'])->group(function(){

    Route::get('/out','KontrakKerja\SubcontController@scanOut')->name('Subcont.scanOut');
    Route::post('/out/ajaxScanOut','KontrakKerja\SubcontController@ajaxScanOut')->name('Subcont.ajaxScanOut');
    Route::get('/out/get-data','KontrakKerja\SubcontController@getDataTemp')->name('Subcont.getDataTemp');
    Route::post('/out/delete-data','KontrakKerja\SubcontController@deleteBundleTmp')->name('Subcont.deleteBundleTmp');
    Route::get('/out/submit-data','KontrakKerja\SubcontController@submitPackinglist')->name('Subcont.submitPackinglist');
    Route::get('/out/get-data-pl','KontrakKerja\SubcontController@getDataPl')->name('Subcont.getDataPl');


});

// end master kontrak kerja & subcont

// LAPORAN BARU marker

Route::prefix('/laporan-baru-marker')->middleware(['permission:menu-actual-marker'])->group(function(){
    Route::get('', 'RequestMarker\LaporanBaruMarkerController@index')->name('laporanBaruMarker.index');
    Route::get('data-planning', 'RequestMarker\LaporanBaruMarkerController@dataPlanning')->name('laporanBaruMarker.dataPlanning');
    Route::get('data-marker', 'RequestMarker\LaporanBaruMarkerController@dataMarker')->name('laporanBaruMarker.dataMarker');
    Route::get('data-detail-marker', 'RequestMarker\LaporanBaruMarkerController@dataDetailMarker')->name('laporanBaruMarker.dataDetailMarker');
    Route::get('data-lebar-fabrik', 'RequestMarker\LaporanBaruMarkerController@dataLebarFB')->name('laporanBaruMarker.dataLebarFB');
    Route::get('data-size-balance', 'RequestMarker\LaporanBaruMarkerController@dataSizeBalance')->name('laporanBaruMarker.dataSizeBalance');
    Route::get('data-over-consum', 'RequestMarker\LaporanBaruMarkerController@dataOverConsum')->name('laporanBaruMarker.dataOverConsum');
    Route::get('/detail-modal/{id}', 'RequestMarker\LaporanBaruMarkerController@detailModal')->name('laporanBaruMarker.detailModal');
    Route::post('/post-data-marker', 'RequestMarker\LaporanBaruMarkerController@updateDataMarker')->name('laporanBaruMarker.updateDataMarker');
    Route::get('/detail-marker/{id}/detail/{part_no}', 'RequestMarker\LaporanBaruMarkerController@detailDetailModal')->name('laporanBaruMarker.detailDetailModal');
    Route::post('/approve-data-marker', 'RequestMarker\LaporanBaruMarkerController@approveDataMarker')->name('laporanBaruMarker.approveDataMarker');
    Route::post('/add-row', 'RequestMarker\LaporanBaruMarkerController@addRow')->name('laporanBaruMarker.addRow');
    Route::post('/delete-row', 'RequestMarker\LaporanBaruMarkerController@deleteRow')->name('laporanBaruMarker.deleteRow');
    Route::post('/update-layer', 'RequestMarker\LaporanBaruMarkerController@updateLayer')->name('laporanBaruMarker.updateLayer');
    Route::post('/update-fabric-width', 'RequestMarker\LaporanBaruMarkerController@updateFabricWidth')->name('laporanBaruMarker.updateFabricWidth');
    Route::post('/update-perimeter', 'RequestMarker\LaporanBaruMarkerController@updatePerimeter')->name('laporanBaruMarker.updatePerimeter');
    Route::post('/update-marker-length', 'RequestMarker\LaporanBaruMarkerController@updateMarkerLength')->name('laporanBaruMarker.updateMarkerLength');
    Route::post('/update-rasio', 'RequestMarker\LaporanBaruMarkerController@updateRasio')->name('laporanBaruMarker.updateRasio');
    Route::post('/reset-data-marker', 'RequestMarker\LaporanBaruMarkerController@resetData')->name('laporanBaruMarker.resetData');
    Route::post('/remove-page-access', 'RequestMarker\LaporanBaruMarkerController@removePageAccess')->name('laporanBaruMarker.removePageAccess');
    Route::get('/download-report/{id}', 'RequestMarker\LaporanBaruMarkerController@downloadReport')->name('laporanBaruMarker.downloadReport');
    Route::get('/download-ratio/{id}', 'RequestMarker\LaporanBaruMarkerController@downloadRatio')->name('laporanBaruMarker.downloadRatio');
    Route::post('/release-marker', 'RequestMarker\LaporanBaruMarkerController@ReleaseMarker')->name('laporanBaruMarker.ReleaseMarker');
});

// end LAPORAN BARU marker

// info fir

Route::prefix('/info-fir')->middleware(['permission:menu-qc-check'])->group(function(){
    Route::get('', 'Qccheck\InfoFIRController@index')->name('infoFIR.index');
    Route::get('data-detail', 'Qccheck\InfoFIRController@getData')->name('infoFIR.getData');
    Route::get('detail-fir/{id}', 'Qccheck\InfoFIRController@detail')->name('infoFIR.detail');

    // QR
    Route::get('qr/{id}', 'Qccheck\InfoFIRController@indexQR')->name('infoFIR.indexQR');
});

// end info fir

// movement component subcont -> scan component

Route::prefix('/scan-component-subcont')->middleware(['permission:menu-scan-component-subcont'])->group(function(){
    Route::get('', 'ComponentMovement\ScanComponentSubcontController@index')->name('ScanComponentSubcont.index');
    Route::get('/data-locator', 'ComponentMovement\ScanComponentSubcontController@dataLocator')->name('ScanComponentSubcont.dataLocator');
    Route::get('/data-component', 'ComponentMovement\ScanComponentSubcontController@dataComponent')->name('ScanComponentSubcont.dataComponent');
    Route::get('/data-component-packinglist', 'ComponentMovement\ScanComponentSubcontController@dataComponentPackinglist')->name('ScanComponentSubcont.dataComponentPackinglist');
    Route::get('/detail-packinglist', 'ComponentMovement\ScanComponentSubcontController@detailPackinglist')->name('ScanComponentSubcont.detailPackinglist');
    Route::post('/data-packinglist', 'ComponentMovement\ScanComponentSubcontController@dataPackinglist')->name('ScanComponentSubcont.dataPackinglist');
    Route::post('/scan-component', 'ComponentMovement\ScanComponentSubcontController@scanComponent')->name('ScanComponentSubcont.scanComponent');
    Route::post('/scan-component/{id}', 'ComponentMovement\ScanComponentSubcontController@deleteComponent')->name('ScanComponentSubcont.deleteComponent');
    Route::post('/save-component', 'ComponentMovement\ScanComponentSubcontController@saveComponent')->name('ScanComponentSubcont.saveComponent');
});

// END movement component subcont -> scan component

// movement component subcont -> scan component

Route::prefix('/scan-component-subcont-in')->middleware(['permission:menu-scan-component-subcont-in'])->group(function(){
    Route::get('', 'ComponentMovement\ScanComponentSubcontInController@index')->name('ScanComponentSubcontIn.index');
});

// END movement component subcont -> scan component

// QR Code

Route::prefix('/qr-code')->middleware(['permission:menu-scan-qr'])->group(function(){
    Route::get('/scan/{id}', 'QRController@scanBarcode')->name('QR.scan');
});

Route::prefix('/qr-code-setting')->middleware(['permission:menu-scan-qr-setting'])->group(function(){
    Route::get('/state', 'QRController@settingState')->name('QRSetting.settingState');
});

// End QR Code

// MARKER TRACE
Route::prefix('/marker-trace')->middleware(['permission:menu-marker-trace'])->group(function(){
    Route::get('', 'RequestMarker\MarkerTraceController@index')->name('markerTrace.index');
    Route::post('scan-barcode', 'RequestMarker\MarkerTraceController@scanBarcode')->name('markerTrace.scanBarcode');
    // Route::get('get-review-spreading', 'RequestMarker\MarkerTraceController@getReviewspreading')->name('markerTrace.getReviewspreading');
});

Route::prefix('/po-trace')->middleware(['permission:menu-marker-trace'])->group(function(){
    Route::get('', 'RequestMarker\MarkerTraceController@POTrace')->name('poTrace.index');
    Route::get('data-po', 'RequestMarker\MarkerTraceController@dataPO')->name('poTrace.dataPO');
    Route::post('find-po', 'RequestMarker\MarkerTraceController@findPO')->name('poTrace.findPO');
    Route::post('data-marker', 'RequestMarker\MarkerTraceController@dataMarker')->name('poTrace.dataMarker');
    Route::get('/detail-modal/{id}', 'RequestMarker\MarkerTraceController@partModal')->name('poTrace.markerModal');
    Route::post('gen-download-link', 'RequestMarker\MarkerTraceController@downloadGenerate')->name('poTrace.downloadGenerate');
    Route::get('download/{po}', 'RequestMarker\MarkerTraceController@download')->name('poTrace.download');

});
// END MARKER TRACE

// SECOND PLAN
Route::prefix('/second-plan')->middleware(['permission:menu-second-plan'])->group(function(){
    Route::get('', 'SpecialAccess\SecondPlanController@index')->name('secondPlan.index');
    Route::post('scan-barcode', 'SpecialAccess\SecondPlanController@scanBarcode')->name('secondPlan.scanBarcode');
    Route::post('add', 'SpecialAccess\SecondPlanController@add')->name('secondPlan.add');
    Route::post('delete', 'SpecialAccess\SecondPlanController@delete')->name('secondPlan.delete');
});
// END SECOND PLAN

// PACKINGLIST SUBCONT
Route::prefix('/packinglist-subcont')->middleware(['permission:menu-packinglist-subcontsys'])->group(function(){
    Route::get('', 'Subcont\PackinglistSubcontController@index')->name('packinglistSubcont.index');
    Route::get('get-data', 'Subcont\PackinglistSubcontController@getData')->name('packinglistSubcont.getData');
    Route::get('data-packinglist', 'Subcont\PackinglistSubcontController@ajaxGetDataPackinglist')->name('packinglistSubcont.ajaxGetDataPackinglist');
    Route::get('data-packinglist-head', 'Subcont\PackinglistSubcontController@ajaxGetDataPackinglisthead')->name('packinglistSubcont.ajaxGetDataPackinglisthead');
    Route::get('data-component', 'Subcont\PackinglistSubcontController@dataComponent')->name('packinglistSubcont.dataComponent');
    Route::get('data-component-packinglist', 'Subcont\PackinglistSubcontController@dataComponentPackinglist')->name('packinglistSubcont.dataComponentPackinglist');
    Route::get('detail-packinglist', 'Subcont\PackinglistSubcontController@detailPackinglist')->name('packinglistSubcont.detailPackinglist');
    Route::post('data-packinglist', 'Subcont\PackinglistSubcontController@dataPackinglist')->name('packinglistSubcont.dataPackinglist');
    Route::get('print/{id}', 'Subcont\PackinglistSubcontController@print')->name('packinglistSubcont.print');
    Route::post('store', 'Subcont\PackinglistSubcontController@store')->name('packinglistSubcont.store');
    Route::post('get-no-packinglist', 'Subcont\PackinglistSubcontController@getNoPackinglist')->name('packinglistSubcont.getNoPackinglist');
    Route::delete('delete/{id}', 'Subcont\PackinglistSubcontController@delete')->name('packinglistSubcont.delete');
    Route::get('edit-modal/{id}', 'Subcont\PackinglistSubcontController@edit')->name('packinglistSubcont.edit');
    Route::post('/update-data', 'Subcont\PackinglistSubcontController@update')->name('packinglistSubcont.update');
    Route::post('/release/{id}', 'Subcont\PackinglistSubcontController@release')->name('packinglistSubcont.release');
    Route::post('scan-component/{id}', 'Subcont\PackinglistSubcontController@deleteComponent')->name('packinglistSubcont.deleteComponent');
    Route::post('scan-component', 'Subcont\PackinglistSubcontController@scanComponent')->name('packinglistSubcont.scanComponent');
    Route::post('save-component', 'Subcont\PackinglistSubcontController@saveComponent')->name('packinglistSubcont.saveComponent');
});
// END PACKINGLIST SUBCONT

// SCAN SUBCONT OUT
Route::prefix('/scan-subcont-out')->middleware(['permission:menu-scan-component-subcontsys'])->group(function(){
    Route::get('', 'Subcont\ScanSubcontOutController@index')->name('scanSubcontOut.index');
    Route::get('data-component', 'Subcont\ScanSubcontOutController@dataComponent')->name('scanSubcontOut.dataComponent');
    Route::get('data-scan-outsubcont', 'Subcont\ScanSubcontOutController@dataScanOutsubcont')->name('scanSubcontOut.dataScanOutsubcont');
    Route::get('detail-packinglist', 'Subcont\ScanSubcontOutController@detailPackinglist')->name('scanSubcontOut.detailPackinglist');
    Route::post('data-out', 'Subcont\ScanSubcontOutController@dataScanout')->name('scanSubcontOut.dataScanout');
    Route::post('scan-component', 'Subcont\ScanSubcontOutController@scanComponent')->name('scanSubcontOut.scanComponent');
    Route::post('scan-component/{id}', 'Subcont\ScanSubcontOutController@deleteComponent')->name('scanSubcontOut.deleteComponent');
    Route::post('save-component', 'Subcont\ScanSubcontOutController@saveComponent')->name('scanSubcontOut.saveComponent');
});
// END SCAN SUBCONT OUT

// SCAN SUBCONT IN
Route::prefix('/scan-subcont-in')->middleware(['permission:menu-scan-component-subcontsys'])->group(function(){
    Route::get('', 'Subcont\ScanSubcontInController@index')->name('scanSubcontIn.index');
    Route::get('data-component', 'Subcont\ScanSubcontInController@dataComponent')->name('scanSubcontIn.dataComponent');
    Route::get('data-scan-insubcont', 'Subcont\ScanSubcontInController@dataScanInsubcont')->name('scanSubcontIn.dataScanInsubcont');
    Route::get('detail-packinglist', 'Subcont\ScanSubcontInController@detailPackinglist')->name('scanSubcontIn.detailPackinglist');
    Route::post('data-in', 'Subcont\ScanSubcontInController@dataScanin')->name('scanSubcontIn.dataScanin');
    Route::post('scan-component', 'Subcont\ScanSubcontInController@scanComponent')->name('scanSubcontIn.scanComponent');
    Route::post('scan-component/{id}', 'Subcont\ScanSubcontInController@deleteComponent')->name('scanSubcontIn.deleteComponent');
    Route::post('save-component', 'Subcont\ScanSubcontInController@saveComponent')->name('scanSubcontIn.saveComponent');
});
// END SUBCONT IN

// SCAN DISTRIBUSI ARTWORK OUT
Route::prefix('/scan-distribusi-artwork-out')->middleware(['permission:menu-distribusi-artwork'])->group(function(){
    Route::get('', 'DistribusiArtwork\DistribusiArtworkoutController@index')->name('distribusi_artwork.index');
    Route::get('data-component', 'DistribusiArtwork\DistribusiArtworkoutController@dataComponent')->name('distribusi_artwork.dataComponent');
    Route::get('data-component-packinglist', 'DistribusiArtwork\DistribusiArtworkoutController@dataComponentPackinglist')->name('distribusi_artwork.dataComponentPackinglist');
    Route::get('detail-packinglist', 'DistribusiArtwork\DistribusiArtworkoutController@detailPackinglist')->name('distribusi_artwork.detailPackinglist');
    Route::post('data-scan-out', 'DistribusiArtwork\DistribusiArtworkoutController@dataScanout')->name('distribusi_artwork.dataScanout');
    Route::post('data-packinglist', 'DistribusiArtwork\DistribusiArtworkoutController@dataPackinglist')->name('distribusi_artwork.dataPackinglist');
    Route::post('scan-component', 'DistribusiArtwork\DistribusiArtworkoutController@scanComponent')->name('distribusi_artwork.scanComponent');
    Route::post('scan-component/{id}', 'DistribusiArtwork\DistribusiArtworkoutController@deleteComponent')->name('distribusi_artwork.deleteComponent');
    Route::post('save-component', 'DistribusiArtwork\DistribusiArtworkoutController@saveComponent')->name('distribusi_artwork.saveComponent');
});
// END DISTRIBUSI ARTWORK IN

// PACKINGLIST DISTRIBUSI
Route::prefix('/packinglist-distribusi')->middleware(['permission:menu-distribusi-artwork'])->group(function(){
    Route::get('', 'DistribusiArtwork\PackinglistDistribusiController@index')->name('packinglistDistribusi.index');
    Route::get('get-data', 'DistribusiArtwork\PackinglistDistribusiController@getData')->name('packinglistDistribusi.getData');
    Route::get('print/{id}', 'DistribusiArtwork\PackinglistDistribusiController@print')->name('packinglistDistribusi.print');
    Route::post('store', 'DistribusiArtwork\PackinglistDistribusiController@store')->name('packinglistDistribusi.store');
    Route::post('get-no-packinglist', 'DistribusiArtwork\PackinglistDistribusiController@getNoPackinglist')->name('packinglistDistribusi.getNoPackinglist');
    Route::delete('delete/{id}', 'DistribusiArtwork\PackinglistDistribusiController@delete')->name('packinglistDistribusi.delete');
    Route::get('edit-modal/{id}', 'DistribusiArtwork\PackinglistDistribusiController@edit')->name('packinglistDistribusi.edit');
    Route::post('/update-data', 'DistribusiArtwork\PackinglistDistribusiController@update')->name('packinglistDistribusi.update');
    Route::post('/release/{id}', 'DistribusiArtwork\PackinglistDistribusiController@release')->name('packinglistDistribusi.release');
    //INHOUSE
    Route::get('inhouse', 'DistribusiArtwork\PackinglistDistribusiController@inhouse_index')->name('packinglistDistribusi.inhouse_index');
    Route::get('get-data-inhouse', 'DistribusiArtwork\PackinglistDistribusiController@getDataInhouse')->name('packinglistDistribusi.getDataInhouse');
    Route::get('data-packinglist', 'DistribusiArtwork\PackinglistDistribusiController@ajaxGetDataPackinglistinhouse')->name('packinglistDistribusi.ajaxGetDataPackinglistinhouse');
    Route::post('store-inhouse', 'DistribusiArtwork\PackinglistDistribusiController@storeInhouse')->name('packinglistDistribusi.storeInhouse');
    Route::get('edit-modal-inhouse/{id}', 'DistribusiArtwork\PackinglistDistribusiController@editInhouse')->name('packinglistDistribusi.editInhouse');
    Route::post('/update-data-inhouse', 'DistribusiArtwork\PackinglistDistribusiController@updateInhouse')->name('packinglistDistribusi.updateInhouse');
    Route::get('getKk', 'DistribusiArtwork\PackinglistDistribusiController@getKk')->name('packinglistDistribusi.getKk');
    Route::get('getKkUpdate', 'DistribusiArtwork\PackinglistDistribusiController@getKkUpdate')->name('packinglistDistribusi.getKkUpdate');
    Route::get('/destroySessionedit', 'DistribusiArtwork\PackinglistDistribusiController@deleteEditPackinglist')->name('packinglistDistribusi.deleteEditPackinglist');
});
// END PACKINGLIST DISTRIBUSI

// Route::prefix('/reverse-packinglist')->middleware(['permission:reverse-packinglist-distribusi'])->group(function(){
//     Route::post('get-no-packinglist', 'DistribusiArtwork\PackinglistDistribusiController@ajaxGetDataPackinglistinhouse')->name('reverse_doc.ajaxGetDataPackinglistinhouse');
//     Route::get('reverse', 'DistribusiArtwork\PackinglistDistribusiController@indexReverse')->name('reverse_doc.indexReverse');
//     Route::post('reverse-packinglist', 'DistribusiArtwork\PackinglistDistribusiController@reversePackinglist')->name('reverse_doc.reversePackinglist');
// });
// SCAN DISTRIBUSI ARTWORK IN
Route::prefix('/scan-distribusi-artwork-in')->middleware(['permission:menu-distribusi-artwork'])->group(function(){
    Route::get('', 'DistribusiArtwork\DistribusiArtworkinController@index')->name('distribusi_artworkin.index');
    Route::get('data-component', 'DistribusiArtwork\DistribusiArtworkinController@dataComponent')->name('distribusi_artworkin.dataComponent');
    Route::get('data-component-packinglist', 'DistribusiArtwork\DistribusiArtworkinController@dataComponentPackinglist')->name('distribusi_artworkin.dataComponentPackinglist');
    Route::get('detail-packinglist', 'DistribusiArtwork\DistribusiArtworkinController@detailPackinglist')->name('distribusi_artworkin.detailPackinglist');
    Route::post('data-packinglist', 'DistribusiArtwork\DistribusiArtworkinController@dataPackinglist')->name('distribusi_artworkin.dataPackinglist');
    Route::post('scan-component', 'DistribusiArtwork\DistribusiArtworkinController@scanComponent')->name('distribusi_artworkin.scanComponent');
    Route::post('scan-component/{id}', 'DistribusiArtwork\DistribusiArtworkinController@deleteComponent')->name('distribusi_artworkin.deleteComponent');
    Route::post('save-component', 'DistribusiArtwork\DistribusiArtworkinController@saveComponent')->name('distribusi_artworkin.saveComponent');
});
// END DISTRIBUSI ARTWORK IN


// SCAN OPERATOR
Route::prefix('/scan-distribusi')->middleware(['permission:menu-scan-operator'])->group(function(){
    Route::get('', 'Operator\DistribusiController@index')->name('distribusi.index');
    Route::get('{id}/menu', 'Operator\DistribusiController@menu')->name('distribusi.menu');
    Route::get('/bundle-loc', 'Operator\DistribusiController@bundleLocator')->name('distribusi.bundleLocator');
    Route::get('/data-bundle-loc', 'Operator\DistribusiController@getDataBundleLoc')->name('distribusi.getDataBundleLoc');

    Route::get('{id}/check-in', 'Operator\DistribusiController@checkIn')->name('distribusi.checkIn');
    // Route::get('{id}/move-loc-set', 'Operator\DistribusiController@moveLocSet')->name('distribusi.moveLocSet');
    // Route::post('/move-locator-bundle', 'Operator\DistribusiController@scanMoveBundle')->name('distribusi.scanMoveBundle');
    // Route::post('/locator-set', 'Operator\DistribusiController@selectLocatorMove')->name('distribusi.selectLocatorMove');
    // Route::get('/scan-component/line-move-locator', 'Operator\DistribusiController@moveLocatorLine')->name('distribusi.moveLocatorLine');

    Route::post('pengajuan', 'Operator\DistribusiController@pengajuan')->name('distribusi.pengajuan');

    Route::get('{id}/check-out', 'Operator\DistribusiController@checkOut')->name('distribusi.checkOut');
    Route::get('{id}/check-bundle', 'Operator\DistribusiController@checkBundle')->name('distribusi.checkBundle');

    Route::post('/scan-manual-set', 'Operator\DistribusiController@scanManualSet')->name('distribusi.scanManualSet');
    // delete data jika modal list line ditutup
    Route::post('/delete-scan-proc', 'Operator\DistribusiController@deleteScanProcSet')->name('distribusi.deleteScanProcSet');

    Route::post('/scan-component', 'Operator\DistribusiController@scanComponent')->name('distribusi.scanComponent');
    Route::get('/scan-component/get-data/mjbundle-json', 'Operator\DistribusiController@ajaxGetDataBundletableJson')->name('distribusi.ajaxGetDataBundletableJson');
    // Route::get('/scan-component/line-set-locator', 'Operator\DistribusiController@SetLocatorLine')->name('distribusi.SetLocatorLine');
    Route::post('info-bundle', 'Operator\DistribusiController@bundleInfo')->name('distribusi.bundleInfo');
    Route::get('force-del-temp','Operator\DistribusiController@forceDelTemp')->name('distribusi.forceDelTemp');
    // Route::post('/scanCuttingTemp', 'Operator\DistribusiController@scanCuttingTemp')->name('distribusi.scanCuttingTemp');

    // Route::get('menu/{id}', 'Operator\DistribusiController@menu')->name('distribusi.menu');

    // Route::get('check-in/{id}', 'Operator\DistribusiController@checkIn')->name('distribusi.checkIn');
    // Route::get('check-out/{id}', 'Operator\DistribusiController@checkOut')->name('distribusi.checkOut');
    // Route::get('check-bundle/{id}', 'Operator\DistribusiController@checkBundle')->name('distribusi.checkBundle');

    // Route::get('menu-scan', 'Operator\DistribusiController@menu')->name('distribusi.menu');

    // Route::get('/scan-distribusi', 'HomeController@scanDistribusi')->name('scanDistribusi');
});
// END SCAN OPERATOR
//LAB TESTING
    Route::prefix('fabric-testing')->middleware(['permission:menu-trf-lab'])->group(function () 
    {
        Route::get('','LabFabric\FabricTestingController@index')->name('lab_fabric.index');
        Route::get('/get-data','LabFabric\FabricTestingController@getDatatrf')->name('lab_fabric.getDatatrf');
        Route::get('barcode/{id}', 'LabFabric\FabricTestingController@barcode')->name('lab_fabric.barcode');
        Route::get('print_detail/{id}', 'LabFabric\FabricTestingController@print_detail')->name('lab_fabric.print_detail');
        Route::post('store', 'LabFabric\FabricTestingController@store')->name('lab_fabric.store');
        Route::get('getCategory', 'LabFabric\FabricTestingController@getCategory')->name('lab_fabric.getCategory');
        Route::get('getTypeSpecimen', 'LabFabric\FabricTestingController@getTypeSpecimen')->name('lab_fabric.getTypeSpecimen');
        Route::get('getPoreference', 'LabFabric\FabricTestingController@getPoreference')->name('lab_fabric.getPoreference');
        Route::get('getStyle', 'LabFabric\FabricTestingController@getStyle')->name('lab_fabric.getStyle');
        Route::get('getTestingMethod', 'LabFabric\FabricTestingController@getTestingMethod')->name('lab_fabric.getTestingMethod');
        Route::get('getSize', 'LabFabric\FabricTestingController@getSize')->name('lab_fabric.getSize');
        Route::get('getRequirements', 'LabFabric\FabricTestingController@getRequirements')->name('lab_fabric.getRequirements');
    });
//END OF LAB TESTING
// BUNDLE REMOVAL
Route::prefix('/bundle-removal')->middleware(['permission:ticket-bundle-removal'])->group(function(){
    Route::get('', 'BundleRemoval\BundleremovalController@index')->name('bundle_removal.index');
    Route::post('scan-barcode', 'BundleRemoval\BundleremovalController@scanBarcode')->name('bundle_removal.scanBarcode');
    Route::post('add-parameter', 'BundleRemoval\BundleremovalController@addParameter')->name('bundle_removal.addParameter');
    Route::get('data-po-buyer', 'BundleRemoval\BundleremovalController@ajaxGetDataPobuyer')->name('bundle_removal.ajaxGetDataPobuyer');
    Route::get('data-po-size', 'BundleRemoval\BundleremovalController@ajaxGetDataSize')->name('bundle_removal.ajaxGetDataSize');
    Route::get('data-po-style', 'BundleRemoval\BundleremovalController@ajaxGetDataStyle')->name('bundle_removal.ajaxGetDataStyle');
    Route::get('data-po-cut', 'BundleRemoval\BundleremovalController@ajaxGetDataCut')->name('bundle_removal.ajaxGetDataCut');
    Route::get('data-po-article', 'BundleRemoval\BundleremovalController@ajaxGetDataArticle')->name('bundle_removal.ajaxGetDataArticle');
    Route::get('data-po-partname', 'BundleRemoval\BundleremovalController@ajaxGetDataPartname')->name('bundle_removal.ajaxGetDataPartname');
    Route::get('data-ticket', 'BundleRemoval\BundleremovalController@dataTicket')->name('bundle_removal.dataTicket');
    Route::post('/delete-ticket', 'BundleRemoval\BundleremovalController@deleteTicket')->name('bundle_removal.deleteTicket');
    Route::get('/getdata-ticket', 'BundleRemoval\BundleremovalController@getDataTicket')->name('bundle_removal.getDataTicket');
    Route::get('/cancel-delete', 'BundleRemoval\BundleremovalController@cancelDelete')->name('bundle_removal.cancelDelete');
    Route::get('/delete-ticketsds', 'BundleRemoval\BundleremovalController@deleteTicketSds')->name('bundle_removal.deleteTicketSds');
    Route::get('/delete-rowdetail/{id}', 'BundleRemoval\BundleremovalController@deleteRowDetail')->name('bundle_removal.deleteRowDetail');
});
//ENDS OF BUNDLE REMOVAL

// BUNDLE RESTORE
Route::prefix('/bundle-restore')->middleware(['permission:ticket-bundle-restore'])->group(function(){
    Route::get('', 'BundleRestore\BundlerestoreController@index')->name('bundle_restore.index');
    Route::post('scan-barcode', 'BundleRestore\BundlerestoreController@scanBarcode')->name('bundle_restore.scanBarcode');
    Route::get('data-ticket', 'BundleRestore\BundlerestoreController@dataTicket')->name('bundle_restore.dataTicket');
    Route::post('/restore-ticket', 'BundleRestore\BundlerestoreController@restoreTicket')->name('bundle_restore.restoreTicket');
});
//ENDS OF BUNDLE REMOVAL
// request fabric submit
Route::prefix('/request-fabric-submit')->middleware(['permission:menu-permintaan-fabric-submit'])->group(function(){
    Route::get('', 'RequestFabric\PermintaanFabricSubmitController@index')->name('reqFabricSubmit.index');
    Route::post('/add-queue', 'RequestFabric\PermintaanFabricSubmitController@addQueue')->name('reqFabricSubmit.addQueue');
    // Route::post('/edit-queue', 'RequestFabric\PermintaanFabricSubmitController@editQueue')->name('reqFabricSubmit.editQueue');

    // Route::get('edit/{id}', 'ProductionController@edit')->name('production.edit');
    Route::get('data-planning', 'RequestFabric\PermintaanFabricSubmitController@dataPlanning')->name('reqFabricSubmit.dataPlanning');
    Route::get('detail-queue/{id}', 'RequestFabric\PermintaanFabricSubmitController@detailQueue')->name('reqFabricSubmit.detailQueue');
    Route::get('data-detail', 'RequestFabric\PermintaanFabricSubmitController@dataDetail')->name('reqFabricSubmit.dataDetail');
    // Route::post('submit-request/{id}', 'RequestFabric\PermintaanFabricSubmitController@submitRequest')->name('reqFabricSubmit.submitRequest');
    Route::delete('delete-queue/{id}', 'RequestFabric\PermintaanFabricSubmitController@deleteQueue')->name('reqFabricSubmit.deleteQueue');
});
// END request fabric submit

// REPLACEMENT BUNDLE
Route::prefix('/replace-bundle')->middleware(['permission:replacement-bundle'])->group(function(){
    Route::get('', 'Cutting\BarcodeReplacementController@index')->name('replacement.index');
    Route::get('/getDataDetailbundle', 'Cutting\BarcodeReplacementController@getDataDetailbundle')->name('replacement.getDataDetailbundle');
    Route::post('/process', 'Cutting\BarcodeReplacementController@process')->name('replacement.process');
    Route::get('/print-pdf', 'Cutting\BarcodeReplacementController@printPdf')->name('replacement.printPdf');
});
// END REPLACEMENT BUNDLE

// Route::prefix('/rack-distribusi')->middleware(['permission:rack-distribusi'])->group(function(){
//     Route::get('/', 'RackDistribusi\RackDistribusiController@index')->name('rack_distribusi.index');
//     Route::get('/data-rack', 'RackDistribusi\RackDistribusiController@dataRackDist')->name('rack_distribusi.dataRackDist');
//     Route::get('/check-rack', 'RackDistribusi\RackDistribusiController@checkRack')->name('rack_distribusi.checkRack');
//     Route::get('/check-rack-all', 'RackDistribusi\RackDistribusiController@checkRackAll')->name('rack_distribusi.checkRackAll');
// });

// DATA SEWING
Route::prefix('/data-sewing')->middleware(['permission:data-mo-sewing'])->group(function(){
    Route::get('', 'Sewing\DataSewingController@index')->name('data_sewing.index');
    Route::get('/get-data', 'Sewing\DataSewingController@getDataMoSewing')->name('data_sewing.getDataMoSewing');
    Route::post('update-mo', 'Sewing\DataSewingController@updateMosewing')->name('data_sewing.updateMosewing');
    Route::post('update-mo3', 'Sewing\DataSewingController@updateMosewing3')->name('data_sewing.updateMosewing3');
    Route::post('update-mo5', 'Sewing\DataSewingController@updateMosewing5')->name('data_sewing.updateMosewing5');
    Route::get('/detail-line/{line_id}', 'Sewing\DataSewingController@detailLine')->name('data_sewing.detailLine');
    Route::get('get-data-detail', 'Sewing\DataSewingController@getDataDetail')->name('data_sewing.getDataDetail');
});
// END DATA SEWING

// test
Route::get('/update-data', function() {
    $data = \DB::table('cutting_plans2')->whereNull('deleted_at')->get();
    foreach($data as $aa) {
        \DB::table('data_cuttings')->where('cutting_date', $aa->cutting_date)->where('queu', $aa->queu)->where('warehouse', $aa->factory_id)->update([
            'plan_id' => $aa->id
        ]);
    }
    dd('berhasil!');
});
