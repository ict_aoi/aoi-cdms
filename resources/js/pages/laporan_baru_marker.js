$(document).ready( function () {
		
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/laporan-baru-marker/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
                factory: $("input[name='factory']:checked").val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'queu', sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'season', name: 'season',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'poreference', name: 'poreference',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:false,orderable:false},
            {data: 'last_updated', name: 'last_updated',searchable:false,orderable:false},
            {data: 'detail', name: 'detail',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();
        var factory = $("input[name='factory']:checked").val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/laporan-baru-marker/data-planning',
            data: {
                cutting_date: cutting_date,
                factory: factory,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#postData').on('click', function() {
        var data = Array();
        $('#detailDetailTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    data[i][ii] = $(this).find('input').val();
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });
        
        $.ajax({
            type: "post",
            url: '/laporan-baru-marker/post-data-marker',
            data: {
                data: data,
                planning_id: $('#planning_id_detail').val()
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.id_plan);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                $("#alert_success").trigger("click", 'Data Berhasil Diperbarui');
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    $('#reset_all').on('click', function() {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();
        var data = Array();
        $('#detailDetailTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    data[i][ii] = $(this).find('input').val();
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });

        $('#detailDetailModal').modal('hide');

        bootbox.confirm("Apakah anda yakin akan me-reset data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "post",
                    url: '/laporan-baru-marker/reset-data-marker',
                    data: {
                        data: data,
                        planning_id: planning_id,
                        part_no: part_no,
                        color: color,
                    },
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function (response) {
                        $.unblockUI();
                        $('#planning_id_detail').val(response.planning_id);
                        $('#part_no_detail').val(response.part_no);
                        $('#color_detail').val(response.color);
                        $('#detailDetailModal').modal();
                        get_data_over_consum_table();
                        get_data_modal_detail_table();
                        get_data_lebar_fb_table();
                        get_data_size_balance_table();
                    },
                    error: function (response) {
                        $.unblockUI();
                        console.log(response);
                    }
                });
            } else {
                $.unblockUI();
                $('#planning_id_detail').val(planning_id);
                $('#part_no_detail').val(part_no);
                $('#color_detail').val(color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
        //$('#file_marker').val(null);
    });

    $('#detailDetailModal').on('hidden.bs.modal', function(){
        var planning_id_detail = $('#planning_id_detail').val();
        var part_no_detail = $('#part_no_detail').val();
        removePageAccess(planning_id_detail, part_no_detail);
        $('#form_filter_date').submit();
        $('#planning_id').val(planning_id_detail);
        $('#detailModal').modal();
        get_data_modal_table()
    });

    var actual_marker = '#actual_marker';

    $('#detailDetailTable').on('change', actual_marker, function(event) {
        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $actual_marker = $row.find(actual_marker).val();
        var $alokasi_fb = $row.find('.alokasi_fb').text();
        var $over_consum = $alokasi_fb - $actual_marker;
        $row.find('#over_consum').val($over_consum);
        if ($over_consum >= 0) {
            $row.find('#over_consum').addClass('bg-success-300');
        } else {
            $row.find('#over_consum').addClass('bg-danger-300');
        }
    });

    var layer = '#layer';

    $('#detailDetailTable').on('change', layer, function(event) {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $layer = $row.find(layer).val();
        var $id = $row.find('#id_marker').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/update-layer',
            data: {
                id: $id,
                layer: $layer,
                planning_id: planning_id,
                part_no: part_no,
                color: color
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    var fabric_width = '#fabric_width';

    $('#detailDetailTable').on('change', fabric_width, function(event) {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $fabric_width = $row.find(fabric_width).val();
        var $id = $row.find('#id_marker').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/update-fabric-width',
            data: {
                id: $id,
                fabric_width: $fabric_width,
                planning_id: planning_id,
                part_no: part_no,
                color: color
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    var perimeter = '#perimeter';

    $('#detailDetailTable').on('blur', perimeter, function(event){
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var index = $row.index();
        var $perimeter = $row.find(perimeter).val().replace(/,/g, '.');
        var $perimeter_data = $perimeter.split(' ');
        var $table = $this.closest('tbody');
        var data_array = Array();
        var increment = 0;
        
        $('#detailDetailTable tbody tr').each(function(i, v) {
            if(i >= index && i < index + $perimeter_data.length) {
                $(this).find('#perimeter').val($perimeter_data[increment]);
                data_array[increment] = Array();
                data_array[increment][0] = $(this).find('#id_marker').val();
                data_array[increment][1] = $(this).find('#perimeter').val();
                increment++;
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/update-perimeter',
            data: {
                planning_id: planning_id,
                part_no: part_no,
                color: color,
                data_array: data_array
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    var marker_length = '#marker_length';

    $('#detailDetailTable').on('blur', marker_length, function(event) {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var index = $row.index();
        var $marker_length = $row.find(marker_length).val().replace(/,/g, '.');
        var $marker_length_data = $marker_length.split(' ');
        var $table = $this.closest('tbody');
        var data_array = Array();
        var increment = 0;

        $('#detailDetailTable tbody tr').each(function(i, v) {
            if(i >= index && i < index + $marker_length_data.length) {
                $(this).find('#marker_length').val($marker_length_data[increment]);
                data_array[increment] = Array();
                data_array[increment][0] = $(this).find('#id_marker').val();
                data_array[increment][1] = $(this).find('#marker_length').val();
                increment++;
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/update-marker-length',
            data: {
                planning_id: planning_id,
                part_no: part_no,
                color: color,
                data_array: data_array
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    var rasio = '#rasio';

    $('#detailDetailTable').on('change', rasio, function(event) {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        var $this = $(event.currentTarget);
        var $row = $this.closest('tr');
        var $rasio = $row.find(rasio).val().replace(/\s/g, '');
        var $id = $row.find('#id_marker').val();

        var data_rasio = $rasio.split(',');

        console.log(data_rasio);

        var i;

        for(i = 0; i < data_rasio.length; i++) {
            if(data_rasio[i] != '') {
                if(!validateRasio(data_rasio[i])) {
                    $("#alert_info").trigger("click", 'Format rasio tidak valid.');
                    return false;
                }
            }
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/update-rasio',
            data: {
                id: $id,
                data_rasio: data_rasio,
                planning_id: planning_id,
                part_no: part_no,
                color: color
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    $('#detailDetailTable').on('click', '#add_row', function() {
        var id = $(this).data('id');
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/add-row',
            data: {
                id: id,
                planning_id: planning_id,
                part_no: part_no,
                color: color
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    $('#detailDetailTable').on('click', '#delete_row', function() {
        var id = $(this).data('id');
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var color = $('#color_detail').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/delete-row',
            data: {
                id: id,
                planning_id: planning_id,
                part_no: part_no,
                color: color
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#planning_id_detail').val(response.planning_id);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#detailDetailModal').modal();
                get_data_over_consum_table();                
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });

    $('#btn-release').on('click', function() {
        $('#btn-release-submit').removeClass('hidden');
        $('#remark-release').removeClass('hidden');
        $('#btn-release-cancel').removeClass('hidden');
        $('#btn-release').addClass('hidden');
        $('#btn-close-modal').addClass('hidden');
        $('#reset_all').addClass('hidden');
    });

    $('#btn-release-cancel').on('click', function() {
        $('#btn-release-submit').addClass('hidden');
        $('#remark-release').addClass('hidden');
        $('#btn-release-cancel').addClass('hidden');
        $('#btn-release').removeClass('hidden');
        $('#btn-close-modal').removeClass('hidden');
        $('#reset_all').removeClass('hidden');
    });

    $('#btn-release-submit').on('click', function() {
        var planning_id = $('#planning_id_detail').val();
        var part_no = $('#part_no_detail').val();
        var remark = $('#remark-release').val();

        var data = Array();
        $('#overConsumTable tbody tr').each(function(i, v) {
            data[i] = Array();
            $(this).children('td').each(function(ii, vv) {
                if ($(this).find('input').length > 0) {
                    data[i][ii] = $(this).find('input').val();
                } else {
                    data[i][ii] = $(this).text();
                }
            });
        });

        var qty_csi = data[0][0];
        var qty_need = data[0][1];
        var overconsum = data[0][2];

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: 'laporan-baru-marker/release-marker',
            data: {
                planning_id: planning_id,
                part_no: part_no,
                remark: remark,
                overconsum: overconsum,
                qty_csi: qty_csi,
                qty_need: qty_need,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                $('#btn-release-submit').addClass('hidden');
                $('#remark-release').addClass('hidden');
                $('#btn-release-cancel').addClass('hidden');
                $('#btn-release').removeClass('hidden');
                $('#btn-close-modal').removeClass('hidden');
                $('#reset_all').removeClass('hidden');
                if(response.status == 200) {
                    $("#alert_success").trigger("click", 'Marker berhasil direlease!');
                } else {
                    $("#alert_warning").trigger("click", response.responseJSON);
                }
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            }
        });
    });
});

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#po_detail_view').text(response.poreference);
            $('#detailModal').modal();
            $('#btn-download-ratio').attr('href', 'laporan-baru-marker/download-ratio/'+response.id);
            if(response.download == true) {
                $('#btn-download-report').removeClass('disabled');
                $('#btn-download-report').attr('href', 'laporan-baru-marker/download-report/'+response.id);
            } else {
                $('#btn-download-report').addClass('disabled');
            }
            get_data_modal_table();
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function detailDetailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            if(response.user == null) {
                $('#planning_id_detail').val(response.id_plan);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#style_detail_detail_view').text(response.style);
                $('#articleno_detail_detail_view').text(response.articleno);
                $('#cutting_date_detail_detail_view').text(response.cutting_date);
                $('#part_no_detail_detail_view').text(response.part_no);
                $('#po_buyer_detail_detail_view').text(response.po_buyer);
                $('#detailModal').modal('toggle');
                $('#detailDetailModal').modal('toggle');
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            } else {
                $('#planning_id').val(response.id_plan);
                $('#style_detail_view').text(response.style);
                $('#articleno_detail_view').text(response.articleno);
                $('#cutting_date_detail_view').text(response.cutting_date);
                $('#detailModal').modal();
                get_data_modal_table();

                $("#alert_warning").trigger("click", 'Halaman ini sedang di edit oleh '+ response.user);
                return false;
            }
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function approve(url,id_plan,part_no) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            id_plan: id_plan,
            part_no: part_no
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id_plan);
            $('#detailModal').modal();
            get_data_modal_table();
            $("#alert_success").trigger("click", 'Data Berhasil Disetujui!');
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function reject(url) {
    console.log('function reject');
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/laporan-baru-marker/data-marker',
            data: {
                planning_id: $('#planning_id').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'part_no', name: 'part_no', searchable:true, visible:true, orderable:false},
            {data: 'total_cutting', name: 'total_cutting', searchable:false, visible:true, orderable:false},
            {data: 'status', name: 'status', searchable:false, visible:true, orderable:false},
            {data: 'detail', name: 'detail', searchable:false, visible:true, orderable:false},
        ],
    });

    return table1;
}

function get_data_modal_detail_table() {
    var table1 = $('#detailDetailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/laporan-baru-marker/data-detail-marker',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: [0],
                className: 'dt_col_hide',
            },
        ],
        columns: [
            {data: 'id_row', name: 'id_row', searchable: false},
            {data: 'cut', name: 'cut', searchable: true, visible:true, orderable:false},
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'rasio', name: 'rasio', searchable:true, visible:true, orderable:false},
            {data: 'layer', name: 'layer', searchable:true, visible:true, orderable:false},
            {data: 'lebar_fb', name: 'lebar_fb', searchable:true, visible:true, orderable:false},
            {data: 'lebar_marker', name: 'lebar_marker', searchable:true, visible:true, orderable:false},
            {data: 'perimeter', name: 'perimeter', searchable:true, visible:true, orderable:false},
            {data: 'actual_marker', name: 'actual_marker', searchable:true, visible:true, orderable:false},	
            {data: 'need_total', name: 'need_total', searchable:true, visible:true, orderable:false},	
            {data: 'action', name: 'action', searchable:true, visible:true, orderable:false},
        ],
    });

    return table1;
}

function get_data_lebar_fb_table() {
    var table2 = $('#lebarFBTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/laporan-baru-marker/data-lebar-fabrik',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'lebar_fb', name: 'lebar_fb', searchable:true, visible:true, orderable:false},
            {data: 'alokasi_fb', name: 'alokasi_fb', searchable:true, visible:true, orderable:false},
            {data: 'need_total', name: 'need_total', searchable:true, visible:true, orderable:false},
        ],
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column(2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column(2).footer() ).html(
                total +' <strong>(Total Kebutuhan)</strong>'
            );
        }
    });

    return table2;
}

function get_data_size_balance_table() {
    var table3 = $('#sizeBalanceTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/laporan-baru-marker/data-size-balance',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center',
            },
        ],
        columns: [
            {data: 'column1', name: 'column1', searchable:false, visible:true, orderable:false},
            {data: 'column2', name: 'column2', searchable:false, visible:true, orderable:false},
            {data: 'column3', name: 'column3', searchable:false, visible:true, orderable:false},
            {data: 'column4', name: 'column4', searchable:false, visible:true, orderable:false},
            {data: 'column5', name: 'column5', searchable:false, visible:true, orderable:false},
            {data: 'column6', name: 'column6', searchable:false, visible:true, orderable:false},
            {data: 'column7', name: 'column7', searchable:false, visible:true, orderable:false},
            {data: 'column8', name: 'column8', searchable:false, visible:true, orderable:false},
            {data: 'column9', name: 'column9', searchable:false, visible:true, orderable:false},
            {data: 'column10', name: 'column10', searchable:false, visible:true, orderable:false},
            {data: 'column11', name: 'column11', searchable:false, visible:true, orderable:false},
            {data: 'column12', name: 'column12', searchable:false, visible:true, orderable:false},
            {data: 'column13', name: 'column13', searchable:false, visible:true, orderable:false},
            {data: 'column14', name: 'column14', searchable:false, visible:true, orderable:false},
            {data: 'column15', name: 'column15', searchable:false, visible:true, orderable:false},
            {data: 'column16', name: 'column16', searchable:false, visible:true, orderable:false},
            {data: 'column17', name: 'column17', searchable:false, visible:true, orderable:false},
        ],
    });

    return table3;
}

function get_data_over_consum_table() {
    var table4 = $('#overConsumTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/laporan-baru-marker/data-over-consum',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData < 0 ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'})
                    }
                }
            },
        ],
        columns: [
            {data: 'qty_csi', name: 'qty_csi', searchable:true, visible:true, orderable:false},
            {data: 'qty_need', name: 'qty_need', searchable:true, visible:true, orderable:false},
            {data: 'over_consum', name: 'over_consum', searchable:true, visible:true, orderable:false},
        ],
    });

    return table4;
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
    return true;
}

function validateRasio(rasio)
{
    var re = /^[A-Z0-9]{1,3}\-\d{1,3}$/;
    return re.test(rasio);
}

function removePageAccess(planning_id, part_no)
{
    $.ajax({
        type: "post",
        url: '/laporan-baru-marker/remove-page-access',
        data: {
            planning_id: planning_id,
            part_no: part_no,
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}