<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

use App\Models\Data\DataCuttingDev;
use App\Models\RequestMarker\FileExcel;
use App\Models\CuttingPlan;
use App\Models\Factory;
use App\Models\DetailPlan;
use App\Http\Controllers\Controller;

class UploadFileSSPController extends Controller
{
    public function index()
    {
        $factorys = Factory::where('deleted_at', null)
            ->where('status', 'internal')
            ->orderBy('factory_alias', 'asc')
            ->get();

        return view('request_marker.upload_file_ssp.index', compact('factorys'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;

            $factory = isset($request->factory) ? $request->factory : \Auth::user()->factory_id;

            if($cutting_date != NULL) {

                if($factory > 0) {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('is_header', true)
                        ->where('factory_id', $factory)
                        ->where('deleted_at', null);

                } else {
                    $data = CuttingPlan::orderBy('queu', 'asc')
                        ->where('cutting_date', $cutting_date)
                        ->where('is_header', true)
                        ->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('articleno', function($data) {
                    if ($data->remark == 'partial') return $data->articleno.' ('.$data->queu.')';
                    else return $data->articleno;
                })
                ->addColumn('po', function($data) {
                    if($data->header_id != null && $data->is_header) {
                        $get_plan_id = CuttingPlan::where('header_id', $data->id)
                            ->whereNull('deleted_at')
                            ->pluck('id')
                            ->toArray();

                        return implode(', ', DetailPlan::whereIn('id_plan', $get_plan_id)->whereNull('deleted_at')->pluck('po_buyer')->toArray());
                    } else {
                        return implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                    }
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Internasional';
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('status', function($data) {
                    if (count($data->file_excels) > 0) return '<span class="badge bg-primary">'.count($data->file_excels).' File</span>';
                    else return '<span class="badge bg-danger">Not Yet</span>';
                })
                ->addColumn('last_update', function($data) {
                    if (count($data->file_excels) > 0) return $data->file_excels->first()->updated_at;
                    else return '-';
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.upload_file_ssp._action', [
                        'model'      => $data,
                        'upload'     => route('UploadFileSSP.uploadFile',$data->id)
                    ]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return null;
                })
                ->editColumn('size_category', function($data) {
                    return null;
                })
                ->addColumn('po', function($data) {
                    return null;
                })
                ->addColumn('action', function($data) {
                    return null;
                })
                ->rawColumns(['action'])
                ->make(true);
            }
        }
    }

    public function dataFile(Request $request)
    {
        if(request()->ajax()) 
        {
            $file_id = $request->file_id;

            if($file_id != NULL) {
                $data = FileExcel::orderBy('created_at', 'desc')
                    ->where('cutting_plan_id', $file_id);

                return datatables()->of($data)
                ->editColumn('user_id', function($data) {
                    return $data->user->name;
                })
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->created_at)->format('d/M/Y H:i');
                })
                ->editColumn('terima_user_id', function($data) {
                    if($data->terima_user_id == null) {
                        return '-';
                    } else {
                        return $data->terima_user->name;
                    }
                })
                ->editColumn('terima_at', function($data) {
                    if($data->terima_at == null) {
                        return '-';
                    } else {
                        return Carbon::parse($data->terima_at)->format('d/M/Y H:i');
                    }
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.upload_file_ssp._action2', [
                        'model'      => $data,
                        'download'     => route('UploadFileSSP.dataFileDownloadView',$data->id),
                        'delete'     => route('UploadFileSSP.dataFileDelete',$data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function uploadFile($id)
    {
        $upload = CuttingPlan::where('id', $id)
            ->where('deleted_at', null)
            ->first();

        $po_buyer = implode(', ', $upload->po_details->pluck('po_buyer')->toArray());
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $upload->cutting_date;
        $obj->style = $upload->style;
        $obj->articleno = $upload->articleno;
        $obj->size_category = $upload->size_category;
        $obj->po_buyer = $po_buyer;
		$obj->url_upload = route('UploadFileSSP.dataFileUpload');
		
		return response()->json($obj,200);
    }

    public function dataFileUpload(Request $request)
    {
        $file = $request->file('file_excel');
        $file_name = $file->getClientOriginalName();
        $tujuan_upload = 'public/files/ssp/'.Carbon::now()->format('Y').'/'.Carbon::now()->format('m').'/'.Carbon::now()->format('d');
        
        $path = $file->storeAs($tujuan_upload, $file_name);

        $upload_file = FileExcel::FirstOrCreate([
            'cutting_plan_id' => $request->upload_file_id,
            'file_name' => $file_name,
            'file_link' => $path,
            'user_id' => \Auth::user()->id,
        ]);
        return response()->json($upload_file, 200);
    }

    public function dataFileDownloadView($id)
    {
        $data = FileExcel::where('id', $id)->first();
        return response()->json($data->id, 200);
    }

    public function dataFileDownload($id)
    {
        $data = FileExcel::where('id', $id)->first();
        $path = storage_path('app/'.$data->file_link);
        return response()->download($path);
    }

    public function dataFileDelete($id)
    {
        $data = FileExcel::where('id', $id)->first();
        Storage::disk('local')->delete($data->file_link);     
        $data->delete();
        return response()->json(200);
    }
}
