@extends('layouts.app', ['active' => 'cuttingBarcode'])

@section('page-css')
<style>
    .margin-kanan {
        margin-right: 5px;
    }
    .btn-size1{
        width:133px;
        padding:10px;
        margin-right:0px;
        margin-bottom:0px;
        margin-top:10px;
        margin-left:4px;
    }
    .btn-size2{
        width:120px;
        padding-left:3px;
        padding-right:3px;
        padding-top:10px;
        padding-bottom:10px;
        margin-right:0px;
        margin-bottom:0px;
        margin-top:10px;
        margin-left:4px;
    }
</style>
@endsection    

@section('page-header')
<div class="page-header">
    {{--  <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting Generate Barcode</span></h4>
        </div>
    </div>  --}}
    <br>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Generate Barcode</li>
        </ul>
    </div>
</div>
<br>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <form action="{{ route('cuttingbarcode.ajaxSetMarker') }}" id="form-marker">
            @csrf
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_marker" name="barcodeMarker" placeholder="#Scan Marker"></input>
                    <input type="hidden" id="_barcodeid"></input>
                    <input type="hidden" id="_status"></input>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-xs btn-primary add_new legitRipple"><span class="icon-search4"></span></button>
                </div>
            </div>
            <br>
        </form>
    </div>
</div>
<div class="panel panel-flat">
    <form action="{{ route('cuttingbarcode.generateBarcode') }}" id="form-marker-generate">
        @csrf
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="barcode_id">Barcode Marker:</label>
                            <input type="text" id="barcode_id" name="barcode_id" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cut_number">Cut Number:</label>
                            <input type="text" id="cut_number" name="cut_number" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="layer">Layer:</label>
                            <input type="text" id="layer" name="layer" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 hidden">
                        <div class="form-group">
                            <label for="cutter">Cutter:</label>
                            <input type="text" id="cutter" name="cutter" class="form-control" value="-">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="max_bundle">Max Qty/Bundle:</label>
                            <input type="text" id="max_bundle" name="max_bundle" class="form-control text-right" onkeypress="return isNumberKey(event);">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="total_bundle">total max bundle to Box</label>
                                <input type="number" id="total_bundle" name="total_bundle" value="1" class="form-control text-right" onkeypress="return isNumberKey(event);">
                            </div>
                            <div class="col-sm-6">
                                <label for="box_lim">Max Qty/Box:</label>
                                <input type="text" id="box_lim" name="box_lim" class="form-control text-right" onkeypress="return isNumberKey(event);" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="note">Note:</label>
                            <input type="text" id="note" name="note" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="style">Style:</label>
                            <input type="text" name="style" id="style" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-md-4 hidden">
                        <div class="form-group">
                            <label for="id_plan">ID Plan</label>
                            <input type="text" name="id_plan" id="id_plan" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="checklist_komponen_break">Checklist komponen per page</label>
                            <input type="text" name="checklist_komponen_break" id="checklist_komponen_break" class="form-control text-right" value="{{$factories->checklist_komponen_break}}" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="komponen_on_bundle">Checklist komponen di page bundle</label>
                            <input type="text" name="komponen_on_bundle" id="komponen_on_bundle" class="form-control text-right" value="{{$factories->komponen_on_bundle}}" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group" style="margin-bottom:0px;margin-top:20px">
                            <label for="split_size">Select Size</label>
                            <select name="split_size[]" id="split_size" class="form-control select-search" multiple data-placeholder="Please Select Size">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-5">
                        <div class="form-group" style="width:400px">
                            <label class="radio-inline"><input type="radio" name="radio_status" checked="checked" value="a3">Paper A3</label>
                            <label class="radio-inline"><input type="radio" name="radio_status" value="a4">Paper A4</label>
                            @if(\Auth::user()->factory_id == 1)
                            <label class="radio-inline"><input type="radio" name="radio_status" value="a3_kanban">Paper A3 Kanban</label>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <button type="submit" id="btnGenerate" class="btn btn-primary pull-right">Generate <i class="icon-barcode2 position-right"></i></button>
                            <button type="button" id="btnPrint" class="btn btn-warning pull-right margin-kanan">RePrint <i class="icon-printer2 position-right"></i></button>
                            <button type="button" id="btnSinkronSDS" class="btn btn-success pull-right margin-kanan">Sinkron <i class="icon-sync position-right"></i></button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-5">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <button type="button" id="btnViewPO" class="btn btn-size1 bg-grey-300 pull-right">View List PO<i class="icon-eye2 position-right"></i></button>
                            {{-- <button type="button" id="migrateSDS" class="btn btn-size2 bg-teal-600 pull-right">Migrate SDS<i class="icon-git-compare position-right"></i></button> --}}
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>

<a href="{{route('cuttingbarcode.ajaxGetSize')}}" id="get_size"></a>
<a href="{{route('cuttingbarcode.ajaxSetMarker')}}" id="set-marker"></a>
<a href="{{route('cuttingbarcode.ajaxGetDataMarker')}}" id="get_data_marker"></a>
<a href="{{ route('cuttingbarcode.updateKomponenBreak') }}" id="updateKomponenBreak"></a>
<a href="{{ route('cuttingbarcode.sinkronSDS') }}" id="sinkronSDS"></a>
@endsection

@section('page-modal')
    @include('cutting.barcode._index_modal')
@endsection

@section('page-js')
<script type="text/javascript">
    $(document).ready(function() {
        $('#barcode_marker').focus();

        $('#total_bundle').on('change', function(){
            var max_bundle = $('#max_bundle').val();

            if($(this).val() <= 0){
                $(this).val(1);
                return;
            }

            if( max_bundle != ''){
                $('#box_lim').val($(this).val()*max_bundle);
            }
        });

        $('#max_bundle').on('blur', function(){
            $('#total_bundle').trigger('change');
        });
    
        //scan marker
        $('.input-new').keypress(function(event) {
            if(event.which == 13) {
                $('#split_size option').remove();
                event.preventDefault();
                var barcodeid = $('#barcode_marker').val();
    
                $('#_barcodeid').val(barcodeid);
    
                //check apakah search bar sudah diisi atau belum
                if($('#barcode_marker').val() == '') {
                    $('#barcode_marker').val('');
                    $('#barcode_marker').focus();
                    $('#_barcodeid').val('');
                    $('#_status').val('');
                    return false;
                }
    
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: $('#set-marker').attr('href'),
                    data: {barcode_id: barcodeid, status: status},
                    beforeSend: function() {
                        $('#barcode_id').val(barcodeid);
                        $('#barcode_marker').val('');
                        $('#barcode_marker').focus();
                        $('#_barcodeid').val('');
                        $('#_status').val('');
                        $('#cut_number').val('');
                        $('#layer').val('');
                        //$('#spStyle').text('');
                        $('#style').val('');
                        $('#id_plan').val('');
                    },
                    success: function(response){
                        $('#cut_number').val(response.marker.cut);
                        $('#layer').val(response.marker.layer);
                        $('#style').val(response.marker.style);
                        $('#id_plan').val(response.marker.id_plan);
                        $('#max_bundle').focus();
                        if(response.check_header.length > 0){
                            $.each(response.check_header, function(i,v){
                                $('#split_size').append('<option value='+v+'>'+v+'</option>');
                            });
                        }
                    },
                    error: function(response){
                        if(response['status'] == 422) {
                            myalert('error', response['responseJSON']);
                        }
                        clear();
                    }
                });
            }
        });
    
        //choose marker
        $('.add_new').on('click',function(){
            $('#modal_detail_').modal('show');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url: $('#get_data_marker').attr('href'),
                beforeSend: function() {
                    $('#modal_detail_ > .modal-content').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response){
                    $('#modal_detail_ > .modal-content').unblock();
                    $('#list').html(response);
                }
            });
        });
    
        //
        $('#list').on('click','#choose-marker', function(){
            var barcodeid = $(this).data('barcode');
            var cutnumber = $(this).data('cut');
            var layer = $(this).data('layer');
            $('#modal_detail_').modal('hide');
            $('#barcode_marker').val(barcodeid);
            $('#cut_number').val(cutnumber);
            $('#layer').val(layer);
            var e = jQuery.Event("keypress");
            e.which = 13; // # Some key code value
            $(".input-new").trigger(e);
        });

        //verify
        $('#form-marker-generate').submit(function(event) {
            event.preventDefault();

            var barcode_id = $('#barcode_id').val();
            var cut_number = $('#cut_number').val();
            var layer = $('#layer').val();
            var cutter = $('#cutter').val();
            var max_bundle = $('#max_bundle').val();
            var box_lim = $('#box_lim').val();
            //$('#note').val();
            var style = $('#style').val();

            if(barcode_id == '' || cut_number == '' || cutter == '' || max_bundle == ''){
                myalert('error', 'Opps, something error..! please check your input..!');
                return false;
            }
            
            if(parseInt(max_bundle) <= 0){
                myalert('error', 'Max bundle invalid..!');
                return false;
            }

            if(box_lim != ''){
                if(parseInt(box_lim) < parseInt(max_bundle)){
                    myalert('error', 'Max Box invalid..!');
                    return false;
                }
            }

            //
            var formData = new FormData($(this)[0]);
            var parameter = ['barcode_id', 'cut_number', 'layer', 'cutter', 'max_bundle', 'box_lim', 'note', 'style', 'id_plan'];
            var val = [
                        $('#barcode_id').val(),
                        $('#cut_number').val(),
                        $('#layer').val(),
                        $('#cutter').val(),
                        $('#max_bundle').val(),
                        $('#box_lim').val(),
                        $('#note').val(),
                        $('#style').val(),
                        $('#id_plan').val()
            ]

            for(var i = 0; i < parameter.length; i++) {
                formData.append(parameter[i], val[i]);
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url : $('#form-marker-generate').attr('action'),
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#form-marker-generate').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Generating Barcode</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('#form-marker-generate').unblock();
                    myalert('success', 'Good..!');
                  //  window.open("cutting-barcode/print-preview/"+barcode_id+"?paper="+$('input[name="radio_status"]:checked').val(), '_blank');
                    clear();
                },
                error: function(response) {
                    $('#form-marker-generate').unblock();
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                    clear();
                }
            });
        });


        $('#btnPrint').on('click', function(){
            var barcode_id = $('#barcode_id').val();
            var split_size = $('#split_size').val() == null ? 0 : $('#split_size').val();
            var paper = $('input[name="radio_status"]:checked').val();
            var url_print = "cutting-barcode/print-preview/"+barcode_id+"?paper="+paper;
            if (split_size != 0) {
                url_print = "cutting-barcode/print-preview/"+barcode_id+"?paper="+paper+"&size="+split_size;
            }

            if(barcode_id == ''){
                myalert('error', 'Barcode marker not found..!');
            }else{
                window.open(url_print, '_blank');
            }
        });

        //NEW MODAL BLADE FOR EDITING RATIO DETAIL
        $('#btnViewPO').on('click', function(){
            var barcode_id = $('#barcode_id').val();
            var style = $('#style').val();
            var paper = $('input[name="radio_status"]:checked').val();

            if(barcode_id == ''){
                myalert('error', 'Barcode marker not found..!');
            }else{
                //window.open(url_print, '_blank');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('cuttingbarcode.getPoSize')}}",
                    type: "GET",
                    data: {
                        "barcode_id": barcode_id,
                        "style": style
                    },
                    beforeSend: function(){
                        $('#modal_detail_ > .modal-content').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Load Data..</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    complete: function() {
                        $('#modal_detail_').modal('show');
                    },
                    success: function(response){
                        console.log(response);
                        $('#modal_detail_ > .modal-content').unblock();
                        $('#title-line-number').text('List PO Size');
                        $('#list').html(response);
                        // clear();
                    },
                    error: function(response) {
                        $('#modal_detail_ > .modal-content').unblock();
                        myalert('error', response['responseJSON']);
                    }
                });

            }
        });

        $('#migrateSDS').on('click', function(){
            var barcode_id = $('#barcode_id').val();

            if(barcode_id == ''){
                myalert('error', 'Barcode marker not found..!');
            }else{
                //window.open(url_print, '_blank');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('cuttingbarcode.migrateSDS')}}",
                    type: "GET",
                    data: {
                        "barcode_id": barcode_id
                    },
                    beforeSend: function(){
                        $('#form-marker-generate').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Migrating Data..</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response){
                        $('#form-marker-generate').unblock();
                        myalert('success','MIGRATE SUCCESS!');
                        clear();
                    },
                    error: function(response) {
                        $('#form-marker-generate').unblock();
                        myalert('error', response['responseJSON']);
                        clear();
                    }
                });

            }
        });

        $(document).on('blur', '.qty_unfilled', function() {
            var id = $(this).data('id');
            var qty = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-qty-ratio',
                data: {id:id, qty:qty},
                success: function(response){
                    $('#qty_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });
        $(document).on('blur', '.cutinfo_unfilled', function() {
            var id = $(this).data('id');
            var cut_info = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-cutinfo-ratio',
                data: {id:id, cut_info:cut_info},
                success: function(response){
                    $('#cutinfo_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });
        // update po
        $(document).on('blur', '.po_unfilled', function() {
            var id = $(this).data('id');
            var po_buyer = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: 'cutting-barcode/update-po-ratio',
                data: {id:id, po_buyer:po_buyer},
                success: function(response){
                    //$('#po_' + id).html(response);
                },
                error: function(response) {
                    console.log(response);
                    myalert('error', response['responseText']);
                }
            });
        });

        // update checklist komponen break
        $('#checklist_komponen_break').on('blur', function(){
            var checklist_komponen_break = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#updateKomponenBreak').attr('href'),
                data: {
                    checklist_komponen_break:checklist_komponen_break,
                    action: 'komponen_break'
                },
                success: function(response){
                    $('#checklist_komponen_break').val(response);

                },
                error: function(response) {
                    return false;
                }
            });
        })

        // update komponen on bundle
        $('#komponen_on_bundle').on('blur', function(){
            var komponen_on_bundle = $(this).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#updateKomponenBreak').attr('href'),
                data: {
                    komponen_on_bundle:komponen_on_bundle,
                    action: 'komponen_bundle'
                },
                success: function(response){
                    $('#komponen_on_bundle').val(response);

                },
                error: function(response) {
                    return false;
                }
            });
        });

        // sinkron barcode sds
        $('#btnSinkronSDS').on('click', function() {
            var barcode_id = $('#barcode_id').val();
            var cut_number = $('#cut_number').val();
            var layer = $('#layer').val();

            if(barcode_id == '' || cut_number == '' || layer == ''){
                myalert('error', 'Opps, something error..! please check your input..!');
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url : $('#sinkronSDS').attr('href'),
                data: {
                    barcode_id: barcode_id,
                    cut_number: cut_number,
                    layer: layer,
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait',
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent',
                        },
                        theme: true,
                        baseZ: 2000
                    });
                },
                success: function(response) {
                    $.unblockUI();
                    myalert('success', 'Good..!');
                },
                error: function(response) {
                    $.unblockUI();
                    if(response['status'] == 422) {
                        myalert('error', response['responseJSON']);
                    }
                }
            });
        });
    });

    function clear(){
        $('#barcode_id').val('');
        $('#barcode_marker').val('');
        $('#barcode_marker').focus();
        $('#_barcodeid').val('');
        $('#_status').val('');
        $('#cut_number').val('');
        $('#layer').val('');
        $('#style').val('');
        $('#id_plan').val('');
        $('#cutter').val('-');
        $('#max_bundle').val('');
        $('#box_lim').val('');
        $('#note').val('');
        $('#total_bundle').val(1);
        $('#split_size option').remove();
    }
</script>
@endsection