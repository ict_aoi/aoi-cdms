<?php

namespace App\Http\Controllers\Cutting;

use DB;
use App\Models\User;
use App\Models\CuttingMarker;
use App\Models\ScanCutting;
use App\Models\ScanCuttingOperator;
use App\Models\Spreading\FabricUsed;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CuttingResultController extends Controller
{
    public function index()
    {
        return view('cutting.cutting_result.index');
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id > 0) {
                    $data = ScanCutting::orderBy('start_time', 'desc')->whereRaw('start_time::date = ?', [$cutting_date])->where('factory_id', \Auth::user()->factory_id)->where('deleted_at', null);
                } else {
                    $data = ScanCutting::orderBy('start_time', 'desc')->whereRaw('start_time::date = ?', [$cutting_date])->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->editColumn('end_time', function($data) {
                    if ($data->end_time == null) return 'Masih Cutting';
                    else return $data->end_time;
                })
                ->addColumn('detail', function($data) {
                    return $data->marker->cutting_plan->style.' - '.$data->marker->cutting_plan->articleno.' - '.$data->marker->cutting_plan->size_category;
                })
                ->addColumn('operator', function($data) {
                    return implode(', ', $data->operator->pluck('name')->toArray());
                })
                ->addColumn('status', function($data) {
                    if ($data->state == 'done') return '<span class="badge bg-primary">'.$data->state.'</span>';
                    else return '<span class="badge bg-danger">'.$data->state.'</span>';
                })
                ->rawColumns(['status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }
}
