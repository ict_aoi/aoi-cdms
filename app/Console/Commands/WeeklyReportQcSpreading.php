<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use App\Http\Controllers\Report\SpreadingReportController;

use App\Models\Scheduler;

class WeeklyReportQcSpreading extends Command
{
    protected $signature = 'weeklyReportQcSpreading:insert';
    protected $description = 'weekly report qc spreading';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','WEEKLY_REPORT_QC_SPREADING')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('START GENERATE REPORT AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            SpreadingReportController::weeklyReportQcSpreading(1);
            SpreadingReportController::weeklyReportQcSpreading(2);
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE GENERATE REPORT AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','WEEKLY_REPORT_QC_SPREADING')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job'    => 'WEEKLY_REPORT_QC_SPREADING',
                    'status' => 'ongoing'
                ]);
                $this->info('CHECK GENERATE REPORT AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                SpreadingReportController::weeklyReportQcSpreading(1);
                SpreadingReportController::weeklyReportQcSpreading(2);
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler);
                $this->info('DONE GENERATE REPORT AT '.carbon::now());
            }else{
                $this->info('GENERATE REPORT SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
