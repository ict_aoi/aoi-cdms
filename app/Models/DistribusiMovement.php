<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DistribusiMovement extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'distribusi_movements';
	protected $fillable = ['id','barcode_id', 'locator_from', 'status_from', 'locator_to', 'status_to', 'user_id', 'description', 'ip_address', 'deleted_at', 'loc_dist','bundle_table','factory_id'];

	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

	public function bundle_table()
    {
        return $this->belongsTo('App\Models\BundleTable','bundle_table','id_table');
    }
}
