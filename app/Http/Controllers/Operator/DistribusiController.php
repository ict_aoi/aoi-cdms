<?php

namespace App\Http\Controllers\Operator;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;
use App\Models\User;
use App\Models\Locator;
use App\Models\BundleTable;
use App\Models\MasterPpcm;
use App\Models\MasterSetting;
use App\Models\DistribusiMovement;
use DataTables;
use Auth;

class DistribusiController extends Controller
{

    private $sds_connection;

    public function __construct()
    {
        $this->middleware('auth');
        $this->sds_connection = DB::connection('sds_live');
    }

    public function index()
    {
        User::where('id', \Auth::user()->id)->update(['last_login_at' => Carbon::now()]);
        return view('distribusi.index');
    }

    public function bundleLocator()
    {
        return view('distribusi.bundle_loc');
    }
    public function getDataBundleLoc(Request $request)
    {
        if ($request->ajax()){
            $barcode_input = $request->barcode_input;
            $po_cdms = DB::table('bundle_detail as bd')->select('bh.poreference','bh.cut_num','bh.size','bd.start_no')->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')->where('bd.barcode_id',$barcode_input)->first();
            $po_sds_cdms = DB::table('bundle_detail as bd')->select('bh.poreference','bh.cut_num','bh.size','bd.start_no')->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')->where('bd.sds_barcode',$barcode_input)->first();
            $po_sds_only = DB::table('sds_detail_movements as sdm')->select('shm.poreference','shm.cut_num','shm.size','sdm.start_no')->leftJoin('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')->where('sdm.sds_barcode',$barcode_input)->first();
            
            $x = array_filter([$po_cdms,$po_sds_cdms,$po_sds_only]);
            $zz = array_merge($x);
            $poreference = "'".$zz[0]->poreference."'";
            $cut_num = "'".$zz[0]->cut_num."'";
            $size = "'".$zz[0]->size."'";
            $start_no = "'".$zz[0]->start_no."'";
            $datax = DB::SELECT(DB::RAW("SELECT bd.barcode_id,bd.komponen_name,bh.style,vms.type_name,vms.season_name as season,bh.poreference,bh.article,bh.size,vms.part,bh.cut_num,bd.start_no,bd.end_no,bd.qty,bd.current_description,bd.updated_movement_at,
            u.name AS pic,
            bh.factory_id,
            max(dm.loc_dist) FILTER (where dm.locator_to='11') as loc_setting,
            max(dm.loc_dist) FILTER (where dm.locator_to='16' and dm.status_to = 'out') as loc_supply
            FROM
            bundle_header bh
            JOIN bundle_detail bd ON bd.bundle_header_id = bh.id
            JOIN v_master_style vms ON vms.id = bd.style_detail_id
            JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
            JOIN users u ON u.id = bd.current_user_id 
            WHERE
            bh.poreference=$poreference AND bh.cut_num = $cut_num AND bh.size = $size AND bd.start_no = $start_no
            GROUP BY
            bh.cut_num,bh.style,bh.size,bd.komponen_name,bd.barcode_id,vms.type_name,vms.season_name,bh.poreference,bh.article,vms.part,bd.start_no,bd.end_no,bd.qty,bd.current_description,bd.updated_movement_at,u.name,bh.factory_id
            UNION ALL
            SELECT sdm.sds_barcode AS barcode_id,sdm.komponen_name,shm.style,vms.type_name,vms.season_name as season,shm.poreference,shm.article,shm.size,vms.part,shm.cut_num,sdm.start_no,sdm.end_no,sdm.qty,sdm.current_description,sdm.updated_movement_at,u.name AS pic,shm.factory_id,
            max(dm.loc_dist) FILTER (where dm.locator_to='11') as loc_setting,
            max(dm.loc_dist) FILTER (where dm.locator_to='16' and dm.status_to='out') as loc_supply
            FROM
            sds_header_movements shm
            JOIN sds_detail_movements sdm ON sdm.sds_header_id = shm.id
            JOIN v_master_style vms ON vms.ID = sdm.style_id
            JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
            JOIN users u ON u.id = sdm.current_user_id 
            WHERE
            shm.poreference = $poreference AND shm.cut_num = $cut_num AND shm.size = $size AND sdm.start_no = $start_no
            GROUP BY shm.cut_num,shm.style,shm.size,sdm.komponen_name,sdm.sds_barcode,vms.type_name,vms.season_name,shm.poreference,shm.article,vms.part,sdm.start_no,sdm.end_no,sdm.qty,sdm.current_description,sdm.updated_movement_at,u.name,
            shm.factory_id"));
            
            return DataTables::of($datax)
            ->addColumn('cut_sticker',function ($datax)
            {
                return $datax->cut_num.' | '.$datax->start_no.'-'.$datax->end_no;
            })
            ->addColumn('style',function ($datax)
            {
                if($datax->type_name == 'Non'){
                    $style = $datax->season.'-'.$datax->style;
                }else{
                    $style = $datax->season.'-'.$datax->style.'-'.$datax->type_name;
                }
                return $style;
            })
            ->addColumn('current_description',function ($datax)
            {
                if($datax->current_description == null){
                    $current_description = '-';
                }else{
                    $current_description = $datax->current_description;
                }
                return $current_description;
            })
            ->addColumn('line',function ($datax)
            {
                if($datax->current_description == 'in SETTING'){
                    $line = $datax->loc_setting;
                }elseif($datax->current_description == 'out SUPPLY'){
                    $line = $datax->loc_supply;
                }else{
                    $line =  '-';
                }
                return str_replace('LINE','L.',$line);
            })
            ->addColumn('factory_id',function ($datax)
            {
                if($datax->factory_id == 1){
                    $factory_id = 'AOI 1';
                }else{
                    $factory_id = 'AOI 2';
                }
                return $factory_id;
            })
            ->addColumn('is_supplied',function ($datax)
            {
                if($datax->loc_supply != null){
                    return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                }else{
                    return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                }
            })
            ->addColumn('user_scan',function($datax){
                return strtoupper($datax->pic);
            })
            ->rawColumns(['style','factory_id','user_scan','current_description','is_supplied'])
            ->make(true);
        }else {
            $datax = array();
            return DataTables::of($datax)
            ->make(true);
        }
    }

    public function menu($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();

        // dd($user_role,$locator->role, (in_array($locator->role, $user_role)), $locator);
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.action',compact('locator'));
            }
            else{
                return abort(405);
            }

        }
        else{
            return abort(405);
        }

        // return view('component_movement.scan_component.index', compact('locator_set'));

        // User::where('id', \Auth::user()->id)->update(['last_login_at' => Carbon::now()]);
        // return view('distribusi.index');
    }

    public function checkIn($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();

        // dd($user_role,$locator->role, (in_array($locator->role, $user_role)), !$locator);
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_in',compact('locator','locator_set'));
            }
            else{
                return abort(405);
            }

        }
        else{
            return abort(405);
        }
    }

    public function checkOut($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        if(\Auth::user()->factory_id == 2){
            $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('area_name', 'asc')->get();
        }else{
            $locator_set = MasterSetting::select('alias')->whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('alias', 'asc')->distinct('alias')->get();
        }
        $bundle_table = BundleTable::select('id_table','name')->where('factory_id',\Auth::user()->factory_id)->orderBy('id_table')->get();
        // dd($user_role,$locator->role, (in_array($locator->role, $user_role)), !$locator);
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_out',compact('locator','locator_set','bundle_table'));
            }
            else{
                return abort(405);
            }

        }
        else{
            return abort(405);
        }
    }

    public function checkBundle($id)
    {
        $user_role = \Auth::user()->roles->pluck('id')->toArray();
        $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
        $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();

        // dd($user_role,$locator->role, (in_array($locator->role, $user_role)), !$locator);
        if($locator){
            if (in_array($locator->role, $user_role)){
                return view('distribusi.check_bundle',compact('locator','locator_set'));
            }
            else{
                return abort(405);
            }

        }
        else{
            return abort(405);
        }
    }

    public function bundleInfo(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_input = $request->barcode_input;

            $barcode_cdms = DB::table('bundle_detail')
            ->selectRaw('bundle_detail.barcode_id,bundle_detail.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, bundle_header.style, bundle_header.season, bundle_header.poreference, bundle_header.article, bundle_header.size, bundle_header.cut_num, bundle_detail.start_no, bundle_detail.end_no, bundle_detail.qty,bundle_header.factory_id')
            ->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'bundle_detail.style_detail_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('bundle_detail.barcode_id', $barcode_input)
            ->first();

            $barcode_sds_cdms = DB::table('bundle_detail')
            ->selectRaw('bundle_detail.barcode_id,bundle_detail.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, bundle_header.style, bundle_header.season, bundle_header.poreference, bundle_header.article, bundle_header.size, bundle_header.cut_num, bundle_detail.start_no, bundle_detail.end_no, bundle_detail.qty,bundle_header.factory_id')
            ->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'bundle_detail.style_detail_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('bundle_detail.sds_barcode', $barcode_input)
            ->first();

            $barcode_sds_only = DB::table('sds_detail_movements')
            ->selectRaw('sds_detail_movements.sds_barcode,sds_detail_movements.komponen_name, master_style_detail.part, master_style_detail.process, types.type_name, sds_header_movements.style, sds_header_movements.season, sds_header_movements.poreference, sds_header_movements.article, sds_header_movements.size, sds_header_movements.cut_num, sds_detail_movements.start_no, sds_detail_movements.end_no, sds_detail_movements.qty,sds_header_movements.factory_id')
            ->leftJoin('sds_header_movements', 'sds_header_movements.id', '=', 'sds_detail_movements.sds_header_id')
            ->leftJoin('master_style_detail', 'master_style_detail.id', '=', 'sds_detail_movements.style_id')
            ->leftJoin('types', 'types.id', '=', 'master_style_detail.type_id')
            ->where('sds_detail_movements.sds_barcode', $barcode_input)
            ->first();

            if($barcode_cdms != null) {
                if($barcode_cdms->type_name == 'Non') {
                    $style = $barcode_cdms->style;
                } else {
                    $style = $barcode_cdms->style.$barcode_cdms->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_cdms->process == null || $barcode_cdms->process == '' || $barcode_cdms->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_cdms->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();
                    $process = implode(', ', $process_get);
                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_cdms->barcode_id)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }

                $obj = new StdClass();
                $obj->komponen = $barcode_cdms->part.' - '.$barcode_cdms->komponen_name;
                $obj->style = $style.' ('.$barcode_cdms->season.')';
                $obj->po_buyer = $barcode_cdms->poreference;
                $obj->article = $barcode_cdms->article;
                $obj->size = $barcode_cdms->size;
                $obj->cut_sticker = $barcode_cdms->cut_num.' / '.$barcode_cdms->start_no.' - '.$barcode_cdms->end_no;
                $obj->qty = $barcode_cdms->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_cdms->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms != null && $barcode_sds_only == null){
                if($barcode_sds_cdms->type_name == 'Non') {
                    $style = $barcode_sds_cdms->style;
                } else {
                    $style = $barcode_sds_cdms->style.$barcode_sds_cdms->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_sds_cdms->process == null || $barcode_sds_cdms->process == '' || $barcode_sds_cdms->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_sds_cdms->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();
                    $process = implode(', ', $process_get);
                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_cdms->barcode_id)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_sds_cdms->barcode_id)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }

                $obj = new StdClass();
                $obj->komponen = $barcode_sds_cdms->part.' - '.$barcode_sds_cdms->komponen_name;
                $obj->style = $style.' ('.$barcode_sds_cdms->season.')';
                $obj->po_buyer = $barcode_sds_cdms->poreference;
                $obj->article = $barcode_sds_cdms->article;
                $obj->size = $barcode_sds_cdms->size;
                $obj->cut_sticker = $barcode_sds_cdms->cut_num.' / '.$barcode_sds_cdms->start_no.' - '.$barcode_sds_cdms->end_no;
                $obj->qty = $barcode_sds_cdms->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_sds_cdms->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms == null && $barcode_sds_only != null){
                if($barcode_sds_only->type_name == 'Non') {
                    $style = $barcode_sds_only->style;
                } else {
                    $style = $barcode_sds_only->style.$barcode_sds_only->type_name;
                }
                $tir = [];
                $tor = [];
                if($barcode_sds_only->process == null || $barcode_sds_only->process == '' || $barcode_sds_only->process == ' ') {
                    $process = '-';
                } else {
                    $process_temp = explode(',', $barcode_sds_only->process);
                    $process_get = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('process_name')->toArray();

                    $process = implode(', ', $process_get);

                    $locator_id = DB::table('master_process')->whereIn('id', $process_temp)->whereNull('deleted_at')->pluck('id')->toArray();
                    $trans_in = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_only->sds_barcode)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','in')
                    ->get();

                    $trans_out = DB::table('distribusi_movements')
                    ->select('distribusi_movements.description','master_process.process_name')
                    ->Join('master_process','master_process.locator_id','=','distribusi_movements.locator_to')
                    ->where('distribusi_movements.barcode_id',$barcode_sds_only->sds_barcode)
                    ->whereIn('master_process.id',$locator_id)
                    ->where('distribusi_movements.status_to','out')
                    ->get();

                    foreach($trans_in as $ti){
                        $ti1[] = $ti->process_name;
                        $tir = implode(',',$ti1);
                    }
                    
                    foreach($trans_out as $to){
                        $to2[] = $to->process_name;
                        $tor = implode(',',$to2);
                    }
                }
                
                $cutting_out = DB::table('distribusi_movements')
                ->selectRaw('distribusi_movements.created_at, users.name, users.factory_id')->leftJoin('users', 'distribusi_movements.user_id', '=', 'users.id')
                ->where('distribusi_movements.barcode_id', $barcode_sds_only->sds_barcode)
                ->where('distribusi_movements.locator_to', 10)
                ->where('distribusi_movements.status_to', 'out')
                ->whereNull('distribusi_movements.deleted_at')
                ->first();

                if($cutting_out != null) {
                    $factory = DB::table('master_factory')->where('id', $cutting_out->factory_id)->first();
                    if($factory != null) {
                        $factory = $factory->factory_alias;
                    } else {
                        $factory = '-';
                    }
                    $bundling = $cutting_out->name.' ('.$factory.') ('.$cutting_out->created_at.')';
                } else {
                    $bundling = '-';
                }
                $obj = new StdClass();
                $obj->komponen = $barcode_sds_only->part.' - '.$barcode_sds_only->komponen_name;
                $obj->style = $style.' ('.$barcode_sds_only->season.')';
                $obj->po_buyer = $barcode_sds_only->poreference;
                $obj->article = $barcode_sds_only->article;
                $obj->size = $barcode_sds_only->size;
                $obj->cut_sticker = $barcode_sds_only->cut_num.' / '.$barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no;
                $obj->qty = $barcode_sds_only->qty;
                $obj->process = $process;
                $obj->bundling = $bundling;
                $obj->tir   = $tir;
                $obj->tor   = $tor;
                $obj->factory_id = $barcode_sds_only->factory_id;
                $obj->barcode = $barcode_input;

                return response()->json($obj,200);
            }elseif($barcode_sds_cdms != null && $barcode_sds_only != null){
                return response()->json('Duplikat Detail Barcode!',422);
            }else{
                return response()->json('bundle tidak ditemukan',422);
            }
        }
    }
    // SKEMA RESTORE BY PUSH DEDY 2022-05-27
    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            $locator_id = $request->locator_id;
            $state = $request->state;
            $bundle_table = $request->mj_bundle;
            $line_supply = $request->line_supply;

            if($barcode_id != null && $locator_id != null){
                $barcode_check      = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
                if($barcode_check != null){
                    $barcode_sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_check->sds_barcode)->first();
                    if($barcode_check != null && $barcode_sds_ver != null){
                        return response()->json('DUPLIKAT DETAIL BARCODE SDS!D001',422);
                    }

                    if($locator->is_cutting){
                        $component_movement_out = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();
                        $last_locator = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();

                        if($component_movement_out->count() > 0) {
                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!'.'di Meja -'.$last_locator->bundle_table,422);
                        } else {

                            $bundle_check = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = DB::table('master_komponen')->where('id', $komponen_detail->komponen)->where('deleted_at', null)->first()->komponen_name;

                            try {
                                DB::beginTransaction();

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => null,
                                    'status_from'  => null,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => '-',
                                    'bundle_table' => $bundle_table,
                                    'factory_id'   => \Auth::user()->factory_id
                                ]);

                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $bundle_table
                                ];

                                DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                    'is_out_cutting'        => 'Y',
                                    'out_cutting_at'        => Carbon::now(),
                                    'current_locator_id'    => $locator_id,
                                    'current_status_to'     => $state,
                                    'current_description'   => $state.' '.$locator->locator_name,
                                    'updated_movement_at'   => Carbon::now(),
                                    'current_user_id'       => \Auth::user()->id
                                ]);

                                if($barcode_check->sds_barcode != null) {
                                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                    if($update_sds != null) {
                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                'location' => $convert_locator['locator_sds'],
                                                $convert_locator['column_update'] => Carbon::now(),
                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                'factory' => $convert_locator['factory_sds'],
                                            ]);
                                        }
                                    }
                                }

                                DB::commit();

                            } catch (Exception $e) {
                                DB::rollback();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }

                            return response()->json($response,200);
                        }

                        //////////////////////////// SINKRON SDS  /////////////////////////////
                    }else if($locator->is_setting){
                        ////////////////////////////////////-LOCATOR SETTING-///////////////////////////////////
                        $check_last_status = DistribusiMovement::where('barcode_id', $barcode_id)
                        ->leftJoin('master_locator', 'master_locator.id', '=', 'distribusi_movements.locator_to')
                        ->orderBy('distribusi_movements.created_at', 'desc')->first();
                        $last_locator = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();

                        if($last_locator != null){
                            return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_locator->loc_dist,422);
                        }else{
                            $user=\Auth::user()->nik;
                            $factory=\Auth::user()->factory_id;
                            $dstat=1;
                            $alert=NULL;
                            $line_location = null;

                            $chk = $this->sds_connection->select($this->sds_connection->raw("
                                SELECT
                                barcode_id,TRIM(style) AS style,TRIM(set_type) AS set_type,season,TRIM(poreference) AS poreference,article,TRIM(size) AS size,cut_num,start_num,end_num,qty,
                                proc_scan IS NULL AND supply_scan IS NULL AS not_scanned,
                                cut_out,is_wip,supply_unlock,rejects,processing_fact,
                                CASE WHEN is_set THEN CONCAT(TRIM(location_name),' (',TRIM(factory),')') ELSE NULL END AS is_set,
                                CASE WHEN is_supplied THEN supplied_to ELSE NULL END AS is_supplied,
                                CASE
                                    WHEN NOT artwork THEN 'Artwork'
                                    WHEN NOT he THEN 'Heat Transfer'
                                    WHEN NOT pad THEN 'Pad Print'
                                    WHEN NOT ppa THEN 'PPA Jahit'
                                    WHEN NOT fuse THEN 'Fuse'
                                    WHEN NOT auto THEN 'Auto'
                                    WHEN NOT bonding THEN 'Bonding'
                                    ELSE NULL
                                END AS incomplete_proc
                                FROM bundle_for_set_new
                                WHERE barcode_id = '$barcode_check->sds_barcode'::NUMERIC
                            "));

                            if(count($chk) > 0) {

                                $chk = $chk[0];

                                if($chk->not_scanned==false){
                                    $alert="Bundle sudah dipindai di SDS!";
                                    $dstat=0;
                                }else if($chk->processing_fact != $factory){
                                    $alert="Bundle merupakan alokasi pabrik lain!";
                                    $dstat=0;
                                }else if($chk->is_wip != ''){
                                    $alert="Bundle belum Check-Out proses ".($chk->is_wip)."!";
                                    $dstat=0;
                                }else if($chk->supply_unlock == true){

                                }else if($chk->is_supplied != ''){
                                    $alert="Bundle sudah di supply ke ".($chk->is_supplied)."!";
                                    $dstat=0;
                                }else if($chk->is_set != ''){
                                    $alert="Bundle sudah menjalani Setting di ".($chk->is_set)."!";
                                    $dstat=0;
                                }else if($chk->rejects == true){
                                    $alert="Bundle dalam proses ganti reject!";
                                    $dstat=0;
                                }else if($chk->cut_out == false){
                                    $alert="Bundle belum checkout dari Cutting/Bundling!";
                                    $dstat=0;
                                }else if($chk->processing_fact==2 && $chk->incomplete_proc != ''){
                                    $alert="Bundle belum menjalani proses ".($chk->incomplete_proc)."!";
                                    $dstat=0;
                                }

                                if($dstat > 0){

                                    // $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                    //     SELECT
                                    //     CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                    //     FROM set_pairs_alloc_new
                                    //     WHERE
                                    //     season='$chk->season'::VARCHAR AND set_type='$chk->set_type'::VARCHAR AND poreference='$chk->poreference'::VARCHAR AND article='$chk->article'::VARCHAR
                                    //     AND size='$chk->size'::VARCHAR AND cut_num='$chk->cut_num'::INTEGER AND start_num='$chk->start_num'::INTEGER AND end_num='$chk->end_num'::INTEGER
                                    // "));

                                    $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                    SELECT DISTINCT c.factory_id AS sugg_fact,c.location AS sugg_loc,a.season,
                                    b.set_type, a.poreference, a.article, a.size, a.cut_num, a.start_num, a.end_num
                                    FROM
                                        bundle_info_new
                                        a LEFT JOIN art_desc_new b ON (
                                            b.season = a.season
                                            AND b.style = a.style
                                            AND b.part_num = a.part_num
                                            AND b.part_name = a.part_name
                                        )
                                        LEFT JOIN location c ON c.location = a.location
                                        AND c.factory = a.factory
                                    WHERE
                                        1 = 1
                                        AND c.area IN ( 4, 5 )
                                        AND a.setting IS NOT NULL
                                        AND c.factory_id = $factory
                                        AND a.season =  '$chk->season'
                                        AND b.set_type = '$chk->set_type'
                                        AND a.poreference = '$chk->poreference'
                                        AND a.article = '$chk->article'
                                        AND a.cut_num = $chk->cut_num
                                        AND a.start_num = $chk->start_num
                                        AND a.end_num = $chk->end_num
                                        AND a.size = '$chk->size'
                                    "));

                                    $set_notes=(count($set_notes) > 0 ? $set_notes[0] : NULL);

                                    // if($set_notes != NULL && $set_notes->is_moving==true){
                                    //     $alert="Pasangan-pasangan bundle ini sedang dalam proses movement di area Setting!";
                                    //     $dstat=0;
                                    // }else
                                    if($set_notes != NULL && $set_notes->sugg_fact != ''){
                                        $ppcm_id=NULL;
                                        $s_factory=$factory;
                                        if($set_notes->sugg_fact == $factory){
                                            $set_notes=$set_notes->sugg_loc;
                                        }else{
                                            $alert="Garment ini dikerjakan di ".($this->sds_connection->table('factory')->where('factory_id',$set_notes->sugg_fact)->first()->factory_name)."!";
                                            $dstat=0;
                                        }
                                    }else if($factory==1){
                                        $ppcm_id=NULL;
                                        $set_notes=NULL;
                                        $s_factory=NULL;
                                    }else{
                                        $choose_option = 2;
                                        if($choose_option == 1) {
                                            $set_notes=NULL;
                                            $que = "SELECT set_helper_new AS set_helper FROM set_helper_new(?,?,?,?,?::INTEGER)";
                                            $ppcm_id=$this->db->query($que,array(
                                                $chk->season,
                                                $chk->article,
                                                $chk->set_type,
                                                $chk->poreference,
                                                (int)$factory
                                            ))->result()[0]->set_helper;
                                            $ppcm_id = ($ppcm_id==NULL || $ppcm_id=='' ? NULL : $ppcm_id);
                                            $s_factory=NULL;
                                        } else if($choose_option == 2) {

                                            $check_temp_setting_set = $this->sds_connection
                                                ->table('jaz_temp_scan_setting')
                                                ->where('style', $chk->style)
                                                ->where('set_type', $chk->set_type)
                                                ->where('season', $chk->season)
                                                ->where('article', $chk->article)
                                                ->where('poreference', $chk->poreference)
                                                ->where('size', $chk->size)
                                                ->where('cut_num', $chk->cut_num)
                                                ->where('start_num', $chk->start_num)
                                                ->where('end_num', $chk->end_num)
                                                ->where('qty', $chk->qty)
                                                ->where('processing_fact', $factory)
                                                ->get();

                                            if(count($check_temp_setting_set) > 0) {
                                                $check_temp_setting_set = $check_temp_setting_set->first();
                                                $ppcm_id = $check_temp_setting_set->location;
                                            } else {
                                                $style_array = array();
                                                if($chk->set_type == 'TOP') {
                                                    $style_array[] = $chk->style.'-TOP';
                                                } else if($chk->set_type == 'BOTTOM') {
                                                    $style_array[] = $chk->style.'-BOT';
                                                } else if($chk->set_type == 'INNER') {
                                                    $style_array[] = $chk->style.'-INN';
                                                    $style_array[] = $chk->style.'-TIN';
                                                } else if($chk->set_type == 'OUTER') {
                                                    $style_array[] = $chk->style.'-OUT';
                                                    $style_array[] = $chk->style.'-TOU';
                                                } else {
                                                    $style_array[] = $chk->style;
                                                }

                                                $get_ppcm_ids = $this->sds_connection
                                                    ->table('jaz_ppcm_upload_new')
                                                    ->whereIn('style', $style_array)
                                                    ->whereNull('deleted_at')
                                                    ->where('poreference', $chk->poreference)
                                                    ->where('season', $chk->season)
                                                    ->where('article', $chk->article)
                                                    ->get();

                                                if(count($get_ppcm_ids) > 0) {
                                                    $selisih = 10000000;
                                                    $ppcm_id = null;
                                                    foreach($get_ppcm_ids as $data_ppcm) {

                                                        $qty_temp = $this->sds_connection->select($this->sds_connection->raw("select sum(qty) as qty from jaz_temp_scan_setting where location = '$data_ppcm->id'::numeric"))[0]->qty;

                                                        $qty_mod = $qty_temp + $data_ppcm->qty_mod;

                                                        if($data_ppcm->qty_split > $qty_mod) {

                                                            $get_selisih = abs(($data_ppcm->qty_split - $qty_mod) - $chk->qty);
                                                            if($get_selisih < $selisih) {
                                                                $selisih = $get_selisih;
                                                                $ppcm_id = $data_ppcm->id;
                                                            }

                                                        }
                                                    }
                                                } else {
                                                    $ppcm_id = null;
                                                }
                                            }
                                            $s_factory=NULL;
                                            $set_notes=NULL;
                                        }
                                    }

                                    if($dstat > 0){
                                        $check_temp_setting = $this->sds_connection->table('temp_scan_proc')->insert([
                                            'user_id' => $user,
                                            'proc' => 'SET',
                                            'ap_id' => $barcode_check->sds_barcode,
                                            'set_notes'=>$set_notes,
                                            'factory'=>$s_factory,
                                            'ppcm_id'=>$ppcm_id
                                        ]);

                                        $j=0;

                                        // NEW ENTRIES
                                        $bc_list = $this->sds_connection->select($this->sds_connection->raw("
                                            SELECT ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,
                                            TRIM(lc.location) AS location,TRIM(lc.factory) AS factory,
                                            MAX(bi.qty) AS qty,COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id
                                            FROM (
                                            SELECT ap_id,ppcm_id
                                            FROM temp_scan_proc
                                            WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NOT NULL AND NOT check_out
                                            ) ts
                                            JOIN bundle_info_new bi ON ts.ap_id=bi.barcode_id
                                            JOIN ppcm_upload_new pu ON ts.ppcm_id=pu.id
                                            JOIN location lc ON pu.line ~ CONCAT(E'^',REPLACE(TRIM(lc.factory),' ',E'#'),' ',REPLACE(TRIM(lc.location_name),' ','0?'),E'$')
                                            GROUP BY ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,lc.location,lc.factory
                                        "));

                                        foreach($bc_list as $bc){
                                            $this->sds_connection->select($this->sds_connection->raw("UPDATE ppcm_upload_new SET qty_mod=qty_mod+'$bc->qty' WHERE id='$bc->ppcm_id'"));

                                            $cond = explode(',',$bc->bc_id);
                                            $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                'location'=>$bc->location,
                                                'factory'=>$bc->factory,
                                                'setting'=>Carbon::now(),
                                                'setting_pic'=>$user,
                                                'supply_unlock'=>false
                                            ]);
                                            $line_location = $bc->location;
                                            $j+=$bc->count;
                                        }

                                        $bc_list = $this->sds_connection->select($this->sds_connection->raw("SELECT COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id,ts.set_notes,f.factory_name FROM (SELECT ap_id,set_notes,factory FROM temp_scan_proc WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NULL AND NOT check_out) ts JOIN factory f ON ts.factory=f.factory_id GROUP BY ts.set_notes,f.factory_name"));

                                        foreach($bc_list as $bc){

                                            $cond = explode(',',$bc->bc_id);
                                            $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                'location'=>$bc->set_notes,
                                                'factory'=>$bc->factory_name,
                                                'setting'=> Carbon::now(),
                                                'setting_pic'=>$user,
                                                'supply_unlock'=>false
                                            ]);
                                            $line_location = $bc->set_notes;
                                            $j+=$bc->count;
                                        }

                                        $this->sds_connection->table('system_log')->insert([
                                            'user_nik' => $user,
                                            'user_proc' => 'SET',
                                            'log_operation' => 'set_checkin',
                                            'log_text' => 'add item '.$barcode_check->sds_barcode.' to SET check in',
                                        ]);
                                    }
                                }

                            } else {
                                $alert="Bundle tidak terdaftar!";
                                $dstat=0;
                            }


                            if($dstat == 1) {
                                //ALOKASI LINE TIDAK TERDAPAT DI PPCM MAKA PILIH LOCATOR
                                if($line_location == null) {
                                    $bsds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->first();
                                    $set_type = DB::connection('sds_live')->table('art_desc_new')
                                    ->where('season',$bsds->season)
                                    ->where('style',$bsds->style)
                                    ->where('part_num',$bsds->part_num)
                                    ->where('part_name',$bsds->part_name)
                                    ->first();
                                    if($set_type != null){
                                        if($set_type->set_type == 'TOP'){
                                            $style_set = $bsds->style.'-TOP';
                                        }elseif($set_type->set_type == 'BOTTOM'){
                                            $style_set = $bsds->style.'-BOT';
                                        }else{
                                            $style_set = $bsds->style;
                                        }
                                    }else{
                                        $style_set = $bsds->style;
                                    }
                                    $ppcm_data = DB::connection('sds_live')->table('ppcm_upload_new')
                                        ->where('poreference',$bsds->poreference)
                                        ->where('style',$style_set)
                                        ->where('article',$bsds->article)
                                        ->whereNull('deleted_at')
                                        ->first();
                                    $obj = new StdClass;
                                    $obj->barcode_id = $barcode_id;
                                    $obj->line       = $ppcm_data != null ? $ppcm_data->line : '';
                                    return response()->json($obj, 331);
                                    // return response()->json($barcode_id, 331);
                                } else {
                                    $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();
                                    $komponen_name = $barcode_check->komponen_name;

                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $bundle_header->style,
                                        'article'       => $bundle_header->article,
                                        'po_buyer'      => $bundle_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $komponen_name,
                                        'size'          => $bundle_header->size,
                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                        'qty'           => $barcode_check->qty,
                                        'cut'           => $bundle_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $line_location
                                    ];

                                    $last_movement = DistribusiMovement::where('barcode_id', $barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();

                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => $last_movement->locator_to,
                                        'status_from'  => $last_movement->status_to,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => $line_location,
                                        'factory_id'   => \Auth::user()->factory_id
                                    ]);
                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                        'is_out_setting'    => 'Y',
                                        'out_setting_at'    => Carbon::now(),
                                        'current_locator_id' => $locator_id,
                                        'current_status_to' => $state,
                                        'current_description' => $state.' '.$locator->locator_name,
                                        'updated_movement_at' => Carbon::now(),
                                        'current_user_id' => \Auth::user()->id
                                    ]);

                                    $this->sds_connection->table('temp_scan_proc')
                                        ->where('user_id' ,$user)
                                        ->where('proc','SET')
                                        ->where('check_out',false)
                                        ->delete();

                                    return response()->json($response,200);
                                }
                            } else {
                                return response()->json($alert,422);
                            }
                        }

                    }else if($locator->is_artwork){

                        ///////////////////////-LOCATOR ARTWORK-////////////////////////////

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->where('description','out CUTTING')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();
                            //GET INFO KOMPONEN NAME DARI BUNDLE DETAIL
                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);
                                $check_artwork_process = DB::table('dd_secondary_process_check')->where('style_id',$barcode_check->style_detail_id)->first();
                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                                // if($check_artwork_process->print == 'f' || $check_artwork_process->embro == 'f' ){
                                //     return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                // }
                                else{
                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                if(in_array($locator_id, $get_locator_process)) {
                                    $last_loc = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->orderBy('created_at','DESC')->first();
                                    if($state == 'in') {
                                        return response()->json('Module Ini Sudah Tidak Aktif!',422);
                                    } else {
                                        $check_art_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_check->sds_barcode)->first();
                                        $in_cdms = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                        if($check_art_sds->artwork_out != null){
                                            return response()->json('Barcode Sudah Scan Out Artwork!',422);
                                        }elseif($check_art_sds->artwork == null){
                                            return response()->json('Barcode Belum Scan In Artwork!',422);
                                        }else{
                                            try {
                                                DB::beginTransaction();
                                                if($check_art_sds->artwork != null && $in_cdms == null){
                                                    $user_cdms = DB::table('users')->where('nik',$check_art_sds->artwork_pic)->first();
                                                    if($user_cdms == null){
                                                        return response()->json('Terjadi Kesalahan Harap Hubungi ICT!#D0USR01',422);
                                                    }
                                                    if($last_loc != null){
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'        => $barcode_id,
                                                            'locator_from'      => $last_loc->locator_to,
                                                            'status_from'       => $last_loc->status_to,
                                                            'locator_to'        => $locator_id,
                                                            'status_to'         => 'in',
                                                            'user_id'           => $user_cdms->id,
                                                            'description'       => 'in'.' '.$locator->locator_name,
                                                            'ip_address'        => $this->getIPClient(),
                                                            'deleted_at'        => null,
                                                            'loc_dist'          => '-',
                                                            'created_at'        => $check_art_sds->artwork,
                                                            'updated_at'        => $check_art_sds->artwork,
                                                            'factory_id'        => \Auth::user()->factory_id
                                                        ]);
                                                    }else{
                                                        return response()->json('Barcode Belum Out Cutting!',422);
                                                    }
                                                }
                                                if($last_loc != null){
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'        => $barcode_id,
                                                        'locator_from'      => $last_loc->locator_to,
                                                        'status_from'       => $last_loc->status_to,
                                                        'locator_to'        => $locator_id,
                                                        'status_to'         => $state,
                                                        'user_id'           => \Auth::user()->id,
                                                        'description'       => $state.' '.$locator->locator_name,
                                                        'ip_address'        => $this->getIPClient(),
                                                        'deleted_at'        => null,
                                                        'loc_dist'          => '-',
                                                        'factory_id'        => \Auth::user()->factory_id
                                                    ]);
        
                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id'    => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id
                                                    ]);
                                                    if($barcode_check->sds_barcode != null) {
                                                        $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                        if($update_sds != null) {
                                                            $convert_locator = $this->convertLocator($locator_id, $state);
                                                            if($update_sds->{$convert_locator['column_update']} == null) {
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                    'location' => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update'] => Carbon::now(),
                                                                    $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                    'factory' => $convert_locator['factory_sds'],
                                                                ]);
                                                            }
                                                        }
                                                    }
                                                    $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                                    $response = [
                                                        'barcode_id' => $barcode_id,
                                                        'style' => $barcode_header->style,
                                                        'article' => $barcode_header->article,
                                                        'po_buyer' => $barcode_header->poreference,
                                                        'part' => $komponen_detail->part,
                                                        'komponen_name' => $komponen_name,
                                                        'size' => $barcode_header->size,
                                                        'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                        'qty' => $barcode_check->qty,
                                                        'cut' => $barcode_header->cut_num,
                                                        'process' => $locator->locator_name,
                                                        'status' => $state,
                                                        'loc_dist' => '-'
                                                    ];
                                                    DB::commit();
                                                }else{
                                                    return response()->json('Barcode Belum Out Cutting!',422);
                                                }
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    }else{
                        ///////////////////-LOCATOR SECONDARY PROCESS-/////////////////////
                        // $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)
                        // ->orderBy('created_at', 'desc')
                        // ->first();
                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)
                                ->Join('master_locator', 'master_locator.id', '=', 'distribusi_movements.locator_to')
                                ->orderBy('distribusi_movements.created_at', 'desc')
                                ->first();
                        if($out_cutting_check != null){
                            $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();
                            $komponen_name      = $barcode_check->komponen_name;
                            if($komponen_detail != null){
                                //LOCATOR PPA 2 TANPA DEFINE PROSES PPA2 PADA MASTER STYLE DETAIL
                                if($locator_id == '7'){
                                    $is_ppa2 = DB::table('master_style_detail')->where('id',$barcode_check->style_detail_id)->first();
                                    if($is_ppa2 != null){
                                        if($is_ppa2->is_ppa2 == false){
                                            return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                        }
                                    }else{
                                        return response()->json('Master Style Dihapus Dari Sistem!',422);
                                    }
                                    $check_in_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                                    $check_out_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                                    if($state == 'in') {
                                        if($check_in_status != null){
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        }else{
                                            $check_setting = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                            if($check_setting == null){
                                                return response()->json('Harap Scan Setting dahulu!',422);
                                            }else{
                                                try {
                                                    DB::beginTransaction();
                                                    DistribusiMovement::FirstOrCreate([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $out_cutting_check->locator_to,
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'user_id'      => \Auth::user()->id,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'ip_address'   => $this->getIPClient(),
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => null,
                                                        'factory_id'   => \Auth::user()->factory_id
                                                    ]);

                                                    DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                        'current_locator_id' => $locator_id,
                                                        'current_status_to' => $state,
                                                        'current_description' => $state.' '.$locator->locator_name,
                                                        'updated_movement_at' => Carbon::now(),
                                                        'current_user_id' => \Auth::user()->id
                                                    ]);

                                                    $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $barcode_header->style,
                                                        'article'       => $barcode_header->article,
                                                        'po_buyer'      => $barcode_header->poreference,
                                                        'part'          => $komponen_detail->part,
                                                        'komponen_name' => $komponen_name,
                                                        'size'          => $barcode_header->size,
                                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                        'qty'           => $barcode_check->qty,
                                                        'cut'           => $barcode_header->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];

                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                                return response()->json($response,200);
                                            }
                                        }
                                    }else{
                                        if($check_in_status == null){
                                            return response()->json('Bundle Belum Scan In PPA2!',422);
                                        }elseif($check_out_status != null){
                                            return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                        }else{
                                            $barcode = DB::select("SELECT ms.season_name,bh.poreference,bh.style,t.type_name,bd.barcode_id,bd.bundle_header_id,bd.komponen_name,msd.is_ppa2,bd.style_detail_id
                                            FROM bundle_detail bd
                                            LEFT JOIN bundle_header bh on bh.id = bd.bundle_header_id
                                            LEFT JOIN master_style_detail msd on msd.id = bd.style_detail_id
                                            LEFT JOIN master_seasons ms on ms.id = msd.season
                                            LEFT JOIN types t on t.id = msd.type_id
                                            WHERE bd.bundle_header_id='$barcode_check->bundle_header_id' AND msd.is_ppa2 = true AND bd.start_no='$barcode_check->start_no' AND bd.end_no='$barcode_check->end_no'");
                                            $barcode_group = [];
                                            foreach($barcode as $b){
                                                $barcode_group[] = $b->barcode_id;
                                            }
                                            $barcode_group = implode(',',$barcode_group);
                                            $barcode_finish = explode(',',$barcode_group);
                                            $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                            $check_barcode = implode(',',$is_all_in);
                                            foreach($barcode_finish as $bf){
                                                $unscanned = DB::table('bundle_detail')->where('barcode_id',$bf)->first();
                                                if(strpos($check_barcode,$bf) === false){
                                                    return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                                }
                                            }
                                            try {
                                                DB::beginTransaction();
                                                $description = $state.' '.$locator->locator_name;
                                                for ($i = 0; $i < count($barcode_finish); $i++) {
                                                    $data_insert[] = [
                                                        'id'           => Uuid::generate()->string,
                                                        'barcode_id'   => $barcode_finish[$i],
                                                        'deleted_at'   => null,
                                                        'description'  => $description,
                                                        'ip_address'   => $this->getIPClient(),
                                                        'loc_dist'     => null,
                                                        'locator_from' => intval($out_cutting_check->locator_to),
                                                        'locator_to'   => intval($locator_id),
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'status_to'    => $state,
                                                        'user_id'      => \Auth::user()->id,
                                                        'created_at'   => Carbon::now(),
                                                        'updated_at'   =>Carbon::now(),
                                                        'factory_id'   => \Auth::user()->factory_id
                                                    ];
                                                }
                                                DistribusiMovement::insert($data_insert);
    
                                                DB::table('bundle_detail')->whereIn('barcode_id',$barcode_finish)->update([
                                                    'current_locator_id'    => $locator_id,
                                                    'current_status_to'     => $state,
                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                    'updated_movement_at'   => Carbon::now(),
                                                    'current_user_id'       => \Auth::user()->id
                                                ]);
    
                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
    
                                                $response = [
                                                    'barcode_id'        => $barcode_id,
                                                    'style'             => $barcode_header->style,
                                                    'article'           => $barcode_header->article,
                                                    'po_buyer'          => $barcode_header->poreference,
                                                    'part'              => $komponen_detail->part,
                                                    'komponen_name'     => $komponen_name,
                                                    'size'              => $barcode_header->size,
                                                    'sticker_no'        => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'               => $barcode_check->qty,
                                                    'cut'               => $barcode_header->cut_num,
                                                    'process'           => $locator->locator_name,
                                                    'status'            => $state,
                                                    'loc_dist'          => '-'
                                                ];
                                                DB::commit();
    
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    }
                                }else if($locator_id == '16'){
                                    $out_supply_check = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('description','out SUPPLY')->first();
                                    if($out_supply_check != null){
                                        return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                    }else{
                                        $data_barcode = DB::table('dd_bundle_check')->where('barcode_id', $barcode_id)->first();
                                        if(Auth::user()->factory_id == 2){
                                            $list_bundle_komponen = DB::table('dd_bundle_check')
                                            ->where('start_no',$data_barcode->start_no)
                                            ->where('end_no',$data_barcode->end_no)
                                            ->where('factory_id',$data_barcode->factory_id)
                                            ->where('bundle_header_id',$data_barcode->bundle_header_id)
                                            ->where('type_name',$data_barcode->type_name)
                                            ->where('size',$data_barcode->size)
                                            ->where('season_name',$data_barcode->season_name)
                                            ->pluck('barcode_id')
                                            ->toArray();
                                            foreach($list_bundle_komponen as $lbk){
                                                $check_in_setting = DistribusiMovement::where('barcode_id',$lbk)->where('locator_to','11')->where('status_to','in')->OrderBy('created_at','DESC')->first();
                                                $bundle_detail = DB::table('bundle_detail')->where('barcode_id',$lbk)->first();
                                                $check_in_set_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$bundle_detail->sds_barcode)->first();
                                                if($check_in_setting == null && $check_in_set_sds->setting == null){
                                                    return response()->json('Barcode '.$lbk.' Komponen '.$bundle_detail->komponen_name.' Belum Scan Setting!',422);
                                                }elseif($check_in_setting == null && $check_in_set_sds->setting != null){
                                                    $last_loc = DistribusiMovement::where('barcode_id',$lbk)->OrderBy('created_at','DESC')->first();
                                                    $user_sds = User::where('nik',$check_in_set_sds->setting_pic)->first();
                                                        try {
                                                            DB::beginTransaction();
                                                            $description = $state.' '.$locator->locator_name;
                                                            DistribusiMovement::firstOrCreate([
                                                                'barcode_id'    => $bundle_detail->barcode_id,
                                                                'deleted_at'    => null,
                                                                'description'   => 'in SETTING',
                                                                'ip_address'    => $this->getIPClient(),
                                                                'locator_from'  => $last_loc == null ? '10' : $last_loc->locator_to,
                                                                'locator_to'    => '11',
                                                                'status_from'   => $last_loc->status_to,
                                                                'status_to'     => 'in SETTING',
                                                                'user_id'       => (int)$user_sds->id,
                                                                'created_at'    => $check_in_set_sds->setting,
                                                                'updated_at'    => $check_in_set_sds->setting,
                                                                'factory_id'   => \Auth::user()->factory_id
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                }
                                            }
                                            foreach($list_bundle_komponen as $lbk){
                                                $last_loc = DistribusiMovement::where('barcode_id',$lbk)->OrderBy('created_at','DESC')->first();
                                                try {
                                                    DB::beginTransaction();
                                                    $description = $state.' '.$locator->locator_name;
                                                    DistribusiMovement::firstOrCreate([
                                                        'barcode_id'    => $lbk,
                                                        'deleted_at'    => null,
                                                        'description'   => $description,
                                                        'ip_address'    => $this->getIPClient(),
                                                        'loc_dist'      => $line_supply,
                                                        'locator_from'  => intval($last_loc->locator_to),
                                                        'locator_to'    => intval($locator_id),
                                                        'status_from'   => $last_loc->status_to,
                                                        'status_to'     => $state,
                                                        'user_id'       => Auth::user()->id,
                                                        'created_at'    => Carbon::now(),
                                                        'updated_at'    => Carbon::now(),
                                                        'factory_id'   => \Auth::user()->factory_id
                                                    ]);
        
                                                    DB::table('bundle_detail')->whereIn('barcode_id',$list_bundle_komponen)->update([
                                                        'is_out_setting' => 'Y',
                                                        'out_setting_at' => Carbon::now(),
                                                        'current_locator_id' => $locator_id,
                                                        'current_status_to' => $state,
                                                        'current_description' => $state.' '.$locator->locator_name,
                                                        'updated_movement_at' => Carbon::now(),
                                                        'current_user_id' => \Auth::user()->id
                                                    ]);
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                            }
                                            $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $line_supply
                                            ];
                                            return response()->json($response,200);
                                        }else{
                                            $list_bundle_komponen = DB::table('dd_bundle_check')
                                            ->where('start_no',$data_barcode->start_no)
                                            ->where('end_no',$data_barcode->end_no)
                                            ->where('factory_id',$data_barcode->factory_id)
                                            ->where('bundle_header_id',$data_barcode->bundle_header_id)
                                            ->where('type_name',$data_barcode->type_name)
                                            ->where('size',$data_barcode->size)
                                            ->where('season_name',$data_barcode->season_name)
                                            ->pluck('sds_barcode')
                                            ->toArray();
                                            foreach($list_bundle_komponen as $lbk){
                                                $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbk)->first();
                                                $bundle_detail = DB::table('bundle_detail')->where('sds_barcode',$lbk)->first();
                                                if($check_in_setting->setting == null){
                                                    return response()->json('Barcode '.$lbk.' Komponen '.$bundle_detail->komponen_name.' Belum Scan Setting!',422);
                                                }
                                            }
                                            foreach($list_bundle_komponen as $lbk){
                                                $bundle_detail = DB::table('bundle_detail')->where('sds_barcode',$lbk)->first();
                                                $last_loc = DistribusiMovement::where('barcode_id',$bundle_detail->barcode_id)->OrderBy('created_at','DESC')->first();
                                                $scan_set_cdms = DistribusiMovement::where('barcode_id',$bundle_detail->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                                $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbk)->first();
                                                try {
                                                    DB::beginTransaction();
                                                    if($check_in_setting->setting != null && $scan_set_cdms == null){
                                                        $user_ = User::where('nik',$check_in_setting->setting_pic)->first();
                                                        $user_set = $user_ == null ? Auth::user()->id : $user_->id;
                                                        DistribusiMovement::firstOrCreate([
                                                            'barcode_id'    => $bundle_detail->barcode_id,
                                                            'deleted_at'    => null,
                                                            'description'   => 'in SETTING',
                                                            'ip_address'    => $this->getIPClient(),
                                                            'locator_from'  => intval($last_loc->locator_to),
                                                            'locator_to'    => '11',
                                                            'loc_dist'      => trim($check_in_setting->location),
                                                            'status_from'   => $last_loc->status_to,
                                                            'status_to'     => 'in',
                                                            'user_id'       => $user_set,
                                                            'created_at'    => Carbon::now(),
                                                            'updated_at'    => Carbon::now(),
                                                            'factory_id'   => \Auth::user()->factory_id
                                                        ]);
                                                    }
                                                    $description = $state.' '.$locator->locator_name;
                                                    DistribusiMovement::firstOrCreate([
                                                        'barcode_id'    => $bundle_detail->barcode_id,
                                                        'deleted_at'    => null,
                                                        'description'   => $description,
                                                        'ip_address'    => $this->getIPClient(),
                                                        'loc_dist'      => $line_supply,
                                                        'locator_from'  => intval($last_loc->locator_to),
                                                        'locator_to'    => intval($locator_id),
                                                        'status_from'   => $last_loc->status_to,
                                                        'status_to'     => $state,
                                                        'user_id'       => Auth::user()->id,
                                                        'created_at'    => Carbon::now(),
                                                        'updated_at'    => Carbon::now(),
                                                        'factory_id'   => \Auth::user()->factory_id
                                                    ]);
        
                                                    DB::table('bundle_detail')->whereIn('sds_barcode',$list_bundle_komponen)->update([
                                                        'is_out_setting'        => 'Y',
                                                        'out_setting_at'        => Carbon::now(),
                                                        'current_locator_id'    => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => Auth::user()->id
                                                    ]);
                                                    DB::commit();
                                                } catch (Exception $e) {
                                                    DB::rollback();
                                                    $message = $e->getMessage();
                                                    ErrorHandler::db($message);
                                                }
                                            }
                                            $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $line_supply
                                            ];
                                            return response()->json($response,200);
                                        }
                                    }
                                }else{
                                    $get_process = explode(',', $komponen_detail->process);
                                    if($get_process[0] == null || $get_process[0] == '') {
                                        return response()->json('Barcode tidak lewat proses '.$locator->locator_name,422);
                                    }
                                    $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();
                                    if(in_array($locator_id, $get_locator_process)){
                                        //BUNDLE MELALUI PROSES SECONDARY TETAPI ADA PROSES ARTWORK
                                        if(in_array('3',$get_locator_process)){
                                            $component_movement_artwork = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', '3')->where('status_to', 'out')->first();
                                            if($barcode_check->sds_barcode != null) {
                                                $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                if($update_sds != null) {
                                                    $loc_name = DB::table('master_locator')->where('id',$out_cutting_check->locator_to)->first();
                                                    $in_out_loc     = ['1','2','3','4','5','6','7','8','9'];
                                                    if($out_cutting_check->locator_to != $locator_id && in_array($out_cutting_check->locator_to,$in_out_loc) && $out_cutting_check->status_to == 'in'){
                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                    }

                                                    $check_in   = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$locator_id)->where('status_to','in')->first();
                                                    $check_out  = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();
                                                    
                                                    if($check_in != null && $state == 'in'){
                                                        return response()->json('BARCODE SUDAH SCAN IN '.$locator->locator_name,422);
                                                    }elseif($check_out != null && $state == 'out'){
                                                        return response()->json('BARCODE SUDAH SCAN OUT '.$locator->locator_name,422);
                                                    }
                                                    
                                                    if($update_sds->artwork_out != null && $component_movement_artwork != null){
                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                            try {
                                                                DB::beginTransaction();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                    'location'                                  => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']           => Carbon::now(),
                                                                    $convert_locator['column_update_pic']       => \Auth::user()->nik,
                                                                    'factory'                                   => $convert_locator['factory_sds'],
                                                                ]);
                                                                DistribusiMovement::FirstOrCreate([
                                                                    'barcode_id'   => $barcode_id,
                                                                    'locator_from' => $out_cutting_check->locator_to,
                                                                    'status_from'  => $out_cutting_check->status_to,
                                                                    'locator_to'   => $locator_id,
                                                                    'status_to'    => $state,
                                                                    'user_id'      => \Auth::user()->id,
                                                                    'description'  => $state.' '.$locator->locator_name,
                                                                    'ip_address'   => $this->getIPClient(),
                                                                    'deleted_at'   => null,
                                                                    'loc_dist'     => '-',
                                                                    'factory_id'   => \Auth::user()->factory_id
                                                                ]);
                                                                DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                                    'current_locator_id'    => $locator_id,
                                                                    'current_status_to'     => $state,
                                                                    'current_description'   => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'   => Carbon::now(),
                                                                    'current_user_id'       => \Auth::user()->id
                                                                ]);
                                                                
                                                                DB::commit();
                                                            }catch (Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                            $response = [
                                                                'barcode_id'    => $barcode_id,
                                                                'style'         => $update_sds->style,
                                                                'article'       => $update_sds->article,
                                                                'po_buyer'      => $update_sds->poreference,
                                                                'part'          => $update_sds->part_num,
                                                                'komponen_name' => $update_sds->part_name,
                                                                'size'          => $update_sds->size,
                                                                'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                                'qty'           => $update_sds->qty,
                                                                'cut'           => $update_sds->cut_num,
                                                                'process'       => $locator->locator_name,
                                                                'status'        => $state,
                                                                'loc_dist'      => '-'
                                                            ];
                                                            return response()->json($response,200);
                                                        }else{
                                                            return response()->json('BARCODE SUDAH SCAN '.$state.' '.$locator->locator_name,422);
                                                        }
                                                    }elseif($update_sds->artwork_out == null && $component_movement_artwork == null){
                                                        return response()->json('BUNDLE HARUS PROSES ARTWORK DAHULU !',422);
                                                    }elseif($update_sds->artwork_out != null && $component_movement_artwork == null){
                                                        return response()->json('TERJADI KESALAHAN HUB.ICT #ER-ART'.Auth::user()->factory_id,422);
                                                    }elseif($update_sds->artwork_out == null && $component_movement_artwork != null){
                                                        return response()->json('TERJADI KESALAHAN HUB.ICT #ARTSYNC'.Auth::user()->factory_id.'ART',422);
                                                    }else{
                                                        return response()->json('BARCODE BELUM SCAN OUT ARTWORK!',422);    
                                                    }
                                                }else{
                                                    return response()->json('BARCODE SDS TIDAK DITEMUKAN!',422);    
                                                }
                                            }else{
                                                return response()->json('BARCODE SDS TIDAK DITEMUKAN!',422);
                                            }
                                        }else{
                                            // BUNDLE PROSES SECONDARY TANPA PROSES ARTWORK
                                            if($barcode_check->sds_barcode != null) {
                                                $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                if($update_sds != null) {
                                                    $in_out_loc     = ['1','2','3','4','5','6','7','8','9'];
                                                    if($out_cutting_check->locator_to != $locator_id && in_array($out_cutting_check->locator_to,$in_out_loc) && $out_cutting_check->status_to == 'in'){
                                                        $loc_name = DB::table('master_locator')->where('id',$out_cutting_check->locator_to)->first();
                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                    }
                                                    $check_in   = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$locator_id)->where('status_to','in')->first();
                                                    $check_out  = DistribusiMovement::where('barcode_id',$barcode_check->barcode_id)->where('locator_to',$locator_id)->where('status_to','out')->first();
                                                    if($out_cutting_check->locator_to == 10 && $state == 'out'){
                                                        if($check_in == null){
                                                            return response()->json('BARCODE BELUM SCAN IN '.$locator->locator_name,422);
                                                        }
                                                    }

                                                    if($out_cutting_check->locator_to != $locator_id && $check_in == null && $state == 'out'){
                                                        return response()->json('BARCODE BELUM SCAN IN '.$locator->locator_name,422);
                                                    }
                                                    
                                                    if($check_in != null && $state == 'in'){
                                                        return response()->json('BARCODE SUDAH SCAN IN '.$locator->locator_name,422);
                                                    }elseif($check_out != null && $state == 'out'){
                                                        return response()->json('BARCODE SUDAH SCAN OUT '.$locator->locator_name,422);
                                                    }
                                                    
                                                    $convert_locator = $this->convertLocator($locator_id, $state);
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        try {
                                                            DB::beginTransaction();
                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                                'location'                                  => $convert_locator['locator_sds'],
                                                                $convert_locator['column_update']           => Carbon::now(),
                                                                $convert_locator['column_update_pic']       => \Auth::user()->nik,
                                                                'factory'                                   => $convert_locator['factory_sds'],
                                                            ]);
                                                            DistribusiMovement::FirstOrCreate([
                                                                'barcode_id'   => $barcode_id,
                                                                'locator_from' => $out_cutting_check->locator_to,
                                                                'status_from'  => $out_cutting_check->status_to,
                                                                'locator_to'   => $locator_id,
                                                                'status_to'    => $state,
                                                                'user_id'      => \Auth::user()->id,
                                                                'description'  => $state.' '.$locator->locator_name,
                                                                'ip_address'   => $this->getIPClient(),
                                                                'deleted_at'   => null,
                                                                'loc_dist'     => '-',
                                                                'factory_id'   => \Auth::user()->factory_id
                                                            ]);

                                                            DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                                                                'current_locator_id' => $locator_id,
                                                                'current_status_to' => $state,
                                                                'current_description' => $state.' '.$locator->locator_name,
                                                                'updated_movement_at' => Carbon::now(),
                                                                'current_user_id' => \Auth::user()->id
                                                            ]);
                                                            DB::commit();
                                                        } catch (Exception $e) {
                                                            DB::rollback();
                                                            $message = $e->getMessage();
                                                            ErrorHandler::db($message);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        return response()->json('BARCODE SUDAH SCAN '.$state.' '.$locator->locator_name,422);
                                                    }
                                                }else{
                                                    return response()->json('BARCODE SDS TIDAK DITEMUKAN!',422);    
                                                }
                                            }else{
                                                return response()->json('BARCODE SDS TIDAK DITEMUKAN!',422);
                                            }
                                        }
                                    }else{
                                        return response()->json('Barcode tidak lewat proses '.$locator->locator_name,422);
                                    }
                                }
                            }else{
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        }else{
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    }
                }else{
                    ////////////////////////////////--JIKA BARCODE YANG DI SCAN BARCODE SDS--//////////////////////////////////
                    $barcode_check_sds = DB::table('bundle_detail as bd')
                    ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                    ->leftJoin('v_master_style as vms','vms.id','=','bd.style_detail_id')
                    ->where('bd.sds_barcode', $barcode_id)
                    ->first();

                    $sds_detail = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();

                    if($barcode_check_sds != null && $sds_detail != null){
                        return response()->json('DUPLIKAT DETAIL BARCODE SDS!D002',422);
                    }
                    //LOCATOR SUPPLY DENGAN SCAN BARCODE CDMS
                    if($locator_id == '16'){
                        $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                            ->where('barcode_id', $barcode_id)
                            ->where('processing_fact',\Auth::user()->factory_id)
                            ->first();
                        if($barcode_check_sds != null && $sds_detail == null){
                            $out_supply_check = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                            if($out_supply_check != null){
                                return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                            }else{
                                // $data_barcode = DB::table('dd_bundle_check')->where('sds_barcode', $barcode_id)->first();
                                if(Auth::user()->factory_id == 2){
                                    $list_bundle_komponen = DB::table('dd_bundle_check')
                                    ->where('start_no',$barcode_check_sds->start_no)
                                    ->where('end_no',$barcode_check_sds->end_no)
                                    ->where('factory_id',$barcode_check_sds->factory_id)
                                    ->where('bundle_header_id',$barcode_check_sds->bundle_header_id)
                                    ->where('type_id',$barcode_check_sds->type_id)
                                    ->where('size',$barcode_check_sds->size)
                                    ->where('season_name',$barcode_check_sds->season_name)
                                    ->pluck('barcode_id')->toArray();
                                    foreach($list_bundle_komponen as $lbk){
                                        $check_in_setting = DistribusiMovement::where('barcode_id',$lbk)->where('locator_to','11')->where('status_to','in')->first();
                                        $bundle_detail = DB::table('bundle_detail')->where('barcode_id',$lbk)->first();
                                        $check_set_sds = DB::connection('sds_live')->table('bundle_info_new')->select('setting','setting_pic')->where('barcode_id',$bundle_detail->sds_barcode)->first();
                                        if($check_in_setting == null && $check_set_sds->setting == null){
                                            return response()->json('Barcode '.$lbk.' Komponen '.$bundle_detail->komponen_name.' Belum Scan Setting!',422);
                                        }elseif($check_in_setting == null && $check_set_sds->setting != null){
                                            $last_loc = DistribusiMovement::where('barcode_id',$lbk)->OrderBy('created_at','DESC')->first();
                                            $user_sds = User::where('nik',$check_set_sds->setting_pic)->first();
                                            $user_set = $user_sds == null ? Auth::user()->id : $user_sds->id;
                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::firstOrCreate([
                                                    'barcode_id'    => $lbk,
                                                    'deleted_at'    => null,
                                                    'description'   => 'in SETTING',
                                                    'ip_address'    => $this->getIPClient(),
                                                    'loc_dist'      => '-',
                                                    'locator_from'  => intval($last_loc->locator_to),
                                                    'locator_to'    => '11',
                                                    'status_from'   => $last_loc->status_to,
                                                    'status_to'     => 'in',
                                                    'user_id'       => (int)$user_sds->id,
                                                    'created_at'    => $check_set_sds->setting,
                                                    'updated_at'    => $check_set_sds->setting,
                                                    'factory_id'   => \Auth::user()->factory_id
                                                ]);
                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                        }
                                    }
                                    foreach($list_bundle_komponen as $lbk){
                                        $last_loc = DistribusiMovement::where('barcode_id',$lbk)->OrderBy('created_at','DESC')->first();
                                        try {
                                            DB::beginTransaction();
                                            $description = $state.' '.$locator->locator_name;
                                            DistribusiMovement::firstOrCreate([
                                                'barcode_id'    => $lbk,
                                                'deleted_at'    => null,
                                                'description'   => $description,
                                                'ip_address'    => $this->getIPClient(),
                                                'loc_dist'      => $line_supply,
                                                'locator_from'  => intval($last_loc->locator_to),
                                                'locator_to'    => intval($locator_id),
                                                'status_from'   => $last_loc->status_to,
                                                'status_to'     => $state,
                                                'user_id'       => Auth::user()->id,
                                                'created_at'    => Carbon::now(),
                                                'updated_at'    => Carbon::now(),
                                                'factory_id'   => \Auth::user()->factory_id
                                            ]);

                                            DB::table('bundle_detail')->whereIn('barcode_id',$list_bundle_komponen)->update([
                                                'is_out_setting' => 'Y',
                                                'out_setting_at' => Carbon::now(),
                                                'current_locator_id' => $locator_id,
                                                'current_status_to' => $state,
                                                'current_description' => $state.' '.$locator->locator_name,
                                                'updated_movement_at' => Carbon::now(),
                                                'current_user_id' => \Auth::user()->id
                                            ]);
                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                    }
                                    $barcode_header = DB::table('bundle_header')->where('id', $barcode_check_sds->bundle_header_id)->first();
                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $barcode_header->style,
                                        'article'       => $barcode_header->article,
                                        'po_buyer'      => $barcode_header->poreference,
                                        'part'          => $barcode_check_sds->part,
                                        'komponen_name' => $barcode_check_sds->komponen_name,
                                        'size'          => $barcode_header->size,
                                        'sticker_no'    => $barcode_check_sds->start_no.' - '.$barcode_check_sds->end_no,
                                        'qty'           => $barcode_check_sds->qty,
                                        'cut'           => $barcode_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $line_supply
                                    ];
                                    return response()->json($response,200);
                                }else{
                                    $list_bundle_komponen = DB::table('dd_bundle_check')
                                    ->where('start_no',$barcode_check_sds->start_no)
                                    ->where('end_no',$barcode_check_sds->end_no)
                                    ->where('factory_id',$barcode_check_sds->factory_id)
                                    ->where('bundle_header_id',$barcode_check_sds->bundle_header_id)
                                    ->where('type_id',$barcode_check_sds->type_id)
                                    ->where('size',$barcode_check_sds->size)
                                    ->where('season_name',$barcode_check_sds->season_name)
                                    ->pluck('sds_barcode')
                                    ->toArray();
                                    foreach($list_bundle_komponen as $lbk){
                                        $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbk)->first();
                                        if($check_in_setting->setting == null){
                                            return response()->json('Barcode '.$lbk.' Komponen '.$check_in_setting->part_name.' Belum Scan Setting!',422);
                                        }
                                    }
                                    foreach($list_bundle_komponen as $lbk){
                                        $bundle_detail = DB::table('bundle_detail')->where('sds_barcode',$lbk)->first();
                                        $last_loc = DistribusiMovement::where('barcode_id',$bundle_detail->barcode_id)->OrderBy('created_at','DESC')->first();
                                        $scan_set_cdms = DistribusiMovement::where('barcode_id',$bundle_detail->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                        $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbk)->first();
                                        try {
                                            DB::beginTransaction();
                                            if($check_in_setting->setting != null && $scan_set_cdms == null){
                                                $user_ = User::where('nik',$check_in_setting->setting_pic)->first();
                                                $user_set = $user_ == null ? Auth::user()->id : $user_->id;
                                                DistribusiMovement::firstOrCreate([
                                                    'barcode_id'    => $bundle_detail->barcode_id,
                                                    'deleted_at'    => null,
                                                    'description'   => 'in SETTING',
                                                    'ip_address'    => $this->getIPClient(),
                                                    'locator_from'  => intval($last_loc->locator_to),
                                                    'locator_to'    => '11',
                                                    'loc_dist'      => trim($check_in_setting->location),
                                                    'status_from'   => $last_loc->status_to,
                                                    'status_to'     => 'in',
                                                    'user_id'       => (int)$user_set,
                                                    'created_at'    => Carbon::now(),
                                                    'updated_at'    => Carbon::now(),
                                                    'factory_id'   => \Auth::user()->factory_id
                                                ]);
                                            }
                                            $description = $state.' '.$locator->locator_name;
                                            DistribusiMovement::firstOrCreate([
                                                'barcode_id'    => $bundle_detail->barcode_id,
                                                'deleted_at'    => null,
                                                'description'   => $description,
                                                'ip_address'    => $this->getIPClient(),
                                                'loc_dist'      => $line_supply,
                                                'locator_from'  => intval($last_loc->locator_to),
                                                'locator_to'    => intval($locator_id),
                                                'status_from'   => $last_loc->status_to,
                                                'status_to'     => $state,
                                                'user_id'       => Auth::user()->id,
                                                'created_at'    => Carbon::now(),
                                                'updated_at'    => Carbon::now(),
                                                'factory_id'   => \Auth::user()->factory_id
                                            ]);

                                            DB::table('bundle_detail')->whereIn('sds_barcode',$list_bundle_komponen)->update([
                                                'is_out_setting'        => 'Y',
                                                'out_setting_at'        => Carbon::now(),
                                                'current_locator_id'    => $locator_id,
                                                'current_status_to'     => $state,
                                                'current_description'   => $state.' '.$locator->locator_name,
                                                'updated_movement_at'   => Carbon::now(),
                                                'current_user_id'       => Auth::user()->id
                                            ]);
                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                    }
                                    $barcode_header = DB::table('bundle_header')->where('id', $barcode_check_sds->bundle_header_id)->first();
                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $barcode_header->style,
                                        'article'       => $barcode_header->article,
                                        'po_buyer'      => $barcode_header->poreference,
                                        'part'          => $barcode_check_sds->part,
                                        'komponen_name' => $barcode_check_sds->komponen_name,
                                        'size'          => $barcode_header->size,
                                        'sticker_no'    => $barcode_check_sds->start_no.' - '.$barcode_check_sds->end_no,
                                        'qty'           => $barcode_check_sds->qty,
                                        'cut'           => $barcode_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $line_supply
                                    ];
                                    return response()->json($response,200);
                                }
                            }
                        }elseif($barcode_check_sds == null && $sds_header != null && $sds_detail != null){
                            if(Auth::user()->factory_id == 2){
                                $list_barcode_sds = $this->sds_connection->table('dd_info_bundle_supply')
                                ->where('poreference', $sds_header->poreference)
                                ->where('cut_num',$sds_header->cut_num)
                                ->where('start_num',$sds_header->start_num)
                                ->where('end_num',$sds_header->end_num)
                                ->where('size',$sds_header->size)
                                ->where('style',$sds_header->style)
                                ->where('set_type',$sds_header->set_type)
                                ->where('article',$sds_header->article)
                                ->where('coll',$sds_header->coll)
                                ->where('processing_fact',Auth::user()->factory_id)->pluck('barcode_id')->toArray();

                                foreach($list_barcode_sds as $lbs){
                                    $check_out_cut = DB::table('sds_detail_movements')->where('sds_barcode',$lbs)->first();
                                    if($check_out_cut == null){
                                        return response()->json('Barcode '.$lbs.' Belum Scan Out Cutting!',422);
                                    }
                                    $check_out_supply = DistribusiMovement::where('barcode_id',$lbs)->where('locator_to','16')->where('status_to','out')->first();
                                    if($check_out_supply != null){
                                        return response()->json('Barcode '.$lbs.' Sudah Scan Supply ke '.$check_out_supply->loc_dist,422);
                                    }
                                    $check_in_set = DistribusiMovement::where('barcode_id',$lbs)->where('locator_to','11')->where('status_to','in')->first();
                                    $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbs)->first();
                                    $sds_detail = DB::table('sds_detail_movements')->where('sds_barcode',$lbs)->first();
                                    if($check_in_set == null && $check_in_setting->setting == null){
                                        return response()->json('Barcode '.$lbs.' '.$sds_detail->komponen_name.' Belum Menjalani Scan Setting',422);
                                    }elseif($check_in_set == null && $check_in_setting->setting != null){
                                        $user_ = User::where('nik',$check_in_setting->setting_pic)->first();
                                        $user_set = $user_ == null ? Auth::user()->id : $user_->id;
                                        $last_loc = DistribusiMovement::where('barcode_id',$lbs)->OrderBy('created_at','DESC')->first();
                                        DistribusiMovement::firstOrCreate([
                                            'barcode_id'    => $lbs,
                                            'deleted_at'    => null,
                                            'description'   => 'in SETTING',
                                            'ip_address'    => $this->getIPClient(),
                                            'locator_from'  => intval($last_loc->locator_to),
                                            'locator_to'    => '11',
                                            'loc_dist'      => str_replace('LINE','L.',$line_supply),
                                            'status_from'   => $last_loc->status_to,
                                            'status_to'     => 'in',
                                            'user_id'       => $user_set,
                                            'created_at'    => $check_in_setting->setting,
                                            'updated_at'    => $check_in_setting->setting,
                                            'factory_id'   => \Auth::user()->factory_id
                                        ]);
                                    }
                                }
                                foreach($list_barcode_sds as $lbx){
                                    $sds_detail = DB::table('sds_detail_movements')->where('sds_barcode',$lbx)->first();
                                    $last_loc = DistribusiMovement::where('barcode_id',$lbx)->OrderBy('created_at','DESC')->first();
                                    $scan_set_cdms = DistribusiMovement::where('barcode_id',$lbx)->where('locator_to','11')->where('status_to','in')->first();
                                    
                                    try {
                                        DB::beginTransaction();
                                        $description = $state.' '.$locator->locator_name;
                                        DistribusiMovement::firstOrCreate([
                                            'barcode_id'    => $lbx,
                                            'deleted_at'    => null,
                                            'description'   => $description,
                                            'ip_address'    => $this->getIPClient(),
                                            'loc_dist'      => $line_supply,
                                            'locator_from'  => intval($last_loc->locator_to),
                                            'locator_to'    => intval($locator_id),
                                            'status_from'   => $last_loc->status_to,
                                            'status_to'     => $state,
                                            'user_id'       => Auth::user()->id,
                                            'created_at'    => Carbon::now(),
                                            'updated_at'    => Carbon::now(),
                                            'factory_id'   => \Auth::user()->factory_id
                                        ]);

                                        DB::table('sds_detail_movements')->where('sds_barcode',$lbx)->update([
                                            'is_out_setting'        => 'Y',
                                            'out_setting_at'        => Carbon::now(),
                                            'current_location_id'   => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => Auth::user()->id
                                        ]);
                                        DB::commit();
                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                }
                                $barcode_header = DB::table('sds_header_movements')->where('id', $sds_detail->sds_header_id)->first();
                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $sds_detail->part_num,
                                    'komponen_name' => $sds_detail->komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $sds_detail->start_no.' - '.$sds_detail->end_no,
                                    'qty'           => $sds_detail->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $line_supply
                                ];
                                return response()->json($response,200);
                            }else{
                                $list_barcode_sds = $this->sds_connection->table('dd_info_bundle_supply')
                                ->where('poreference', $sds_header->poreference)
                                ->where('cut_num',$sds_header->cut_num)
                                ->where('start_num',$sds_header->start_num)
                                ->where('end_num',$sds_header->end_num)
                                ->where('size',$sds_header->size)
                                ->where('style',$sds_header->style)
                                ->where('set_type',$sds_header->set_type)
                                ->where('article',$sds_header->article)
                                ->where('coll',$sds_header->coll)
                                ->where('processing_fact',Auth::user()->factory_id)->pluck('barcode_id')->toArray();

                                foreach($list_barcode_sds as $lbs){
                                    $check_out_cut = DB::table('sds_detail_movements')->where('sds_barcode',$lbs)->first();
                                    if($check_out_cut == null){
                                        return response()->json('Barcode '.$lbs.' Belum Scan Out Cutting!',422);
                                    }
                                    $check_out_supply = DistribusiMovement::where('barcode_id',$lbs)->where('locator_to','16')->where('status_to','out')->first();
                                    if($check_out_supply != null){
                                        return response()->json('Barcode '.$lbs.' Sudah Scan Supply ke '.$check_out_supply->loc_dist,422);
                                    }
                                    $check_in_set = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbs)->first();
                                    if($check_in_set->setting == null){
                                        return response()->json('Barcode '.$lbs.' Belum Menjalani Scan Setting',422);
                                    }
                                }
                                foreach($list_barcode_sds as $lbx){
                                    $sds_detail = DB::table('sds_detail_movements')->where('sds_barcode',$lbx)->first();
                                    $last_loc = DistribusiMovement::where('barcode_id',$lbx)->OrderBy('created_at','DESC')->first();
                                    $scan_set_cdms = DistribusiMovement::where('barcode_id',$lbx)->where('locator_to','11')->where('status_to','in')->first();
                                    $check_in_setting = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$lbx)->first();
                                    try {
                                        DB::beginTransaction();
                                        if($check_in_setting->setting != null && $scan_set_cdms == null){
                                            $user_ = User::where('nik',$check_in_setting->setting_pic)->first();
                                            $user_set = $user_ == null ? Auth::user()->id : $user_->id;
                                            DistribusiMovement::firstOrCreate([
                                                'barcode_id'    => $lbx,
                                                'deleted_at'    => null,
                                                'description'   => 'in SETTING',
                                                'ip_address'    => $this->getIPClient(),
                                                'locator_from'  => intval($last_loc->locator_to),
                                                'locator_to'    => '11',
                                                'loc_dist'      => trim($check_in_setting->location),
                                                'status_from'   => $last_loc->status_to,
                                                'status_to'     => 'in',
                                                'user_id'       => $user_set,
                                                'created_at'    => Carbon::now(),
                                                'updated_at'    => Carbon::now(),
                                                'factory_id'   => \Auth::user()->factory_id
                                            ]);
                                        }
                                        $description = $state.' '.$locator->locator_name;
                                        DistribusiMovement::firstOrCreate([
                                            'barcode_id'    => $lbx,
                                            'deleted_at'    => null,
                                            'description'   => $description,
                                            'ip_address'    => $this->getIPClient(),
                                            'loc_dist'      => $line_supply,
                                            'locator_from'  => intval($last_loc->locator_to),
                                            'locator_to'    => intval($locator_id),
                                            'status_from'   => $last_loc->status_to,
                                            'status_to'     => $state,
                                            'user_id'       => Auth::user()->id,
                                            'created_at'    => Carbon::now(),
                                            'updated_at'    => Carbon::now(),
                                            'factory_id'   => \Auth::user()->factory_id
                                        ]);

                                        DB::table('sds_detail_movements')->where('sds_barcode',$lbx)->update([
                                            'is_out_setting'        => 'Y',
                                            'out_setting_at'        => Carbon::now(),
                                            'current_location_id'   => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => Auth::user()->id
                                        ]);
                                        DB::commit();
                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                }
                                $barcode_header = DB::table('sds_header_movements')->where('id', $sds_detail->sds_header_id)->first();
                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $sds_detail->part_num,
                                    'komponen_name' => $sds_detail->komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $sds_detail->start_no.' - '.$sds_detail->end_no,
                                    'qty'           => $sds_detail->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $line_supply
                                ];
                                return response()->json($response,200);
                            }
                        }else{
                            return response()->json('Barcode Tidak Ditemukan...!',422);
                        }
                    }
                    if($locator_id == '7'){
                        if($barcode_check_sds != null){
                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check_sds->style_detail_id)->where('deleted_at', null)->first();
                            $komponen_name = $barcode_check_sds->komponen_name;
                            $out_cutting_check = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                            $is_ppa2 = DB::table('master_style_detail')->where('id',$barcode_check_sds->style_detail_id)->first();
                            if($is_ppa2 != null){
                                if($is_ppa2->is_ppa2 == false){
                                    return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                }
                            }else{
                                return response()->json('Master Style Dihapus Dari Sistem!',422);
                            }
                            $check_in_status = DistribusiMovement::where('barcode_id', $barcode_check_sds->barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                            $check_out_status = DistribusiMovement::where('barcode_id', $barcode_check_sds->barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                            if($state == 'in') {
                                if($check_in_status != null){
                                    return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                }else{
                                    $check_setting = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                    if($check_setting == null){
                                        return response()->json('Harap Scan Setting dahulu!',422);
                                    }else{
                                        try {
                                            DB::beginTransaction();
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_check_sds->barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => '-',
                                                'factory_id'   => \Auth::user()->factory_id
                                            ]);
    
                                            DB::table('bundle_detail')->where('barcode_id',$barcode_check_sds->barcode_id)->update([
                                                'current_locator_id' => $locator_id,
                                                'current_status_to' => $state,
                                                'current_description' => $state.' '.$locator->locator_name,
                                                'updated_movement_at' => Carbon::now(),
                                                'current_user_id' => \Auth::user()->id
                                            ]);
    
                                            $barcode_header = DB::table('bundle_header')->where('id', $barcode_check_sds->bundle_header_id)->first();
    
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $barcode_check_sds->komponen,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $barcode_check_sds->start_no.' - '.$barcode_check_sds->end_no,
                                                'qty'           => $barcode_check_sds->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => '-'
                                            ];
    
                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }else{
                                if($check_in_status == null){
                                    return response()->json('Bundle Belum Scan In PPA2!',422);
                                }elseif($check_out_status != null){
                                    return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                }else{
                                    $barcode = DB::select("SELECT ms.season_name,bh.poreference,bh.style,t.type_name,bd.barcode_id,bd.bundle_header_id,bd.komponen_name,msd.is_ppa2,bd.style_detail_id
                                    FROM bundle_detail bd
                                    LEFT JOIN bundle_header bh on bh.id = bd.bundle_header_id
                                    LEFT JOIN master_style_detail msd on msd.id = bd.style_detail_id
                                    LEFT JOIN master_seasons ms on ms.id = msd.season
                                    LEFT JOIN types t on t.id = msd.type_id
                                    WHERE bd.bundle_header_id='$barcode_check_sds->bundle_header_id' AND msd.is_ppa2 = true AND bd.start_no='$barcode_check_sds->start_no' AND bd.end_no='$barcode_check_sds->end_no'");
                                    $barcode_group = [];
                                    foreach($barcode as $b){
                                        $barcode_group[] = $b->barcode_id;
                                    }
                                    $barcode_group = implode(',',$barcode_group);
                                    $barcode_finish = explode(',',$barcode_group);
                                    $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                    $check_barcode = implode(',',$is_all_in);
                                    foreach($barcode_finish as $bf){
                                        $unscanned = DB::table('bundle_detail')->where('barcode_id',$bf)->first();
                                        if(strpos($check_barcode,$bf) === false){
                                            return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                        }
                                    }
                                    try {
                                        DB::beginTransaction();
                                        $description = $state.' '.$locator->locator_name;
                                        for ($i = 0; $i < count($barcode_finish); $i++) {
                                            $data_insert[] = [
                                                'id'           => Uuid::generate()->string,
                                                'barcode_id'   => $barcode_finish[$i],
                                                'deleted_at'   => null,
                                                'description'  => $description,
                                                'ip_address'   => $this->getIPClient(),
                                                'loc_dist'     => $line_supply,
                                                'locator_from' => intval($out_cutting_check->locator_to),
                                                'locator_to'   => intval($locator_id),
                                                'status_from'  => $out_cutting_check->status_to,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'created_at'   => Carbon::now(),
                                                'updated_at'   =>Carbon::now(),
                                                'factory_id'   => \Auth::user()->factory_id
                                            ];
                                        }
                                        DistribusiMovement::insert($data_insert);
    
                                        DB::table('bundle_detail')->whereIn('barcode_id',$barcode_finish)->update([
                                            'current_locator_id'    => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => \Auth::user()->id
                                        ]);
    
                                        $barcode_header = DB::table('bundle_header')->where('id', $barcode_check_sds->bundle_header_id)->first();
    
                                        $response = [
                                            'barcode_id'        => $barcode_check_sds->barcode_id,
                                            'style'             => $barcode_header->style,
                                            'article'           => $barcode_header->article,
                                            'po_buyer'          => $barcode_header->poreference,
                                            'part'              => $komponen_detail->part,
                                            'komponen_name'     => $komponen_name,
                                            'size'              => $barcode_header->size,
                                            'sticker_no'        => $barcode_check_sds->start_no.' - '.$barcode_check_sds->end_no,
                                            'qty'               => $barcode_check_sds->qty,
                                            'cut'               => $barcode_header->cut_num,
                                            'process'           => $locator->locator_name,
                                            'status'            => $state,
                                            'loc_dist'          => '-'
                                        ];
                                        DB::commit();
    
                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                    return response()->json($response,200);
                                }
                            }
                        }elseif($barcode_check_sds == null && $sds_detail != null){
                            $out_cutting_check = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                            $is_ppa2 = DB::table('master_style_detail')->where('id',$sds_detail->style_id)->first();
                            if($is_ppa2 != null){
                                if($is_ppa2->is_ppa2 == false){
                                    return response()->json('Bundle Tidak Melewati Proses Ini!',422);
                                }
                            }else{
                                return response()->json('Master Style Dihapus Dari Sistem!',422);
                            }
                            $check_in_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->first();
                            $check_out_status = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->first();
                            if($state == 'in') {
                                if($check_in_status != null){
                                    return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                }else{
                                    $check_setting = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                    if($check_setting == null){
                                        return response()->json('Harap Scan Setting dahulu!',422);
                                    }else{
                                        try {
                                            DB::beginTransaction();
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => null,
                                                'factory_id'   => \Auth::user()->factory_id
                                            ]);
    
                                            DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                'current_location_id'   => $locator_id,
                                                'current_status_to'     => $state,
                                                'current_description'   => $state.' '.$locator->locator_name,
                                                'updated_movement_at'   => Carbon::now(),
                                                'current_user_id'       => \Auth::user()->id
                                            ]);
    
                                            $barcode_header = DB::table('sds_header_movements')->where('id', $sds_detail->sds_header_id)->first();
    
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $barcode_header->style,
                                                'article'       => $barcode_header->article,
                                                'po_buyer'      => $barcode_header->poreference,
                                                'part'          => $sds_detail->part_num,
                                                'komponen_name' => $sds_detail->komponen_name,
                                                'size'          => $barcode_header->size,
                                                'sticker_no'    => $sds_detail->start_no.' - '.$sds_detail->end_no,
                                                'qty'           => $sds_detail->qty,
                                                'cut'           => $barcode_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => '-'
                                            ];
    
                                            DB::commit();
                                        } catch (Exception $e) {
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                        return response()->json($response,200);
                                    }
                                }
                            }else{
                                if($check_in_status == null){
                                    return response()->json('Bundle Belum Scan In PPA2!',422);
                                }elseif($check_out_status != null){
                                    return response()->json('Bundle Sudah Scan Out PPA 2!',422);
                                }else{
                                    $barcode = DB::select("SELECT ms.season_name,shm.poreference,shm.style,t.type_name,sdm.barcode_id,sdm.sds_header_id,sdm.komponen_name,msd.is_ppa2,sdm.style_id
                                    FROM sds_detail_movements sdm
                                    LEFT JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
                                    LEFT JOIN master_style_detail msd on msd.id = sdm.style_id
                                    LEFT JOIN master_seasons ms on ms.id = msd.season
                                    LEFT JOIN types t on t.id = msd.type_id
                                    WHERE sdm.sds_header_id='$sds_detail->sds_header_id' AND msd.is_ppa2 = true AND sdm.start_no='$sds_detail->start_no' AND sdm.end_no='$sds_detail->end_no'");
                                    $barcode_group = [];
                                    foreach($barcode as $b){
                                        $barcode_group[] = $b->barcode_id;
                                    }
                                    $barcode_group = implode(',',$barcode_group);
                                    $barcode_finish = explode(',',$barcode_group);
                                    $is_all_in = DistribusiMovement::whereIn('barcode_id',$barcode_finish)->where('locator_to','7')->where('status_to','in')->pluck('barcode_id')->toArray();
                                    $check_barcode = implode(',',$is_all_in);
                                    foreach($barcode_finish as $bf){
                                        $unscanned = DB::table('sds_detail_movements')->where('sds_barcode',$bf)->first();
                                        if(strpos($check_barcode,$bf) === false){
                                            return response()->json('Barcode '.$bf.' - '.$unscanned->komponen_name.' || Sticker '.$unscanned->start_no.'-'.$unscanned->end_no.' Belum Scan In PPA2!',422);
                                        }
                                    }
                                    try {
                                        DB::beginTransaction();
                                        $description = $state.' '.$locator->locator_name;
                                        for ($i = 0; $i < count($barcode_finish); $i++) {
                                            $data_insert[] = [
                                                'id'           => Uuid::generate()->string,
                                                'barcode_id'   => $barcode_finish[$i],
                                                'deleted_at'   => null,
                                                'description'  => $description,
                                                'ip_address'   => $this->getIPClient(),
                                                'loc_dist'     => null,
                                                'locator_from' => intval($out_cutting_check->locator_to),
                                                'locator_to'   => intval($locator_id),
                                                'status_from'  => $out_cutting_check->status_to,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'created_at'   => Carbon::now(),
                                                'updated_at'   => Carbon::now(),
                                                'factory_id'   => \Auth::user()->factory_id
                                            ];
                                        }
                                        DistribusiMovement::insert($data_insert);
    
                                        DB::table('sds_detail_movements')->whereIn('barcode_id',$barcode_finish)->update([
                                            'current_location_id'    => $locator_id,
                                            'current_status_to'     => $state,
                                            'current_description'   => $state.' '.$locator->locator_name,
                                            'updated_movement_at'   => Carbon::now(),
                                            'current_user_id'       => \Auth::user()->id
                                        ]);
    
                                        $barcode_header = DB::table('sds_header_id')->where('id', $sds_detail->sds_header_id)->first();
    
                                        $response = [
                                            'barcode_id'        => $barcode_id,
                                            'style'             => $barcode_header->style,
                                            'article'           => $barcode_header->article,
                                            'po_buyer'          => $barcode_header->poreference,
                                            'part'              => $sds_detail->part_num,
                                            'komponen_name'     => $sds_detail->komponen_name,
                                            'size'              => $barcode_header->size,
                                            'sticker_no'        => $sds_detail->start_no.' - '.$sds_detail->end_no,
                                            'qty'               => $sds_detail->qty,
                                            'cut'               => $barcode_header->cut_num,
                                            'process'           => $locator->locator_name,
                                            'status'            => $state,
                                            'loc_dist'          => '-'
                                        ];
                                        DB::commit();
    
                                    } catch (Exception $e) {
                                        DB::rollback();
                                        $message = $e->getMessage();
                                        ErrorHandler::db($message);
                                    }
                                    return response()->json($response,200);
                                }
                            }
                        }
                    }
                    $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->first();
                    if($update_sds != null) {
                        ///////////////////LOCATOR SETTING SDS////////////////////////
                        if($locator->is_setting){
                            /////////////// TEMPORARRY USED FOR FACTORY AOI 2 ////////////////////
                            if(\Auth::user()->factory_id == 2){
                            
                                $barcode_check_sds = DB::table('bundle_detail as bd')
                                ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                ->leftJoin('master_style_detail as msd','msd.id','=','bd.style_detail_id')
                                ->where('bd.sds_barcode', $barcode_id)
                                ->whereNull('msd.deleted_at')
                                ->first();

                                $sds_barcode_only = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();

                                if($barcode_check_sds != null && $sds_barcode_only == null){
                                    $user          = \Auth::user()->nik;
                                    $factory       = \Auth::user()->factory_id;
                                    $dstat         = 1;
                                    $alert         = NULL;
                                    $line_location = null;

                                    $chk = $this->sds_connection->select($this->sds_connection->raw("
                                        SELECT
                                        barcode_id,TRIM(style) AS style,TRIM(set_type) AS set_type,season,TRIM(poreference) AS poreference,article,TRIM(size) AS size,cut_num,start_num,end_num,qty,
                                        proc_scan IS NULL AND supply_scan IS NULL AS not_scanned,
                                        cut_out,is_wip,supply_unlock,rejects,processing_fact,
                                        CASE WHEN is_set THEN CONCAT(TRIM(location_name),' (',TRIM(factory),')') ELSE NULL END AS is_set,
                                        CASE WHEN is_supplied THEN supplied_to ELSE NULL END AS is_supplied,
                                        CASE
                                            WHEN NOT artwork THEN 'Artwork'
                                            WHEN NOT he THEN 'Heat Transfer'
                                            WHEN NOT pad THEN 'Pad Print'
                                            WHEN NOT ppa THEN 'PPA Jahit'
                                            WHEN NOT fuse THEN 'Fuse'
                                            WHEN NOT auto THEN 'Auto'
                                            WHEN NOT bonding THEN 'Bonding'
                                            ELSE NULL
                                        END AS incomplete_proc
                                        FROM bundle_for_set_new
                                        WHERE barcode_id = '$barcode_id'::NUMERIC
                                    "));

                                    if(count($chk) > 0) {

                                        $chk = $chk[0];

                                        if($chk->not_scanned==false){
                                            $alert="Bundle sudah dipindai di SDS!";
                                            $dstat=0;
                                        }else if($chk->processing_fact != $factory){
                                            $alert="Bundle merupakan alokasi pabrik lain!";
                                            $dstat=0;
                                        }else if($chk->is_wip != ''){
                                            $alert="Bundle belum Check-Out proses ".($chk->is_wip)."!";
                                            $dstat=0;
                                        }else if($chk->supply_unlock == true){

                                        }else if($chk->is_supplied != ''){
                                            $alert="Bundle sudah di supply ke ".($chk->is_supplied)."!";
                                            $dstat=0;
                                        }else if($chk->is_set != ''){
                                            $alert="Bundle sudah menjalani Setting di ".($chk->is_set)."!";
                                            $dstat=0;
                                        }else if($chk->rejects == true){
                                            $alert="Bundle dalam proses ganti reject!";
                                            $dstat=0;
                                        }else if($chk->cut_out == false){
                                            $alert="Bundle belum checkout dari Cutting/Bundling!";
                                            $dstat=0;
                                        }else if($chk->processing_fact==2 && $chk->incomplete_proc != ''){
                                            $alert="Bundle belum menjalani proses ".($chk->incomplete_proc)."!";
                                            $dstat=0;
                                        }

                                        if($dstat > 0){

                                            // $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                            //     SELECT
                                            //     CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                            //     FROM set_pairs_alloc_new
                                            //     WHERE
                                            //     season='$chk->season'::VARCHAR AND set_type='$chk->set_type'::VARCHAR AND poreference='$chk->poreference'::VARCHAR AND article='$chk->article'::VARCHAR
                                            //     AND size='$chk->size'::VARCHAR AND cut_num='$chk->cut_num'::INTEGER AND start_num='$chk->start_num'::INTEGER AND end_num='$chk->end_num'::INTEGER
                                            // "));
                                            $set_notes = DB::connection('sds_live')->select(DB::raw("SELECT bi.style,
                                            ad.set_type,
                                            bi.poreference,
                                            bi.size,
                                            bi.cut_num,
                                            bi.start_num,
                                            bi.end_num,
                                            max(bi.qty) AS qty,
                                            max(CASE WHEN (lc.area = ANY (ARRAY[4, 5])) THEN bi.location ELSE NULL END) AS pairs_loc,
                                            max(CASE WHEN (lc.area = ANY (ARRAY[4, 5])) THEN f.factory_id ELSE NULL END) AS pairs_fact,
                                            bool_and((ts.ap_id IS NULL)) AS not_scanned,
                                            max(ts.set_notes) AS scan_loc,
                                            max(ts.factory) AS scan_fact,
                                            bool_or(ts.proc = 'MOVE') AS is_moving,
                                            bi.season,
                                            bi.article
                                            FROM (bundle_info_new bi
                                            JOIN art_desc_new ad USING (season, style, part_num, part_name)
                                            JOIN location lc ON bi.location = lc.location AND bi.factory = lc.factory
                                            LEFT JOIN factory f ON lc.factory = f.factory_name
                                            LEFT JOIN (SELECT temp_scan_proc.ap_id,
                                            temp_scan_proc.set_notes,
                                            temp_scan_proc.factory,
                                            temp_scan_proc.proc
                                            FROM temp_scan_proc
                                            WHERE temp_scan_proc.proc = ANY (ARRAY['SET', 'MOVE']) AND NOT temp_scan_proc.check_out)ts ON bi.barcode_id = ts.ap_id)
                                            where bi.season=?
                                            and bi.style=?
                                            and ad.set_type=?
                                            and bi.poreference=?
                                            and bi.article=?
                                            and bi.size=?
                                            and bi.cut_num=?
                                            and bi.start_num=?
                                            and bi.end_num=?
                                            and bi.processing_fact=?
                                            GROUP BY bi.season, bi.style, ad.set_type, bi.article, bi.poreference, bi.size, bi.cut_num, bi.start_num, bi.end_num"),[$chk->season,$chk->style,$chk->set_type,$chk->poreference,$chk->article,$chk->size,$chk->cut_num,$chk->start_num,$chk->end_num,$factory]);

                                            $set_notes=(count($set_notes) > 0 ? $set_notes[0] : NULL);

                                            if($set_notes != NULL && $set_notes->is_moving==true){
                                                $alert="Pasangan-pasangan bundle ini sedang dalam proses movement di area Setting!";
                                                $dstat=0;
                                            }else if($set_notes != NULL && $set_notes->pairs_loc != ''){
                                                $ppcm_id=NULL;
                                                $s_factory=$factory;
                                                if($set_notes->pairs_fact == $factory){
                                                    $set_notes=$set_notes->pairs_loc;
                                                }else{
                                                    $alert="Garment ini dikerjakan di ".($this->sds_connection->table('factory')->where('factory_id',$set_notes->pairs_fact)->first()->factory_name)."!";
                                                    $dstat=0;
                                                }
                                            }else if($factory==1){
                                                $ppcm_id=NULL;
                                                $set_notes=NULL;
                                                $s_factory=NULL;
                                            }else{
                                                $choose_option = 2;
                                                if($choose_option == 1) {
                                                    $set_notes=NULL;
                                                    $que = "SELECT set_helper_new AS set_helper FROM set_helper_new(?,?,?,?,?::INTEGER)";
                                                    $ppcm_id=$this->db->query($que,array(
                                                        $chk->season,
                                                        $chk->article,
                                                        $chk->set_type,
                                                        $chk->poreference,
                                                        (int)$factory
                                                    ))->result()[0]->set_helper;
                                                    $ppcm_id = ($ppcm_id==NULL || $ppcm_id=='' ? NULL : $ppcm_id);
                                                    $s_factory=NULL;
                                                } else if($choose_option == 2) {

                                                    $check_temp_setting_set = $this->sds_connection
                                                        ->table('jaz_temp_scan_setting')
                                                        ->where('style', $chk->style)
                                                        ->where('set_type', $chk->set_type)
                                                        ->where('season', $chk->season)
                                                        ->where('article', $chk->article)
                                                        ->where('poreference', $chk->poreference)
                                                        ->where('size', $chk->size)
                                                        ->where('cut_num', $chk->cut_num)
                                                        ->where('start_num', $chk->start_num)
                                                        ->where('end_num', $chk->end_num)
                                                        ->where('qty', $chk->qty)
                                                        ->where('processing_fact', $factory)
                                                        ->get();

                                                    if(count($check_temp_setting_set) > 0) {
                                                        $check_temp_setting_set = $check_temp_setting_set->first();
                                                        $ppcm_id = $check_temp_setting_set->location;
                                                    } else {
                                                        $style_array = array();
                                                        if($chk->set_type == 'TOP') {
                                                            $style_array[] = $chk->style.'-TOP';
                                                        } else if($chk->set_type == 'BOTTOM') {
                                                            $style_array[] = $chk->style.'-BOT';
                                                        } else if($chk->set_type == 'INNER') {
                                                            $style_array[] = $chk->style.'-INN';
                                                            $style_array[] = $chk->style.'-TIN';
                                                        } else if($chk->set_type == 'OUTER') {
                                                            $style_array[] = $chk->style.'-OUT';
                                                            $style_array[] = $chk->style.'-TOU';
                                                        } else {
                                                            $style_array[] = $chk->style;
                                                        }

                                                        $get_ppcm_ids = $this->sds_connection
                                                            ->table('jaz_ppcm_upload_new')
                                                            ->whereIn('style', $style_array)
                                                            ->whereNull('deleted_at')
                                                            ->where('poreference', $chk->poreference)
                                                            ->where('season', $chk->season)
                                                            ->where('article', $chk->article)
                                                            ->get();

                                                        if(count($get_ppcm_ids) > 0) {
                                                            $selisih = 10000000;
                                                            $ppcm_id = null;
                                                            foreach($get_ppcm_ids as $data_ppcm) {

                                                                $qty_temp = $this->sds_connection->select($this->sds_connection->raw("select sum(qty) as qty from jaz_temp_scan_setting where location = '$data_ppcm->id'::numeric"))[0]->qty;

                                                                $qty_mod = $qty_temp + $data_ppcm->qty_mod;

                                                                if($data_ppcm->qty_split > $qty_mod) {

                                                                    $get_selisih = abs(($data_ppcm->qty_split - $qty_mod) - $chk->qty);
                                                                    if($get_selisih < $selisih) {
                                                                        $selisih = $get_selisih;
                                                                        $ppcm_id = $data_ppcm->id;
                                                                    }

                                                                }
                                                            }
                                                        } else {
                                                            $ppcm_id = null;
                                                        }
                                                    }
                                                    $s_factory=NULL;
                                                    $set_notes=NULL;
                                                }
                                            }

                                            if($dstat > 0){
                                                $check_temp_setting = $this->sds_connection->table('temp_scan_proc')->insert([
                                                    'user_id'   => $user,
                                                    'proc'      => 'SET',
                                                    'ap_id'     => $barcode_id,
                                                    'set_notes' => $set_notes,
                                                    'factory'   => $s_factory,
                                                    'ppcm_id'   => $ppcm_id
                                                ]);

                                                $j=0;

                                                // NEW ENTRIES
                                                $bc_list = $this->sds_connection->select($this->sds_connection->raw("
                                                    SELECT ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,
                                                    TRIM(lc.location) AS location,TRIM(lc.factory) AS factory,
                                                    MAX(bi.qty) AS qty,COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id
                                                    FROM (
                                                    SELECT ap_id,ppcm_id
                                                    FROM temp_scan_proc
                                                    WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NOT NULL AND NOT check_out
                                                    ) ts
                                                    JOIN bundle_info_new bi ON ts.ap_id=bi.barcode_id
                                                    JOIN ppcm_upload_new pu ON ts.ppcm_id=pu.id
                                                    JOIN location lc ON pu.line ~ CONCAT(E'^',REPLACE(TRIM(lc.factory),' ',E'#'),' ',REPLACE(TRIM(lc.location_name),' ','0?'),E'$')
                                                    GROUP BY ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,lc.location,lc.factory
                                                "));

                                                foreach($bc_list as $bc){
                                                    $this->sds_connection->select($this->sds_connection->raw("UPDATE ppcm_upload_new SET qty_mod=qty_mod+'$bc->qty' WHERE id='$bc->ppcm_id'"));

                                                    $cond = explode(',',$bc->bc_id);
                                                    $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                        'location'=>$bc->location,
                                                        'factory'=>$bc->factory,
                                                        'setting'=>Carbon::now(),
                                                        'setting_pic'=>$user,
                                                        'supply_unlock'=>false
                                                    ]);
                                                    $line_location = $bc->location;
                                                    $j+=$bc->count;
                                                }

                                                $bc_list = $this->sds_connection->select($this->sds_connection->raw("SELECT COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id,ts.set_notes,f.factory_name FROM (SELECT ap_id,set_notes,factory FROM temp_scan_proc WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NULL AND NOT check_out) ts JOIN factory f ON ts.factory=f.factory_id GROUP BY ts.set_notes,f.factory_name"));

                                                foreach($bc_list as $bc){

                                                    $cond = explode(',',$bc->bc_id);
                                                    $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                        'location'=>$bc->set_notes,
                                                        'factory'=>$bc->factory_name,
                                                        'setting'=> Carbon::now(),
                                                        'setting_pic'=>$user,
                                                        'supply_unlock'=>false
                                                    ]);
                                                    $line_location = $bc->set_notes;
                                                    $j+=$bc->count;
                                                }
                                                $is_scanned = DB::table('distribusi_movements')->where('barcode_id',$barcode_check_sds->barcode_id)->where('description','in SETTING')->first();
                                                if($is_scanned != null){
                                                    return response()->json('Barcode Sudah Scan Setting',422);
                                                }elseif($line_location != null){
                                                    try{
                                                        DB::beginTransaction();
                                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                            'is_out_setting'        => 'Y',
                                                            'out_setting_at'        => Carbon::now(),
                                                            'current_locator_id'    => $locator_id,
                                                            'current_status_to'     => $state,
                                                            'current_description'   => 'in SETTING',
                                                            'updated_movement_at'   => Carbon::now(),
                                                            'current_user_id'       => \Auth::user()->id
                                                        ]);
                                                            $data_insert[] =[
                                                                'id'            => Uuid::generate()->string,
                                                                'barcode_id'    => $barcode_check_sds->barcode_id,
                                                                'deleted_at'    => null,
                                                                'description'   => 'in SETTING',
                                                                'ip_address'    => $this->getIPClient(),
                                                                'loc_dist'      => $line_location,
                                                                'locator_from'  => intval($barcode_check_sds->current_locator_id),
                                                                'locator_to'    => $locator_id,
                                                                'status_from'   => $barcode_check_sds->current_status_to,
                                                                'status_to'     => $state,
                                                                'user_id'       => \Auth::user()->id,
                                                                'created_at'    => Carbon::now(),
                                                                'updated_at'    => Carbon::now(),
                                                                'factory_id'   => \Auth::user()->factory_id
                                                            ];
                                                            $check_double = DB::table('distribusi_movements')->where('barcode_id',$barcode_check_sds->barcode_id)->where('description','in SETTING')->first();
                                                            if($check_double != null){
                                                                return response()->json('Bundle Sudah Scan Setting!',422);
                                                            }else{
                                                                DistribusiMovement::insert($data_insert);
                                                            }
                                                    DB::commit();
                                                    }catch (Exception $e) {
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                }

                                                $this->sds_connection->table('system_log')->insert([
                                                    'user_nik' => $user,
                                                    'user_proc' => 'SET',
                                                    'log_operation' => 'set_checkin',
                                                    'log_text' => 'add item '.$barcode_id.' to SET check in',
                                                ]);
                                            }
                                        }

                                    } else {
                                        $alert="Bundle tidak terdaftar!";
                                        $dstat=0;
                                    }


                                    if($dstat == 1) {
                                        //gak ada di ppcm
                                        if($line_location == null) {
                                            $set_type = DB::connection('sds_live')->table('art_desc_new')
                                            ->where('season',$update_sds->season)
                                            ->where('style',$update_sds->style)
                                            ->where('part_num',$update_sds->part_num)
                                            ->where('part_name',$update_sds->part_name)
                                            ->first();
                                            if($set_type != null){
                                                if($set_type->set_type == 'TOP'){
                                                    $style_set = $update_sds->style.'-TOP';
                                                }elseif($set_type->set_type == 'BOTTOM'){
                                                    $style_set = $update_sds->style.'-BOT';
                                                }else{
                                                    $style_set = $update_sds->style;
                                                }
                                            }else{
                                                $style_set = $update_sds->style;
                                            }
                                            $ppcm_data = DB::connection('sds_live')->table('ppcm_upload_new')
                                            ->where('poreference',$update_sds->poreference)
                                            ->where('style',$style_set)
                                            ->where('article',$update_sds->article)
                                            ->whereNull('deleted_at')
                                            ->first();
                                            $obj = new StdClass;
                                            $obj->barcode_id = $barcode_id;
                                            $obj->line       = $ppcm_data != null ? $ppcm_data->line : '';
                                            return response()->json($obj, 331);
                                        } else {
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $update_sds->style,
                                                'article'       => $update_sds->article,
                                                'po_buyer'      => $update_sds->poreference,
                                                'part'          => $update_sds->part_num,
                                                'komponen_name' => $update_sds->part_name,
                                                'size'          => $update_sds->size,
                                                'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                'qty'           => $update_sds->qty,
                                                'cut'           => $update_sds->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $line_location,
                                            ];

                                            $this->sds_connection->table('temp_scan_proc')
                                                ->where('user_id' ,$user)
                                                ->where('proc','SET')
                                                ->where('check_out',false)
                                                ->delete();

                                            return response()->json($response,200);
                                        }
                                    } else {
                                        return response()->json($alert,422);
                                    }
                                }elseif($update_sds != null && $barcode_check_sds == null && $sds_barcode_only != null){
                                    $user          = \Auth::user()->nik;
                                    $factory       = \Auth::user()->factory_id;
                                    $dstat         = 1;
                                    $alert         = NULL;
                                    $line_location = null;

                                    $chk = $this->sds_connection->select($this->sds_connection->raw("
                                        SELECT
                                        barcode_id,TRIM(style) AS style,TRIM(set_type) AS set_type,season,TRIM(poreference) AS poreference,article,TRIM(size) AS size,cut_num,start_num,end_num,qty,
                                        proc_scan IS NULL AND supply_scan IS NULL AS not_scanned,
                                        cut_out,is_wip,supply_unlock,rejects,processing_fact,
                                        CASE WHEN is_set THEN CONCAT(TRIM(location_name),' (',TRIM(factory),')') ELSE NULL END AS is_set,
                                        CASE WHEN is_supplied THEN supplied_to ELSE NULL END AS is_supplied,
                                        CASE
                                            WHEN NOT artwork THEN 'Artwork'
                                            WHEN NOT he THEN 'Heat Transfer'
                                            WHEN NOT pad THEN 'Pad Print'
                                            WHEN NOT ppa THEN 'PPA Jahit'
                                            WHEN NOT fuse THEN 'Fuse'
                                            WHEN NOT auto THEN 'Auto'
                                            WHEN NOT bonding THEN 'Bonding'
                                            ELSE NULL
                                        END AS incomplete_proc
                                        FROM bundle_for_set_new
                                        WHERE barcode_id = '$barcode_id'::NUMERIC
                                    "));

                                    if(count($chk) > 0) {

                                        $chk = $chk[0];

                                        if($chk->not_scanned==false){
                                            $alert="Bundle sudah dipindai di SDS!";
                                            $dstat=0;
                                        }else if($chk->processing_fact != $factory){
                                            $alert="Bundle merupakan alokasi pabrik lain!";
                                            $dstat=0;
                                        }else if($chk->is_wip != ''){
                                            $alert="Bundle belum Check-Out proses ".($chk->is_wip)."!";
                                            $dstat=0;
                                        }else if($chk->supply_unlock == true){

                                        }else if($chk->is_supplied != ''){
                                            $alert="Bundle sudah di supply ke ".($chk->is_supplied)."!";
                                            $dstat=0;
                                        }else if($chk->is_set != ''){
                                            $alert="Bundle sudah menjalani Setting di ".($chk->is_set)."!";
                                            $dstat=0;
                                        }else if($chk->rejects == true){
                                            $alert="Bundle dalam proses ganti reject!";
                                            $dstat=0;
                                        }else if($chk->cut_out == false){
                                            $alert="Bundle belum checkout dari Cutting/Bundling!";
                                            $dstat=0;
                                        }else if($chk->processing_fact==2 && $chk->incomplete_proc != ''){
                                            $alert="Bundle belum menjalani proses ".($chk->incomplete_proc)."!";
                                            $dstat=0;
                                        }

                                        if($dstat > 0){

                                            // $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                            //     SELECT
                                            //     CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                            //     FROM set_pairs_alloc_new
                                            //     WHERE
                                            //     season='$chk->season'::VARCHAR AND set_type='$chk->set_type'::VARCHAR AND poreference='$chk->poreference'::VARCHAR AND article='$chk->article'::VARCHAR
                                            //     AND size='$chk->size'::VARCHAR AND cut_num='$chk->cut_num'::INTEGER AND start_num='$chk->start_num'::INTEGER AND end_num='$chk->end_num'::INTEGER
                                            // "));
                                            $set_notes = DB::connection('sds_live')->select(DB::raw("SELECT CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                            from(SELECT bi.style,
                                            ad.set_type,
                                            bi.poreference,
                                            bi.size,
                                            bi.cut_num,
                                            bi.start_num,
                                            bi.end_num,
                                            max(bi.qty) AS qty,
                                            max(CASE WHEN lc.area = ANY (ARRAY[4,5]) THEN bi.location ELSE NULL END) AS pairs_loc,
                                            max(CASE WHEN lc.area = ANY (ARRAY[4,5]) THEN f.factory_id ELSE NULL END) AS pairs_fact,
                                            bool_and((ts.ap_id IS NULL)) AS not_scanned,
                                            max(ts.set_notes) AS scan_loc,
                                            max(ts.factory) AS scan_fact,
                                            bool_or(ts.proc = 'MOVE') AS is_moving,
                                            bi.season,
                                            bi.article
                                            FROM bundle_info_new bi
                                            JOIN art_desc_new ad USING (season, style, part_num, part_name)
                                            JOIN location lc ON bi.location = lc.location AND bi.factory = lc.factory
                                            LEFT JOIN factory f ON lc.factory = f.factory_name
                                            LEFT JOIN ( SELECT temp_scan_proc.ap_id,
                                            temp_scan_proc.set_notes,
                                            temp_scan_proc.factory,
                                            temp_scan_proc.proc
                                            FROM temp_scan_proc
                                            WHERE temp_scan_proc.proc = ANY (ARRAY['SET', 'MOVE']) AND NOT temp_scan_proc.check_out) ts ON bi.barcode_id = ts.ap_id
                                            where bi.season=?
                                            AND ad.set_type=?
                                            AND bi.poreference=?
                                            AND bi.style=?
                                            AND bi.article=?
                                            AND bi.size=?
                                            AND bi.cut_num=?
                                            AND bi.start_num=?
                                            AND bi.end_num=?
                                            AND bi.processing_fact=?
                                            GROUP BY bi.season, bi.style, ad.set_type, bi.article, bi.poreference, bi.size, bi.cut_num, bi.start_num, bi.end_num,bi.processing_fact
                                            )x"),[$chk->season,$chk->set_type,$chk->poreference,$chk->style,$chk->article,$chk->size,$chk->cut_num,$chk->start_num,$chk->end_num,$factory]);
                                            $set_notes=(count($set_notes) > 0 ? $set_notes[0] : NULL);

                                            if($set_notes != NULL && $set_notes->is_moving==true){
                                                $alert="Pasangan-pasangan bundle ini sedang dalam proses movement di area Setting!";
                                                $dstat=0;
                                            }else if($set_notes != NULL && $set_notes->sugg_fact != ''){
                                                $ppcm_id=NULL;
                                                $s_factory=$factory;
                                                if($set_notes->sugg_fact == $factory){
                                                    $set_notes=$set_notes->sugg_loc;
                                                }else{
                                                    $alert="Garment ini dikerjakan di ".($this->sds_connection->table('factory')->where('factory_id',$set_notes->sugg_fact)->first()->factory_name)."!";
                                                    $dstat=0;
                                                }
                                            }else if($factory==1){
                                                $ppcm_id=NULL;
                                                $set_notes=NULL;
                                                $s_factory=NULL;
                                            }else{
                                                $choose_option = 2;
                                                if($choose_option == 1) {
                                                    $set_notes=NULL;
                                                    $que = "SELECT set_helper_new AS set_helper FROM set_helper_new(?,?,?,?,?::INTEGER)";
                                                    $ppcm_id=$this->db->query($que,array(
                                                        $chk->season,
                                                        $chk->article,
                                                        $chk->set_type,
                                                        $chk->poreference,
                                                        (int)$factory
                                                    ))->result()[0]->set_helper;
                                                    $ppcm_id = ($ppcm_id==NULL || $ppcm_id=='' ? NULL : $ppcm_id);
                                                    $s_factory=NULL;
                                                } else if($choose_option == 2) {

                                                    $check_temp_setting_set = $this->sds_connection
                                                        ->table('jaz_temp_scan_setting')
                                                        ->where('style', $chk->style)
                                                        ->where('set_type', $chk->set_type)
                                                        ->where('season', $chk->season)
                                                        ->where('article', $chk->article)
                                                        ->where('poreference', $chk->poreference)
                                                        ->where('size', $chk->size)
                                                        ->where('cut_num', $chk->cut_num)
                                                        ->where('start_num', $chk->start_num)
                                                        ->where('end_num', $chk->end_num)
                                                        ->where('qty', $chk->qty)
                                                        ->where('processing_fact', $factory)
                                                        ->get();

                                                    if(count($check_temp_setting_set) > 0) {
                                                        $check_temp_setting_set = $check_temp_setting_set->first();
                                                        $ppcm_id = $check_temp_setting_set->location;
                                                    } else {
                                                        $style_array = array();
                                                        if($chk->set_type == 'TOP') {
                                                            $style_array[] = $chk->style.'-TOP';
                                                        } else if($chk->set_type == 'BOTTOM') {
                                                            $style_array[] = $chk->style.'-BOT';
                                                        } else if($chk->set_type == 'INNER') {
                                                            $style_array[] = $chk->style.'-INN';
                                                            $style_array[] = $chk->style.'-TIN';
                                                        } else if($chk->set_type == 'OUTER') {
                                                            $style_array[] = $chk->style.'-OUT';
                                                            $style_array[] = $chk->style.'-TOU';
                                                        } else {
                                                            $style_array[] = $chk->style;
                                                        }

                                                        $get_ppcm_ids = $this->sds_connection
                                                            ->table('jaz_ppcm_upload_new')
                                                            ->whereIn('style', $style_array)
                                                            ->whereNull('deleted_at')
                                                            ->where('poreference', $chk->poreference)
                                                            ->where('season', $chk->season)
                                                            ->where('article', $chk->article)
                                                            ->get();

                                                        if(count($get_ppcm_ids) > 0) {
                                                            $selisih = 10000000;
                                                            $ppcm_id = null;
                                                            foreach($get_ppcm_ids as $data_ppcm) {

                                                                $qty_temp = $this->sds_connection->select($this->sds_connection->raw("select sum(qty) as qty from jaz_temp_scan_setting where location = '$data_ppcm->id'::numeric"))[0]->qty;

                                                                $qty_mod = $qty_temp + $data_ppcm->qty_mod;

                                                                if($data_ppcm->qty_split > $qty_mod) {

                                                                    $get_selisih = abs(($data_ppcm->qty_split - $qty_mod) - $chk->qty);
                                                                    if($get_selisih < $selisih) {
                                                                        $selisih = $get_selisih;
                                                                        $ppcm_id = $data_ppcm->id;
                                                                    }

                                                                }
                                                            }
                                                        } else {
                                                            $ppcm_id = null;
                                                        }
                                                    }
                                                    $s_factory=NULL;
                                                    $set_notes=NULL;
                                                }
                                            }

                                            if($dstat > 0){
                                                $check_temp_setting = $this->sds_connection->table('temp_scan_proc')->insert([
                                                    'user_id'   => $user,
                                                    'proc'      => 'SET',
                                                    'ap_id'     => $barcode_id,
                                                    'set_notes' => $set_notes,
                                                    'factory'   => $s_factory,
                                                    'ppcm_id'   => $ppcm_id
                                                ]);

                                                $j=0;

                                                // NEW ENTRIES
                                                $bc_list = $this->sds_connection->select($this->sds_connection->raw("
                                                    SELECT ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,
                                                    TRIM(lc.location) AS location,TRIM(lc.factory) AS factory,
                                                    MAX(bi.qty) AS qty,COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id
                                                    FROM (
                                                    SELECT ap_id,ppcm_id
                                                    FROM temp_scan_proc
                                                    WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NOT NULL AND NOT check_out
                                                    ) ts
                                                    JOIN bundle_info_new bi ON ts.ap_id=bi.barcode_id
                                                    JOIN ppcm_upload_new pu ON ts.ppcm_id=pu.id
                                                    JOIN location lc ON pu.line ~ CONCAT(E'^',REPLACE(TRIM(lc.factory),' ',E'#'),' ',REPLACE(TRIM(lc.location_name),' ','0?'),E'$')
                                                    GROUP BY ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,lc.location,lc.factory
                                                "));

                                                foreach($bc_list as $bc){
                                                    $this->sds_connection->select($this->sds_connection->raw("UPDATE ppcm_upload_new SET qty_mod=qty_mod+'$bc->qty' WHERE id='$bc->ppcm_id'"));

                                                    $cond = explode(',',$bc->bc_id);
                                                    $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                        'location'=>$bc->location,
                                                        'factory'=>$bc->factory,
                                                        'setting'=>Carbon::now(),
                                                        'setting_pic'=>$user,
                                                        'supply_unlock'=>false
                                                    ]);
                                                    $line_location = $bc->location;
                                                    $j+=$bc->count;
                                                }

                                                $bc_list = $this->sds_connection->select($this->sds_connection->raw("SELECT COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id,ts.set_notes,f.factory_name FROM (SELECT ap_id,set_notes,factory FROM temp_scan_proc WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NULL AND NOT check_out) ts JOIN factory f ON ts.factory=f.factory_id GROUP BY ts.set_notes,f.factory_name"));

                                                foreach($bc_list as $bc){

                                                    $cond = explode(',',$bc->bc_id);
                                                    $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                        'location'=>$bc->set_notes,
                                                        'factory'=>$bc->factory_name,
                                                        'setting'=> Carbon::now(),
                                                        'setting_pic'=>$user,
                                                        'supply_unlock'=>false
                                                    ]);
                                                    $line_location = $bc->set_notes;
                                                    $j+=$bc->count;
                                                }
                                                //RUNNING THIS CODE AFTER SDS UPDATE
                                                $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                                ->where('barcode_id', $barcode_id)
                                                ->where('processing_fact',\Auth::user()->factory_id)
                                                ->first();
                                                $sds_was_scanned = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_id)->first();
                                                $cdms_was_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                                if($sds_was_scanned->distrib_received == null && $sds_was_scanned->bd_pic == null){
                                                    return response()->json('Barcode Belum Scan Out Cutting',422);
                                                }elseif($sds_was_scanned->setting != null && $sds_was_scanned->setting_pic != null && $cdms_was_scanned != null){
                                                    return response()->json('Barcode Sudah Setting di '.$sds_was_scanned->location,422);
                                                }else{
                                                    if(trim($sds_header->set_type) == 'TOP'){
                                                        $set_type = 'TOP';
                                                    }elseif(trim($sds_header->set_type) == 'BOTTOM'){
                                                        $set_type = 'BOTTOM';
                                                    }else{
                                                        $set_type = 'Non';
                                                    }
                
                                                    try{
                                                        DB::beginTransaction();
                                                        $data_insert_header[] = [
                                                            'id'            => Uuid::generate()->string,
                                                            'season'        => $sds_header->season,
                                                            'style'         => trim($sds_header->style),
                                                            'set_type'      => trim($set_type),
                                                            'poreference'   => $sds_header->poreference,
                                                            'article'       => $sds_header->article,
                                                            'cut_num'       => $sds_header->cut_num,
                                                            'size'          => $sds_header->size,
                                                            'factory_id'    => (int)$sds_header->processing_fact,
                                                            'cutting_date'  => null,
                                                            'color_name'    => trim($sds_header->coll),
                                                            'created_by'    => \Auth::user()->id,
                                                            'created_at'    => $sds_header->setting,
                                                            'updated_at'    => Carbon::now(),
                                                            'updated_by'    => null,
                                                            'sticker'       => $sds_header->start_num.'-'.$sds_header->end_num
                                                        ];
                                                        $header_exists = DB::table('sds_header_movements')
                                                        ->where('poreference',$sds_header->poreference)
                                                        ->where('style',$sds_header->style)
                                                        ->where('set_type',$set_type)
                                                        ->where('season',$sds_header->season)
                                                        ->where('cut_num',$sds_header->cut_num)
                                                        ->where('size',$sds_header->size)
                                                        ->where('factory_id',$sds_header->processing_fact)
                                                        ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                        ->exists();
                                                        if(!$header_exists){
                                                            DB::table('sds_header_movements')->insert($data_insert_header);
                                                        }else{
                                                            DB::table('sds_header_movements')
                                                            ->where('poreference',$sds_header->poreference)
                                                            ->where('style',$sds_header->style)
                                                            ->where('set_type',$set_type)
                                                            ->where('season',$sds_header->season)
                                                            ->where('cut_num',$sds_header->cut_num)
                                                            ->where('size',$sds_header->size)
                                                            ->where('factory_id',$sds_header->processing_fact)
                                                            ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                            ->update([
                                                                'updated_at'        => Carbon::now(),
                                                                'updated_by'        => \Auth::user()->id
                                                            ]);
                                                        }
                                                        
                                                        $get_id_sds_header_supply = DB::table('sds_header_movements')
                                                        ->where('season',$sds_header->season)
                                                        ->where('poreference',$sds_header->poreference)
                                                        ->where('article',$sds_header->article)
                                                        ->where('cut_num',$sds_header->cut_num)
                                                        ->where('size',$sds_header->size)
                                                        ->where('style',$sds_header->style)
                                                        ->where('set_type',$set_type)
                                                        ->first()->id;  
                                                        $style_detail_id = DB::table('master_style_detail as msd')
                                                        ->select(DB::raw('msd.id,msd.process'))
                                                        ->leftJoin('master_seasons as ms','ms.id','=','msd.season')
                                                        ->leftJoin('types as t','t.id','=','msd.type_id')
                                                        ->leftJoin('master_komponen as mk','mk.id','=','msd.komponen')
                                                        ->where('msd.style',$sds_header->style)
                                                        ->where('t.type_name',$set_type)
                                                        ->where('ms.season_name',$sds_header->season)
                                                        ->where('msd.part',$sds_header->part_num)
                                                        ->where('mk.komponen_name',trim($sds_header->part_name))
                                                        ->whereNull('msd.deleted_at')->first();      
                                                        $secondary_process = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_id)->first();
                                                        $process = $secondary_process == null ? null : $secondary_process->note;
                                                        $process_id = $style_detail_id == null ? null : $style_detail_id->process;
                                                        $is_out_cutting = $sds_header->out_cutting == null ? null : $sds_header->out_cutting;  
                                                        $style_id = $style_detail_id == null ? null : $style_detail_id->id;                          
                                                        
                                                        $data_insert_detail[] = [
                                                            'sds_header_supply'     => $get_id_sds_header_supply,
                                                            'sds_barcode'           => $barcode_id,
                                                            'komponen_name'         => trim($sds_header->part_name),
                                                            'part_num'              => $sds_header->part_num,
                                                            'start_no'              => $sds_header->start_num,
                                                            'end_no'                => $sds_header->end_num,
                                                            'qty'                   => $sds_header->qty,
                                                            'factory_id'            => $sds_header->processing_fact,
                                                            'job'                   => $sds_header->style."::".$sds_header->poreference."::".$sds_header->article,
                                                            'is_recycle'            => null,
                                                            'is_out_cutting'        => 'Y',
                                                            'is_out_cutting_at'     => $sds_header->out_cutting,
                                                            'out_setting_at'        => Carbon::now(),
                                                            'is_out_setting'        => 'Y',
                                                            'current_location_id'   => $locator_id,
                                                            'current_status_to'     => $state,
                                                            'current_description'   => 'in SETTING',
                                                            'updated_movement_at'   => Carbon::now(),
                                                            'current_user_id'       => \Auth::user()->id,
                                                            'secondary_process'     => $process,
                                                            'style_id'              => $style_id,
                                                            'process_id'            => $process_id
                                                        ];
                                                        
                                                        $last_locator = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                                                        $locator_from = $last_locator == null ? null : $last_locator->current_location_id;
                                                        $status_to = $last_locator == null ? null : $last_locator->current_status_to;
                                                        $data_insert[] = [
                                                            'id'            => Uuid::generate()->string,
                                                            'barcode_id'    => $barcode_id,
                                                            'deleted_at'    => null,
                                                            'description'   => 'in SETTING',
                                                            'ip_address'    => $this->getIPClient(),
                                                            'loc_dist'      => $line_location,
                                                            'locator_from'  => $locator_from,
                                                            'locator_to'    => $locator_id,
                                                            'status_from'   => $status_to,
                                                            'status_to'     => $state,
                                                            'user_id'       => \Auth::user()->id,
                                                            'created_at'    => Carbon::now(),
                                                            'updated_at'    => Carbon::now(),
                                                            'factory_id'   => \Auth::user()->factory_id
                                                        ];
                                                        $data_detail_exists = DB::table('sds_detail_movements')
                                                        ->where('sds_barcode',$barcode_id)->exists();
                                                        if(!$data_detail_exists){
                                                            DB::table('sds_detail_movements')->insert($data_insert_detail);
                                                        }else{
                                                            DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                'out_setting_at'            => Carbon::now(),
                                                                'is_out_setting'            => 'Y',
                                                                'current_location_id'       => $locator_id,
                                                                'current_status_to'         => $state,
                                                                'current_description'       => 'in SETTING',
                                                                'updated_movement_at'       => Carbon::now(),
                                                                'current_user_id'           => \Auth::user()->id
                                                            ]);
                                                        }
                                                        $check_double = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('description','in SETTING')->first();
                                                        if($check_double != null){
                                                            return response()->json('Barcode Sudah Scan Setting',422);
                                                        }else{
                                                            DB::table('distribusi_movements')->insert($data_insert);
                                                        }
                                                        DB::commit();
                                                    }catch(Exception $e){
                                                        DB::rollback();
                                                        $message = $e->getMessage();
                                                        ErrorHandler::db($message);
                                                    }
                                                }

                                                $this->sds_connection->table('system_log')->insert([
                                                    'user_nik' => $user,
                                                    'user_proc' => 'SET',
                                                    'log_operation' => 'set_checkin',
                                                    'log_text' => 'add item '.$barcode_id.' to SET check in',
                                                ]);
                                            }
                                        }

                                    } else {
                                        $alert="Bundle tidak terdaftar!";
                                        $dstat=0;
                                    }


                                    if($dstat == 1) {
                                        //gak ada di ppcm
                                        if($line_location == null) {
                                            $set_type = DB::connection('sds_live')->table('art_desc_new')
                                            ->where('season',$update_sds->season)
                                            ->where('style',$update_sds->style)
                                            ->where('part_num',$update_sds->part_num)
                                            ->where('part_name',$update_sds->part_name)
                                            ->first();
                                            if($set_type != null){
                                                if($set_type->set_type == 'TOP'){
                                                    $style_set = $update_sds->style.'-TOP';
                                                }elseif($set_type->set_type == 'BOTTOM'){
                                                    $style_set = $update_sds->style.'-BOT';
                                                }else{
                                                    $style_set = $update_sds->style;
                                                }
                                            }else{
                                                $style_set = $update_sds->style;
                                            }
                                            $ppcm_data = DB::connection('sds_live')->table('ppcm_upload_new')
                                                ->where('poreference',$update_sds->poreference)
                                                ->where('style',$style_set)
                                                ->where('article',$update_sds->article)
                                                ->whereNull('deleted_at')
                                                ->first();
                                                $obj = new StdClass;
                                                $obj->barcode_id = $barcode_id;
                                                $obj->line       = $ppcm_data != null ? $ppcm_data->line : '';
                                            return response()->json($obj, 331);
                                        } else {
                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $update_sds->style,
                                                'article'       => $update_sds->article,
                                                'po_buyer'      => $update_sds->poreference,
                                                'part'          => $update_sds->part_num,
                                                'komponen_name' => $update_sds->part_name,
                                                'size'          => $update_sds->size,
                                                'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                'qty'           => $update_sds->qty,
                                                'cut'           => $update_sds->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $line_location,
                                            ];

                                            $this->sds_connection->table('temp_scan_proc')
                                                ->where('user_id' ,$user)
                                                ->where('proc','SET')
                                                ->where('check_out',false)
                                                ->delete();

                                            return response()->json($response,200);
                                        }
                                    } else {
                                        return response()->json($alert,422);
                                    }
                                }else{
                                    return response()->json('Barcode Tidak Ditemukan',422);
                                }
                                /////////////////// NEW CODE /////////////////////////
                                // $barcode_check      = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                                // $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode', $barcode_id)->first();
                                // $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                                // $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
                                // $barcode_sds_bi     = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_id)->first();
                                // if($barcode_sds != null && $barcode_sds_only == null){
                                //     $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
                                //     $bundle_info_sds = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',10)->first();
                                //     $nik_user   = DB::table('users')->where('id',\Auth::user()->id)->first();
                                //     $nik_sds    = DB::connection('sds_live')->table('m_user')->where('nik',$nik_user->nik)->first();
                                //     $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->whereNull('deleted_at')->first();
                                //     if($nik_sds == null){
                                //         return response()->json('ANDA TIDAK MEMILIKI AKSES DI SDS!',422);
                                //     }
                                //     $last_loc = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                //     if($bundle_info_sds == null){
                                //         return response()->json('Barcode Belum Scan Out Cutting!',422);
                                //     }elseif($last_loc != null){
                                //         return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_loc->loc_dist,422);
                                //     }else{
                                //         $process    = DB::table('v_master_style')->where('id',$barcode_sds->style_detail_id)->first();
                                //         //JIKA TERDAPAT PROSES SECONDARY MAKA AKAN DI LAKUKAN PENGECEKAN SUDAH SELESAI ATAU BELUM
                                //         $last_move = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                //         $type_name = DB::table('types')->where('id',$komponen_detail->type_id)->first();
                                                                        
                                //         $get_locator_set_cdms = DB::table('distribusi_movements as dm')
                                //         ->select('dm.barcode_id','bh.season','bh.style','bh.poreference','bh.article','vms.type_name','dm.loc_dist','bh.size')
                                //         ->Join('bundle_detail as bd','bd.barcode_id','=','dm.barcode_id')
                                //         ->Join('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                //         ->Join('v_master_style as vms','vms.id','=','bd.style_detail_id')
                                //         ->where('bh.poreference',$barcode_header->poreference)
                                //         ->where('bh.style',$barcode_header->style)
                                //         ->where('bh.season',$barcode_header->season)
                                //         ->where('bh.article',$barcode_header->article)
                                //         ->where('vms.type_name',$type_name->type_name)
                                //         ->where('bh.cut_num',$barcode_header->cut_num)
                                //         ->where('bh.size',$barcode_header->size)
                                //         ->where('bd.start_no',$barcode_sds->start_no)
                                //         ->where('dm.locator_to','11')
                                //         ->where('dm.status_to','in')
                                //         ->OrderBy('dm.created_at','DESC')
                                //         ->first();
        
                                //         $type_name = $type_name->type_name == 'Non' ? 0 : $type_name->type_name;
        
                                //         $get_locator_set_sds = DB::connection('sds_live')->table('bundle_info_new as bi')
                                //         ->leftJoin('art_desc_new as ad',function($join){
                                //             $join->on('ad.season','=','bi.season');
                                //             $join->on('ad.style','=','bi.style');
                                //             $join->on('ad.part_num','=','bi.part_num');
                                //             $join->on('ad.part_name','=','bi.part_name');
                                //         })
                                //         ->where('bi.poreference',$barcode_header->poreference)
                                //         ->where('bi.style',$barcode_header->style)
                                //         ->where('bi.season',$barcode_header->season)
                                //         ->where('bi.article',$barcode_header->article)
                                //         ->where('ad.set_type',$type_name)             
                                //         ->where('bi.cut_num',$barcode_header->cut_num)
                                //         ->where('size',$barcode_header->size)
                                //         ->where('bi.start_num',$barcode_sds->start_no)
                                //         ->whereNotNull('setting')
                                //         ->orderBy('setting','DESC')
                                //         ->first();
        
                                //         $linkage_bi = $get_locator_set_cdms == null ? $get_locator_set_sds : $get_locator_set_cdms;
                                    
                                //         if($process->process != null){
                                //             $proc_id        = explode(',',$process->process);
                                //             $loc_id_temp    = DB::table('master_process')->select('locator_id')->whereIn('id',$proc_id)->whereNull('deleted_at')->distinct('locator_id')->get();
                                //             foreach($loc_id_temp as $lit){
                                //                 $locator_names = DB::table('master_locator')->where('id',$lit->locator_id)->whereNull('deleted_at')->first();
                                //                 $loc_arr[] = $lit->locator_id;
                                //                 if($barcode_header->factory_id == '1'){
                                //                     if(in_array(3,$loc_arr)){
                                //                         $scan_sds_art = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->first();
                                //                         if($scan_sds_art->artwork == null){
                                //                             return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                //                         }
                                //                         if($scan_sds_art->artwork_out == null){
                                //                             return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                //                         }
                                //                     }
                                //                 }else{
                                //                     $in_status     = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','in')->first();
                                //                     $out_status    = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to',$lit->locator_id)->where('status_to','out')->first();
                                //                     if($in_status == null){
                                //                         return response()->json('Bundle Belum Scan In'.' - '.$locator_names->locator_name,422);
                                //                     }
                                //                     if($out_status == null){
                                //                         return response()->json('Bundle Belum Scan Out'.' - '.$locator_names->locator_name,422);
                                //                     }
                                //                 }
                                //             }
                                //             if($linkage_bi == null){
                                //                 return response()->json($barcode_sds->barcode_id, 331);
                                //             }else{
                                //                 $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds->location : $get_locator_set_cdms->loc_dist;
                                //                 try{
                                //                     DB::beginTransaction();
                                //                         DistribusiMovement::FirstOrCreate([
                                //                             'barcode_id'   => $barcode_sds->barcode_id,
                                //                             'locator_from' => $last_move->locator_to,
                                //                             'status_from'  => $last_move->status_to,
                                //                             'locator_to'   => $locator_id,
                                //                             'status_to'    => $state,
                                //                             'user_id'      => \Auth::user()->id,
                                //                             'description'  => $state.' '.$locator->locator_name,
                                //                             'ip_address'   => $this->getIPClient(),
                                //                             'deleted_at'   => null,
                                //                             'loc_dist'     => $loc_dist
                                //                         ]);
                                //                         DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                //                             'current_locator_id'    => $locator_id,
                                //                             'current_status_to'     => $state,
                                //                             'current_description'   => $state.' '.$locator->locator_name,
                                //                             'updated_movement_at'   => Carbon::now(),
                                //                             'current_user_id'       => \Auth::user()->id
                                //                         ]);
                                //                         $this->sds_connection->table('bundle_info_new')
                                //                         ->where('barcode_id',$barcode_sds->sds_barcode)
                                //                         ->update([
                                //                             'location'      => $loc_dist,
                                //                             'factory'       => 'AOI '.\Auth::user()->factory_id,
                                //                             'setting'       => Carbon::now(),
                                //                             'setting_pic'   => \Auth::user()->nik,
                                //                             'supply_unlock' => false
                                //                         ]);
                                //                     DB::commit();
                                //                     $response = [
                                //                         'barcode_id'    => $barcode_sds->barcode_id,
                                //                         'style'         => $barcode_header->style,
                                //                         'article'       => $barcode_header->article,
                                //                         'po_buyer'      => $barcode_header->poreference,
                                //                         'part'          => $komponen_detail->part,
                                //                         'komponen_name' => $barcode_sds->komponen_name,
                                //                         'size'          => $barcode_header->size,
                                //                         'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                //                         'qty'           => $barcode_sds->qty,
                                //                         'cut'           => $barcode_header->cut_num,
                                //                         'process'       => $locator->locator_name,
                                //                         'status'        => $state,
                                //                         'loc_dist'      => $loc_dist
                                //                     ];
                                //                 }catch(Exception $e){
                                //                     DB::rollback();
                                //                     $message = $e->getMessage();
                                //                     ErrorHandler::db($message);
                                //                 }
                                //                 return response()->json($response,200);
                                //             }
                                //         }else{
                                //             if($linkage_bi == null){
                                //                 return response()->json($barcode_sds->barcode_id, 331);
                                //             }else{
                                //                 $loc_dist = $get_locator_set_cdms == null ? $get_locator_set_sds->location : $get_locator_set_cdms->loc_dist;
                                //                 try{
                                //                     DB::beginTransaction();
                                //                     DB::disableQueryLog();
                                //                         DistribusiMovement::FirstOrCreate([
                                //                             'barcode_id'   => $barcode_sds->barcode_id,
                                //                             'locator_from' => $last_move->locator_to,
                                //                             'status_from'  => $last_move->status_to,
                                //                             'locator_to'   => $locator_id,
                                //                             'status_to'    => $state,
                                //                             'user_id'      => \Auth::user()->id,
                                //                             'description'  => $state.' '.$locator->locator_name,
                                //                             'ip_address'   => $this->getIPClient(),
                                //                             'deleted_at'   => null,
                                //                             'loc_dist'     => $loc_dist
                                //                         ]);
                                //                         DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
                                //                             'current_locator_id'    => $locator_id,
                                //                             'current_status_to'     => $state,
                                //                             'current_description'   => $state.' '.$locator->locator_name,
                                //                             'updated_movement_at'   => Carbon::now(),
                                //                             'current_user_id'       => \Auth::user()->id
                                //                         ]);
                                //                         $this->sds_connection->table('bundle_info_new')
                                //                         ->where('barcode_id',$barcode_sds->sds_barcode)
                                //                         ->update([
                                //                             'location'      => $loc_dist,
                                //                             'factory'       => 'AOI '.\Auth::user()->factory_id,
                                //                             'setting'       => Carbon::now(),
                                //                             'setting_pic'   => $nik_user->nik,
                                //                             'supply_unlock' => false
                                //                         ]);
                                //                     DB::commit();
                                //                     $response = [
                                //                         'barcode_id'    => $barcode_sds->barcode_id,
                                //                         'style'         => $barcode_header->style,
                                //                         'article'       => $barcode_header->article,
                                //                         'po_buyer'      => $barcode_header->poreference,
                                //                         'part'          => $komponen_detail->part,
                                //                         'komponen_name' => $barcode_sds->komponen_name,
                                //                         'size'          => $barcode_header->size,
                                //                         'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
                                //                         'qty'           => $barcode_sds->qty,
                                //                         'cut'           => $barcode_header->cut_num,
                                //                         'process'       => $locator->locator_name,
                                //                         'status'        => $state,
                                //                         'loc_dist'      => $loc_dist
                                //                     ];
                                //                 }catch(Exception $e){
                                //                     DB::rollback();
                                //                     $message = $e->getMessage();
                                //                     ErrorHandler::db($message);
                                //                 }
                                //                 return response()->json($response,200);
                                //             }
                                //         }
                                //     }
                                // }elseif($barcode_sds == null && $barcode_sds_bi != null && $barcode_sds_only != null){
                                //     $last_move_sds          = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                //     $barcode_sds_only       = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                                //     $barcode_header_sds     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
                                //     $komponen_detail_sds    = DB::table('master_style_detail')->where('id', $barcode_sds_only->style_id)->whereNull('deleted_at')->first();

                                //     $nik_user   = DB::table('users')->where('id',\Auth::user()->id)->first();
                                //     $nik_sds    = DB::connection('sds_live')->table('m_user')->where('nik',$nik_user->nik)->first();
                                //     if($nik_sds == null){
                                //         return response()->json('ANDA TIDAK MEMILIKI AKSES DI SDS!',422);
                                //     }
                                //     $last_sett_sds = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
                                //     if($last_sett_sds != null){
                                //         return response()->json('Bundle Sudah Menjalani Setting di'.' - '.$last_sett_sds->loc_dist,422);
                                //     }else{
                                //         $process_sds    = DB::table('v_master_style')->where('id',$barcode_sds_only->style_id)->first();
                                //         //JIKA TERDAPAT PROSES SECONDARY MAKA AKAN DI LAKUKAN PENGECEKAN SUDAH SELESAI ATAU BELUM
                                //         $type_name_sds = DB::table('types')->where('id',$komponen_detail_sds->type_id)->first();
                                //         $linkage_bi_sds = DB::table('distribusi_movements as dm')
                                //         ->select('dm.barcode_id','shm.season','shm.style','shm.poreference','shm.article','vms.type_name','dm.loc_dist','shm.size')
                                //         ->Join('sds_detail_movements as sdm','sdm.sds_barcode','=','dm.barcode_id')
                                //         ->Join('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')
                                //         ->Join('v_master_style as vms','vms.id','=','sdm.style_id')
                                //         ->where('shm.poreference',$barcode_header_sds->poreference)
                                //         ->where('shm.style',$barcode_header_sds->style)
                                //         ->where('shm.season',$barcode_header_sds->season)
                                //         ->where('shm.article',$barcode_header_sds->article)
                                //         ->where('vms.type_name',$type_name_sds->type_name)
                                //         ->where('dm.locator_to','11')
                                //         ->where('dm.status_to','in')
                                //         ->OrderBy('dm.created_at','ASC')
                                //         ->first();
                                //         if($process_sds->process != null){
                                //             $proc_id_sds        = explode(',',$process_sds->process);
                                //             $loc_id_temp_sds    = DB::table('master_process')->select('locator_id')->whereIn('id',$proc_id_sds)->whereNull('deleted_at')->distinct('locator_id')->get();
                                //             foreach($loc_id_temp_sds as $vx){
                                //                 $last_loc_sds_check = DB::table('master_locator')->where('id',$vx->locator_id)->whereNull('deleted_at')->first();
                                //                 $in_status     = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$vx->locator_id)->where('status_to','in')->first();
                                //                 $out_status    = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to',$vx->locator_id)->where('status_to','out')->first();
                                //                 if($in_status == null){
                                //                     return response()->json('Bundle Belum Scan In'.' - '.$last_loc_sds_check->locator_name,422);
                                //                 }
                                //                 if($out_status == null){
                                //                     return response()->json('Bundle Belum Scan Out'.' - '.$last_loc_sds_check->locator_name,422);
                                //                 }
                                //             }

                                //             if($linkage_bi_sds == null){
                                //                 return response()->json($barcode_id, 331);
                                //             }else{
                                //                 try{
                                //                     DB::beginTransaction();
                                //                     DB::disableQueryLog();
                                //                         DistribusiMovement::FirstOrCreate([
                                //                             'barcode_id'   => $barcode_id,
                                //                             'locator_from' => $last_move_sds->locator_to,
                                //                             'status_from'  => $last_move_sds->status_to,
                                //                             'locator_to'   => $locator_id,
                                //                             'status_to'    => $state,
                                //                             'user_id'      => \Auth::user()->id,
                                //                             'description'  => $state.' '.$locator->locator_name,
                                //                             'ip_address'   => $this->getIPClient(),
                                //                             'deleted_at'   => null,
                                //                             'loc_dist'     => $linkage_bi_sds->loc_dist
                                //                         ]);
                                //                         DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                //                             'current_location_id'   => $locator_id,
                                //                             'current_status_to'     => $state,
                                //                             'current_description'   => $state.' '.$locator->locator_name,
                                //                             'updated_movement_at'   => Carbon::now(),
                                //                             'current_user_id'       => \Auth::user()->id
                                //                         ]);
                                //                         $this->sds_connection->table('bundle_info_new')
                                //                         ->where('barcode_id',$barcode_id)
                                //                         ->update([
                                //                             'location'      => $linkage_bi_sds->loc_dist,
                                //                             'factory'       => 'AOI '.\Auth::user()->factory_id,
                                //                             'setting'       => Carbon::now(),
                                //                             'setting_pic'   => \Auth::user()->nik,
                                //                             'supply_unlock' => false
                                //                         ]);
                                //                     DB::commit();
                                //                     $response = [
                                //                         'barcode_id'    => $barcode_id,
                                //                         'style'         => $barcode_header_sds->style,
                                //                         'article'       => $barcode_header_sds->article,
                                //                         'po_buyer'      => $barcode_header_sds->poreference,
                                //                         'part'          => $komponen_detail_sds->part,
                                //                         'komponen_name' => $barcode_sds_only->komponen_name,
                                //                         'size'          => $barcode_header_sds->size,
                                //                         'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                //                         'qty'           => $barcode_sds_only->qty,
                                //                         'cut'           => $barcode_header_sds->cut_num,
                                //                         'process'       => $locator->locator_name,
                                //                         'status'        => $state,
                                //                         'loc_dist'      => $linkage_bi_sds->loc_dist
                                //                     ];
                                //                 }catch(Exception $e){
                                //                     DB::rollback();
                                //                     $message = $e->getMessage();
                                //                     ErrorHandler::db($message);
                                //                 }
                                //                 return response()->json($response,200);
                                //             }
                                //         }else{
                                //             if($linkage_bi_sds == null){
                                //                 return response()->json($barcode_id, 331);
                                //             }else{
                                //                 try{
                                //                     DB::beginTransaction();
                                //                     DB::disableQueryLog();
                                //                         DistribusiMovement::FirstOrCreate([
                                //                             'barcode_id'   => $barcode_id,
                                //                             'locator_from' => $last_move_sds->locator_to,
                                //                             'status_from'  => $last_move_sds->status_to,
                                //                             'locator_to'   => $locator_id,
                                //                             'status_to'    => $state,
                                //                             'user_id'      => \Auth::user()->id,
                                //                             'description'  => $state.' '.$locator->locator_name,
                                //                             'ip_address'   => $this->getIPClient(),
                                //                             'deleted_at'   => null,
                                //                             'loc_dist'     => $linkage_bi_sds->loc_dist
                                //                         ]);
                                //                         DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                //                             'current_location_id'   => $locator_id,
                                //                             'current_status_to'     => $state,
                                //                             'current_description'   => $state.' '.$locator->locator_name,
                                //                             'updated_movement_at'   => Carbon::now(),
                                //                             'current_user_id'       => \Auth::user()->id
                                //                         ]);
                                //                         $this->sds_connection->table('bundle_info_new')
                                //                         ->where('barcode_id',$barcode_id)
                                //                         ->update([
                                //                             'location'      => $linkage_bi_sds->loc_dist,
                                //                             'factory'       => 'AOI '.\Auth::user()->factory_id,
                                //                             'setting'       => Carbon::now(),
                                //                             'setting_pic'   => $nik_user->nik,
                                //                             'supply_unlock' => false
                                //                         ]);
                                //                     DB::commit();
                                //                     $response = [
                                //                         'barcode_id'    => $barcode_id,
                                //                         'style'         => $barcode_header_sds->style,
                                //                         'article'       => $barcode_header_sds->article,
                                //                         'po_buyer'      => $barcode_header_sds->poreference,
                                //                         'part'          => $komponen_detail_sds->part,
                                //                         'komponen_name' => $barcode_sds_only->komponen_name,
                                //                         'size'          => $barcode_header_sds->size,
                                //                         'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
                                //                         'qty'           => $barcode_sds_only->qty,
                                //                         'cut'           => $barcode_header_sds->cut_num,
                                //                         'process'       => $locator->locator_name,
                                //                         'status'        => $state,
                                //                         'loc_dist'      => $linkage_bi_sds->loc_dist
                                //                     ];
                                //                 }catch(Exception $e){
                                //                     DB::rollback();
                                //                     $message = $e->getMessage();
                                //                     ErrorHandler::db($message);
                                //                 }
                                //                 return response()->json($response,200);
                                //             }
                                //         }
                                //     }
                                // }elseif($barcode_sds_only != null && $barcode_sds != null){
                                //     return response()->json('Terdapat Duplikat Data Pada Detail Barcode!',422);
                                // }else{
                                //     return response()->json('Barcode Tidak Dikenali!',422);
                                // }
                            }else{
                                $user          = \Auth::user()->nik;
                                $factory       = \Auth::user()->factory_id;
                                $dstat         = 1;
                                $alert         = NULL;
                                $line_location = null;

                                $chk = $this->sds_connection->select($this->sds_connection->raw("
                                    SELECT
                                    barcode_id,TRIM(style) AS style,TRIM(set_type) AS set_type,season,TRIM(poreference) AS poreference,article,TRIM(size) AS size,cut_num,start_num,end_num,qty,
                                    proc_scan IS NULL AND supply_scan IS NULL AS not_scanned,
                                    cut_out,is_wip,supply_unlock,rejects,processing_fact,
                                    CASE WHEN is_set THEN CONCAT(TRIM(location_name),' (',TRIM(factory),')') ELSE NULL END AS is_set,
                                    CASE WHEN is_supplied THEN supplied_to ELSE NULL END AS is_supplied,
                                    CASE
                                        WHEN NOT artwork THEN 'Artwork'
                                        WHEN NOT he THEN 'Heat Transfer'
                                        WHEN NOT pad THEN 'Pad Print'
                                        WHEN NOT ppa THEN 'PPA Jahit'
                                        WHEN NOT fuse THEN 'Fuse'
                                        WHEN NOT auto THEN 'Auto'
                                        WHEN NOT bonding THEN 'Bonding'
                                        ELSE NULL
                                    END AS incomplete_proc
                                    FROM bundle_for_set_new
                                    WHERE barcode_id = '$barcode_id'::NUMERIC
                                "));

                                if(count($chk) > 0) {

                                    $chk = $chk[0];

                                    if($chk->not_scanned==false){
                                        $alert="Bundle sudah dipindai di SDS!";
                                        $dstat=0;
                                    }else if($chk->processing_fact != $factory){
                                        $alert="Bundle merupakan alokasi pabrik lain!";
                                        $dstat=0;
                                    }else if($chk->is_wip != ''){
                                        $alert="Bundle belum Check-Out proses ".($chk->is_wip)."!";
                                        $dstat=0;
                                    }else if($chk->supply_unlock == true){

                                    }else if($chk->is_supplied != ''){
                                        $alert="Bundle sudah di supply ke ".($chk->is_supplied)."!";
                                        $dstat=0;
                                    }else if($chk->is_set != ''){
                                        $alert="Bundle sudah menjalani Setting di ".($chk->is_set)."!";
                                        $dstat=0;
                                    }else if($chk->rejects == true){
                                        $alert="Bundle dalam proses ganti reject!";
                                        $dstat=0;
                                    }else if($chk->cut_out == false){
                                        $alert="Bundle belum checkout dari Cutting/Bundling!";
                                        $dstat=0;
                                    }else if($chk->processing_fact==2 && $chk->incomplete_proc != ''){
                                        $alert="Bundle belum menjalani proses ".($chk->incomplete_proc)."!";
                                        $dstat=0;
                                    }

                                    if($dstat > 0){

                                        $set_notes = $this->sds_connection->select($this->sds_connection->raw("
                                            SELECT
                                            CASE WHEN not_scanned THEN TRIM(pairs_loc) ELSE scan_loc END AS sugg_loc,CASE WHEN not_scanned THEN pairs_fact ELSE scan_fact END AS sugg_fact,is_moving
                                            FROM set_pairs_alloc_new
                                            WHERE
                                            season='$chk->season'::VARCHAR AND set_type='$chk->set_type'::VARCHAR AND poreference='$chk->poreference'::VARCHAR AND article='$chk->article'::VARCHAR
                                            AND size='$chk->size'::VARCHAR AND cut_num='$chk->cut_num'::INTEGER AND start_num='$chk->start_num'::INTEGER AND end_num='$chk->end_num'::INTEGER
                                        "));

                                        $set_notes=(count($set_notes) > 0 ? $set_notes[0] : NULL);

                                        if($set_notes != NULL && $set_notes->is_moving==true){
                                            $alert="Pasangan-pasangan bundle ini sedang dalam proses movement di area Setting!";
                                            $dstat=0;
                                        }else if($set_notes != NULL && $set_notes->sugg_fact != ''){
                                            $ppcm_id=NULL;
                                            $s_factory=$factory;
                                            if($set_notes->sugg_fact == $factory){
                                                $set_notes=$set_notes->sugg_loc;
                                            }else{
                                                $alert="Garment ini dikerjakan di ".($this->sds_connection->table('factory')->where('factory_id',$set_notes->sugg_fact)->first()->factory_name)."!";
                                                $dstat=0;
                                            }
                                        }else if($factory==1){
                                            $ppcm_id=NULL;
                                            $set_notes=NULL;
                                            $s_factory=NULL;
                                        }else{
                                            $choose_option = 2;
                                            if($choose_option == 1) {
                                                $set_notes=NULL;
                                                $que = "SELECT set_helper_new AS set_helper FROM set_helper_new(?,?,?,?,?::INTEGER)";
                                                $ppcm_id=$this->db->query($que,array(
                                                    $chk->season,
                                                    $chk->article,
                                                    $chk->set_type,
                                                    $chk->poreference,
                                                    (int)$factory
                                                ))->result()[0]->set_helper;
                                                $ppcm_id = ($ppcm_id==NULL || $ppcm_id=='' ? NULL : $ppcm_id);
                                                $s_factory=NULL;
                                            } else if($choose_option == 2) {

                                                $check_temp_setting_set = $this->sds_connection
                                                    ->table('jaz_temp_scan_setting')
                                                    ->where('style', $chk->style)
                                                    ->where('set_type', $chk->set_type)
                                                    ->where('season', $chk->season)
                                                    ->where('article', $chk->article)
                                                    ->where('poreference', $chk->poreference)
                                                    ->where('size', $chk->size)
                                                    ->where('cut_num', $chk->cut_num)
                                                    ->where('start_num', $chk->start_num)
                                                    ->where('end_num', $chk->end_num)
                                                    ->where('qty', $chk->qty)
                                                    ->where('processing_fact', $factory)
                                                    ->get();

                                                if(count($check_temp_setting_set) > 0) {
                                                    $check_temp_setting_set = $check_temp_setting_set->first();
                                                    $ppcm_id = $check_temp_setting_set->location;
                                                } else {
                                                    $style_array = array();
                                                    if($chk->set_type == 'TOP') {
                                                        $style_array[] = $chk->style.'-TOP';
                                                    } else if($chk->set_type == 'BOTTOM') {
                                                        $style_array[] = $chk->style.'-BOT';
                                                    } else if($chk->set_type == 'INNER') {
                                                        $style_array[] = $chk->style.'-INN';
                                                        $style_array[] = $chk->style.'-TIN';
                                                    } else if($chk->set_type == 'OUTER') {
                                                        $style_array[] = $chk->style.'-OUT';
                                                        $style_array[] = $chk->style.'-TOU';
                                                    } else {
                                                        $style_array[] = $chk->style;
                                                    }

                                                    $get_ppcm_ids = $this->sds_connection
                                                        ->table('jaz_ppcm_upload_new')
                                                        ->whereIn('style', $style_array)
                                                        ->whereNull('deleted_at')
                                                        ->where('poreference', $chk->poreference)
                                                        ->where('season', $chk->season)
                                                        ->where('article', $chk->article)
                                                        ->get();

                                                    if(count($get_ppcm_ids) > 0) {
                                                        $selisih = 10000000;
                                                        $ppcm_id = null;
                                                        foreach($get_ppcm_ids as $data_ppcm) {

                                                            $qty_temp = $this->sds_connection->select($this->sds_connection->raw("select sum(qty) as qty from jaz_temp_scan_setting where location = '$data_ppcm->id'::numeric"))[0]->qty;

                                                            $qty_mod = $qty_temp + $data_ppcm->qty_mod;

                                                            if($data_ppcm->qty_split > $qty_mod) {

                                                                $get_selisih = abs(($data_ppcm->qty_split - $qty_mod) - $chk->qty);
                                                                if($get_selisih < $selisih) {
                                                                    $selisih = $get_selisih;
                                                                    $ppcm_id = $data_ppcm->id;
                                                                }

                                                            }
                                                        }
                                                    } else {
                                                        $ppcm_id = null;
                                                    }
                                                }
                                                $s_factory=NULL;
                                                $set_notes=NULL;
                                            }
                                        }

                                        if($dstat > 0){
                                            $check_temp_setting = $this->sds_connection->table('temp_scan_proc')->insert([
                                                'user_id'   => $user,
                                                'proc'      => 'SET',
                                                'ap_id'     => $barcode_id,
                                                'set_notes' => $set_notes,
                                                'factory'   => $s_factory,
                                                'ppcm_id'   => $ppcm_id
                                            ]);

                                            $j=0;

                                            // NEW ENTRIES
                                            $bc_list = $this->sds_connection->select($this->sds_connection->raw("
                                                SELECT ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,
                                                TRIM(lc.location) AS location,TRIM(lc.factory) AS factory,
                                                MAX(bi.qty) AS qty,COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id
                                                FROM (
                                                SELECT ap_id,ppcm_id
                                                FROM temp_scan_proc
                                                WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NOT NULL AND NOT check_out
                                                ) ts
                                                JOIN bundle_info_new bi ON ts.ap_id=bi.barcode_id
                                                JOIN ppcm_upload_new pu ON ts.ppcm_id=pu.id
                                                JOIN location lc ON pu.line ~ CONCAT(E'^',REPLACE(TRIM(lc.factory),' ',E'#'),' ',REPLACE(TRIM(lc.location_name),' ','0?'),E'$')
                                                GROUP BY ts.ppcm_id,bi.size,bi.cut_num,bi.start_num,bi.end_num,lc.location,lc.factory
                                            "));

                                            foreach($bc_list as $bc){
                                                $this->sds_connection->select($this->sds_connection->raw("UPDATE ppcm_upload_new SET qty_mod=qty_mod+'$bc->qty' WHERE id='$bc->ppcm_id'"));

                                                $cond = explode(',',$bc->bc_id);
                                                $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                    'location'=>$bc->location,
                                                    'factory'=>$bc->factory,
                                                    'setting'=>Carbon::now(),
                                                    'setting_pic'=>$user,
                                                    'supply_unlock'=>false
                                                ]);
                                                $line_location = $bc->location;
                                                $j+=$bc->count;
                                            }

                                            $bc_list = $this->sds_connection->select($this->sds_connection->raw("SELECT COUNT(ts.ap_id),STRING_AGG(ts.ap_id::TEXT,',') AS bc_id,ts.set_notes,f.factory_name FROM (SELECT ap_id,set_notes,factory FROM temp_scan_proc WHERE user_id='$user' AND proc='SET' AND ppcm_id IS NULL AND NOT check_out) ts JOIN factory f ON ts.factory=f.factory_id GROUP BY ts.set_notes,f.factory_name"));

                                            foreach($bc_list as $bc){

                                                $cond = explode(',',$bc->bc_id);
                                                $this->sds_connection->table('bundle_info_new')->whereIn('barcode_id',$cond)->update([
                                                    'location'=>$bc->set_notes,
                                                    'factory'=>$bc->factory_name,
                                                    'setting'=> Carbon::now(),
                                                    'setting_pic'=>$user,
                                                    'supply_unlock'=>false
                                                ]);
                                                $line_location = $bc->set_notes;
                                                $j+=$bc->count;
                                            }

                                            $this->sds_connection->table('system_log')->insert([
                                                'user_nik' => $user,
                                                'user_proc' => 'SET',
                                                'log_operation' => 'set_checkin',
                                                'log_text' => 'add item '.$barcode_id.' to SET check in',
                                            ]);
                                        }
                                    }

                                } else {
                                    $alert="Bundle tidak terdaftar!";
                                    $dstat=0;
                                }


                                if($dstat == 1) {
                                    //gak ada di ppcm
                                    if($line_location == null) {
                                        return response()->json($barcode_id, 331);
                                    } else {
                                        $response = [
                                            'barcode_id'    => $barcode_id,
                                            'style'         => $update_sds->style,
                                            'article'       => $update_sds->article,
                                            'po_buyer'      => $update_sds->poreference,
                                            'part'          => $update_sds->part_num,
                                            'komponen_name' => $update_sds->part_name,
                                            'size'          => $update_sds->size,
                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                            'qty'           => $update_sds->qty,
                                            'cut'           => $update_sds->cut_num,
                                            'process'       => $locator->locator_name,
                                            'status'        => $state,
                                            'loc_dist'      => $line_location,
                                        ];

                                        $this->sds_connection->table('temp_scan_proc')
                                            ->where('user_id' ,$user)
                                            ->where('proc','SET')
                                            ->where('check_out',false)
                                            ->delete();

                                        return response()->json($response,200);
                                    }
                                } else {
                                    return response()->json($alert,422);
                                }
                            }
                        }else{
                            $cek_art = $this->sds_connection->table('art_desc_new')
                            ->where([
                                ['style',$update_sds->style],
                                ['season',$update_sds->season],
                                ['part_name',$update_sds->part_name],
                                ['part_num',$update_sds->part_num]
                            ])
                            ->first();

                            $convert_locator = $this->convertLocator($locator_id, $state);
                            //LOCATOR CUTTING
                            if($convert_locator['art_desc']=='bd'){
                                $barcode_check_sds = DB::table('bundle_detail as bd')
                                ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                ->leftJoin('master_style_detail as msd','msd.id','=','bd.style_detail_id')
                                ->where('bd.sds_barcode', $barcode_id)
                                ->whereNull('msd.deleted_at')
                                ->first();
                                $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                ->where('barcode_id', $barcode_id)
                                ->first();
                                $bundle_check = $this->sds_connection->table('dd_info_bundle_supply')->where('barcode_id',$barcode_id)->first();
                                if($update_sds->{$convert_locator['column_update']} == null) {
                                    $style_cdms_check = DB::table('v_master_style')->where('style',$bundle_check->style)->where('season_name',$bundle_check->season)->where('part',$bundle_check->part_num)->where('komponen_name',$bundle_check->part_name)->first();
                                    if($style_cdms_check == null){
                                        return response()->json('Inputkan Master Style di CDMS dahulu!',422);
                                    }
                                    if($barcode_check_sds != null && $barcode_check_sds->sds_barcode == $barcode_id){
                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                            'location' => $convert_locator['locator_sds'],
                                            $convert_locator['column_update'] => Carbon::now(),
                                            $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                            'factory' => $convert_locator['factory_sds'],
                                        ]);
                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                            'is_out_cutting'            => 'Y',
                                            'out_cutting_at'            => Carbon::now(),
                                            'current_locator_id'        => $locator_id,
                                            'current_status_to'         => $state,
                                            'current_description'       => $state.' '.$locator->locator_name,
                                            'updated_movement_at'       => Carbon::now(),
                                            'current_user_id'           => \Auth::user()->id
                                        ]);
                                        $check_out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                        if($check_out_cutting != null){
                                            return response()->json('Barcode sudah di proses scan CUTTING!',422);
                                        }else{
                                            DB::table('distribusi_movements')->insert([
                                                'id'                    => Uuid::generate()->string,
                                                'barcode_id'            => $barcode_check_sds->barcode_id,
                                                'locator_to'            => $locator_id,
                                                'status_to'             => $state,
                                                'description'           => $state.' '.$locator->locator_name,
                                                'user_id'               => \Auth::user()->id,
                                                'ip_address'            => $this->getIPClient(),
                                                'created_at'            => Carbon::now(),
                                                'updated_at'            => Carbon::now(),
                                                'bundle_table'          => $bundle_table,
                                                'factory_id'            => \Auth::user()->factory_id
                                            ]);
                                        }
                                        
                                    }elseif($barcode_check_sds == null && $update_sds->barcode_id == $barcode_id){
                                        try{
                                            DB::beginTransaction();
                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                'location' => $convert_locator['locator_sds'],
                                                $convert_locator['column_update'] => Carbon::now(),
                                                $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                'factory' => $convert_locator['factory_sds'],
                                            ]);
                                            if($sds_header->set_type == 'TOP'){
                                                $set_type = 'TOP';
                                            }elseif($sds_header->set_type == 'BOTTOM'){
                                                $set_type = 'BOTTOM';
                                            }else{
                                                $set_type = 'Non';
                                            }
                                            $header_exists = DB::table('sds_header_movements')
                                            ->where('poreference',$sds_header->poreference)
                                            ->where('style',$sds_header->style)
                                            ->where('set_type',$set_type)
                                            ->where('season',$sds_header->season)
                                            ->where('cut_num',$sds_header->cut_num)
                                            ->where('size',$sds_header->size)
                                            ->where('factory_id',$sds_header->processing_fact)
                                            ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                            ->exists();

                                            $get_style_process = DB::table('master_style_detail as msd')
                                            ->select('msd.id as style_id','msd.process as process_id')
                                            ->leftJoin('types as t','t.id','=','msd.type_id')
                                            ->leftJoin('master_komponen as mk','mk.id','=','msd.komponen')
                                            ->leftJoin('master_seasons as ms','ms.id','=','msd.season')
                                            ->where('msd.style',$sds_header->style)
                                            ->where('t.type_name',$set_type)
                                            ->where('ms.season_name',$sds_header->season)
                                            ->where('msd.part',$sds_header->part_num)
                                            ->where('mk.komponen_name',$sds_header->part_name)
                                            ->whereNull('msd.deleted_at')
                                            ->first();
                                            
                                            if(!$header_exists){
                                                DB::table('sds_header_movements')->insert([
                                                    'id'            => Uuid::generate()->string,
                                                    'season'        => $sds_header->season,
                                                    'style'         => trim($sds_header->style),
                                                    'set_type'      => trim($set_type),
                                                    'poreference'   => $sds_header->poreference,
                                                    'article'       => $sds_header->article,
                                                    'cut_num'       => $sds_header->cut_num,
                                                    'size'          => $sds_header->size,
                                                    'factory_id'    => (int)$sds_header->processing_fact,
                                                    'cutting_date'  => null,
                                                    'color_name'    => trim($sds_header->coll),
                                                    'created_by'    => \Auth::user()->id,
                                                    'created_at'    => Carbon::now(),
                                                    'updated_at'    => Carbon::now(),
                                                    'updated_by'    => null,
                                                    'sticker'       => $sds_header->start_num.'-'.$sds_header->end_num
                                                ]);
                                                $get_header_id = DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                ->first()->id;
                                                $is_recycle = DB::table('data_cuttings')
                                                ->where('po_buyer',$sds_header->poreference)
                                                ->where('part_no',$sds_header->part_num)
                                                ->where('size_finish_good',$sds_header->size)->first();
                                                $recycle = $is_recycle == null ? null : $is_recycle->is_recycle;
                                                $detail_exists = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->exists();
                                                if(!$detail_exists){
                                                    DB::table('sds_detail_movements')->insert([
                                                        'sds_header_id'         => $get_header_id,
                                                        'sds_barcode'           => $barcode_id,
                                                        'komponen_name'         => trim($sds_header->part_name),
                                                        'part_num'              => $sds_header->part_num,
                                                        'start_no'              => $sds_header->start_num,
                                                        'end_no'                => $sds_header->end_num,
                                                        'qty'                   => $sds_header->qty,
                                                        'factory_id'            => $sds_header->processing_fact,
                                                        'job'                   => $sds_header->style."::".$sds_header->poreference."::".$sds_header->article,
                                                        'is_recycle'            => $recycle,
                                                        'is_out_cutting'        => 'Y',
                                                        'is_out_cutting_at'     => Carbon::now(),
                                                        'out_setting_at'        => null,
                                                        'is_out_setting'        => null,
                                                        'current_location_id'   => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id,
                                                        'secondary_process'     => trim($update_sds->note),
                                                        'style_id'              => $get_style_process->style_id,
                                                        'process_id'            => $get_style_process->process_id
                                                    ]);
                                                }else{
                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                        'current_location_id'   => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'current_user_id'       => \Auth::user()->id,
                                                        'updated_movement_at'   => Carbon::now()
                                                    ]);
                                                }
                                                $check_out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                                if($check_out_cutting != null){
                                                    return response()->json('Barcode sudah di proses scan CUTTING!',422);
                                                }else{
                                                    DB::table('distribusi_movements')->insert([
                                                        'id'                    => Uuid::generate()->string,
                                                        'barcode_id'            => $barcode_id,
                                                        'locator_to'            => $locator_id,
                                                        'status_to'             => $state,
                                                        'description'           => $state.' '.$locator->locator_name,
                                                        'user_id'               => \Auth::user()->id,
                                                        'ip_address'            => $this->getIPClient(),
                                                        'created_at'            => Carbon::now(),
                                                        'updated_at'            => Carbon::now(),
                                                        'bundle_table'          => $bundle_table,
                                                        'factory_id'            => \Auth::user()->factory_id
                                                    ]);
                                                }
                                            }else{
                                                DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)->update([
                                                    'updated_at'        => Carbon::now(),
                                                    'updated_by'        => \Auth::user()->id
                                                ]);
                                                $get_header_id = DB::table('sds_header_movements')
                                                ->where('poreference',$sds_header->poreference)
                                                ->where('style',$sds_header->style)
                                                ->where('set_type',$set_type)
                                                ->where('season',$sds_header->season)
                                                ->where('cut_num',$sds_header->cut_num)
                                                ->where('size',$sds_header->size)
                                                ->where('factory_id',$sds_header->processing_fact)
                                                ->where('sticker',$sds_header->start_num.'-'.$sds_header->end_num)
                                                ->first()->id;
                                                $is_recycle = DB::table('data_cuttings')
                                                ->where('po_buyer',$sds_header->poreference)
                                                ->where('part_no',$sds_header->part_num)
                                                ->where('size_finish_good',$sds_header->size)->first();
                                                $recycle = $is_recycle == null ? null : $is_recycle->is_recycle;
                                                $detail_exists = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->exists();
                                                if(!$detail_exists){
                                                    DB::table('sds_detail_movements')->insert([
                                                        'sds_header_id'         => $get_header_id,
                                                        'sds_barcode'           => $barcode_id,
                                                        'komponen_name'         => trim($sds_header->part_name),
                                                        'part_num'              => $sds_header->part_num,
                                                        'start_no'              => $sds_header->start_num,
                                                        'end_no'                => $sds_header->end_num,
                                                        'qty'                   => $sds_header->qty,
                                                        'factory_id'            => $sds_header->processing_fact,
                                                        'job'                   => $sds_header->style."::".$sds_header->poreference."::".$sds_header->article,
                                                        'is_recycle'            => $recycle,
                                                        'is_out_cutting'        => 'Y',
                                                        'is_out_cutting_at'     => Carbon::now(),
                                                        'out_setting_at'        => null,
                                                        'is_out_setting'        => null,
                                                        'current_location_id'   => $locator_id,
                                                        'current_status_to'     => $state,
                                                        'current_description'   => $state.' '.$locator->locator_name,
                                                        'updated_movement_at'   => Carbon::now(),
                                                        'current_user_id'       => \Auth::user()->id,
                                                        'secondary_process'     => trim($update_sds->note),
                                                        'style_id'              => $get_style_process->style_id,
                                                        'process_id'            => $get_style_process->process_id
                                                    ]);
                                                }else{
                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                        'current_location_id'       => $locator_id,
                                                        'current_status_to'         => $state,
                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                        'current_user_id'           => \Auth::user()->id,
                                                        'updated_movement_at'       => Carbon::now()
                                                    ]);
                                                }
                                                $check_out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                                if($check_out_cutting != null){
                                                    return response()->json('Barcode sudah di proses scan CUTTING!',422);
                                                }else{
                                                    DB::table('distribusi_movements')->insert([
                                                        'id'                    => Uuid::generate()->string,
                                                        'barcode_id'            => $barcode_id,
                                                        'locator_to'            => $locator_id,
                                                        'status_to'             => $state,
                                                        'description'           => $state.' '.$locator->locator_name,
                                                        'user_id'               => \Auth::user()->id,
                                                        'ip_address'            => $this->getIPClient(),
                                                        'created_at'            => Carbon::now(),
                                                        'updated_at'            => Carbon::now(),
                                                        'bundle_table'          => $bundle_table,
                                                        'factory_id'            => \Auth::user()->factory_id
                                                    ]);
                                                }
                                            }
                                            DB::commit();
                                        }catch(Exception $e){
                                            DB::rollback();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }
                                    }else{
                                        return response()->json('Barcode Tidak Dikenali!',422);
                                    }
                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $update_sds->style,
                                        'article'       => $update_sds->article,
                                        'po_buyer'      => $update_sds->poreference,
                                        'part'          => $update_sds->part_num,
                                        'komponen_name' => $update_sds->part_name,
                                        'size'          => $update_sds->size,
                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                        'qty'           => $update_sds->qty,
                                        'cut'           => $update_sds->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $bundle_table
                                    ];

                                    return response()->json($response,200);
                                }
                                else{
                                    return response()->json('Barcode sudah di proses scan CUTTING!',422);
                                }
                            }
                            else{
                                if($update_sds->distrib_received == null && $update_sds->bd_pic == null){
                                    return response()->json('Barcode belum scan out cutting!',422);
                                }else{
                                    $in_out_loc     = ['1','2','3','4','5','6','7','8','9'];
                                    $art_process    = DB::table('master_process')->select('id')->where('locator_id','3')->pluck('id')->toArray();
                                    //LOCATOR PPA SDS
                                    if($convert_locator['art_desc']=='ppa'){
                                        if($cek_art->ppa == true || $cek_art->ppa_2 == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where('process_name','like','%PPA%')->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_locator_id'        => $locator_id,
                                                                            'current_status_to'         => $state,
                                                                            'current_description'       => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'       => Carbon::now(),
                                                                            'current_user_id'           => \Auth::user()->id
                                                                        ]);
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch (Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'           => $locator_id,
                                                                        'current_status_to'             => $state,
                                                                        'current_user_id'               => \Auth::user()->id,
                                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'           => Carbon::now()
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                    
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail != null){
                                                            return response()->json('Duplicate Barcode!',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }

                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        return response()->json('Barcode sudah scan IN '.$locator->locator_name.'!',422);
                                                    }
                                                }else{
                                                    return response()->json('Barcode belum scan IN '.$locator->locator_name.'!',422);
                                                }
                                            }else{
                                                if($update_sds->{$convert_locator['column_update']} == null){
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned   = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$barcode_check_sds->process);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                }
                                                            }else{
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch (Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned   = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$sds_detail->process_id);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                }
                                                            }else{
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'           => $locator_id,
                                                                        'current_status_to'             => $state,
                                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                                        'current_user_id'               => \Auth::user()->id,
                                                                        'updated_movement_at'           => Carbon::now()
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds != null && $sds_detail != null){
                                                        return response()->json('DUPLICATE BARCODE!',422);
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }else{
                                                    return response()->json('Barcode sudah di proses scan PPA!',422);
                                                }
                                            }
                                        }else{
                                            return response()->json('Barcode tidak lewat proses '.$locator->locator_name.'!',422);
                                        }
                                    }
                                    //LOCATOR HE SDS
                                    elseif($convert_locator['art_desc']=='he'){
                                        if($cek_art->he == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where(function($q){
                                                $q->where('process_name','like','%HE%')
                                                ->orWhere('process_name','like','%DHTL%');
                                            })->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null){
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_locator_id'        => $locator_id,
                                                                            'current_status_to'         => $state,
                                                                            'current_description'       => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'       => Carbon::now(),
                                                                            'current_user_id'           => \Auth::user()->id
                                                                        ]);
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch (Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'           => $locator_id,
                                                                        'current_status_to'             => $state,
                                                                        'current_user_id'               => \Auth::user()->id,
                                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'           => Carbon::now()
                                                                    ]);
                                                                        
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail != null){
                                                            return response()->json('DUPLICATE BARCODE',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        return response()->json('Barcode sudah di proses scan '.$locator->locator_name,422);
                                                    }
                                                }else{
                                                    return response()->json('Barcode belum proses scan IN '.$locator->locator_name,422);
                                                }
                                            }else{
                                                if($update_sds->{$convert_locator['column_update']} == null) {
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$barcode_check_sds->process);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }else{
                                                                        if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                            if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                                $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                                return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'bundle_table'          => null,
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                    $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                    return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                              => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']       => Carbon::now(),
                                                                    $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                    'factory'                               => $convert_locator['factory_sds'],
                                                                ]);
                                                                DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_location_id'       => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_user_id'           => \Auth::user()->id,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now()
                                                                ]);
                                                                DistribusiMovement::firstOrCreate([
                                                                    'barcode_id'            => $barcode_id,
                                                                    'locator_from'          => $last_scanned->locator_to,
                                                                    'locator_to'            => $locator_id,
                                                                    'status_from'           => $last_scanned->status_to,
                                                                    'status_to'             => $state,
                                                                    'description'           => $state.' '.$locator->locator_name,
                                                                    'user_id'               => \Auth::user()->id,
                                                                    'ip_address'            => $this->getIPClient(),
                                                                    'created_at'            => Carbon::now(),
                                                                    'updated_at'            => Carbon::now(),
                                                                    'factory_id'            => \Auth::user()->factory_id
                                                                ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds != null && $sds_detail != null){
                                                        return response()->json('DUPLICATE BARCODE',422);
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    return response()->json('Barcode sudah di proses scan HE!',422);
                                                }
                                            }
                                        }
                                        else{
                                            return response()->json('Barcode tidak lewat proses HE!',422);
                                        }
                                    }
                                    //LOCATOR PAD PRINT SDS
                                    elseif($convert_locator['art_desc']=='pad'){
                                        if($cek_art->pad == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where('process_name','like','%PAD%')->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_locator_id'        => $locator_id,
                                                                            'current_status_to'         => $state,
                                                                            'current_description'       => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'       => Carbon::now(),
                                                                            'current_user_id'           => \Auth::user()->id
                                                                        ]);
                                                                        
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch (Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_location_id'           => $locator_id,
                                                                            'current_status_to'             => $state,
                                                                            'current_user_id'               => \Auth::user()->id,
                                                                            'current_description'           => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'           => Carbon::now()
                                                                        ]);
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail != null){
                                                            return response()->json('DUPLICATE BARCODE',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }else{
                                                        return response()->json('Barcode sudah proses scan '.$locator->locator_name,422);
                                                    }
                                                }else{
                                                    return response()->json('Barcode belum proses scan '.$locator->locator_name,422);
                                                }
                                            }else{
                                                if($update_sds->{$convert_locator['column_update']} == null) {
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$barcode_check_sds->process);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location' => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update'] => Carbon::now(),
                                                                        $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                        'factory' => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$sds_detail->process_id);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                }
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'       => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_user_id'           => \Auth::user()->id,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now()
                                                                    ]);
                                                                    
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'bundle_table'          => null,
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds != null && $sds_detail != null){
                                                        return response()->json('DUPLICATE BARCODE',422);
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    return response()->json('Barcode sudah di proses scan PAD!',422);
                                                }
                                            }
                                        }else{
                                            return response()->json('Barcode tidak lewat proses PAD PRINT!',422);
                                        }
                                    }
                                    //LOCATOR BOBOK / AUTO
                                    elseif($convert_locator['art_desc']=='bobok'){
                                        if($cek_art->bobok == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where(function($q){
                                                $q->where('process_name','like','%BOBOK%')
                                                ->orWhere('process_name','like','%AUTO%')
                                                ->orWhere('process_name','like','%TEMPLATE%')
                                                ->orWhere('process_name','like','%AMS%');
                                            })
                                            ->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_locator_id'        => $locator_id,
                                                                            'current_status_to'         => $state,
                                                                            'current_description'       => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'       => Carbon::now(),
                                                                            'current_user_id'           => \Auth::user()->id
                                                                        ]);
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'           => $locator_id,
                                                                        'current_status_to'             => $state,
                                                                        'current_user_id'               => \Auth::user()->id,
                                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'           => Carbon::now()
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail != null){
                                                            return response()->json('DUPLICATE BARCODE!',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }

                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah di proses scan BOBOK!',422);
                                                    }
                                                }
                                                else{
                                                    return response()->json('Barcode belum proses scan IN BOBOK!',422);
                                                }
                                            }
                                            else{
                                                if($update_sds->{$convert_locator['column_update']} == null) {
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$barcode_check_sds->process);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                        
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            //JIKA TERDAPAT PROSES ARTWORK MAKA HARUS MELEWATI ARTWORK DAHULU
                                                            $process = explode(',',$sds_detail->process_id);
                                                            if(count($process) > 0){
                                                                foreach($process as $p){
                                                                    if(in_array($p,$art_process)){
                                                                        $done_art = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','3')->orderBy('created_at','DESC')->get();
                                                                        if(count($done_art) > 0){
                                                                            $status_movement = [];
                                                                            foreach($done_art as $da){
                                                                                $status_movement[] = $da->status_to;
                                                                            }
                                                                            if(!in_array('out',$status_movement)){
                                                                                return response()->json('HARAP SCAN OUT ARTWORK DAHULU!',422);
                                                                            }
                                                                        }else{
                                                                            return response()->json('HARAP SCAN IN ARTWORK DAHULU!',422);
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                            }
                                                            
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'       => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_user_id'           => \Auth::user()->id,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now()
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds != null && $sds_detail != null){
                                                        return response()->json('DUPLICATE BARCODE',422);
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }

                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];
                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    return response()->json('Barcode sudah scan '.$locator->locator_name.'!',422);
                                                }
                                            }
                                        }
                                        else{
                                            return response()->json('Barcode Tidak Melewati proses '.$locator->locator_name.'!',422);
                                        }
                                    }
                                    //LOCATOR FUSE
                                    elseif($convert_locator['art_desc']=='fuse'){
                                        if($cek_art->fuse == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where('process_name','like','%FUSE%')->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                try{
                                                                    DB::beginTransaction();
                                                                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                            'location'                              => $convert_locator['locator_sds'],
                                                                            $convert_locator['column_update']       => Carbon::now(),
                                                                            $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                            'factory'                               => $convert_locator['factory_sds'],
                                                                        ]);
                                                                        DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_location_id'           => $locator_id,
                                                                            'current_status_to'             => $state,
                                                                            'current_user_id'               => \Auth::user()->id,
                                                                            'current_description'           => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'           => Carbon::now()
                                                                        ]);
                                                                        DistribusiMovement::firstOrCreate([
                                                                            'barcode_id'            => $barcode_id,
                                                                            'locator_from'          => $last_scanned->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_scanned->status_to,
                                                                            'status_to'             => $state,
                                                                            'description'           => $state.' '.$locator->locator_name,
                                                                            'user_id'               => \Auth::user()->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => Carbon::now(),
                                                                            'updated_at'            => Carbon::now(),
                                                                            'bundle_table'          => null,
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail !=null){
                                                            return response()->json('DUPLICATE BARCODE',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah di proses scan FUSE!',422);
                                                    }
                                                }
                                                else{
                                                    return response()->json('Barcode belum proses scan IN FUSE!',422);
                                                }
                                            }
                                            else{
                                                if($update_sds->{$convert_locator['column_update']} == null) {
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                    $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                    return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'bundle_table'          => null,
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                    $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                    return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'       => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_user_id'           => \Auth::user()->id,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now()
                                                                    ]);
                                                                    
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'bundle_table'          => null,
                                                                        'factory_id'            => \Auth::user()->factory_id
                                                                    ]);
                                                                DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds != null && $sds_detail != null){
                                                        return response()->json('DUPLICATE BARCODE',422);
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];

                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    return response()->json('Barcode sudah di proses scan FUSE!',422);
                                                }
                                            }
                                        }
                                        else{
                                            return response()->json('Barcode tidak lewat proses FUSE!',422);
                                        }
                                    }
                                    //LOCATOR ARTWORK SDS
                                    else if($convert_locator['art_desc']=='artwork'){
                                        $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                        ->where('barcode_id', $barcode_id)
                                        ->first();
                                        $barcode_check_sds = DB::table('bundle_detail as bd')
                                        ->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
                                        ->leftJoin('master_style_detail as msd','msd.id','=','bd.style_detail_id')
                                        ->where('bd.sds_barcode', $barcode_id)
                                        ->whereNull('msd.deleted_at')
                                        ->first();
                                        if($cek_art->artwork == true){
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $barcode_id == $barcode_check_sds->sds_barcode){
                                                            $check_art_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->first();
                                                            $in_art_cdms = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                                            $last_loc = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->OrderBy('created_at','DESC')->first();
                                                            if($last_loc != null){
                                                                if($check_art_sds->artwork != null && $in_art_cdms == null){
                                                                    $user_cdms = DB::table('users')->where('nik',$check_art_sds->artwork_pic)->first();
                                                                    if($user_cdms == null){
                                                                        return response()->json('Terjadi Kesalahan Harap Hubungi ICT!#D0USR02',422);
                                                                    }else{
                                                                        DistribusiMovement::FirstorCreate([
                                                                            'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                            'locator_from'          => $last_loc->locator_to,
                                                                            'locator_to'            => $locator_id,
                                                                            'status_from'           => $last_loc->status_to,
                                                                            'status_to'             => 'in',
                                                                            'description'           => 'in'.' '.$locator->locator_name,
                                                                            'user_id'               => $user_cdms->id,
                                                                            'ip_address'            => $this->getIPClient(),
                                                                            'created_at'            => $check_art_sds->artwork,
                                                                            'updated_at'            => $check_art_sds->artwork,
                                                                            'factory_id'            => \Auth::user()->factory_id
                                                                        ]);
                                                                    }
                                                                }
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location' => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update'] => Carbon::now(),
                                                                    $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                    'factory' => $convert_locator['factory_sds'],
                                                                ]);
                                                                DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_locator_id'        => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DB::table('distribusi_movements')->insert([
                                                                    'id'                    => Uuid::generate()->string,
                                                                    'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                    'locator_from'          => $locator_id,
                                                                    'locator_to'            => $locator_id,
                                                                    'status_from'           => 'in',
                                                                    'status_to'             => $state,
                                                                    'description'           => $state.' '.$locator->locator_name,
                                                                    'user_id'               => \Auth::user()->id,
                                                                    'ip_address'            => $this->getIPClient(),
                                                                    'created_at'            => Carbon::now(),
                                                                    'updated_at'            => Carbon::now(),
                                                                    'factory_id'            => \Auth::user()->factory_id
                                                                ]);
                                                            }
                                                        }elseif($update_sds->barcode_id == $barcode_id && $barcode_check_sds == null){
                                                            $out_cut = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','10')->where('status_to','out')->first();
                                                            if($out_cut == null){
                                                                return response()->json('Barcode Belum Out Cutting!',422);
                                                            }else{
                                                                $last_loc       = DistribusiMovement::where('barcode_id',$barcode_id)->OrderBy('created_at','DESC')->first();
                                                                $check_art_sds  = $this->sds_connection->table('bundle_info_new')->where('barcode_id',$barcode_id)->first();
                                                                $in_cdms        = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','3')->where('status_to','in')->first();
                                                                if($check_art_sds->artwork_out != null){
                                                                    return response()->json('Barcode Sudah Scan Out Artwork!',422);
                                                                }elseif($check_art_sds->artwork == null){
                                                                    return response()->json('Barcode Belum Scan In Artwork!',422);
                                                                }else{
                                                                    try {
                                                                        DB::beginTransaction();
                                                                        
                                                                        $convert_locator = $this->convertLocator($locator_id, $state);
                                                                        if($update_sds->{$convert_locator['column_update']} == null) {
                                                                            $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                                'location'                              => $convert_locator['locator_sds'],
                                                                                $convert_locator['column_update']       => Carbon::now(),
                                                                                $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                                'factory'                               => $convert_locator['factory_sds'],
                                                                            ]);
                                                                        }
                                                                        if($check_art_sds->artwork != null && $in_cdms == null){
                                                                            $user_cdms = DB::table('users')->where('nik',$check_art_sds->artwork_pic)->first();
                                                                            DistribusiMovement::FirstOrCreate([
                                                                                'barcode_id'        => $barcode_id,
                                                                                'locator_from'      => $last_loc->locator_to,
                                                                                'status_from'       => $last_loc->status_to,
                                                                                'locator_to'        => $locator_id,
                                                                                'status_to'         => 'in',
                                                                                'user_id'           => $user_cdms == null ? Auth::user()->id : $user_cdms->id,
                                                                                'description'       => 'in'.' '.$locator->locator_name,
                                                                                'ip_address'        => $this->getIPClient(),
                                                                                'created_at'        => $check_art_sds->artwork,
                                                                                'updated_at'        => $check_art_sds->artwork,
                                                                                'deleted_at'        => null,
                                                                                'loc_dist'          => '-',
                                                                                'factory_id'   => \Auth::user()->factory_id
                                                                            ]);
                                                                        }
                                                                        DistribusiMovement::FirstOrCreate([
                                                                            'barcode_id'        => $barcode_id,
                                                                            'locator_from'      => '3',
                                                                            'status_from'       => 'in',
                                                                            'locator_to'        => $locator_id,
                                                                            'status_to'         => $state,
                                                                            'user_id'           => \Auth::user()->id,
                                                                            'description'       => $state.' '.$locator->locator_name,
                                                                            'ip_address'        => $this->getIPClient(),
                                                                            'deleted_at'        => null,
                                                                            'loc_dist'          => '-',
                                                                            'factory_id'   => \Auth::user()->factory_id
                                                                        ]);
                            
                                                                        DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                            'current_location_id'    => $locator_id,
                                                                            'current_status_to'     => $state,
                                                                            'current_description'   => $state.' '.$locator->locator_name,
                                                                            'updated_movement_at'   => Carbon::now(),
                                                                            'current_user_id'       => \Auth::user()->id
                                                                        ]);
                                                                        
                                                                        $sds_detail = DB::table('sds_detail_movements as sdm')
                                                                        ->select('vms.part','sdm.komponen_name','sdm.qty','sdm.sds_header_id')
                                                                        ->Join('v_master_style as vms','vms.id','=','sdm.style_id')
                                                                        ->where('sdm.sds_barcode',$barcode_id)
                                                                        ->first();
                                                                        $barcode_header = DB::table('sds_header_movements')->where('id', $sds_detail->sds_header_id)->first();
                                                                        $response = [
                                                                            'barcode_id'        => $barcode_id,
                                                                            'style'             => $barcode_header->style,
                                                                            'article'           => $barcode_header->article,
                                                                            'po_buyer'          => $barcode_header->poreference,
                                                                            'part'              => $sds_detail->part,
                                                                            'komponen_name'     => $sds_detail->komponen_name,
                                                                            'size'              => $barcode_header->size,
                                                                            'sticker_no'        => $barcode_header->sticker,
                                                                            'qty'               => $sds_detail->qty,
                                                                            'cut'               => $barcode_header->cut_num,
                                                                            'process'           => $locator->locator_name,
                                                                            'status'            => $state,
                                                                            'loc_dist'          => '-'
                                                                        ];
                                                                        DB::commit();
                                                                    } catch (Exception $e) {
                                                                        DB::rollback();
                                                                        $message = $e->getMessage();
                                                                        ErrorHandler::db($message);
                                                                    }
                                                                    return response()->json($response,200);
                                                                }
                                                            }
                                                        }else{
                                                            return response()->json('Barcode Tidak Dikenali!',422);
                                                        }
                                                        $komponen_detail    = $this->sds_connection->table('bundle_line_valid_new')->where('barcode_id', $barcode_id)->first();
                                                        $part_num           = $this->sds_connection->table('dd_info_bundle_supply')->where('barcode_id',$barcode_id)->first();
                                                        $type_set           = $komponen_detail->set_type;

                                                        $check_data_exists = $this->sds_connection->table('v_packing_list_new')
                                                                                ->where('style', $update_sds->style)
                                                                                ->where('set_type', $type_set)
                                                                                ->where('poreference', $update_sds->poreference)
                                                                                ->where('article', $update_sds->article)
                                                                                ->where('size', $update_sds->size)
                                                                                ->where('component', $update_sds->part_name)
                                                                                ->where('part_num', $update_sds->part_num)
                                                                                ->where('processing_fact', $update_sds->processing_fact)
                                                                                ->first();

                                                        if($check_data_exists == null) {
                                                            $check_data_exists = $this->sds_connection->table('v_packing_list_new')->insert([
                                                                'style'           => $update_sds->style,
                                                                'set_type'        => $type_set,
                                                                'poreference'     => $update_sds->poreference,
                                                                'article'         => $update_sds->article,
                                                                'size'            => $update_sds->size,
                                                                'component'       => $update_sds->part_name,
                                                                'part_num'        => $update_sds->part_num,
                                                                'processing_fact' => $update_sds->processing_fact,
                                                                'coll'            => $update_sds->coll,
                                                                'sum_qty'         => $update_sds->qty,
                                                                'first_check'     => Carbon::now(),
                                                                'first_user'      => \Auth::user()->nik,
                                                                'qty_aloc'        => 0,
                                                                'last_check'      => Carbon::now(),
                                                                'last_user'       => \Auth::user()->nik,
                                                                'is_out'          => false,
                                                            ]);
                                                        } else {
                                                            $sum_qty = $check_data_exists->sum_qty + $update_sds->qty;
                                                            $check_data_exists = $this->sds_connection->table('v_packing_list_new')->where('id', $check_data_exists->id)->update([
                                                                'sum_qty'    => $sum_qty,
                                                                'last_check' => Carbon::now(),
                                                                'last_user'  => \Auth::user()->nik,
                                                            ]);
                                                        }

                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah di proses scan ARTWORK!',422);
                                                    }
                                                }
                                                else{
                                                    return response()->json('Barcode belum proses scan IN ARTWORK!',422);
                                                }
                                            }else{
                                                return response()->json('Module Sudah Tidak Aktif!',422);
                                            }
                                        }
                                        else{
                                            return response()->json('Barcode tidak lewat proses ARTWORK!',422);
                                        }
                                    }
                                    //LOCATOR BONDING
                                    else if($convert_locator['art_desc']=='bonding'){
                                        $sds_header = $this->sds_connection->table('dd_info_bundle_supply')
                                        ->where('barcode_id', $barcode_id)
                                        ->where('processing_fact',\Auth::user()->factory_id)
                                        ->first();
                                        if($cek_art->bonding == true){
                                            $sync_proc_cdms = DB::table('v_master_style')->where('style',$update_sds->style)->where('part',$update_sds->part_num)->where('season_name',$update_sds->season)->where('komponen_name',$update_sds->part_name)->where('process_name','like','%BONDING%')->first();
                                            if($sync_proc_cdms == null){
                                                return response()->json('HARAP UPDATE MASTER STYLE PRODUCTION DI CDMS!',422);
                                            }
                                            if($state == 'out'){
                                                $in_locator = $this->convertLocator($locator_id, 'in');
                                                $cek_in = $this->sds_connection->table('bundle_info_new')
                                                ->where('barcode_id', $barcode_id)
                                                ->whereNotNull($in_locator['column_update'])
                                                ->whereNotNull($in_locator['column_update_pic'])
                                                ->first();
                                                if($cek_in != null){
                                                    if($update_sds->{$convert_locator['column_update']} == null) {
                                                        if($barcode_check_sds != null && $sds_detail == null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                                $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                    'location'                                  => $convert_locator['locator_sds'],
                                                                    $convert_locator['column_update']           => Carbon::now(),
                                                                    $convert_locator['column_update_pic']       => \Auth::user()->nik,
                                                                    'factory'                                   => $convert_locator['factory_sds'],
                                                                ]);
                                                                DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                    'current_locator_id'        => $locator_id,
                                                                    'current_status_to'         => $state,
                                                                    'current_description'       => $state.' '.$locator->locator_name,
                                                                    'updated_movement_at'       => Carbon::now(),
                                                                    'current_user_id'           => \Auth::user()->id
                                                                ]);
                                                                DistribusiMovement::firstOrCreate([
                                                                    'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                    'locator_from'          => $last_scanned->locator_to,
                                                                    'locator_to'            => $locator_id,
                                                                    'status_from'           => $last_scanned->status_to,
                                                                    'status_to'             => $state,
                                                                    'description'           => $state.' '.$locator->locator_name,
                                                                    'user_id'               => \Auth::user()->id,
                                                                    'ip_address'            => $this->getIPClient(),
                                                                    'created_at'            => Carbon::now(),
                                                                    'updated_at'            => Carbon::now(),
                                                                    'factory_id'            => Auth::user()->factory_id
                                                                ]);
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds == null && $sds_detail != null){
                                                            $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                            if($last_scanned != null){
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                                  => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']           => Carbon::now(),
                                                                        $convert_locator['column_update_pic']       => \Auth::user()->nik,
                                                                        'factory'                                   => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'           => $locator_id,
                                                                        'current_status_to'             => $state,
                                                                        'current_user_id'               => \Auth::user()->id,
                                                                        'current_description'           => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'           => Carbon::now()
                                                                    ]);

                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => Auth::user()->factory_id
                                                                    ]);
                                                                    
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }elseif($barcode_check_sds != null && $sds_detail != null){
                                                            return response()->json('DUPLICATE BARCODE',422);
                                                        }else{
                                                            return response()->json('Barcode Tidak Ditemukan!',422);
                                                        }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $update_sds->style,
                                                            'article'       => $update_sds->article,
                                                            'po_buyer'      => $update_sds->poreference,
                                                            'part'          => $update_sds->part_num,
                                                            'komponen_name' => $update_sds->part_name,
                                                            'size'          => $update_sds->size,
                                                            'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                            'qty'           => $update_sds->qty,
                                                            'cut'           => $update_sds->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => '-'
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah di proses scan '.$locator->locator_name.'!',422);
                                                    }
                                                }
                                                else{
                                                    return response()->json('Barcode belum proses scan IN '.$locator->locator_name,422);
                                                }
                                            }
                                            else{
                                                if($update_sds->{$convert_locator['column_update']} == null) {
                                                    if($barcode_check_sds != null && $sds_detail == null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_check_sds->barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                    $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                    return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                }
                                                            }
                                                            try{
                                                                DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location' => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update'] => Carbon::now(),
                                                                        $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                                        'factory' => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_locator_id'        => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now(),
                                                                        'current_user_id'           => \Auth::user()->id
                                                                    ]);
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_check_sds->barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'factory_id'            => Auth::user()->factory_id
                                                                    ]);
                                                                    DB::commit();
                                                            }catch(Exception $e){
                                                                DB::rollback();
                                                                $message = $e->getMessage();
                                                                ErrorHandler::db($message);
                                                            }
                                                        }else{
                                                            return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                        }
                                                    }elseif($barcode_check_sds == null && $sds_detail != null){
                                                        $last_scanned = DistribusiMovement::where('barcode_id',$barcode_id)->orderBy('created_at','DESC')->first();
                                                        if($last_scanned != null){
                                                            if($last_scanned != null){
                                                                if(in_array($last_scanned->locator_to,$in_out_loc)){
                                                                    if($last_scanned->status_to == 'in' && $locator_id != $last_scanned->locator_to){
                                                                        $loc_name = DB::table('master_locator')->where('id',$last_scanned->locator_to)->first();
                                                                        return response()->json('BUNDLE BELUM SCAN OUT '.$loc_name->locator_name,422);
                                                                    }
                                                                }
                                                                try{
                                                                    DB::beginTransaction();
                                                                    $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                                                                        'location'                              => $convert_locator['locator_sds'],
                                                                        $convert_locator['column_update']       => Carbon::now(),
                                                                        $convert_locator['column_update_pic']   => \Auth::user()->nik,
                                                                        'factory'                               => $convert_locator['factory_sds'],
                                                                    ]);
                                                                    
                                                                    DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                                                                        'current_location_id'       => $locator_id,
                                                                        'current_status_to'         => $state,
                                                                        'current_user_id'           => \Auth::user()->id,
                                                                        'current_description'       => $state.' '.$locator->locator_name,
                                                                        'updated_movement_at'       => Carbon::now()
                                                                    ]);
    
                                                                    DistribusiMovement::firstOrCreate([
                                                                        'barcode_id'            => $barcode_id,
                                                                        'locator_from'          => $last_scanned->locator_to,
                                                                        'locator_to'            => $locator_id,
                                                                        'status_from'           => $last_scanned->status_to,
                                                                        'status_to'             => $state,
                                                                        'description'           => $state.' '.$locator->locator_name,
                                                                        'user_id'               => \Auth::user()->id,
                                                                        'ip_address'            => $this->getIPClient(),
                                                                        'created_at'            => Carbon::now(),
                                                                        'updated_at'            => Carbon::now(),
                                                                        'bundle_table'          => null,
                                                                        'factory_id'            => Auth::user()->factory_id
                                                                    ]);
                                                                    DB::commit();
                                                                }catch(Exception $e){
                                                                    DB::rollback();
                                                                    $message = $e->getMessage();
                                                                    ErrorHandler::db($message);
                                                                }
                                                            }else{
                                                                return response()->json('TERJADI KESALAHAN HUB.ICT #E-OC'.Auth::user()->factory_id,422);
                                                            }
                                                        }
                                                    }else{
                                                        return response()->json('Barcode Tidak Ditemukan!',422);
                                                    }
                                                    $response = [
                                                        'barcode_id'    => $barcode_id,
                                                        'style'         => $update_sds->style,
                                                        'article'       => $update_sds->article,
                                                        'po_buyer'      => $update_sds->poreference,
                                                        'part'          => $update_sds->part_num,
                                                        'komponen_name' => $update_sds->part_name,
                                                        'size'          => $update_sds->size,
                                                        'sticker_no'    => $update_sds->start_num.' - '.$update_sds->end_num,
                                                        'qty'           => $update_sds->qty,
                                                        'cut'           => $update_sds->cut_num,
                                                        'process'       => $locator->locator_name,
                                                        'status'        => $state,
                                                        'loc_dist'      => '-'
                                                    ];

                                                    return response()->json($response,200);
                                                }
                                                else{
                                                    return response()->json('Barcode sudah di proses scan '.$locator->locator_name.'!',422);
                                                }
                                            }
                                        }
                                        else{
                                            return response()->json('Barcode tidak lewat proses '.$locator->locator_name.'!',422);
                                        }                                            
                                    }
                                }
                            }
                        }
                    }else{
                        return response()->json('Barcode tidak dikenali!',422);
                    }
                }
            }else{
                return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
            }
        }
    }

    public function scanComponent_nonsds(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_input;
            $locator_id = $request->locator_id;
            $state = $request->state;

            if($barcode_id != null && $locator_id != null) {

                $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                if($barcode_check != null) {

                    if($locator->is_cutting) {

                        // locator cutting

                        $component_movement_out = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                        if($component_movement_out->count() > 0) {
                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                        } else {

                            $bundle_check = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'out')->where('deleted_at', null)->get();

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = DB::table('master_komponen')->where('id', $komponen_detail->komponen)->where('deleted_at', null)->first()->komponen_name;

                            try {
                                DB::beginTransaction();

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => null,
                                    'status_from'  => null,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => '-'
                                ]);

                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $barcode_header->style,
                                    'article'       => $barcode_header->article,
                                    'po_buyer'      => $barcode_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $barcode_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $barcode_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => '-'
                                ];

                                // if($barcode_check->sds_barcode != null) {
                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                //     if($update_sds != null) {
                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                //                 'location' => $convert_locator['locator_sds'],
                                //                 $convert_locator['column_update'] => Carbon::now(),
                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                //                 'factory' => $convert_locator['factory_sds'],
                                //             ]);
                                //         }
                                //     }
                                // }

                                DB::commit();

                            } catch (Exception $e) {
                                DB::rollback();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }

                            return response()->json($response,200);
                        }

                            } else if($locator->is_setting){

                                $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                                if($out_cutting_check != null){

                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                                    $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                                    $bundle_terima = $movement_check->count();
                                    $bundle_check  = $movement_check->first();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    $komponen_name = $barcode_check->komponen_name;

                                    if($bundle_terima<1){
                                        $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                                        $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                                        $ppcm_check = DB::table('master_ppcm')->where([
                                            ['master_ppcm.factory_id',$bundle_header->factory_id],
                                            ['set_type',$set_type],
                                            ['season',$bundle_header->season],
                                            ['articleno',$bundle_header->article],
                                            ['po_buyer',$bundle_header->poreference],
                                            ['master_ppcm.deleted_at', null]
                                        ])
                                        ->select('master_ppcm.*','master_setting.area_name')
                                        ->leftJoin('master_setting', 'master_ppcm.area_id', '=', 'master_setting.id')
                                        ->first();

                                        if(!$ppcm_check){
                                            return response()->json($barcode_id, 331);
                                        } else {
                                            //master_style_detail
                                            $style_detail_id = $barcode_check->style_detail_id;

                                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                                            if(strlen(trim($process_id->process))>0){
                                                $array_process = explode(",",$process_id->process);
                                            }
                                            else{
                                                $array_process = array();
                                            }

                                            if(count($array_process) == 0){

                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $ppcm_check->area_name
                                                ]);

                                                // if($bundle_terima == $banyak_komponen){
                                                    // dd($barcode_hasil->qty);
                                                    $qty_actual = ($ppcm_check->qty_actual == null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                                                    MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                // }

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $bundle_header->style,
                                                    'article'       => $bundle_header->article,
                                                    'po_buyer'      => $bundle_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $bundle_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $bundle_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => $ppcm_check->area_name
                                                ];
                                                return response()->json($response,200);
                                            } else {
                                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                                $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                                                $banyak_process = count($get_process_locator) + 1;

                                                array_push($array_locator,10);

                                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                                $count_process_check = $process_check->count();

                                                if($count_process_check == $banyak_process){
                                                    if($ppcm_check->qty_actual < $ppcm_check->qty_split){

                                                        $barcode_bundle_search = DB::table('bundle_detail')->where([
                                                            ['bundle_header_id', $barcode_check->bundle_header_id],
                                                            ['start_no',$barcode_check->start_no],
                                                            ['end_no',$barcode_check->end_no]
                                                        ]);

                                                        $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                                        $barcode_hasil = $barcode_bundle_search->first();

                                                        $banyak_komponen = $barcode_bundle_search->count();

                                                        // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                                                        // $bundle_terima = $movement_check->count();
                                                        // $bundle_check  = $movement_check->first();

                                                        $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                                        $komponen_name = $barcode_check->komponen_name;

                                                        $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                                                        if($check_movement > 0) {
                                                            return response()->json('Barcode sudah di scan in!',422);
                                                        }

                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $ppcm_check->area_name
                                                        ]);

                                                        $qty_actual = ($ppcm_check->qty_actual==null ? 0 : $ppcm_check->qty_actual) + $barcode_hasil->qty;
                                                        MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);


                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $ppcm_check->area_name
                                                        ];
                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        //qty berlebihan
                                                        return response()->json($barcode_id, 331);
                                                    }
                                                } else {

                                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                                    $info_belum_scan = '';
                                                    foreach ($locator_belum_scan as $key => $value) {
                                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                                    }

                                                    $info = trim($info_belum_scan,", ");

                                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                                }
                                            }
                                        }

                                    } else {

                                        $style_detail_id = $barcode_check->style_detail_id;

                                        $process_id = DB::table('master_style_detail')->where('id',$style_detail_id)->first();

                                        if($process_id->process == null || $process_id->process == '' ){
                                            $array_process = array();
                                        } else {
                                            $array_process = explode(",",$process_id->process);
                                        }

                                        if(count($array_process) < 1){

                                            $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                                            if($check_movement > 0) {
                                                return response()->json('Barcode sudah di scan in!',422);
                                            }
                                            // dd(!$bundle_check);
                                            if(!$bundle_check){
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => $bundle_check->loc_dist
                                                ]);

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $bundle_header->style,
                                                    'article'       => $bundle_header->article,
                                                    'po_buyer'      => $bundle_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $bundle_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $bundle_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => $bundle_check->loc_dist
                                                ];
                                                return response()->json($response,200);
                                            } else {
                                                if($bundle_check->locator_to == $locator_id){

                                                    $check_record = DistribusiMovement::where([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $out_cutting_check->locator_to,
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => $bundle_check->loc_dist
                                                    ])->exists();

                                                    if(!$check_record){
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $bundle_check->loc_dist
                                                        ]);

                                                        // if($bundle_terima == $banyak_komponen){
                                                        //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                                                        //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                        // }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $bundle_check->loc_dist
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah pernah scan!',422);
                                                    }

                                                }
                                                else{
                                                    return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                                }

                                            }
                                        } else {
                                            $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                            $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                                            $banyak_process = count($get_process_locator) + 1;

                                            array_push($array_locator,10);

                                            $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                            $count_process_check = $process_check->count();

                                            if($count_process_check == $banyak_process){

                                                $barcode_bundle_search = DB::table('bundle_detail')->where([
                                                    ['bundle_header_id', $barcode_check->bundle_header_id],
                                                    ['start_no',$barcode_check->start_no],
                                                    ['end_no',$barcode_check->end_no]
                                                ]);

                                                $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                                $barcode_hasil = $barcode_bundle_search->first();

                                                $banyak_komponen = $barcode_bundle_search->count();

                                                // $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->where('status_to', 'in')->where('deleted_at', null);

                                                // $bundle_terima = $movement_check->count();
                                                // $bundle_check  = $movement_check->first();

                                                $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                                if($bundle_check->locator_to == $locator_id){

                                                    $check_record = DistribusiMovement::where([
                                                        'barcode_id'   => $barcode_id,
                                                        'locator_from' => $out_cutting_check->locator_to,
                                                        'status_from'  => $out_cutting_check->status_to,
                                                        'locator_to'   => $locator_id,
                                                        'status_to'    => $state,
                                                        'description'  => $state.' '.$locator->locator_name,
                                                        'deleted_at'   => null,
                                                        'loc_dist'     => $bundle_check->loc_dist
                                                    ])->exists();

                                                    if(!$check_record){
                                                        DistribusiMovement::FirstOrCreate([
                                                            'barcode_id'   => $barcode_id,
                                                            'locator_from' => $out_cutting_check->locator_to,
                                                            'status_from'  => $out_cutting_check->status_to,
                                                            'locator_to'   => $locator_id,
                                                            'status_to'    => $state,
                                                            'user_id'      => \Auth::user()->id,
                                                            'description'  => $state.' '.$locator->locator_name,
                                                            'ip_address'   => $this->getIPClient(),
                                                            'deleted_at'   => null,
                                                            'loc_dist'     => $bundle_check->loc_dist
                                                        ]);

                                                        // $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                        // if($bundle_terima == $banyak_komponen){
                                                        //     $qty_actual = $ppcm_check->qty_actual + $bundle_check->qty;
                                                        //     MasterPpcm::where('id',$ppcm_check->id)->update(['qty_actual'=>$qty_actual]);
                                                        // }
                                                        $response = [
                                                            'barcode_id'    => $barcode_id,
                                                            'style'         => $bundle_header->style,
                                                            'article'       => $bundle_header->article,
                                                            'po_buyer'      => $bundle_header->poreference,
                                                            'part'          => $komponen_detail->part,
                                                            'komponen_name' => $komponen_name,
                                                            'size'          => $bundle_header->size,
                                                            'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                            'qty'           => $barcode_check->qty,
                                                            'cut'           => $bundle_header->cut_num,
                                                            'process'       => $locator->locator_name,
                                                            'status'        => $state,
                                                            'loc_dist'      => $bundle_check->loc_dist
                                                        ];

                                                        return response()->json($response,200);
                                                    }
                                                    else{
                                                        return response()->json('Barcode sudah pernah scan!',422);
                                                    }

                                                }
                                                else{
                                                    return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                                }
                                            }
                                            else{

                                                $get_process_check = $process_check->pluck('locator_to')->toArray();

                                                $array_belum_scan = array_diff($array_locator,$get_process_check);

                                                $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                                $info_belum_scan = '';
                                                foreach ($locator_belum_scan as $key => $value) {
                                                    $info_belum_scan .= ''.$value->locator_name.', ';
                                                }

                                                $info = trim($info_belum_scan,", ");

                                                return response()->json('Barcode belum discan proses '.$info.'!',422);
                                            }
                                        }
                                    }

                                    $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                                }
                                else{
                                    return response()->json('Barcode belum out cutting!',422);
                                }
                                ////////////////////////////////////////////////////////////////////////////////////////////////////
                    }else if($locator->is_artwork) {

                        // locator artwork

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);

                                                //             $type_set = DB::table('types')->where('id', $komponen_detail->type_id)->first();
                                                //             if($type_set->type_name == 'Non') {
                                                //                 $type_set = '0';
                                                //             } else {
                                                //                 $type_set = $type_set->type_name;
                                                //             }

                                                //             $check_data_exists = $this->sds_connection->table('v_packing_list_new')
                                                //                                     ->where('style', $barcode_header->style)
                                                //                                     ->where('set_type', $type_set)
                                                //                                     ->where('poreference', $barcode_header->poreference)
                                                //                                     ->where('article', $barcode_header->article)
                                                //                                     ->where('size', $barcode_header->size)
                                                //                                     ->where('component', $komponen_name)
                                                //                                     ->where('part_num', $komponen_detail->part)
                                                //                                     ->where('processing_fact', $barcode_header->factory_id)
                                                //                                     ->first();

                                                //             if($check_data_exists == null) {
                                                //                 $check_data_exists = $this->sds_connection->table('v_packing_list_new')->insert([
                                                //                     'style' => $barcode_header->style,
                                                //                     'set_type' => $type_set,
                                                //                     'poreference' => $barcode_header->poreference,
                                                //                     'article' => $barcode_header->article,
                                                //                     'size' => $barcode_header->size,
                                                //                     'component' => $komponen_name,
                                                //                     'part_num' => $komponen_detail->part,
                                                //                     'processing_fact' => $barcode_header->factory_id,
                                                //                     'coll' => $barcode_header->color_name,
                                                //                     'sum_qty' => $barcode_check->qty,
                                                //                     'first_check' => Carbon::now(),
                                                //                     'first_user' => \Auth::user()->nik,
                                                //                     'qty_aloc' => 0,
                                                //                     'last_check' => Carbon::now(),
                                                //                     'last_user' => \Auth::user()->nik,
                                                //                     'is_out' => false,
                                                //                 ]);
                                                //             } else {
                                                //                 $sum_qty = $check_data_exists->sum_qty + $barcode_check->qty;
                                                //                 $check_data_exists = $this->sds_connection->table('v_packing_list_new')->where('id', $check_data_exists->id)->update([
                                                //                     'sum_qty' => $sum_qty,
                                                //                     'last_check' => Carbon::now(),
                                                //                     'last_user' => \Auth::user()->nik,
                                                //                 ]);
                                                //             }
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];
                                                DB::commit();

                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    } else {

                        // locator secondary process

                        $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                        if($out_cutting_check != null) {

                            $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                            $komponen_name = $barcode_check->komponen_name;

                            if($komponen_detail != null) {

                                $get_process = explode(',', $komponen_detail->process);

                                if($get_process[0] == null || $get_process[0] == '') {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }

                                $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                                if(in_array($locator_id, $get_locator_process)) {

                                    if($state == 'in') {

                                        $component_movement_in = DistribusiMovement::where('barcode_id', $barcode_id)->Where('locator_to', '=', $locator_id)->where('status_to', 'in')->where('deleted_at', null)->get();

                                        if($component_movement_in->count() > 0) {
                                            return response()->json('Barcode sudah pernah scan '.$state.' '.$locator->locator_name.'!',422);
                                        } else {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id'   => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from'  => $out_cutting_check->status_to,
                                                    'locator_to'   => $locator_id,
                                                    'status_to'    => $state,
                                                    'user_id'      => \Auth::user()->id,
                                                    'description'  => $state.' '.$locator->locator_name,
                                                    'ip_address'   => $this->getIPClient(),
                                                    'deleted_at'   => null,
                                                    'loc_dist'     => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id'    => $barcode_id,
                                                    'style'         => $barcode_header->style,
                                                    'article'       => $barcode_header->article,
                                                    'po_buyer'      => $barcode_header->poreference,
                                                    'part'          => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size'          => $barcode_header->size,
                                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty'           => $barcode_check->qty,
                                                    'cut'           => $barcode_header->cut_num,
                                                    'process'       => $locator->locator_name,
                                                    'status'        => $state,
                                                    'loc_dist'      => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();
                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        }
                                    } else {

                                        if($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'in') {

                                            try {
                                                DB::beginTransaction();
                                                DistribusiMovement::FirstOrCreate([
                                                    'barcode_id' => $barcode_id,
                                                    'locator_from' => $out_cutting_check->locator_to,
                                                    'status_from' => $out_cutting_check->status_to,
                                                    'locator_to' => $locator_id,
                                                    'status_to' => $state,
                                                    'user_id' => \Auth::user()->id,
                                                    'description' => $state.' '.$locator->locator_name,
                                                    'ip_address' => $this->getIPClient(),
                                                    'deleted_at' => null,
                                                    'loc_dist' => '-'
                                                ]);

                                                $barcode_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                                                $response = [
                                                    'barcode_id' => $barcode_id,
                                                    'style' => $barcode_header->style,
                                                    'article' => $barcode_header->article,
                                                    'po_buyer' => $barcode_header->poreference,
                                                    'part' => $komponen_detail->part,
                                                    'komponen_name' => $komponen_name,
                                                    'size' => $barcode_header->size,
                                                    'sticker_no' => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                    'qty' => $barcode_check->qty,
                                                    'cut' => $barcode_header->cut_num,
                                                    'process' => $locator->locator_name,
                                                    'status' => $state,
                                                    'loc_dist' => '-'
                                                ];

                                                // if($barcode_check->sds_barcode != null) {
                                                //     $update_sds = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->first();
                                                //     if($update_sds != null) {
                                                //         $convert_locator = $this->convertLocator($locator_id, $state);
                                                //         if($update_sds->{$convert_locator['column_update']} == null) {
                                                //             $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_check->sds_barcode)->update([
                                                //                 'location' => $convert_locator['locator_sds'],
                                                //                 $convert_locator['column_update'] => Carbon::now(),
                                                //                 $convert_locator['column_update_pic'] => \Auth::user()->nik,
                                                //                 'factory' => $convert_locator['factory_sds'],
                                                //             ]);
                                                //         }
                                                //     }
                                                // }

                                                DB::commit();

                                            } catch (Exception $e) {
                                                DB::rollback();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }
                                            return response()->json($response,200);
                                        } elseif($out_cutting_check->locator_to == $locator_id && $out_cutting_check->status_to == 'out') {
                                            return response()->json('Barcode sudah scan out '.$locator->locator_name.'!',422);
                                        } else {
                                            return response()->json('Barcode belum scan in '.$locator->locator_name.'!',422);
                                        }
                                    }
                                } else {
                                    return response()->json('Barcode tidak perlu melalui proses ini!',422);
                                }
                            } else {
                                return response()->json('komponen sudah di hapus dari sistem!',422);
                            }
                        } else {
                            return response()->json('Barcode belum out cutting!',422);
                        }
                    }
                } else {
                    return response()->json('Barcode tidak dikenali!',422);
                }
            } else {
                return response()->json('Terjadi kesalahan saat input data!',422);
            }
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }


    public function scanManualSet(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = 11;
            $state = 'in';
            $area_name = $request->area_name;
            //SCAN WITH SELECT AREA SETTING USING THIS CODE NOT scanComponent function
            if($barcode_id != null && $locator_id != null && $area_name != null) {

                // SINKRON SDS ///////////////////////////////////////////////////////////////////////////////////////////////////

                $user = \Auth::user()->nik;
                $alert=NULL;
                $dstat=1;

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                $bundle_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                $sds_barcode = DB::table('bundle_detail')->where('sds_barcode', $barcode_id)->first();
                $sds_detail = DB::table('sds_detail_movements')->where('sds_barcode', $barcode_id)->first();

                if($bundle_check == null && $sds_detail != null && $sds_barcode == null){
                    $not_allow = $this->sds_connection
                    ->table('temp_scan_proc')
                    ->where('ap_id', $barcode_id)
                    ->where('proc', 'SET')
                    ->get();

                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($area_check == '0') {
                        $area_name = 'L.'.substr($area_name, 1);
                    } else {
                        $area_name = 'L.'.$area_name;
                    }

                    if(count($not_allow) > 0){
                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->update([
                            'location' => $area_name,
                            'factory'=> 'AOI '.\Auth::user()->factory_id,
                            'setting'=> Carbon::now(),
                            'setting_pic'=> $user,
                            'supply_unlock'=> false
                        ]);
                        $last_movement = DistribusiMovement::where('barcode_id', $barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
                        DistribusiMovement::FirstOrCreate([
                            'barcode_id'   => $barcode_id,
                            'locator_from' => $last_movement->locator_to,
                            'status_from'  => $last_movement->status_to,
                            'locator_to'   => $locator_id,
                            'status_to'    => $state,
                            'user_id'      => \Auth::user()->id,
                            'description'  => $state.' '.$locator->locator_name,
                            'ip_address'   => $this->getIPClient(),
                            'deleted_at'   => null,
                            'loc_dist'     => $area_name,
                            'factory_id'   => \Auth::user()->factory_id
                        ]);
                        DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->update([
                            'current_location_id' => $last_movement->locator_to,
                            'current_status_to' => $state,
                            'current_description' => $state.' '.$locator->locator_name,
                            'updated_movement_at' => Carbon::now(),
                            'current_user_id' => \Auth::user()->id
                        ]);
                    } else {
                        $alert="terjadi kesalahan, silakan scan ulang!";
                        $dstat=0;
                    }

                    if($dstat > 0) {
                        $this->sds_connection->table('system_log')->insert([
                            'user_nik' => $user,
                            'user_proc' => 'SET',
                            'log_operation' => 'set_checkin',
                            'log_text' =>  '1 items is checked in.',
                        ]);

                        $this->sds_connection
                            ->table('temp_scan_proc')
                            ->where('ap_id',$barcode_id)
                            ->where('proc','SET')
                            ->delete();

                        $sds_bundle = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $barcode_id)->first();

                        $response = [
                            'barcode_id'    => $barcode_id,
                            'style'         => $sds_bundle->style,
                            'article'       => $sds_bundle->article,
                            'po_buyer'      => $sds_bundle->poreference,
                            'part'          => $sds_bundle->part_num,
                            'komponen_name' => $sds_bundle->part_name,
                            'size'          => $sds_bundle->size,
                            'sticker_no'    => $sds_bundle->start_num.' - '.$sds_bundle->end_num,
                            'qty'           => $sds_bundle->qty,
                            'cut'           => $sds_bundle->cut_num,
                            'process'       => $locator->locator_name,
                            'status'        => $state,
                            'loc_dist'      => $area_name
                        ];

                        return response()->json($response,200);
                    } else {
                        return response()->json($alert,422);
                    }
                }elseif($bundle_check == null && $sds_barcode != null && $sds_detail == null ){
                    $not_allow = $this->sds_connection
                    ->table('temp_scan_proc')
                    ->where('ap_id', $sds_barcode->sds_barcode)
                    ->where('proc', 'SET')
                    ->get();

                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($area_check == '0') {
                        $area_name = 'L.'.substr($area_name, 1);
                    } else {
                        $area_name = 'L.'.$area_name;
                    }

                    if(count($not_allow) > 0){
                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $sds_barcode->sds_barcode)->update([
                            'location' => $area_name,
                            'factory'=> 'AOI '.\Auth::user()->factory_id,
                            'setting'=> Carbon::now(),
                            'setting_pic'=> $user,
                            'supply_unlock'=> false
                        ]);
                        $last_movement = DistribusiMovement::where('barcode_id', $sds_barcode->barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
                        DistribusiMovement::FirstOrCreate([
                            'barcode_id'   => $sds_barcode->barcode_id,
                            'locator_from' => $last_movement->locator_to,
                            'status_from'  => $last_movement->status_to,
                            'locator_to'   => $locator_id,
                            'status_to'    => $state,
                            'user_id'      => \Auth::user()->id,
                            'description'  => $state.' '.$locator->locator_name,
                            'ip_address'   => $this->getIPClient(),
                            'deleted_at'   => null,
                            'loc_dist'     => $area_name,
                            'factory_id'   => \Auth::user()->factory_id
                        ]);

                        DB::table('bundle_detail')->where('barcode_id',$sds_barcode->barcode_id)->update([
                            'current_locator_id' => $last_movement->locator_to,
                            'current_status_to' => $state,
                            'current_description' => $state.' '.$locator->locator_name,
                            'updated_movement_at' => Carbon::now(),
                            'current_user_id' => \Auth::user()->id
                        ]);
                    } else {
                        $alert="terjadi kesalahan, silakan scan ulang!";
                        $dstat=0;
                    }

                    if($dstat > 0) {
                        $this->sds_connection->table('system_log')->insert([
                            'user_nik' => $user,
                            'user_proc' => 'SET',
                            'log_operation' => 'set_checkin',
                            'log_text' =>  '1 items is checked in.',
                        ]);

                        $this->sds_connection
                            ->table('temp_scan_proc')
                            ->where('ap_id',$sds_barcode->sds_barcode)
                            ->where('proc','SET')
                            ->delete();

                        $bundle_header = DB::table('bundle_header')->where('id', $sds_barcode->bundle_header_id)->first();
                        $komponen_detail = DB::table('master_style_detail')->where('id', $sds_barcode->style_detail_id)->where('deleted_at', null)->first();
                        $komponen_name = $sds_barcode->komponen_name;

                        $response = [
                            'barcode_id'    => $barcode_id,
                            'style'         => $bundle_header->style,
                            'article'       => $bundle_header->article,
                            'po_buyer'      => $bundle_header->poreference,
                            'part'          => $komponen_detail->part,
                            'komponen_name' => $komponen_name,
                            'size'          => $bundle_header->size,
                            'sticker_no'    => $sds_barcode->start_no.' - '.$sds_barcode->end_no,
                            'qty'           => $sds_barcode->qty,
                            'cut'           => $bundle_header->cut_num,
                            'process'       => $locator->locator_name,
                            'status'        => $state,
                            'loc_dist'      => $area_name
                        ];

                        return response()->json($response,200);
                    } else {
                        return response()->json($alert,422);
                    }
                }elseif($bundle_check != null && $sds_barcode == null  && $sds_detail == null){
                    $not_allow = $this->sds_connection
                    ->table('temp_scan_proc')
                    ->where('ap_id', $bundle_check->sds_barcode)
                    ->where('proc', 'SET')
                    ->get();

                    $area_name = substr($area_name, 4);
                    $area_check = substr($area_name, 0, 1);
                    if($area_check == '0') {
                        $area_name = 'L.'.substr($area_name, 1);
                    } else {
                        $area_name = 'L.'.$area_name;
                    }

                    if(count($not_allow) > 0){
                        $this->sds_connection->table('bundle_info_new')->where('barcode_id', $bundle_check->sds_barcode)->update([
                            'location' => $area_name,
                            'factory'=> 'AOI '.\Auth::user()->factory_id,
                            'setting'=> Carbon::now(),
                            'setting_pic'=> $user,
                            'supply_unlock'=> false
                        ]);
                        $last_movement = DistribusiMovement::where('barcode_id', $barcode_id)->whereNull('deleted_at')->orderBy('created_at', 'desc')->first();
                        DistribusiMovement::FirstOrCreate([
                            'barcode_id'   => $barcode_id,
                            'locator_from' => $last_movement->locator_to,
                            'status_from'  => $last_movement->status_to,
                            'locator_to'   => $locator_id,
                            'status_to'    => $state,
                            'user_id'      => \Auth::user()->id,
                            'description'  => $state.' '.$locator->locator_name,
                            'ip_address'   => $this->getIPClient(),
                            'deleted_at'   => null,
                            'loc_dist'     => $area_name,
                            'factory_id'   => \Auth::user()->factory_id
                        ]);
                        DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
                            'current_locator_id' => $last_movement->locator_to,
                            'current_status_to' => $state,
                            'current_description' => $state.' '.$locator->locator_name,
                            'updated_movement_at' => Carbon::now(),
                            'current_user_id' => \Auth::user()->id
                        ]);
                    } else {
                        $alert="terjadi kesalahan, silakan scan ulang!";
                        $dstat=0;
                    }

                    if($dstat > 0) {
                        $this->sds_connection->table('system_log')->insert([
                            'user_nik' => $user,
                            'user_proc' => 'SET',
                            'log_operation' => 'set_checkin',
                            'log_text' =>  '1 items is checked in.',
                        ]);

                        $this->sds_connection
                            ->table('temp_scan_proc')
                            ->where('ap_id',$bundle_check->sds_barcode)
                            ->where('proc','SET')
                            ->delete();

                        $sds_bundle = $this->sds_connection->table('bundle_info_new')->where('barcode_id', $bundle_check->sds_barcode)->first();

                        $response = [
                            'barcode_id'    => $barcode_id,
                            'style'         => $sds_bundle->style,
                            'article'       => $sds_bundle->article,
                            'po_buyer'      => $sds_bundle->poreference,
                            'part'          => $sds_bundle->part_num,
                            'komponen_name' => $sds_bundle->part_name,
                            'size'          => $sds_bundle->size,
                            'sticker_no'    => $sds_bundle->start_num.' - '.$sds_bundle->end_num,
                            'qty'           => $sds_bundle->qty,
                            'cut'           => $sds_bundle->cut_num,
                            'process'       => $locator->locator_name,
                            'status'        => $state,
                            'loc_dist'      => $area_name
                        ];

                        return response()->json($response,200);
                    } else {
                        return response()->json($alert,422);
                    }
                }
                else{
                    return response()->json('TERJADI KESALAHAN PEMILIHAN RAK! HUB.ICT',422);
                }

            }
        }
    }

    public function scanManualSet_nonsds(Request $request)
    {
        if(request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $locator_id = 11;
            $state = 'in';
            $area_name = $request->area_name;

            if($barcode_id != null && $locator_id != null && $area_name != null) {

                $barcode_check = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();

                $locator = DB::table('master_locator')->where('id', $locator_id)->first();

                if($barcode_check != null) {

                    $out_cutting_check = DistribusiMovement::where('barcode_id', $barcode_id)->orderBy('created_at', 'desc')->first();

                    if($out_cutting_check != null){

                        $barcode_bundle_search = DB::table('bundle_detail')->where([
                            ['bundle_header_id', $barcode_check->bundle_header_id],
                            ['start_no',$barcode_check->start_no],
                            ['end_no',$barcode_check->end_no]
                        ]);

                        $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();

                        $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                        $barcode_hasil = $barcode_bundle_search->first();

                        $banyak_komponen = $barcode_bundle_search->count();

                        $locator_setting = DB::table('master_locator')->where('is_setting', true)->pluck('id')->toArray();

                        $movement_check = DistribusiMovement::whereIn('barcode_id', $barcode_array)->whereIn('locator_to', $locator_setting)->where('status_to', 'in')->where('deleted_at', null);

                        $bundle_terima = $movement_check->count();
                        $bundle_check  = $movement_check->first();

                        $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                        $komponen_name = $barcode_check->komponen_name;

                        if($bundle_terima < 1){
                            $bundle_header = DB::table('bundle_header')->where('id', $barcode_check->bundle_header_id)->first();
                            $set_type = DB::table('types')->where('id', $komponen_detail->type_id)->first()->type_name;

                            $style_detail_id = $barcode_check->style_detail_id;

                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                            if(strlen(trim($process_id->process))>0){
                                $array_process = explode(",",$process_id->process);
                            }
                            else{
                                $array_process = array();
                            }

                            if(count($array_process) == 0){

                                DistribusiMovement::FirstOrCreate([
                                    'barcode_id'   => $barcode_id,
                                    'locator_from' => $out_cutting_check->locator_to,
                                    'status_from'  => $out_cutting_check->status_to,
                                    'locator_to'   => $locator_id,
                                    'status_to'    => $state,
                                    'user_id'      => \Auth::user()->id,
                                    'description'  => $state.' '.$locator->locator_name,
                                    'ip_address'   => $this->getIPClient(),
                                    'deleted_at'   => null,
                                    'loc_dist'     => $area_name
                                ]);

                                $response = [
                                    'barcode_id'    => $barcode_id,
                                    'style'         => $bundle_header->style,
                                    'article'       => $bundle_header->article,
                                    'po_buyer'      => $bundle_header->poreference,
                                    'part'          => $komponen_detail->part,
                                    'komponen_name' => $komponen_name,
                                    'size'          => $bundle_header->size,
                                    'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                    'qty'           => $barcode_check->qty,
                                    'cut'           => $bundle_header->cut_num,
                                    'process'       => $locator->locator_name,
                                    'status'        => $state,
                                    'loc_dist'      => $area_name
                                ];
                                return response()->json($response,200);
                            } else {
                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                $array_locator =  DB::table('master_locator')->where('id', $get_process_locator)->whereNull('deleted_at')->pluck('id')->toArray();
                                $banyak_process = count($get_process_locator) + 1;

                                array_push($array_locator,10);

                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->whereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                $count_process_check = $process_check->count();

                                if($count_process_check == $banyak_process){
                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    $komponen_name = $barcode_check->komponen_name;

                                    $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $locator_id)->where('status_to', $state)->count();

                                    if($check_movement > 0) {
                                        return response()->json('Barcode sudah di scan in!',422);
                                    }

                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => $out_cutting_check->locator_to,
                                        'status_from'  => $out_cutting_check->status_to,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => $area_name
                                    ]);

                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $bundle_header->style,
                                        'article'       => $bundle_header->article,
                                        'po_buyer'      => $bundle_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $komponen_name,
                                        'size'          => $bundle_header->size,
                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                        'qty'           => $barcode_check->qty,
                                        'cut'           => $bundle_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $area_name
                                    ];
                                    return response()->json($response,200);
                                } else {

                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                    $info_belum_scan = '';
                                    foreach ($locator_belum_scan as $key => $value) {
                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                    }

                                    $info = trim($info_belum_scan,", ");

                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                }
                            }
                        } else {

                            $style_detail_id = $barcode_check->style_detail_id;

                            $process_id = DB::table('master_style_detail')->where('id', $style_detail_id)->first();

                            if($process_id->process == null || $process_id->process == '' ){
                                $array_process = array();
                            } else {
                                $array_process = explode(",",$process_id->process);
                            }

                            if(count($array_process) < 1){

                                $check_movement = DistribusiMovement::where('barcode_id', $barcode_id)->where('locator_to', $out_cutting_check->locator_to)->where('status_to', $state)->count();

                                if($check_movement > 0) {
                                    return response()->json('Barcode sudah di scan in!',422);
                                }
                                // dd(!$bundle_check);
                                if(!$bundle_check){
                                    DistribusiMovement::FirstOrCreate([
                                        'barcode_id'   => $barcode_id,
                                        'locator_from' => $out_cutting_check->locator_to,
                                        'status_from'  => $out_cutting_check->status_to,
                                        'locator_to'   => $locator_id,
                                        'status_to'    => $state,
                                        'user_id'      => \Auth::user()->id,
                                        'description'  => $state.' '.$locator->locator_name,
                                        'ip_address'   => $this->getIPClient(),
                                        'deleted_at'   => null,
                                        'loc_dist'     => $bundle_check->loc_dist
                                    ]);

                                    $response = [
                                        'barcode_id'    => $barcode_id,
                                        'style'         => $bundle_header->style,
                                        'article'       => $bundle_header->article,
                                        'po_buyer'      => $bundle_header->poreference,
                                        'part'          => $komponen_detail->part,
                                        'komponen_name' => $komponen_name,
                                        'size'          => $bundle_header->size,
                                        'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                        'qty'           => $barcode_check->qty,
                                        'cut'           => $bundle_header->cut_num,
                                        'process'       => $locator->locator_name,
                                        'status'        => $state,
                                        'loc_dist'      => $bundle_check->loc_dist
                                    ];
                                    return response()->json($response,200);
                                } else {
                                    if($bundle_check->locator_to == $locator_id){

                                        $check_record = DistribusiMovement::where([
                                            'barcode_id'   => $barcode_id,
                                            'locator_from' => $out_cutting_check->locator_to,
                                            'status_from'  => $out_cutting_check->status_to,
                                            'locator_to'   => $locator_id,
                                            'status_to'    => $state,
                                            'description'  => $state.' '.$locator->locator_name,
                                            'deleted_at'   => null,
                                            'loc_dist'     => $bundle_check->loc_dist
                                        ])->exists();

                                        if(!$check_record){
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'ip_address'   => $this->getIPClient(),
                                                'description'  => $state.' '.$locator->locator_name,
                                                'deleted_at'   => null,
                                                'loc_dist'     => $bundle_check->loc_dist
                                            ]);

                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $bundle_header->style,
                                                'article'       => $bundle_header->article,
                                                'po_buyer'      => $bundle_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $bundle_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $bundle_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_check->loc_dist
                                            ];

                                            return response()->json($response,200);
                                        }
                                        else{
                                            return response()->json('Barcode sudah pernah scan!',422);
                                        }

                                    }
                                    else{
                                        return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                    }

                                }
                            } else {
                                $get_process_locator = DB::table('master_process')->select('locator_id')->whereIn('id', $array_process)->whereNull('deleted_at')->groupBy('locator_id')->pluck('locator_id')->toArray();
                                $array_locator =  DB::table('master_locator')->whereIn('id', $get_process_locator)->where('deleted_at', null)->pluck('id')->toArray();
                                $banyak_process = count($get_process_locator) + 1;

                                array_push($array_locator,10);

                                $process_check = DistribusiMovement::where('barcode_id', $barcode_id)->WhereIn('locator_to', $array_locator)->where('status_to', 'out')->where('deleted_at', null);

                                $count_process_check = $process_check->count();

                                if($count_process_check == $banyak_process){

                                    $barcode_bundle_search = DB::table('bundle_detail')->where([
                                        ['bundle_header_id', $barcode_check->bundle_header_id],
                                        ['start_no',$barcode_check->start_no],
                                        ['end_no',$barcode_check->end_no]
                                    ]);

                                    $barcode_array = $barcode_bundle_search->pluck('barcode_id')->toArray();

                                    $barcode_hasil = $barcode_bundle_search->first();

                                    $banyak_komponen = $barcode_bundle_search->count();

                                    $komponen_detail = DB::table('master_style_detail')->where('id', $barcode_check->style_detail_id)->where('deleted_at', null)->first();

                                    if($bundle_check->locator_to == $locator_id){

                                        $check_record = DistribusiMovement::where([
                                            'barcode_id'   => $barcode_id,
                                            'locator_from' => $out_cutting_check->locator_to,
                                            'status_from'  => $out_cutting_check->status_to,
                                            'locator_to'   => $locator_id,
                                            'status_to'    => $state,
                                            'description'  => $state.' '.$locator->locator_name,
                                            'deleted_at'   => null,
                                            'loc_dist'     => $bundle_check->loc_dist
                                        ])->exists();

                                        if(!$check_record){
                                            DistribusiMovement::FirstOrCreate([
                                                'barcode_id'   => $barcode_id,
                                                'locator_from' => $out_cutting_check->locator_to,
                                                'status_from'  => $out_cutting_check->status_to,
                                                'locator_to'   => $locator_id,
                                                'status_to'    => $state,
                                                'user_id'      => \Auth::user()->id,
                                                'description'  => $state.' '.$locator->locator_name,
                                                'ip_address'   => $this->getIPClient(),
                                                'deleted_at'   => null,
                                                'loc_dist'     => $bundle_check->loc_dist
                                            ]);

                                            $response = [
                                                'barcode_id'    => $barcode_id,
                                                'style'         => $bundle_header->style,
                                                'article'       => $bundle_header->article,
                                                'po_buyer'      => $bundle_header->poreference,
                                                'part'          => $komponen_detail->part,
                                                'komponen_name' => $komponen_name,
                                                'size'          => $bundle_header->size,
                                                'sticker_no'    => $barcode_check->start_no.' - '.$barcode_check->end_no,
                                                'qty'           => $barcode_check->qty,
                                                'cut'           => $bundle_header->cut_num,
                                                'process'       => $locator->locator_name,
                                                'status'        => $state,
                                                'loc_dist'      => $bundle_check->loc_dist
                                            ];

                                            return response()->json($response,200);
                                        }
                                        else{
                                            return response()->json('Barcode sudah pernah scan!',422);
                                        }

                                    }
                                    else{
                                        return response()->json('komponen sudah discan di locator '.$locator->locator_name.' !',422);
                                    }
                                }
                                else{

                                    $get_process_check = $process_check->pluck('locator_to')->toArray();

                                    $array_belum_scan = array_diff($array_locator,$get_process_check);

                                    $locator_belum_scan =  DB::table('master_locator')->whereIn('id', $array_belum_scan)->where('deleted_at', null)->get();

                                    $info_belum_scan = '';
                                    foreach ($locator_belum_scan as $key => $value) {
                                        $info_belum_scan .= ''.$value->locator_name.', ';
                                    }

                                    $info = trim($info_belum_scan,", ");

                                    return response()->json('Barcode belum discan proses '.$info.'!',422);
                                }
                            }
                        }

                        $distribu = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
                    }
                    else{
                        return response()->json('Barcode belum out cutting!',422);
                    }
                } else {
                    return response()->json('Barcode tidak dikenali!',422);
                }
            }
        }
    }


    public function deleteScanProcSet(Request $request)
    {
        if(request()->ajax())
        {
            // $barcode_id = $request->barcode_input;
            // if($barcode_id != null) {
            //     $data_cdms = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
            //     $this->sds_connection
            //         ->table('temp_scan_proc')
            //         ->where('ap_id',$data_cdms->sds_barcode)
            //         ->where('proc','SET')
            //         ->where('check_out',false)
            //         ->delete();
                return response()->json(200);
            // } else {
            //     return response()->json('terjadi kesalahan saat process data!',422);
            // }
        }
    }

    //QUERY AJAX SELECT OPTION PILIH MEJA BUNDLE
    public function ajaxGetDataBundletableJson(Request $request)
    {
        if ($request->has('q')) {
            $term = trim(strtoupper($request->q));
            if (empty($term)) {
                return \Response::json([]);
            }
            $tags = BundleTable::where('factory_id',\Auth::user()->factory_id)
            ->Where('id_table', 'like', '%'.$term.'%')
            ->wherenull('deleted_at')
            ->get();
    
            return response()->json($tags);
        }
    }

    private function convertLocator($locator, $state)
    {
        // id base database master_locator
        switch ($locator) {
            case 10:
                //cutting
                $art_desc = 'bd';
                $locator_sds = 'BD';
                $column_update = 'distrib_received';
                $column_update_pic = 'bd_pic';
                break;
            case 6:
                $art_desc = 'ppa';
                $locator_sds = 'PPA';
                if($state == 'in') {
                    $column_update = 'ppa';
                    $column_update_pic = 'ppa_pic';
                } else {
                    $column_update = 'ppa_out';
                    $column_update_pic = 'ppa_out_pic';
                }
                break;
            case 2:
                $art_desc = 'he';
                $locator_sds = 'HE';
                if($state == 'in') {
                    $column_update = 'he';
                    $column_update_pic = 'he_pic';
                } else {
                    $column_update = 'he_out';
                    $column_update_pic = 'he_out_pic';
                }
                break;
            case 4:
                $art_desc = 'pad';
                $locator_sds = 'PAD';
                if($state == 'in') {
                    $column_update = 'pad';
                    $column_update_pic = 'pad_pic';
                } else {
                    $column_update = 'pad_out';
                    $column_update_pic = 'pad_out_pic';
                }
                break;
            case 5:
                //bobok
                $art_desc = 'bobok';
                $locator_sds = 'WP';
                if($state == 'in') {
                    $column_update = 'bobok';
                    $column_update_pic = 'bobok_pic';
                } else {
                    $column_update = 'bobok_out';
                    $column_update_pic = 'bobok_out_pic';
                }
                break;
            case 1:
                $art_desc = 'fuse';
                $locator_sds = 'FUSE';
                if($state == 'in') {
                    $column_update = 'fuse';
                    $column_update_pic = 'fuse_pic';
                } else {
                    $column_update = 'fuse_out';
                    $column_update_pic = 'fuse_out_pic';
                }
                break;
            case 3:
                //artwork
                $art_desc = 'artwork';
                $locator_sds = 'ART';
                if($state == 'in') {
                    $column_update = 'artwork';
                    $column_update_pic = 'artwork_pic';
                } else {
                    $column_update = 'artwork_out';
                    $column_update_pic = 'artwork_out_pic';
                }
                break;
            case 8:
                //bonding
                $art_desc = 'bonding';
                $locator_sds = 'BONDING';
                if($state == 'in') {
                    $column_update = 'bonding';
                    $column_update_pic = 'bonding_pic';
                } else {
                    $column_update = 'bonding_out';
                    $column_update_pic = 'bonding_out_pic';
                }
                break;
            default:
                $art_desc          = null;
                $locator_sds       = null;
                $column_update     = null;
                $column_update_pic = null;
        }

        if(\Auth::user()->factory_id == '1') {
            $factory_sds = 'AOI 1';
        } else {
            $factory_sds = 'AOI 2';
        }

        $convert_locator = [
            'art_desc'          => $art_desc,
            'locator_sds'       => $locator_sds,
            'column_update'     => $column_update,
            'column_update_pic' => $column_update_pic,
            'factory_sds'       => $factory_sds,
        ];

        return $convert_locator;
    }

    public function forceDelTemp(Request $request){
        $barcode_id = $request->barcode_id;
        dd($request->all());
        if($barcode_id != null){
            $bcdms = BundleDetail::where('barcode_id',$barcode_id)->whereNull('deleted_at')->first();
            if($bcdms != null){
                $check = DB::connection('sds_live')->table('temp_scan_proc')->where('proc','SET')->where('ap_id',$bcdms->sds_barcode)->first();
            }else{
                $check = DB::connection('sds_live')->table('temp_scan_proc')->where('proc','SET')->where('ap_id',$barcode_id)->first();
            }
            if($check != null){
                try{
                    DB::beginTransaction();
                        DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$check->ap_id)->where('proc','SET')->delete();
                    DB::commit();
                    return response()->json(200);
                }catch(Exception $e){
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
        }else{
            return response()->json('Something Went Wrong!',422);
        }
    }

    // public function SetLocatorLine(Request $request){
        
    //     $locator_sets = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();

    //     $barcode_id = $request->barcode_id;

    //     return view('distribusi._view_line_set_new',compact('barcode_id','locator_sets'));
    // }

    // public function moveLocSet($id)
    // {
    //     $user_role = \Auth::user()->roles->pluck('id')->toArray();
    //     $locator = Locator::whereNull('deleted_at')->where('id',$id)->first();
    //     $locator_set = MasterSetting::whereNull('deleted_at')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
    //     // dd($user_role,$locator->role, (in_array($locator->role, $user_role)), !$locator);
    //     if($locator){
    //         if (in_array($locator->role, $user_role)){
    //             return view('distribusi.move_loc',compact('locator','locator_set'));
    //         }
    //         else{
    //             return abort(405);
    //         }

    //     }
    //     else{
    //         return abort(405);
    //     }
    // }

    // public function moveLocatorLine(Request $request){
        
    //     $locator_sets = MasterSetting::whereNull('deleted_at')->where('area_name','like','LINE%')->where('factory_id', \Auth::user()->factory_id)->orderBy('order_name', 'asc')->orderBy('area_name', 'asc')->get();
    //     $locator_set = MasterSetting::whereNull('deleted_at')
    //     ->where('area_name','like','LINE%')
    //     ->where('factory_id', Auth::user()->factory_id)
    //     ->orderBy('display_order', 'asc')
    //     ->orderBy('order_name', 'asc')
    //     ->get();

    //     $barcode_id = $request->barcode_id;
    //     if(Auth::user()->factory_id == 2){
    //         return view('distribusi.move_locator_set',compact('barcode_id','locator_sets'));
    //     }else{
    //         return view('distribusi.move_locator_set2',compact('barcode_id','locator_set'));
    //     }
    // }

    // public function scanMoveBundle(Request $request){
    //     if(request()->ajax())
    //     {
    //         $barcode_id = $request->barcode_id;
    //         $locator_id = $request->locator_id;
    //         $state = $request->state;
    //         $condition = $request->condition;
    //         if($condition == '1'){
    //             if($barcode_id != null && $locator_id != null && $state != null){
    //                 $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
    //                 $barcode_cdms       = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
    //                 $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
    //                 $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
    //                 if($barcode_cdms != null){
    //                     $is_setting         = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                     $is_supply          = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
    //                     if($is_supply != null){
    //                         return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                     }elseif($is_setting == null){
    //                         return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
    //                     }else{
    //                         $obj = new StdClass();
    //                         $obj->barcode_id = $barcode_id;
    //                         $obj->condition = $condition;
    //                         return response()->json($obj, 200);
    //                     }
    //                 }elseif($barcode_cdms == null && $barcode_sds != null){
    //                     $is_setting         = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                     $is_supply          = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
    //                     if($is_supply != null){
    //                         return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                     }elseif($is_setting == null){
    //                         return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
    //                     }else{
    //                         $obj = new StdClass();
    //                         $obj->barcode_id = $barcode_id;
    //                         $obj->condition = $condition;
    //                         return response()->json($obj, 200);
    //                     }
    //                 }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
    //                     $is_setting         = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                     $is_supply          = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','16')->where('status_to','out')->first();
    //                     if($is_supply != null){
    //                         return response()->json('Barcode Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                     }elseif($is_setting == null){
    //                         return response()->json('Harap Pindah Dengan Module Scan Setting Yang Ada!',422);
    //                     }else{
    //                         $obj = new StdClass();
    //                         $obj->barcode_id = $barcode_id;
    //                         $obj->condition = $condition;
    //                         return response()->json($obj, 200);
    //                     }
    //                 }elseif($barcode_sds != null && $barcode_sds_only != null){
    //                     return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
    //                 }else{
    //                     return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //                 }
    //             }else{
    //                 return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //             }
    //         }else{
    //             if($barcode_id != null && $locator_id != null && $state != null){
    //                 $locator            = DB::table('master_locator')->where('id', $locator_id)->first();
    //                 $barcode_cdms       = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
    //                 $barcode_sds        = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
    //                 $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
    //                 if($barcode_cdms != null){
    //                     $data_detail      = DB::table('bundle_detail')->where('bundle_header_id',$barcode_cdms->bundle_header_id);
    //                     $data_barcode       = $data_detail->get();
    //                     foreach($data_barcode as $db){
    //                         $is_setting         = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                         $is_supply          = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
    //                         if($is_supply != null){
    //                             return response()->json('Barcode '.$db->barcode_id.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                         }elseif($is_setting == null){
    //                             return response()->json('Scan Barcode '.$db->barcode_id.' dengan Module Yang Sudah Ada!',422);
    //                         }else{
    //                             $obj = new StdClass();
    //                             $obj->barcode_id = $barcode_id;
    //                             $obj->condition = $condition;
    //                             return response()->json($obj, 200);
    //                         }
    //                     }
    //                 }elseif($barcode_cdms == null && $barcode_sds != null){
    //                     $data_detail      = DB::table('bundle_detail')->where('bundle_header_id',$barcode_sds->bundle_header_id);
    //                     $data_barcode       = $data_detail->get();
    //                     foreach($data_barcode as $db){
    //                         $is_setting         = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                         $is_supply          = DistribusiMovement::where('barcode_id',$db->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
    //                         if($is_supply != null){
    //                             return response()->json('Barcode '.$db->barcode_id.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                         }elseif($is_setting == null){
    //                             return response()->json('Scan Barcode '.$db->barcode_id.' dengan Module Yang Sudah Ada!',422);
    //                         }else{
    //                             $obj = new StdClass();
    //                             $obj->barcode_id = $barcode_id;
    //                             $obj->condition = $condition;
    //                             return response()->json($obj, 200);
    //                         }
    //                     }
    //                 }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
    //                     $data_detail        = DB::table('sds_detail_movements')->where('bundle_header_id',$barcode_sds_only->sds_header_id);
    //                     $data_barcode       = $data_detail->get();
    //                     foreach($data_barcode as $db){
    //                         $is_setting         = DistribusiMovement::where('barcode_id',$db->sds_barcode)->where('locator_to','11')->where('status_to','in')->first();
    //                         $is_supply          = DistribusiMovement::where('barcode_id',$db->sds_barcode)->where('locator_to','16')->where('status_to','out')->first();
    //                         if($is_supply != null){
    //                             return response()->json('Barcode '.$db->sds_barcode.' Sudah di Supply ke Line!'.$is_supply->loc_dist,422);
    //                         }elseif($is_setting == null){
    //                             return response()->json('Scan Barcode '.$db->sds_barcode.' dengan Module Yang Sudah Ada!',422);
    //                         }else{
    //                             $obj = new StdClass();
    //                             $obj->barcode_id = $barcode_id;
    //                             $obj->condition = $condition;
    //                             return response()->json($obj, 200);
    //                         }
    //                     }
    //                 }elseif($barcode_sds != null && $barcode_sds_only != null){
    //                     return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
    //                 }else{
    //                     return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //                 }
    //             }else{
    //                 return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //             }
    //         }
    //     }
    // }

    // public function selectLocatorMove(Request $request)
    // {
    //     $barcode_id = $request->barcode_id;
    //     $locator_id = 11;
    //     $state = 'in';
    //     $area_name = $request->area_name;
    //     $condition = $request->condition;
    //     //SCAN WITH SELECT AREA SETTING USING THIS CODE NOT scanComponent function
    //     if($condition == '1'){
    //         if($barcode_id != null && $locator_id != null && $area_name != null) {
    //             $user = \Auth::user()->nik;
    //             $barcode_cdms   = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
    //             $barcode_sds    = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
    //             $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
    
    //             if($barcode_cdms != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
                    
    //                 $barcode_header     = DB::table('bundle_header')->where('id', $barcode_cdms->bundle_header_id)->first();
    //                 $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_cdms->style_detail_id)->whereNull('deleted_at')->first();
    //                 $area_name = substr($area_name, 4);
    //                 $area_check = substr($area_name, 0, 1);
    //                 if($barcode_header->factory_id == 2){
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name = $request->area_name;
    //                     $line_num = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->where('barcode_id',$barcode_cdms->barcode_id)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->where('barcode_id',$barcode_cdms->sds_barcode)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 $response = [
    //                     'barcode_id'    => $barcode_id,
    //                     'style'         => $barcode_header->style,
    //                     'article'       => $barcode_header->article,
    //                     'po_buyer'      => $barcode_header->poreference,
    //                     'part'          => $komponen_detail->part,
    //                     'komponen_name' => $barcode_cdms->komponen_name,
    //                     'size'          => $barcode_header->size,
    //                     'sticker_no'    => $barcode_cdms->start_no.' - '.$barcode_cdms->end_no,
    //                     'qty'           => $barcode_cdms->qty,
    //                     'cut'           => $barcode_header->cut_num,
    //                     'from'          => $line_num,
    //                     'to'            => $request->area_name
    //                 ];
    //                 return response()->json($response,200);
    //             }elseif($barcode_cdms == null && $barcode_sds != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
    //                 $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
    //                 $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds->style_detail_id)->whereNull('deleted_at')->first();
    //                 $area_name = substr($area_name, 4);
    //                 $area_check = substr($area_name, 0, 1);
    //                 if($barcode_header->factory_id ==2){
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name  = $request->area_name;
    //                     $line_num   = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->where('barcode_id',$barcode_sds->barcode_id)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->where('barcode_id',$barcode_sds->sds_barcode)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                     $response = [
    //                         'barcode_id'    => $barcode_sds->barcode_id,
    //                         'style'         => $barcode_header->style,
    //                         'article'       => $barcode_header->article,
    //                         'po_buyer'      => $barcode_header->poreference,
    //                         'part'          => $komponen_detail->part,
    //                         'komponen_name' => $barcode_sds->komponen_name,
    //                         'size'          => $barcode_header->size,
    //                         'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
    //                         'qty'           => $barcode_sds->qty,
    //                         'cut'           => $barcode_header->cut_num,
    //                         'from'          => $line_num,
    //                         'to'            => $request->area_name
    //                     ];
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 return response()->json($response,200);
    //             }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
                    
    //                 $barcode_header     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
    //                 $komponen_detail    = DB::table('master_style_detail')->where('id', $barcode_sds_only->style_id)->whereNull('deleted_at')->first();
    //                 if($barcode_header->factory_id == '2'){
    //                     $area_name = substr($area_name, 4);
    //                     $area_check = substr($area_name, 0, 1);
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name  = $request->area_name;
    //                     $line_num   = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::where('barcode_id',$barcode_id)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->where('barcode_id',$barcode_id)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->where('barcode_id',$barcode_id)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                     $response = [
    //                         'barcode_id'    => $barcode_id,
    //                         'style'         => $barcode_header->style,
    //                         'article'       => $barcode_header->article,
    //                         'po_buyer'      => $barcode_header->poreference,
    //                         'part'          => $komponen_detail->part,
    //                         'komponen_name' => $barcode_sds_only->komponen_name,
    //                         'size'          => $barcode_header->size,
    //                         'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
    //                         'qty'           => $barcode_sds_only->qty,
    //                         'cut'           => $barcode_header->cut_num,
    //                         'from'          => $line_num,
    //                         'to'            => $request->area_name
    //                     ];
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 return response()->json($response,200);
    //             }elseif($barcode_sds != null && $barcode_sds_only != null){
    //                 return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
    //             }else{
    //                 return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //             }
    //         }else{
    //             return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //         }
    //     }elseif($condition == '2'){
    //         if($barcode_id != null && $locator_id != null && $area_name != null) {
    //             $user = \Auth::user()->nik;
    //             $barcode_cdms   = DB::table('bundle_detail')->where('barcode_id', $barcode_id)->first();
    //             $barcode_sds    = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
    //             $barcode_sds_only   = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
    //             if($barcode_cdms != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_cdms->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
    //                 $barcode_header     = DB::table('bundle_header')->where('id', $barcode_cdms->bundle_header_id)->first();
    //                 $data_detail        = DB::table('bundle_detail')->where('bundle_header_id',$barcode_header->id);
    //                 $barcode_group      = $data_detail->pluck('barcode_id')->toArray();
    //                 $disp_barcode       = implode(',',$barcode_group);
    //                 $barcode_group_sds  = $data_detail->pluck('sds_barcode')->toArray();
    //                 $barcode_groups     = $data_detail->get();
    //                 $style_detail_group = $data_detail->pluck('style_detail_id')->toArray();
    //                 $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
    //                 $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
    //                 $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
    //                 if($barcode_header->factory_id == 2){
    //                     $area_name = substr($area_name, 4);
    //                     $area_check = substr($area_name, 0, 1);
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name = $request->area_name;
    //                     $line_num = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->whereIn('barcode_id',$barcode_group)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->whereIn('barcode_id',$barcode_group_sds)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 $response = [
    //                     'barcode_id'    => $disp_barcode,
    //                     'style'         => $barcode_header->style,
    //                     'article'       => $barcode_header->article,
    //                     'po_buyer'      => $barcode_header->poreference,
    //                     'part'          => $disp_part,
    //                     'komponen_name' => $disp_komponen,
    //                     'size'          => $barcode_header->size,
    //                     'sticker_no'    => $barcode_cdms->start_no.' - '.$barcode_cdms->end_no,
    //                     'qty'           => $barcode_cdms->qty,
    //                     'cut'           => $barcode_header->cut_num,
    //                     'from'          => $line_num,
    //                     'to'            => $request->area_name
    //                 ];
    //                 return response()->json($response,200);
    //             }elseif($barcode_cdms == null && $barcode_sds != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds->barcode_id)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
    //                 $barcode_header     = DB::table('bundle_header')->where('id', $barcode_sds->bundle_header_id)->first();
    //                 $data_detail        = DB::table('bundle_detail')->where('bundle_header_id',$barcode_header->id);
    //                 $barcode_group      = $data_detail->pluck('barcode_id')->toArray();
    //                 $disp_barcode       = implode(',',$barcode_group);
    //                 $barcode_group_sds  = $data_detail->pluck('sds_barcode')->toArray();
    //                 $barcode_groups     = $data_detail->get();
    //                 $style_detail_group = $data_detail->pluck('style_detail_id')->toArray();
    //                 $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
    //                 $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
    //                 $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
    //                 if($barcode_header->factory_id ==2){
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name  = $request->area_name;
    //                     $line_num   = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->where('barcode_id',$barcode_group)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->where('barcode_id',$barcode_group_sds)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                     $response = [
    //                         'barcode_id'    => $disp_barcode,
    //                         'style'         => $barcode_header->style,
    //                         'article'       => $barcode_header->article,
    //                         'po_buyer'      => $barcode_header->poreference,
    //                         'part'          => $disp_part,
    //                         'komponen_name' => $disp_komponen,
    //                         'size'          => $barcode_header->size,
    //                         'sticker_no'    => $barcode_sds->start_no.' - '.$barcode_sds->end_no,
    //                         'qty'           => $barcode_sds->qty,
    //                         'cut'           => $barcode_header->cut_num,
    //                         'from'          => $line_num,
    //                         'to'            => $request->area_name
    //                     ];
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 return response()->json($response,200);
    //             }elseif($barcode_cdms == null && $barcode_sds == null && $barcode_sds_only != null){
    //                 $last_loc           = DistribusiMovement::where('barcode_id',$barcode_sds_only->sds_barcode)->where('locator_to','11')->where('status_to','in')->first();
    //                 $loc_dist           = explode('.',$last_loc->loc_dist);
    //                 $barcode_header     = DB::table('sds_header_movements')->where('id', $barcode_sds_only->sds_header_id)->first();
    //                 $data_detail        = DB::table('sds_detail_movements')->where('sds_header_id',$barcode_header->id);
    //                 $barcode_group      = $data_detail->pluck('sds_barcode')->toArray();
    //                 $disp_barcode       = implode(',',$barcode_group);
    //                 $barcode_groups     = $data_detail->get();
    //                 $style_detail_group = $data_detail->pluck('style_id')->toArray();
    //                 $komponen_detail    = DB::table('v_master_style')->whereIn('id', $style_detail_group)->whereNull('deleted_at');
    //                 $disp_part          = implode(',',$komponen_detail->distinct('part')->pluck('part')->toArray());
    //                 $disp_komponen      = implode(',',$komponen_detail->distinct('komponen_name')->pluck('komponen_name')->toArray());
    //                 if($barcode_header->factory_id == '2'){
    //                     $area_name = substr($area_name, 4);
    //                     $area_check = substr($area_name, 0, 1);
    //                     if($area_check == '0') {
    //                         $area_name = 'L.'.substr($area_name, 1);
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     } else {
    //                         $area_name = 'L.'.$area_name;
    //                         $line_num  = strlen($loc_dist[1]) > 1 ? 'LINE'.$loc_dist[1] : 'LINE0'.$loc_dist[1];
    //                     }
    //                 }else{
    //                     $area_name  = $request->area_name;
    //                     $line_num   = $last_loc->loc_dist;
    //                 }
    //                 try{
    //                     DB::beginTransaction();
    //                         DistribusiMovement::whereIn('barcode_id',$barcode_group)->where('locator_to','11')->update([
    //                             'loc_dist'     => $area_name,
    //                             'ip_address'   => $this->getIPClient(),
    //                             'user_id'      => \Auth::user()->id,
    //                             'created_at'   => Carbon::now()
    //                         ]);
                            
    //                         DB::table('bundle_detail')->whereIn('barcode_id',$barcode_group)->update([
    //                             'updated_movement_at'   => Carbon::now(),
    //                             'current_user_id'       => \Auth::user()->id
    //                         ]);
    
    //                         $this->sds_connection->table('bundle_info_new')
    //                         ->whereIn('barcode_id',$barcode_group)
    //                         ->update([
    //                             'location'      => $area_name,
    //                             'setting'       => Carbon::now(),
    //                             'setting_pic'   => $user,
    //                             'supply_unlock' => false
    //                         ]);
    //                     DB::commit();
    //                     $response = [
    //                         'barcode_id'    => $disp_barcode,
    //                         'style'         => $barcode_header->style,
    //                         'article'       => $barcode_header->article,
    //                         'po_buyer'      => $barcode_header->poreference,
    //                         'part'          => $disp_part,
    //                         'komponen_name' => $disp_komponen,
    //                         'size'          => $barcode_header->size,
    //                         'sticker_no'    => $barcode_sds_only->start_no.' - '.$barcode_sds_only->end_no,
    //                         'qty'           => $barcode_sds_only->qty,
    //                         'cut'           => $barcode_header->cut_num,
    //                         'from'          => $line_num,
    //                         'to'            => $request->area_name
    //                     ];
    //                 }catch(Exception $e){
    //                     DB::rollback();
    //                     $message = $e->getMessage();
    //                     ErrorHandler::db($message);
    //                 }
    //                 return response()->json($response,200);
    //             }elseif($barcode_sds != null && $barcode_sds_only != null){
    //                 return response()->json('Terdapat Duplikat Barcode Di Sistem',422);
    //             }else{
    //                 return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //             }
    //         }else{
    //             return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //         }
    //     }else{
    //         return response()->json('Terjadi kesalahan Harap Refresh Halaman!',422);
    //     }
    // }
}