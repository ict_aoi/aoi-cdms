<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail_queue))
                <li><a href="#" onclick="detail_queue('{!! $detail_queue !!}')"><span class="icon-list"></span> Detail Request</a></li>
            @endif
            @if (isset($delete_queue))
                <li><a href="#" onclick="delete_queue('{!! $delete_queue !!}')"><span class="icon-trash"></span> Delete Request</a></li>
            @endif
        </ul>
    </li>
</ul>

{{-- <a href="#" onclick="submitRequestFabric('{!! $submit_request !!}')" class="btn btn-success btn-sm"><span class="icon-check"></span></a> --}}
