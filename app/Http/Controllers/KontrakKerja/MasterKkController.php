<?php

namespace App\Http\Controllers\KontrakKerja;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Excel;
use Carbon\Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use DataTables;
use App\Models\Factory;
use App\Models\MasterKontrak;
use App\Models\DetailKontrak;

class MasterKkController extends Controller
{
    public function masterKontrak(){
    	$subcont = DB::table('master_subcont')->whereNull('deleted_at')->get();
    	$polist = DB::table('cutting_plans2')->join('detail_cutting_plan','detail_cutting_plan.id_plan','=','cutting_plans2.id')->whereNull('cutting_plans2.deleted_at')->whereNull('detail_cutting_plan.deleted_at')->where('factory_id',Auth::user()->factory_id)->orderBy('cutting_date','desc')->get();
    	return view('kontrakkerja/master_kk')->with('subcont',$subcont)->with('polist',$polist);
    }

    public function create_kk(Request $request){
    	$nomorKK = $request->no_kk;
    	$subcont_id = $request->subcont_id;
    	$total = $request->total;
    	$dataset = $request->dataset;
    	
    	try {
    		DB::begintransaction();
    		$head = array(
    			'no_kk'=>$nomorKK,
    			'subcont_id'=>$subcont_id,
    			'factory_id'=>Auth::user()->factory_id,
    			'user_id'=>Auth::user()->id,
    			'created_at'=>Carbon::now(),
    			'total'=>$total
    		);

    		MasterKontrak::FirstOrCreate($head);

    		$getid = MasterKontrak::where('no_kk',$nomorKK)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

    		$headid = $getid->id;

    		foreach ($dataset as $dt) {
    			$detail = array(
    				'kk_id'=>$headid,
    				'style'=>$dt['style'],
    				'po_number'=>$dt['po_buyer'],
    				'created_at'=>Carbon::now()
    			);

    			DetailKontrak::FirstOrCreate($detail);
    		}

    		

    		DB::commit();

    		$data_response = [
                            'status' => 200,
                            'output' => 'Create Kontrak Kerja Success'
                          ];
    	} catch (Exception $ex) {
    		db::rollback();
			$message = $ex->getMessage();
			\ErrorHandler($message);

			$data_response = [
				                'status' => 422,
				                'output' => 'Create Kontrak Kerja failed !!!'
				              ];
    	}

    	return response()->json(['data' => $data_response]);
    }


    public function LaporanKK(){
    	return view('kontrakkerja/report_kk');
    }

    public function getDataKK(Request $request){
    	$no_kk = $request->no_kk;

    	$data = DB::table('master_kk')->join('detail_kk','detail_kk.kk_id','=','master_kk.id')
    						->where('master_kk.no_kk',$no_kk)
    						->where('master_kk.factory_id',Auth::user()->factory_id)
    						->whereNull('master_kk.deleted_at')
    						->whereNull('detail_kk.deleted_at');
    	return DataTables::of($data)
    						->make(true);
    }

    public function getHeadKK(Request $request){
    	$no_kk = $request->no_kk;

    	$head = MasterKontrak::where('no_kk',$no_kk)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();

    	if ($head!=null) {
    		$subcont = DB::table('master_subcont')->where('id',$head->subcont_id)->first();
    		$user = DB::table('users')->where('id',$head->user_id)->first();

    		$header = array(
    			'nomorKK'=>$no_kk,
    			'subcont'=>$subcont->subcont_name,
    			'user'=>$user->name,
    			'total'=>$head->total,
    			'status'=>200
    		);

    		return response()->json(['data' => $header],200);
    	}else{
    		$data_response = [
				                'status' => 422,
				                'output' => 'Data not find !!!'
				              ];
			return response()->json(['data' => $data_response]);
    	}
    }

    public function exportLaporanKK(Request $request){
        $nokk = $request->no_kk;

        $filename = 'Laporan_Kontrak_Kerja_'.$nokk.'_AOI'.Auth::user()->factory_id;

        $head = MasterKontrak::where('no_kk',$nokk)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->first();
        $detail = DetailKontrak::where('kk_id',$head->id)->whereNull('deleted_at')->get();
        $subcont = DB::table('master_subcont')->where('id',$head->subcont_id)->first();
        $user = DB::table('users')->where('id',$head->user_id)->first();

        $nomorkk = $head->no_kk;
        $subcontname = $subcont->subcont_name;
        $userby = $user->name;

        return Excel::create($filename,function($excel) use($detail,$nomorkk,$subcontname,$userby){
                $excel->sheet('active',function($sheet) use ($detail,$nomorkk,$subcontname,$userby){
                    $sheet->setCellValue('B2','Nomor Kontrak Kerja');
                    $sheet->setCellValue('C2',$nomorkk);
                    $sheet->setCellValue('B3','Subcont Name');
                    $sheet->setCellValue('C3',$subcontname);
                    $sheet->setCellValue('B4','Create By');
                    $sheet->setCellValue('C4',$userby);

                    $sheet->setCellValue('A6','#');
                    $sheet->setCellValue('B6','PO Buyer');
                    $sheet->setCellValue('C6','Style');

                    $i = 1;

                    foreach ($detail as $dt) {
                        $rw = $i + 6;

                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,$dt->po_number);
                        $sheet->setCellValue('C'.$rw,$dt->style);

                        $i++;
                    }

                });

                $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function packingListKK(){
        $dt_kk = MasterKontrak::where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->orderBy('created_at','desc')->get();
        $dt_process = DB::table('master_locator')->join('master_process','master_locator.id','=','master_process.locator_id')->whereNull('master_process.deleted_at')->whereNull('master_locator.deleted_at')->where('master_locator.is_artwork',true)->select('master_process.id','master_process.process_name')->get();
        return view('kontrakkerja.packinglist_kk')->with('nokk',$dt_kk)->with('proses',$dt_process);
    }

    public function generatePackingNumber(Request $request){
        $factory = Auth::user()->factory_id;
        $initial = $request->initial;

        $gmax = DB::table('packinglist_subcont')->max('no_packinglist');
        $num = (int) substr($gmax,-6) +1;

        $newcode = $initial.$factory.sprintf("%06s", $num);

        return response()->json(['nopl'=>$newcode],200);
        
    }

    public function ajaxGetStyle(Request $request){
        $idkk = $request->idkk;
        $headkk = DB::table('master_kk')
                        ->join('master_subcont','master_kk.subcont_id','=','master_subcont.id')
                        ->whereNull('master_kk.deleted_at')
                        ->where('master_kk.id',$idkk)
                        ->select('master_kk.no_kk','master_subcont.subcont_name','master_subcont.initials')
                        ->first();
      
        $style = DetailKontrak::whereNull('deleted_at')->where('kk_id',$idkk)->groupBy('style')->select('style')->get();

        return response()->json(['style'=>$style,'kontrak'=>$headkk],200);
    }
 
    public function ajaxGetPo(Request $request){
        $id_kk = $request->idkk;
        $style = $request->style;

        $po_buyer = DetailKontrak::whereNull('deleted_at')->where('kk_id',$id_kk)->where('style',$style)->get();

        return response()->json(['po_buyer'=>$po_buyer],200);
    }

    public function createPackinglistKK(Request $request){
        $po_buyer = $request->dt_po;
        $process = $request->dt_process;
        $idkk = $request->idkk;
        $nokk = $request->nokk;
        $style = $request->style;
        $subcont_name = $request->subcont_name;
        $nopl = $request->nopl;
        $str_po = array();
        $str_pros = array();
        $time = Carbon::now();
        
        foreach ($po_buyer as $po) {
            $str_po[]= $po;
        }
       

        foreach ($process as $pros) {
            $str_pros[] = $pros;
        }

        try {
            db::begintransaction();
                $dt_in = array(
                    'no_packinglist'=>$nopl ,
                    'kk_id'=>$idkk ,
                    'no_kk'=>$nokk ,
                    'style'=>$style ,
                    'po_buyer'=>implode(", ", $str_po),
                    'process'=>implode(", ", $str_pros),
                    'subcont'=>$subcont_name ,
                    'user_id'=>Auth::user()->id ,
                    'factory_id'=>Auth::user()->factory_id ,
                    'created_at'=>$time ,
                    'updated_at'=>$time,
                    'status' => 'draft'
                );

                DB::table('packinglist_subcont')->insert($dt_in);
            db::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Create Packinglist Success'
                          ];
        } catch (Exception $ex) {
            db::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Create Packinglist failed !!!'
                              ];
        }

        return response()->json(['data'=>$data_response]);
        
    }

    public function packinglistGetData(Request $request)
    {
        if(request()->ajax())
        {
            $data = DB::table('packinglist_subcont')->where('factory_id', \Auth::user()->factory_id)->orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('kontrakkerja._packinglist_action', [
                    'model' => $data,
                    'print' => route('MasterKK.packinglistPrint', $data->no_packinglist),
                ]);
                return 'action';
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function print($id)
    {
        $filename ='packinglist_'.$id;
        $packinglist = DB::table('packinglist_subcont')->where('no_packinglist', $id)->first();
        $kk = DB::table('master_kk')->where('id', $packinglist->kk_id)->first();
        $subcont = DB::table('master_factory')->where('id', $kk->subcont_id)->first();
        $factory = DB::table('master_factory')->where('id', $packinglist->factory_id)->first();

        $size = DB::table('detail_packinglist_subcont')->select('bundle_header.size')->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('bundle_header.size')->orderByRaw('LENGTH(bundle_header.size) asc')->orderBy('bundle_header.size', 'asc')->pluck('bundle_header.size')->toArray();

        $data_details = DB::table('detail_packinglist_subcont')->select(DB::raw('detail_packinglist_subcont.id_packinglist, bundle_header.style, max(detail_packinglist_subcont.created_at) as created_at, bundle_header.poreference, bundle_header.article, bundle_detail.komponen_name, detail_packinglist_subcont.no_polybag'))->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('bundle_header.style', 'bundle_header.poreference', 'bundle_header.article', 'bundle_detail.komponen_name', 'detail_packinglist_subcont.id_packinglist', 'detail_packinglist_subcont.no_polybag')->orderBy('detail_packinglist_subcont.no_polybag', 'bundle_header.poreference')->get();

        $data_qty_array = array();
        $total_qty_all = array();
        
        foreach($data_details as $detail) {
            $data_size_details = array();
            $size_detail = DB::table('detail_packinglist_subcont')->select(DB::raw('detail_packinglist_subcont.no_polybag, bundle_header.size, sum(bundle_detail.qty) as qty'))->leftJoin('bundle_detail', 'detail_packinglist_subcont.bundle_id', '=', 'bundle_detail.barcode_id')->leftJoin('bundle_header', 'bundle_header.id', '=', 'bundle_detail.bundle_header_id')->where('detail_packinglist_subcont.no_polybag', $detail->no_polybag)->where('bundle_header.style', $detail->style)->where('bundle_header.article', $detail->article)->where('bundle_header.poreference', $detail->poreference)->where('bundle_detail.komponen_name', $detail->komponen_name)->where('detail_packinglist_subcont.id_packinglist', $id)->groupBy('detail_packinglist_subcont.no_polybag', 'bundle_header.size')->get();

            $data_size_details['data'] = $size_detail;
            $data_size_details['count'] = $size_detail->count();

            $total_temp = 0;

            foreach($size_detail as $aa) {
                $total_temp = $total_temp + $aa->qty;
            }

            $total_qty_all[] = $total_temp;

            $data_qty_array[] = $data_size_details;
        }
        
        $data_size = array();

        if(count($size) > 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } elseif(count($size) == 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } else {
            for($i = 0; $i < count($size); $i++) {
                $data_size[] = $size[$i];
            }
            $selisih_size = 6 - count($size);
            for($i = 0; $i < $selisih_size; $i++) {
                $data_size[] = 0;
            }
        }

        $paper = array(0,0,609.449,396.85);

        $data = [
            'no_packinglist' => $id,
            'kk' => $kk->no_kk,
            'kepada' => $subcont->factory_name,
            'proses'=>'Embro',
            'factory' => $factory->factory_alias,
            'sizes' => $data_size,
            'data_details' => $data_details,
            'data_size_details' => $data_qty_array,
            'total_qty_all' => $total_qty_all,
        ];
        
        $pdf = \PDF::loadView('print.packinglist', $data)->setPaper($paper,'potrait');

        return $pdf->stream($filename);
    }
}
