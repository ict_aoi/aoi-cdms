<div id="ratioModal"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="ratioTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="150px;">Barcode Marker</th>
                                <th width="50px;">Cut</th>
                                <th width="50px;">Part</th>
                                <th>Ratio</th>
                                <th>Layer</th>
                                <th>Marker Width</th>
                            </tr>
                        </thead>
                    </table>
                </div>                   
            </div>
        </div>
    </div>
</div>