<div id="lineSetModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Choose Locator Set</span>
				</legend>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="lineSetTable">
						<thead>
							<tr>
								<th>#</th>
								<th>Locator</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php
								$set_increment = 1;
							@endphp
							@foreach($locator_set as $set)
								<tr>
									<th>{{ $set_increment }}</th>
									<th>{{ $set->area_name }}</th>
									<th><button class="btn btn-primary select-locator-set" data-id="{{ $set->id }}" data-area_name="{{ $set->area_name }}">Select</button></th>
								</tr>
								@php
									$set_increment++;
								@endphp
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" id="barcode_id_set_locator" />
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
		</div>
	</div>
</div>