<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($combine))
                <li>
                    <a
                        type="button"
                        class="combine-plan-action"
                        data-pcd="{{ $model->cutting_date }}"
                        data-style="{{ $model->style }}"
                        data-article="{{ $model->articleno }}"
                        data-size="{{ $model->size_category }}"
                        data-tb="{{ $model->top_bot }}"
                        data-season="{{ $model->season }}"
                        data-factory_id="{{ $model->factory_id }}"
                    >
                        <span class="icon-list"></span> Combine
                </a>
                </li>
            @endif
        </ul>
    </li>
</ul>