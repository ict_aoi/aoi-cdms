@extends('layouts.app',['active' => 'fabric_prepare_whs'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Fabric Prepared </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Fabric Prepared</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_date">
            <div class="col-lg-10">
                <label><b>Choose Planing Date</b></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                    <input type="date" class="form-control pickadate" name="pcd" id="pcd">
                    
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                
                <form action="{{ route('preparation.exportFabricPrepared') }}" method="GET">
                    @csrf
                    <label style="color: white;"><b>Choose Planing Date</b></label>
                    <input type="text" name="mpcd" id="mpcd" class="hidden">
                    <button  type="submit" class="btn btn-success" id="btn-export" ><span class="icon-download"></span> Export Excel</button>
                </form>
                
               <!--  <a href="{{ route('preparation.exportFabricPrepared') }}" id="exportFabricPrepared" type="button">cetak</a> -->
            </div>
            
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Style</th>
                        <th>Aricle</th>
                        <th>Part</th>
                        <th>Width</th>
                        <th>Qty</th>
                        <th>PO Buyer</th>
                        <th>Item ID</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('preparation.exportFabricPrepared') }}" id="exportFabricPrepared"></a>
@endsection

@section('page-js')

<script type="text/javascript">
$(document).ready(function(){
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{route('preparation.getFabricPrepared')}}",
            data: function(d) {
                 return $.extend({}, d, {
                     "pcd"         : $('#pcd').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'style', name: 'style'},
            {data: 'article_no', name: 'article_no'},
            {data: 'part_no', name: 'part_no'},
            {data: 'actual_width', name: 'actual_width'},
            {data: 'qty', name: 'qty',sortable: false, orderable: false, searchable: false},
            {data: 'po_buyer', name: 'po_buyer',sortable: false, orderable: false, searchable: false},
            {data: 'item_id', name: 'item_id',sortable: false, orderable: false, searchable: false}
        ]
    });

    $('#btn-filter').on('click',function(){
            event.preventDefault();

            if ($('#pcd').val()=='') {
                $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
                return false;
            }else{
                $('#mpcd').val($('#pcd').val());
                 $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'GET',
                    url : "{{route('preparation.getFabricPrepared')}}",
                    data:{pcd:$('#pcd').val()},
                    beforeSend:function(){
                        loading_process();
                    },
                    success:function(response){
                        $('#table-list').unblock();

                        tableL.clear();
                        tableL.draw();
                    },
                    error:function(response){
                        myalert('error','eror');
                    }
                });
            }
    });

    
});
</script>
@endsection