<?php

namespace App\Http\Controllers\Report;

use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use App\Models\DistribusiMovement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PoSummaryController extends Controller
{
    public function index()
    {
        return view('report.po_summary.index');
    }

    public function getDataSummaryPo(Request $request){

        if ($request->ajax()){
            $poreference = $request->po_buyer;
            $factory = \Auth::user()->factory_id;
            if($poreference != null){
                $data = DB::SELECT(DB::RAW("SELECT xz.poreference,xz.style,xz.season,xz.article,xz.size,
                MAX(xz.ordered_qty) AS ordered_qty,
                SUM(xz.qty_bundle) AS qty_bundle,
                SUM(xz.qty_cut) AS qty_cut,
                SUM(xz.qty_fuse) AS qty_fuse,
                SUM(xz.qty_ppa1) AS qty_ppa1,
                SUM(xz.qty_ppa2) AS qty_ppa2,
                SUM(xz.qty_pad) AS qty_pad,
                SUM(xz.qty_he) AS qty_he,
                SUM(xz.qty_auto) AS qty_auto,
                SUM(xz.qty_artwork) AS qty_artwork,
                CASE WHEN SUM(xz.qty_cut) < SUM(xz.qty_setting) THEN SUM(xz.qty_cut) ELSE SUM(xz.qty_setting) END AS qty_setting,
                SUM(xz.qty_supply) AS qty_supply
                FROM(SELECT z.poreference,z.style,z.season,z.article,z.size,dor.ordered_qty,
                SUM(z.qty_bundle) AS qty_bundle,
                CASE WHEN z.act_set_garment = z.c_set_garment THEN SUM(z.qty_cut) ELSE 0 END AS qty_cut,
                SUM(z.qty_fuse) AS qty_fuse,
                SUM(z.qty_ppa1) AS qty_ppa1,
                SUM(z.qty_ppa2) AS qty_ppa2,
                SUM(z.qty_pad) AS qty_pad,
                SUM(z.qty_he) AS qty_he,
                SUM(z.qty_auto) AS qty_auto,
                SUM(z.qty_artwork) AS qty_artwork,
                SUM(z.qty_setting) AS qty_setting,
                SUM(z.qty_supply) AS qty_supply
                FROM(SELECT 
                y.poreference,
                y.season,
                y.style,
                y.article,y.size,y.cut_num,y.start_no,y.end_no,
                COUNT(y.part_z) AS act_set_garment,
                set_g.c_set_garment,
                MAX(y.qty_bundle) AS qty_bundle,
                CASE WHEN MIN(y.qty_cut) = 0 THEN 0 ELSE max(y.qty_cut) END AS qty_cut,
                MAX(y.qty_fuse) as qty_fuse,
                MAX(y.qty_ppa1) as qty_ppa1,
                MAX(y.qty_ppa2) as qty_ppa2,
                MAX(y.qty_pad) as qty_pad,
                MAX(y.qty_he) qty_he,
                MAX(y.qty_auto) as qty_auto,
                MAX(y.qty_artwork) as qty_artwork,
                MAX(y.qty_setting) as qty_setting,
                MAX(y.qty_supply) as qty_supply
                FROM(
                SELECT x.barcode_id,
                x.poreference,
                x.season,
                case when x.type_name = 'BOTTOM' then concat(x.style,'-BOT') WHEN x.type_name = 'TOP' THEN concat(x.style,'-TOP') else x.style end as style,
                x.article,x.size,x.komponen_name,x.process,x.process_name,x.cut_num,x.start_no,x.end_no,
                CASE WHEN vms.style is not null THEN 1 ELSE null END AS part_z,
                MAX(x.qty) AS qty_bundle,
                MAX(x.qty_cut) AS qty_cut,
                CASE WHEN MAX(x.qty_fuse) IS NULL and x.process_name like '%FUSE%' THEN 0 ELSE MAX(x.qty_fuse)END AS qty_fuse,
                CASE WHEN MAX(x.qty_ppa1) IS NULL and x.process_name like '%PPA%' THEN 0 ELSE MAX(x.qty_ppa1)END AS qty_ppa1,
                CASE WHEN MAX(x.qty_ppa2) IS NULL AND x.process_name like '%PPA2%' THEN 0 ELSE MAX(x.qty_ppa2) END AS qty_ppa2,
                CASE WHEN MAX(x.qty_pad) IS NULL AND x.process_name like '%PAD%' THEN 0 ELSE MAX(x.qty_pad) END AS qty_pad,
                CASE WHEN MAX(x.qty_he) IS NULL and x.process_name like '%HE%' THEN 0 ELSE MAX(x.qty_he)END AS qty_he,
                CASE WHEN x.process_name LIKE '%TEMPLATE%' or x.process_name LIKE '%AUTO%' or x.process_name LIKE '%BOBOK%' or x.process_name LIKE '%AMS%' AND MAX(x.qty_auto) IS NOT NULL THEN MAX(x.qty_auto) ELSE null END AS qty_auto,
                CASE WHEN x.process_name LIKE '%EMBRO%' or x.process_name LIKE '%PRINT%' and MAX(x.qty_artwork) IS NOT NULL THEN MAX(x.qty_artwork) ELSE null END AS qty_artwork,
                CASE WHEN MAX(x.qty_setting) IS NULL THEN 0 ELSE MAX(x.qty_setting)END AS qty_setting,
                CASE WHEN MAX(x.qty_supply) IS NULL THEN 0 ELSE MAX(x.qty_supply) END AS qty_supply
                FROM(SELECT bd.barcode_id,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,bd.komponen_name,vms.process,vms.process_name,bh.cut_num,bd.start_no,bd.end_no,bd.qty,
                COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN bd.qty END,0) AS qty_cut,
                CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN bd.qty END AS qty_fuse,
                CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN bd.qty END AS qty_ppa1,
                CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN bd.qty END AS qty_ppa2,
                CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN bd.qty END AS qty_pad,
                CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN bd.qty END AS qty_he,
                CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN bd.qty END AS qty_auto,
                CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN bd.qty END AS qty_artwork,
                CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN bd.qty END AS qty_setting,
                CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN bd.qty END AS qty_supply
                FROM bundle_detail bd
                JOIN bundle_header bh on bh.id = bd.bundle_header_id
                JOIN v_master_style vms on vms.id = bd.style_detail_id
                LEFT JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
                WHERE bh.poreference='$poreference' and bh.factory_id='$factory'
                UNION
                SELECT sdm.sds_barcode as barcode_id,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,sdm.komponen_name,vms.process,vms.process_name,shm.cut_num,sdm.start_no,sdm.end_no,sdm.qty,
                COALESCE(CASE WHEN dm.locator_to = '10' AND dm.status_to = 'out' THEN sdm.qty END,0) AS qty_cut,
                CASE WHEN dm.locator_to = '1' AND dm.status_to = 'out' THEN sdm.qty END AS qty_fuse,
                CASE WHEN dm.locator_to='6' AND dm.status_to ='out' THEN sdm.qty END AS qty_ppa1,
                CASE WHEN dm.locator_to = '7' AND dm.status_to = 'out' THEN sdm.qty END AS qty_ppa2,
                CASE WHEN dm.locator_to = '4' AND dm.status_to = 'out' THEN sdm.qty END AS qty_pad,
                CASE WHEN dm.locator_to = '2' AND dm.status_to = 'out' THEN sdm.qty END AS qty_he,
                CASE WHEN dm.locator_to = '5' AND dm.status_to = 'out' THEN sdm.qty END AS qty_auto,
                CASE WHEN dm.locator_to = '3' AND dm.status_to = 'out' THEN sdm.qty END AS qty_artwork,
                CASE WHEN dm.locator_to = '11' AND dm.status_to = 'in' THEN sdm.qty END AS qty_setting,
                CASE WHEN dm.locator_to = '16' AND dm.status_to = 'out' THEN sdm.qty END AS qty_supply
                FROM sds_detail_movements sdm
                JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
                JOIN v_master_style vms on vms.id = sdm.style_id
                LEFT JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
                WHERE shm.poreference='$poreference' AND shm.factory_id = '$factory'
                ORDER BY size,cut_num,start_no,end_no)x
                JOIN v_master_style vms on vms.style = x.style and vms.season_name = x.season
                GROUP BY
                x.size,vms.style,x.cut_num,x.start_no,x.end_no,x.komponen_name,x.process,x.process_name,x.barcode_id,x.poreference,x.season,x.style,x.type_name,x.article)y
                LEFT JOIN (SELECT st.season_name,
                CASE WHEN st.type_name = 'TOP' THEN CONCAT(st.style,'-TOP') WHEN st.type_name = 'BOTTOM' THEN CONCAT(st.style,'-BOT') ELSE st.style END AS style,
                COUNT(st.style) as c_set_garment
                FROM(SELECT season_name,style,type_name
                FROM v_master_style 
                WHERE style=(SELECT DISTINCT
                style
                FROM bundle_header
                WHERE poreference='$poreference'
                UNION
                SELECT DISTINCT
                style
                FROM sds_header_movements
                WHERE poreference='$poreference') and season_name=(SELECT DISTINCT
                season
                FROM bundle_header
                WHERE poreference='$poreference'
                UNION
                SELECT DISTINCT
                season
                FROM sds_header_movements
                WHERE poreference='$poreference'))st
                GROUP BY
                st.season_name,st.style,st.type_name)set_g
                ON set_g.style = y.style and set_g.season_name = y.season
                GROUP BY
                y.poreference,y.season,y.style,set_g.c_set_garment,y.article,y.size,y.cut_num,y.start_no,y.end_no)z
                LEFT JOIN (SELECT DISTINCT po_buyer,style,size_finish_good,ordered_qty
                FROM
                data_cuttings
                where po_buyer='$poreference' or job_no like '%$poreference%')dor on dor.po_buyer = z.poreference AND dor.style = z.style AND dor.size_finish_good = z.size
                GROUP BY
                z.poreference,z.season,z.style,z.article,z.size,z.act_set_garment,z.c_set_garment,dor.ordered_qty)xz
                GROUP BY
                xz.poreference,xz.style,xz.season,xz.article,xz.size"));
            }else{
                $data = [];
            }
            return DataTables::of($data)
            ->editColumn('qty_cut',function($data){
                if($data->ordered_qty == $data->qty_cut){
                    return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                }elseif((int)$data->qty_cut >= 0 && $data->ordered_qty != $data->qty_cut){
                    return $data->qty_cut.'/'.$data->ordered_qty;
                }else{
                    return '';
                }
            })
            ->editColumn('qty_fuse',function($data){
                if($data->qty_fuse != null){
                    if($data->ordered_qty == $data->qty_fuse){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_fuse != null && (int)$data->qty_fuse >= 0 && $data->ordered_qty != $data->qty_fuse){
                        return $data->qty_fuse.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_ppa1',function($data){
                if($data->qty_ppa1 != null){
                    if($data->ordered_qty == $data->qty_ppa1){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_ppa1 != null && (int)$data->qty_ppa1 >= 0 && $data->ordered_qty != $data->qty_ppa1){
                        return $data->qty_ppa1.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_ppa2',function($data){
                if($data->qty_ppa2 != null){
                    if($data->ordered_qty == $data->qty_ppa2){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_ppa2 != null){
                        return $data->qty_ppa2.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_pad',function($data){
                if($data->qty_pad != null){
                    if($data->ordered_qty == $data->qty_pad){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_pad >= 0 || $data->ordered_qty != $data->qty_pad){
                        return $data->qty_pad.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_he',function($data){
                if($data->qty_he != null){
                    if($data->ordered_qty == $data->qty_he){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_he >= 0 || $data->ordered_qty != $data->qty_he){
                        return $data->qty_he.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_auto',function($data){
                if($data->qty_auto != null){
                    if($data->ordered_qty == $data->qty_auto){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_auto >= 0 && $data->qty_auto != null){
                        return $data->qty_auto.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_artwork',function($data){
                if($data->qty_artwork != null){
                    if($data->ordered_qty == $data->qty_artwork){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_artwork >= 0 || $data->ordered_qty != $data->qty_artwork){
                        return $data->qty_artwork.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('qty_setting',function($data){
                if($data->ordered_qty == $data->qty_setting){
                    return '<span class="label label-success label-rounded">
                        <i class="icon-checkmark2"></i>
                        </span>';
                }elseif($data->qty_setting >= 0){
                    return $data->qty_setting.'/'.$data->ordered_qty;
                }else{
                    return '';
                }
            })
            ->editColumn('qty_supply',function($data){
                if($data->qty_supply != null){
                    if($data->ordered_qty == $data->qty_supply){
                        return '<span class="label label-success label-rounded">
                            <i class="icon-checkmark2"></i>
                            </span>';
                    }elseif($data->qty_supply >= 0 || $data->ordered_qty != $data->qty_supply){
                        return $data->qty_supply.'/'.$data->ordered_qty;
                    }else{
                        return '';
                    }
                }else{
                    return '';
                }
            })
            ->rawColumns(['qty_cut','qty_fuse','qty_ppa1','qty_ppa2','qty_pad','qty_he','qty_auto','qty_artwork','qty_setting','qty_supply'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
}
