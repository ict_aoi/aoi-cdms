@extends('layouts.app',['active' => 'cutting'])
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Cutting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Cutting</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="col-md-2">
                <label style="padding-top: 10px;"><b>Scan ID Marker</b></label>
            </div>
            <div class="col-md-10">
                <input type="text" name="barcode_id" id="barcode_id" class="form-control input-new" placeholder="#Scan in here" autofocus="">
                <input type="text" name="user_id" id="user_id" value="{{ $user_id }}" class="hidden">
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="table-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Barcode ID</th>
                            <th>Cutting Date</th>
                            <th>Style</th>
                            <th>Article</th>
                            <th>Color</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
    $('#barcode_id').focus();
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('cutting.ajaxDailyCutting') }}",
            data: function(d) {
                 return $.extend({}, d, {
                     "user_id"         : $('#user_id').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id_marker', name: 'id_marker'},
            {data: 'cutting_date', name: 'cutting_date'},
            {data: 'style', name: 'style'},
            {data: 'articleno', name: 'articleno'},
            {data: 'color', name: 'color'},
            {data: 'created_at', name: 'created_at'},
            {data: 'finished_at', name: 'finished_at'},
            {data: 'status', name: 'status'}
        ]
    });

    $('.input-new').keypress(function(event){
        if (event.which==13) {
          
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('cutting.ajaxSetStatus') }}",
                data: {barcodeid: $('#barcode_id').val()},
                beforeSend: function() {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                    $('#barcode_id').val('');
                    
                },
                success: function(response){
                    $.unblockUI();
                     var data_response = response.data;
                     if (data_response.status==200) {
                         $("#alert_success").trigger("click", data_response.output);
                    }else{
                         $("#alert_warning").trigger("click", data_response.output);
                    }

                    tableL.clear();
                    tableL.draw();
                },
                error: function(response){
                    var data_response = response.data;
                    $.unblockUI();
                    $("#alert_warning").trigger("click", data_response);
                    tableL.clear();
                    tableL.draw();
                }
            });
        }
    });
});
</script>
@endsection
