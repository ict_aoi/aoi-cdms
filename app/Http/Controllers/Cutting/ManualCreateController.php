<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Models\TempheadBundle;
use App\Models\TempaddSize;
use App\Models\CutData;
use App\Models\BundleHeader;
use App\Models\BundleDetail;
use App\Models\TempdataTicket;
use StdClass;
use DataTables;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Data\DataCutting;
use Uuid;
use App\Http\Controllers\Cutting\CuttingBarcodeController;

class ManualCreateController extends Controller
{
    public function index(){
        // $style              = DataCutting::select('style','season')->whereNull('deleted_at')->where('warehouse',Auth::user()->factory_id)->distinct('style','season')->get();
        // $styles             = DB::connection('erp_live')->table('rma_style_fg')->select(DB::raw('upc as style,kst_season as season'))->groupBy('upc','kst_season')->get();
        $size               = DataCutting::select('size_finish_good')->whereNull('deleted_at')->where('warehouse',Auth::user()->factory_id)->distinct('size_finish_good')->get();
        $session_edit       = TempaddSize::where('user_nik',Auth::user()->nik)->where('factory_id',Auth::user()->factory_id)->first();
        if($session_edit == null){
            $set_type = null;
        }else{
            $set_type = $session_edit->set_type == '0' ? null : '-'.$session_edit->set_type;
        }
        $style_head         = $session_edit == null ? '' : $session_edit->season.'-'.$session_edit->style.$set_type;
        $temp_style_queue   = $session_edit == null ? 0 : $session_edit->style;
        $cut_num            = $session_edit == null ? '' : $session_edit->cut_num;
        $detail_temp        = TempheadBundle::where('style',$temp_style_queue)->where('user_nik',Auth::user()->nik)->OrderBy('input_time','DESC')->first();
        $is_details         = $detail_temp == null ? false : true;
        $last_queue         = TempheadBundle::where('user_nik',Auth::user()->nik)->where('factory_id',Auth::user()->factory_id)->whereNotNull('que_no')->orderBy('que_no','DESC')->first();
        $que_no             = $last_queue == null ? 1 : (int)$last_queue->que_no + 1;
        $size_register      = TempaddSize::where('user_nik',Auth::user()->nik)->where('factory_id',Auth::user()->factory_id)->orderBy('size','ASC')->get();
        // if($session_edit != null){
        //     return abort(429);
        // }else{
            return view('cutting.manual_create.index',compact(['size','style_head','poreference','size_register','is_details','cut_num','que_no']));
        //}
    }

    public function getDataStyle(Request $request)
    {
        $term = trim(strtoupper($request->q));
        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DB::connection('erp_live')->table('rma_style_fg')
        ->select(DB::raw('upc as style,kst_season as season'))
        ->where('isactive','Y')
        ->where('upc', 'like', '%'.$term.'%')
        ->groupBy('upc','kst_season')
        ->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->season.'-'.$tag->style,
                'text' => $tag->season.'-'.$tag->style
            ];
        }
        return \Response::json($formatted_tags);
    }

    public function addDatasize(Request $request){
        $style_temp = $request->style;
        $style      = explode('-',$style_temp);
        if(count($style) > 2 && in_array('TOP',$style) || in_array('BOTTOM',$style)){
            $season     = $style[0];
            $style_arr = [];
            for($i=1;$i < count($style) - 1;$i++){
                $style_arr[] = $style[$i];
            }
            $style      = implode('-',$style_arr);
            $pos_set    = count($style) - 1;
            $set_type   = $style[$pos_set] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style) > 1 && count($style) <= 2 && in_array('TOP',$style) || in_array('BOTTOM',$style)){
            $season     = $style[0];
            $style      = $style[1];
            $set_type   = $style[2] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style) > 2 && !in_array('TOP',$style) || !in_array('BOTTOM',$style)){
            $season     = $style[0];
            $style_arr = [];
            for($i=1;$i < count($style); $i++){
                $style_arr[] = $style[$i];
            }
            $style = implode('-',$style_arr);
            $set_type   = 0;
        }else{
            $season     = $style[0];
            $style      = $style[1];
            $set_type   = 0;
        }
        $cut_num    = $request->cut_num;
        $coll       = $request->coll;
        $bd_op      = $request->bd_op;
        $qc_op      = $request->qc_op;
        $cutter     = $request->machine;
        $bd_lim     = $request->q_bd;
        $size       = $request->size;
        $box_lim    = $request->q_box;
        $add_note   = $request->note;
        
        $set_type_cdms = $set_type == 0 ? 'Non' : $set_type;
        $double_size        = DB::table('temp_add_size')->where('size',$size)->where('user_nik',Auth::user()->nik)->where('factory_id',Auth::user()->factory_id)->first();
        $master_style_cdms  = DB::table('v_master_style')->where('style',$style)->where('type_name',$set_type_cdms)->where('season_name',$season)->first();
        $master_style_sds   = DB::connection('sds_live')->table('art_desc_new')->where('style',$style)->where('set_type',$set_type)->where('season',$season)->first();
        if($master_style_cdms == null){
            return response()->json('MASTER STYLE CDMS BELUM DI BUAT!',422);
        }
        if($master_style_sds == null){
            return response()->json('MASTER STYLE SDS BELUM DI BUAT!',422);
        }
        if($double_size != null){
            return response()->json('Size Sudah Terdaftar',422);
        }
        try{
            DB::begintransaction();
                TempaddSize::firstorCreate([
                    'season'        => $season,
                    'style'         => $style,
                    'set_type'      => $set_type,
                    'article'       => 0,
                    'cut_num'       => (int)$cut_num,
                    'size'          => $size,
                    'coll'          => trim(strtoupper($coll)),
                    'qc_op'         => trim(strtoupper($qc_op)),
                    'bd_op'         => trim(strtoupper($bd_op)),
                    'cutter'        => trim(strtoupper($cutter)),
                    'add_note'      => trim(strtoupper($add_note)),
                    'bd_lim'        => trim((int)$bd_lim),
                    'box_lim'       => trim((int)$box_lim),
                    'input_time'    => Carbon::now(),
                    'user_nik'      => Auth::user()->nik,
                    'factory_id'    => Auth::user()->factory_id,
                    'created_by'    => Auth::user()->id
                ]);
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function createHead(Request $request){
        $user_nik       = $request->user_id;
        $cut_num        = $request->cut_num;
        $factory_id     = User::where('nik',$user_nik)->first();
        $style_temp     = explode('-',$request->style);
        if(count($style_temp) > 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style_arr = [];
            for($i=1;$i < count($style_temp) - 1;$i++){
                $style_arr[] = $style_temp[$i];
            }
            $style      = implode('-',$style_arr);
            $pos_set    = count($style_temp) - 1;
            $set_type   = $style_temp[$pos_set] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style_temp) > 1 && count($style_temp) <= 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style      = $style_temp[1];
            $set_type   = $style_temp[2] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style_temp) > 2 && !in_array('TOP',$style_temp) || !in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style_arr = [];
            for($i=1;$i < count($style_temp); $i++){
                $style_arr[] = $style_temp[$i];
            }
            $style = implode('-',$style_arr);
            $set_type   = 0;
        }else{
            $season     = $style_temp[0];
            $style      = $style_temp[1];
            $set_type   = 0;
        }
        $cut_num            = $request->cut_num;
        try{
            DB::begintransaction();
                TempheadBundle::firstorCreate([
                    'season'        => $season,
                    'style'         => $style,
                    'set_type'      => $set_type,
                    'cut_num'       => $cut_num,
                    'input_time'    => Carbon::now(),
                    'user_nik'      => $user_nik,
                    'factory_id'    => $factory_id->factory_id,
                    'created_by'    => Auth::user()->id
                ]);
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getDataSize(Request $request){
        if(request()->ajax()) 
        {
            $data = DB::table('temp_add_size')
                    ->where('factory_id',Auth::user()->factory_id)
                    ->where('user_nik',Auth::user()->nik)
                    ->orderBy('size','asc')
                    ->get();

            return DataTables::of($data)
            ->addColumn('action', function($data){
                if($data->id != null){
                    return '<a href="'.route('manual_create.deleteTempSize', $data->id).'" data-id="'.$data->id.'" class="ignore-click delete btn btn-danger btn-raised btn-xs"><i class="icon-trash"></i></a>';
                }
            })
            ->rawColumns(['action'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function getDataDetail(Request $request){
        if(request()->ajax()) 
        {
            $data = DB::table('temp_head_bundle')
                    ->where('factory_id',Auth::user()->factory_id)
                    ->where('user_nik',Auth::user()->nik)
                    ->whereNotNull('poreference')
                    ->orderBy('que_no','ASC')
                    ->orderBy('size','ASC')
                    ->get();

            return DataTables::of($data)
            ->editColumn('style',function($data){
                $set_type = $data->set_type == '0' ? null : '-'.$data->set_type;
                return $data->style.$set_type.' / '.$data->cut_num;
            })
            ->editColumn('cutting_date',function($data){
                return Carbon::parse($data->ed_pcd)->format('d-m-Y');
            })
            ->editColumn('qty',function($data){
                return '<div id="qty_'.$data->id.'">
                            <input type="text" style="background-color:#f5f5f5;text-align:center"
                                data-id="'.$data->id.'"
                                class="form-control qty_unfilled text-center"
                                onfocus="return $(this).select();"
                                value="'.$data->qty.'">
                            </input>
                        </div>';
            })
            ->addColumn('action', function($data){
                if($data->id != null){
                    return '<a href="'.route('manual_create.deleteDetailsize', $data->id).'" data-id="'.$data->id.'" class="ignore-click delete btn btn-danger btn-raised btn-xs"><i class="icon-trash"></i></a>';
                }
            })
            ->rawColumns(['action','qty'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function summarySizeQty(Request $request){
        if(request()->ajax()) 
        {
            $data = DB::table('temp_head_bundle')
                    ->select('que_no' ,'season' ,'style' ,'cut_num' ,'article' ,'size','set_type')
                    ->where('factory_id',Auth::user()->factory_id)
                    ->where('user_nik',Auth::user()->nik)
                    ->whereNotNull('poreference')
                    ->distinct('que_no','size','season','style','cut_num','article','set_type')
                    ->get();

            return DataTables::of($data)
            ->editColumn('style',function($data){
                $set_type = $data->set_type == '0' ? null : '-'.$data->set_type;
                return $data->style.$set_type.'/'.$data->cut_num;
            })
            ->editColumn('qty',function($data){
                $total_qty = DB::table('temp_head_bundle')->where('size',$data->size)->where('style',$data->style)->where('set_type',$data->set_type)->where('cut_num',$data->cut_num)->where('user_nik',Auth::user()->nik)->whereNotNull('poreference')->sum('qty');
                return $total_qty;
            })
            ->rawColumns(['action','qty'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function updateQty(Request $request)
    {
        $id = $request->id;
        $qty = $request->qty;
        try{
            DB::begintransaction();
                TempheadBundle::where('id',$id)->update([
                    'qty'    => $qty
                ]);
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function deleteTempSize(Request $request){
        $id = $request->id;
        try{
            DB::begintransaction();
                TempaddSize::where('id', $id)->delete();
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function deleteDetailsize(Request $request){
        $id = $request->id;
        try{
            DB::begintransaction();
                TempheadBundle::where('id', $id)->delete();
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function sessionEdit(Request $request){
        if(request()->ajax()) 
        {
            $user_id = $request->user_id;
            $factory_id = User::where('nik',$user_id)->first();
            $data   = TempaddSize::where('user_nik',$user_id)->where('factory_id',$factory_id->factory_id)->first();
            return response()->json($data, 200);
        }
    }

    public function checkRegSize(Request $request){
        if(request()->ajax()) 
        {
            $user_id = $request->user_id;
            $factory_id = User::where('nik',$user_id)->first();
            $data   = TempheadBundle::where('user_nik',$user_id)->where('factory_id',$factory_id->factory_id)->where('season','<>','0')->count();
            return response()->json($data, 200);
        }
    }

    public function updateQue(Request $request){
        if(request()->ajax()) 
        {
            $user_id    = $request->user_id;
            $factory_id = User::where('nik',$user_id)->first();
            $data_temp  = TempheadBundle::where('user_nik',$user_id)->where('factory_id',$factory_id->factory_id)->whereNotNull('que_no')->OrderBy('que_no','DESC')->first();
            $data = $data_temp == null ? 0 : $data_temp;
            return response()->json($data, 200);
        }
    }

    public function ajaxGetDataSize(Request $request)
    {
        $term = trim(strtoupper($request->q));
        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DataCutting::select('size_finish_good')
        ->where('warehouse',Auth::user()->factory_id)
        ->where('size_finish_good', 'like', $term.'%')
        ->whereNull('deleted_at')
        ->limit(1)
        ->distinct('size_finish_good')
        ->orderBy('size_finish_good')
        ->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->size_finish_good, 
                'text' => $tag->size_finish_good
            ];
        }
        return \Response::json($formatted_tags);
    }

    public function ajaxGetDataPobuyer(Request $request)
    {
        $term = trim(strtoupper($request->q));
        if (empty($term)) {
            return \Response::json([]);
        }
        // $tags = DB::connection('erp_live')
        // ->table('dd_csi_details')
        // ->select('po_buyer')
        // ->where(function($query)use ($term){
        //     $query->where('po_buyer','like','%'.$term)
        //     ->orWhere('job_no','like','%'.$term.'%');
        // })
        // ->distinct('po_buyer')
        // ->get();
        $tags = DB::connection('sds_live')
        ->table('ppcm_new')
        ->select('poreference')
        ->where(function($query)use ($term){
            $query->where('poreference','like','%'.$term)
            ->orWhere('job','like','%'.$term.'%');
        })
        ->distinct('poreference')
        ->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->poreference, 
                'text' => $tag->poreference
            ];
        }
        return \Response::json($formatted_tags);
    }

    public function ajaxGetDataArt(Request $request)
    {
        $term = trim(strtoupper($request->q));
        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DataCutting::select('articleno')->where('articleno', 'like', '%'.$term)->whereNull('deleted_at')->limit(5)->distinct('articleno')->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->articleno, 
                'text' => $tag->articleno
            ];
        }
        return \Response::json($formatted_tags);
    }

    public function generateBarcode(Request $request){
        $user_nik       = $request->user_id;
        $factory_id     = User::where('nik',$user_nik)->first();
        $header_bundle  = TempaddSize::where('user_nik',$user_nik)->where('factory_id',$factory_id->factory_id)->get();
        $detail_check   = TempheadBundle::where('user_nik',$user_nik)->where('factory_id',$factory_id->factory_id)->whereNotNull('poreference')->get();
        if(count($detail_check) == 0){
            return response()->json('DETAIL BELUM DI INPUT!',422);
        }
        if(count($header_bundle) > 0){
            $temp[]=NULL;
            foreach($header_bundle as $hb){
                $start=1;
                $detail_bundle = TempheadBundle::where('style',$hb->style)
                ->where('cut_num',$hb->cut_num)
                ->where('season',$hb->season)
                ->where('set_type',$hb->set_type)
                ->where('size',$hb->size)
                ->orderBy('que_no','ASC')
                ->get();
                if(count($detail_bundle) == 0){
                    return response()->json('TERJADI KESALAHAN HUBUNGI ICT',422);
                }
                foreach($detail_bundle as $dbl){
                    $season         = $dbl->season;
                    $poreference    = $dbl->poreference;
                    $po_qty         = $dbl->qty;
                    $set_type       = $dbl->set_type == '0' ? 'Non' : $dbl->set_type;
                    $input_ad_sds = DB::table('v_master_style')->where('style',$dbl->style)->where('type_name',$set_type)->where('season_name',$dbl->season)->get();
                    if(count($input_ad_sds) == 0){
                        return response()->json('HARAP INPUT MASTER STYLE DAHULU',422);
                    }
                    $part = DB::table('v_master_style')->where('style',$dbl->style)->where('type_name',$set_type)->where('season_name',$dbl->season)->get();
                    while($po_qty > 0){
                        if($hb->bd_lim==''){
                            $d_depth=$po_qty;
                            $po_qty=0;
                        }else if($po_qty > ($hb->bd_lim)){
                            $d_depth=$hb->bd_lim;
                            $po_qty-=$hb->bd_lim;
                        }else{
                            $d_depth=$po_qty;
                            $po_qty=0;
                        }
                        $end_num=$start+($d_depth)-1;
                        foreach($part as $pt){
                            $dd = new StdClass();
                            $dd->season=$season;
                            $dd->style=$dbl->style;
                            $dd->set_type=$set_type;
                            $dd->cut_num=$dbl->cut_num;
                            $dd->coll=$hb->coll;
                            $dd->article=$dbl->article;
                            $dd->poreference=$dbl->poreference;
                            $dd->size=$hb->size;
                            $dd->depth=$d_depth;
                            $dd->user_nik=$user_nik;
                            $dd->part_num=$pt->part;
                            $dd->part_name=$pt->komponen_name;
                            $dd->note=$pt->process_name == null || $pt->process_name == '' ? '' : str_replace(';','/',$pt->process_name);
                            $dd->start_num=$start;
                            $dd->end_num=$end_num;
                            $dd->bd_op=$hb->bd_op;
                            $dd->qc_op=$hb->qc_op;
                            $dd->cutter=$hb->cutter;
                            $dd->ed_pcd=$dbl->ed_pcd;
                            $dd->box_lim=$hb->box_lim;
                            $dd->add_note=$hb->add_note;
                            $dd->processing_fact=$factory_id->factory_id;
                            $temp[]=$dd;
                            unset($dd);
                        }
                        $start+=$d_depth;
                    }
                }
            }
            if(isset($temp[1])){
                unset($temp[0]);
                foreach($temp as $tmp){
                    try{
                        DB::begintransaction();
                            TempdataTicket::firstorCreate([
                                'season'            => $tmp->season,
                                'style'             => $tmp->style,
                                'set_type'          => $tmp->set_type,
                                'poreference'       => $tmp->poreference,
                                'article'           => $tmp->article,
                                'cut_num'           => $tmp->cut_num,
                                'size'              => $tmp->size,
                                'part_id'           => null,
                                'part_num'          => $tmp->part_num,
                                'part_name'         => $tmp->part_name,
                                'note'              => $tmp->note,
                                'start_num'         => $tmp->start_num,
                                'end_num'           => $tmp->end_num,
                                'depth'             => $tmp->depth,
                                'processing_fact'   => $tmp->processing_fact,
                                'ed_pcd'            => $tmp->ed_pcd,
                                'lot'               => null,
                                'coll'              => $tmp->coll,
                                'qc_op'             => $tmp->qc_op,
                                'bd_op'             => $tmp->bd_op,
                                'cutter'            => $tmp->cutter,
                                'add_note'          => $tmp->add_note,
                                'box_lim'           => $tmp->box_lim,
                                'replacement'       => false,
                                'input_time'        => Carbon::now(),
                                'user_nik'          => $tmp->user_nik,
                                'created_by'        => Auth::user()->id
                            ]);
                        DB::commit();
                    }catch (Exception $e) {
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
            }
        }else{
            return response()->json('Terjadi Kesalahan Harap Refresh Laman Ini',422);
        }
        //INSERT / UPDATE KE TABLE cut_data_ticket (BUNDLE HEADER VERSI SDS)
        // $check_cut_data = DB::SELECT(DB::RAW("SELECT x.*,cdt.poreference AS alt_po,cdt.id AS cut_id 
        // FROM(SELECT tas.style,thb.set_type,thb.poreference,thb.article,tas.cut_num,thb.size,tas.coll,tas.bd_op,tas.cutter,tas.qc_op,thb.ed_pcd,thb.user_nik,tas.add_note,tas.box_lim,thb.season,tas.factory_id,tas.bd_lim,thb.qty
        // FROM temp_add_size tas
        // JOIN temp_head_bundle thb ON thb.style = tas.style AND thb.set_type = tas.set_type AND thb.cut_num=tas.cut_num AND thb.size=tas.size AND thb.poreference is not null)x
        // LEFT JOIN cut_data_ticket cdt on cdt.season = x.season AND x.article=cdt.article AND x.poreference=cdt.poreference AND x.set_type=cdt.set_type AND x.size=cdt.size AND x.cut_num=cdt.cut_num
        // WHERE x.user_nik='$user_nik' ORDER BY cdt.poreference,x.size"));

        // foreach($check_cut_data as $ccd){
        //     $cut_data_sds = DB::connection('sds_live')
        //     ->table('cut_data_new')
        //     ->where('poreference',$ccd->poreference)
        //     ->where('style',$ccd->style)
        //     ->where('set_type',$ccd->set_type)
        //     ->where('size',$ccd->size)
        //     ->where('cut_num',$ccd->cut_num)
        //     ->where('processing_fact',$ccd->factory_id)
        //     ->first();
        //     if($cut_data_sds == null){
        //         try{
        //             DB::begintransaction();
        //                 if($ccd->set_type == 'BOTTOM'){
        //                     $set_type_erp = '-BOT';
        //                 }elseif($ccd->set_type == 'TOP'){
        //                     $set_type_erp = '-TOP';
        //                 }else{
        //                     $set_type_erp = null;
        //                 }
        //                 $pcd = DB::connection('erp_live')->table('rma_dist_sys')->where('poreference',$ccd->poreference)->where('style',$ccd->style.$set_type_erp)->where('gmt_size',$ccd->size)->where('kst_articleno',$ccd->article)->first();
        //                 $cut_code = Carbon::now()->format('YmdHis').(sprintf('%09d',(string)$user_nik)).(sprintf('%04d',rand(1,9999)));
        //                 DB::connection('sds_live')->table('cut_data_new')->insertGetId([
        //                     'season'            => $ccd->season,
        //                     'style'             => $ccd->style,
        //                     'set_type'          => $ccd->set_type,
        //                     'poreference'       => $ccd->poreference,
        //                     'article'           => $ccd->article,
        //                     'cut_num'           => $ccd->cut_num,
        //                     'size'              => $ccd->size,
        //                     'act_spread'        => Carbon::now(),
        //                     'adm'               => Auth::user()->nik,
        //                     'processing_fact'   => $ccd->factory_id,
        //                     'ed_pcd'            => $pcd->pcd,
        //                     'coll'              => $ccd->color_name,
        //                     'qc_op'             => $ccd->qc_op,
        //                     'bd_op'             => $ccd->bd_op,
        //                     'cutter'            => $ccd->cutter,
        //                     'add_note'          => $ccd->add_note,
        //                     'box_lim'           => $ccd->box_lim,
        //                     'cut_code'          => $cut_code
        //                 ]);
        //             DB::commit();
        //         }catch (Exception $e) {
        //             DB::rollback();
        //             $message = $e->getMessage();
        //             ErrorHandler::db($message);
        //         }
        //     }else{
        //         if($ccd->set_type == 'BOTTOM'){
        //             $set_type_erp = '-BOT';
        //         }elseif($ccd->set_type == 'TOP'){
        //             $set_type_erp = '-TOP';
        //         }else{
        //             $set_type_erp = null;
        //         }
        //         $pcd = DB::connection('erp_live')->table('rma_dist_sys')->where('poreference',$ccd->poreference)->where('style',$ccd->style.$set_type_erp)->where('gmt_size',$ccd->size)->where('kst_articleno',$ccd->article)->first();
        //         try{
        //             DB::begintransaction();
        //                 DB::connection('sds_live')->table('cut_data_new')->where('poreference',$ccd->poreference)
        //                 ->where('style',$ccd->style)
        //                 ->where('set_type',$ccd->set_type)
        //                 ->where('size',$ccd->size)
        //                 ->where('cut_num',$ccd->cut_num)
        //                 ->where('processing_fact',$ccd->factory_id)->update([
        //                     'act_spread'        => Carbon::now(),
        //                     'adm'               => Auth::user()->nik,
        //                     'ed_pcd'            => $pcd->pcd,
        //                 ]);
        //         DB::commit();
        //         }catch (Exception $e) {
        //             DB::rollback();
        //             $message = $e->getMessage();
        //             ErrorHandler::db($message);
        //         }
        //     }
        // }
        //CREATE DATA PREPARATION BEFORE INSERT TO DETAIL TICKET
        $data_prep = TempdataTicket::where('created_by',Auth::user()->id)->get();

        foreach($data_prep as $dp){
            if($dp->set_type == 'Non'){
                $set_type = '0';
            }elseif($dp->set_type == 'TOP'){
                $set_type = 'TOP';
            }else{
                $set_type = 'BOTTOM';
            }

            $cut_id_tmp = DB::connection('sds_live')->table('cut_data_new')->where('poreference',$dp->poreference)->where('style',$dp->style)->where('set_type',$set_type)->where('article',$dp->article)->where('cut_num',$dp->cut_num)->where('size',$dp->size)->where('processing_fact',$dp->processing_fact)->first();

            if($cut_id_tmp == null){
                try{
                    DB::begintransaction();
                        $cut_code = Carbon::now()->format('YmdHis').(sprintf('%09d',(string)$user_nik)).(sprintf('%04d',rand(1,9999)));
                        if($dp->set_type == 'BOTTOM'){
                            $set_type_sds = 'BOTTOM';
                        }elseif($dp->set_type == 'TOP'){
                            $set_type_sds = 'TOP';
                        }else{
                            $set_type_sds = '0';
                        }
                        DB::connection('sds_live')->table('cut_data_new')->insertGetId([
                            'season'            => $dp->season,
                            'style'             => $dp->style,
                            'set_type'          => $set_type_sds,
                            'poreference'       => $dp->poreference,
                            'article'           => $dp->article,
                            'cut_num'           => $dp->cut_num,
                            'size'              => $dp->size,
                            'act_spread'        => Carbon::now(),
                            'adm'               => Auth::user()->nik,
                            'processing_fact'   => $dp->processing_fact,
                            'ed_pcd'            => $dp->ed_pcd,
                            'coll'              => $dp->coll,
                            'qc_op'             => $dp->qc_op,
                            'bd_op'             => $dp->bd_op,
                            'cutter'            => $dp->cutter,
                            'add_note'          => $dp->add_note,
                            'box_lim'           => $dp->box_lim,
                            'cut_code'          => $cut_code
                        ]);
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
            $cek = DB::connection('sds_live')->select(DB::raw("SELECT * FROM bundle_info_new WHERE poreference = '$dp->poreference' AND style = '$dp->style' AND article = '$dp->article' AND cut_num = '$dp->cut_num' AND size = '$dp->size' AND part_num = '$dp->part_num' AND part_name = '$dp->part_name' AND start_num = '$dp->start_num'
            AND end_num = '$dp->end_num' AND processing_fact = '$dp->processing_fact'"));

            $cut_id = DB::connection('sds_live')->table('cut_data_new')->where('poreference',$dp->poreference)->where('style',$dp->style)->where('set_type',$set_type)->where('article',$dp->article)->where('cut_num',$dp->cut_num)->where('size',$dp->size)->where('processing_fact',$dp->processing_fact)->first();
            if(count($cek) > 0){
                //Do nothing!, just executes existing data
            }else{
                try{
                    DB::begintransaction();
                        DB::connection('sds_live')->table('bundle_info_new')->insert([
                            'barcode_id'        => 1,
                            'season'            => $dp->season,
                            'style'             => $dp->style,
                            'poreference'       => $dp->poreference,
                            'article'           => $dp->article,
                            'cut_num'           => $dp->cut_num,
                            'size'              => $dp->size,
                            'part_ids'          => null,
                            'part_num'          => $dp->part_num,
                            'part_name'         => $dp->part_name,
                            'note'              => $dp->note,
                            'start_num'         => $dp->start_num,
                            'end_num'           => $dp->end_num,
                            'qty'               => $dp->depth,
                            'location'          => 'GEN',
                            'factory'           => 'GEN',
                            'processing_fact'   => $dp->processing_fact,
                            'create_date'       => Carbon::now(),
                            'bc_print'          => 1,
                            'cut_id'            => $cut_id->id,
                            'coll'              => $dp->coll
                        ]);
                        TempaddSize::where('user_nik',$user_nik)->delete();
                        TempheadBundle::where('user_nik',$user_nik)->delete();
                        TempdataTicket::where('user_nik',$user_nik)->delete();
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
        }
    }

    public function deleteHeader(Request $request){
        $user_nik       = $request->user_id;
        $factory_id     = User::where('nik',$user_nik)->first();
        try{
            DB::begintransaction();
                TempaddSize::where('user_nik',$user_nik)->delete();
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function deleteDetail(Request $request){
        $user_nik       = $request->user_id;
        $factory_id     = User::where('nik',$user_nik)->first();
        try{
            DB::begintransaction();
                TempaddSize::where('user_nik',$user_nik)->where('factory_id',$factory_id->factory_id)->delete();
                TempheadBundle::where('user_nik',$user_nik)->where('factory_id',$factory_id->factory_id)->delete();
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function addDetailsize(Request $request){
        $x = $request->all();
        $user_id        = $request->user_id;
        $factory_id     = User::where('nik',$user_id)->first();
        $data_size      = TempaddSize::select('size')->where('user_nik',$user_id)->where('factory_id',$factory_id->factory_id)->distinct('size')->count();
        $style_temp     = explode('-',$request->style);
        $cut_num        = $request->cut_num;
        if(count($style_temp) > 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style_arr = [];
            for($i=1;$i < count($style_temp) - 1;$i++){
                $style_arr[] = $style_temp[$i];
            }
            $style      = implode('-',$style_arr);
            $pos_set    = count($style_temp) - 1;
            $set_type   = $style_temp[$pos_set] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style_temp) > 1 && count($style_temp) <= 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style      = $style_temp[1];
            $set_type   = $style_temp[2] == 'TOP' ? 'TOP' : 'BOTTOM';
        }elseif(count($style_temp) > 2 && !in_array('TOP',$style_temp) || !in_array('BOTTOM',$style_temp)){
            $season     = $style_temp[0];
            $style_arr = [];
            for($i=1;$i < count($style_temp); $i++){
                $style_arr[] = $style_temp[$i];
            }
            $style = implode('-',$style_arr);
            $set_type   = 0;
        }else{
            $season     = $style_temp[0];
            $style      = $style_temp[1];
            $set_type   = 0;
        }
        $queue_temp     = $request->order;
        $po_buyer       = $request->po_buyer;
        $article        = $request->article;
        $queue_check    = TempheadBundle::where('poreference',$po_buyer)->where('article',$article)->first();
        $queue          = $queue_check == null ? $queue_temp : $queue_check->que_no;
        for($i=(count($x) - $data_size);$i < (count($x)); $i++){
            $z                  = $i - (count($x) - $data_size);
            $qty_temp           = 'size_reg-'.$z;
            $size_temp          = 'sz-'.((int)$z+1);
            $qty_size           = $request->$qty_temp;
            $size               = $request->$size_temp;
            $cutting_date_temp  = DataCutting::where(function($query) use ($po_buyer){
                $query->where('po_buyer',$po_buyer)
                ->orWhere('job_no','like','%'.$po_buyer.'%');
            })
            ->where('articleno',$article)
            ->where('size_finish_good',$size)
            ->first();
            $cutting_date       = $cutting_date_temp == null ? Carbon::now()->format('Y-m-d h:i:s') : Carbon::parse($cutting_date_temp->cutting_date)->format('Y-m-d 00:00:00');
            //CHECK CREATED BARCODE CDMS
            $set_type_cdms = $set_type == 0 ? 'Non' : $set_type;
            // $created_cdms = DB::table('bundle_header as bh')
            // ->join('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
            // ->join('v_master_style as vms','vms.id','=','bd.style_detail_id')
            // ->where('bh.poreference',$po_buyer)
            // ->where('bh.style',$style)
            // ->where('vms.type_name',$set_type_cdms)
            // ->where('bh.size',$size)
            // ->where('bh.cut_num',$cut_num)
            // ->where('bh.article',$article)
            // ->first();
            //CHECK CREATED BARCODE SDS
            $created_sds = DB::connection('sds_live')->table('bundle_info_new as bi')
            ->join('art_desc_new as ad',function($j){
                $j->on('ad.season','=','bi.season')
                ->on('ad.style','=','bi.style')
                ->on('ad.part_num','=','bi.part_num')
                ->on('ad.part_name','=','bi.part_name');
            })
            ->where('bi.poreference',$po_buyer)
            ->where('bi.style',$style)
            ->where('ad.set_type',$set_type)
            ->where('bi.article',$article)
            ->where('bi.cut_num',$cut_num)
            ->where('bi.size',$size)
            ->first();
            // if($created_cdms != null){
            //     return response()->json('PO '.$po_buyer.'-'.$article.' - CUT '.$cut_num.' - SIZE '.$size.' SUDAH DI BUAT TIKET DI CDMS!',422);
            // }
            if($created_sds != null){
                return response()->json('PO '.$po_buyer.'-'.$article.' - CUT '.$cut_num.' - SIZE '.$size.' SUDAH DI BUAT TIKET DI SDS!',422);
            }
            try{
                DB::beginTransaction();
                TempheadBundle::firstorCreate([
                    'season'        => $season,
                    'poreference'   => $po_buyer,
                    'article'       => $article,
                    'style'         => $style,
                    'set_type'      => $set_type,
                    'ed_pcd'        => $cutting_date,
                    'cut_num'       => (int)$cut_num,
                    'que_no'        => (int)$queue,
                    'size'          => $size,
                    'qty'           => (int)$qty_size,
                    'input_time'    => Carbon::now(),
                    'user_nik'      => $user_id,
                    'created_by'    => Auth::user()->id,
                    'factory_id'    => $factory_id->factory_id
                ]);
                DB::table('temp_head_bundle')->where('qty',0)->delete();
                $last_input = TempheadBundle::where('poreference',$po_buyer)->where('article',$article)->whereNotNull('size')->orderBy('input_time','DESC')->first();
                if($last_input != null){
                    $count_size = TempheadBundle::where('poreference',$po_buyer)->where('article',$article)->where('size',$last_input->size)->count('size');
                    if($count_size > 1){
                        TempheadBundle::where('poreference',$po_buyer)->where('article',$article)->where('size',$last_input->size)->orderBy('input_time','DESC')->delete();
                        return response()->json('SIZE '.$last_input->size.' Sudah Terdaftar', 422);
                    }
                }
                DB::commit();
            }catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    public function sypView(){
        return view('cutting.manual_create.syp_view');
    }

    public function getDataManualCreate(Request $request){
        if(request()->ajax()) 
        {
            $temp = explode('-',$request->date_range);
            $from = Carbon::parse($temp[0])->format('Y-m-d 00:00:00');
            $to = Carbon::parse($temp[1])->format('Y-m-d 23:59:59');

            $data = DB::table('bundle_header as bh')
            ->selectRaw('barcode_marker,season,cut_num,style,set_type,factory_id,admin_name')
            ->where('factory_id',Auth::user()->factory_id)
            ->whereBetween('created_at',[$from,$to])
            ->where('barcode_marker','like','%MAN%')
            ->distinct('barcode_marker','cut_num','season','style','set_type','factory_id','admin_name')
            ->get();
            
            if($data == null){
                return response()->json('Data Tidak Ditemukan..', 422);
            }else{
                return DataTables::of($data)
                ->editColumn('style',function($data){
                    if($data->set_type == 'Non'){
                        $style = $data->style;
                    }else{
                        $style = $data->style.'-'.$data->set_type;
                    }
                    return $style;
                })
                ->editColumn('poreference',function($data){
                    $po_buyers = BundleHeader::select('poreference')->where('barcode_marker',$data->barcode_marker)->pluck('poreference')->toArray();
                    $po_buyer = implode(',',array_unique($po_buyers));
                    return $po_buyer;
                })
                ->addColumn('action',function($data){
                    $bundle_head_id = BundleHeader::where('barcode_marker',$data->barcode_marker)->first();
                    $syncronized  = BundleDetail::select('sds_barcode')->where('bundle_header_id',$bundle_head_id->id)->whereNotNull('sds_barcode')->get();
                    if(count($syncronized) > 0){
                        return '<table><tr><td><a data-id="'.$data->barcode_marker.'" class="ignore-click btn btn-default btn-raised btn-xs" disabled><i class="icon-sync"></i></a></td><td>&nbsp&nbsp</td>
                        <td><a href="#" data-id="'.$data->barcode_marker.'" class="ignore-click print btn btn-success btn-raised btn-xs"><i class="icon-printer2"></i></a></td></tr></table>';
                    }else{
                        return '<table><tr><td><a data-id="'.$data->barcode_marker.'" class="ignore-click singkron btn btn-warning btn-raised btn-xs"><i class="icon-sync"></i></a></td><td>&nbsp&nbsp</td>
                        <td><a href="#" data-id="'.$data->barcode_marker.'" class="ignore-click btn btn-success btn-raised btn-xs" disabled><i class="icon-printer2"></i></a></td></tr></table>';
                    }
                })
            ->rawColumns(['action'])
            ->make(true);
            }
        }
    }

    public function sycnBarcodeBundle($id){
        $barcode_marker = $id;
        $get_data_cdms = DB::table('jaz_sinkron_sds')->where('barcode_marker', $barcode_marker)->get();
        if(count($get_data_cdms) > 0){
            try {
                DB::beginTransaction();
                foreach($get_data_cdms as $key => $cdms) {
                    $get_data_sds = DB::connection('sds_live')->table('jaz_sinkron_cdms')->where('style', $cdms->style)
                        ->where('set_type', $cdms->set_type)
                        ->where('season', $cdms->season)
                        ->where('article', $cdms->article)
                        ->where('poreference', $cdms->poreference)
                        ->where('cut_num', $cdms->cut_num)
                        ->where('size', $cdms->size)
                        ->where('part_num', $cdms->part_num)
                        ->where('part_name', $cdms->part_name)
                        ->where('start_num', $cdms->start_num)
                        ->where('end_num', $cdms->end_num)
                        ->where('qty', $cdms->qty)
                        ->where('processing_fact', $cdms->processing_fact)->first();
                    if($get_data_sds != null) {
                        $update_ = DB::table('bundle_detail')->where('barcode_id', $cdms->barcode_id)->update(['sds_barcode' => $get_data_sds->barcode_id]);
                    } else {
                        return response()->json('Style: '.$cdms->style.' || Set: '.$cdms->set_type.' || PO: '.$cdms->poreference.' || No Cut: '.$cdms->cut_num.' || size: '.$cdms->size.' || part: '.$cdms->part_num.' || komponen: '.$cdms->part_name.' || sticker: '.$cdms->start_num.'-'.$cdms->end_num.' || qty: '.$cdms->qty.' ||Terdapat data yang tidak sama!',422);
                    }
                }
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
        }else{
            return response()->json('Target belum di print!',422);
        }
    }

    public function parseBarcode(Request $request){
        $barcode_marker = $request->id;
        return response()->json($barcode_marker,200);
    }

    public function printBundle($barcode_id) {
        $barcode_marker = $barcode_id;
        //PENGECEKAN JIKA AWAL SUDAH CREATE BUNDLE LEWAT SDS MAKA TIDAK BOLEH DI CETAK LAGI LEWAT CDMS
        $sds_barcode_in_transaction = DB::table('bundle_detail')->Join('bundle_header','bundle_header.id','=','bundle_detail.bundle_header_id')
        ->where('bundle_header.barcode_marker',$barcode_marker)->whereNotNull('bundle_detail.sds_barcode')->pluck('sds_barcode')->toArray();
        if(count($sds_barcode_in_transaction) > 0){
            $cek_sds_transaction = DB::table('sds_detail_movements')->whereIn('sds_barcode',$sds_barcode_in_transaction)->get();
            if(count($cek_sds_transaction) > 0){
                return abort(403);
            }
        }

        $data = DB::table('bundle_detail as bd')->select('bd.barcode_id','bd.bundle_header_id','bd.style_detail_id','bd.komponen_name','bd.start_no','bd.end_no','bd.qty','bd.location','bd.sds_barcode','bd.is_recycle','bh.season','bh.style','bh.set_type','bh.poreference','bh.article','bh.cut_num','bh.size','bh.factory_id','bh.cutting_date','bh.color_name','bh.cutter','bh.note','bh.admin','bh.qc_operator','bh.bundle_operator','bh.box_lim','bh.max_bundle','bh.barcode_marker','bh.job','bh.cut_info','vms.part','vms.process','vms.process_name','vms.type_id','vms.type_name','mf.factory_name','mf.factory_alias','bh.admin_name','bh.qc_operator_name','bh.bundle_operator_name','bh.statistical_date','bh.created_at','bh.destination')
        ->join('bundle_header as bh','bh.id','=','bd.bundle_header_id')
        ->join('v_master_style as vms', 'vms.id', '=', 'bd.style_detail_id')
        ->join('master_factory as mf', 'mf.id', '=', 'bh.factory_id');

            //CONFIG LAYOUT
            $data = $data->where('bh.barcode_marker', $barcode_marker);
            $filename = 'barcode_marker_#' . $barcode_marker . '.pdf';
            $orientation = 'landscape';
            $papersize = 'a3';
            $load_view = 'cutting.barcode.print.index2';

            $data = $data->orderBy('bh.cut_num')->orderBy('bh.style')->orderBy('vms.type_id')->orderBy('bh.size')->orderBy('bd.komponen_name')->orderBy('bd.start_no')->orderBy('bd.end_no', 'ASC')->orderBy('bh.poreference')->get();
            
            if ($data[0]->factory_id != Auth::user()->factory_id) {
                return abort(403);
            }
            // get list komponen
            $list_komponen = DB::table('v_list_komponen')->where('barcode_marker', $barcode_marker);

            $list_komponen = $list_komponen->get();

            $po_buyer = $list_komponen->pluck('poreference')->toArray();
            $cut_date = DataCutting::whereIn('po_buyer',$po_buyer)->first();

            $data_plan = $cut_date;
            $master_style = DB::table('v_master_style')
                                ->where('style', $data[0]->style)
                                ->where('season_name', $data[0]->season)
                                ->where('type_id', $data[0]->type_id)
                                ->get();

            $data_bundle_header = DB::table('bundle_header')->where('barcode_marker', $barcode_marker);
            $data_bundle_header = $data_bundle_header->get();

            $no = 1;
            $from = 1;

            $data_bundle_detail = array();

            foreach ($master_style as $key1 => $val1) {
                foreach ($data_bundle_header as $key2 => $val2) {

                    $box_lim = $val2->box_lim > 0 ? $val2->box_lim : $val2->max_bundle;

                    $total_qty = $val2->qty;
                    $totalan = $total_qty + ($from-1);
                    $to = $box_lim - 1;
                        for ($i=1; $i <= $totalan; $i+=$box_lim) {
                            $panel = $i;
                            $panel_to = $panel+$to;
                            if ($panel_to > $totalan) {
                                $panel_to = $totalan;
                            }
                            $qty = ($panel_to-$panel)+1;
                            $detail['bundle_header_id'] =  $val2->id;
                            $detail['style_detail_id'] = $val1->id;
                            $detail['komponen_name'] = $val1->komponen_name;
                            $detail['start_no'] = $panel;
                            $detail['end_no'] = $panel_to;
                            $detail['qty'] = $qty;
                            $detail['location'] = 'barcoding';
                            $detail['sds_barcode'] = null;
                            $detail['created_at'] = Carbon::now();
                            $detail['updated_at'] = Carbon::now();
                            $detail['job'] = $val2->job;
                            $detail['season'] = $val2->season;
                            $detail['style'] = $val2->style;
                            $detail['set_type'] = $val2->set_type;
                            $detail['poreference'] = $val2->poreference;
                            $detail['article'] = $val2->article;
                            $detail['cut_num'] = $val2->cut_num;
                            $detail['size'] = $val2->size;
                            $detail['factory_id'] = $val2->factory_id;
                            $detail['cutting_date'] = $val2->cutting_date;
                            $detail['color_name'] = $val2->color_name;
                            $detail['cutter'] = $val2->cutter;
                            $detail['note'] = $val2->note;
                            $detail['admin'] = $val2->admin;
                            $detail['qc_operator'] = $val2->qc_operator;
                            $detail['bundle_operator'] = $val2->bundle_operator;
                            $detail['box_lim'] = $val2->box_lim;
                            $detail['max_bundle'] = $val2->max_bundle;
                            $detail['created_at'] = $val2->created_at;
                            $detail['updated_at'] = $val2->updated_at;
                            // $detail['qty'] = $val2->qty;
                            $detail['barcode_marker'] = $val2->barcode_marker;
                            $detail['job'] = $val2->job;
                            $detail['admin_name'] = $val2->admin_name;
                            $detail['qc_operator_name'] = $val2->qc_operator_name;
                            $detail['bundle_operator_name'] = $val2->bundle_operator_name;
                            $detail['statistical_date'] = $val2->statistical_date;
                            $detail['destination'] = $val2->destination;
                            $detail['part'] = $val1->part;
                            $detail['komponen'] = $val1->komponen;
                            $detail['total'] = $val1->total;
                            $detail['process'] = $val1->process;
                            $detail['process_name'] = $val1->process_name;
                            $detail['type_id'] = $val1->type_id;
                            $detail['type_name'] = $val1->type_name;
                            $data_bundle_detail[] = $detail;
                            $no++;
                        }
                }
            }

            $data_bundle_detail_new = array();
            $newkeytest = 0;
            foreach ($data_bundle_detail as $key => $val) {
                $data_bundle_detail_new[$val['size'].$val['style_detail_id']][$newkeytest] = $val;
                $newkeytest++;
            }

            $rows = array();
            $rows_new = array();
            $rows_new2 = array();
            $inc = 0;
            foreach ($data_bundle_detail_new as $vall) {
                $num = 1;
                foreach($vall as $row){
                    $row['num'] = $num;
                    $rows[] = $row;

                    $inc++;
                    $num++;
                }
            }

            foreach ($rows as $k => $v) {
                if ($v['num'] != 1) {
                    $rows[$k]['start_no'] = $rows[$k-1]['end_no']+1;
                    $rows[$k]['end_no'] = $rows[$k-1]['end_no'] + $v['qty'];
                }
            }

            foreach ($rows as $kk => $vv) {
                $rr['bundle_header_id'] =  $vv['bundle_header_id'];
                $rr['style_detail_id'] = $vv['style_detail_id'];
                $rr['komponen_name'] = $vv['komponen_name'];
                $rr['start_no'] = $vv['start_no'];
                $rr['end_no'] = $vv['end_no'];
                $rr['qty'] = $vv['qty'];
                $rr['location'] = $vv['location'];
                $rr['sds_barcode'] = $vv['sds_barcode'];
                $rr['created_at'] = $vv['created_at'];
                $rr['updated_at'] = $vv['updated_at'];
                $rr['job'] = $vv['job'];
                $rr['season'] = $vv['season'];
                $rr['style'] = $vv['style'];
                $rr['set_type'] = $vv['set_type'];
                $rr['poreference'] = $vv['poreference'];
                $rr['article'] = $vv['article'];
                $rr['cut_num'] = $vv['cut_num'];
                $rr['size'] = $vv['size'];
                $rr['factory_id'] = $vv['factory_id'];
                $rr['cutting_date'] = $vv['cutting_date'];
                $rr['color_name'] = $vv['color_name'];
                $rr['cutter'] = $vv['cutter'];
                $rr['note'] = $vv['note'];
                $rr['admin'] = $vv['admin'];
                $rr['qc_operator'] = $vv['qc_operator'];
                $rr['bundle_operator'] = $vv['bundle_operator'];
                $rr['box_lim'] = $vv['box_lim'];
                $rr['max_bundle'] = $vv['max_bundle'];
                $rr['created_at'] = $vv['created_at'];
                $rr['updated_at'] = $vv['updated_at'];
                $rr['barcode_marker'] = $vv['barcode_marker'];
                $rr['job'] = $vv['job'];
                $rr['admin_name'] = $vv['admin_name'];
                $rr['qc_operator_name'] = $vv['qc_operator_name'];
                $rr['bundle_operator_name'] = $vv['bundle_operator_name'];
                $rr['statistical_date'] = $vv['statistical_date'];
                $rr['destination'] = $vv['destination'];
                $rr['part'] = $vv['part'];
                $rr['komponen'] = $vv['komponen'];
                $rr['total'] = $vv['total'];
                $rr['process'] = $vv['process'];
                $rr['process_name'] = $vv['process_name'];
                $rr['type_id'] = $vv['type_id'];
                $rr['type_name'] = $vv['type_name'];

                $rows_new[] = $rr;
            }

            foreach ($rows_new as $kk1 => $vv1) {
                // cek apakah ada di bundle detail
                $cekbundledetail = DB::table('bundle_detail')
                                    ->where('bundle_header_id', $vv1['bundle_header_id'])
                                    ->where('style_detail_id', $vv1['style_detail_id'])
                                    ->where('start_no', $vv1['start_no'])
                                    ->first();
                if (!$cekbundledetail) {
                    unset($rows_new[$kk1]);
                }
            }

            foreach ($rows_new as $kk2 => $vv2) {
                //  bundle detail
                $bundledetail = DB::table('bundle_detail')->where('bundle_header_id', $vv2['bundle_header_id'])->where('style_detail_id', $vv2['style_detail_id'])->where('start_no', $vv2['start_no'])->first();

                $rr2['bundle_header_id'] =  $vv2['bundle_header_id'];
                $rr2['style_detail_id'] = $vv2['style_detail_id'];
                $rr2['komponen_name'] = $vv2['komponen_name'];
                $rr2['start_no'] = $vv2['start_no'];
                $rr2['end_no'] = $bundledetail->end_no;
                $rr2['qty'] = $bundledetail->qty;
                $rr2['location'] = $vv2['location'];
                $rr2['sds_barcode'] = $vv2['sds_barcode'];
                $rr2['created_at'] = $vv2['created_at'];
                $rr2['updated_at'] = $vv2['updated_at'];
                $rr2['job'] = $vv2['job'];
                $rr2['season'] = $vv2['season'];
                $rr2['style'] = $vv2['style'];
                $rr2['set_type'] = $vv2['set_type'];
                $rr2['poreference'] = $vv2['poreference'];
                $rr2['article'] = $vv2['article'];
                $rr2['cut_num'] = $vv2['cut_num'];
                $rr2['size'] = $vv2['size'];
                $rr2['factory_id'] = $vv2['factory_id'];
                $rr2['cutting_date'] = $vv2['cutting_date'];
                $rr2['color_name'] = $vv2['color_name'];
                $rr2['cutter'] = $vv2['cutter'];
                $rr2['note'] = $vv2['note'];
                $rr2['admin'] = $vv2['admin'];
                $rr2['qc_operator'] = $vv2['qc_operator'];
                $rr2['bundle_operator'] = $vv2['bundle_operator'];
                $rr2['box_lim'] = $vv2['box_lim'];
                $rr2['max_bundle'] = $vv2['max_bundle'];
                $rr2['created_at'] = $vv2['created_at'];
                $rr2['updated_at'] = $vv2['updated_at'];
                $rr2['barcode_marker'] = $vv2['barcode_marker'];
                $rr2['job'] = $vv2['job'];
                $rr2['admin_name'] = $vv2['admin_name'];
                $rr2['qc_operator_name'] = $vv2['qc_operator_name'];
                $rr2['bundle_operator_name'] = $vv2['bundle_operator_name'];
                $rr2['statistical_date'] = $vv2['statistical_date'];
                $rr2['destination'] = $vv2['destination'];
                $rr2['part'] = $vv2['part'];
                $rr2['komponen'] = $vv2['komponen'];
                $rr2['total'] = $vv2['total'];
                $rr2['process'] = $vv2['process'];
                $rr2['process_name'] = $vv2['process_name'];
                $rr2['type_id'] = $vv2['type_id'];
                $rr2['type_name'] = $vv2['type_name'];
                $rr2['is_recycle'] = $bundledetail->is_recycle;

                $rows_new2[] = $rr2;
            }

            $list_komponen_arr = array();
            $list_sticker = array();
            $newkey = 0;
            $list_komponen_new = array();
            $sum_qty = 0;
            foreach ($rows_new2 as $key => $value) {
                $style = $value['type_name'] != 'Non' ? $value['style'].$value['type_name'] : $value['style'];

                $list_komponen_arr[$style.$value['poreference'].$value['size'].$value['start_no'].$value['end_no'].$value['cut_num']][$newkey] = $value;
                $list_sticker[$style.$value['poreference'].$value['size'].$value['start_no'].$value['end_no'].$value['cut_num'].$value['is_recycle']] = $value;
                $newkey++;
            }

            $chunk = array_chunk($list_komponen_arr,2);
            $chunk_new = array();
            foreach ($chunk as $key2 => $val2) {
                foreach ($val2 as $key3 => $val3) {
                        $chunk_new[] = $val3;
                }
            }

            $list_sticker_new = array();
            $keyy = 0;
            foreach ($list_sticker as $vals) {
                $list_sticker_new[$keyy] = $vals;
                $keyy++;
            }

            // count komponen
            $count_komponen = DB::table('master_style_detail')
                                    ->where('style', $master_style[0]->style)
                                    ->where('season', $master_style[0]->season)
                                    ->whereNull('deleted_at')
                                    ->count();

            $factories = DB::table('master_factory')
                            ->where('id', Auth::user()->factory_id)
                            ->whereNull('deleted_at')
                            ->first();
            $pdf = \PDF::loadView($load_view,[
                                'data' => $data,
                                'list_komponen_arr' => $list_komponen_arr,
                                'list_sticker_new' => $list_sticker_new,
                                'chunk_new' => $chunk_new,
                                'showbarcode' => true,
                                'data_plan' => $data_plan,
                                'count_komponen' => $count_komponen,
                                'factories' => $factories])
                            ->setPaper($papersize, $orientation);
            return $pdf->stream($filename);
    }
}
