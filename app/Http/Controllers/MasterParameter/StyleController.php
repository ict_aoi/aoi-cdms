<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use StdClass;
use DataTables;
use Carbon\Carbon;

class StyleController extends Controller
{
    //MASTER STYLE IE
    public function masterStyle()
    {
        $process = DB::table('master_process')
                    ->whereNull('deleted_at')
                    ->get();

        $komponen = DB::table('master_komponen')
                    ->whereNull('deleted_at')
                    ->get();

        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        $seasons = DB::table('master_seasons')
                    ->whereNull('deleted_at')
                    ->get();

        // $styles = DB::connection('erp_live')
        //             ->table('rma_style_fg')
        //             ->select(DB::raw('upc as style'))
        //             ->groupBy('upc')
        //             ->get();
        $styles = DB::table('master_list_style')
                    ->join('master_seasons', 'master_seasons.id', '=', 'master_list_style.season')
                    ->select(DB::raw("master_list_style.id, master_list_style.style_standart, master_list_style.set_type, master_list_style.season, master_seasons.season_name,
                    concat(master_list_style.style,CASE WHEN master_list_style.set_type = 'Non' THEN NULL ELSE master_list_style.set_type END,'-',master_seasons.season_name) as style"))
                    // ->groupBy('style_standart')
                    ->get();

        return view('master_parameter.style.index', compact('process', 'komponen', 'types', 'styles', 'seasons'));
    }

    public function ajaxGetDataMasterStyle(Request $request)
    {
        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'style') {

                if(empty($request->set_type)) {
                   $set_type = 'Non';
                }
                else {
                   $set_type = $request->set_type;
                }

            //    $params = DB::connection('erp_live')
            //                 ->table('rma_style_fg')
            //                 ->select(DB::raw('upc as style'))
            //                 ->where('upc', $style)
            //                 ->groupBy('upc')
            //                 ->get();
               $params = DB::table('master_list_style')
                            ->select(DB::raw('master_list_style.style_standart as style, master_list_style.season, master_seasons.season_name, master_list_style.set_type'))
                            ->join('master_seasons', 'master_seasons.id', '=', 'master_list_style.season')
                            ->where('master_list_style.style_standart', $request->style_standart)
                            ->where('master_list_style.season', $request->season)
                            ->where('master_list_style.set_type', $set_type)
                            // ->groupBy(['master_list_style.style_standart', 'master_list_style.season', 'master_seasons.season_name'])
                            ->get();
            }
           //
            else {

            }

            // get type_id
            // $get_types = DB::table('types')
            //                 ->where('type_name', $set_type)
            //                 ->whereNull('deleted_at')
            //                 ->first();

            // cek style exists
            $cekifstyleexist = DB::table('master_style')
                                    ->join('master_style_detail_ie', 'master_style_detail_ie.style', '=', 'master_style.style')
                                    ->join('types', 'types.id', '=', 'master_style_detail_ie.type_id')
                                    ->where('master_style.style', $request->style_standart)
                                    ->where('master_style_detail_ie.season', $request->season)
                                    ->where('types.type_name', $set_type)
                                    ->whereNull('master_style.deleted_at')
                                    ->whereNull('master_style_detail_ie.deleted_at')
                                    ->exists();



            if ($cekifstyleexist) {
                $dataxx = DB::table('v_master_style_ie')
                            ->where('style', $request->style_standart)
                            ->where('season', $request->season)
                            ->where('type_name', $set_type)
                            ->get();

                $data = json_decode(json_encode($dataxx), true);
            }else{

                $data = json_decode(json_encode($params), true);
            }

            return Datatables::of($data)
                    ->editColumn('season_name', function($data){
                        $season_name = isset($data['season_name']) ? $data['season_name'] : '-';

                        return $season_name;
                    })
                    ->editColumn('komponen_name', function($data){
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : '-';

                        return $komponen_name;
                    })
                    ->editColumn('type_name', function($data){
                        $type_name = isset($data['type_name']) ? $data['type_name'] : $data['set_type'];

                        return $type_name;
                    })
                    ->editColumn('action', function($data){

                        $style = $data['style'];
                        $season_name = isset($data['season_name']) ? $data['season_name'] : null;
                        $seasonid = isset($data['season']) ? $data['season'] : null;
                        $komponen = isset($data['komponen']) ? $data['komponen'] : null;
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : null;
                        $type_name = isset($data['type_name']) ? $data['type_name'] : $data['set_type'];

                        // get type_id
                        $get_types = DB::table('types')
                                        ->where('type_name', $type_name)
                                        ->whereNull('deleted_at')
                                        ->first();

                        $type_id = isset($data['type_id']) ? $data['type_id'] : $get_types->id;

                        $check = DB::table('v_master_style_ie')
                                    ->where('style', $style)
                                    ->where('season', $seasonid)
                                    ->where('type_name', $type_name)
                                    ->exists();

                        if(!$check) {

                                return view('_action', [
                                    'model'  => $data,
                                    'seasonid'  => $seasonid,
                                    'seasonname'  => $season_name,
                                    'type_name'  => $type_name,
                                    'type_id'  => $type_id,
                                    'upload_komponen' => $style
                                ]);

                        }
                        else {

                                return view('_action', [
                                    'model' => $data,
                                    'seasonid'  => $seasonid,
                                    'seasonname'  => $season_name,
                                    'upload_komponen' => $style,
                                    'style' => $style,
                                    'komponen' => $komponen,
                                    'type_id' => $type_id,
                                    'type_name' => $type_name,
                                    'edit_modal' => route('masterparam.editStyle',
                                                [
                                                    'style' => $style,
                                                    'komponen' => $komponen,
                                                    'type_id' => $type_id,
                                                    'type_name' => $type_name,
                                                    'season' => $seasonid
                                                ]),
                                    'delete_style' => route('masterparam.deleteStyle',
                                                [
                                                    'style' => $style,
                                                    'komponen' => $komponen,
                                                    'type_id' => $type_id,
                                                    'type_name' => $type_name,
                                                    'season' => $seasonid
                                                ])
                                ]);

                        }
                    })
                    ->rawColumns(['season_name', 'komponen', 'action'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                   ->editColumn('style', function($data) {
                       return null;
                   })
                   ->editColumn('season_name', function($data) {
                       return null;
                   })
                   ->editColumn('komponen', function($data) {
                       return null;
                   })
                   ->editColumn('type_name', function($data) {
                       return null;
                   })
                   ->editColumn('action', function($data) {
                       return null;
                   })
                   ->rawColumns(['season_name', 'komponen', 'action'])
                   ->make(true);
        }
    }

    //add new master style
    public function addStyle(Request $request)
    {

        $this->validate($request, [
            'modalstyle' => 'required',
            'season' => 'required',
            'komponen' => 'required'
        ]);

        $komponen = $request->komponen;

        // cek style & komponen
        $cekifexist = DB::table('master_style')
                        ->join('master_style_detail_ie', 'master_style_detail_ie.style', '=', 'master_style.style')
                        ->where('master_style_detail_ie.style', $request->modalstyle)
                        ->where('master_style_detail_ie.komponen', $komponen)
                        ->where('master_style_detail_ie.type_id', $request->type_id)
                        ->whereNull('master_style.deleted_at')
                        ->whereNull('master_style_detail_ie.deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('komponen & Type already exists..!', 422);
        }

        // $process = $request->process_id != 'null' ? $request->process_id : null;

        $data = array();

        $det['style'] = $request->modalstyle;
        $det['season'] = $request->season;
        $det['komponen'] = $komponen;
        $det['created_at'] = Carbon::now();
        $det['type_id'] = $request->type_id;
        $det['user_id'] = Auth::user()->id;

        $data[] = $det;

        try {
            DB::begintransaction();

            //insert table master style
            // cek if style exists
            $cekifstyleexist = DB::table('master_style')
                                    ->where('style', $request->modalstyle)
                                    ->whereNull('deleted_at')
                                    ->exists();

            if (!$cekifstyleexist) {

                DB::table('master_style')->insert([
                    'style' => $request->modalstyle,
                    'created_at' => Carbon::now()
                ]);
            }

            //insert table master style detail
            DB::table('master_style_detail_ie')->insert($data);

            // update master list style
            DB::table('master_list_style')
                    ->where('style_standart', $request->modalstyle)
                    ->update([
                        'updated_at' => Carbon::now()
                    ]);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master style
    public function editStyle(Request $request)
    {
        $style = $request->style;
        $komponen = $request->komponen;
        $type_id = $request->type_id;

        $data = DB::table('v_master_style_ie')
                    ->where('style', $style)
                    ->where('komponen', $komponen)
                    ->where('type_id', $type_id)
                    ->first();

        return response()->json($data, 200);
    }

    //update master style
    public function updateStyle(Request $request)
    {
        $data = $request->all();

        try {
            DB::beginTransaction();

                DB::table('master_style')
                    ->where('style', $data['styleUpdate'])
                    ->update([
                        'updated_at' => Carbon::now()
                    ]);

                $update = DB::table('master_style_detail_ie')
                            ->where('id', $data['idUpdate'])
                            ->update([
                                //'season' => $data['seasonUpdate'],
                                // 'process' => $data['process_id_update'] != 'null' ? $data['process_id_update'] : null,
                                'komponen' => $data['komponenUpdate'],
                                'type_id' => $data['type_id_update'],
                                'updated_at' => Carbon::now(),
                                'user_id' => Auth::user()->id
                            ]);

                // cek double
                if ($update) {
                    // cek style & komponen
                    $cekcount = DB::table('master_style')
                                ->join('master_style_detail_ie', 'master_style_detail_ie.style', '=', 'master_style.style')
                                ->where('master_style_detail_ie.style', $data['styleUpdate'])
                                ->where('master_style_detail_ie.komponen', $data['komponenUpdate'])
                                ->where('master_style_detail_ie.type_id', $data['type_id_update'])
                                ->whereNull('master_style.deleted_at')
                                ->whereNull('master_style_detail_ie.deleted_at')
                                ->count();

                    if ($cekcount > 1) {
                            return response()->json('error duplikat komponen & type..!', 422);
                            // throw new \Exception("error duplikat komponen..!");
                    }
                }

                // update master list style
                DB::table('master_list_style')
                    ->where('style_standart', $data['styleUpdate'])
                    ->update([
                        'updated_at' => Carbon::now()
                    ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('cutting.master.index');
    }

    //delete master style per komponen
    public function deleteStyle(Request $request)
    {
        $style = $request->style;
        $komponen = $request->komponen;
        $type_id = $request->type_id;
        $season = $request->season;

        try {
            DB::beginTransaction();

            DB::table('master_style')
                ->where('style')
                ->update([
                    'updated_at' => Carbon::now()
                ]);

            $delete = DB::table('master_style_detail_ie')
                        ->where('style', $style)
                        ->where('komponen', $komponen)
                        ->where('type_id', $type_id)
                        ->where('season', $season)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);

            // cek
            if ($delete) {

                $cekcount = DB::table('master_style_detail_ie')
                            ->where('style', $style)
                            ->whereNull('deleted_at')
                            ->count();

                $cekcount2 = DB::table('master_style_detail')
                            ->where('style', $style)
                            ->whereNull('deleted_at')
                            ->count();

                if ($cekcount <= 0 && $cekcount2 <= 0) {
                    DB::table('master_style')
                        ->where('style')
                        ->update([
                            'updated_at' => Carbon::now(),
                            'deleted_at' => Carbon::now()
                        ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //return view('cutting.master.index');
    }

    //END OF MASTER STYLE IE


    //MASTER STYLE PRODUCTION
    public function masterStyleProduction()
    {
        $process = DB::table('master_process')
                    ->whereNull('deleted_at')
                    ->get();

        $komponen = DB::table('master_komponen')
                    ->whereNull('deleted_at')
                    ->get();

        $types = DB::table('types')
                    ->whereNull('deleted_at')
                    ->get();

        $seasons = DB::table('master_seasons')
                    ->whereNull('deleted_at')
                    ->get();

        $styles = DB::connection('erp_live')
                    ->table('rma_style_fg')
                    ->select(DB::raw('upc as style'))
                    ->groupBy('upc')
                    ->get();

        return view('master_parameter.style.production.index', compact('process', 'komponen', 'types', 'styles', 'seasons'));
    }

    public function ajaxGetDataMasterStyleProduction(Request $request)
    {
        //api
        if($request->radio_status) {
            //filtering
            if($request->radio_status == 'style') {

                if(empty($request->style)) {
                   $style = 'null';
                }
                else {
                   $style = $request->style;
                }

               $params = DB::connection('erp_live')
                            ->table('rma_style_fg')
                            ->select(DB::raw('upc as style'))
                            ->where('upc', $style)
                            ->groupBy('upc')
                            ->get();
            }
           //
            else {

            }

            // cek style exists
            $cekifstyleexist = DB::table('master_style')
                                    ->join('master_style_detail', 'master_style_detail.style', '=', 'master_style.style')
                                    ->where('master_style.style', $style)
                                    ->whereNull('master_style.deleted_at')
                                    ->whereNull('master_style_detail.deleted_at')
                                    ->exists();

            if ($cekifstyleexist) {
                $dataxx = DB::table('v_master_style as vms')
                            ->leftJoin('master_style_detail as msd','msd.id','=','vms.id')
                            ->where('vms.style', $style)
                            ->get();
                $data = json_decode(json_encode($dataxx), true);
            }else{
                $data = json_decode(json_encode($params), true);
            }
            return Datatables::of($data)
                    ->editColumn('part', function($data){
                        $part = isset($data['part']) ? $data['part'] : '-';

                        return $part;
                    })
                    ->editColumn('season_name', function($data){
                        $season_name = isset($data['season_name']) ? $data['season_name'] : '-';

                        return $season_name;
                    })
                    ->editColumn('komponen_name', function($data){
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : '-';

                        return $komponen_name;
                    })
                    ->editColumn('process_name', function($data){
                        $process_name = isset($data['process_name']) ? $data['process_name'] : '-';

                        return $process_name;
                    })
                    ->editColumn('type_name', function($data){
                        $type_name = isset($data['type_name']) ? $data['type_name'] : '-';

                        return $type_name;
                    })
                    ->addColumn('is_ppa2', function($data){
                        if(isset($data['is_ppa2'])){
                            if($data['is_ppa2'] == true){
                                return '<span class="label label-success label-rounded"><i class="icon-checkmark2"></i></span>';
                            }else{
                                return '<span class="label label-default label-rounded"><i class="icon-cross3"></i></span>';
                            }
                        }else{
                            return '-';
                        }
                    })
                    ->editColumn('action', function($data){

                        $style = $data['style'];
                        $season_name = isset($data['season_name']) ? $data['season_name'] : null;
                        $komponen = isset($data['komponen']) ? $data['komponen'] : null;
                        $komponen_name = isset($data['komponen_name']) ? $data['komponen_name'] : null;

                        $id = isset($data['id']) ? $data['id'] : null;

                        $check = DB::table('v_master_style')
                                    ->where('style', $style)
                                    ->exists();

                        if(!$check) {

                                return view('_action', [
                                    'model'  => $data,
                                    'upload' => $style,
                                    'upload_sync' => $style
                                ]);

                        }
                        else {

                                return view('_action', [
                                    'model' => $data,
                                    'upload' => $style,
                                    'style' => $style,
                                    'komponen' => $komponen,
                                    'id' => $id,
                                    'edit_modal' => route('masterparam.editStyleProduction',
                                                [
                                                    'style' => $style,
                                                    'komponen' => $komponen,
                                                    'id' => $id
                                                ]),
                                    'delete_style' => route('masterparam.deleteStyleProduction',
                                                [
                                                    'style' => $style,
                                                    'komponen' => $komponen,
                                                    'id' => $id
                                                ]),
                                    'upload_sync' => $style
                                ]);

                        }
                    })
                    ->rawColumns(['season_name', 'komponen', 'action','is_ppa2'])
                    ->make(true);
        }
        else {
            $data = array();
            return Datatables::of($data)
                    ->editColumn('style', function($data) {
                        return null;
                    })
                    ->editColumn('part', function($data) {
                        return null;
                    })
                    ->editColumn('season_name', function($data) {
                        return null;
                    })
                    ->editColumn('komponen', function($data) {
                        return null;
                    })
                    ->editColumn('process_name', function($data) {
                        return null;
                    })
                    ->editColumn('type_name', function($data) {
                        return null;
                    })
                    ->editColumn('is_ppa2', function($data) {
                    return null;
                    })
                    ->editColumn('action', function($data) {
                        return null;
                    })
            ->rawColumns(['season_name', 'komponen', 'action'])
            ->make(true);
        }
    }

    //add new master style
    public function addStyleProduction(Request $request)
    {

        // $this->validate($request, [
        //     'modalstyle' => 'required',
        //     'season' => 'required',
        //     'komponen' => 'required',
        //     'part' => 'required'
        // ]);
        
        $komponen = $request->komponen;
        if($komponen == 'null' || $komponen == ''){
            return response()->json('Harap Pilih Nama Komponen Dahulu!', 422);
        }elseif($request->part == 'null' || $request->part == ''){
            return response()->json('Harap Pilih Part Dahulu!', 422);
        }elseif($request->season == 'null' || $request->season == ''){
            return response()->json('Harap Pilih Season Dahulu!', 422);
        }else{
            // cek style & komponen & season
            $cekifexist = DB::table('master_style')
                            ->join('master_style_detail', 'master_style_detail.style', '=', 'master_style.style')
                            ->where('master_style_detail.style', $request->modalstyle)
                            ->where('master_style_detail.komponen', $komponen)
                            ->where('master_style_detail.season', $request->season)
                            ->where('master_style_detail.type_id', $request->type_id)
                            ->whereNull('master_style.deleted_at')
                            ->whereNull('master_style_detail.deleted_at')
                            ->exists();
    
            if ($cekifexist) {
                return response()->json('komponen, season & set type already exists..!', 422);
            }
        }


        $process = $request->process_id != 'null' ? $request->process_id : null;

        $data = array();

        $det['style'] = $request->modalstyle;
        $det['season'] = $request->season;
        $det['komponen'] = $komponen;
        $det['created_at'] = Carbon::now();
        $det['type_id'] = $request->type_id;
        $det['user_id'] = Auth::user()->id;
        $det['factory_id'] = Auth::user()->factory_id;
        $det['part'] = $request->part;
        $det['is_ppa2'] = $request->is_ppa2;
        $det['process'] = $process;

        $data[] = $det;

        try {
            DB::begintransaction();

            //insert table master style
            // cek if style exists
            $cekifstyleexist = DB::table('master_style')
                                    ->where('style', $request->modalstyle)
                                    ->whereNull('deleted_at')
                                    ->exists();

            if (!$cekifstyleexist) {

                DB::table('master_style')->insert([
                    'style' => $request->modalstyle,
                    'created_at' => Carbon::now()
                ]);
            }

            //insert table master style detail
            DB::table('master_style_detail')->insert($data);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    //edit master style
    public function editStyleProduction(Request $request)
    {
        $style = $request->style;
        $komponen = $request->komponen;
        $id = $request->id;

        $datax = DB::table('v_master_style')
                    // ->where('style', $style)
                    // ->where('komponen', $komponen)
                    ->where('id', $id)
                    ->first();

        $ppa2 = DB::table('master_style_detail')->where('id',$id)->first();

        $data = new StdClass();
        $data->deleted_at = $datax->deleted_at;
        $data->id = $datax->id;
        $data->komponen = $datax->komponen;
        $data->komponen_name = $datax->komponen_name;
        $data->part = $datax->part;
        $data->process = $datax->process;
        $data->process_name = $datax->process_name;
        $data->season = $datax->season;
        $data->season_name = $datax->season_name;
        $data->style   = $datax->style;
        $data->total   = $datax->total;
        $data->type_id = $datax->type_id;
        $data->type_name = $datax->type_name;
        $data->is_ppa2 = $ppa2->is_ppa2;

        return response()->json($data,200);
    }

    //update master style
    public function updateStyleProduction(Request $request)
    {
        $data = $request->all();

        try {
            DB::beginTransaction();

                DB::table('master_style')
                    ->where('style', $data['styleUpdate'])
                    ->update([
                        'updated_at' => Carbon::now()
                    ]);

                $update = DB::table('master_style_detail')
                            ->where('style', $data['styleUpdate'])
                            ->where('komponen', $data['komponenUpdate'])
                            ->where('id', $data['idUpdate'])
                            ->update([
                                'part' => $data['partUpdate'],
                                'season' => $data['seasonUpdate'],
                                'process' => $data['process_id_update'] != 'null' ? $data['process_id_update'] : null,
                                'type_id' => $data['type_id_update'],
                                'updated_at' => Carbon::now(),
                                'user_id' => Auth::user()->id,
                                'is_ppa2' => $data['is_ppa2_update'] == 'true' ? true : false
                            ]);


                // cek style & komponen & season
                if ($update) {
                    $cekifexist = DB::table('master_style')
                                    ->join('master_style_detail', 'master_style_detail.style', '=', 'master_style.style')
                                    ->where('master_style_detail.style', $data['styleUpdate'])
                                    ->where('master_style_detail.komponen', $data['komponenUpdate'])
                                    ->where('master_style_detail.season', $data['seasonUpdate'])
                                    ->where('master_style_detail.type_id', $data['type_id_update'])
                                    ->whereNull('master_style.deleted_at')
                                    ->whereNull('master_style_detail.deleted_at')
                                    ->count();

                    if ($cekifexist > 1) {
                        throw new \Exception("error duplikat komponen, season & set type..!");
                    }
                }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        // return view('cutting.master.index');
    }

    //delete master style per komponen
    public function deleteStyleProduction(Request $request)
    {
        $style = $request->style;
        $komponen = $request->komponen;
        $id = $request->id;

        $cek_transaction = DB::table('bundle_detail')->where('style_detail_id',$id)->first();

        if($cek_transaction != null){
            return response()->json('Style Sudah Dibuat Ticket Bundle! Tidak Dapat Dihapus..', 422);
        }
        try {
            DB::beginTransaction();

            DB::table('master_style')
                ->where('style')
                ->update([
                    'updated_at' => Carbon::now()
                ]);

            DB::table('master_style_detail')
                ->where('style', $style)
                ->where('komponen', $komponen)
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now()
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //return view('cutting.master.index');
    }

    //END OF MASTER STYLE PRODUCTION


    // get json komponen
    public function ajaxGetDataKomponen(Request $request)
    {
        $data = DB::table('master_komponen')
                    ->select(DB::raw('id, komponen_name as text'))
                    ->where('komponen_name', 'like', '%'.strtoupper($request->term).'%')
                    ->whereNull('deleted_at')
                    ->get();

        return response()->json($data, 200);
    }

    // ajax get style
    public function ajaxGetDataGetListStyle(Request $request)
    {
        // $style_sync = $request->style_sync;

        $data = DB::table('master_style_detail')
                    ->select('master_style_detail.style', 'master_style_detail.season', 'master_style_detail.factory_id', 'master_seasons.season_name')
                    ->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail.season')
                    ->whereNull('master_style_detail.deleted_at')
                    ->whereNull('master_seasons.deleted_at')
                    ->groupBy('master_style_detail.style', 'master_style_detail.season', 'master_style_detail.factory_id', 'master_seasons.season_name')
                    ->get();

        return view('master_parameter.style.production.list_style')->with('data', $data);
    }

    // sync style
    public function styleSync(Request $request)
    {
        $this->validate($request, [
            'season' => 'required',
            'style' => 'required',
            'season_sync' => 'required',
            'style_sync' => 'required'
        ]);

        $style = $request->style;
        $season = $request->season;

        $style_sync = $request->style_sync;
        $season_sync = $request->season_sync;

        // cek style & season
        $cekifexist = DB::table('master_style')
                        ->join('master_style_detail', 'master_style_detail.style', '=', 'master_style.style')
                        ->where('master_style_detail.style', $style_sync)
                        ->where('master_style_detail.season', $season_sync)
                        ->whereNull('master_style.deleted_at')
                        ->whereNull('master_style_detail.deleted_at')
                        ->exists();

        if ($cekifexist) {
            return response()->json('style & season already exists..!', 422);
        }

        $data_style_detail = array();

        $get_style = DB::table('master_style_detail')
                            ->where('style', $style)
                            ->where('season', $season)
                            ->get();

        foreach ($get_style as $key => $val) {
            $detail['style'] = $style_sync;
            $detail['season'] = $season_sync;
            $detail['part'] = $val->part;
            $detail['komponen'] = $val->komponen;
            $detail['process'] = $val->process;
            $detail['total'] = $val->total;
            $detail['created_at'] = Carbon::now();
            $detail['type_id'] = $val->type_id;
            $detail['user_id'] = Auth::user()->id;
            $detail['factory_id'] = Auth::user()->factory_id;

            $data_style_detail[] = $detail;
        }

        try {
            DB::begintransaction();

            // cek style header
            $cekheader = DB::table('master_style')
                            ->where('style', $style_sync)
                            ->exists();

            if(!$cekheader){
                // insert style header
                DB::table('master_style')->insert(
                        [
                            'style' => $style_sync,
                            'created_at' => Carbon::now()
                        ]
                    );
            }else{
                // update style header
                DB::table('master_style')
                    ->where('style', $style_sync)
                    ->update(
                        [
                            'style' => $style_sync,
                            'updated_at' => Carbon::now()
                        ]
                    );

            }

            // insert style detail
            DB::table('master_style_detail')->insert($data_style_detail);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);
    }

    public function styleSyncSDS(Request $request)
    {
        if (request()->ajax()) {
            $style = $request->style;
            $season = $request->season;

            // cek style & season
            $check_style = DB::connection('sds_live')->table('art_desc_new')->where('style', $style)->where('season', $season)->get();

            if(count($check_style) > 0) {
                $check_style_ = DB::table('master_style_detail')->join('master_seasons', 'master_seasons.id', '=', 'master_style_detail.season')->whereNull('master_style_detail.deleted_at')->where('master_style_detail.style', $style)->where('master_seasons.season_name', $season)->get();

                if(count($check_style_) > 0) {
                    return response()->json('Style sudah di buat di CDMS', 422);
                } else {
                    try {
                        DB::begintransaction();

                        foreach($check_style as $data_style) {
                            $komponen_check = DB::table('master_komponen')->where('komponen_name', $data_style->part_name)->whereNull('deleted_at')->first();
                            if($komponen_check == null) {
                                $komponen_id = DB::table('master_komponen')->insertGetId([
                                    'komponen_name' => $data_style->part_name,
                                    'total' => 1,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ]);
                            } else {
                                $komponen_id = $komponen_check->id;
                            }

                            $process = $this->generateProcessSDS($data_style->note);

                            $style_check = DB::table('master_style')->where('style', $style)->whereNull('deleted_at')->first();

                            if($style_check == null) {
                                DB::table('master_style')->insert([
                                    'style' => $style,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ]);
                            }

                            $season_check = DB::table('master_seasons')->where('season_name', $season)->whereNull('deleted_at')->first();
                            if($season_check == null) {
                                return response()->json('Season Belum Terdaftar di CDMS', 422);
                            }

                            if($data_style->set_type == '0') {
                                $set_type = 'Non';
                            } else {
                                $set_type = $data_style->set_type;
                            }

                            $type_check = DB::table('types')->where('type_name', $set_type)->whereNull('deleted_at')->first();
                            if($type_check == null) {
                                return response()->json('Set Type Belum Terdaftar di CDMS', 422);
                            }

                            $style_insert = DB::table('master_style_detail')->insert([
                                'style' => $data_style->style,
                                'part' => $data_style->part_num,
                                'komponen' => $komponen_id,
                                'process' => $process,
                                'total' => 1,
                                'type_id' => $type_check->id,
                                'is_check_qc' => false,
                                'qc_name_komponen' => null,
                                'season' => $season_check->id,
                                'factory_id' => \Auth::user()->factory_id,
                                'user_id' => \Auth::user()->id,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ]);
                        }

                        DB::commit();
                    }
                    catch (Exception $e) {
                        DB::rollback();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
            } else {
                return response()->json('Style tidak Ditemukan Di SDS', 422);
            }

            return response()->json('success', 200);
        }
    }

    private function generateProcessSDS($process)
    {
        // base on database
        if($process == null) {
            return $process;
        } else {
            $process_ = array();
            $process = explode('+', preg_replace('/\s+/', '', $process));
            foreach($process as $aa) {
                switch ($aa) {
                    case "TEMPLATE":
                        $process_[] = '1';
                        break;
                    case "PRINT":
                        $process_[] = '2';
                        break;
                    case "HT":
                        $process_[] = '4';
                        break;
                    case "HE":
                        $process_[] = '4';
                        break;
                    case "PPA":
                        $process_[] = '5';
                        break;
                    case "EMBRO":
                        $process_[] = '6';
                        break;
                    case "AUTO":
                        $process_[] = '7';
                        break;
                    case "FUSE":
                        $process_[] = '9';
                        break;
                    case "PAD PRINT":
                        $process_[] = '14';
                        break;
                    default:
                }
            }

            $process_ = implode(',', $process_);
            return $process_;
        }
    }
}
