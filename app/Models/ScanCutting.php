<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class ScanCutting extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'scan_cuttings';
	protected $fillable = ['barcode_marker', 'barcode_table', 'start_time', 'end_time', 'state', 'deleted_at', 'factory_id', 'table_name', 'downtime', 'keterangan', 'downtime_category_id'];

	public function operator()
    {
        return $this->hasMany('App\Models\ScanCuttingOperator', 'scan_cutting_id', 'id')->where('deleted_at', null);
	}

	public function factory()
    {
        return $this->belongsTo('App\Models\Factory', 'factory_id', 'id');
	}
	
	public function marker()
    {
        return $this->belongsTo('App\Models\CuttingMarker', 'barcode_marker', 'barcode_id');
    }
}
