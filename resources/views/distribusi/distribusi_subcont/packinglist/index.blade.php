@extends('layouts.app',['active' => 'packinglist_distribusi'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Packinglist Distribusi - Subcont</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Packinglist Distribusi - Subcont</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="#" class="btn btn-default" data-toggle="modal" data-target="#insertPackinglistModal"><i class="icon-plus2 position-left"></i>Create New</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="packinglistTable" width="100%">
                <thead>
                    <tr>
                        <th>Packinglist</th>
                        <th>Subcont</th>
                        <th>Locator</th>
                        <th>Remark</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<a href="{{ route('packinglistDistribusi.getData') }}" id="getDataLink"></a>
<a href="{{ route('packinglistDistribusi.getNoPackinglist') }}" id="getNoPackinglistaLink"></a>
@endsection

@section('page-modal')
	@include('distribusi.distribusi_subcont.packinglist._insert_modal')
    @include('distribusi.distribusi_subcont.packinglist._update_modal')
@endsection

@section('page-js')
<script>
    $(document).ready( function (){
        $(".file-styled").uniform({
            fileButtonClass: 'action btn bg-warning'
        });

        $('#packinglistTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: $('#getDataLink').attr('href'),
            },
            columns: [
                {data: 'no_packinglist', name: 'no_packinglist',searchable:true,orderable:true},
                {data: 'subcont', name: 'subcont',searchable:true,orderable:true},
                {data: 'locator_name', name: 'locator_name',searchable:true,orderable:true},
                {data: 'remark', name: 'remark',searchable:true,orderable:true},
                {data: 'status', name: 'status',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        var dtable = $('#packinglistTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#subcont_insert').on('change', function(event) {
            var subcont = this.value;        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
            $.ajax({
                type: "POST",
                url: $('#getNoPackinglistaLink').attr('href'),
                data: {
                    subcont: subcont,
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('#no_packinglist_insert').val(response);
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });
    
        $('#form_insert').submit(function(event) {
            event.preventDefault();
            var no_kk = $('#no_kk_insert').val();
            var po_buyer = $('#po_buyer_insert').val();
            var style = $('#style').val();
            var no_packinglist = $('#no_packinglist_insert').val();
            var subcont = $('#subcont_insert').val();
            var locator = $('#locator_insert').val();
            console.log(no_kk);
            if(!no_kk){
                $("#alert_warning").trigger("click", 'No KK wajib diisi');
                return false;
            }
    
            if(!po_buyer){
                $("#alert_warning").trigger("click", 'PO buyer wajib diisi');
                return false;
            }
    
            if(!no_packinglist){
                $("#alert_warning").trigger("click", 'packinglist wajib diisi');
                return false;
            }
    
            if(!subcont){
                $("#alert_warning").trigger("click", 'subcont wajib dipilih');
                return false
            }

            if(!style){
                $("#alert_warning").trigger("click", 'style wajib dipilih');
                return false
            }
    
            if(!locator){
                $("#alert_warning").trigger("click", 'locator wajib dipilih');
                return false
            }
            
            $.ajax({
                type: "POST",
                url: $('#form_insert').attr('action'),
                data:new FormData($("#form_insert")[0]),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function () {
                    $('#packinglistTable').DataTable().ajax.reload();
                    $('#insertPackinglistModal').modal('hide');
                    $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    $('#insertPackinglistModal').modal();
                }
            });
        });

        $('#form_update').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var parameter = ['no_packinglist_insert_update', 'no_kk_insert_update', 'subcont_insert_update', 'remark_insert_update', 'po_buyer_insert_update','locator_insert_update','style_update'];
        var val = [
            $('#no_kk_insert_update').val(),
            $('#subcont_insert_update').val(),
            $('#no_packinglist_insert_update').val(),
            $('#po_buyer_insert_update').val(),
            $('#style_update').val(),
            $('#locator_insert_update').val(),
            $('#remark_insert_update').val(),
        ]
        console.log(formData);
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/packinglist-distribusi/update-data',
            data: new FormData($('#form_update')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function(response) {
                myalert('success','GOOD');
            },
            error: function(response) {
                myalert('error',response['responseJSON']);
            }
        });
    });
    });
    function hapus(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "delete",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
            },
            error: function () {
                $.unblockUI();
            }
        })
        .done(function () {
            $('#packinglistTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Data Berhasil dihapus');
        });
    }
    function edit(url) {
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                var po_buyer_insert = response.po_buyer;
                var style = response.style;
                var locator_insert = response.process;
                var no_kk_insert = response.no_kk;
                var subcont_insert = response.subcont;
                console.log(response);
                $.unblockUI();
                $('#no_packinglist_insert_update').val(response.no_packinglist);
                // $('#no_kk_insert_update').append("<option value='"+no_kk_insert+"' selected>"+response.no_kk+"</option>");
                $('#no_kk_insert_update').val(response.no_kk)
                $('#locator_insert_update').val(response.process)
                $('#style_update').append("<option value='"+style+"' selected>"+response.style+"</option>");
                $('#subcont_insert_update').append("<option value='"+subcont_insert+"' selected>"+response.subcont+"</option>");
                $('#remark_insert_update').val(response.remark);

                if(po_buyer_insert != null && po_buyer_insert != ''){
                $.each(po_buyer_insert.split(","), function(i, v){
                    $('#po_buyer_insert_update option[value='+ v +']').prop('selected', true).trigger('change');
                });
                }else{
                    $('#po_buyer_insert_update').val(null).trigger('change');
                // }
                // if(locator_insert != null && locator_insert != ''){
                // $.each(locator_insert.split(","), function(i, v){
                //     $('#locator_insert_update option[value='+ v +']').prop('selected', true).trigger('change');
                // });
                }

                $('#updatePackinglistModal').modal();
            },
            error: function (response) {
                $.unblockUI();
            }
        });
    }
    function release(url) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
            },
            error: function () {
                $.unblockUI();
            }
        })
        .done(function () {
            $('#packinglistTable').DataTable().ajax.reload();
            $("#alert_success").trigger("click", 'Document Released..!');
        });
    }
</script>
@endsection
