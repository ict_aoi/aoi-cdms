<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DockkPpcheader extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'doc_kk_ppc_header';
	protected $fillable = ['id','season','style','set_type','remark','total_qty','kk_no','kk_type','factory_id','created_by','created_at','updated_by','updated_at','deleted_at','deleted_by','document_no','document_type','quota_used','balanced_quota'];
}
