<style type="text/css">

    @page {
        margin: 0 20 0 20;
    }

    @font-face {
        font-family: 'impact' !important;
        src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
    }

    .template {
       font-size: 16 !important;
       //font-family: 'impact';
    }

    .page-break {
        page-break-after: always;
    }

    .separator {
        margin: 30 0 0 0;
    }

    .container {
        display: flex;
        flex-wrap: wrap;
    }

    .break {
        flex-basis: 100%;
        height: 0;
    }

    .print-friendly {
        //height: 30%;
        width: 100%;
        line-height: 0px;
        padding-top: 8px;
        //margin-left: -5px;
        //margin-right: -5px;
        font-size: 14px;
        border-right: thick solid #000;
        border-width: 2px;
        margin-right: 1em;
    }

    .print-friendly-single {
        height: 15%;
        width: 100%;
        //font-family: sans-serif;
        font-size: 14px !important;
        line-height: 6px;
        padding-top: 10px;
    }

    table.print-friendly tr td, table.print-friendly tr th,
    table.print-friendly-single tr td, table.print-friendly-single tr th, {
        page-break-inside: avoid;
        padding: 6.4px;
    }
    .barcode2 {
        float: left;
        margin-bottom: 5px;
        line-height: 16px;
        position:absolute;
        top: -18.5em;
        padding-left: 85px;
        font-family: 'impact';
    }
    .barcode {
        float: left;
        margin-bottom: 20px;
        line-height: 16px;
        position:absolute;
        top: -4em;
        padding-left: 200px;
        //margin-left: 230px;
        //transform: rotate(270deg);
        font-family: 'impact';
    }

    .img_barcode {
        display: block;
        padding: 0px;
    }

    .img_barcode > img {
        width: 130px;
        height: 30px;
    }
    /////
    .img_barcode2 {
        display: block;
        padding: 0px;
    }

    .img_barcode2 > img {
        width: 180px;
        height: 30px;
    }

    .barcode_number {
        font-family: sans-serif;
        font-size: 14px !important;
    }

    .area_barcode {
        width: 25%;
    }

    p{
        line-height: 0.5;
    }

    .verticalLine {
        border-right: thick solid #ff0000;
    }

    //
    div {
        margin:0;
        padding:0;
    }
    /*table {
        border-collapse:collapse;
        margin:0;
        padding:0;
    }*/

    table.outer {
        border:1px dotted #333;
    }
    table,table tr {
        padding:0;
        margin:0;
    }
    table tr td {
        /*border:1px solid #000;*/
        padding:0;
        margin:0;
    }
    td.main {
        padding:0;
        margin:0;
        width:100mm;/*100mm;*/
        height:74mm;/*74mm;*/
    }
    tr.bold td {
        font-weight:bold;
    }

    td.col-1 {
        width:8mm;
    }
    td.col-2 {
        width:23mm;
    }
    td.col-3 {
        width:20mm;
    }
    td.col-4 {
        width:15mm;
    }
    td.col-5 {
        width:34mm;
    }

    td.box-1 {
        width:10mm;
    }
    td.box-2 {
        width:10mm;
    }
    td.box-3 {
        width:8mm;
    }
    td.box-4 {
        width:76mm;
    }

    .smallfont {
        font-size:0.9em;
    }
    .evensmallerfont {
        font-size:0.6em;
    }

    .center {
        text-align:center;
    }
    .left {
        text-align:left;
    }
    .right {
        text-align:right;
    }

    td.bc-cell {
        position:relative;
        padding:2mm;
    }

    tr.box, tr td.box {
        font-weight:bold;
    }
    td.box-title {
        font-size:1.5em;
    }
    td.small-font {
        font-size:0.9em;
    }
    td.med-large-font {
        font-size:1.5em;
    }
    td.med-large-font-2 {
        font-size:1.3em;

    }
    table.box-table {
        font-family:Arial, Helvetica, sans-serif;
        font-size:11px;
    }
    table.box-table, table.box-table tr.content td{
        border:1px solid #333;
    }
    table.box-table tr.content td {
        padding:0 0.5em;
    }
    table.box-table tr.tr-title {
        border:1px solid #333;
        vertical-align: top;
    }
    table.box-table tr.box-header {
        vertical-align: bottom;
    }
    table.box-table tr td.bot-cel {
        border-bottom: 1pt solid #333;
    }
    //

    </style>

    @if(isset($data))
        @if(count($data) == 1)
        <table class="print-friendly" style="width: 50.8%; float: left; padding-left: -10px;">
        </table>
        @else
            @foreach($data as $key => $value)
            @php
                $proname = $value->process_name != null ? ' ('.$value->process_name.')' : '';

                $komponen_name = $value->komponen_name;
                $color_name = $value->color_name;

                if(strlen($komponen_name.$proname)>30){
                    $spn = '<span style="font-size: 9px;">'.$komponen_name.$proname;
                }else{
                    $spn = '<span style="font-size: 14px;">'.$komponen_name.$proname;
                }

                if(strlen($color_name)>30){
                    if(strlen($color_name)>45){
                        $spn_color = '<span style="font-size: 10px;">'.substr($color_name,0,45).' ..';
                    }else{
                        $spn_color = '<span style="font-size: 10px;">'.$color_name;
                    }
                }else{
                    $spn_color = '<span style="font-size: 14px;">'.$color_name;
                }

                $type_name = $value->type_name != 'Non' ? $value->type_name : '';
            @endphp
            <table class="print-friendly" style="width: 52.6%; float: left; padding-left: -10px; padding-right: 10px;">
                <tr>
                    <td style="width:4rem;"></td>
                    <td></td>
                    <td align="right"><b style="padding-right: 50px;">{{ \Carbon\Carbon::parse($data_plan->cutting_date)->format('d F') }}</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    {{-- <td align="right"><b style="padding-right: 65px;">{{ $value->factory_alias }}</b></td> --}}
                    @php
                        if($value->is_recycle == 'Y'){
                            echo '<td align="right"><b style="padding-right: 65px;">RECYC/'.$value->factory_alias.'</b></td>';
                        }
                        else{
                            echo '<td align="right"><b style="padding-right: 65px;">'.$value->factory_alias.'</b></td>';
                        }
                    @endphp
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width:4rem;"><b>CUT</b></td>
                    <td style="width: 4rem;" align=""><b>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $value->cut_num }}</b></b></td>
                    <td align="right"></td>
                </tr>
                <tr>
                    <td style="width:4rem;"><b>DELV</b></td>
                    <td style="width: 5.5rem;" align="left"><b>: {{ \Carbon\Carbon::parse($value->statistical_date)->format('d-M-y') }}</b></td>
                </tr>
                <tr>
                    <td><b>KOMPONEN</b></td>
                    <td colspan="2"><b>: @php echo $spn; @endphp </b></td>
                </tr>
                <tr>
                    <td><b>PART</b></td>
                    <td style="width: 5.5rem;"><b>: {{ $value->part }}</b></td>

                </tr>
                <tr>
                    <td>JOB</td>
                    <td colspan="2">: {{ $value->job }}</td>
                </tr>
                <tr>
                    <td>STYLE</td>
                    <td colspan="2">: {{ $value->style.$type_name }} ({{$value->season}})</td>
                </tr>
                <tr>
                    <td><b>PO/SO</b></td>
                    <td colspan="2"><b>: {{ $value->poreference.'/'.$value->destination }}</b></td>
                </tr>
                <tr>
                    <td>ARTICLE</td>
                    <td colspan="2">: {{ $value->article }}</td>
                </tr>
                <tr>
                    <td>COLL</td>
                    <td colspan="2">: @php echo $spn_color; @endphp</td>
                </tr>
                {{-- <tr>
                    <td>LOT</td>
                    <td colspan="2">: {{ $value->lot }}</td>
                </tr> --}}
                <tr>
                    <td>SIZE</td>
                    <td colspan="2">: {{ $value->size }}</td>
                </tr>
                <tr>
                    <td>QTY</td>
                    <td colspan="2">: {{ $value->qty }}</td>
                </tr>
                <tr>
                    <td>NO STICKER</td>
                    <td colspan="2">: {{ $value->start_no.' - '.$value->end_no }}</td>
                </tr>
                {{--  <tr>
                    <td>TGL</td>
                    <td>: {{ \Carbon\Carbon::parse($value->cutting_date)->format('d/m') }}</td>
                </tr>
                <tr>
                    <td>BUNDLE</td>
                    <td colspan="2">: {{ $value->bundle_operator_name }}</td>
                </tr>
                <tr>
                    <td>CUTTER</td>
                    <td colspan="2">: {{ $value->cutter }}</td>
                </tr>
                <tr>
                    <td>QC</td>
                    <td colspan="2">: {{ $value->qc_operator_name }}</td>
                </tr>  --}}
                {{--  <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                </tr>  --}}
                @if ($value->sds_barcode != null)
                <tr>
                    <td colspan="3">
                        <div class="barcode">
                            <div class="img_barcode">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->sds_barcode, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->sds_barcode : null }}</span >
                        </div>
                    </td>
                </tr>
                @else
                    <tr>
                        <td colspan="3">
                            <div class="barcode">
                                <div class="img_barcode">

                                </div>
                                <span class="barcode_number">{{ isset($value) ? $value->sds_barcode : null }}</span >
                            </div>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td colspan="3">
                        <div class="barcode2">
                            <div class="img_barcode2">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($value->barcode_id, 'C128') }}" alt="barcode"   />
                            </div>
                            <span class="barcode_number">{{ isset($value) ? $value->barcode_id : null }}</span >
                        </div>
                    </td>
                </tr>
            </table>
            @if(($key + 1) % 2 == 0 && $key != (count($data) - 1))
                {{--  <hr style="clear: left; margin: -0.1em; border-width: 0.1px;" />  --}}
                <hr style="clear: left; margin: -0.1em; border-width: 2px; margin-left: -300%;  margin-right: -300%;" />
            @endif
            @if($key == (count($data)-1))
                {{--  <div class="page-break"></div>  --}}
                <hr style="clear: left; margin: -0.1em; border-width: 2px; margin-left: -300%;  margin-right: -300%;" />
            @endif
            @endforeach

            @php
                $count=0;
                $i=0;
                $num=0;
                $margin = ($count_komponen*15)+134;

                $type_name = $list_sticker_new[$i]['type_name'] != 'Non' ? $list_sticker_new[$i]['type_name'] : '';
            @endphp
            @foreach ($chunk_new as $key1 => $item)

                <div style=" position: absolute;left: 0px;padding-top: 15px;margin-left: -10px; right: -5px; width: 100%;">
                    <table class="box-table" style="border-collapse:collapse;margin:0;padding:0;">
                        <tr class='tr-title'>
                            <td colspan='5' class='box small-font'>PT. APPAREL ONE INDONESIA</td>
                            <td colspan='5' class='box box-title'>CHECKLIST KOMPONEN</td>
                        </tr>
                        <tr>
                            <td colspan='10'>_</td>
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>STYLE</td>
                            <td colspan='2' class='box med-large-font bot-cel' width="9.5rem">:{{$list_sticker_new[$i]['style'].$type_name}}</td>
                            <td></td>
                            <td colspan='4' class="box med-large-font bot-cel">No. Stiker : {{$list_sticker_new[$i]['start_no'].'-'.$list_sticker_new[$i]['end_no']}}</td>
                            <td></td>
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>COLOUR</td>
                            <td colspan='2' class='box {{ strlen(trim($list_sticker_new[$i]['color_name'])) > 26 ? NULL : "med-large-font"}} bot-cel'>{{$list_sticker_new[$i]['color_name']}}</td>
                            <td></td>
                            <td colspan='4' class="box med-large-font bot-cel">
                                @php
                                    if($list_sticker_new[$i]['is_recycle'] == 'Y'){
                                        echo '<b>RECYCLE</b>';
                                    }
                                    else{
                                        echo '';
                                    }
                                @endphp
                            </td>
                            <td></td>
                            {{-- <td colspan='6'></td> --}}
                        </tr>
                        <tr class='box-header'>
                            <td colspan='2' class='box bot-cel'>PO</td>
                            <td colspan='3' class='box med-large-font bot-cel'>{{$list_sticker_new[$i]['poreference']}}/{{$list_sticker_new[$i]['destination']}}</td>
                            <td colspan='5'></td>
                        </tr>
                        <tr class='center content'>
                            <td colspan='4'></td>
                            <td colspan='3' class='box med-large-font-2'>CUTTING</td>
                            <td colspan='3' class='box med-large-font-2'>DISTRIBUSI</td>
                        </tr>
                        <tr class='center content box'>
                            <td class='box-1'>SIZE</td>
                            <td class='box-2'>CUT</td>
                            <td class='box-3'>QTY</td>
                            <td class='box-4'>KOMPONEN</td>
                            <td class='box-3'>JML</td>
                            <td class='box-1'>STATUS</td>
                            <td class='box-2'>KET</td>
                            <td class='box-3'>JML</td>
                            <td class='box-1'>STATUS</td>
                            <td class='box-2'>KET</td>
                        </tr>
                        @php
                            $j=0;
                        @endphp
                        @foreach ($item as $key2 => $item2)
                            @php
                                $process_name=trim($item2['process_name']);
                                $komponen_name=trim($item2['komponen_name']);
                                if($process_name=='' || $process_name==null){
                                    $comp=$komponen_name;
                                }else{
                                    $comp=$komponen_name." (".$process_name.")";
                                }
                                if(strlen($comp) > 27){
                                    $cls='smallfont';
                                }else{
                                    $cls='';
                                }
                            @endphp

                            <tr class="content">
                                <td class='box-1 center'>{{$item2['size']}}</td>
                                <td class='box-2 center'>{{$item2['cut_num']}}</td>
                                <td class='box-3 right'>{{$item2['qty']}}</td>
                                <td class='box-4 {{$cls}}'>{{$comp}}</td>
                                <td class='box-3'></td>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                                <td class='box-3'></td>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                            </tr>
                        @php
                            $j++;
                        @endphp
                        @endforeach
                        {{--  @for ($ii = 0; $ii < (25-$j); $ii++)
                            <tr class='content'>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                                <td class='box-3'></td>
                                <td class='box-4'>-</td>
                                <td class='box-3'></td>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                                <td class='box-3'></td>
                                <td class='box-1'></td>
                                <td class='box-2'></td>
                            </tr>
                        @endfor  --}}
                    </table>
                </div>
                @php
                    $i++;
                    $num++;
                 //   $margin = (($count_komponen-1)*11)+$plus;
                @endphp
                @if(($key1+1) % 1 == 0)
                {{--  <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>  --}}
                <br style="clear: left; line-height: {{$margin}}px; border-width: 0.1px;" />
                @endif
                @if(($key1 + 1) % ($factories->checklist_komponen_break/2) == 0)
                <div class="page-break"></div>
                @endif
                {{--  @if ($num==2)
                    <div class="page-break"></div>
                    @php

                    $num=0;
                    @endphp
                @endif  --}}

            @endforeach
        @endif
    @else
    <table style="width:100%;">
        <tr>
            <td colspan="8"></td>
            <td></td>
        </tr>
    </table>
    @endif
    </div>
