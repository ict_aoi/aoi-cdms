<script type="x-tmpl-mustache" id="defect_table">
	{% #item %}
		<tr>
			<td>
				{% nama_meja %}
			</td>
            <td>
				{% nama_defect %}
			</td>
			<td>
				{% qty %}
            </td>
			<td>
				{% ror %}
			</td>
			<td>
				{% measurement %}
			</td>
			<td>
				{% position %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-icon-anim btn-delete-item"><i class="icon-close2"></i></button>
			</td>
		</tr>
	{%/item%}
</script>
