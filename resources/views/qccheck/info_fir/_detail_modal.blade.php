<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Detail Hasil Lab Fabric</span>
				</legend>
			</div>
            <div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr class="ubah-bg">
								<th class="huruf-center huruf-tebal">Date Of</th>
								<th class="huruf-center huruf-tebal">No Roll</th>
								<th class="huruf-center huruf-tebal">Batch Number</th>
								<th class="huruf-center huruf-tebal">Actual Lot</th>
								<th class="huruf-center huruf-tebal">N.W (Kg s))</th>
								<th class="huruf-center huruf-tebal">Sticker Yds Qty</th>
								<th class="huruf-center huruf-tebal">Width On Barcode</th>
								<th class="huruf-center huruf-tebal">Yards Actual</th>
								<th class="huruf-center huruf-tebal">Yds Diff</th>
								<th class="huruf-center huruf-tebal">Cuttable Width</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="huruf-center"><span id="date_of_detail">No Data!</span></td>
								<td class="huruf-center"><span id="nomor_roll_detail">No Data!</span></td>
								<td class="huruf-center"><span id="batch_number_detail">No Data!</span></td>
								<td class="huruf-center"><span id="actual_lot_detail">No Data!</span></td>
								<td class="huruf-center"><span id="kg_detail">No Data!</span></td>
								<td class="huruf-center"><span id="sticker_yard_detail">No Data!</span></td>
								<td class="huruf-center"><span id="width_on_barcode_detail">No Data!</span></td>
								<td class="huruf-center"><span id="yard_actual_detail">No Data!</span></td>
								<td class="huruf-center"><span id="yard_diff_detail">No Data!</span></td>
								<td class="huruf-center"><span id="cuttable_width_detail">No Data!</span></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="table-responsive batas-atas">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr class="ubah-bg">
								<td colspan="20" class="huruf-center huruf-tebal">Defective Code</td>
							</tr>
							<tr class="ubah-bg">
								<th class="huruf-center huruf-tebal">A</th>
								<th class="huruf-center huruf-tebal">B</th>
								<th class="huruf-center huruf-tebal">C</th>
								<th class="huruf-center huruf-tebal">D</th>
								<th class="huruf-center huruf-tebal">E</th>
								<th class="huruf-center huruf-tebal">F</th>
								<th class="huruf-center huruf-tebal">G</th>
								<th class="huruf-center huruf-tebal">H</th>
								<th class="huruf-center huruf-tebal">I</th>
								<th class="huruf-center huruf-tebal">J</th>
								<th class="huruf-center huruf-tebal">K</th>
								<th class="huruf-center huruf-tebal">L</th>
								<th class="huruf-center huruf-tebal">M</th>
								<th class="huruf-center huruf-tebal">N</th>
								<th class="huruf-center huruf-tebal">O</th>
								<th class="huruf-center huruf-tebal">P</th>
								<th class="huruf-center huruf-tebal">Q</th>
								<th class="huruf-center huruf-tebal">R</th>
								<th class="huruf-center huruf-tebal">S</th>
								<th class="huruf-center huruf-tebal">T</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="huruf-center"><span id="defect_a_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_b_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_c_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_d_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_e_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_f_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_g_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_h_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_i_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_j_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_k_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_l_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_m_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_n_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_o_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_p_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_q_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_r_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_s_detail">No Data!</span></td>
								<td class="huruf-center"><span id="defect_t_detail">No Data!</span></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="table-responsive batas-atas">
					<table class="table table-basic table-condensed" id="detailTable">
						<thead>
							<tr class="ubah-bg">
								<td class="huruf-center huruf-tebal" colspan="2">Total</td>
								<td class="huruf-center huruf-tebal" colspan="2">Fabric Replacement Summary (Yards)</td>
								<td class="huruf-center huruf-tebal" rowspan="2">Created By</td>
								<td class="huruf-center huruf-tebal" rowspan="2">Confirm By</td>
								<td class="huruf-center huruf-tebal" rowspan="2">Final Result</td>
								<td class="huruf-center huruf-tebal" rowspan="2">Note</td>
							</tr>
							<tr class="ubah-bg">
								<th class="huruf-center huruf-tebal">Linear Point</th>
								<th class="huruf-center huruf-tebal">100SQ/YD</th>
								<th class="huruf-center huruf-tebal">Formula 1</th>
								<th class="huruf-center huruf-tebal">Formula 2</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="huruf-center"><span id="linier_point_detail">No Data!</span></td>
								<td class="huruf-center"><span id="sq_detail">No Data!</span></td>
								<td class="huruf-center"><span id="formula_1_detail">No Data!</span></td>
								<td class="huruf-center"><span id="formula_2_detail">No Data!</span></td>
								<td class="huruf-center"><span id="created_by_detail">No Data!</span></td>
								<td class="huruf-center"><span id="comfirm_detail">No Data!</span></td>
								<td class="huruf-center"><span id="final_result_detail">No Data!</span></td>
								<td class="huruf-center"><span id="note_detail">No Data!</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="table-responsive batas-atas">
				<table class="table table-basic table-condensed" id="detailTable">
					<tbody>
						<tr class="ubah-bg">
							<td colspan="8" class="huruf-tebal huruf-center">Keterangan</td>
						</tr>
						<tr>
							<td class="huruf-tebal">A</td>
							<td>: THICK YARN</td>
							<td class="huruf-tebal">F</td>
							<td>: KNOT</td>
							<td class="huruf-tebal">K</td>
							<td>: DYE SPOT</td>
							<td class="huruf-tebal">P</td>
							<td>: NEEDLE LINE</td>
						</tr>
						<tr>
							<td class="huruf-tebal">B</td>
							<td>: THIN YARN</td>
							<td class="huruf-tebal">G</td>
							<td>: CREASE MARK</td>
							<td class="huruf-tebal">L</td>
							<td>: STAIN</td>
							<td class="huruf-tebal">Q</td>
							<td>: UNEVEN DYE</td>
						</tr>
						<tr>
							<td class="huruf-tebal">C</td>
							<td>: HOLE / TEAR</td>
							<td class="huruf-tebal">H</td>
							<td>: STREAK VERTICAL</td>
							<td class="huruf-tebal">M</td>
							<td>: FOREIGN YARN</td>
							<td class="huruf-tebal">R</td>
							<td>: DUST</td>
						</tr>
						<tr>
							<td class="huruf-tebal">D</td>
							<td>: BROKEN YARN</td>
							<td class="huruf-tebal">I</td>
							<td>: SHADEBAR VERTICAL</td>
							<td class="huruf-tebal">N</td>
							<td>: DIRTY MARK</td>
							<td class="huruf-tebal">S</td>
							<td>: JOIN STICH</td>
						</tr>
						<tr>
							<td class="huruf-tebal">E</td>
							<td>: MISSING YARN</td>
							<td class="huruf-tebal">J</td>
							<td>: SHADEBAR HORIZONTAL</td>
							<td class="huruf-tebal">O</td>
							<td>: PRINTING DEFECT</td>
							<td class="huruf-tebal">T</td>
							<td>: SKEWING</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer batas-atas">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>