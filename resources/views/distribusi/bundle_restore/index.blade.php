@extends('layouts.app',['active' => 'bundle_restore'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }

    .change-bg-header {
        background-color: aqua;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Restoring Deleted Ticket Bundle</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Ticket Bundle Restoration</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_marker" name="barcode_marker" placeholder="MK-00000000000" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Marker" class="form-control bg-info" readonly="readonly">
                </div>
                @csrf
                <div class="col-md-2">
                    <button type="button" class="btn btn-success btn-md btn-icon icon-spinner9" id="restore_ticket"> RESTORE TICKET</button>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="panel-heading">
            <h5>MARKER -## <span id="detail_view_barcode_marker">-</span></h5>
        </div>
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="headerTable">
                <tbody>
                    <tr>
                        <td><strong>Style</strong></td>
                        <td><span id="detail_view_style">Data Empty!</span></td>
                        <td><strong>Cut</strong></td>
                        <td><span id="detail_view_cut">Data Empty!</span></td>
                        <td><strong>Item</strong></td>
                        <td colspan="3"><span id="detail_view_item">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Deliv</strong></td>
                        <td><span id="detail_view_deliv">Data Empty!</span></td>
                        <td><strong>Marker Width (inch)</strong></td>
                        <td><span id="detail_view_width">Data Empty!</span></td>
                        <td rowspan="2"><strong>Color</strong></td>
                        <td colspan="3"><span id="detail_view_color_code">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Article</strong></td>
                        <td><span id="detail_view_article">Data Empty!</span></td>
                        <td><strong>Marker Length (inch)</strong></td>
                        <td><span id="detail_view_length">Data Empty!</span></td>
                        <td colspan="3"><span id="detail_view_color_name">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Marker Pcs</strong></td>
                        <td><span id="detail_view_marker_pcs">Data Empty!</span></td>
                        <td><strong>Total Length (+allow)</strong></td>
                        <td><span id="detail_view_allow">Data Empty!</span></td>
                        <td><strong>Part</strong></td>
                        <td><span id="detail_view_part">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Total Grmt</strong></td>
                        <td><span id="detail_view_total_garment">Data Empty!</span></td>
                        <td><strong>Layer</strong></td>
                        <td><span id="detail_view_layer">Data Empty!</span></td>
                        <td><strong>Plan</strong></td>
                        <td><span id="detail_view_plan">Data Empty!</span></td>
                    </tr>
                    <tr>
                        <td><strong>Po Buyer</strong></td>
                        <td colspan="2"><span id="detail_view_po_buyer">Data Empty!</span></td>
                        <td><strong>Ratio</strong></td>
                        <td colspan="2"><span id="detail_view_ratio_size">Data Empty!</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="table-list">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Barcode Marker</th>
                        <th>Style</th>
                        <th>Size</th>
                        <th>CDMS Barcode</th>
                        <th>SDS Barcode</th>
                        <th>Komponen</th>
                        <th>Sticker</th>
                        <th>Qty</th>
                        <th>Last Locator</th>
                        <th>Admin Cutting</th>
                        <th>Deleted By</th>
                        <th>Deleted At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<a href="{{ route('bundle_restore.scanBarcode') }}" id="scanBarcodeLink"></a>
<a href="{{ route('bundle_restore.dataTicket') }}" id="scanBarcodeLink2"></a>
@endsection

@section('page-js')
<script>
    $(document).ready( function () {
        $('.input-new').keypress(function(event){
            if(event.which == 13) {
                event.preventDefault();
                var barcode_id = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: $('#scanBarcodeLink').attr('href'),
                    data: {
                        barcode_id: barcode_id,
                    },
                    beforeSend: function() {
                        $('.panel').block({
                            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: '10px 15px',
                                color: '#fff',
                                width: 'auto',
                                '-webkit-border-radius': 2,
                                '-moz-border-radius': 2,
                                backgroundColor: '#333'
                            }
                        });
                    },
                    success: function(response) {
                        $('.panel').unblock();
                        $('#detail_view_barcode_marker').text(response.data.barcode_id);
                        $('#detail_view_style').text(response.style);
                        $('#detail_view_cut').text(response.data.cut);
                        $('#detail_view_layer').text(response.layer);
                        $('#detail_view_length').text(response.data.marker_length);
                        $('#detail_view_allow').text(parseFloat(response.data.marker_length) + 0.75);
                        $('#detail_view_detail_view_width').text(parseFloat(response.data.fabric_width) - 0.5);
                        $('#detail_view_part').text(response.data.part_no);
                        $('#detail_view_plan').text(response.plan);
                        $('#detail_view_color_name').text(response.color_name);
                        $('#detail_view_color_code').text(response.color_code);
                        $('#detail_view_item').text(response.item_code);
                        $('#detail_view_deliv').text(response.stat_date);
                        $('#detail_view_width').text(response.marker_width);
                        $('#detail_view_article').text(response.article);
                        $('#detail_view_marker_pcs').text(response.ratio);
                        $('#detail_view_total_garment').text(response.total_garment);
                        $('#detail_view_ratio_size').text(response.ratio_size);
                        $('#detail_view_po_buyer').text(response.po_buyer);
                        $('#spreading_status').text(response.spreading_status);
                        $('#spreading_date').text(response.spreading_date);
                        $('#spreading_by').text(response.spreading_by);
                        $('#spreading_table').text(response.spreading_table);
                        $('#approve_gl_status').text(response.approve_gl_status);
                        $('#approve_gl_date').text(response.approve_gl_date);
                        $('#approve_gl_by').text(response.approve_gl_by);
                        $('#approve_qc_status').text(response.approve_qc_status);
                        $('#approve_qc_date').text(response.approve_qc_date);
                        $('#approve_qc_by').text(response.approve_qc_by);
                        $('#cutting_status').text(response.cutting_status);
                        $('#cutting_date').text(response.cutting_date);
                        $('#cutting_by').text(response.cutting_by);
                        $('#cutting_table').text(response.cutting_table);
                        $('#bundling_status').text(response.bundling_status);
                        $('#bundling_date').text(response.bundling_date);
                        $('#bundling_by').text(response.bundling_by);
                        $('#bundling_table').text(response.bundling_table);
                        $('#barcode_marker').val('');
                        $('#barcode_marker').focus();
                    },
                    error: function(response) {
                        $('.panel').unblock();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });

        $('.input-new').keypress(function(event)
        {
            if(event.which == 13) {
                event.preventDefault();
                var barcode_id = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var barcode_marker = $('#barcode_marker').val();
                var tableL = $('#table-list').DataTable({
                processing: true,
                serverSide: true,
                deferRender:true,
                scrollX:true,
                destroy: true,
                searching: false,
                dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
                },
                ajax: {
                    type: 'GET',
                    url: $('#scanBarcodeLink2').attr('href'),
                    data: {
                        barcode_marker: barcode_marker,
                    }
                },
                fnCreatedRow: function (row, data, index) {
                    var info = tableL.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},//0
                    {data: 'barcode_marker', name: 'barcode_marker',orderable:true},//1
                    {data: 'style', name: 'style',orderable:true},//2
                    {data: 'size', name: 'size',orderable:true},//3
                    {data: 'barcode_id', name: 'barcode_id',orderable:true},//4
                    {data: 'sds_barcode', name: 'sds_barcode',orderable:true},//5
                    {data: 'komponen_name', name: 'komponen_name',orderable:true},//6
                    {data: 'sticker', name: 'sticker',orderable:true},//7
                    {data: 'qty', name: 'qty',orderable:true},//8
                    {data: 'current_location', name: 'current_location',orderable:true},//9
                    {data: 'admin_name', name: 'admin_name',orderable:true},//10
                    {data: 'deleted_by', name: 'deleted_by',orderable:true},//10
                    {data: 'deleted_at', name: 'deleted_at',orderable:true},//10
                ]
                });
                tableL.draw();
            }
        });
        $('#restore_ticket').on('click',function(){
            var barcode_marker = $('#detail_view_barcode_marker').text();
            console.log(barcode_marker);
            if(barcode_marker == null){
                $("#alert_warning").trigger("click","Masukkan Barcode Marker Dahulu!");
            }else{
                bootbox.confirm("Apakah Anda Yakin Merestore Ticket Bundle Yang Sudah Dihapus..?", function (result) {
                if(result){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "post",
                        url: "{{ route('bundle_restore.restoreTicket') }}",
                        data: {
                            barcode_marker: barcode_marker,
                        },
                        beforeSend: function() {
                            $('.panel').block({
                                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: '10px 15px',
                                    color: '#fff',
                                    width: 'auto',
                                    '-webkit-border-radius': 2,
                                    '-moz-border-radius': 2,
                                    backgroundColor: '#333'
                                }
                            });
                        },
                        success: function (response) {
                            $('.panel').unblock();
                            $("#alert_success").trigger("click",'TICKET BUNDLE BERHASIL DI RESTORE!');
                            location.reload(true);
                        },
                        error: function(response) {
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                            location.reload(true);
                        }
                    });
                }
                else{
                    $('.modal').on('hidden.bs.modal', function (e) {
                    if($('.modal').hasClass('in')) {
                        $('body').addClass('modal-open');
                    }
                    });
                }
                });
            }
        });
    });
</script>
@endsection
