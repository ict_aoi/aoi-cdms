<?php

namespace App\Http\Controllers\BundleRemoval;

use DB;
use Uuid;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CuttingMarker;
use App\Models\Spreading\FabricUsed;
use App\Models\User;
use App\Models\ScanCutting;
use App\Models\TempTicketSdsRemove;
use Auth;

class BundleremovalController extends Controller
{
    public function index()
    {
        return view('distribusi.bundle_removal.index');
    }
    public function scanBarcode(Request $request)
    {
        if (request()->ajax())
        {
            $barcode_id = $request->barcode_id;
            $data = CuttingMarker::where('barcode_id', $barcode_id)->whereNotNull('marker_length')->whereNotNull('perimeter')->whereNotNull('fabric_width')->whereNull('deleted_at')->get()->first();
            $ticket_exists = DB::table('bundle_header')->where('barcode_marker',$barcode_id)->first();
            //TIDAK BOLEH DELETE LEBIH DARI 1X KARENA IMPACT KETIKA AKAN DI RESTORE!
            if($ticket_exists == null){
                return response()->json('Ticket Bundle Belum Pernah Dibuat!', 422);
            }elseif($data != null) {
                $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));

                $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

                $ratio = 0;
                foreach ($data->rasio_markers as $a) {
                    $ratio = $ratio + $a->ratio;
                }
                $total_garment = $ratio * $data->layer;

                $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

                $po_buyer = implode(', ', $po_buyer);

                $ratio_size = '';

                foreach ($data->rasio_markers as $aa) {
                    $ratio_size .= $aa->size . '/' . $aa->ratio . ', ';
                }

                $spreading_check = FabricUsed::where('barcode_marker', $data->barcode_id)->orderBy('created_spreading', 'desc')->get();

                if(count($spreading_check) > 0) {
                    $spreading_status = 'Done';
                    $spreading_date = Carbon::parse($spreading_check->first()->created_at)->format('d M Y / H:i');
                    $spreading_by = User::where('id', $spreading_check->first()->user_id)->first()->name;
                    $spreading_table = DB::table('master_table')->where('id_table', $spreading_check->first()->barcode_table)->first()->table_name;
                } else {
                    $spreading_status = '-';
                    $spreading_date = '-';
                    $spreading_by = '-';
                    $spreading_table = '-';
                }

                $approval_gl_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'GL Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_gl_check != null) {
                    $approve_gl_status = $approval_gl_check->status;
                    $approve_gl_date = Carbon::parse($approval_gl_check->created_at)->format('d M Y / H:i');
                    $approve_gl_by = User::where('id', $approval_gl_check->user_id)->first()->name;
                    $approve_gl_table = '-';
                } else {
                    $approve_gl_status = '-';
                    $approve_gl_date = '-';
                    $approve_gl_by = '-';
                    $approve_gl_table = '-';
                }

                $approval_qc_check = DB::table('spreading_approval')->where('id_marker', $data->barcode_id)->where('apv_type', 'QC Spreading')->orderBy('created_at', 'desc')->first();

                if($approval_qc_check != null) {
                    $approve_qc_status = $approval_qc_check->status;
                    $approve_qc_date = Carbon::parse($approval_qc_check->created_at)->format('d M Y / H:i');
                    $approve_qc_by = User::where('id', $approval_qc_check->user_id)->first()->name;
                    $approve_qc_table = '-';
                } else {
                    $approve_qc_status = '-';
                    $approve_qc_date = '-';
                    $approve_qc_by = '-';
                    $approve_qc_table = '-';
                }

                $cutting_check = ScanCutting::where('barcode_marker', $data->barcode_id)->where('state', 'done')->first();

                if($cutting_check != null) {
                    $get_user_array = $cutting_check->operator->pluck('user_id')->toArray();
                    $operator = User::whereIn('id', $get_user_array)->pluck('name')->toArray();
                    $cutting_status = 'Done';
                    $cutting_date = Carbon::parse($cutting_check->end_time)->format('d M Y / H:i');
                    $cutting_by = implode(', ', $operator);
                    $cutting_table = $cutting_check->table_name;
                } else {
                    $cutting_status = '-';
                    $cutting_date = '-';
                    $cutting_by = '-';
                    $cutting_table = '-';
                }

                $response = [
                    'data' => $data,
                    'layer' => $data->layer_plan.' ('.$data->layer.')',
                    'style' => $data->cutting_plan->style,
                    'article' => $data->cutting_plan->articleno,
                    'plan' => Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y'),
                    'color_name' => substr($get_data_cutting->color_name, 0, 15),
                    'ratio' => $ratio,
                    'total_garment' => $total_garment,
                    'item_code' => $get_data_cutting->material,
                    'color_code' => substr($get_data_cutting->color_code_raw_material, 0, 20),
                    'stat_date' => Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y'),
                    'marker_width' => $data->fabric_width - 0.5,
                    'po_buyer' => $po_buyer,
                    'ratio_size' => $ratio_size,
                    'ket' => null,
                    'spreading_status' => $spreading_status,
                    'approve_gl_status' => $approve_gl_status,
                    'approve_qc_status' => $approve_qc_status,
                    'cutting_status' => $cutting_status,
                    'bundling_status' => '-',
                    'spreading_date' => $spreading_date,
                    'approve_gl_date' => $approve_gl_date,
                    'approve_qc_date' => $approve_qc_date,
                    'cutting_date' => $cutting_date,
                    'bundling_date' => '-',
                    'spreading_by' => $spreading_by,
                    'approve_gl_by' => $approve_gl_by,
                    'approve_qc_by' => $approve_qc_by,
                    'cutting_by' => $cutting_by,
                    'bundling_by' => '-',
                    'spreading_table' => $spreading_table,
                    'cutting_table' => $cutting_table,
                    'bundling_table' => '-',
                ];
                return response()->json($response,200);
            } else {
                return response()->json('marker tidak ditemukan', 422);
            }
        }
    }
    public function dataTicket(Request $request)
    {
        if ($request->ajax()){
            $barcode_id = $request->barcode_marker;
            $data = DB::table('dd_bundle_removal')
            ->where('barcode_marker',$barcode_id)
            ->get();
            
            return DataTables::of($data)
            ->addColumn('sticker',function ($data)
            {
                return $data->start_no.'-'.$data->end_no;
            })
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->rawColumns(['style','sticker'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function deleteTicket(Request $request){
        if (request()->ajax()) {
            $barcode_marker = $request->barcode_marker;
            if($barcode_marker != null){
                $barcode_id_cdms = DB::table('bundle_detail as bd')->Join('bundle_header as bh','bh.id','=','bd.bundle_header_id')->where('bh.barcode_marker',$barcode_marker)->pluck('barcode_id')->toArray();
                $temp_scan_artwork1 = DB::table('detail_packinglist_subtemp')->whereIn('bundle_id',$barcode_id_cdms)->get();
                $temp_scan_artwork2 = DB::table('detail_packinglist_distribusi')->whereIn('bundle_id',$barcode_id_cdms)->get();
                if(count($temp_scan_artwork1) > 0 || count($temp_scan_artwork2) > 0){
                    return response()->json('Barcode Dalam Proses Artwork!', 422);
                }
                try{
                    DB::beginTransaction();
                    DB::table('deleted_bundle_flag')->insert([
                        'id'  => Uuid::generate()->string,
                        'barcode_marker' => $barcode_marker,
                        'created_at' => Carbon::now(),
                        'created_by' => \Auth::user()->id,
                        'flag' => 1
                    ]);
                    $deleted_bundle_flag_id = DB::table('deleted_bundle_flag')->where('barcode_marker',$barcode_marker)->first();
                    $get_bundle_header_id = DB::table('bundle_header')->where('barcode_marker',$barcode_marker)->where('factory_id',\Auth::user()->factory_id)->get();
                    foreach($get_bundle_header_id as $key){
                        DB::table('deleted_bundle_header')->insert([
                            'id'                    => $key->id,
                            'season'                => $key->season,
                            'style'                 => $key->style,
                            'set_type'              => $key->set_type,
                            'poreference'           => $key->poreference,
                            'article'               => $key->article,
                            'cut_num'               => $key->cut_num,
                            'size'                  => $key->size,
                            'factory_id'            => $key->factory_id,
                            'cutting_date'          => $key->cutting_date,
                            'color_name'            => $key->color_name,
                            'cutter'                => $key->cutter,
                            'note'                  => $key->note,
                            'admin'                 => $key->admin,
                            'qc_operator'           => $key->qc_operator,
                            'bundle_operator'       => $key->bundle_operator,
                            'box_lim'               => $key->box_lim,
                            'max_bundle'            => $key->max_bundle,
                            'created_at'            => $key->created_at,
                            'updated_at'            => $key->updated_at,
                            'qty'                   => $key->qty,
                            'barcode_marker'        => $key->barcode_marker,
                            'job'                   => $key->job,
                            'admin_name'            => $key->admin_name,
                            'qc_operator_name'      => $key->qc_operator_name,
                            'bundle_operator_name'  => $key->bundle_operator_name,
                            'statistical_date'      => $key->statistical_date,
                            'destination'           => $key->destination,
                            'po_reroute'            => $key->po_reroute,
                            'job_reroute'           => $key->job_reroute,
                            'deleted_by'            => \Auth::user()->id,
                            'deleted_at'            => Carbon::now(),
                            'deleted_bundle_flag_id' => $deleted_bundle_flag_id->id
                        ]);
                    }
                    $bundle_header_id_arr = DB::table('bundle_header')->where('barcode_marker',$barcode_marker)->pluck('id')->toArray();
                    $get_bundle_detail = DB::table('bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->get();
                    foreach($get_bundle_detail as $x){
                        DB::table('deleted_bundle_detail')->insert([
                            'barcode_id'            => $x->barcode_id,
                            'bundle_header_id'      => $x->bundle_header_id,
                            'style_detail_id'       => $x->style_detail_id,
                            'komponen_name'         => $x->komponen_name,
                            'start_no'              => $x->start_no,
                            'end_no'                => $x->end_no,
                            'qty'                   => $x->qty,
                            'location'              => $x->location,
                            'factory_id'            => $x->factory_id,
                            'sds_barcode'           => $x->sds_barcode,
                            'created_at'            => $x->created_at,
                            'updated_at'            => $x->updated_at,
                            'job'                   => $x->job,
                            'is_recycle'            => $x->is_recycle,
                            'is_out_cutting'        => $x->is_out_cutting,
                            'out_cutting_at'        => $x->out_cutting_at,
                            'is_out_setting'        => $x->is_out_setting,
                            'out_setting_at'        => $x->out_setting_at,
                            'job_reroute'           => $x->job_reroute,
                            'current_locator_id'    => $x->current_locator_id,
                            'current_status_to'     => $x->current_status_to,
                            'current_description'   => $x->current_description,
                            'updated_movement_at'   => $x->updated_movement_at,
                            'current_user_id'       => $x->current_user_id,
                            'deleted_by'            => \Auth::user()->id,
                            'deleted_at'            => Carbon::now()
                        ]);
                        $barcode_pl_dist = DB::table('detail_packinglist_distribusi')->where('bundle_id',$x->barcode_id)->first();

                        if($barcode_pl_dist != null){
                            DB::table('deleted_barcode_pl_distribusi')->insert([
                                'id'                    => $barcode_pl_dist->id,
                                'id_packinglist'        => $barcode_pl_dist->id_packinglist,
                                'bundle_id'             => $barcode_pl_dist->bundle_id,
                                'qty'                   => $barcode_pl_dist->qty,
                                'created_at'            => $barcode_pl_dist->created_at,
                                'deleted_at'            => Carbon::now(),
                                'deleted_by'            => \Auth::user()->id,
                                'user_id'               => $barcode_pl_dist->user_id,
                                'factory_id'            => $barcode_pl_dist->factory_id,
                                'style'                 => $barcode_pl_dist->style,
                                'article'               => $barcode_pl_dist->article,
                                'poreference'           => $barcode_pl_dist->poreference,
                                'komponen_name'         => $barcode_pl_dist->komponen_name,
                                'color_name'            => $barcode_pl_dist->color_name,
                                'set_type'              => $barcode_pl_dist->set_type,
                                'size'                  => $barcode_pl_dist->size
                            ]);
                            DB::table('detail_packinglist_distribusi')->where('bundle_id',$x->barcode_id)->delete();
                        }
                    }
                    $list_barcode = DB::table('bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->pluck('barcode_id')->toArray();
                    $data_movements = DB::table('distribusi_movements')->whereIn('barcode_id',$list_barcode)->get();
                    foreach($data_movements as $xx){
                        DB::table('deleted_distribusi_movements')->insert([
                            'id'                => $xx->id,
                            'barcode_id'        => $xx->barcode_id,
                            'locator_from'      => $xx->locator_from,
                            'status_from'       => $xx->status_from,
                            'locator_to'        => $xx->locator_to,
                            'status_to'         => $xx->status_to,
                            'description'       => $xx->description,
                            'user_id'           => $xx->user_id,
                            'ip_address'        => $xx->ip_address,
                            'created_at'        => $xx->created_at,
                            'updated_at'        => $xx->updated_at,
                            'deleted_at'        => Carbon::now(),
                            'loc_dist'          => $xx->loc_dist,
                            'bundle_table'      => $xx->bundle_table,
                            'deleted_by'        => \Auth::user()->id
                        ]);
                    }
                    DB::table('bundle_header')->where('barcode_marker',$barcode_marker)->delete();
                    DB::table('bundle_detail')->whereIn('bundle_header_id',$bundle_header_id_arr)->delete();
                    DB::table('distribusi_movements')->whereIn('barcode_id',$list_barcode)->delete();
                    DB::table('cutting_marker')->where('barcode_id',$barcode_marker)->update([
                        'is_bundling'       => false 
                    ]);
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }else{
                return response()->json('Harap Input Barcode Marker!', 422);
            }
        }
    }

    public function addParameter(Request $request){
        if($request->ajax()){
            $data = $request->all();
            $param = [
                'poreference'    => $data['poreference'],
                'cut_num'        => $data['cut_num'],
                'article'        => $data['article'],
                'style'          => $data['style_num'],
                'part_name'      => $data['part_name'],
                'size'           => count($data['size']) > 1 ? "'".implode("','",$data['size'])."'" : "'".$data['size'][0]."'",
                'start_num'      => $data['start_num'],
                'end_num'        => $data['end_num']
            ];
            $param = array_filter($param);
            $x=[];
            $y=[];
            foreach($param as $p => $key){
                $x[] = $p;
                $y[] = $key;
            }
            $z1 = $x;
            $z2 = $y;
            $params = [];
            for($i=0;$i < count($z1);$i++){
                $params[] = $z1[$i] == 'size' ? $z1[$i].' in ('.$z2[$i].')' : $z1[$i].'='."'".$z2[$i]."'";
            }
            $where_clause = implode(' and ',$params);
            $data = DB::connection('sds_live')->select(DB::raw("SELECT * FROM dd_bundle_sds_removal WHERE $where_clause"));
            if(count($data) > 0){
                foreach($data as $d){
                    $check_art = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$d->barcode_id)->whereNotNull('artwork')->first();
                    $info_bundle = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$d->barcode_id)->first();
                    $check_exists = TempTicketSdsRemove::where('barcode_id',$d->barcode_id)->first();
                    if($info_bundle == null){
                        return response()->json('BARCODE '.$info_bundle->barcode_id.' TIDAK DI TEMUKAN / SUDAH DIHAPUS!',422);
                    }
                    if($info_bundle->supply != null){
                        return response()->json('BARCODE '.$info_bundle->barcode_id.' KOMPONEN '.$info_bundle->part_name.' CUT '.$info_bundle->cut_num.' STICKER '.$info_bundle->start_num.'-'.$info_bundle->end_num.' SUDAH DI TERIMA LINE '.$info_bundle->supplied_to,422);
                    }
                    if($check_exists != null){
                        return response()->json('PO '.$check_exists->poreference.' STYLE '.$check_exists->style.' CUT '.$check_exists->cut_num.' SEDANG DI PROSES HAPUS OLEH '.$check_exists->user->name,422);
                    }
                    if($check_art){
                        return response()->json('STYLE '.$d->style.' KOMPONEN '.$d->part_name.' TERDAPAT PROSES ARTWORK!',422);
                    }
                    $check_added = TempTicketSdsRemove::where('barcode_id',$d->barcode_id)->exists();
                    if($check_added){
                        return response()->json('PO '.$d->poreference.' STYLE '.$d->style.' KOMPONEN '.$d->part_name.' STICKER '.$d->start_num.'-'.$d->end_num.' CUT '.$d->cut_num.' SUDAH DITAMBAHAKAN',422);
                    }
                }
                foreach($data as $dt){
                    try{
                        DB::beginTransaction();
                            TempTicketSdsRemove::firstOrCreate([
                                'barcode_id'        => $dt->barcode_id,
                                'season'            => $dt->season,
                                'style'             => $dt->style,
                                'poreference'       => $dt->poreference,
                                'article'           => $dt->article,
                                'cut_num'           => $dt->cut_num,
                                'size'              => $dt->size,
                                'part_num'          => $dt->part_num,
                                'part_name'         => $dt->part_name,
                                'start_num'         => $dt->start_num,
                                'end_num'           => $dt->end_num,
                                'qty'               => $dt->qty,
                                'factory_id'        => $dt->processing_fact,
                                'created_by'        => Auth::user()->id,
                                'note'              => $dt->note
                            ]);
                        DB::commit();
                    }catch (Exception $e) {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
            }else{
                return response()->json('DATA TIDAK DI TEMUKAN!',422);
            }
            
        }
    }

    public function getDataTicket(Request $request){
        if($request->ajax()){
            $user_id = $request->user_id;
            $data = DB::table('temp_ticket_sds_remove')->where('created_by',$user_id)->where('factory_id',Auth::user()->factory_id)->get();
            return DataTables::of($data)
            ->editColumn('cut_num',function($data){
                $cut_sticker = $data->cut_num.'|'.$data->start_num.'-'.$data->end_num;
                return $cut_sticker;
            })
            ->editColumn('part_name',function($data){
                $part_name = $data->part_num.'|'.$data->part_name;
                return $part_name;
            })
            ->addColumn('action', function($data){
                if($data->id != null){
                    return '<a href="'.route('bundle_removal.deleteRowDetail', $data->id).'" data-id="'.$data->id.'" class="ignore-click delete btn btn-danger btn-raised btn-xs"><i class="icon-trash"></i></a>';
                }
            })
            ->rawColumns(['action','cut_num','part_name'])
            ->make(true);
        }else{
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function cancelDelete(Request $request){
        $user = $request->user;
        TempTicketSdsRemove::where('created_by',$user)->where('factory_id',Auth::user()->factory_id)->delete();
        return response()->json(200);
    }

    public function ajaxGetDataPobuyer(Request $request)
    {
        $term = trim(strtoupper($request->q));
        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DB::table('data_cuttings')
        ->select('po_buyer as poreference')
        ->where('po_buyer',$term)
        ->orWhere('job_no','like','%'.$term.'%')
        ->distinct('po_buyer')
        ->limit(3)
        ->get();
        $formatted_tags = [];
        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->poreference, 
                'text' => $tag->poreference
            ];
        }
        return \Response::json($formatted_tags);
    }

    public function ajaxGetDataSize(Request $request)
    {
        if($request->ajax()){
            $po = $request['poreference'];
            $data = DB::table('data_cuttings')
            ->select('size_finish_good as size')
            ->where('po_buyer',$po)
            ->distinct()
            ->get();
            return response()->json(['response' => $data],200);
        }
    }

    public function ajaxGetDataStyle(Request $request)
    {
        if($request->ajax()){
            $po = $request['poreference'];
            $po_info = DB::table('data_cuttings')->where('po_buyer',$po)->orWhere('job_no','like','%'.$po.'%')->first();
            if($po_info != null){
                if (strpos($po_info->style,'TOP') > 0 || strpos($po_info->style,'BOT') > 0){
                    $style_temp = explode('-',$po_info->style);
                    $style      = $style_temp[0];
                }else{
                    $style      = $po_info->style;
                }
                $data = DB::select(DB::raw("SELECT DISTINCT style,case when type_name = 'Non' then '0' else type_name end as set_type FROM v_master_style WHERE style = '$style' AND season_name='$po_info->season'"));
            }else{
                $data = [];
            }
            return response()->json(['response' => $data],200);
        }
    }

    public function ajaxGetDataCut(Request $request)
    {
        if($request->ajax()){
            $poreference = $request['poreference'];
            $factory_id = Auth::user()->factory_id;

            $data = DB::select(DB::raw("SELECT x.cut_num FROM (SELECT DISTINCT
            bh.cut_num,length(bh.cut_num) as cut_order
            FROM bundle_header bh
            WHERE bh.poreference='$poreference'
            AND bh.factory_id='$factory_id'
            UNION 
            SELECT DISTINCT
            shm.cut_num,length(shm.cut_num) as cut_order
            FROM sds_header_movements shm
            WHERE shm.poreference='$poreference'
            AND shm.factory_id='$factory_id')x ORDER BY x.cut_order,x.cut_num"));
            
            return response()->json(['response' => $data],200);
        }
    }

    public function ajaxGetDataArticle(Request $request)
    {
        if($request->ajax()){
            $poreference = $request['poreference'];
            $factory_id = Auth::user()->factory_id;

            $data = DB::select(DB::raw("SELECT x.article FROM (SELECT DISTINCT
            bh.article,length(bh.article) as article_order
            FROM bundle_header bh
            WHERE bh.poreference='$poreference'
            AND bh.factory_id='$factory_id'
            UNION 
            SELECT DISTINCT
            shm.article,length(shm.article) as article_order
            FROM sds_header_movements shm
            WHERE shm.poreference='$poreference'
            AND shm.factory_id='$factory_id')x ORDER BY x.article_order,x.article"));
            
            return response()->json(['response' => $data],200);
        }
    }

    public function ajaxGetDataPartname(Request $request)
    {
        if($request->ajax()){
            $po = $request['poreference'];
            $po_info = DB::table('data_cuttings')->where('po_buyer',$po)->orWhere('job_no','like','%'.$po.'%')->first();
            if($po_info != null){
                if(strpos($po_info->style,'TOP') > 0 || strpos($po_info->style,'BOT') > 0){
                    $style_temp = explode('-',$po_info->style);
                    $style      = $style_temp[0];
                }else{
                    $style      = $po_info->style;
                }
                $data = DB::table('v_master_style')->select('komponen_name as part_name')->where('style',$style)->where('season_name',$po_info->season)->get();
            }else{
                $data = [];
            }
            return response()->json(['response' => $data],200);
        }
    }

    public function deleteTicketSds(Request $request){
        if($request->ajax()){
            $user = $request['user'];
            $data_ticket = TempTicketSdsRemove::where('created_by',$user)->where('factory_id',Auth::user()->factory_id)->get();
            if(count($data_ticket) > 0){
                foreach($data_ticket as $dt){
                    $check_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$dt->barcode_id)->first();
                    $sync_cdms = DB::table('bundle_detail')->where('sds_barcode',$dt->barcode_id)->exists();
                    if($sync_cdms){
                        return response()->json('Harap Hapus Data Ticket CDMS Dahulu!',422);
                    }
                    if($check_sds == null){
                        return response()->json('BARCODE '.$dt->barcode_id.' TIDAK DITEMUKAN',422);
                    }
                }

                foreach($data_ticket as $dtc){
                    $check_sds_cdms = DB::table('sds_detail_movements')->where('sds_barcode',$dtc->barcode_id)->first();
                    if($check_sds_cdms != null){
                        try{
                            DB::beginTransaction();
                                DB::table('sds_header_movements')->where('id',$check_sds_cdms->sds_header_id)->delete();
                                DB::table('sds_detail_movements')->where('sds_barcode',$dtc->barcode_id)->delete();
                                DB::table('distribusi_movements')->where('barcode_id',$dtc->barcode_id)->delete();
                            DB::commit();
                        }catch (Exception $e) {
                            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                    try{
                        DB::beginTransaction();
                            $db_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$dtc->barcode_id)->first();
                            DB::connection('sds_live')->table('deleted_bundle_info_new')->insert([
                                'barcode_id'        => $db_sds->barcode_id,
                                'season'            => $db_sds->season,
                                'style'             => $db_sds->style,
                                'poreference'       => $db_sds->poreference,
                                'article'           => $db_sds->article,
                                'cut_num'           => $db_sds->cut_num,
                                'size'              => $db_sds->size,
                                'part_num'          => $db_sds->part_num,
                                'part_name'         => $db_sds->part_name,
                                'part_ids'          => $db_sds->part_ids,
                                'note'              => $db_sds->note,
                                'start_num'         => $db_sds->start_num,
                                'end_num'           => $db_sds->end_num,
                                'qty'               => $db_sds->qty,
                                'location'          => $db_sds->location,
                                'factory'           => $db_sds->factory,
                                'processing_fact'   => $db_sds->processing_fact,
                                'create_date'       => $db_sds->create_date,
                                'distrib_received'  => $db_sds->distrib_received,
                                'bd_pic'            => $db_sds->bd_pic,
                                'artwork'           => $db_sds->artwork,
                                'artwork_pic'       => $db_sds->artwork_pic,
                                'artwork_out'       => $db_sds->artwork_out,
                                'artwork_out_pic'   => $db_sds->artwork_out_pic,
                                'he'                => $db_sds->he,
                                'he_pic'            => $db_sds->he_pic,
                                'he_out'            => $db_sds->he_out,
                                'he_out_pic'        => $db_sds->he_out_pic,
                                'pad'               => $db_sds->pad,
                                'pad_pic'           => $db_sds->pad_pic,
                                'pad_out'           => $db_sds->pad_out,
                                'pad_out_pic'       => $db_sds->pad_out_pic,
                                'ppa'               => $db_sds->ppa,
                                'ppa_pic'           => $db_sds->ppa_pic,
                                'ppa_out'           => $db_sds->ppa_out,
                                'ppa_out_pic'       => $db_sds->ppa_out_pic,
                                'fuse'              => $db_sds->fuse,
                                'fuse_pic'          => $db_sds->fuse_pic,
                                'fuse_out'          => $db_sds->fuse_out,
                                'fuse_out_pic'      => $db_sds->fuse_out_pic,
                                'bobok'             => $db_sds->bobok,
                                'bobok_pic'         => $db_sds->bobok_pic,
                                'bobok_out'         => $db_sds->bobok_out,
                                'bobok_out_pic'     => $db_sds->bobok_out_pic,
                                'bonding'           => $db_sds->bonding,
                                'bonding_pic'       => $db_sds->bonding_pic,
                                'bonding_out'       => $db_sds->bonding_out,
                                'bonding_out_pic'   => $db_sds->bonding_out_pic,
                                'setting'           => $db_sds->setting,
                                'setting_pic'       => $db_sds->setting_pic,
                                'supply'            => $db_sds->supply,
                                'supply_pic'        => $db_sds->supply_pic,
                                'bc_print'          => $db_sds->bc_print,
                                'cut_id'            => $db_sds->cut_id,
                                'barcode_wms'       => $db_sds->barcode_wms,
                                'lot'               => $db_sds->lot,
                                'coll'              => $db_sds->coll,
                                'supplied_to'       => $db_sds->supplied_to,
                                'supply_unlock'     => $db_sds->supply_unlock,
                                'supply_line'       => $db_sds->supply_line,
                                'plan_id'           => $db_sds->plan_id,
                                'created_at'        => Carbon::now(),
                                'created_by'        => Auth::user()->nik,
                                'packing_list_number' => $db_sds->packing_list_number,
                            ]);
                            DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$dtc->barcode_id)->delete();
                        DB::commit();
                    }catch (Exception $e) {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
                try{
                    DB::beginTransaction();
                        TempTicketSdsRemove::where('created_by',$user)->where('factory_id',Auth::user()->factory_id)->delete();
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }else{
                return response()->json('TERJADI KESALAHAN HUB ICT',422);
            }
        }
    }

    public function deleteRowDetail($id){
        TempTicketSdsRemove::where('id',$id)->delete();
    }
}
