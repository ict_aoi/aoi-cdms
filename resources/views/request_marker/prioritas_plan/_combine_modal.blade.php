<div id="combineModal" data-backdrop="static" data-keyboard="false" class="modal">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
			<div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Combine Plan | <span id="style_combine_view"></span> - <span id="article_combine_view"></span></span>
				</legend>
			</div>
			<div class="modal-body">
				<fieldset class="content-group">
					<div class="form-group">
						<label class="control-label col-lg-2 text-bold">Tanggal Cutting</label>
						<div class="col-lg-10">
							<div class="input-group">
								<span class="input-group-btn">
									<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
								</span>
								<input type="text" class="form-control" name="cutting_date_combine_plan" placeholder="Masukkan Tanggal Cutting" id="cutting_date_combine_plan">
								<span class="input-group-btn">
									<button class="btn btn-primary legitRipple" type="button" id="submit_filter_combine_plan">Pilih</button>
								</span>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-basic table-condensed" id="combineTable">
						<thead>
							<tr>
								<th>#</th>
								<th>Style</th>
								<th>Article</th>
								<th>Size Ctgy</th>
								<th>Color</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="modal-body">
				<input type="hidden" name="combine_planning_id" id="combine_planning_id" />
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
