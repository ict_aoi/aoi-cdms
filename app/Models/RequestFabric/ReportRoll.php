<?php

namespace App\Models\RequestFabric;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ReportRoll extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'report_roll';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['barcode', 'no_roll', 'material', 'color', 'actual_lot', 'actual_width', 'qty_prepared','user_id','factory_id','planning_cutting','po_supplier','po_buyer'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
