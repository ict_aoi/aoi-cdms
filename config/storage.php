<?php

return [
    'avatar' => storage_path() . '/app/avatar',
    'qc' => storage_path() . '/app/qc',
];
