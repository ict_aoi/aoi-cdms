<div class="container">
	<div class="row">
		<?php
			$barcode_cdms = DB::table('bundle_detail')->where('barcode_id',$barcode_id)->first();
			$barcode_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_id)->first();
			$barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
			if($barcode_cdms == null && $barcode_sds_cdms == null){
				$barcode_info 	= DB::table('sds_detail_movements as sdm')
				->select('shm.season','shm.poreference','shm.style','t.type_name','shm.article')
				->leftJoin('sds_header_movements as shm','shm.id','=','sdm.sds_header_id')
				->leftJoin('master_style_detail as msd','msd.id','=','sdm.style_id')
				->leftJoin('types as t','t.id','=','msd.type_id')
				->where('sdm.sds_barcode',$barcode_id)
				->first();
			}elseif($barcode_cdms == null){
				$barcode_info 	= DB::table('bundle_detail as bd')
				->select('bh.poreference','bh.season','bh.style','t.type_name','bh.article')
				->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
				->leftJoin('master_style_detail as msd','msd.id','=','bd.style_detail_id')
				->leftJoin('types as t','t.id','=','msd.type_id')
				->where('bd.sds_barcode',$barcode_id)
				->first();
			}else{
				$barcode_info 	= DB::table('bundle_detail as bd')
				->select('bh.poreference','bh.season','bh.style','t.type_name','bh.article')
				->leftJoin('bundle_header as bh','bh.id','=','bd.bundle_header_id')
				->leftJoin('master_style_detail as msd','msd.id','=','bd.style_detail_id')
				->leftJoin('types as t','t.id','=','msd.type_id')
				->where('bd.barcode_id',$barcode_id)
				->first();
			}
				$refer_line		= DB::table('master_ppcm')
								->where('po_buyer',$barcode_info->poreference)
								->where('style',$barcode_info->style)
								->where('set_type',$barcode_info->type_name)
								->where('articleno',$barcode_info->article)
								->pluck('area_name')->toArray();
		?>
		@foreach($locator_sets as $key => $set)
		<?php
			if(in_array($set->area_name,$refer_line)){
				$color = " btn-success ";
			}else{
				$color = " btn-info ";
			}
		?>
		<div class="col-md-1" style="padding-bottom:5px;padding-top:5px;height:80px">
			<button class="btn <?php echo $color; ?> btn-xl select-locator-set" style="width:84px;height:68px" data-id="{{ $set->id }}" data-area_name="{{ $set->area_name }}">{{ $set->area_name }}</button>
		</div>
		@endforeach
	</div>
</div>