<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Cutting\CuttingBarcodeController;

use DB;
use Auth;
use DataTables;
use Carbon\Carbon;
use Uuid;

class BarcodeReplacementController extends Controller
{
    public function index()
    {
        $barcode_marker = DB::table('bundle_header')
                            ->select('barcode_marker')
                            ->where('factory_id',\Auth::user()->factory_id)
                            ->where('created_at','>',Carbon::now()->subDays(90))
                            ->distinct()
                            ->get();

        $po_buyer = DB::table('bundle_header')
                        ->select('poreference')
                        ->where('factory_id',\Auth::user()->factory_id)
                        ->where('created_at','>',Carbon::now()->subDays(90))
                        ->distinct()
                        ->get();

        return view('cutting.barcode.replacement', compact('barcode_marker','po_buyer'));
    }

    public function getDataDetailbundle(Request $request){

        if ($request->ajax()){
            $factory_id = \Auth::user()->factory_id;
            $data = DB::select("SELECT qb.id,qb.barcode_id,SUM ( dqb.qty ) AS qty_reject,bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.start_no,bd.end_no,dqb.defect_name
            FROM
            qc_bundles qb
            JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id 
            JOIN bundle_detail bd ON bd.barcode_id = qb.barcode_id
            JOIN bundle_header bh ON bh.ID = bd.bundle_header_id
            JOIN master_style_detail msd ON msd.ID = bd.style_detail_id
            JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id 
            WHERE
            qb.deleted_at IS NULL 
            AND dqb.deleted_at IS NULL 
            AND bd.replace_date IS NULL 
            AND NULLIF ( msd.process, '' ) IS NOT NULL 
            AND bt.is_secondary = TRUE 
            AND qb.status <> 'ok'
            AND bh.factory_id = '$factory_id'
            GROUP BY
            qb.id,qb.barcode_id,bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no,dqb.defect_name
            UNION ALL
            SELECT
            qb.id,qb.barcode_id,SUM ( dqb.qty ) AS qty_reject,sdm.sds_header_id as bundle_header_id,shm.season,shm.STYLE,shm.set_type,shm.cutting_date,shm.poreference,shm.article,shm.cut_num,shm.size,shm.color_name,concat(shm.style,'::',shm.poreference,'::',shm.article) as job,sdm.qty,sdm.komponen_name,sdm.start_no,sdm.end_no ,dqb.defect_name
            FROM
            qc_bundles qb
            JOIN detail_qc_bundles dqb ON dqb.qc_bundle_id = qb.id 
            JOIN sds_detail_movements sdm ON sdm.sds_barcode = qb.barcode_id
            JOIN sds_header_movements shm ON shm.id = sdm.sds_header_id
            JOIN master_style_detail msd ON msd.id = sdm.style_id
            JOIN bundle_table bt ON bt.id_table = qb.bundle_table_id 
            WHERE
            qb.deleted_at IS NULL 
            AND dqb.deleted_at IS NULL 
            AND sdm.replace_date IS NULL 
            AND NULLIF ( msd.process, '' ) IS NOT NULL 
            AND bt.is_secondary = TRUE 
            AND qb.status <> 'ok'
            AND shm.factory_id = '$factory_id'
            GROUP BY
            qb.id,qb.barcode_id,sdm.sds_header_id,shm.season,shm.style,shm.set_type,shm.cutting_date,shm.poreference,shm.article,shm.cut_num,shm.size,shm.color_name,concat(shm.style,'::',shm.poreference,'::',shm.article),sdm.qty,sdm.komponen_name,sdm.sds_barcode,sdm.start_no,
            sdm.end_no,dqb.defect_name", []);


            return DataTables::of($data)
                ->addColumn('cut_sticker',function ($data)
                {
                    return $data->cut_num.' | '.$data->start_no.'-'.$data->end_no;
                })
                ->addColumn('style',function ($data)
                {
                    if($data->set_type == 'Non'){
                        $style = $data->style;
                    }else{
                        $style = $data->style.'-'.$data->set_type;
                    }
                    return $style;
                })
                ->editColumn('defect_name',function($data){
                    if($data->defect_name == '0'){
                        return $defect_name = '';
                    }else{
                        return $defect_name = $data->defect_name;
                    }
                })
                ->addColumn('selector', function($data) {
                    return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'">';
                })
                ->rawColumns(['style','cut_sticker','selector','defect_name'])
                ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
                ->make(true);
        }
    }

    public function process(Request $request)
    {
        $data = json_decode($request->data);

        if (count($data) <= 0) {
            return response()->json('data tidak ditemukan..!', 422);
        }
        
        //0 => array:15 [
        //    0 => "1"
        //    1 => true
        //    2 => "HO21"
        //    3 => "646824"
        //    4 => "D8SC8YA"
        //    5 => "SOLID_BLACK_TOP"
        //    6 => "BLACK_JACK"
        //    7 => "10 | 1-52"
        //    8 => "21070709390100026"
        //    9 => "L"
        //    10 => "WAISTBAND FRONT"
        //    11 => "2021-07-07"
        //    12 => "52"
        //    13 => "1"
        //    14 => "646824::D8SC8YA"
        //]
        $data_barcode = [];
        $data_detail1 = [];
        $data_detail2 = [];
        $data_move1 = [];
        $data_move2 = [];
        $data_deleted_detail = [];
        $no=1;
        
        try {

            DB::beginTransaction();

                for ($i=0; $i < count($data); $i++) { 
                    $barcode_id = $data[$i][2];
                    $qty_total = (int)$data[$i][12];
                    $qty_reject = (int)$data[$i][13];
                    $qty_ok = $qty_total - $qty_reject;

                    $data_details = DB::select("SELECT bd.bundle_header_id,bd.style_detail_id,bd.komponen_name,bd.start_no,bd.end_no,bd.factory_id,bd.barcode_id,bd.created_at,bd.updated_at,bd.job,bd.is_recycle,bd.is_out_cutting,bd.out_cutting_at,bd.is_out_setting,bd.out_setting_at,bd.job_reroute,bd.current_locator_id,bd.current_status_to,bd.current_description,bd.updated_movement_at,bd.current_user_id,bd.sds_barcode,bd.qty,bd.parent_barcode,bd.qty_total,bd.qty_reject,bd.is_reject,bd.qc_bundle_id,bd.replace_by,bd.replace_date
                    FROM 
                    bundle_detail bd
                    WHERE bd.barcode_id = '$barcode_id'
                    UNION ALL
                    SELECT sdm.sds_header_id as bundle_header_id,sdm.style_id as style_detail_id,sdm.komponen_name,sdm.start_no,sdm.end_no,sdm.factory_id,sdm.sds_barcode as barcode_id,null as created_at,null as updated_at,sdm.job,sdm.is_recycle,sdm.is_out_cutting,sdm.is_out_cutting_at as out_cutting_at,null as is_out_setting,sdm.out_setting_at,null as job_reroute,sdm.current_location_id as current_locator_id,sdm.current_status_to,sdm.current_description,sdm.updated_movement_at,sdm.current_user_id,null as sds_barcode,sdm.qty,sdm.parent_barcode,sdm.qty_total,sdm.qty_reject,sdm.is_reject,sdm.qc_bundle_id,sdm.replace_by,sdm.replace_date
                    FROM 
                    sds_detail_movements sdm
                    WHERE sdm.sds_barcode = '$barcode_id'");

                    $data_qc = DB::table('qc_bundles')->where('barcode_id', $barcode_id)->whereNull('deleted_at')->first();

                    if ($data[$i][1]) {
                        
                        if ($qty_ok < 1) {
                            continue;
                        }

                        foreach($data_details as $x => $z){
                            if (!empty($z->parent_barcode)) {
                                continue;
                            }
                        $data_barcode[] = $barcode_id;
                            $barcode_sds = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_id)->first();
                            if($barcode_sds !=null){
                                $det1sds['sds_barcode'] = $barcode_id.'-R'.$no++;
                                $det1sds['sds_header_id'] = $z->bundle_header_id;
                                $det1sds['style_id'] = $z->style_detail_id;
                                $det1sds['komponen_name'] = $z->komponen_name;
                                $det1sds['start_no'] = $z->start_no;
                                $det1sds['end_no'] = $z->end_no;
                                $det1sds['qty'] = $qty_ok;
                                $det1sds['factory_id'] = $z->factory_id;
                                $det1sds['job'] = $z->job;
                                $det1sds['is_recycle'] = $z->is_recycle;
                                $det1sds['is_out_cutting'] = $z->is_out_cutting;
                                $det1sds['is_out_cutting_at'] = $z->out_cutting_at;
                                $det1sds['out_setting_at'] = $z->out_setting_at;
                                $det1sds['current_location_id'] = $z->current_locator_id;
                                $det1sds['current_status_to'] = $z->current_status_to;
                                $det1sds['current_description'] = $z->current_description;
                                $det1sds['updated_movement_at'] = $z->updated_movement_at;
                                $det1sds['current_user_id'] = $z->current_user_id;
                                $det1sds['parent_barcode'] = $z->barcode_id;
                                $det1sds['qty_total'] = $z->qty;
                                $det1sds['qty_reject'] = $qty_reject;
                                $det1sds['is_reject'] = false;
                                $det1sds['qc_bundle_id'] = $data_qc->id;
                                $det1sds['replace_by'] = Auth::user()->id;
                                $det1sds['replace_date'] = Carbon::now();
                                $data_detail_sds1[] = $det1sds;

                                //////////////////////////////////////DETAIL////////////////////////////////
                                $det2sds['sds_barcode'] = $barcode_id.'-R'.$no++;
                                $det2sds['sds_header_id'] = $z->bundle_header_id;
                                $det2sds['style_id'] = $z->style_detail_id;
                                $det2sds['komponen_name'] = $z->komponen_name;
                                $det2sds['start_no'] = $z->start_no;
                                $det2sds['end_no'] = $z->end_no;
                                $det2sds['qty'] = $qty_reject;
                                $det2sds['factory_id'] = $z->factory_id;
                                $det2sds['job'] = $z->job;
                                $det2sds['is_recycle'] = $z->is_recycle;
                                $det2sds['is_out_cutting'] = $z->is_out_cutting;
                                $det2sds['is_out_cutting_at'] = $z->out_cutting_at;
                                $det2sds['out_setting_at'] = $z->out_setting_at;
                                $det2sds['current_location_id'] = $z->current_locator_id;
                                $det2sds['current_status_to'] = $z->current_status_to;
                                $det2sds['current_description'] = $z->current_description;
                                $det2sds['updated_movement_at'] = $z->updated_movement_at;
                                $det2sds['current_user_id'] = $z->current_user_id;
                                $det2sds['parent_barcode'] = $z->barcode_id;
                                $det2sds['qty_total'] = $z->qty;
                                $det2sds['qty_reject'] = $qty_reject;
                                $det2sds['is_reject'] = true;
                                $det2sds['qc_bundle_id'] = $data_qc->id;
                                $det2sds['replace_by'] = Auth::user()->id;
                                $det2sds['replace_date'] = Carbon::now();
                                $data_detail_sds2[] = $det2sds;
                            }else{
                                $det1['barcode_id'] = CuttingBarcodeController::generateBarcodeNumber($no++);
                                $det1['bundle_header_id'] = $z->bundle_header_id;
                                $det1['style_detail_id'] = $z->style_detail_id;
                                $det1['komponen_name'] = $z->komponen_name;
                                $det1['start_no'] = $z->start_no;
                                $det1['end_no'] = $z->end_no;
                                $det1['qty'] = $qty_ok;
                                $det1['location'] = $z->location;
                                $det1['factory_id'] = $z->factory_id;
                                $det1['sds_barcode'] = $z->sds_barcode;
                                $det1['created_at'] = $z->created_at;
                                $det1['updated_at'] = $z->updated_at;
                                $det1['job'] = $z->job;
                                $det1['is_recycle'] = $z->is_recycle;
                                $det1['is_out_cutting'] = $z->is_out_cutting;
                                $det1['out_cutting_at'] = $z->out_cutting_at;
                                $det1['is_out_setting'] = $z->is_out_setting;
                                $det1['out_setting_at'] = $z->out_setting_at;
                                $det1['job_reroute'] = $z->job_reroute;
                                $det1['current_locator_id'] = $z->current_locator_id;
                                $det1['current_status_to'] = $z->current_status_to;
                                $det1['current_description'] = $z->current_description;
                                $det1['updated_movement_at'] = $z->updated_movement_at;
                                $det1['current_user_id'] = $z->current_user_id;
                                $det1['parent_barcode'] = $z->barcode_id;
                                $det1['qty_total'] = $z->qty;
                                $det1['qty_reject'] = $qty_reject;
                                $det1['is_reject'] = false;
                                $det1['qc_bundle_id'] = $data_qc->id;
                                $det1['replace_by'] = Auth::user()->id;
                                $det1['replace_date'] = Carbon::now();
                                $data_detail1[] = $det1;

                                //////////////////////////////////////DETAIL////////////////////////////////

                                $det2['barcode_id'] = CuttingBarcodeController::generateBarcodeNumber($no++);
                                $det2['bundle_header_id'] = $z->bundle_header_id;
                                $det2['style_detail_id'] = $z->style_detail_id;
                                $det2['komponen_name'] = $z->komponen_name;
                                $det2['start_no'] = $z->start_no;
                                $det2['end_no'] = $z->end_no;
                                $det2['qty'] = $qty_reject;
                                $det2['location'] = $z->location;
                                $det2['factory_id'] = $z->factory_id;
                                $det2['sds_barcode'] = $z->sds_barcode;
                                $det2['created_at'] = $z->created_at;
                                $det2['updated_at'] = $z->updated_at;
                                $det2['job'] = $z->job;
                                $det2['is_recycle'] = $z->is_recycle;
                                $det2['is_out_cutting'] = $z->is_out_cutting;
                                $det2['out_cutting_at'] = $z->out_cutting_at;
                                $det2['is_out_setting'] = $z->is_out_setting;
                                $det2['out_setting_at'] = $z->out_setting_at;
                                $det2['job_reroute'] = $z->job_reroute;
                                $det2['current_locator_id'] = $z->current_locator_id;
                                $det2['current_status_to'] = $z->current_status_to;
                                $det2['current_description'] = $z->current_description;
                                $det2['updated_movement_at'] = $z->updated_movement_at;
                                $det2['current_user_id'] = $z->current_user_id;
                                $det2['parent_barcode'] = $z->barcode_id;
                                $det2['qty_total'] = $z->qty;
                                $det2['qty_reject'] = $qty_reject;
                                $det2['is_reject'] = true;
                                $det2['qc_bundle_id'] = $data_qc->id;
                                $det2['replace_by'] = Auth::user()->id;
                                $det2['replace_date'] = Carbon::now();
                                $data_detail2[] = $det2;
                            }
                        }
                    }
                }
                if(count($data_detail1)<=0){
                    $insert = DB::table('sds_detail_movements')->insert($data_detail_sds1);
                    $insert = DB::table('sds_detail_movements')->insert($data_detail_sds2);
                }else{
                    $insert = DB::table('bundle_detail')->insert($data_detail1);
                    $insert = DB::table('bundle_detail')->insert($data_detail2);
                }

                //update
                DB::table('bundle_detail')->whereIn('barcode_id', $data_barcode)
                                        ->update([
                                            'updated_at' => Carbon::now(),
                                            'replace_date' => Carbon::now(),
                                            'replace_by' => Auth::user()->id
                                        ]);
                DB::table('sds_detail_movements')->whereIn('sds_barcode', $data_barcode)
                ->update([
                    'replace_date' => Carbon::now(),
                    'replace_by' => Auth::user()->id
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json($data_barcode, 200);
        
    }

    ////
    public function printPdf(Request $request)
    {        
        $data = json_decode($request->data);
        $data_barcode = "'".implode("','",$data)."'";
        $barcode_id = array();

        $map = explode(',',implode(",",array_map(function($a) {return implode("~",$a);},$barcode_id)));

        $data_detail_bundle = DB::select("SELECT bd.barcode_id,bd.bundle_header_id,bd.style_detail_id,bd.komponen_name,bd.start_no,bd.end_no,bd.qty,bd.sds_barcode,bd.is_recycle,bh.season,bh.style,bh.set_type,bh.poreference,bh.article,bh.cut_num,bh.size,bh.factory_id,bh.cutting_date,bh.color_name,bh.cutter,bh.note,bh.admin,bh.qc_operator,bh.bundle_operator,bh.box_lim,bh.max_bundle,bh.job,vms.part,vms.process,vms.process_name,vms.type_id,vms.type_name,mf.factory_name,mf.factory_alias,bh.admin_name,bh.qc_operator_name,bh.bundle_operator_name,bh.statistical_date,bh.created_at,bh.destination,bd.is_reject,bd.qty_total,bd.qty_reject
        FROM bundle_detail bd
        JOIN bundle_header bh on bh.id = bd.bundle_header_id
        JOIN v_master_style vms on vms.id = bd.style_detail_id
        JOIN master_factory mf on mf.id = bh.factory_id WHERE bd.parent_barcode IN($data_barcode)
        UNION ALL
        SELECT sdm.sds_barcode as barcode_id,sdm.sds_header_id as bundle_header_id,sdm.style_id as style_detail_id,sdm.komponen_name,sdm.start_no,sdm.end_no,sdm.qty,sdm.sds_barcode,sdm.is_recycle,shm.season,shm.style,shm.set_type,shm.poreference,shm.article,shm.cut_num,shm.size,shm.factory_id,shm.cutting_date,shm.color_name,null as cutter,null as note,null as admin,null as qc_operator,null as bundle_operator,null as box_lim,null as max_bundle,sdm.job,vms.part,vms.process,vms.process_name,vms.type_id,vms.type_name,mf.factory_name,mf.factory_alias,null as admin_name,null as qc_operator_name,null as bundle_operator_name,null as statistical_date,shm.created_at,null as destination,sdm.is_reject,sdm.qty_total,sdm.qty_reject
        FROM sds_detail_movements sdm
        JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
        JOIN v_master_style vms on vms.id = sdm.style_id
        JOIN master_factory mf on mf.id = shm.factory_id 
        WHERE sdm.parent_barcode IN($data_barcode)
        ");
        
        $pdf = \PDF::loadView('/cutting/barcode/print/index_replace',['data_detail_bundle' => $data_detail_bundle])->setPaper('a4');

        return $pdf->stream("Replacement".'pdf');
    }
}
