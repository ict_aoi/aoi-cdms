<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-1" >
                        <label><b>PCD</b></label>
                        <input type="text" name="pcd_md" id="pcd_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Style</b></label>
                        <input type="text" name="style_md" id="style_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <input style="margin-top: 28px;" type="text" name="top_bot_md" id="top_bot_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Article</b></label>
                        <input type="text" name="article_md" id="article_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Size Ctg</b></label>
                        <input type="text" name="size_md" id="size_md" class="form-control" readonly="">
                    </div>
                    <div class="col-lg-3">
                        <label><b>Color</b></label>
                        <input type="text" name="color_md" id="color_md" class="form-control" readonly="">
                        <input type="text" name="mo_md" id="mo_md" class="hidden">
                        <input type="text" name="material_md" id="material_md" class="hidden">
                        <input type="text" name="color_code_md" id="color_code_md" class="hidden">
                        <input type="text" name="season_md" id="season_md" class="hidden">
                        <input type="text" name="gtb_md" id="gtb_md" class="hidden">
                    </div>
                    <div class="col-lg-1">
                        <label><b>Queue</b></label>
                        <input type="text" name="queue_md" id="queue_md" class="form-control" placeholder="Queue">
                    </div>
                    <div class="col-lg-1">
                        <!-- <label style="color: white; padding-top: 10px;"><b>button</b></label> -->
                        <button class="btn btn-primary btn-xs" id="btn-save" style="margin-top: 30px;"><span class="icon-stack2"></span></button>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="table-list-detail">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Po Buyer</th>
                                    <th>Queu</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                </div>             
            </div>
        </div>
    </div>
</div>