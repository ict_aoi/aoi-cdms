
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="#" onclick="detail('{!! $detail !!}')" ><i class="icon-document"></i> Detail</a></li>
            @endif
        </ul>
    </li>
</ul>
