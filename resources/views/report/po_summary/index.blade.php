@extends('layouts.app',['active' => 'po_summary'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report - PO Summary</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Report - PO Summary</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat">
	<div class="panel-body">
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">PO SUMMARY</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
                    <form class="form-horizontal" action="" id="" method="POST">
						<fieldset class="content-group">
								@csrf
								<div class="form-group">
									<label class="control-label col-lg-2 text-bold">PO Buyer</label>
									<div class="col-lg-10">
										<div class="input-group">
											<span class="input-group-btn">	
												<button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-search4"></i></button>
											</span>
											<input type="text" class="form-control" name="po_buyer" id="po_buyer" placeholder="Masukkan PO Buyer..!" onkeydown="return (event.keyCode!=13);"/>
                                            <span class="input-group-btn">
												<button class="btn btn-primary legitRipple" type="button" id="btn-filter">FILTER</button>
											</span>
										</div>
									</div>
								</div>
						</fieldset>
					</form>
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="table-list">
									<thead class="bg-grey">
										<tr>
											<th>No</th>
											<th>PO Buyer</th>
											<th>Season</th>
											<th>Style</th>
											<th>Article</th>
                                            <th>Size</th>
											<th>QTY Bundle</th>
											<th>QTY Order</th>
											<th>CUT</th>
                                            <th>FUSE</th>
                                            <th>PPA 1</th>
                                            <th>PPA 2</th>
                                            <th>PAD</th>
                                            <th>HE</th>
                                            <th>AUTO</th>
                                            <th>ART</th>
                                            <th>SETTING</th>
                                            <th>SUPPLY</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page-js')
<script>
    $(document).ready( function () {
		//DATATABLES
		var po_buyer_field = document.getElementById("po_buyer");
		po_buyer_field.addEventListener("keyup", function(event) {
			if (event.keyCode === 13) {
				event.preventDefault();
				document.getElementById("btn-filter").click();
			}
		});
		var po_buyer =$('#po_buyer').val();
			var cut_num =$('#cut_num').val();
			var url = "{{route('po_summary.getDataSummaryPo')}}";
			var tableL = $('#table-list').DataTable({
				processing: true,
				serverSide: true,
				deferRender:true,
				pageLength:100,
				destroy:true,
				scrollX:true,
				dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
				language: {
					search: '<span>Filter:</span> _INPUT_',
					searchPlaceholder: 'Type to filter...',
					lengthMenu: '<span>Show:</span> _MENU_',
					paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
				},
				ajax: {
					type: 'GET',
					url: url,
					data: function(d) {
						return $.extend({}, d, {
							"po_buyer"		: $('#po_buyer').val(),
							"cut_num"		: $('#cut_num').val(),
						});
					},
				},
				fnCreatedRow: function (row, data, index) {
					var info = tableL.page.info();
					var value = index+1+info.start;
					$('td', row).eq(0).html(value);
				},
				columns: [
					{data: null, sortable: false, orderable: false, searchable: false},//0
					{data: 'poreference', name: 'poreference',searchable:false,visible:true,orderable:false},//12
					{data: 'season', name: 'season',searchable:false,visible:true,orderable:false},//1
					{data: 'style', name: 'style',searchable:true,visible:true,orderable:true},//2
					{data: 'article', name: 'article',searchable:false,visible:true,orderable:false},//3
                    {data: 'size', name: 'size',searchable:false,visible:true,orderable:false},//3
					{data: 'qty_bundle', name: 'qty_bundle',searchable:false,visible:true,orderable:false},//4
					{data: 'ordered_qty', name: 'ordered_qty',searchable:false,visible:true,orderable:false},//4
					{data: 'qty_cut', name: 'qty_cut',searchable:false,visible:true,orderable:true},//5
					{data: 'qty_fuse', name: 'qty_fuse',searchable:true,visible:true,orderable:true},//6
					{data: 'qty_ppa1', name: 'qty_ppa1',searchable:true,visible:true,orderable:true},//7
					{data: 'qty_ppa2', name: 'qty_ppa2',searchable:true,visible:true,orderable:true},//8
					{data: 'qty_pad', name: 'qty_pad',searchable:false,visible:true,orderable:false},//11
					{data: 'qty_he', name: 'qty_he',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_auto', name: 'qty_auto',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_artwork', name: 'qty_artwork',searchable:true,visible:true,orderable:true},//12
					{data: 'qty_setting', name: 'qty_setting',searchable:true,visible:true,orderable:true},//13
                    {data: 'qty_supply', name: 'qty_supply',searchable:true,visible:true,orderable:true},//13
				]
			});
	
			var tableL = $('#table-list').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						tableL.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						tableL.search("").draw();
					}
					return;
			});
			tableL.on('preDraw',function(){
				Pace.start();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			})
			.on('draw.dt',function(){
				Pace.stop();
				$.unblockUI();
			});
	
		$('#btn-filter').click(function(){
			event.preventDefault();
			var po_buyer    = $('#po_buyer').val();
			if(po_buyer =='') {
				$("#alert_warning").trigger("click", 'PO Buyer Tidak Boleh Kosong!');
				return false;
			}
			tableL.draw();
		});
    });
</script>
@endsection