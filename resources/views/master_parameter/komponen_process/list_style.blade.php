<div class="table-responsive">
    <table class="table" id="table-">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th width="10%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $val)
                <tr>
                    <td>{{ $val-> }}</td>
                    <td>{{ $val-> }}</td>
                    <td>
                        <button type="button" id="choose-"
                                data-id="{{ $val->id }}"
                                data-="{{ $val-> }}"
                                class="btn btn-default">
                                SELECT
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    </div>
    <script type="text/javascript">
        var tables = $('#table-').DataTable({  
            "lengthChange": false,
            "pageLength": 5,
            "columnDefs": [{
                "targets" : [0],
                "visible" : false,
                "searchable" : false
            }]
        });
    </script>
    