<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Auth;
use Excel;

class RndController extends Controller
{
    public function index()
    {
        $master_seasons = DB::table('master_seasons')
                            ->whereNull('deleted_at')
                            ->orderBy('season_name')
                            ->get();

        return view('report.rnd.index', compact('master_seasons'));
    }

    public function export(Request $request)
    {

        $data = DB::table('v_report_rnd');

        if ($request->tab_active == 'season') {
            $data = $data->where('season', $request->season);
        }

        $data = $data->orderBy('style_standart')
                        ->orderBy('set_type');

        $filename = 'report_rnd_'.Carbon::now()->format('d_m_Y');
        $i=1;
        return Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report', function($sheet) use($data, $i) {

                    $sheet->setCellValue('A3','NO');
                    $sheet->setCellValue('B3','SEASON');
                    $sheet->setCellValue('C3','STYLE STANDARD');
                    $sheet->setCellValue('D3','STYLE');
                    $sheet->setCellValue('E3','PRODUCT TYPE');
                    $sheet->setCellValue('F3','CATEGORY PRODUCT');
                    $sheet->setCellValue('G3','BISNIS UNIT');
                    $sheet->setCellValue('H3','MAIN MATERIAL');
                    $sheet->setCellValue('I3','DESC MATERIAL');
                    $sheet->setCellValue('J3','CATEGORY FABRIC');
                    $sheet->setCellValue('K3','FABRIC GROUP');
                    $sheet->setCellValue('L3','TINGKAT KESULITAN');
                    $sheet->setCellValue('M3','MP');
                    $sheet->setCellValue('N3','TOTAL PROCESS');
                    $sheet->setCellValue('O3','STATUS STYLE');
                    $sheet->setCellValue('P3','TSME');
                    $sheet->mergeCells('Q1:Q2');
                    $sheet->setCellValue('Q1','SMALL CELL');
                    $sheet->getStyle('Q1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('Q3','aSC Priority');
                    $sheet->mergeCells('R1:X1');
                    $sheet->setCellValue('R1','FUSE TYPE');
                    $sheet->getStyle('R1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('R3','WELT POCKET');
                    $sheet->setCellValue('S3','FACING');
                    $sheet->setCellValue('T3','COLLAR');
                    $sheet->setCellValue('U3','PLAKET');
                    $sheet->setCellValue('V3','3 STRIPE');
                    $sheet->setCellValue('W3','OTHER');
                    $sheet->setCellValue('X3','TOTAL FUSE');
                    // $sheet->setCellValue('X3','WELT POCKET');

                    $sheet->mergeCells('Y1:AA1');
                    $sheet->setCellValue('Y1','3 STRIPES');
                    $sheet->getStyle('Y1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('Y3','DEBOSS');
                    $sheet->setCellValue('Z3','TAPE');
                    $sheet->setCellValue('AA3','PIPING');

                    $sheet->mergeCells('AB1:AL1');
                    $sheet->setCellValue('AB1','HEAT TRANSFER TYPE');
                    $sheet->getStyle('AB1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('AB3','ADIDAS');
                    $sheet->setCellValue('AC3','Climawarm');
                    $sheet->setCellValue('AD3','CLIMALITE');
                    $sheet->setCellValue('AE3','CLIMACOOL');
                    $sheet->setCellValue('AF3','RESPONSE');
                    $sheet->setCellValue('AG3','DHTL');
                    $sheet->setCellValue('AH3','3 STRIPE');
                    $sheet->setCellValue('AI3','Interlining');
                    $sheet->setCellValue('AJ3','AEROREADY');
                    $sheet->setCellValue('AK3','OTHER');
                    $sheet->setCellValue('AL3','TOTAL HEAT TRANSFER');

                    $sheet->mergeCells('AM1:AP1');
                    $sheet->setCellValue('AM1','EMBROIDERY');
                    $sheet->getStyle('AM1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('AM3','CORP LOGO ADIDAS');
                    $sheet->setCellValue('AN3','BODY');
                    $sheet->setCellValue('AO3','EMBRO EYELET');
                    $sheet->setCellValue('AP3','TOTAL EMBRO');
                    
                    $sheet->mergeCells('AQ1:AU1');
                    $sheet->setCellValue('AQ1','PRINTING TYPE');
                    $sheet->getStyle('AQ1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('AQ3','3 STRIPES');
                    $sheet->setCellValue('AR3','ADIDAS LOGO');
                    $sheet->setCellValue('AS3','LOGO CLUB');
                    $sheet->setCellValue('AT3','PRINT BODY');
                    $sheet->setCellValue('AU3','TOTAL PRINT');

                    $sheet->mergeCells('AV1:BJ1');
                    $sheet->setCellValue('AV1','PRINTING TYPE');
                    $sheet->getStyle('AV1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('AV2','3 DAYS');
                    $sheet->setCellValue('AW2','5 DAYS');
                    $sheet->setCellValue('AX2','5 DAYS');
                    $sheet->setCellValue('BG2','5 DAYS');
                    $sheet->setCellValue('AV3','Rubber Printing');
                    $sheet->setCellValue('AW3','HD Print');
                    $sheet->setCellValue('AX3','Solvent Print');
                    $sheet->setCellValue('AY3','Water Based Print');
                    $sheet->setCellValue('AZ3','Sublimation Print');
                    $sheet->setCellValue('BA3','Flock Print');
                    $sheet->setCellValue('BB3','Reflective Print');
                    $sheet->setCellValue('BC3','Silicon Print');
                    $sheet->setCellValue('BD3','Glossy Print');
                    $sheet->setCellValue('BE3','Gel Print');
                    $sheet->setCellValue('BF3','Photo Print');
                    $sheet->setCellValue('BG3','Matte Oil Print');
                    $sheet->setCellValue('BH3','Puff Print');
                    $sheet->setCellValue('BI3','Metalic Print');
                    $sheet->setCellValue('BJ3','Foll Print');

                    $sheet->mergeCells('BK2:BL2');
                    $sheet->setCellValue('BK2','REMARK');
                    $sheet->getStyle('BK2')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BK3','Seri');
                    $sheet->setCellValue('BL3','Pararel');

                    $sheet->mergeCells('BM2:BN2');
                    $sheet->setCellValue('BM2','PANEL PRINTING');
                    $sheet->getStyle('BM2')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BM3','Quantity Panel');
                    $sheet->setCellValue('BN3','Ukuran Panel');

                    $sheet->setCellValue('BO3','PRINTING & EMBROIDERY');
                    $sheet->setCellValue('BP3','Pad Printing');
                    
                    $sheet->mergeCells('BQ1:BR2');
                    $sheet->setCellValue('BQ1','Engineering Fabric');
                    $sheet->getStyle('BQ1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BQ3','Fabric Sublime');
                    $sheet->setCellValue('BR3','Zebra Special');

                    $sheet->mergeCells('BS1:BT2');
                    $sheet->setCellValue('BS1','DEBOSS & EMBOSS');
                    $sheet->getStyle('BS1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BS3','CORP LOGO DEBOSS');
                    $sheet->setCellValue('BT3','CORP LOGO EMBOSS');

                    $sheet->mergeCells('BU1:BV2');
                    $sheet->setCellValue('BU1','ARTWORK BONDING & LASER CUT');
                    $sheet->getStyle('BU1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BU3','BONDING');
                    $sheet->setCellValue('BV3','LASER CUT');

                    $sheet->setCellValue('BW3','CRITICAL PROSES');
                    $sheet->getStyle('BW3')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );

                    $sheet->mergeCells('BX1:CD2');
                    $sheet->setCellValue('BX1','AUTOMATION');
                    $sheet->getStyle('BX1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('BX3','Drawcord');
                    $sheet->setCellValue('BY3','Auto Welt Pocket');
                    $sheet->setCellValue('BZ3','Join Care Label');
                    $sheet->setCellValue('CA3','Automatic  Attach String Hangtag');
                    $sheet->setCellValue('CB3','Auto Cut & Join Circular WB Elastic (Pants)');
                    $sheet->setCellValue('CC3','Auto Measure Cut & Elastic (Pants)');
                    $sheet->setCellValue('CD3','Auto Make Looper & Cut with Auto Counting (Jacket)');

                    $sheet->setCellValue('CE3','TOTAL WELT POCKET');

                    $sheet->mergeCells('CF1:CI2');
                    $sheet->setCellValue('CF1','PANEL ARTWORK');
                    $sheet->getStyle('CF1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('CF3','Panel Fuse Type');
                    $sheet->setCellValue('CG3','Panel Heat Transfer Type');
                    $sheet->setCellValue('CH3','Panel Embroydery Type');
                    $sheet->setCellValue('CI3','Panel Printing Type');

                    $sheet->mergeCells('CJ2:CL2');
                    $sheet->setCellValue('CJ2','AD (maximal H + 5)');
                    $sheet->getStyle('CJ2')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('CJ3','Update AD');
                    $sheet->setCellValue('CK3','Update SPT Input');
                    $sheet->setCellValue('CL3','Status AD');
                    // $sheet->setCellValue('CM3','Notes');
                    
                    $sheet->mergeCells('CM1:CN2');
                    $sheet->setCellValue('CM1','Diameter Hemmning (cm)');
                    $sheet->getStyle('CM1')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center') //left,right,center & vertical
                    );
                    $sheet->setCellValue('CM3','Minimal Diameter Hemmning');
                    $sheet->setCellValue('CN3','Maximal Diameter Hemmning');

                    // $sheet->setCellValue('CN3','STATUS STYLE SPT');

                    // $data->chunk(1000, function($rows) use ($sheet, $i)
                    // {
                        foreach ($data->get() as $row)
                        {
                            $style = $row->style_standart;
                            $season = $row->season;
                            $set_type = $row->set_type;

                            ////// fuse type //////
                            $WELT_POCKET = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'WELT POCKET')
                                            ->count();

                            $FACING = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'FACING')
                                            ->count();

                            $COLLAR = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'COLLAR')
                                            ->count();

                            $PLAKET = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'PLAKET')
                                            ->count();

                            $STRIPE3 = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), '3 STRIPE')
                                            ->count();

                            $OTHER_FUSE = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'OTHER')
                                            ->count();

                            $TOTAL_FUSE = $WELT_POCKET+$FACING+$COLLAR+$PLAKET+$STRIPE3+$OTHER_FUSE;

                            ////// end fuse type //////

                            ///// bobok /////

                            $WELT_POCKET_BOBOK = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.BOBOK_TYPE'))
                                                // ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%WELT POCKET%')
                                                ->where(DB::raw('upper(column_name)'), 'WELT POCKET')
                                                ->count();

                            ///// end bobok ///


                            ////// he type //////
                            $ADIDAS = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'ADIDAS')
                                            ->count();

                            $CLIMAWARM = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), '%CLIMAWARM%')
                                            ->count();

                            $CLIMALITE = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), '%CLIMALITE%')
                                            ->count();

                            $CLIMACOOL = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), '%CLIMACOOL%')
                                            ->count();

                            $RESPONSE = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'LIKE', '%RESPONSE%')
                                            ->count();

                            $DHTL = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'LIKE', '%DHTL%')
                                            ->count();

                            $STRIPE3_HE = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'LIKE', '%3 STRIPE%')
                                            ->count();

                            $INTERLINING = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'LIKE', '%INTERLINING%')
                                            ->count();

                            $AEROREADY = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'LIKE', '%AEROREADY%')
                                            ->count();

                            $OTHER_HE = DB::table('v_rnd_detail_process')
                                            ->where('style', $style)
                                            ->where('season', $season)
                                            ->where('type_name', $set_type)
                                            ->where('process_id', \Config::get('constants.HE_TYPE'))
                                            ->where(DB::raw('upper(column_name)'), 'OTHER')
                                            ->count();
                                            
                            $TOTAL_HE = $ADIDAS+$CLIMAWARM+$CLIMALITE+$CLIMACOOL+$RESPONSE+$DHTL+$STRIPE3_HE+$INTERLINING+$AEROREADY+$OTHER_HE;
                            ////// end he type /////
                                            
                                            
                            ////// embro type ///////

                            $CORP_LOGO_ADIDAS = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.EMBRO_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'CORP LOGO ADIDAS')
                                                ->count();

                            $BODY = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                            ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.EMBRO_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'BODY')
                                                ->count();

                            $EMBRO_EYELET = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.EMBRO_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'EMBRO EYELET')
                                                ->count();

                            $TOTAL_EMBRO = $CORP_LOGO_ADIDAS+$BODY+$EMBRO_EYELET;


                            ///// end embro type ////

                            ////// printing type 1 ///////

                            $STRIPE3_PRINTING1 = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), '3 STRIPES')
                                                ->sum('total');

                            $ADIDAS_LOGO = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'ADIDAS LOGO')
                                                ->sum('total');

                            $LOGO_CLUB = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'LOGO CLUB')
                                                ->sum('total');

                            $PRINT_BODY = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'PRINT BODY')
                                                ->sum('total');
                            
                            $TOTAL_PRINTING1 = $STRIPE3_PRINTING1+$ADIDAS_LOGO+$LOGO_CLUB+$PRINT_BODY;

                            ///// end printing type 1 /////

                            ////// printing type 2 ///////

                            $RUBBER_PRINTING = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'RUBBER PRINTING')
                                                ->sum('total');

                            $HD_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'HD PRINT')
                                                ->sum('total');

                            $SOLVENT_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'SOLVENT PRINT')
                                                ->sum('total');

                            $WATER_BASED_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'WATER BASED PRINT')
                                                ->sum('total');

                            $SUBLIMATION_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'SUBLIMATION PRINT')
                                                ->sum('total');

                            $FLOCK_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'FLOCK PRINT')
                                                ->sum('total');

                            $REFLECTIVE_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'REFLECTIVE PRINT')
                                                ->sum('total');

                            $SILICON_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'SILICON PRINT')
                                                ->sum('total');

                            $GLOSSY_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'GLOSSY PRINT')
                                                ->sum('total');

                            $GEL_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'GEL PRINT')
                                                ->sum('total');

                            $PHOTO_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'PHOTO PRINT')
                                                ->sum('total');

                            $MATTE_OIL_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'MATTE OIL PRINT')
                                                ->sum('total');

                            $PUFF_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'PUFF PRINT')
                                                ->sum('total');

                            $METALIC_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'METALIC PRINT')
                                                ->sum('total');

                            $FOLL_PRINT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where(DB::raw('upper(column_name2)'), 'FOLL PRINT')
                                                ->sum('total');

                            
                            ///// end printing type 2 ////
                            
                            ///// critical process ///// 
                            $CRITICAL_PROCESS = DB::table('v_rnd_detail_process')
                                                ->select('detail_process_name')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->first();
                            ///// end critical process ///// 
                            if($CRITICAL_PROCESS != null){
                                $detail_process_name = $CRITICAL_PROCESS->detail_process_name;
                            }else{
                                $detail_process_name = null;
                            }

                            ///// automation type /////
                            $DRAWCORD = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'DRAWCORD')
                                                ->count();

                            $AUTO_WELT_POCKET = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'AUTO WELT POCKET')
                                                ->count();

                            $JOIN_CARE_LABEL = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'JOIN CARE LABEL')
                                                ->count();

                            $JOIN_CUT_ELASTIC = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%CUT ELASTIC%')
                                                ->where(DB::raw('upper(detail_process_name)'), 'NOT LIKE', '%MEASURE CUT ELASTIC%')
                                                ->where(DB::raw('upper(detail_process_name)'), 'NOT LIKE', '%AUTO%')
                                                ->count();

                            $MEASURE_CUT_ELASTIC = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%MEASURE CUT ELASTIC%')
                                                ->where(DB::raw('upper(detail_process_name)'), 'NOT LIKE', '%AUTO%')
                                                ->count();

                            $ATTACH_STRING_HANGTAG = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(column_name)'), 'AUTOMATIC ATTACH STRING HANGTAG')
                                                ->count();

                            $JOIN_CUT_ELASTIC_AUTO = DB::table('v_rnd_detail_process')
                                                    ->where('style', $style)
                                                    ->where('season', $season)
                                                    ->where('type_name', $set_type)
                                                    ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                    ->where(DB::raw('upper(column_name)'), 'AUTO CUT & JOIN CIRCULAR WB ELASTIC (PANTS)')
                                                    ->count();

                            $MEASURE_CUT_ELASTIC_AUTO = DB::table('v_rnd_detail_process')
                                                    ->where('style', $style)
                                                    ->where('season', $season)
                                                    ->where('type_name', $set_type)
                                                    ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                    ->where(DB::raw('upper(column_name)'), 'AUTO MEASURE CUT & ELASTIC (PANTS)')
                                                    ->count();

                            $MAKE_LOOPER_AUTO = DB::table('v_rnd_detail_process')
                                                    ->where('style', $style)
                                                    ->where('season', $season)
                                                    ->where('type_name', $set_type)
                                                    ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                    ->where(DB::raw('upper(column_name)'), 'AUTO MAKE LOOPER & CUT WITH AUTO COUNTING (JACKET)')
                                                    ->count();

                            $TOTAL_WELT_POCKET = DB::table('v_rnd_detail_process')
                                                ->select(DB::raw('MAX(total) as total'))
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                // ->where('process_id', \Config::get('constants.AUTOMATION_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'WELT POCKET WITH ZIPPER')
                                                // ->groupBy(['style', 'season', 'type_name'])
                                                ->first()->total;

                            
                            ///// end automation type ////

                            ///// panel artwork ///
                            $PANEL_FUSE_TYPE = DB::table('v_rnd_detail_process')
                                                ->select(DB::raw("string_agg(concat(detail_process_name,' AT ', komponen_name),',') as panel_fuse"))
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.FUSE_TYPE'))
                                                ->first()->panel_fuse;

                            $PANEL_HE_TYPE = DB::table('v_rnd_detail_process')
                                                ->select(DB::raw("string_agg(concat(detail_process_name,' AT ', komponen_name),',') as panel_he"))
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.HE_TYPE'))
                                                ->first()->panel_he;

                            $PANEL_EMBRO_TYPE = DB::table('v_rnd_detail_process')
                                                ->select(DB::raw("string_agg(concat(detail_process_name,' AT ', komponen_name),',') as panel_embro"))
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.EMBRO_TYPE'))
                                                ->first()->panel_embro;

                            $PANEL_PRINTING_TYPE = DB::table('v_rnd_detail_process')
                                                ->select(DB::raw("string_agg(concat(detail_process_name,' AT ', komponen_name),',') as panel_printing"))
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->first()->panel_printing;

                            //// end panel artwork ///
                            
                            ///// ppa type ///////
                            $JOIN_CIRCULAR = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PPA_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%JOIN CIRCULAR%')
                                                ->count();

                            $MEASURE_CUT = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PPA_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%MEASURE CUT%')
                                                ->count();

                            $MAKE_LOOPER = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PPA_TYPE'))
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%MAKE LOOPER%')
                                                ->count();

                            
                            ///// end ppa type ////

                            // stripes //
                            $stripes = $row->stripes != null ? array_map('trim', explode(',', $row->stripes)) : array();
                            // end stripes //

                            // printing & embro //
                            $prem_agg = DB::select("SELECT style, season, type_name,
                                                    string_agg(process_id::text, ', ') process_id,
                                                string_agg(komponen::text, ', ') komponen,
                                                string_agg(komponen_name::text, ', ') komponen_name	
                                                FROM v_rnd_detail_process
                                                WHERE style=? and season=? and type_name=? and (process_id= ? OR process_id= ?)
                                                GROUP BY style, season, type_name", [$style, $season, $set_type,\Config::get('constants.PRINTING_TYPE'), \Config::get('constants.EMBRO_TYPE')]);

                            $printing_embro = '';

                            if ($prem_agg) {

                                $prem_agg_process = explode(',',$prem_agg[0]->process_id);
                                $prem_agg_komponen = array_map('trim', explode(',',$prem_agg[0]->komponen));
                                $newkey = 0;

                                if (in_array(\Config::get('constants.PRINTING_TYPE'),$prem_agg_process) && in_array(\Config::get('constants.EMBRO_TYPE'),$prem_agg_process)) {
                                    // dd($prem_agg_komponen);

                                    $arr_new = array();
                                    foreach ($prem_agg_komponen as $v) {

                                        $arr_new[$v][$newkey] = $v;
                                        $newkey++;
                                    }

                                    $printing_embro = count($arr_new);
                                }
                            }
                            // $printing_embro = DB::table('v_rnd_detail_process')
                            //                     ->where('style', $style)
                            //                     ->where('season', $season)
                            //                     ->where('type_name', $set_type)
                            //                     ->where( function($query){
                            //                         $query->where('process_id', \Config::get('constants.PRINTING_TYPE'));
                            //                         $query->orWhere('process_id', \Config::get('constants.EMBRO_TYPE'));
                            //                     })
                            //                     ->count();

                            // end printing & embro //

                            // pad print //
                            $pad_print = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PAD_PRINT'))
                                                ->count();

                            // end pad print //

                            // bonding //
                            $bonding = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.BONDING'))
                                                ->count();

                            // end bonding //

                            // deboss & emboss
                            $DEBOSS = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%DEBOSS%')
                                                ->count();

                            $EMBOSS = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where(DB::raw('upper(detail_process_name)'), 'LIKE', '%EMBOSS%')
                                                ->count();
                            // end deboss & emboss

                            // laser cut //
                            $laser_cut = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.LASER_CUT'))
                                                ->count();

                            // end laser cut //
                            
                            // qty panel //
                            $qty_panel = DB::table('v_master_style_ie_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->sum('total');

                            // end qty panel //

                            // qty panel size//
                            $panel_size = '';
                            $qty_panel_besar = DB::table('v_master_style_ie_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where('komponen_size', 'Besar')
                                                ->sum('total');

                            $qty_panel_kecil = DB::table('v_master_style_ie_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                ->where('komponen_size', 'Kecil')
                                                ->sum('total');

                            if ($qty_panel_besar>0) {

                                $panel_size = '('.$qty_panel_besar.') Besar';

                                if ($qty_panel_kecil>0) {
                                    $panel_size ='('.$qty_panel_besar.') Besar, ('.$qty_panel_kecil.') Kecil';
                                }
                            }else{

                                if ($qty_panel_kecil>0) {
                                    $panel_size = '('.$qty_panel_kecil.') Kecil';
                                }
                            }


                            // end qty panel size //

                            // update ad 
                                // if ($row->ad) {
                                    if ($row->updated_at) {
                                        $updateAd = Carbon::parse($row->updated_at)->format('d-m-Y');
                                    }else{
                                        $updateAd = Carbon::parse($row->created_at)->format('d-m-Y');
                                    }
                                // }else {
                                //     $updateAd = null;
                                // }
                            // end update ad

                             // remark (seri/pararel) //
                             $remark_seri = DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                // ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                // ->where('process_id', \Config::get('constants.HE_TYPE'))
                                                ->where(DB::raw('upper(remark_rnd)'), 'SERI')
                                                ->count();

                             $remark_pararel= DB::table('v_rnd_detail_process')
                                                ->where('style', $style)
                                                ->where('season', $season)
                                                ->where('type_name', $set_type)
                                                // ->where('process_id', \Config::get('constants.PRINTING_TYPE'))
                                                // ->where('process_id', \Config::get('constants.HE_TYPE'))
                                                ->where(DB::raw('upper(remark_rnd)'), 'PARAREL')
                                                ->count();

                            // end remark (seri/pararel) //

                            $set_type = $row->set_type != 'Non' ? $row->set_type : '';

                            $diff = '';
                            $notes = '';
                            if ($row->ad != null && $updateAd != null) {
                                $dateAd = Carbon::parse($row->ad);
                                $updateAdx = Carbon::parse($updateAd);

                                $diff = $dateAd->diffInDays($updateAdx);

                                $notes = 'H + '.$diff;
                            }


                            //
                            $sheet->appendRow(array(
                                $i++, $row->season_name, $row->style_standart, trim($row->style_standart.' '.$set_type), $row->product_type, $row->product_category, $row->bisnis_unit
                                , $row->main_material, $row->desc_material, $row->fabric_category, $row->fabric_group, $row->difficulty_level, $row->mp, $row->total, $row->status_style, $row->tsme, 
                                $row->asc_priority,
                                $WELT_POCKET > 0 ? $WELT_POCKET : '',
                                $FACING > 0 ? $FACING : '',
                                $COLLAR > 0 ? $COLLAR : '',
                                $PLAKET > 0 ? $PLAKET : '',
                                $STRIPE3 > 0 ? $STRIPE3 : '',
                                $OTHER_FUSE > 0 ? $OTHER_FUSE : '',
                                $TOTAL_FUSE,
                                // $WELT_POCKET_BOBOK > 0 ? $WELT_POCKET_BOBOK : '',
                                in_array('DEBOSS', $stripes) ? 1 : '',
                                in_array('TAPE', $stripes) ? 1 : '',
                                in_array('PIPING', $stripes) ? 1 : '',
                                $ADIDAS > 0 ? $ADIDAS : '',
                                $CLIMAWARM > 0 ? $CLIMAWARM : '',
                                $CLIMALITE > 0 ? $CLIMALITE : '',
                                $CLIMACOOL > 0 ? $CLIMACOOL : '',
                                $RESPONSE > 0 ? $RESPONSE : '',
                                $DHTL > 0 ? $DHTL : '',
                                $STRIPE3_HE > 0 ? $STRIPE3_HE : '',
                                $INTERLINING > 0 ? $INTERLINING : '',
                                $AEROREADY > 0 ? $AEROREADY : '',
                                $OTHER_HE > 0 ? $OTHER_HE : '',
                                $TOTAL_HE,
                                $CORP_LOGO_ADIDAS > 0 ? $CORP_LOGO_ADIDAS : '',
                                $BODY > 0 ? $BODY : '',
                                $EMBRO_EYELET > 0 ? $EMBRO_EYELET : '',
                                $TOTAL_EMBRO,
                                $STRIPE3_PRINTING1 > 0 ? $STRIPE3_PRINTING1 : '',
                                $ADIDAS_LOGO > 0 ? $ADIDAS_LOGO : '',
                                $LOGO_CLUB > 0 ? $LOGO_CLUB : '',
                                $PRINT_BODY > 0 ? $PRINT_BODY : '',
                                $TOTAL_PRINTING1,
                                $RUBBER_PRINTING > 0 ? $RUBBER_PRINTING : '',
                                $HD_PRINT > 0 ? $HD_PRINT : '',
                                $SOLVENT_PRINT > 0 ? $SOLVENT_PRINT : '',
                                $WATER_BASED_PRINT > 0 ? $WATER_BASED_PRINT : '',
                                $SUBLIMATION_PRINT > 0 ? $SUBLIMATION_PRINT : '',
                                $FLOCK_PRINT > 0 ? $FLOCK_PRINT : '',
                                $REFLECTIVE_PRINT > 0 ? $REFLECTIVE_PRINT : '',
                                $SILICON_PRINT > 0 ? $SILICON_PRINT : '',
                                $GLOSSY_PRINT > 0 ? $GLOSSY_PRINT : '',
                                $GEL_PRINT > 0 ? $GEL_PRINT : '',
                                $PHOTO_PRINT > 0 ? $PHOTO_PRINT : '',
                                $MATTE_OIL_PRINT > 0 ? $MATTE_OIL_PRINT : '',
                                $PUFF_PRINT > 0 ? $PUFF_PRINT : '',
                                $METALIC_PRINT > 0 ? $METALIC_PRINT : '',
                                $FOLL_PRINT > 0 ? $FOLL_PRINT : '',
                                $remark_seri > 0 ? $remark_seri : '',
                                $remark_pararel > 0 ? $remark_pararel : '',
                                $qty_panel > 0 ? $qty_panel : '',
                                $panel_size,
                                $printing_embro > 0 ? $printing_embro : '',
                                $pad_print > 0 ? $pad_print : '',
                                $row->engineering_fabric == 'Fabric Sublime' ? 1 : '',
                                $row->engineering_fabric == 'Zebra Special' ? 1 : '',
                                $DEBOSS > 0 ? $DEBOSS : '',
                                $EMBOSS > 0 ? $EMBOSS : '',
                                $bonding > 0 ? $bonding : '',
                                $laser_cut > 0 ? $laser_cut : '',
                                $row->critical_process,
                                $DRAWCORD > 0 ? $DRAWCORD : '',
                                $AUTO_WELT_POCKET > 0 ? $AUTO_WELT_POCKET : '',
                                $JOIN_CARE_LABEL > 0 ? $JOIN_CARE_LABEL : '',
                                $ATTACH_STRING_HANGTAG > 0 ? $ATTACH_STRING_HANGTAG : '',
                                $JOIN_CUT_ELASTIC_AUTO > 0 ? $JOIN_CUT_ELASTIC_AUTO : '',
                                $MEASURE_CUT_ELASTIC_AUTO > 0 ? $MEASURE_CUT_ELASTIC_AUTO : '',
                                $MAKE_LOOPER_AUTO > 0 ? $MAKE_LOOPER_AUTO : '',
                                $TOTAL_WELT_POCKET > 0 ? $TOTAL_WELT_POCKET : '',
                                $PANEL_FUSE_TYPE,
                                $PANEL_HE_TYPE,
                                $PANEL_EMBRO_TYPE,
                                $PANEL_PRINTING_TYPE,
                                $row->ad ? Carbon::parse($row->ad)->format('d-m-Y') : null,
                                $row->update_spt_input ? Carbon::parse($row->update_spt_input)->format('d-m-Y') : null,
                                $row->status_ad,
                                // $notes,
                                $row->min_diameter_hemning,
                                $row->max_diameter_hemning
                            ));
                        }
                    // });
                });
            })->download('xlsx');
    }
}
