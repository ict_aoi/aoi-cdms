@extends('layouts.app',['active' => 'qty_fabric_check'])

@section('page-css')
<style>
	.dt_col_hide {
		display: none;
	}

    .hidden {
		display: none;
	}

    .remove-padding-side {
        padding-right: 0px !important;
        padding-left: 0px !important;
        text-align: center !important;
        font-size: 10px !important;
    }

    .remove-margin {
        margin: 0px !important;
    }

    .remove-padding {
        padding: 0px !important;
    }

    .custom-padding {
        padding-right: 10px !important;
        padding-left: 10px !important;
        text-align: center !important;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Qty Fabric Check</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Qty Fabric Check</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        <div id="scan1" class="">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form-control input-new" style="opacity: 1;" id="barcode_fabric" name="barcode_fabric" placeholder="#Scan Barcode Fabric" autofocus autocomplete="off" />
                </div>
                <div class="col-md-4">
                    <input type="text" value="Barcode Fabric" class="form-control bg-info" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="detailData">
                <thead>
                    <tr>
                        <th>Barcode</th>
                        <th>Plan</th>
                        <th>No Roll</th>
                        <th>Lebar</th>
                        <th>Lot</th>
                        <th>Material</th>
                        <th>Qty Supply</th>
                        <th>Qty Actual</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="barcode_view">-</td>
                        <td id="plan_view">-</td>
                        <td id="no_roll_view">-</td>
                        <td id="lebar_view">-</td>
                        <td id="lot_view">-</td>
                        <td id="material_view">-</td>
                        <td id="qty_supply_view">-</td>
                        <td id="qty_actual_view">-</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>History Penggunaan</h5>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="lastActivityTable">
                <thead>
                    <tr>
                        <th>Tgl Spread</th>
                        <th>Marker</th>
                        <th>Meja</th>
                        <th>Qty Awal</th>
                        <th>Qty Sisa</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/qty_fabric_check.js') }}"></script>
@endsection