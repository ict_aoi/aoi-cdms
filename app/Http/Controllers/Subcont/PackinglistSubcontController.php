<?php

namespace App\Http\Controllers\Subcont;

use DB;
use Uuid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;

class PackinglistSubcontController extends Controller
{
    public function index()
    {
        $poreference = DB::table('detail_transaction_subcontsys')->select('poreference')->whereNull('packinglist_no')->where('factory_id',\Auth::user()->factory_id)->distinct('poreference')->get();
        $division = DB::table('master_factory')->where('status','internal')->where('factory_alias','<>','ALL')->orderBy('id','asc')->get();
        $process = DB::table('master_process')->where('locator_id',3)->whereNull('deleted_at')->orderBy('process_name','asc')->get();
        $style = DB::table('detail_transaction_subcontsys')->select('style','set_type')->whereNull('packinglist_no')->distinct()->get();
        $size = DB::table('detail_transaction_subcontsys')->select('size')->whereNull('packinglist_no')->distinct()->get();
        $packinglist_no = DB::table('packinglist_subcontsys_new')->whereNull('deleted_at')->where('factory_id',\Auth::user()->factory_id)->whereNull('printed_at')->get();
        // DB::table('temp_edit_packinglist_subcontsys')->where('edited_by',\Auth::user()->id)->delete();
        return view('subcont.packinglist.index', compact('division', 'poreference', 'process','style','size','packinglist_no'));
    }

    public function getData(Request $request)
    {
        if(request()->ajax()) 
        {
            $packinglist_no = $request->packinglist_no;
            if($packinglist_no != null){
                $data = DB::table('packinglist_subcontsys_new')
                ->where('no_packinglist',$packinglist_no)
                ->whereNull('deleted_at')
                ->where('factory_id',\Auth::user()->factory_id)
                ->get();
            }else{
                $data = DB::table('packinglist_subcontsys')
                ->whereNull('deleted_at')
                ->where('factory_id',\Auth::user()->factory_id)
                ->limit(10)
                ->get();
            }
            
            return datatables()->of($data)
            ->addColumn('remark',function ($data){
                return strtoupper($data->remark);
            })
            ->addColumn('style_set',function ($data){
                if($data->set_type == 'Non'){
                    $style_set = $data->style;
                }else{
                    $style_set = $data->style.'-'.$data->set_type;
                }
                return strtoupper($style_set);
            })
            ->addColumn('total_panel',function ($data){
                $c_panel = DB::table('detail_transaction_subcontsys')->where('packinglist_no',$data->no_packinglist)->get()->count('barcode_id');
                return $c_panel.' Panel';
            })
            ->addColumn('division',function ($data){
                $divisi = DB::table('master_process')->where('id',$data->process)->first();
                return $divisi->process_name;
            })
            ->addColumn('factory_order',function ($data){
                $factory_orders = DB::table('master_factory')->where('id',$data->factory_order)->first();
                return $factory_orders->factory_alias;
            })
            ->addColumn('action', function($data) {
                return view('subcont.packinglist._action', [
                    'model' => $data,
                    // 'edit' => route('packinglistSubcont.edit', $data->no_packinglist),
                    // 'delete' => route('packinglistSubcont.delete', $data->no_packinglist),
                    'print' => route('packinglistSubcont.print', $data->no_packinglist),
                    // 'release' => route('packinglistSubcont.release', $data->no_packinglist),
                ]);
                return 'action';
            })
            ->rawColumns(['action','total_panel','division','factory_order','style_set'])
            ->make(true);
        }
    }

    //STORE PACKINGLIST NO
    public function store(Request $request)
    {
        if(request()->ajax()) 
        {
            $no_packinglist = $request->no_packinglist_insert;
            $poreference = $request->poreference;
            $style_temp = $request->style;
            $size = $request->size;
            $no_kk = $request->kk_no;
            $process = $request->process;
            $factory_order = $request->factory_order;
            $remark = $request->remark;
            $bc_no = $request->bc_no;
            
            if(strpos($style_temp,'-TOP') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'TOP';
            }elseif(strpos($style_temp,'-BOT') > 1){
                $temp = explode('-',$style_temp);
                $style = $temp[0];
                $set_type = 'BOTTOM';
            }else{
                $style = $style_temp;
                $set_type = 'Non';
            }

            if(count($size) > 6){
                return response()->json('Jumlah Size Melebihi Kolom Yang Tersedia!',422);
            }

            $article = DB::table('data_cuttings')->select('articleno')->whereIn('po_buyer',$poreference)->pluck('articleno')->toArray();
            $article = array_unique($article);
            $no_kk = $no_kk == '' ? '-' : $no_kk;
            $bc_no =$bc_no == '' ? '-' : $bc_no;

            try {
                DB::beginTransaction();
                    DB::table('packinglist_subcontsys_new')->insert([
                        'id' => Uuid::generate()->string,
                        'no_packinglist'    => $no_packinglist,
                        'poreference'       => implode(',', $poreference),
                        'style'             => $style,
                        'set_type'          => $set_type,
                        'article'           => implode(',',$article),
                        'size'              => implode(',', $size),
                        'kk_no'             => $no_kk,
                        'process'           => $process,
                        'factory_id'        => \Auth::user()->factory_id,
                        'factory_order'     => $factory_order,
                        'remark'            => $remark,
                        'bc_no'             => $bc_no,
                        'created_at'        => Carbon::now(),
                        'created_by'        => \Auth::user()->id,
                    ]);

                    DB::table('detail_transaction_subcontsys')
                    ->whereIn('poreference',$poreference)
                    ->whereIn('size',$size)
                    ->where('style',$style)
                    ->where('set_type',$set_type)->update([
                        'packinglist_no'    => $no_packinglist,
                    ]);

                    DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }   

            return response()->json(200);
        }
    }

    public function getNoPackinglist(Request $request)
    {
        if(request()->ajax()) 
        {
            $process = $request->process;
            
            $year_value = substr(Carbon::now()->format('Y'),2,2);
            $month_value = Carbon::now()->format('m');
            $get_subcont = DB::table('master_factory')->where('id', \Auth::user()->factory_id)->first();
            $process_name = DB::table('master_process')->where('id',$process)->first();
            $initials_div = substr($process_name->process_name,0,1);
            $get_last_number = DB::table('packinglist_subcontsys_new')
            ->where('no_packinglist', 'like', '%'.substr($get_subcont->factory_alias,0,2).$initials_div.$year_value.$month_value.'%')
            ->whereNull('deleted_at')
            ->orderBy('no_packinglist', 'desc')->first();
            
            if($get_last_number == null){
                $no_packinglist = substr($get_subcont->factory_alias,0,2).$initials_div.$year_value.$month_value.'0001';
            }else{
                $no_packinglist = (int) substr(str_replace(substr($get_subcont->factory_alias,0,2).$initials_div,'',$get_last_number->no_packinglist) + 1,4,4);
                $no_packinglist = substr($get_subcont->factory_alias,0,2).$initials_div.$year_value.$month_value.sprintf("%04d", $no_packinglist);
            }
            return response()->json($no_packinglist,200);
        }
    }

    public function ajaxGetDataPackinglist(Request $request)
    {
        $term = trim(strtoupper($request->q));

        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DB::table('packinglist_subcontsys_new')->where('no_packinglist', 'like', '%'.$term.'%')->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {

            $formatted_tags[] = [
                'id' => $tag->no_packinglist, 
                'text' => $tag->no_packinglist
            ];
        }

        return \Response::json($formatted_tags);
    }

    public function ajaxGetDataPackinglisthead(Request $request)
    {
        $term = trim(strtoupper($request->q));

        if (empty($term)) {
            return \Response::json([]);
        }
        $tags = DB::table('packinglist_subcontsys_new')->where('no_packinglist', 'like', '%'.$term.'%')->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {

            $formatted_tags[] = [
                'id' => $tag->no_packinglist, 
                'text' => $tag->no_packinglist
            ];
        }

        return \Response::json($formatted_tags);
    }

    public function detailPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $data = DB::table('packinglist_subcontsys_new')
            ->where('no_packinglist', $packinglist)
            ->whereNull('printed_at')
            ->where('deleted_at', null)
            ->first();

            $process = DB::table('master_process')->where('id',$data->process)->first();
            $factory_order = DB::table('master_factory')->where('id',$data->factory_order)->first();
            
            $po_buyer = implode(', ', explode(',', $data->poreference));
            $article = implode(', ', explode(',', $data->article));

            $obj = new StdClass();
            $obj->packinglist = $data->no_packinglist;
            $obj->kk_no = $data->kk_no;
            $obj->process_name = $process->process_name;
            $obj->po_buyer = $po_buyer;
            $obj->article = $article;
            $obj->factory_res = $factory_order->factory_alias;
            
            return response()->json($obj,200);
        }
    }

    public function dataPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $data = DB::table('packinglist_subcontsys_new')->where('no_packinglist', $packinglist)->first();

            $obj = new StdClass();
            $obj->packinglist = $packinglist;
            $obj->locator = $data->factory_id;
            $obj->state = 'out';
            
            return response()->json($obj,200);
        }
    }

    public function dataComponentPackinglist(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;

            if($packinglist != null) {
                $data = DB::table('temp_scan_subcontsys')->where('packinglist_no',$packinglist)->where('factory_id',\Auth::user()->factory_id)->where('created_by',\Auth::user()->id)->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == null || $data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->rawColumns([])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function dataComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $locator = $request->locator;
            $state = $request->state;

            if($packinglist != null && $locator != null && $state != null) {
                $data = DB::table('temp_scan_subcontsys')->where('packinglist_no',$packinglist)->where('factory_id',\Auth::user()->factory_id)->where('created_by',\Auth::user()->id)->get();

                return datatables()->of($data)
                ->editColumn('style', function($data) {
                    if($data->set_type == 'Non') {
                        return $data->style.' | '.$data->article;
                    } else {
                        return $data->style.'-'.$data->set_type.' | '.$data->article;
                    }
                })
                ->editColumn('cut_num', function($data) {
                    return $data->cut_num.' / '.$data->start_no.' - '.$data->end_no;
                })
                ->addColumn('action', function($data) {
                    return view('subcont.scan_out._action', [
                        'model'      => $data,
                        'hapus'     => route('packinglistSubcont.deleteComponent', $data->barcode_id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function deleteComponent(Request $request, $id)
    {
        if(request()->ajax())
        {
            if($id != null) {
                $state = 'in';
                    $data = DB::table('temp_scan_subcontsys')->where('barcode_id', $id)->first();
                    $packinglist = DB::table('packinglist_subcontsys_new')->where('no_packinglist', $data->packinglist_no)->first();
                    DB::table('temp_scan_subcontsys')->where('barcode_id', $id)->delete();
                    // $barcode_sds_cdms = DB::table('bundle_detail')->where('barcode_id',$id)->first();
                    // if($barcode_sds_cdms != null){
                    //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds_cdms->sds_barcode)->update([
                    //         'artwork'       => null,
                    //         'artwork_pic'   => null,
                    //         'packing_list_number' => null
                    //     ]);
                    //     DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_sds_cdms->sds_barcode)->delete();
                    // }else{
                    //     DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$id)->update([
                    //         'artwork'       => null,
                    //         'artwork_pic'   => null,
                    //         'packing_list_number' => null
                    //     ]);
                    //     DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$id)->delete();
                    // }

                    $obj = new StdClass();
                    $obj->packinglist = $packinglist->no_packinglist;
                    $obj->locator = $packinglist->process;
                    $obj->state = $state;

                    return response()->json($obj,200);
            } else {
                return response()->json('Terjadi Kesalahan Silahkan Refresh Halaman Ini!',422);
            }
        }
    }

    public function scanComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist;
            $locator_pl = DB::table('packinglist_subcontsys_new')->where('no_packinglist',$packinglist)->first();
            $state = 'out';
            $locator = $locator_pl->factory_id;
            $barcode_component = $request->barcode_component;

            if($packinglist != null && $state != null && $barcode_component != null && $locator != null)
            {
                $bundle_detail = DB::table('bundle_detail')->where('barcode_id',$barcode_component)->first();
                $bundle_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                $barcode_sds_only = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                //$barcode_sds_check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();
                $process_scan_factory = DB::table('detail_packinglist_subtemp')->where('bundle_id',$barcode_component)->first();
                if($process_scan_factory == null){
                    if($bundle_detail != null && $bundle_detail->barcode_id == $barcode_component){
                        $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->where('locator_to',10)->where('status_to','out')->first();
                        $in_factory_order = DB::table('distribusi_movements')->where('barcode_id',$barcode_component)->where('locator_to',3)->where('status_to','in')->first();
                        $check_scan_subcont = DB::table('detail_transaction_subcontsys')->where('barcode_id',$barcode_component)->first();
                        if($out_cutting != null)
                        {
                            $subcont_factory = DB::table('master_factory')->where('id',\Auth::user()->factory_id)->first();
                            if($in_factory_order == null){
                                return response()->json('Bundle Belum Scan In Factory Asal!',422);
                            }
                            if($check_scan_subcont == null){
                                return response()->json('Bundle Belum Scan In '.$subcont_factory->factory_name.'!',422);
                            }elseif($check_scan_subcont->state_to == 'out'){
                                return response()->json('Bundle Sudah Menjalani Scan '.$check_scan_subcont->state_to.' '.$subcont_factory->factory_name.'!',422);
                            }
                            $part_num = DB::table('master_style_detail')->where('id',$bundle_detail->style_detail_id)->first();
                            $type_name = DB::table('types')->where('id',$part_num->type_id)->first();
                            $bundle_header = DB::table('bundle_header')->where('id',$bundle_detail->bundle_header_id)->first();
                            //$barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$bundle_detail->sds_barcode)->first();
                            if($part_num == null){
                                return response()->json('Part Number Dihapus Dari Sistem',422);
                            }
                            // if($barcode_sds->artwork_out != null){
                            //     return response()->json('Bundle Sudah Scan di SDS!',422);
                            // }
                            $get_process = explode(',', $part_num->process);

                            if($get_process[0] == null || $get_process[0] == '') {
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }
        
                            $get_locator_process = DB::table('master_process')->select('locator_id')->whereIn('id', $get_process)->groupBy('locator_id')->pluck('locator_id')->toArray();

                            if(!in_array(3, $get_locator_process)){
                                return response()->json('Barcode tidak perlu melalui proses ini!',422);
                            }

                            if($bundle_header->set_type == 'Non'){
                                $style = $bundle_header->style;
                            }else{
                                $style = $bundle_header->style;
                            }

                            $po_packinglist = DB::table('packinglist_subcontsys_new')
                            ->where('no_packinglist',$packinglist)
                            ->where('style',$style)
                            ->where('set_type',$type_name->type_name)
                            ->first();

                            $subcont_destination_check = DB::table('detail_packinglist_distribusi as dpd')
                            ->select('subcont')
                            ->leftJoin('packinglist_distribusi as pd','pd.no_packinglist','=','dpd.id_packinglist')
                            ->where('dpd.bundle_id',$barcode_component)
                            ->first();

                            $subcont_process_check = DB::table('master_factory')->where('id',$check_scan_subcont->factory_id)->first();

                            $process_art_check = DB::table('packinglist_distribusi as pd')
                            ->select('ms.division')
                            ->leftJoin('detail_packinglist_distribusi as dpd','dpd.id_packinglist','pd.no_packinglist')
                            ->leftJoin('master_subcont as ms','ms.id','=','pd.subcont_id')
                            ->where('dpd.bundle_id',$barcode_component)
                            ->first();

                            $process_art_check2 = DB::table('packinglist_subcontsys_new as ps')
                            ->select('mp.process_name')
                            ->leftJoin('detail_transaction_subcontsys as dts','dts.packinglist_no','=','ps.no_packinglist')
                            ->leftJoin('master_process as mp','mp.id','=','ps.process')
                            ->where('dts.barcode_id',$barcode_component)
                            ->first();

                            $div_name_from = $process_art_check->division == 'PRINTING' ? 'PRINT' : 'EMBRO';

                            if($process_art_check2->process_name != $div_name_from){
                                return response()->json('Bundle Bukan Untuk Process '.$process_art_check2->process_name.'!',422);
                            }

                            if($subcont_process_check->factory_alias != $subcont_destination_check->subcont){
                                return response()->json('Bundle Bukan Untuk '.$subcont_process_check->factory_alias.'!',422);
                            }

                            if($po_packinglist == null){
                                return response()->json('Style / PO Bundle bukan untuk Packinglist Ini!',422);
                            }else{
                                $po_packinglist_header = explode(',',$po_packinglist->poreference);
                                $size_packinglist = explode(',',$po_packinglist->size);
                                if(in_array($bundle_header->poreference,$po_packinglist_header) && in_array($bundle_header->size,$size_packinglist) == false){
                                    return response()->json('Bundle bukan untuk Packinglist Ini!',422);
                                }
                            }

                            $component_check = DB::table('subcont_movements')->where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                            $current_scan = DB::table('temp_scan_subcontsys')->where('barcode_id',$barcode_component)->first();
                            $pl_factory_orders = DB::table('detail_packinglist_distribusi')->where('bundle_id',$barcode_component)->first();
            
                            if($current_scan == null){
                                if($component_check != null && $component_check->description == 'out '.$subcont_factory->factory_name){
                                    return response()->json('Barcode sudah melalui scan out '.$subcont_factory->factory_name,422);
                                }else{
                                    DB::table('temp_scan_subcontsys')->insert([
                                        'id'                => Uuid::generate()->string,
                                        'barcode_id'        => $barcode_component,
                                        'poreference'       => $bundle_header->poreference,
                                        'season'            => $bundle_header->season,
                                        'style'             => $bundle_header->style,
                                        'set_type'          => $type_name->type_name,
                                        'size'              => $bundle_header->size,
                                        'komponen_name'     => $bundle_detail->komponen_name,
                                        'cut_num'           => $bundle_header->cut_num,
                                        'start_no'          => $bundle_detail->start_no,
                                        'end_no'            => $bundle_detail->end_no,
                                        'factory_id'        => \Auth::user()->factory_id,
                                        'locator'           => $locator,
                                        'state'             => $state,
                                        'created_at'        => Carbon::now(),
                                        'created_by'        => \Auth::user()->id,
                                        'factory_order'     => $bundle_header->factory_id,
                                        'article'           => $bundle_header->article,
                                        'part_num'          => $part_num->part,
                                        'qty'               => $bundle_detail->qty,
                                        'pl_factory_order'  => $pl_factory_orders->id_packinglist,
                                        'packinglist_no'    => $packinglist
                                    ]);

                                    //INSERT DATA IN TEMPORARY SCAN ARTWORK SDS
                                    // DB::connection('sds_live')->table('temp_scan_proc')->insert([
                                    //     'ap_id'         => $bundle_detail->sds_barcode,
                                    //     'proc'          => 'ART',
                                    //     'user_id'       => \Auth::user()->nik,
                                    //     'scan_time'     => Carbon::now(),
                                    //     'check_out'     => false,
                                    //     'factory'       => \Auth::user()->factory_id
                                    // ]);
                                }
                            }else{
                                return response()->json('Bundle Sedang menjalani Proses scan out '.$subcont_factory->factory_name,422);
                            }
                        }else{
                            return response()->json('Bundle Belum Scan Out Cutting!',422);
                        }
                    }
                    // elseif($bundle_detail == null && $barcode_sds_check != null && $bundle_sds_cdms != null && $barcode_component == $bundle_sds_cdms->sds_barcode){
                    //     $out_cutting = DB::table('distribusi_movements')->where('barcode_id',$bundle_sds_cdms->barcode_id)->where('locator_to',10)->where('status_to','out')->first();
                    //     if($out_cutting != null)
                    //     {
                    //         $barcode_sds_cdms = DB::table('bundle_detail')->where('sds_barcode',$barcode_component)->first();
                    //         $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_detail_id)->first();
                    //         $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                    //         $bundle_header_sds = DB::table('bundle_header')->where('id',$barcode_sds_cdms->bundle_header_id)->first();
                    //         $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                    //         if($part_num_sds == null){
                    //             return response()->json('Part Number Dihapus dari Sistem!',422);
                    //         }

                    //         if($barcode_sds->artwork_out != null){
                    //             return response()->json('Bundle Sudah Scan di SDS!',422);
                    //         }

                    //         $get_process = explode(',', $part_num_sds->process);

                    //         if($get_process[0] == null || $get_process[0] == '') {
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }
        
                    //         $get_locator_process = DB::table('master_process')
                    //         ->select('locator_id')
                    //         ->whereIn('id', $get_process)
                    //         ->groupBy('locator_id')
                    //         ->pluck('locator_id')->toArray();

                    //         if(!in_array(3, $get_locator_process)){
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }

                    //         if($bundle_header_sds->set_type == 'Non'){
                    //             $style = $bundle_header_sds->style;
                    //         }else{
                    //             $style = $bundle_header_sds->style;
                    //         }

                    //         $component_check = DistribusiMovement::where('barcode_id', $bundle_sds_cdms->barcode_id)->orderBy('created_at','desc')->first();
                    //         $current_scan = DB::table('temp_scan_out_artwork')->where('barcode_id',$bundle_sds_cdms->barcode_id)->first();

                    //         $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                    //         if(!$current_scan){
                    //             if($component_check != null && $component_check->description == 'out ARTWORK'){
                    //                 return response()->json('Barcode sudah melalui scan Out Artwork!',422);
                    //             }else{
                    //                 DB::table('temp_scan_out_artwork')->insert([
                    //                     'id'                => Uuid::generate()->string,
                    //                     'barcode_id'        => $barcode_sds_cdms->barcode_id,
                    //                     'poreference'       => $bundle_header_sds->poreference,
                    //                     'style'             => $bundle_header_sds->style,
                    //                     'set_type'          => $bundle_header_sds->set_type,
                    //                     'size'              => $bundle_header_sds->size,
                    //                     'article'           => $bundle_header_sds->article,
                    //                     'part_num'          => $part_num_sds->part,
                    //                     'komponen_name'     => $bundle_sds_cdms->komponen_name,
                    //                     'cut_num'           => $bundle_header_sds->cut_num,
                    //                     'start_no'          => $bundle_sds_cdms->start_no,
                    //                     'end_no'            => $bundle_sds_cdms->end_no,
                    //                     'qty'               => $bundle_sds_cdms->qty,
                    //                     'locator'           => $locator,
                    //                     'state'             => $state,
                    //                     'created_at'        => Carbon::now(),
                    //                     'created_by'        => $user,
                    //                     'factory_id'        => \Auth::user()->factory_id
                    //                 ]);

                    //                 //INSERT DATA IN TEMPORARY TABLE SCAN OUT ARTWORK
                    //                 // DB::connection('sds_live')->table('temp_scan_proc')->insert([
                    //                 //     'ap_id'         => $barcode_component,
                    //                 //     'proc'          => 'ART',
                    //                 //     'user_id'       => \Auth::user()->nik,
                    //                 //     'scan_time'     => Carbon::now(),
                    //                 //     'check_out'     => false,
                    //                 //     'factory'       => \Auth::user()->factory_id
                    //                 // ]);
                    //             }
                    //         }else{
                    //             return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                    //         }
                    //     }else{
                    //         return response()->json('Bundle Belum Scan Out Cutting!',422);
                    //     }
                    // }
                    // elseif($bundle_sds_cdms == null  && $barcode_sds_only != null)
                    // {
                    //     $out_cutting = DB::table('distribusi_movements')
                    //     ->where('barcode_id',$barcode_component)
                    //     ->where('locator_to',10)->where('status_to','out')->first();
                    //     if($out_cutting != null){
                    //         $barcode_sds_cdms = DB::table('sds_detail_movements')->where('sds_barcode',$barcode_component)->first();
                    //         $part_num_sds = DB::table('master_style_detail')->where('id',$barcode_sds_cdms->style_id)->first();
                    //         $type_name_sds = DB::table('types')->where('id',$part_num_sds->type_id)->first();
                    //         $bundle_header_sds = DB::table('sds_header_movements')->where('id',$barcode_sds_cdms->sds_header_id)->first();
                    //         $barcode_sds = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_component)->first();

                    //         if($part_num_sds == null){
                    //             return response()->json('Part Number Dihapus Dari Sistem!',422);
                    //         }

                    //         if($barcode_sds->artwork != null){
                    //             return response()->json('Bundle Sudah Scan di SDS!',422);
                    //         }

                    //         $get_process = explode(',', $part_num_sds->process);

                    //         if($get_process[0] == null || $get_process[0] == '') {
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }
        
                    //         $get_locator_process = DB::table('master_process')
                    //         ->select('locator_id')
                    //         ->whereIn('id', $get_process)
                    //         ->groupBy('locator_id')
                    //         ->pluck('locator_id')->toArray();

                    //         if(!in_array(3, $get_locator_process)){
                    //             return response()->json('Barcode tidak perlu melalui proses ini!',422);
                    //         }

                    //         if($bundle_header_sds->set_type == 'Non'){
                    //             $style = $bundle_header_sds->style;
                    //         }else{
                    //             $style = $bundle_header_sds->style;
                    //         }

                    //         $component_check = DistribusiMovement::where('barcode_id', $barcode_component)->orderBy('created_at','desc')->first();
                    //         $current_scan = DB::table('temp_scan_out_artwork')->where('barcode_id',$barcode_component)->first();

                    //         $temp_scan_proc = DB::connection('sds_live')->table('temp_scan_proc')->where('ap_id',$barcode_component)->first();
                    //         if(!$temp_scan_proc){
                    //             if($component_check != null && $component_check->description == 'out ARTWORK'){
                    //                 return response()->json('Barcode sudah melalui scan Out Artwork!',422);
                    //             }else{
                    //                 DB::table('temp_scan_out_artwork')->insert([
                    //                     'id'                => Uuid::generate()->string,
                    //                     'barcode_id'        => $barcode_sds_cdms->sds_barcode,
                    //                     'poreference'       => $bundle_header_sds->poreference,
                    //                     'style'             => $bundle_header_sds->style,
                    //                     'set_type'          => $type_name_sds->type_name,
                    //                     'size'              => $bundle_header_sds->size,
                    //                     'article'           => $bundle_header_sds->article,
                    //                     'part_num'          => $part_num_sds->part,
                    //                     'komponen_name'     => $barcode_sds_cdms->komponen_name,
                    //                     'cut_num'           => $bundle_header_sds->cut_num,
                    //                     'start_no'          => $barcode_sds_cdms->start_no,
                    //                     'end_no'            => $barcode_sds_cdms->end_no,
                    //                     'qty'               => $barcode_sds_cdms->qty,
                    //                     'locator'           => $locator,
                    //                     'state'             => $state,
                    //                     'created_at'        => Carbon::now(),
                    //                     'created_by'        => $user,
                    //                     'factory_id'        => \Auth::user()->factory_id
                    //                 ]);

                    //                 DB::connection('sds_live')->table('temp_scan_proc')->insert([
                    //                     'ap_id'         => $barcode_component,
                    //                     'proc'          => 'ART',
                    //                     'user_id'       => \Auth::user()->nik,
                    //                     'scan_time'     => Carbon::now(),
                    //                     'check_out'     => false,
                    //                     'factory'       => \Auth::user()->factory_id
                    //                 ]);
                    //             }
                    //         }else{
                    //             return response()->json('Bundle Sedang menjalani Proses scan Artwork!',422);
                    //         }
                    //     }else{
                    //         return response()->json('Bundle Belum Scan Out Cutting!',422);
                    //     }
                    // }
                    else{
                        return response()->json('Barcode Tidak Ditemukan!',422);
                    }
                }else{
                    return response()->json('Bundle Masih di Proses di Factory Asal!',422);
                }
            }else{
                return response()->json('Harap refresh Halaman Ini!',422);
            }
        }
    }

    public function saveComponent(Request $request)
    {
        if(request()->ajax())
        {
            $packinglist = $request->packinglist_no;
            $state = $request->state;
            $locator =$request->locator;
            if($locator != null && $packinglist != null && $state != null) {
                $data = DB::table('temp_scan_subcontsys')->where('packinglist_no', $packinglist)->get();
                if(count($data) > 0) {
                        try {
                            DB::beginTransaction();
                            $locator = DB::table('master_locator')->where('id', $locator)->first();
                            foreach($data as $x){
                                $last_locator = DB::table('temp_scan_subcontsys')->where('barcode_id',$x->barcode_id)->orderBy('created_at','desc')->first();
                                $subcont_factory = DB::table('master_factory')->where('id',$last_locator->factory_id)->first();
                                $last_movement = DB::table('subcont_movements')->where('barcode_id',$x->barcode_id)->orderBy('created_at','desc')->first();
                                $last_locator_to = $last_movement->factory_to == null ? null : $last_movement->factory_to;
                                $last_status_to = $last_movement->status_to == null ? null : $last_movement->status_to;

                                DB::table('subcont_movements')->insert([
                                    'id'            => Uuid::generate()->string,
                                    'barcode_id'    => $x->barcode_id,
                                    'factory_from'  => $last_locator_to,
                                    'status_from'   => $last_status_to,
                                    'factory_to'    => \Auth::user()->factory_id,
                                    'factory_order' => $x->factory_order,
                                    'status_to'     => $state,
                                    'description'   => $state.' '.$subcont_factory->factory_name,
                                    'user_id'       => \Auth::user()->id,
                                    'ip_address'    => $this->getIPClient(),
                                    'created_at'    => Carbon::now(),
                                    'updated_at'    => Carbon::now()
                                ]);

                                $barcode_sds = DB::table('bundle_detail')->where('barcode_id',$x->barcode_id)->first();
                                $bundle_header = DB::table('bundle_header')->where('id',$barcode_sds->bundle_header_id)->fisrt();
                                $pl_sub_id = DB::table('packinglist_subcontsys')->where('no_packinglist',$packinglist)->first();
                                if($barcode_sds == null){
                                    // DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$x->barcode_id)->update([
                                    //     'location'              => 'ART',
                                    //     'factory'               => 'AOI'.\Auth::user()->factory_id,
                                    //     'artwork_out'           => Carbon::now(),
                                    //     'artwork_out_pic'       => \Auth::user()->nik
                                    // ]);
                                    DB::table('sds_detail_movements')->where('sds_barcode',$x->barcode_id)->update([
                                        'current_location_id'   => $last_locator->factory_id,
                                        'current_status_to'     => $state,
                                        'current_description'   => $state.' '.$subcont_factory->factory_name,
                                        'updated_movement_at'   => Carbon::now(),
                                        'current_user_id'       => \Auth::user()->id,
        
                                    ]);
                                    DB::table('detail_transaction_subcontsys')->where('barcode_id',$x->barcode_id)->update([
                                        'locator_to'        => $x->locator,
                                        'state_to'          => $x->state,
                                        'updated_at'        => Carbon::now(),
                                        'updated_by'        => \Auth::user()->id,
                                        'pl_subsys_id'      => $pl_sub_id->id,
                                        'color_name'        => $bundle_header->color_name
                                    ]);
                                }else{
                                    // DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$barcode_sds->sds_barcode)->update([
                                    //     'location'              => 'ART',
                                    //     'factory'               => 'AOI'.\Auth::user()->factory_id,
                                    //     'artwork_out'           => Carbon::now(),
                                    //     'artwork_out_pic'       => \Auth::user()->nik
                                    // ]);
                                    DB::table('bundle_detail')->where('barcode_id',$x->barcode_id)->update([
                                        'current_locator_id'    => $last_locator->factory_id,
                                        'current_status_to'     => $state,
                                        'current_description'   => $state.' '.$subcont_factory->factory_name,
                                        'updated_movement_at'   => Carbon::now(),
                                        'current_user_id'       => \Auth::user()->id,
                                    ]);
                                    DB::table('detail_transaction_subcontsys')->where('barcode_id',$x->barcode_id)->update([
                                        'locator_to'      => $x->locator,
                                        'state_to'        => $x->state,
                                        'updated_at'        => Carbon::now(),
                                        'updated_by'        => \Auth::user()->id,
                                        'pl_subsys_id'      => $pl_sub_id->id,
                                        'color_name'        => $bundle_header->color_name
                                    ]);
                                }
                            }

                            DB::table('temp_scan_subcontsys')->where('packinglist_no', $packinglist)->delete();
                            //DB::connection('sds_live')->table('temp_scan_proc')->whereIn('ap_id',$barcode_arr_sds)->delete();

                            DB::commit();
                        } catch (Exception $e) {
                            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                } else {
                    return response()->json('Harap Refresh Halaman Ini!',422);
                }
            } else {
                return response()->json('Harap Refresh Halaman Ini!',422);
            }
        }
    }

    private function getIPClient()
    {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }

    //PRINT SUBCONT & INHOUSE PACKINGLIST
    public function print($id)
    {
        $packinglist = DB::table('packinglist_subcontsys_new')->where('no_packinglist', $id)->first();
        $process = DB::table('master_process')->where('id',$packinglist->process)->first();
        $filename ='packinglist_'.$packinglist->no_packinglist;
        $factory = DB::table('master_factory')->where('id',$packinglist->factory_id)->first();
        $factory_order = DB::table('master_factory')->where('id', $packinglist->factory_order)->first();

        $size = DB::table('detail_transaction_subcontsys')
        ->select('size')
        ->where('packinglist_no', $packinglist->no_packinglist)
        ->groupBy('size')
        ->orderByRaw('LENGTH(size) asc')
        ->orderBy('size', 'asc')
        ->pluck('size')
        ->toArray();

        $data_details = DB::table('detail_transaction_subcontsys')
        ->select(DB::raw('packinglist_no, style, max(created_at) as created_at, poreference, article, komponen_name,color_name,no_polybag'))
        ->where('packinglist_no', $packinglist->no_packinglist)
        ->groupBy('style', 'poreference', 'article', 'komponen_name', 'packinglist_no', 'no_polybag','color_name')
        ->orderBy('no_polybag', 'poreference')
        ->get();



        $data_qty_array = array();
        $total_qty_all = array();
        
        foreach($data_details as $detail) {
            $data_size_details = array();
            $size_detail = DB::table('detail_transaction_subcontsys')
            ->select(DB::raw('no_polybag, size, sum(qty) as qty'))
            ->where('style', $detail->style)
            ->where('article', $detail->article)
            ->where('poreference', $detail->poreference)
            ->where('komponen_name', $detail->komponen_name)
            ->where('packinglist_no', $packinglist->no_packinglist)
            ->groupBy('no_polybag', 'size')
            ->get();

            $data_size_details['data'] = $size_detail;

            $data_size_details['count'] = $size_detail->count();

            $total_temp = 0;

            foreach($size_detail as $aa) {
                $total_temp = $total_temp + $aa->qty;
            }

            $total_qty_all[] = $total_temp;

            $data_qty_array[] = $data_size_details;
        }
        
        $data_size = array();

        if(count($size) > 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } elseif(count($size) == 6) {
            for($i = 0; $i < 6; $i++) {
                $data_size[] = $size[$i];
            }
        } else {
            for($i = 0; $i < count($size); $i++) {
                $data_size[] = $size[$i];
            }
            $selisih_size = 6 - count($size);
            for($i = 0; $i < $selisih_size; $i++) {
                $data_size[] = 0;
            }
        }

        $paper = array(0,0,689.449,450.85);

        // //UPDATED PRINT BY AND PRINT AT
        DB::table('packinglist_subcontsys_new')->where('no_packinglist',$id)->update([
            'printed_at'        => Carbon::now(),
            'printed_by'        => \Auth::user()->id
        ]);

        DB::table('packinglist_subcontsys_new')->where('no_packinglist',$packinglist->no_packinglist)->update([
                'is_completed' => true,
                'updated_at'    => Carbon::now(),
                'updated_by'    => \Auth::user()->id
            ]);

        $data = [
            'no_packinglist' => $packinglist->no_packinglist,
            'kk' => $packinglist->kk_no,
            'proses'=>$process->process_name,
            'factory' => $factory_order->factory_alias,
            'sizes' => $data_size,
            'remark'    => $packinglist->remark,
            'data_details' => $data_details,
            'data_size_details' => $data_qty_array,
            'total_qty_all' => $total_qty_all,
            'created_at' => Carbon::parse($packinglist->created_at)->format('d M Y'),
        ];
        
        $pdf = \PDF::loadView('print.packinglist_subcont', $data)->setPaper($paper,'potrait');

        return $pdf->stream($filename);
    }
}
