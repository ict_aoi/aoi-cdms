@extends('layouts.app',['active' => 'approval_spreading_gl'])
@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Approval Spreading</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Approval Spreading</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-horizontal">
            <fieldset class="content-group">
                <div class="form-group" id="form-filter">
                    <label class="control-label col-lg-2 text-bold">Spreading Date</label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-icon legitRipple" type="button"><i class="icon-calendar"></i></button>
                            </span>
                            <input type="date" class="form-control pickadate" name="spread" id="spread">

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
        	<table class="table table-basic table-condensed" id="table-list" width="100%">
        		<thead>
        			<tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">Plan</th>
                        <th rowspan="2">Style</th>
                        <th rowspan="2">Article</th>
                        <th rowspan="2">Marker ID</th>
                        <th rowspan="2">Part No</th>
                        <th rowspan="2">Cut</th>
                        <th rowspan="2">Pemakaian Fabric</th>
                        <th colspan="2"><center>Approval</center></th>
                        <th rowspan="2">Table</th>
                        <th rowspan="2">Operator</th>
                        <th rowspan="2">Action</th>
                    </tr>
                    <tr>
                        <th>GL</th>
                        <th>QC</th>
                    </tr>
        		</thead>
        	</table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalApprove" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
            <div class="modal-header">
				<legend class="text-semibold">
					<i class="icon-file-text2 position-left"></i>
					<span id="title"> Spreading Approval</span>
				</legend>
			</div>
			<div class="modal-body">
                <div class="row form-group">
                	<div class="table-responsive">
						<table class="table table-basic table-condensed">
                            <tr>
                                <td><strong>Plan</strong></td>
                                <td>: <span id="plan">Data Empty!</span></td>
                                <td><strong>Marker Code</strong></td>
                                <td>: <span id="barcode_head">Data Empty!</span></td>
                                <td><strong>Operator</strong></td>
                                <td>: <span id="user">Data Empty!</span></td>
                                <td><strong>Table</strong></td>
                                <td>: <span id="table">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Style</strong></td>
                                <td>: <span id="style">Data Empty!</span></td>
                                <td><strong>Color</strong></td>
                                <td>: <span id="color">Data Empty!</span></td>
                                <td><strong>Marker Width</strong></td>
                                <td>: <span id="marker_width">Data Empty!</span></td>
                                <td><strong>Total Length</strong></td>
                                <td>: <span id="total_length">Data Empty!</span></td>
                            </tr>
                            <tr>
                                <td><strong>Article</strong></td>
                                <td>: <span id="article">Data Empty!</span></td>
                                <td><strong>Part</strong></td>
                                <td>: <span id="part">Data Empty!</span></td>
                                <td><strong>Marker Length</strong></td>
                                <td>: <span id="marker_length">Data Empty!</span></td>
                                <td><strong>Layer (Plan)</strong></td>
                                <td>: <span id="layer">Data Empty!</span> (<span id="layer_plan">Data Empty!</span>)</td>
                            </tr>
                            <tr>
                                <td><strong>Item ID</strong></td>
                                <td>: <span id="item_id">Data Empty!</span></td>
                                <td><strong>Cut</strong></td>
                                <td>: <span id="cut">Data Empty!</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><strong>PO Buyer</strong></td>
                                <td colspan="3">: <span id="po_buyer">Data Empty!</span></td>
                                <td><strong>Ratio</strong></td>
                                <td colspan="3">: <span id="ratio">Data Empty!</span></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row form-group">
                	<div class="table-responsive">
						<table class="table table-basic table-condensed" id="inputTable">
							<thead>
	                            <tr>
	                                <th rowspan="2" class="remove-padding-side">No Roll</th>
	                                <th rowspan="2" class="remove-padding-side">Lot</th>
	                                <th rowspan="2" class="remove-padding-side">Yard In Roll</th>
	                                <th rowspan="2" class="remove-padding-side">Lebar Aktual</th>
	                                <th colspan="2" class="remove-padding-side"><center>Layer</center></th>
	                                <th colspan="2" class="remove-padding-side"><center>Sisa</center></th>
	                                <th rowspan="2" class="remove-padding-side">Akumulasi Layer</th>
	                                <th rowspan="2" class="remove-padding-side">Reject</th>
	                                <th colspan="2" class="remove-padding-side">Sambungan</th>
	                                <th rowspan="2" class="remove-padding-side">Aktual Pemakaian</th>
	                                <th rowspan="2" class="remove-padding-side">Remark</th>
	                                <th rowspan="2" class="remove-padding-side">Kualitas Gelaran</th>
	                            </tr>
	                            <tr>
	                                <th class="remove-padding-side">Suplai</th>
	                                <th class="remove-padding-side">Aktual</th>
	                                <th class="remove-padding-side">Suplai</th>
	                                <th class="remove-padding-side">Aktual</th>
	                                <th class="remove-padding-side">Awal</th>
	                                <th class="remove-padding-side">Akhir</th>
	                            </tr>
	                        </thead>
						</table>
					</div>
                </div>
			</div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" id="btn-close">Close</button>
                <button type="button" class="btn btn-success" id="btn-approve" data-status="approve"  onclick="setStatus(this);">Approve</button>
                <button type="button" class="btn btn-warning" id="btn-hold" data-status="hold" onclick="setStatus(this);">Hold</button>

            </div>
		</div>
	</div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
	var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        scrollX: true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('ApprovalSpreadingGL.getApprovalSpreading') }}",
            data: function(d) {
                 return $.extend({}, d, {
                     "spread"         : $('#spread').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'cutting_date', name: 'cutting_date'},
            {data: 'style', name: 'style'},
            {data: 'articleno', name: 'articleno'},
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'part_no', name: 'part_no'},
            {data: 'cut', name: 'cut'},
            {data: 'actual_use', name: 'actual_use'},
            {data: 'gl_status', name: 'gl_status'},
            {data: 'qc_status', name: 'qc_status'},
            {data: 'table_name', name: 'table_name'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
    });

    $('#btn-filter').on('click',function(){
    	var spread = $('#spread').val();
    	if (spread.trim()!='') {
    		$.ajaxSetup({
                headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
            });
            $.ajax({
                type:'get',
                url : "{{ route('ApprovalSpreadingGL.getApprovalSpreading') }}",
                data:{spread:spread},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                	$('#table-list').unblock();
                	tableL.clear();
                	tableL.draw();
                },
                error:function(response){
                   myalert('error', response);
                   console.log(response);
                }
            });
    	}else{
    		myalert('eror','Select Planning Date First');
    	}
    });

    $('#btn-close').on('click',function(){
    	$('#btn-approve').removeClass('hidden');
        	$('#btn-hold').removeClass('hidden');
        	$('#modalApprove').modal('hide');
    });
});

function detail(e){
	var barcode = e.getAttribute('data-id');

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$.ajax({
        type:'get',
        url : "{{ route('ApprovalSpreadingGL.ajaxGetSpreadingApv') }}",
        data:{barcode:barcode},
        beforeSend:function(){
           $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
        },
        success:function(response){

        	var data = response.data;

        	$('#plan').text(data.plan);
        	$('#style').text(data.style);
        	$('#article').text(data.article);
        	$('#part').text(data.part);
        	$('#cut').text(data.cut);
        	$('#layer').text(data.layer);
        	$('#item_id').text(data.item_id);
        	$('#color').text(data.color);
        	$('#marker_width').text(data.marker_width);
        	$('#marker_length').text(data.marker_length);
        	$('#total_length').text(data.total_length);
        	$('#btn-approve').attr('data-id',data.barcode_id);
        	$('#btn-hold').attr('data-id',data.barcode_id);
            $('#barcode_head').text(data.barcode_id);
            $('#user').text(data.name);
            $('#table').text(data.table);
            $('#po_buyer').text(data.po);
            $('#ratio').text(data.ratio);
            $('#layer_plan').text(data.layer_plan);

        	if (data.gl_status=='approve') {
        		$('#btn-approve').addClass('hidden');
        		$('#btn-hold').addClass('hidden');
        	}else if(data.gl_status=='hold'){
        		$('#btn-hold').addClass('hidden');
        	}
        	tableFabricSpreading(data.barcode_id);
        	$.unblockUI();
            $('#modalApprove').modal('show');

        },
        error:function(response){
           myalert('error', response);
           console.log(response);
        }
    });


}

function tableFabricSpreading(barcode_id){
	$('#inputTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            type: 'get',
            url: "{{ route('ApprovalSpreadingGL.ajaxFabricSpreadingApv') }}",
            data: {
                barcode: barcode_id,
            }
        },
        columnDefs: [
            {
                className: 'dt-center',
            },
            {
                  className: 'text-center',
            },
        ],
        columns: [
            {data: 'no_roll', name: 'no_roll', searchable:true, visible:true, orderable:false},
            {data: 'lot', name: 'lot', searchable:true, visible:true, orderable:false},
            {data: 'qty_fabric', name: 'qty_fabric', searchable:true, visible:true, orderable:false},
            {data: 'actual_width', name: 'actual_width', searchable:true, visible:true, orderable:false},
            {data: 'suplai_layer', name: 'suplai_layer', searchable:true, visible:true, orderable:false},
            {data: 'actual_layer', name: 'actual_layer', searchable:true, visible:true, orderable:false},
            {data: 'suplai_sisa', name: 'suplai_sisa', searchable:true, visible:true, orderable:false},
            {data: 'actual_sisa', name: 'actual_sisa', searchable:true, visible:true, orderable:false},
            {data: 'akumulasi_layer', name: 'akumulasi_layer', searchable:true, visible:true, orderable:false},
            {data: 'reject', name: 'reject', searchable:true, visible:true, orderable:false},
            {data: 'sambungan', name: 'sambungan', searchable:true, visible:true, orderable:false},
            {data: 'sambungan_end', name: 'sambungan_end', searchable:true, visible:true, orderable:false},
            {data: 'actual_use', name: 'actual_use', searchable:true, visible:true, orderable:false},
            {data: 'remark', name: 'remark', searchable:true, visible:true, orderable:false},
            {data: 'kualitas_gelaran', name: 'kualitas_gelaran', searchable:true, visible:true, orderable:false},
        ],
    });

}

function setStatus(e){
	var barcode = e.getAttribute('data-id');
	var status = e.getAttribute('data-status');
    var tableI = $('#inputTable').DataTable();
    var layer = $('#layer').text();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type:'post',
        url : "{{ route('ApprovalSpreadingGL.ajaxApproveMarker') }}",
        data:{barcode:barcode,status:status,layer:layer},
        beforeSend:function(){
           $('#modalApprove').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
        },
        success:function(response){
        	$('#modalApprove').unblock();
        	var data_response = response.data;
            if (data_response.status == 200) {
                myalert('success', data_response.output);
            }else{
                myalert('error', data_response.output);
            }
            $('#plan').text('');
            $('#style').text('');
            $('#article').text('');
            $('#part').text('');
            $('#cut').text('');
            $('#layer').text('');
            $('#item_id').text('');
            $('#color').text('');
            $('#marker_width').text('');
            $('#marker_length').text('');
            $('#total_length').text('');
            $('#btn-approve').attr('data-id','');
            $('#btn-hold').attr('data-id','');
            $('#barcode_head').text('');
            $('#user').text('');
            $('#table').text('');
            $('#ratio').text('');
            $('#po_buyer').text('');
            $('#layer_plan').text('');

            tableI.clear();
            $('#btn-approve').removeClass('hidden');
        	$('#btn-hold').removeClass('hidden');
        	$('#modalApprove').modal('hide');
            $('#table-list').DataTable().ajax.reload();




        },
        error:function(response){
           myalert('error', response);
           console.log(response);
           $('#modalApprove').unblock();
        }
    });
}


</script>
@endsection
