<?php

namespace App\Models;
use App\Uuids;

use Illuminate\Database\Eloquent\Model;

class RequestFabric extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    // protected $fillable = ['plan_id', 'submit_by', 'submit_at', 'approval_whs_by', 'approval_whs_at', 'approval_lab_by', 'approval_lab_at'];

    protected $fillable     = ['plan_id', 'factory_id', 'queue', 'queue_before', 'created_by', 'updated_by','deleted_at', 'deleted_by','queue_conc'];
    protected $dates        = ['created_at','updated_at'];
}
