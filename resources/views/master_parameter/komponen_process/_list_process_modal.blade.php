<!-- MODAL ADD -->
<div id="modal_detail_process_" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
		<div class="modal-content modal-md">
            <div class="modal-header">
                {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                <h5 class="modal-title" id="title-line-number"><i class="position-left"></i>Detail Komponen Process</h5>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <fieldset>
                        <div id="list_process" class="col-lg-12">
                            <!-- template pilihan -->
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-number="2">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /MODAL ADD -->