<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TableSpreading extends Model
{
   	use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'table_spreading';
	protected $fillable = ['id_table','shift','id_spreading','work_hours','created_at','updated_at','deleted_at','date_spreading','factory_id'];
}
