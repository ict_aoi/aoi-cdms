<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('home') }}" id="home_menu"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    @permission(['menu-user','menu-role','menu-permission'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>User Management</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-permission')
                                <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Permission</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Role</a></li>
                            @endpermission
                            @permission('menu-user')
                                <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">User</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-plan', 'menu-cutting-preparation', 'menu-download-file-ssp','menu-download-marker', 'menu-report-fabric', 'menu-request-rasio-download-cutting', 'menu-actual-spreading-input','menu-approval-spreading-gl','menu-approval-spreading-qc', 'menu-report-roll', 'menu-upload-permintaan', 'menu-print-id-marker','menu-cutting', 'menu-info-kebutuhan-fabric', 'menu-geser-planning', 'menu-review-spreading-result', 'menu-cutting-barcode'])

                        @permission(['menu-plan', 'menu-cutting-preparation', 'menu-download-file-ssp','menu-download-marker', 'menu-request-rasio-download-cutting', 'menu-upload-permintaan', 'menu-print-id-marker', 'menu-geser-planning'])                   <li class="navigation-header"><span>Cutting</span> <i class="icon-menu" title="Forms"></i></li>
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-briefcase3"></i> <span>Permintaan Marker</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                @permission('menu-plan')
                                    <li class="{{ $active == 'cuttingplan' ? 'active' : '' }}"><a href="{{ route('prioritasPlan.index') }}" class="legitRipple">Prioritas Planning</a></li>
                                @endpermission
                                @permission('menu-geser-planning')
                                    <li class="{{ $active == 'plan_movement' ? 'active' : '' }}"><a href="{{ route('planMovement.index') }}" class="legitRipple">Geser Planning</a></li>
                                @endpermission
                                @permission('menu-request-rasio-download-cutting')
                                    <li class="{{ $active == 'request_rasio_download_cutting' ? 'active' : '' }}"><a href="{{ route('markerRequestRasio.indexDownload') }}" class="legitRipple">Download Permintaan SSP</a></li>
                                @endpermission
                                @permission('menu-download-file-ssp')
                                    <li class="{{ $active == 'request_rasio_upload' ? 'active' : '' }}"><a href="{{ route('downloadFileSSP.index') }}" class="legitRipple">Download SSP</a></li>
                                @endpermission
                                <!-- @permission('menu-upload-permintaan')
                                    <li class="{{ $active == 'upload_premintaan' ? 'active' : '' }}"><a href="{{ route('preparation.upload') }}" class="legitRipple">Permintaan Marker (Auto)</a></li>
                                @endpermission -->
                                @permission('menu-upload-permintaan')
                                    <li class="{{ $active == 'upload_premintaan_manual' ? 'active' : '' }}"><a href="{{ route('permintaanMarker.index') }}" class="legitRipple">Permintaan Marker (Manual)</a></li>
                                @endpermission
                                @permission('menu-download-marker')
                                    <li class="{{ $active == 'download_marker' ? 'active' : '' }}"><a href="{{ route('downloadMarker.index') }}" class="legitRipple">Download Marker</a></li>
                                @endpermission
                                @permission('menu-print-id-marker')
                                    <li class="{{ $active == 'print_id_marker' ? 'active' : '' }}"><a href="{{ route('printIDMarker.index') }}" class="legitRipple">Print Id Marker</a></li>
                                @endpermission
                                @permission('menu-upload-permintaan')
                                    <li class="{{ $active == 'fabric_prepare_whs' ? 'active' : '' }}"><a href="{{ route('preparation.fabricPrepared') }}" class="legitRipple">Report Fabric Prepared</a></li>
                                @endpermission
                                @permission('menu-upload-permintaan')
                                    <li class="{{ $active == 'fabric_outstanding' ? 'active' : '' }}"><a href="{{ route('reportFabricOS.index') }}" class="legitRipple">Report Fabric Outstanding</a></li>
                                @endpermission
                                @permission('menu-print-id-marker')
                                    <li class="{{ $active == 'laporan_baru' ? 'active' : '' }}"><a href="{{ route('laporanBaru.index') }}" class="legitRipple">Laporan Baru</a></li>
                                @endpermission
                                {{-- @permission('menu-marker-trace')
                                    <li class="{{ $active == 'marker_trace' ? 'active' : '' }}"><a href="{{ route('markerTrace.index') }}" class="legitRipple">Marker Trace</a></li>
                                @endpermission         --}}
                            </ul>
                        </li>
                        @endpermission

                        @permission(['menu-report-fabric', 'menu-report-roll', 'menu-info-kebutuhan-fabric'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-briefcase3"></i> <span>Permintaan Fabric</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                @permission('menu-info-kebutuhan-fabric')
                                    <li class="{{ $active == 'info_kebutuhan_fabric' ? 'active' : '' }}"><a href="{{ route('fabricNeed.index') }}" class="legitRipple">Info Kebutuhan Fabric</a></li>
                                @endpermission
                                @permission('menu-report-fabric')
                                    <li class="{{ $active == 'report_fabric_width' ? 'active' : '' }}"><a href="{{ route('reportFabric.index') }}" class="legitRipple">Laporan Fabric Width</a></li>
                                    <li class="{{ $active == 'report_fabric_compare' ? 'active' : '' }}"><a href="{{ route('reportFabric.indexOutstanding') }}" class="legitRipple">Laporan Fabric Compare</a></li>
                                @endpermission
                                @permission('menu-report-roll')
                                    <li class="{{ $active == 'report_roll' ? 'active' : '' }}"><a href="{{ route('reportRoll.index') }}" class="legitRipple">Laporan Per Roll</a></li>
                                @endpermission
                                @permission('menu-permintaan-fabric-submit')
                                    <li class="{{ $active == 'permintaan_fabric_submit' ? 'active' : '' }}"><a href="{{ route('reqFabricSubmit.index') }}" class="legitRipple">Permintaan Fabric</a></li>
                                @endpermission
                                @permission('menu-permintaan-fabric-submit')
                                    <li class="{{ $active == 'report_fabric_ci' ? 'active' : '' }}"><a href="{{ route('reportCI.index') }}" class="legitRipple">Daily Cutting Instruction</a></li>
                                @endpermission
                                @permission('menu-permintaan-fabric-submit')
                                    <li class="{{ $active == 'report_fabric_prepare' ? 'active' : '' }}"><a href="{{ route('reportFabricPrepare.index') }}" class="legitRipple">Daily Material Prepare</a></li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        @permission(['menu-actual-spreading-input', 'menu-review-spreading-result', 'menu-approval-spreading-gl','menu-approval-spreading-qc', 'menu-cutting-instruction-spreading', 'menu-qty-fabric-check', 'menu-marker-trace'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>Spreading</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                @permission('menu-actual-spreading-input')
                                    <li class="{{ $active == 'actual_spreading' ? 'active' : '' }}"><a href="{{ route('actualSpreading.index') }}" class="legitRipple">Scan Spreading</a></li>
                                @endpermission
                                @permission('menu-review-spreading-result')
                                    <li class="{{ $active == 'review_spreading_result' ? 'active' : '' }}"><a href="{{ route('reviewSpreadingResult.index') }}" class="legitRipple">Review Hasil Spreading</a></li>
                                @endpermission
                                @permission('menu-cutting-instruction-spreading')
                                    <li class="{{ $active == 'cutting_instruction_spreading' ? 'active' : '' }}"><a href="{{ route('SpreadingCI.index') }}" class="legitRipple">Packinglist Fabric WHS</a></li>
                                @endpermission
                                @permission('menu-qty-fabric-check')
                                    <li class="{{ $active == 'qty_fabric_check' ? 'active' : '' }}"><a href="{{ route('QtyFabricCheck.index') }}" class="legitRipple">Qty Fabric Check</a></li>
                                @endpermission
                                @permission('menu-approval-spreading-gl')
                                    <li class="{{ $active == 'approval_spreading_gl' ? 'active' : '' }}"><a href="{{ route('ApprovalSpreadingGL.approvalSpreading') }}" class="legitRipple">Approval Spreading GL</a></li>
                                @endpermission
                                @permission('menu-approval-spreading-qc')
                                    <li class="{{ $active == 'approval_spreading_qc' ? 'active' : '' }}"><a href="{{ route('ApprovalSpreadingQC.approvalSpreadingQc') }}" class="legitRipple">Approval Spreading QC</a></li>
                                @endpermission
                                @permission('menu-marker-trace')
                                    <li class="{{ $active == 'marker_trace' ? 'active' : '' }}"><a href="{{ route('markerTrace.index') }}" class="legitRipple">Marker Trace</a></li>
                                    <li class="{{ $active == 'po_trace' ? 'active' : '' }}"><a href="{{ route('poTrace.index') }}" class="legitRipple">PO Trace</a></li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission

                        @permission(['menu-scan-cutting','menu-cutting-result'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-scissors"></i> <span>Cutting</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                @permission('menu-scan-cutting')
                                    <li class="{{ $active == 'scan_cutting' ? 'active' : '' }}"><a href="{{ route('ScanCutting.index') }}" class="legitRipple">Scan Cutting</a></li>
                                @endpermission
                                @permission('menu-cutting-result')
                                    <li class="{{ $active == 'cutting_result' ? 'active' : '' }}"><a href="{{ route('CuttingResult.index') }}" class="legitRipple">Cutting Result</a></li>
                                @endpermission
                            </ul>
                        </li>
                        @endpermission
                    @endpermission
                    @permission(['menu-cutting-barcode'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-barcode2"></i> <span>Bundling</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'cuttingBarcode' ? 'active' : '' }}"><a href="{{ route('cuttingbarcode.index') }}" class="legitRipple">Generate Ticket</a></li>
                            <li class="{{ $active == 'manual_create' ? 'active' : '' }}"><a href="{{ route('manual_create.index') }}" class="legitRipple">Manual Create</a></li>
                            <li class="{{ $active == 'manual_barcode' ? 'active' : '' }}"><a href="{{ route('manual_barcode.index') }}" class="legitRipple">RePrint Bundle</a></li>
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-request-rasio-download','menu-request-rasio-upload','menu-download-file-ssp','menu-actual-marker','menu-upload-marker', 'menu-upload-file-ssp','menu-release-marker','menu-laporan-baru-marker'])
                    <li class="navigation-header"><span>Pola Marker</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-stackoverflow"></i> <span>Permintaan Marker</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-request-rasio-download')
                                <li class="{{ $active == 'request_rasio_download' ? 'active' : '' }}"><a href="{{ route('markerRequestRasio.indexDownload') }}" class="legitRipple">Download Permintaan SSP</a></li>
                            @endpermission
                            @permission('menu-upload-file-ssp')
                                <li class="{{ $active == 'upload_file_ssp' ? 'active' : '' }}"><a href="{{ route('UploadFileSSP.index') }}" class="legitRipple">Upload SSP</a></li>
                            @endpermission
                            @permission('menu-download-file-ssp')
                                <li class="{{ $active == 'request_rasio_upload' ? 'active' : '' }}"><a href="{{ route('downloadFileSSP.index') }}" class="legitRipple">Download SSP</a></li>
                            @endpermission
                            @permission('menu-actual-marker')
                                <li class="{{ $active == 'actual_marker' ? 'active' : '' }}"><a href="{{ route('actualMarker.index') }}" class="legitRipple">Permintaan Marker</a></li>
                            @endpermission
                            @permission('menu-upload-marker')
                                <li class="{{ $active == 'upload_marker' ? 'active' : '' }}"><a href="{{ route('uploadMarker.index') }}" class="legitRipple">Upload Marker</a></li>
                            @endpermission
                            @permission('menu-release-marker')
                                <li class="{{ $active == 'release_marker' ? 'active' : '' }}"><a href="{{ route('ReleaseMarker.index') }}" class="legitRipple">Release Marker</a></li>
                            @endpermission
                            @permission('menu-laporan-baru-marker')
                                <li class="{{ $active == 'laporan_baru_marker' ? 'active' : '' }}"><a href="{{ route('laporanBaruMarker.index') }}" class="legitRipple">Laporan Baru</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-scan-component','menu-scan-component-subcont', 'replacement-bundle'])
                    <li class="navigation-header"><span>Distribusi</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-move-alt1"></i> <span>Component Movement</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission('menu-scan-component')
                                {{-- <li class="{{ $active == 'scan_component' ? 'active' : '' }}"><a href="{{ route('ScanComponent.index') }}" class="legitRipple">Scan Component</a></li> --}}
                                <li class="{{ $active == 'bundle_check' ? 'active' : '' }}"><a href="{{ route('bundleCheck.index') }}" class="legitRipple">Bundle Check</a></li>
                            @endpermission
                            {{-- @permission('menu-scan-component-subcont')
                                <li class="{{ $active == 'scan_component_subcont' ? 'active' : '' }}"><a href="{{ route('ScanComponentSubcont.index') }}" class="legitRipple">Scan Component Subcont Out</a></li>
                            @endpermission
                            @permission('menu-scan-component-subcont-in')
                                <li class="{{ $active == 'scan_component_subcont' ? 'active' : '' }}"><a href="{{ route('ScanComponentSubcontIn.index') }}" class="legitRipple">Scan Component Subcont In</a></li>
                            @endpermission --}}
                        </ul>
                    </li>
                    <li class="">
                        @permission('menu-switch-locator-supply')
                        <a href="{{ route('switch_locator.index') }}" class="legitRipple"><i class=" icon-flip-vertical4"></i> <span>Edit Supply Line</span></a>
                        @endpermission
                    </li>
                    @permission(['replacement-bundle'])
                    <li class="">
                    <li class="{{ $active == 'replacement_bundle' ? 'active' : '' }}"><a href="{{ route('replacement.index') }}"><i class="icon-split"></i> <span>Replacement Bundle</span></a></li>
                    </li>
                    @endpermission
                    @endpermission

                    @permission(['menu-distribusi-artwork'])
                    <li class="navigation-header"><span>Distribusi Artwork</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-shredder"></i> <span>Packinglist</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'packinglist_distribusi_inhouse' ? 'active' : '' }}"><a href="{{ route('packinglistDistribusi.inhouse_index') }}" class="legitRipple">Packinglist - Inhouse</a></li>
                            <li class="{{ $active == 'packinglist_distribusi' ? 'active' : '' }}"><a href="{{ route('packinglistDistribusi.index') }}" class="legitRipple">Packinglist - Subcont</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-picassa2"></i> <span>Scan</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'distribusi_artwork_in' ? 'active' : '' }}"><a href="{{ route('distribusi_artworkin.index') }}" class="legitRipple">In</a></li>
                            <li class="{{ $active == 'distribusi_artwork_out' ? 'active' : '' }}"><a href="{{ route('distribusi_artwork.index') }}" class="legitRipple">Out</a></li>
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-packinglist-subcont','menu-scan-component-subcontsys'])
                    <li class="navigation-header"><span>Subcont</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-shredder"></i> <span>Packinglist</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'packinglist_subcont' ? 'active' : '' }}"><a href="{{ route('packinglistSubcont.index') }}" class="legitRipple">Packinglist</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-picassa2"></i> <span>Scan</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'scan_subcont_out' ? 'active' : '' }}"><a href="{{ route('scanSubcontOut.index') }}" class="legitRipple">Out</a></li>
                            <li class="{{ $active == 'scan_subcont_in' ? 'active' : '' }}"><a href="{{ route('scanSubcontIn.index') }}" class="legitRipple">In</a></li>
                        </ul>
                    </li>
                    @endpermission
                    @permission(['menu-trf-lab'])
                    <li class="navigation-header"><span>Material Testing LAB</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-lab"></i> <span>Fabric Testing</span></a>
                        <ul class="hidden-ul" style="display: none;">
                        <li class="{{ $active == 'fabric_testing' ? 'active' : '' }}"><a href="{{ route('lab_fabric.index') }}" class="legitRipple">Fabric Testing</a></li>
                        </ul>
                    </li>
                    @endpermission

                    <!-- @permission(['menu-kontrak-kerja'])
                    <li class="navigation-header"><span>Kontrak Kerja</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-newspaper"></i> <span>Master Kontrak Kerja</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'master_kk' ? 'active' : '' }}"><a href="{{ route('MasterKK.masterKontrak') }}" class="legitRipple">Create Header KK</a></li>
                        </ul>
                    </li>
                    @endpermission -->

                    @permission(['menu-second-plan'])
                    <li class="navigation-header"><span>Special Access</span> <i class="icon-menu" title="Forms"></i></li>
                        <li class="{{ $active == 'second_plan' ? 'active' : '' }}"><a href="{{ route('secondPlan.index') }}" id="home_menu"><i class="icon-home4"></i> <span>Second Plan</span></a></li>
                    </li>
                    @endpermission

                    @permission(['menu-master-parameter','menu-master-setting'])
                    <li class="navigation-header"><span>Master Data</span> <i class="icon-menu" title="Forms"></i></li>
                        @permission(['menu-master-parameter'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-server"></i> <span>Master Spreading</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                 <li class="{{ $active == 'master_spreading' ? 'active' : '' }}"><a href="{{ route('masterparam') }}" class="legitRipple">Master Spreading</a></li>
                                 <li class="{{ $active == 'master_table' ? 'active' : '' }}"><a href="{{ route('masterparam.masterTable') }}" class="legitRipple">Master Table</a></li>
                                 <li class="{{ $active == 'master_set_table' ? 'active' : '' }}"><a href="{{ route('masterparam.masterSetTable') }}" class="legitRipple">Master Setting Table</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-server"></i> <span>Master Style&Komponen</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                 <li class="{{ $active == 'masterLocator' ? 'active' : '' }}"><a href="{{ route('masterparam.masterLocator') }}" class="legitRipple">Master Locator</a></li>
                                 <li class="{{ $active == 'masterProcess' ? 'active' : '' }}"><a href="{{ route('masterparam.masterProcess') }}" class="legitRipple">Master Process</a></li>
                                 <li class="{{ $active == 'masterKomponen' ? 'active' : '' }}"><a href="{{ route('masterparam.masterKomponen') }}" class="legitRipple">Master Komponen</a></li>
                                 <li class="{{ $active == 'masterDetailProcess' ? 'active' : '' }}"><a href="{{ route('masterparam.masterDetailProcess') }}" class="legitRipple">Master Detail Process</a></li>
                                 <li class="{{ $active == 'masterBisnisUnit' ? 'active' : '' }}"><a href="{{ route('masterparam.masterBisnisUnit') }}" class="legitRipple">Master Bisnis Unit</a></li>
                                 <li class="{{ $active == 'masterListStyle' ? 'active' : '' }}"><a href="{{ route('masterparam.masterListStyle') }}" class="legitRipple">Master RnD</a></li>
                                 <li class="{{ $active == 'masterStyleProduction' ? 'active' : '' }}"><a href="{{ route('masterparam.masterStyleProduction') }}" class="legitRipple">Master Style Production</a></li>
                                 <li class="{{ $active == 'masterStyle' ? 'active' : '' }}"><a href="{{ route('masterparam.masterStyle') }}" class="legitRipple">Master Style IE</a></li>
                                 <li class="{{ $active == 'masterKomponenProcess' ? 'active' : '' }}"><a href="{{ route('masterparam.masterKomponenProcess') }}" class="legitRipple">Master Komponen Process</a></li>
                                 <li class="">
                                    <a href="#" class="has-ul legitRipple"><span>Master Machine</span></a>
                                    <ul class="hidden-ul" style="display: none;">
                                        <li class="{{ $active == 'master_machine_type' ? 'active' : '' }}"><a href="{{ route('master_machine_type.index') }}" class="legitRipple">Type</a></li>
                                        <li class="{{ $active == 'machine_details' ? 'active' : '' }}"><a href="{{ route('machine_details.index') }}" class="legitRipple">Detail</a></li>
                                    </ul>
                                 </li>
                            </ul>
                        </li>
                        @endpermission

                        @permission(['menu-master-setting'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-server"></i> <span>Master Setting</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                 <li class="{{ $active == 'master_setting' ? 'active' : '' }}"><a href="{{ route('Asetting.index') }}" class="legitRipple">Setting Area</a></li>
                            </ul>
                        </li>
                        @endpermission

                        @permission(['master-subcont-factory'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-server"></i> <span>Master Subcont Factory</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                <li class="{{ $active == 'subcontfactory' ? 'active' : '' }}"><a href="{{ route('subcont_factory.subcontfactory') }}" class="legitRipple">Create Subcont Name</a></li>
                            </ul>
                        </li>
                        @endpermission

                        @permission(['master-defect-name'])
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class="icon-server"></i> <span>Master Defect Name</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                <li class="{{ $active == 'masterDefectName' ? 'active' : '' }}"><a href="{{ route('masterparam.masterDefectName') }}" class="legitRipple">Create Defect Name</a></li>
                            </ul>
                        </li>
                        @endpermission

                    @endpermission


                    @permission(['menu-master-ppcm'])
                        <li class="navigation-header"><span>PPC</span> <i class="icon-menu" title="Forms"></i></li>
                        <li class="{{ $active == 'master_ppcm' ? 'active' : '' }}">
                            <a href="{{ route('MasterPpcm.index') }}"><i class="icon-joomla"></i> <span>Upload PPCM</span></a>
                        </li>
                        <li class="{{ $active == 'doc_kk_ppc' ? 'active' : '' }}">
                        <a href="{{ route('DocKK.index') }}"><i class="icon-stack"></i> <span>Upload Doc KK</span></a>
                        </li>
                    @endpermission

                    @permission(['menu-exim'])
                        <li class="navigation-header"><span>Exim</span> <i class="icon-menu" title="Forms"></i></li>
                        <li class="">
                            <a href="#" class="has-ul legitRipple"><i class=" icon-certificate"></i> <span>Document BC IN & OUT</span></a>
                            <ul class="hidden-ul" style="display: none;">
                                <li class="{{ $active == 'document_bc' ? 'active' : '' }}"><a href="{{ route('Exim.index') }}" class="legitRipple">Upload Document BC</a></li>
                            </ul>
                        </li>
                    @endpermission

                    @permission(['menu-qc-check','menu-qc-component','menu-qc-reject'])
                    <li class="navigation-header"><span>QC CHECK</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-search4"></i> <span>QC Check</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            @permission(['menu-qc-check'])
                                    <li class="{{ $active == 'qc_check' ? 'active' : '' }}"><a href="{{ route('Qccheck.index') }}" class="legitRipple">QC Check</a></li>
                                    <li class="{{ $active == 'qc_bundle' ? 'active' : '' }}"><a href="{{ route('Qccheck.qcBundle') }}" class="legitRipple">QC Check<span class="label bg-danger-400">Version 2.0</span></a></li>
                                    <li class="{{ $active == 'qc_approve' ? 'active' : '' }}"><a href="{{ route('Qccheck.qcApprove') }}" class="legitRipple">QC Approve</a></li>
                            @endpermission
                            @permission(['menu-qc-component'])
                                <li class="{{ $active == 'qc_component' ? 'active' : '' }}"><a href="{{ route('ComponenQC.index') }}" class="legitRipple">QC Component</a></li>
                            @endpermission
                            @permission(['menu-approve-qc-check'])
                                <li class="{{ $active == 'approve_qc_check' ? 'active' : '' }}"><a href="{{ route('Qccheck.approveQcCheck')}}" class="legitRipple">Approve QC Check</a></li>
                            @endpermission
                            @permission(['menu-qc-check'])
                                <li class="{{ $active == 'info_fir' ? 'active' : '' }}"><a href="{{ route('infoFIR.index') }}" class="legitRipple">Info FIR</a></li>
                            @endpermission
                            @permission(['menu-qc-reject'])
                                <li class="{{ $active == 'qc_reject' ? 'active' : '' }}"><a href="{{ route('Qccheck.qcReject') }}" class="legitRipple">Ganti Reject</a></li>
                            @endpermission
                            <li class="{{ $active == 'review_qc_v4' ? 'active' : '' }}"><a href="{{ route('Qccheck.reviewQcV4') }}" class="legitRipple">Review QC Check</a></li> {{-- V4 --}}
                        </ul>
                    </li>
                    @endpermission

                    {{-- <!-- menu kontrak kerja by naufal -->
                    @permission(['menu-kontrak-kerja'])
                    <li class="navigation-header"><span>Subcont</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-infinite"></i> <span>Kontrak Kerja</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'master_kk' ? 'active' : '' }}"><a href="{{ route('MasterKK.masterKontrak') }}" class="legitRipple">Master Kontrak Kerja</a></li>
                            <li class="{{ $active == 'report_kk' ? 'active' : '' }}"><a href="{{ route('MasterKK.LaporanKK') }}" class="legitRipple">Laporan Kontrak Kerja</a></li>
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-packinglist-subcont'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-newspaper"></i> <span>Packinglist Subcont</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'packinglist_kk' ? 'active' : '' }}"><a href="{{ route('MasterKK.packingListKK') }}" class="legitRipple">Packing List</a></li>
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-scan-subcont'])
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-target2"></i> <span>Scan Subcont</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'scanout_subcont' ? 'active' : '' }}"><a href="{{ route('Subcont.scanOut') }}" class="legitRipple">Scan Out</a></li>
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-second-plan'])
                    <li class="navigation-header"><span>Special Access</span> <i class="icon-menu" title="Forms"></i></li>
                        <li class="{{ $active == 'second_plan' ? 'active' : '' }}"><a href="{{ route('secondPlan.index') }}" id="home_menu"><i class="icon-home4"></i> <span>Second Plan</span></a></li>
                    </li>
                    @endpermission --}}

                    <!-- Menu Report -->

                    @permission(['menu-report'])
                    <li class="navigation-header"><span>Report</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-stack-empty"></i> <span>Marker</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'report_serah_terima' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexSerahTerima') }}" class="legitRipple">Laporan Serah Terima</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-stackoverflow"></i> <span>Spreading</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'report_spreading_rekonsiliasi' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexRekonsiliasi') }}" class="legitRipple">Laporan Rekonsiliasi</a></li>
                            <li class="{{ $active == 'report_spreading_fabric_per_roll' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexFabricRoll') }}" class="legitRipple">Laporan Fabric Per Roll</a></li>
                            <li class="{{ $active == 'report_spreading_data_potong' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexDataPotong') }}" class="legitRipple">Laporan Data Potong</a></li>
                            <li class="{{ $active == 'report_qc_spreading' ? 'active' : '' }}"><a href="{{ route('reportSpreading.reportQcSpreading') }}" class="legitRipple">Laporan QC Spreading</a></li>
                            <li class="{{ $active == 'report_output_spreading' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexOutput') }}" class="legitRipple">Laporan Output Spreading</a></li>
                            <li class="{{ $active == 'report_downtime_spreading' ? 'active' : '' }}"><a href="{{ route('reportSpreading.indexDowntime') }}" class="legitRipple">Laporan Downtime</a></li>
                            <li class="{{ $active == 'report_saving_fabric' ? 'active' : '' }}"><a href="{{ route('savingfabricssp.index') }}" class="legitRipple">Laporan Balance Fabric (SSP)</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-scissors"></i> <span>Cutting</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'report_downtime_cutting' ? 'active' : '' }}"><a href="{{ route('reportCutting.indexDowntime') }}" class="legitRipple">Laporan Downtime</a></li>
                            <li class="{{ $active == 'report_perimeter_cutting' ? 'active' : '' }}"><a href="{{ route('reportCutting.indexPerimeter') }}" class="legitRipple">Laporan Perimeter</a></li>
                            <li class="{{ $active == 'report_scan_cutting' ? 'active' : '' }}"><a href="{{ route('reportCutting.indexCutting') }}" class="legitRipple">Laporan Cutting</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-search4"></i> <span>QC Check</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'report_qc_check' ? 'active' : '' }}"><a href="{{ route('reportQc.reportQc') }}" class="legitRipple">Laporan QC Check</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-display4"></i> <span>RnD</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'reportRnd' ? 'active' : '' }}"><a href="{{ route('reportRnd.index') }}" class="legitRipple">Laporan RnD</a></li>
                            <li class="{{ $active == 'reportPpa2' ? 'active' : '' }}"><a href="{{ route('reportKitt.ppa2') }}" class="legitRipple">Laporan KITT</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#" class="has-ul legitRipple"><i class="icon-truck"></i> <span>Component Movement</span></a>
                        <ul class="hidden-ul" style="display: none;">
                            <li class="{{ $active == 'bundle_movement_report' ? 'active' : '' }}"><a href="{{ route('reportComponentMovement.indexBundleMovement') }}" class="legitRipple">Laporan Bundle Movement</a></li>
                            <li class="{{ $active == 'bundle_output_report' ? 'active' : '' }}"><a href="{{ route('reportComponentMovement.indexBundleOutput') }}" class="legitRipple">Laporan Bundle Output</a></li>
                            <li class="{{ $active == 'pds_report' ? 'active' : '' }}"><a href="{{ route('pds_report.index') }}" class="legitRipple">Laporan PDS</a></li>
                            <li class="{{ $active == 'po_summary' ? 'active' : '' }}"><a href="{{ route('po_summary.index') }}" class="legitRipple">PO Summary</a></li>
                            <li class="{{ $active == 'po_monitoring' ? 'active' : '' }}"><a href="{{ route('po_monitoring.index') }}" class="legitRipple">PO Detail Monitoring</a></li>
                        </ul>
                    </li>
                    @endpermission
                    @permission(['report-artwork-movement'])
                    <li class="">
                        <a href="{{ route('distribusi_artwork.indexArtworkmovement') }}"><i class="icon-paint-format {{ $active == 'distribusi_artwork_report' ? 'active' : '' }}"></i> <span>Artwork</span></a>
                    </li>
                    @endpermission
                    <!-- SPECIAL ACCESS -->
                    {{--@permission(['ticket-bundle-removal','ticket-bundle-restore'])
                    <li class="navigation-header"><span>Special Access</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="{{ $active == 'bundle_removal' ? 'active' : '' }}">
                        <a href="{{ route('bundle_removal.index') }}"><i class="icon-trash"></i> <span>Bundle Removal</span></a>
                    </li>
                    <li class="{{ $active == 'bundle_restore' ? 'active' : '' }}">
                        <a href="{{ route('bundle_restore.index') }}"><i class="icon-database-arrow"></i> <span>Bundle Restore</span></a>
                    </li>
                    @endpermission--}}
                    @permission(['data-mo-sewing'])
                    <li class="navigation-header"><span>SEWING</span> <i class="icon-menu" title="Forms"></i></li>
                    <li class="{{ $active == 'data_sewing' ? 'active' : '' }}">
                        <a href="{{ route('data_sewing.index') }}"><i class="icon-stack-text"></i> <span>Data Sewing</span></a>
                    </li>
                    @endpermission
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
