<?php

namespace App\Http\Controllers\Sewing;

use DB;
use Auth;
use StdClass;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Data\DataSewing;
use App\Models\Data\DataCutting;

class DataSewingController extends Controller
{
    public function index()
    {
        return view('data_sewing.index');
    }

    public function getDataMoSewing(Request $request)
    {
        if($request->ajax()){
            $start_sewing = $request->start_sewing;
            $data = DataSewing::select('desc_produksi','season','style','articleno','start_sewing')
                ->where('start_sewing',$start_sewing)
                ->where('factory_id',Auth::user()->factory_id)
                ->distinct()
                ->orderBy('desc_produksi','ASC')->get();
    
            return DataTables::of($data)
            ->editColumn('desc_produksi',function($data){
                $line = explode(' ',$data->desc_produksi);
                if(count($line) > 2){
                    return 'LINE'.strtoupper($line[2]);
                }else{
                    if(strlen($line[1]) == 5){
                        $line_num = explode('line',$line[1]);
                        return 'LINE0'.$line_num[1];
                    }else{
                        return strtoupper($line[1]);
                    }
                }
            })
            ->addColumn('po_buyer',function($data){
                $po_buyer = DataSewing::select('po_buyer')->where('desc_produksi',$data->desc_produksi)->where('start_sewing',$data->start_sewing)
                ->where('factory_id',Auth::user()->factory_id)
                ->where('style',$data->style)
                ->where('articleno',$data->articleno)
                ->distinct('po_buyer')->pluck('po_buyer')->toArray();
                return implode(', ',$po_buyer);
            })
            ->addColumn('action', function($data){
                if($data->desc_produksi != null){
                    $data_id = $data->desc_produksi.'|'.$data->style.'|'.$data->articleno;
                    return '<a href="'.route('data_sewing.detailLine', $data->desc_produksi).'" data-id="'.$data_id.'" class="ignore-click detail btn btn-info btn-raised btn-xs"><i class="icon-eye2"></i></a>';
                }
            })
            ->rawColumns(['action','desc_produksi','po_buyer'])
            ->make(true);
        }
    }

    public function updateMosewing(Request $request){
        if($request->ajax()){
            $start_sewing = Carbon::createFromFormat('d M, Y',$request->start_sewing)->format('Y-m-d');
            $factory_id = Auth::user()->factory_id;
            if($factory_id == '1'){
                $warehouse_id = '1000005';
            }elseif($factory_id == '2'){
                $warehouse_id = '1000012';
            }else{
                $warehouse_id = '1000048';
            }
            
            $data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
            CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
            WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
            WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
            ELSE 'VOIDED'::TEXT
            END AS status_ori,
            mo.created,
            NULL::text AS date_allocation,
            whs.name as warehouse,
            mo.datepromised,
            mpo.recycle,
            fg.kst_season as season
            FROM pp_order_bomline boml
            LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
            LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
            LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
            LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
            LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
            JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
            LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
            LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
            LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
            LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
            LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
            JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
            LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
            LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
            LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
            LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
            LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
            LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
            WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) = '$start_sewing' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP'"));
            if(count($data) > 0){
                foreach($data as $d => $vl){
                    $existing_data = DataSewing::where('start_sewing',$start_sewing)
                    ->where('size_finish_good',$vl->width_mpo)
                    ->where(function($query) use ($vl){
                        $query->where('po_buyer',$vl->po_buyer);
                        $query->orWhere('job_no','like','%'.$vl->po_buyer.'%');
                    })
                    ->where('style',$vl->style)
                    ->where('season',$vl->season)
                    ->where('factory_id',Auth::user()->factory_id);
    
                    $updated_data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
                    CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
                    WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
                    WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
                    ELSE 'VOIDED'::TEXT
                    END AS status_ori,
                    mo.created,
                    NULL::text AS date_allocation,
                    whs.name as warehouse,
                    mo.datepromised,
                    mpo.recycle,
                    fg.kst_season as season
                    FROM pp_order_bomline boml
                    LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
                    LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
                    LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
                    LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
                    LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
                    JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
                    LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
                    LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
                    LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
                    LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
                    LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
                    JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
                    LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
                    LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
                    LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
                    LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
                    LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
                    LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
                    WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) = '$start_sewing' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season' AND smpo.value='$vl->po_buyer' OR od.kst_joborder LIKE '%$vl->po_buyer%' AND mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) = '$start_sewing' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season'"));
                    if(count($existing_data->get()) == count($updated_data)){
                        $existing_data = $existing_data->get();
                        foreach($existing_data as $ed){
                            if($ed->po_buyer != $vl->po_buyer){
                                try{
                                    DB::begintransaction();
                                        $old_po = $existing_data->first();
                                        if($old_po != null){
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $old_po->old_po.','.$vl->po_buyer
                                            ]);
                                        }else{
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $vl->po_buyer
                                            ]);
                                        }
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }else{
                                try{
                                    DB::begintransaction();
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }
                        }
                    }else{
                        if(str_contains($vl->warehouse,'AOI 1')){
                            $warehouse_id   = '1000005';
                            $factory_id     = 1;
                        }elseif(str_contains($vl->warehouse,'AOI 2')){
                            $warehouse_id   = '1000012';
                            $factory_id     = 2;
                        }else{
                            $warehouse_id   = '1000048';
                            $factory_id     = 3;
                        }
                        try{
                            DB::begintransaction();
                                // DELETE OLD MO
                                $existing_data->delete();
                                //INSERT WITH NEW ENTRY MO
                                DataSewing::firstOrcreate([
                                    'documentno'            => $vl->documentno,
                                    'season'                => $vl->season,
                                    'style'                 => $vl->style,
                                    'po_buyer'              => $vl->po_buyer,
                                    'job_no'                => $vl->job_no,
                                    'customer'              => $vl->customer,
                                    'destination'           => $vl->dest,
                                    'ordered_qty'           => (int)$vl->qtyordered,
                                    'material'              => $vl->material,
                                    'product_category'      => $vl->product_category,
                                    'cons'                  => $vl->cons,
                                    'fbc'                   => $vl->fbc,
                                    'uom'                   => $vl->uom,
                                    'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                    'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                    'c_order_id'            => $vl->c_order_id,
                                    'warehouse_id'          => $warehouse_id,
                                    'warehouse'             => $vl->warehouse,
                                    'factory_id'            => $factory_id,
                                    'cust_no'               => $vl->custno,
                                    'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                    'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                    'upc'                   => $vl->upc,
                                    'code_raw_material'     => $vl->code_raw_material,
                                    'desc_raw_material'     => $vl->desc_raw_material,
                                    'color_code_material'   => $vl->color_code_raw_material,
                                    'width_size'            => $vl->width_size,
                                    'desc_produksi'         => $vl->desc_produksi,
                                    'mo_updated_at'         => $vl->updated,
                                    'ts_lc'                 => $vl->kst_tslc,
                                    'articleno'             => $vl->kst_articleno,
                                    'm_product_id'          => $vl->m_product_id,
                                    'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                    'size_finish_good'      => $vl->width_mpo
                                ]);
                            DB::commit();
                        }catch (Exception $e) {
                            DB::rollback();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }else{
                return response()->json('Tidak Ada Update MO Hari Ini!',422);
            }
        }
    }

    public function updateMosewing3(Request $request){
        if($request->ajax()){
            $from   = date(Carbon::now()->subDays(3)->format('Y-m-d'));
            $to     = date(Carbon::now()->addDays(3)->format('Y-m-d'));
            $factory_id = Auth::user()->factory_id;
            if($factory_id == '1'){
                $warehouse_id = '1000005';
            }elseif($factory_id == '2'){
                $warehouse_id = '1000012';
            }else{
                $warehouse_id = '1000048';
            }
            
            $data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
            CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
            WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
            WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
            ELSE 'VOIDED'::TEXT
            END AS status_ori,
            mo.created,
            NULL::text AS date_allocation,
            whs.name as warehouse,
            mo.datepromised,
            mpo.recycle,
            fg.kst_season as season
            FROM pp_order_bomline boml
            LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
            LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
            LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
            LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
            LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
            JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
            LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
            LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
            LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
            LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
            LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
            JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
            LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
            LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
            LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
            LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
            LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
            LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
            WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP'"));
            if(count($data) > 0){
                foreach($data as $d => $vl){
                    $existing_data = DataSewing::whereBetween('start_sewing',[$from,$to])
                    ->where('size_finish_good',$vl->width_mpo)
                    ->where(function($query) use ($vl){
                        $query->where('po_buyer',$vl->po_buyer);
                        $query->orWhere('job_no','like','%'.$vl->po_buyer.'%');
                    })
                    ->where('style',$vl->style)
                    ->where('season',$vl->season)
                    ->where('factory_id',Auth::user()->factory_id);
    
                    $updated_data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
                    CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
                    WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
                    WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
                    ELSE 'VOIDED'::TEXT
                    END AS status_ori,
                    mo.created,
                    NULL::text AS date_allocation,
                    whs.name as warehouse,
                    mo.datepromised,
                    mpo.recycle,
                    fg.kst_season as season
                    FROM pp_order_bomline boml
                    LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
                    LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
                    LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
                    LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
                    LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
                    JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
                    LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
                    LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
                    LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
                    LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
                    LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
                    JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
                    LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
                    LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
                    LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
                    LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
                    LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
                    LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
                    WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season' AND smpo.value='$vl->po_buyer' OR od.kst_joborder LIKE '%$vl->po_buyer%' AND mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season'"));
                    if(count($existing_data->get()) == count($updated_data)){
                        $existing_data = $existing_data->get();
                        foreach($existing_data as $ed){
                            if($ed->po_buyer != $vl->po_buyer){
                                try{
                                    DB::begintransaction();
                                        $old_po = $existing_data->first();
                                        if($old_po != null){
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $old_po->old_po.','.$vl->po_buyer
                                            ]);
                                        }else{
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $vl->po_buyer
                                            ]);
                                        }
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }else{
                                try{
                                    DB::begintransaction();
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }
                        }
                    }else{
                        if(str_contains($vl->warehouse,'AOI 1')){
                            $warehouse_id   = '1000005';
                            $factory_id     = 1;
                        }elseif(str_contains($vl->warehouse,'AOI 2')){
                            $warehouse_id   = '1000012';
                            $factory_id     = 2;
                        }else{
                            $warehouse_id   = '1000048';
                            $factory_id     = 3;
                        }
                        try{
                            DB::begintransaction();
                                // DELETE OLD MO
                                $existing_data->delete();
                                //INSERT WITH NEW ENTRY MO
                                DataSewing::firstOrcreate([
                                    'documentno'            => $vl->documentno,
                                    'season'                => $vl->season,
                                    'style'                 => $vl->style,
                                    'po_buyer'              => $vl->po_buyer,
                                    'job_no'                => $vl->job_no,
                                    'customer'              => $vl->customer,
                                    'destination'           => $vl->dest,
                                    'ordered_qty'           => (int)$vl->qtyordered,
                                    'material'              => $vl->material,
                                    'product_category'      => $vl->product_category,
                                    'cons'                  => $vl->cons,
                                    'fbc'                   => $vl->fbc,
                                    'uom'                   => $vl->uom,
                                    'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                    'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                    'c_order_id'            => $vl->c_order_id,
                                    'warehouse_id'          => $warehouse_id,
                                    'warehouse'             => $vl->warehouse,
                                    'factory_id'            => $factory_id,
                                    'cust_no'               => $vl->custno,
                                    'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                    'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                    'upc'                   => $vl->upc,
                                    'code_raw_material'     => $vl->code_raw_material,
                                    'desc_raw_material'     => $vl->desc_raw_material,
                                    'color_code_material'   => $vl->color_code_raw_material,
                                    'width_size'            => $vl->width_size,
                                    'desc_produksi'         => $vl->desc_produksi,
                                    'mo_updated_at'         => $vl->updated,
                                    'ts_lc'                 => $vl->kst_tslc,
                                    'articleno'             => $vl->kst_articleno,
                                    'm_product_id'          => $vl->m_product_id,
                                    'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                    'size_finish_good'      => $vl->width_mpo
                                ]);
                            DB::commit();
                        }catch (Exception $e) {
                            DB::rollback();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }else{
                return response()->json('Tidak Ada Update MO Hari Ini!',422);
            }
        }
    }

    public function updateMosewing5(Request $request){
        if($request->ajax()){
            $from   = date(Carbon::now()->subDays(5)->format('Y-m-d'));
            $to     = date(Carbon::now()->addDays(5)->format('Y-m-d'));
            $factory_id = Auth::user()->factory_id;
            if($factory_id == '1'){
                $warehouse_id = '1000005';
            }elseif($factory_id == '2'){
                $warehouse_id = '1000012';
            }else{
                $warehouse_id = '1000048';
            }
            
            $data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
            CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
            WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
            WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
            ELSE 'VOIDED'::TEXT
            END AS status_ori,
            mo.created,
            NULL::text AS date_allocation,
            whs.name as warehouse,
            mo.datepromised,
            mpo.recycle,
            fg.kst_season as season
            FROM pp_order_bomline boml
            LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
            LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
            LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
            LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
            LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
            JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
            LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
            LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
            LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
            LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
            LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
            LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
            JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
            LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
            LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
            LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
            LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
            LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
            LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
            WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP'"));
            if(count($data) > 0){
                foreach($data as $d => $vl){
                    $existing_data = DataSewing::whereBetween('start_sewing',[$from,$to])
                    ->where('size_finish_good',$vl->width_mpo)
                    ->where(function($query) use ($vl){
                        $query->where('po_buyer',$vl->po_buyer);
                        $query->orWhere('job_no','like','%'.$vl->po_buyer.'%');
                    })
                    ->where('style',$vl->style)
                    ->where('season',$vl->season)
                    ->where('factory_id',Auth::user()->factory_id);
    
                    $updated_data = DB::connection('erp_live')->select(DB::RAW("SELECT DISTINCT boml.ad_client_id,boml.ad_org_id,mo.documentno,fg.upc AS style,od.kst_joborder AS job_no,od.poreference AS po_buyer,bp.name AS customer,c.name AS dest,fg.value AS product,boml.kst_part AS part_no,mo.qtyordered,mp.value AS material,pc.name AS product_category,boml.qtybom AS cons,boml.qtybom * mo.qtyordered AS fbc,uo.name AS uom,mo.datestartschedule,boml.ispiping,mo.docstatus,od.c_order_id,pc.iswipkst,pc.isfgkst,cd.name AS color,mo.s_resource_id,mo.m_warehouse_id,bp.value AS custno,od.kst_statisticaldate,od.kst_lcdate,mp.upc,a.value AS code_raw_material,a.name AS desc_raw_material,cd.value AS color_code_raw_material,b.value AS width_size,wo.name AS desc_produksi,mo.updated,wo.kst_tslc,fg.kst_articleno,mpo.m_product_id,mpo.value,smpo.value AS width_mpo,cmpo.value AS color_mpo,mo.description AS description_mo,
                    CASE WHEN mo.qtyordered = mo.qtydelivered THEN 'Selesai ORI'::TEXT
                    WHEN mo.qtyordered > mo.qtydelivered AND mo.qtydelivered > 0::numeric THEN 'Proses ORI'::text
                    WHEN mo.qtydelivered = 0::numeric THEN 'Belum ORI'::TEXT
                    ELSE 'VOIDED'::TEXT
                    END AS status_ori,
                    mo.created,
                    NULL::text AS date_allocation,
                    whs.name as warehouse,
                    mo.datepromised,
                    mpo.recycle,
                    fg.kst_season as season
                    FROM pp_order_bomline boml
                    LEFT JOIN pp_order mo ON boml.pp_order_id = mo.pp_order_id
                    LEFT JOIN ad_workflow wo ON mo.ad_workflow_id = wo.ad_workflow_id
                    LEFT JOIN c_orderline ol ON mo.c_orderline_id = ol.c_orderline_id
                    LEFT JOIN c_order od ON ol.c_order_id = od.c_order_id
                    LEFT JOIN c_country c ON od.c_country_id = c.c_country_id
                    JOIN c_bpartner bp ON od.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_bpartner_location bpl ON bpl.c_bpartner_id = bp.c_bpartner_id
                    LEFT JOIN c_location lc ON lc.c_location_id = bpl.c_location_id
                    LEFT JOIN c_country ct ON ct.c_country_id = lc.c_country_id
                    LEFT JOIN m_product fg ON mo.m_product_id = fg.m_product_id
                    LEFT JOIN m_product mp ON mp.m_product_id = boml.m_product_id
                    LEFT JOIN m_product_category a ON mp.m_product_category_id = a.m_product_category_id
                    LEFT JOIN kst_width b ON mp.kst_width_id = b.kst_width_id
                    JOIN m_product_category pc ON fg.m_product_category_id = pc.m_product_category_id
                    LEFT JOIN c_uom uo ON uo.c_uom_id = boml.c_uom_id
                    LEFT JOIN kst_colordetails cd ON cd.kst_colordetails_id = mp.kst_colordetails_id
                    LEFT JOIN m_product mpo ON mpo.m_product_id = ol.m_product_id
                    LEFT JOIN kst_sourcesize smpo ON smpo.kst_sourcesize_id = mpo.kst_sourcesize_id
                    LEFT JOIN kst_colordetails cmpo ON cmpo.kst_colordetails_id = mpo.kst_colordetails_id
                    LEFT JOIN m_warehouse whs on mo.m_warehouse_id = whs.m_warehouse_id
                    WHERE mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season' AND smpo.value='$vl->po_buyer' OR od.kst_joborder LIKE '%$vl->po_buyer%' AND mo.docstatus = 'CO' and pc.isfgkst = 'Y' and date(mo.datestartschedule) BETWEEN '$from' AND '$to' AND whs.m_warehouse_id='$warehouse_id' and a.value='WIP' AND od.poreference = '$vl->po_buyer' AND smpo.value='$vl->width_mpo' AND fg.upc='$vl->style' AND fg.kst_season='$vl->season'"));
                    if(count($existing_data->get()) == count($updated_data)){
                        $existing_data = $existing_data->get();
                        foreach($existing_data as $ed){
                            if($ed->po_buyer != $vl->po_buyer){
                                try{
                                    DB::begintransaction();
                                        $old_po = $existing_data->first();
                                        if($old_po != null){
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $old_po->old_po.','.$vl->po_buyer
                                            ]);
                                        }else{
                                            DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'po_buyer'      => $vl->po_buyer,
                                                'old_po'        => $vl->po_buyer
                                            ]);
                                        }
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->orWhere('job_no','like','%'.$vl->po_buyer.'%')
                                            ->orWhere('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }else{
                                try{
                                    DB::begintransaction();
                                        DataSewing::where('po_buyer',$vl->po_buyer)
                                            ->where('size_finish_good',$vl->width_mpo)
                                            ->where('style',$vl->style)
                                            ->where('season',$vl->season)
                                            ->where('factory_id',Auth::user()->factory_id)->update([
                                                'documentno'            => $vl->documentno,
                                                'job_no'                => $vl->job_no,
                                                'customer'              => $vl->customer,
                                                'destination'           => $vl->dest,
                                                'ordered_qty'           => (int)$vl->qtyordered,
                                                'material'              => $vl->material,
                                                'product_category'      => $vl->product_category,
                                                'cons'                  => $vl->cons,
                                                'fbc'                   => $vl->fbc,
                                                'uom'                   => $vl->uom,
                                                'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                                'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                                'c_order_id'            => $vl->c_order_id,
                                                'warehouse_id'          => $warehouse_id,
                                                'warehouse'             => $vl->warehouse,
                                                'factory_id'            => $factory_id,
                                                'cust_no'               => $vl->custno,
                                                'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                                'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                                'upc'                   => $vl->upc,
                                                'code_raw_material'     => $vl->code_raw_material,
                                                'desc_raw_material'     => $vl->desc_raw_material,
                                                'color_code_material'   => $vl->color_code_raw_material,
                                                'width_size'            => $vl->width_size,
                                                'desc_produksi'         => $vl->desc_produksi,
                                                'mo_updated_at'         => $vl->updated,
                                                'ts_lc'                 => $vl->kst_tslc,
                                                'articleno'             => $vl->kst_articleno,
                                                'm_product_id'          => $vl->m_product_id,
                                                'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                            ]);
                                    DB::commit();
                                }catch (Exception $e) {
                                    DB::rollback();
                                    $message = $e->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }
                        }
                    }else{
                        if(str_contains($vl->warehouse,'AOI 1')){
                            $warehouse_id   = '1000005';
                            $factory_id     = 1;
                        }elseif(str_contains($vl->warehouse,'AOI 2')){
                            $warehouse_id   = '1000012';
                            $factory_id     = 2;
                        }else{
                            $warehouse_id   = '1000048';
                            $factory_id     = 3;
                        }
                        try{
                            DB::begintransaction();
                                // DELETE OLD MO
                                $existing_data->delete();
                                //INSERT WITH NEW ENTRY MO
                                DataSewing::firstOrcreate([
                                    'documentno'            => $vl->documentno,
                                    'season'                => $vl->season,
                                    'style'                 => $vl->style,
                                    'po_buyer'              => $vl->po_buyer,
                                    'job_no'                => $vl->job_no,
                                    'customer'              => $vl->customer,
                                    'destination'           => $vl->dest,
                                    'ordered_qty'           => (int)$vl->qtyordered,
                                    'material'              => $vl->material,
                                    'product_category'      => $vl->product_category,
                                    'cons'                  => $vl->cons,
                                    'fbc'                   => $vl->fbc,
                                    'uom'                   => $vl->uom,
                                    'start_sewing'          => Carbon::parse($vl->datestartschedule)->format('Y-m-d'),
                                    'is_piping'             => $vl->ispiping == 'Y' ? true : false,
                                    'c_order_id'            => $vl->c_order_id,
                                    'warehouse_id'          => $warehouse_id,
                                    'warehouse'             => $vl->warehouse,
                                    'factory_id'            => $factory_id,
                                    'cust_no'               => $vl->custno,
                                    'statistical_date'      => Carbon::parse($vl->kst_statisticaldate)->format('Y-m-d'),
                                    'lc_date'               => Carbon::parse($vl->kst_lcdate)->format('Y-m-d'),
                                    'upc'                   => $vl->upc,
                                    'code_raw_material'     => $vl->code_raw_material,
                                    'desc_raw_material'     => $vl->desc_raw_material,
                                    'color_code_material'   => $vl->color_code_raw_material,
                                    'width_size'            => $vl->width_size,
                                    'desc_produksi'         => $vl->desc_produksi,
                                    'mo_updated_at'         => $vl->updated,
                                    'ts_lc'                 => $vl->kst_tslc,
                                    'articleno'             => $vl->kst_articleno,
                                    'm_product_id'          => $vl->m_product_id,
                                    'is_recycle'            => $vl->recycle == 'Y' ? true : false,
                                    'size_finish_good'      => $vl->width_mpo
                                ]);
                            DB::commit();
                        }catch (Exception $e) {
                            DB::rollback();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }else{
                return response()->json('Tidak Ada Update MO Hari Ini!',422);
            }
        }
    }

    public function detailLine(Request $request){
        $line_id = $request->line_id;
        return response()->json($line_id, 200);
    }

    public function getDataDetail(Request $request){
        if(request()->ajax()){
            $line_id        = explode('|',$request->line_id);
            $line           = $line_id[0];
            $style          = $line_id[1];
            $articleno      = $line_id[2];
            $start_sewing   = $request->start_sewing;
            $data = DataSewing::select('season','style','articleno','po_buyer','size_finish_good')
                ->where('start_sewing',$start_sewing)
                ->where('factory_id',Auth::user()->factory_id)
                ->where('desc_produksi',$line)
                ->where('style',$style)
                ->where('articleno',$articleno)
                ->orderBy('po_buyer')
                ->get();
    
            return DataTables::of($data)

            ->rawColumns([])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
}
