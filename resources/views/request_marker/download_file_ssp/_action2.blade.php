@if(is_null($model->terima_user_id))
    @if (isset($terima))
        <a href='#' onclick="terima('{!! $terima !!}')"  class="btn btn-success btn-icon"><i class="icon-checkmark-circle2"></i></a>
    @endif
@else
    @if (isset($download))
        <a href='#' onclick="unduh('{!! $download !!}')"  class="btn btn-primary btn-icon"><i class="icon-folder-download3"></i></a>
    @endif
@endif