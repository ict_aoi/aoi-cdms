<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Excel;

class QccheckController extends Controller
{
    public function index(){
    	return view('report/qccheck/index');
    }

    public function exportDataQc(Request $req){
    	$date_range = explode('-', preg_replace('/\s+/', '',$req->date));
    	$from = date_format(date_create($date_range[0]),'Y-m-d');
    	$to = date_format(date_create($date_range[1]),'Y-m-d');
    	$factory = Auth::user()->factory_id;
    	$data = DB::table('ns_report_qc')
    					->whereBetween('created_at',[$from,$to])
    					->where('factory_id',$factory)
    					->orderBy('created_at','asc')
    					->orderBy('barcode_id','asc')
    					->orderBy('komponen_id','asc')
    					->get();
    	$filename = "AOI".$factory."_report_qc_date_".$from."-".$to;
    	return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){

                    $sheet->setCellValue('A1','Tanggal');
                    $sheet->setCellValue('B1','Barcode Marker');
                    $sheet->setCellValue('C1','Cut');
					$sheet->setCellValue('D1','Style');
					$sheet->setCellValue('E1','Warna');
					$sheet->setCellValue('F1','PO Buyer');
					$sheet->setCellValue('G1','Item');
					$sheet->setCellValue('H1','PO Supplier');
					$sheet->setCellValue('I1','QTY PO');
					$sheet->setCellValue('J1','QTY Check');
					$sheet->setCellValue('K1','Komponen');
					$sheet->setCellValue('L1','Bowing / Twisting');
					$sheet->setCellValue('M1','Uneven Dye / Shading');
					$sheet->setCellValue('N1','Pilling');
					$sheet->setCellValue('O1','Snag/Pull/Pick');
					$sheet->setCellValue('P1','Slub');
					$sheet->setCellValue('Q1','Crease Mark');
					$sheet->setCellValue('R1','Foregn Yarn');
					$sheet->setCellValue('S1','Missing Yarn');
					$sheet->setCellValue('T1','Shade Bar');
					$sheet->setCellValue('U1','Exposure Stitching on Fabric');
					$sheet->setCellValue('V1','Hole');
					$sheet->setCellValue('W1','Ditry mark/ Chalk / Pen/Pensil Mark/Rust');
					$sheet->setCellValue('X1','Stain');
					$sheet->setCellValue('Y1','Printing Defect');
					$sheet->setCellValue('Z1','Streak Line');
					$sheet->setCellValue('AA1','Dye Spot');
					$sheet->setCellValue('AB1','Shrinkage');
					$sheet->setCellValue('AC1','Slide Cutting');
					$sheet->setCellValue('AD1','Wrong Facr Side');
					$sheet->setCellValue('AE1','Tag Pin Failure');
					$sheet->setCellValue('AF1','Poor Cutting');
					$sheet->setCellValue('AG1','Needle Hole');
					$sheet->setCellValue('AH1','Fabric Join Failure');
					$sheet->setCellValue('AI1','Snagging Cut');
					$sheet->setCellValue('AJ1','Wrong Size');
					$sheet->setCellValue('AK1','Remark');
					$sheet->setCellValue('AL1','Total Defect');
					$sheet->setCellValue('AM1','NIK Operator');
					$sheet->setCellValue('AN1','Nama Operator');
                    $sheet->setCellValue('AO1','Status Recycle');
					$i = 1;
					foreach ($data as $key => $vl) {
						$rw = $i+1;
						$detcutplan = DB::table('ns_detail_cutplan')
											->join('cutting_marker','ns_detail_cutplan.id_plan','=','cutting_marker.id_plan')
											->whereNull('cutting_marker.deleted_at')
											->where('cutting_marker.barcode_id',$vl->barcode_id)
											->select('ns_detail_cutplan.style','ns_detail_cutplan.po_buyer')
											->first();
						$komponen = DB::table('master_komponen')->where('id',$vl->komponen_id)->whereNull('deleted_at')->first();
						$user = DB::table('users')->where('id',$vl->user_id)->first();
                        // $is_recycle = DB::table('ns_report_qc')->where()
						$total = $vl->bowing+$vl->sliding+$vl->pilling+$vl->snag+$vl->siult+$vl->crease_mark+$vl->foresign_yam+$vl->mising_yam+$vl->shide_bar+$vl->exposure+$vl->hole+$vl->dirly_mark+$vl->stain+$vl->printing_defact+$vl->streak_line+$vl->dye_spot+$vl->shrinkage+$vl->slide_cutting+$vl->wrong_fice+$vl->tag_pin+$vl->poor_cutting+$vl->needle_hole+$vl->fabric_join+$vl->snaging_cut+$vl->wrong_fice+$vl->tag_pin+$vl->poor_cutting+$vl->needle_hole+$vl->fabric_join+$vl->snaging_cut+$vl->wrong_size;
						$sheet->setCellValue('A'.$rw,$vl->created_at);
						$sheet->setCellValue('B'.$rw,$vl->barcode_id);
						$sheet->setCellValue('C'.$rw,$vl->cut);
						$sheet->setCellValue('D'.$rw,$detcutplan->style);
						$sheet->setCellValue('E'.$rw,$vl->color);
						$sheet->setCellValue('F'.$rw,$detcutplan->po_buyer);
						$sheet->setCellValue('G'.$rw,$vl->item_code);
						$sheet->setCellValue('H'.$rw,$vl->po_supplier);
						$sheet->setCellValue('I'.$rw,$vl->qty_po);
						$sheet->setCellValue('J'.$rw,$vl->qty_check);
						$sheet->setCellValue('K'.$rw,$komponen->komponen_name);
						$sheet->setCellValue('L'.$rw,$vl->bowing);
						$sheet->setCellValue('M'.$rw,$vl->sliding);
						$sheet->setCellValue('N'.$rw,$vl->pilling);
						$sheet->setCellValue('O'.$rw,$vl->snag);
						$sheet->setCellValue('P'.$rw,$vl->siult);
						$sheet->setCellValue('Q'.$rw,$vl->crease_mark);
						$sheet->setCellValue('R'.$rw,$vl->foresign_yam);
						$sheet->setCellValue('S'.$rw,$vl->mising_yam);
						$sheet->setCellValue('T'.$rw,$vl->shide_bar);
						$sheet->setCellValue('U'.$rw,$vl->exposure);
						$sheet->setCellValue('V'.$rw,$vl->hole);
						$sheet->setCellValue('W'.$rw,$vl->dirly_mark);
						$sheet->setCellValue('X'.$rw,$vl->stain);
						$sheet->setCellValue('Y'.$rw,$vl->printing_defact);
						$sheet->setCellValue('Z'.$rw,$vl->streak_line);
						$sheet->setCellValue('AA'.$rw,$vl->dye_spot);
						$sheet->setCellValue('AB'.$rw,$vl->shrinkage);
						$sheet->setCellValue('AC'.$rw,$vl->slide_cutting);
						$sheet->setCellValue('AD'.$rw,$vl->wrong_fice);
						$sheet->setCellValue('AE'.$rw,$vl->tag_pin);
						$sheet->setCellValue('AF'.$rw,$vl->poor_cutting);
						$sheet->setCellValue('AG'.$rw,$vl->needle_hole);
						$sheet->setCellValue('AH'.$rw,$vl->fabric_join);
						$sheet->setCellValue('AI'.$rw,$vl->snaging_cut);
						$sheet->setCellValue('AJ'.$rw,$vl->wrong_size);
						$sheet->setCellValue('AK'.$rw,$vl->remark);
                        $sheet->setCellValue('AL'.$rw,$total);
                        $sheet->setCellValue('AM'.$rw,$user->nik);
                        $sheet->setCellValue('AN'.$rw,$user->name);
                        $sheet->setCellValue('AO'.$rw,$vl->is_recycle);
						$i++;
					}
                });
                $excel->setActiveSheetIndex(0);
          })->export('xlsx');
    }

    public function exportDataQcCutting(Request $req){
    	$date_range = explode('-', preg_replace('/\s+/', '',$req->date));
    	$from       = date_format(date_create($date_range[0]),'Y-m-d');
    	$to         = date_format(date_create($date_range[1]),'Y-m-d');
    	$factory    = Auth::user()->factory_id;

        $data = DB::select(db::raw("SELECT * FROM get_report_qc_cutting_new('".$factory."','$from', '$to')"));

    	$filename = "AOI".$factory."_report_qc_date_".$from."-".$to;
    	return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){

                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '0',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'J' => '@',
                        'K' => '@',
                        'L' => '@',
                        'M' => '0',
                        'N' => '@',
                        'O' => '@',
                        'P' => '@',
                        'Q' => '0',
                        'R' => '@',
                        'S' => '@',
                        'T' => '@',
                        'U' => '@',
                        'V' => '@',
                        'W' => '@',
                        'X' => '@',
                        'Y' => '@',
                    ));

                    $sheet->setCellValue('A1','Tanggal');
                    $sheet->setCellValue('B1','Barcode Marker');
                    $sheet->setCellValue('C1','Cut');
					$sheet->setCellValue('D1','Style');
					$sheet->setCellValue('E1','Warna');
					$sheet->setCellValue('F1','Item');
					$sheet->setCellValue('G1','PO Supplier');
					$sheet->setCellValue('H1','Barocde Bundle');
					$sheet->setCellValue('I1','PO Buyer');
                    $sheet->setCellValue('J1','Size');
					$sheet->setCellValue('K1','Part-Komponen');
					$sheet->setCellValue('L1','Sticker');
					$sheet->setCellValue('M1','Qty Bundle');
					$sheet->setCellValue('N1','Recycle');
					$sheet->setCellValue('O1','Defect');
					$sheet->setCellValue('P1','Lokasi');
					$sheet->setCellValue('Q1','Qty Defect');
					$sheet->setCellValue('R1','Tgl Ditemukan');
					$sheet->setCellValue('S1','Ditemukan Oleh');
					$sheet->setCellValue('T1','Tgl Reject');
					$sheet->setCellValue('U1','Ganti Reject Oleh');
					$sheet->setCellValue('V1','Tgl Approve');
					$sheet->setCellValue('W1','Approve Oleh');
                    $sheet->setCellValue('X1','Defect Cutting');
                    $sheet->setCellValue('Y1','Defect Fabric');
					$i = 1;
					foreach ($data as $key => $vl) {
						$rw = $i+1;
						$sheet->setCellValue('A'.$rw,$vl->created_at);
						$sheet->setCellValue('B'.$rw,$vl->barcode_marker);
						$sheet->setCellValue('C'.$rw,$vl->cut_num);
						$sheet->setCellValue('D'.$rw,$vl->set_type == '0' ? $vl->style : $vl->style.'-'.$vl->set_type);
						$sheet->setCellValue('E'.$rw,$vl->color);
						$sheet->setCellValue('F'.$rw,$vl->item_code);
						$sheet->setCellValue('G'.$rw,$vl->po_supplier);
						$sheet->setCellValue('H'.$rw,"'".$vl->barcode_id);
						$sheet->setCellValue('I'.$rw,$vl->poreference);
						$sheet->setCellValue('J'.$rw,$vl->size);
						$sheet->setCellValue('K'.$rw,$vl->component_name);
						$sheet->setCellValue('L'.$rw,$vl->stiker);
						$sheet->setCellValue('M'.$rw,$vl->qty_bundle);
						$sheet->setCellValue('N'.$rw,$vl->is_recycle);
						$sheet->setCellValue('O'.$rw,$vl->defect_name);
						$sheet->setCellValue('P'.$rw,$vl->name);
						$sheet->setCellValue('Q'.$rw,$vl->qty);
						$sheet->setCellValue('R'.$rw,$vl->created_at);
						$sheet->setCellValue('S'.$rw,$vl->user_qc);
						$sheet->setCellValue('T'.$rw,$vl->repaired_at);
						$sheet->setCellValue('U'.$rw,$vl->user_reject);
						$sheet->setCellValue('V'.$rw,$vl->confirmed_at);
						$sheet->setCellValue('W'.$rw,$vl->user_approve);
                        $sheet->setCellValue('X'.$rw,$vl->defect_cutting);
                        $sheet->setCellValue('Y'.$rw,$vl->defect_fabric);
						$i++;
					}
                });
                $excel->setActiveSheetIndex(0);
          })->export('xlsx');
    }
}
