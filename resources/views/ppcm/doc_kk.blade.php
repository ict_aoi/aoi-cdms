@extends('layouts.app',['active' => 'doc_kk_ppc'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Document KK PPC </span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Document KK PPC</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="tabbable">
            <ul class="nav nav-tabs">
				<li class="active"><a href="#basic-tab1" data-toggle="tab" class="legitRipple" aria-expanded="true">By Date Upload</a></li>
				<li class=""><a href="#basic-tab2" data-toggle="tab" class="legitRipple" aria-expanded="false">By KK Number</a></li>
			</ul>
            <div class="tab-content">
                <div class="tab-pane active" id="basic-tab1">
                    <div class="row form-group ">
                        <div class="col-lg-10">
                            <label><b>Date Upload</b></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <div class="col-md-12">
                                <input type="text" class="form-control daterange-basic" value="" name="date_range" id="date_range">
                                </div>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" id="btn-filter">Filter</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button style="margin-top: 26px;" type="button" class="btn btn-default" id="btn-upload"><span class="icon-cloud-upload2"></span> Upload File</button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="table-responsive">
                            <table class="table table-basic table-condensed" id="table-list">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Season</th>
                                        <th>Style</th>
                                        <th>Panel</th>
                                        <th>Remark</th>
                                        <th>Qty</th>
                                        <th>Allowance</th>
                                        <th>Qty Allowance</th>
                                        <th>Price</th>
                                        <th>Total Amount</th>
                                        <th>KK No - Type</th>
                                        <th>Factory</th>
                                        <th>User</th>
                                        <th>Dokumen Number</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>  
                    </div>
                </div>
                <div class="tab-pane" id="basic-tab2">
                    <div class="row form-group ">
                        <div class="col-md-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-search4"></i></span>
                                <input type="text" class="form-control" name="kk_no" id="kk_no">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" id="btn-filter2">Filter</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control bg-info" placeholder="KK NO" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="table-responsive">
                            <table class="table table-basic table-condensed" id="table-list2">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Season</th>
                                        <th>Style</th>
                                        <th>Remark</th>
                                        <th>Qty</th>
                                        <th>KK No - Type</th>
                                        <th>Factory</th>
                                        <th>User</th>
                                        <th>Dokumen Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalUpload"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4>Upload Document KK PPC</h4></center>
            </div>
            <div class="modal-body">
                <form action="{{ route('DocKK.UploadKK') }}" method="POST" enctype="multipart/form-data" id="form-upload">
                    @csrf
                    <div class="row form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-1">
                            <label style="margin-top: 10px;"><b>File : </b></label>
                        </div>
                        <div class="col-lg-2">
                            <input type="file" name="file_upload" class="form-control" id="file_upload">
                        </div>
                        <div class="col-lg-1">
                            <button class="btn btn-success" type="submit" id="btn-save"><span class="icon-cloud-upload2"></span> Upload File</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" action="{{ route('DocKK.updateQty') }}" id="form-update" enctype="multipart/form-data">
			@csrf
            <div class="modal-content">
            <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                <div class="panel-body loader-area">
                    <fieldset>
                        <legend class="text-semibold">
                            <i class="icon-file-text2 position-left"></i>
                            <span id="title"> UPDATE QUOTA BOOKING</span>
                            </legend>
							<input type="hidden" class="form-control" id="header_id" name="header_id" readonly>
                        <div class="form-group">
                        <label class="control-label col-lg-2 text-bold">QTY</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="qty" name="qty"></input>
                            </div>
                        </div>
                        <div class="form-group text-center">
                        <button type="submit" class="btn btn-success">Update <i class="icon-floppy-disk position-right"></i></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="icon-reload-alt position-right"></i></button>
                        </div>
                    </fieldset>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
$(document).ready(function(){
  
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        pageLength:50,
        scrollX:true,
        scrollY:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('DocKK.getDataDockk') }}",
            data: function (d) {
                return $.extend({},d,{
                    "date_range": $('#date_range').val(),
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'season', name: 'season'},//1
            {data: 'style', name: 'style'},//2
            {data: 'panel', name: 'panel'},//3
            {data: 'remark', name: 'remark'},//4
            {data: 'qty', name: 'qty'},//5
            {data: 'allowance', name: 'allowance'},//6
            {data: 'qty_plus_allowance', name: 'qty_plus_allowance'},//7
            {data: 'price_estimated', name: 'price_estimated'},//8
            {data: 'total_amount', name: 'total_amount'},//9
            {data: 'kk_no_type', name: 'kk_no_type'},//10
            {data: 'factory', name: 'factory'},//11
            {data: 'user_name', name: 'user_name'},//12
            {data: 'document_no', name: 'document_no'},//13
        ]
    });
    tableL.ajax.reload();

    var tableL2 = $('#table-list2').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        pageLength:50,
        scrollX:true,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('DocKK.editDataDockk') }}",
            data: function (d) {
                return $.extend({},d,{
                    "kk_no"     : $('#kk_no').val()
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL2.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},//0
            {data: 'season', name: 'season'},//1
            {data: 'style', name: 'style'},//2
            {data: 'remark', name: 'remark'},//4
            {data: 'total_qty', name: 'total_qty'},//5
            {data: 'kk_no_type', name: 'kk_no_type'},//10
            {data: 'factory', name: 'factory'},//11
            {data: 'user_name', name: 'user_name'},//12
            {data: 'document_no', name: 'document_no'},//13
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    tableL2.ajax.reload();

    tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    tableL2.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').on('click',function(){
        if ($('#date_range').val()=='') {
            $("#alert_warning").trigger("click",'Please select date !!!');
            return false;
        }
        tableL.draw();
    });

    $('#btn-filter2').on('click',function(){
        if ($('#kk_no').val()=='') {
            $("#alert_warning").trigger("click",'Please select KK Number !!!');
            return false;
        }
        tableL2.draw();
    });


    $('#btn-upload').on('click',function(){
        $('#modalUpload').modal('show');
    });

    $('#form-upload').submit(function(event){
        event.preventDefault();

        var formData = new FormData($(this)[0]);
        var parameter = ['file_upload'];
        var val = [$('#file_upload').val()];

        for(var i = 0; i < parameter.length; i++) {
            formData.append(parameter[i], val[i]);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url : $('#form-upload').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    message: '<center><i class="icon-spinner4 spinner"></i> Please Wait</center>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait',
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent',
                    },
                    theme: true,
                    baseZ: 2000
                });
            },
            success: function(response) {
                $('#modalUpload').modal('hide');
                myalert('success','Data Berhasil Diupload');
                $('#file_upload').val('');
                location.reload();
                $.unblockUI();
            },
            error: function(response) {
                $('#table-list').unblock();
                var responses = response['statusText'] != 'Forbidden' ? response['responseJSON'] : 'Terjadi kesalahan atau cek hak akses anda..!';
                myalert('info', responses);
                console.log(response);
                $('#modalUpload').modal('hide');
                $('#file_upload').val('');
                $.unblockUI();
            }
        });
    });

    $('#form-update').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var parameter = ['qty','header_id'];
        var val = [
            $('#qty').val(),
			$('#header_id').val(), 
        ]
        for(var i = 0; i < parameter.length; i++ ) {
            formData.append(parameter[i], val[i]);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#form-update').attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
            },
            complete: function () {
            $.unblockUI();
            },
            success: function(response) {
                myalert('success','Update Sukses');
				$('#modal_edit').modal('toggle');
				tableL2.draw();
            },
            error: function(response) {
                console.log(response);
                myalert('error',response['responseJSON']);
            }
        })
    });
})
function edit(url){
	$.ajax({
		type: "get",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();
		}
		})
		.done(function (response) {
            console.log(response);
			$('#header_id').val(response.id);
            $('#qty').val(response.total_qty);
			$('#modal_edit').modal();
		});
	}
</script>
@endsection