<?php

namespace App\Http\Controllers\MasterParameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use DataTables;
use Carbon\Carbon;
use App\Models\MasterMachineType;

class MachineTypeController extends Controller
{
    public function index(){
        return view('master_parameter.machine.index');
    }

    public function getDataMachinetype(){
        $data = DB::table('master_machine_type')
                        ->whereNull('deleted_at')
                        ->orderBy('id','asc');
        return DataTables::of($data)

        ->addColumn('action',function($data){
            return '<button class="btn btn-danger btn-delete"  id="btn-delete" data-id="'.$data->id.'" onclick="delType(this);"><span class="icon-trash"></span></button>';
        })
        ->editColumn('machine_type',function($data){
            return strtoupper($data->machine_type);
    })
        ->rawColumns(['action','machine_type'])
        ->make(true);
    }

    public function add(Request $request){
        $machine_type = $request->machine_type;
        $id_max = DB::table('master_machine_type')->select(DB::raw('max(id) as id'))->first();
        if($id_max  == null ){
            $id_max = 1;
        }else{
            $id_max = $id_max->id + 1;
        }
        try {
        db::begintransaction();
            $insert =  MasterMachineType::FirstOrCreate([
                'id' =>$id_max,
                'machine_type' => trim($machine_type),
                'created_by' => Auth::user()->id,
                'created_at' => Carbon::now(),
            ]);
        
        $cek_double = MasterMachineType::where('machine_type', $request->machine_type)->wherenull('deleted_at')->count();
        if($cek_double > 1){
            return response()->json('double insert', 422);
        }
        DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function delete(Request $request){
        $id = $request->id;

        DB::table('master_machine_type')
                ->where('id', $id)
                ->update([
                    'deleted_at' => Carbon::now(),
                    'deleted_by' => \Auth::user()->id
                ]);

        return view('master_parameter.machine.index');
        
    }
}