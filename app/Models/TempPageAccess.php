<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class TempPageAccess extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_page_access';
    protected $fillable = ['page','cutting_date','queu','planning_date','part_no','cut','user_id','deleted_at'];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
