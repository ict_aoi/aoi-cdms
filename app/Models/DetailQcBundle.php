<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DetailQcBundle extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ['qc_bundle_id', 'qty', 'defect_id', 'defect_name', 'deleted_at', 'confrimed_by', 'confirmed_at',
                            'repaired_by', 'repaired_at','created_at', 'updated_at','is_repair','repair_or_reject','measurement_marker','position'];
}
