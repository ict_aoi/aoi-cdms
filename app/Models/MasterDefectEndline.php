<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterDefectEndline extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];
	// protected $table = 'cutting_marker';
    protected $fillable = ['nama_defect','nama_defect_eng','is_repair','is_reject','created_at','updated_at','deleted_at'];
}
