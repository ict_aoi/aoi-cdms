<?php

namespace App\Http\Controllers\Preparation;

use DB;
use Auth;
use File;
use Excel;
use StdClass;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\InfoSync;
use App\Models\DetailPlan;
use App\Models\CuttingPlan;
use App\Models\RatioMarker;
use App\Models\CombinePart;
use App\Models\CuttingMarker;
use App\Models\RatioDetailTemp;
use App\Models\CuttingMovement;
use App\Models\Data\DataCutting;



class PreparationController extends Controller
{
   public function index(){
         return view('preparation/preparation_plan');
   }

   public function ajaxGetPlaning(Request $request){
          $date_cutting = $request->date_cutting;

         if (!empty($date_cutting)) {
            $data = DB::table('jaz_cutting_plan_new_update_2')
                     ->where('cutting_date',$date_cutting)
                     ->where('factory_id',Auth::user()->factory_id)
                     ->orderBy('queu')
                     ->orderBy('style');



         }else{
            $data = array();
         }


         return DataTables::of($data)
                    ->editColumn('style',function($data){
                        return $data->style.' '.$data->top_bot;
                    })
                     ->addColumn('po_buyer',function($data){
                        $po = DB::table('ns_trig_po_update')
                                    ->where('cutting_date',$data->cutting_date)
                                    ->where('style',$data->style)
                                    ->where('articleno',$data->articleno)
                                    ->where('size_category',$data->size_category)
                                    ->where('queu',$data->queu)
                                    ->first();
                         return $pob =  isset($po->po_buyer) ? trim($po->po_buyer) : '';
                     })
                     ->editColumn('size_category',function($data){
                            if ($data->size_category=='A') {
                                return 'Asian';
                            }else if ($data->size_category=='J') {
                                return 'Japan';
                            }else{
                                return 'Intl';
                            }
                     })
                     ->addColumn('queu', function($data){

                        $cek = DB::table('ns_trig_po_update')->where('cutting_date',$data->cutting_date)
                                            ->where('style',$data->style)
                                            ->where('articleno',$data->articleno)
                                            ->where('size_category',$data->size_category)
                                            ->where('queu',$data->queu)
                                            ->first();
                                if (isset($cek->queu)!=null) {
                                  return '<div id="edqueu_'.$cek->id.'">
                                     <input type="text"
                                         data-date="'.$data->cutting_date.'"
                                         data-style="'.$data->style.'"
                                         data-article="'.$data->articleno.'"
                                         data-color="'.$data->color_name.'"
                                         data-size="'.$data->size_category.'"
                                         data-mo_created="'.$data->mo_updated_at.'"
                                         data-material="'.$data->material.'"
                                         data-color_code="'.$data->color_code.'"
                                         data-gtb="'.$data->get_top_bot.'"
                                         data-tb="'.$data->top_bot.'"
                                         data-season="'.$data->season.'"
                                         data-id="'.$cek->id.'"
                                         class="form-control edqueu_unfilled" value="'.$cek->queu.'" >
                                     </input>
                                 </div>';
                                }else{
                                  return '<div id="queu_'.$data->style.'">
                                     <input type="text"
                                         data-date="'.$data->cutting_date.'"
                                         data-style="'.$data->style.'"
                                         data-article="'.$data->articleno.'"
                                         data-color="'.$data->color_name.'"
                                         data-size="'.$data->size_category.'"
                                         data-mo_created="'.$data->mo_updated_at.'"
                                         data-material="'.$data->material.'"
                                         data-color_code="'.$data->color_code.'"
                                         data-gtb = "'.$data->get_top_bot.'"
                                         data-tb="'.$data->top_bot.'"
                                         data-season="'.$data->season.'"
                                         class="form-control queu_unfilled" >
                                     </input>
                                 </div>';
                                }

                     })
                     ->addColumn('action',function($data){
                            $color = str_replace(' ', '-', $data->color_name);
                            $color_code = str_replace(' ', '-', $data->color_code);
                          // $cek = DB::table('ns_trig_po_update')->where('cutting_date',$data->cutting_date)
                          //                   ->where('style',$data->style)
                          //                   ->where('articleno',$data->articleno)
                          //                   ->where('size_category',$data->size_category)
                          //                   ->first();
                          if (isset($data->queu)!=null) {
                            return '<a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$data->color_name.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-color_code='.$color_code.' data-queu='.$data->queu.' data-tb="'.$data->top_bot.'" data-season="'.$data->season.'" class="btn btn-primary btn-detail" id="btn-detail"><span class="icon-list"></span></a>

                              <a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$data->color_name.' data-mo='.$data->mo_updated_at.' data-queu='.$data->queu.' data-tb="'.$data->top_bot.'" class="btn btn-danger btn-delqueu" id="btn-delqueu"><span class="icon-trash"></span></a>  ';
                          }else{
                                if ($data->get_top_bot==2 && $data->top_bot==null) {
                                  return '<a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$color.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-gtb="'.$data->get_top_bot.'" data-color_code='.$color_code.' data-queu="null" data-season="'.$data->season.'" class="btn btn-primary btn-detail" id="btn-detail"><span class="icon-list"></span></a>

                                    <a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$color.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-color_code='.$color_code.'  data-queu="null" class="btn btn-success btn-tb" id="btn-tb"><span class="icon-sort"></span></a>
                                  ';
                                }else if($data->get_top_bot==2 && $data->top_bot!=null){
                                    return '<a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$color.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-color_code='.$color_code.' data-tb="'.$data->top_bot.'" data-gtb="'.$data->get_top_bot.'" data-season="'.$data->season.'" data-queu="null" class="btn btn-primary btn-detail" id="btn-detail"><span class="icon-list"></span></a>

                                      <a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$color.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-color_code='.$color_code.'  data-queu="null" class="btn btn-warning btn-marge" id="btn-marge"><span class="icon-shrink7"></span></a>
                                    ';
                                }else{
                                    return '<a href="#" data-pcd='.$data->cutting_date.' data-style='.$data->style.' data-article='.$data->articleno.' data-size='.$data->size_category.' data-color='.$color.' data-mo='.$data->mo_updated_at.' data-material='.$data->material.' data-color_code='.$color_code.' data-tb="'.$data->top_bot.'" data-gtb="'.$data->get_top_bot.'" data-season="'.$data->season.'" data-queu="null" class="btn btn-primary btn-detail" id="btn-detail"><span class="icon-list"></span></a> ';
                                }
                          }

                     })
                     ->rawcolumns(['po_buyer','queu','action'])
                     ->make(true);
   }#ajaxGetPlaning


   public function sync(){
         $getInfos = DB::table('infos')
                     ->where('is_integrated',false)
                     ->whereNull('deleted_at')
                     ->get();
         try {
               foreach ($getInfos as $gi) {
                  $cutting_date = $gi->cutting_date;
                  $style = $gi->style;
                  $articleno = $gi->articleno;
                  $documentno = $gi->documentno;
                  $id = $gi->id;

                  if ($gi->type) {
                     $getdetail = DetailPlan::where('documentno',$documentno)
                                 ->whereNull('deleted_at')->first();

                           if ($getdetail!=null) {
                              $cekdel = DetailPlan::where('id',$getdetail->id)->update(['deleted_at'=>null]);

                              if ($cekdel) {
                                InfoSync::where('id',$id)->update(['updated_at'=>Carbon::now(),'is_integrated'=>true]);
                              }
                           }
                  }#gi

                  $cekdcplan = DB::table('detail_cutting_plan')
                                 ->join('cutting_plans2','cutting_plans2.id','=','detail_cutting_plan.id_plan')
                                 ->where('cutting_date',$cutting_date)
                                 ->where('style',$style)
                                 ->where('articleno',$articleno)
                                 ->whereNull('detail_cutting_plan.deleted_at')
                                 ->count();
                  $cekplan = CuttingPlan::where('cutting_date',$cutting_date)->where('style',$style)->where('articleno',$articleno)->where('factory_id',Auth::user()->factory_id)->whereNull('deleted_at')->count();
                        if ($cekdcplan==0&&$cekplan>0) {
                              CuttingPlan::where('cutting_date',$cutting_date)->where('style',$style)->where('articleno',$articleno)->where('factory_id',Auth::user()->factory_id)->update(['deleted_at'=>Carbon::now()]);

                        }

            }#gi

            return response()->json('Sync Success . . . ');
         } catch (Exception $ex) {
            $message = $ex->getMessage();
            \ErrorHandler($message);
            return response()->json('Sync field . . . ');
         }

   }#sync


   public function upload_permintaan(){
      return view('preparation/upload_permintaan');
   }#upload_permintaan

    public function ajaxGetPlan(Request $request){
        $date_cutting = $request->date_cutting;

        $data = CuttingPlan::where('cutting_date',$date_cutting)
                ->where('factory_id',Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->orderBy('queu','ASC');

        return DataTables::of($data)
        ->editColumn('size_category', function($data) {
            if ($data->size_category == 'I') {
                return 'Inter';
            } elseif ($data->size_category == 'A') {
                return 'Asian';
            } elseif ($data->size_category == 'J') {
                return 'Japan';
            } else {
                return 'None';
            }
        })
        ->addColumn('poreference', function($data) {
            $po_buyer = implode(', ', $data->po_details->pluck('po_buyer')->toArray());
            return $po_buyer;
        })
        ->addColumn('status',function($data){
            $dt = DB::table('cutting_marker')->join('ratio','ratio.id_marker','=','cutting_marker.barcode_id')->select('ratio')->where('id_plan',$data->id)->whereNull('ratio.deleted_at')->whereNull('cutting_marker.deleted_at')->count();
            $po_array = $data->po_details->pluck('po_buyer')->toArray();
            $check_plan = DB::table('data_cuttings')->where('plan_id', $data->id)->whereIn('po_buyer', $po_array)->get();
            if (count($check_plan) < 1) {
                return '<span class="badge bg-danger">Geser Plan</span>';
            } elseif ($dt == 0) {
                $badge = '<span class="badge bg-warning">Belum Diupload</span>';
            }else{
                $badge = '<span class="badge bg-success">Sudah Diupload</span>';
            }
            return $badge;
        })
        ->addColumn('check_fb', function($data) {
            $dt = DB::table('cutting_marker')->join('ratio','ratio.id_marker','=','cutting_marker.barcode_id')->select('ratio')->where('id_plan',$data->id)->whereNull('ratio.deleted_at')->whereNull('cutting_marker.deleted_at')->count();

            if($dt > 0) {
                return view('request_marker.upload_req_marker._action', [
                    'data'      => $data,
                    'detail'  => '#',
                    'check_fb'  => route('preparation.getCheckFB',$data->id),
                    'check_alokasi'  => route('permintaanMarker.getCheckAlokasi',$data->id),
                    'delete'  => '#',
                ]);
            } else {
                return view('request_marker.upload_req_marker._action', [
                    'data'      => $data,
                    'check_fb'  => route('preparation.getCheckFB',$data->id)
                ]);
            }
        })
        ->rawColumns(['status','check_fb'])
        ->make(true);
   }#ajaxGetPlan

    // datatable modal detail
    public function ajaxGetDetRat(Request $request){
        $id_plan = $request->id_plan;

        $data = DB::table('cutting_marker')
            ->where('id_plan',$id_plan)
            ->whereNull('deleted_at')
            ->orderBy('part_no')
            ->orderBy('cut');

        return DataTables::of($data)
        ->editColumn('ratio',function($data){
            $grt = DB::table('ns_trig_ratio')
                ->select('ratio')
                ->where('id_marker',$data->barcode_id)
                ->first();
            return $grt->ratio;
        })
        ->editColumn('fabric_width',function($data){
            if (is_null($data->fabric_width)) {
                return '<button class="btn bg-slate btn-shoot"  id="btn-shoot" data-barcode="'.$data->barcode_id.'" data-plan="'.$data->id_plan.'" data-part="'.$data->part_no.'" onclick="shoot(this);"><span class="icon-hammer2"></span></button>';
            } else {
                if ($data->bay_pass == true) {
                    return '<span class="label label-warning label-rounded"> '.$data->fabric_width.' </span>';
                } else {
                    return $data->fabric_width;
                }
            }
        })
        ->editColumn('cut',function($data){
            if($data->suggest_cut == null) {
                return $data->cut.' (-)';
            } else {
                return $data->cut.' ('.$data->suggest_cut.')';
            }
        })
        ->addColumn('remark',function($data){
            if($data->is_new == null) {
                return null;
            } else {
                return 'LAPORAN BARU';
            }
        })
        ->addColumn('action',function($data){
            return '<button class="btn btn-danger btn-delpartno" id="btn-delpartno"  data-barcode="'.$data->barcode_id.'"  data-plan="'.$data->id_plan.'" data-part="'.$data->part_no.'" onclick="delpartno(this);"><span class="icon-trash"></span></button>';
        })
        ->setRowAttr([
            'style'=>function($data){
                if ($data->remark==true) {
                    return  'background-color: #00e6ac;';
                }
            }
        ])
        ->rawColumns(['fabric_width','ratio','action'])
        ->make(true);
    }

   public function delrat(Request $request){
      $id_plan=$request->id_plan;
      try {
        db::begintransaction();
            $gcutmark = DB::table('cutting_marker')
                           ->where('id_plan',$id_plan)
                           ->whereNull('deleted_at')
                           ->whereNull('perimeter')
                           ->get();
            foreach ($gcutmark as $gcm) {
                  DB::table('cutting_marker')->where('barcode_id',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                  DB::table('ratio')->where('id_marker',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
                  DB::table('ratio_detail')->where('barcode_id',$gcm->barcode_id)->update(['deleted_at'=>Carbon::now()]);
            }
            $data_response = [
                            'status' => 200,
                            'output' => 'Deleted Success'
                          ];
        db::commit();
      } catch (Exception $ex) {
        db::rollback();
                  $message = $ex->getMessage();
                  \ErrorHandler($message);
          $data_response = [
                            'status' => 422,
                            'output' => 'Deleted Field '.$message
                          ];
      }

      return response()->json(['data' => $data_response]);

   }

   public function delpartno(Request $request){
        $id_plan = $request->id_plan;
        $part_no = $request->part_no;
        $barcode = $request->barcode;

        try {
            DB::beginTransaction();
            $cek = DB::table('cutting_marker')
                    ->where('barcode_id',$barcode)
                    ->whereNull('perimeter')
                    ->whereNull('marker_length')
                    ->count();
            if ($cek > 0) {
                DB::table('cutting_marker')->where('barcode_id',$barcode)->whereNull('deleted_at')->update(['deleted_at'=>Carbon::now()]);
                DB::table('ratio')->where('id_marker',$barcode)->update(['deleted_at'=>Carbon::now()]);
                DB::table('ratio_detail')->where('barcode_id',$barcode)->update(['deleted_at'=>Carbon::now()]);
            }else{
                return response()->json('Deleted Field, marker have perimeter and marker length, please contact pola marker team!',422);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
        }

        return response()->json($data_response,200);
   }

   // public function uploadExcel(Request $request){
   //    $id = $request->idcp;

   //    $results = array();
   //    if($request->hasFile('file_upload')){
   //        $extension = \File::extension($request->file_upload->getClientOriginalName());
   //        if ($extension == "xlsx" || $extension == "xls") {
   //            $path = $request->file_upload->getRealPath();
   //            $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
   //        }
   //    }

   //    foreach ($datax as $key => $value) {

   //       if (!isset($value->color) || !isset($value->part) || !isset($value->cut) || !isset($value->ratio) || !isset($value->layer) || !isset($value->marker)) {

   //          return response()->json('Header tidak lengkap / tidak sesuai');
   //       }

   //       $exratio = explode(", ", $value->ratio);

   //          foreach ($exratio as $xr) {
   //             $data_detail = array(
   //                'id_plan'=>$id,
   //                'color'=>$value->color,
   //                'part_no'=>$value->part,
   //                'cut'=>$value->cut,
   //                'ratio'=>$xr,
   //                'layer'=>$value->layer,
   //                'marker'=>$value->marker,
   //                'created_at'=>Carbon::now(),
   //                'updated_at'=>null
   //             );
   //             PreparationRequest::FirstOrCreate($data_detail);
   //          }

   //       // RequestMarker::FirstOrCreate($data_detail);

   //    }

   //    return redirect()->route('preparation.upload');


   // }#uploadExcel

   public function uploadxls(Request $request){
      $tqty = 0;

      // $pcd = Carbon::parse($request->date_cut,)->format('Y-m-d');
       $pcd = Carbon::createFromFormat('d F, Y', $request->date_cut)->format('Y-m-d');


      $results = array();
      if($request->hasFile('file_upload')){
          $extension = \File::extension($request->file_upload->getClientOriginalName());
          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file_upload->getRealPath();
              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
              // $datas = \Excel::selectSheetsByIndex(1)->load($path)->get();
          }
      }




      try {

          foreach ($datax as $dx) {
              $queu = trim($dx->queu);
              $style  = trim($dx->style);
              $article = trim($dx->article);
              $part = trim($dx->part);
              $cut = trim($dx->cut);
              $layer = trim($dx->layer);
              $ratio = str_replace(" ","", $dx->ratio);


            if ($queu!='') {

                // $check_plan = $this->check_plan($queue,$style,$pcd,$article);

                $check_plan =  DB::table('ns_detail_cutplan')
                                ->where('cutting_date',$pcd)
                                ->where('style',$style)
                                ->where('articleno',$article)
                                ->where('queu',$queu)
                                ->get();

                if(count($check_plan) > 0) {
                    $id_plan = $check_plan->first()->id_plan;
                } else {
                    $id_plan = null;
                    $check_plan = null;
                }


              $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

              if ($check_plan!=null && $check_CutMark==null) {
                // $yy = $this->yy($pcd,$dx->style,$dx->article,$dx->queu,$dx->part,$check_plan->po_buyer);
                $qtyratio = $this->qty_ratio($ratio);

                // $fab_cons = $yy * $qtycut;

                  $newid = $this->id_marker($pcd);
                  $dt_cutmark = array(
                      'barcode_id'=>$newid,
                      'id_plan'=>$id_plan,
                      'part_no'=>$part,
                      'cut'=>$cut,
                      'layer'=>$layer,
                      'layer_plan'=>$layer,
                      'qty_ratio'=>$qtyratio,
                      'created_at'=>carbon::now(),
                      'created_by'=>\Auth::user()->id
                  );

                  DB::table('cutting_marker')->insert($dt_cutmark);

                  $opRatio = explode(",",$ratio);

                  foreach ($opRatio as $or) {
                      $qr = explode("|",$or);

                      $dt_ratio = array(
                            'id_marker'=>$newid,
                            'size'=>$qr[0],
                            'ratio'=>$qr[1],
                            'created_at'=>carbon::now()
                      );

                     RatioMarker::FirstOrCreate($dt_ratio);
                  }


             }

           }
        }
        // $this->set_width_fabric($pcd);

        $getDT = DB::table('ns_set_width')->where('cutting_date',$pcd)->get();

        foreach ($getDT as $gd) {
          $str_part = explode("+", $gd->part_no);

          $gcons = DB::table('jaz_cons')->where('cutting_date',$pcd)->where('style',$gd->style)->where('articleno',$gd->articleno)->where('queu',$gd->queu)->whereIn('part_no',$str_part)->sum('cons');

          $qty_width = $this->qty_width($pcd,$gd->style,$gd->articleno,$gd->part_no,$gd->po_buyer);

            if ($qty_width!=null) {
               $this->set_width_fabric($pcd,$gd->style,$gd->articleno,$gd->part_no,$gd->po_buyer,$gd->id_plan,$gcons,$qty_width);
            }

        }

        $data_response = [
                            'status' => 200,
                            'output' => 'Upload Success'
                          ];
      } catch (Exception $ex) {
          db::rollback();
          $message = $ex->getMessage();
                  \ErrorHandler($message);

        $data_response = [
                            'status' => 422,
                            'output' => 'upload failed !!!'
                          ];
      }

      return response()->json(['data' => $data_response]);

   }

   private function set_width_fabric($pcd,$style,$articleno,$part,$pobuyer,$id_plan,$yy,$qty_width){

      try {

        db::begintransaction();


            foreach ($qty_width as $qw) {
                $getcm = DB::table('cutting_marker')->where('id_plan',$id_plan)
                            ->where('part_no',$part)
                            ->whereNull('fabric_width')
                            ->whereNull('deleted_at')
                            ->orderBy('cut','ASC')
                            ->get();
                  foreach ($getcm as $gcm) {
                        $fab_cons = $yy * ($gcm->layer * $gcm->qty_ratio);
                          if ($fab_cons<=$qw['qty']) {
                              $dtupdate = array(
                                'fabric_width'=>$qw['width'],
                                'fab_cons'=>$fab_cons,
                                'qty_fabric'=>$qw['available'],
                                'item_id'=>$qw['item_id'],
                                'color'=>$qw['color']
                              );
                              DB::table('cutting_marker')->where('barcode_id',$gcm->barcode_id)->update($dtupdate);
                                $qw['qty']=$qw['qty']-$fab_cons;
                          }else if(($fab_cons>$qw['qty'])&&($qw['qty']>($gcm->qty_ratio*$yy))){
                              $nlayer = round(($qw['qty']/($gcm->qty_ratio*$yy)),0);
                              $slayer = round(($gcm->layer-$nlayer),0);
                              $nfab_cons = $yy * ($gcm->qty_ratio * $nlayer);

                              DB::table('cutting_marker')->where('barcode_id',$gcm->barcode_id)->update(['layer'=>$nlayer,'fab_cons'=>$nfab_cons,'fabric_width'=>$qw['width'],'qty_fabric'=>$qw['available'],'color'=>$qw['color'],'item_id'=>$qw['item_id']]);
                              $qw['qty']=$qw['qty']-$nfab_cons;
                                if ($slayer>0) {
                                    $newid = $this->id_marker($pcd);
                                    $maxcut = DB::table('cutting_marker')->where('id_plan',$id_plan)->where('part_no',$part)->max('cut');
                                    $ncut = $gcm->cut+1;

                                    $dt_nmarker = array(
                                          'barcode_id'=>$newid,
                                          'id_plan'=>$id_plan,
                                          'part_no'=>$part,
                                          'cut'=>$ncut,
                                          'color'=>$gcm->color,
                                          'layer'=>$slayer,
                                          'layer_plan'=>$slayer,
                                          'qty_ratio'=>$gcm->qty_ratio,
                                          'created_at'=>Carbon::now(),
                                          'created_by'=>\Auth::user()->id,
                                          'remark'=>true
                                      );
                                    $getbarcode = DB::table('cutting_marker')
                                                      ->where('id_plan',$id_plan)
                                                      ->where('part_no',$part)
                                                      ->whereBetween('cut',[$ncut,$maxcut])
                                                      ->whereNull('deleted_at')
                                                      ->get();
                                    foreach ($getbarcode as $gbar) {
                                        DB::table('cutting_marker')
                                              ->where('barcode_id',$gbar->barcode_id)
                                              ->update(['cut'=>$gbar->cut+1]);
                                    }
                                    // CuttingMarker::FirstOrCreate($dt_nmarker);
                                    DB::table('cutting_marker')->insert($dt_nmarker);

                                    $dt_ratio = RatioMarker::where('id_marker',$gcm->barcode_id)->whereNull('deleted_at')->get();
                                    foreach ($dt_ratio as $drat) {
                                        $ndt_rat = array(
                                            'id_marker'=>$newid,
                                            'ratio'=>$drat->ratio,
                                            'size'=>$drat->size,
                                            'created_at'=>Carbon::now()
                                        );

                                        RatioMarker::FirstOrCreate($ndt_rat);
                                    }
                                }


                          }
                  }

            }

          db::commit();
      } catch (Exception $ex) {
            db::rollback();
          $message = $ex->getMessage();
                  \ErrorHandler($message);
      }

   }



   private function id_marker($pcd){

        $id_row_new = '';

        $data_cutting_all = DB::table('cutting_marker')->where('barcode_id', 'like', 'MK-'.Carbon::now()->format('ymd').'%')->orderBy('barcode_id', 'desc')->get();
        if(count($data_cutting_all) > 0) {
            $id_row_new = substr($data_cutting_all->first()->barcode_id, 3, 12) + 1;
            $id_row_new = 'MK-'.$id_row_new;
        } else {
            $id_row_new = 'MK-'.Carbon::now()->format('ymd').'000001';
        }

        return $id_row_new;


   }

   private function sum_ratio($id_marker){
      $getrat =DB::table('ratio')
            ->where('id_marker',$id_marker)
            ->whereNull('deleted_at')
            ->get();
            $sratio = 0;
         foreach ($getrat as $gr) {
            $qrat = explode("|", $gr->ratio);

             $sratio = $sratio + $qrat[1];

         }

      return $sratio;

   }

   private function check_plan($queu,$style,$pcd,$article){

       $chek = DB::table('ns_detail_cutplan')
                    ->where('cutting_date',$pcd)
                    ->where('style',$style)
                    ->where('articleno',$article)
                    ->where('queu',$queu)
                    ->get()->first()->id_plan;

        if($chek == null) {
            $chek = null;
        }

         return $chek;
   }

   private function check_CutMark($id_plan,$part,$cut){

         $chek = db::table('cutting_marker')->where('id_plan',$id_plan)
                                    ->where('part_no',$part)
                                    ->where('cut',$cut)
                                    ->whereNull('deleted_at')
                                    ->first();

         return $chek;
   }



   private function qty_ratio($ratio){

        $erat = explode(",", $ratio);
        $qty = 0;

        if(count($erat) > 0) {
            foreach ($erat as $e) {
                if($e != null && $e != '') {
                    $qtyrat = explode("|", $e);
                    $qty = $qty+(int)$qtyrat[1];
                }
            }
        }

        return $qty;
   }

   private function qty_width($pcd,$style,$articleno,$part,$pobuyer){
        $qwf=null;
        $prepl = str_replace("+", "','", $part);
        $reppobuy= str_replace(",","','", $pobuyer);
          $get = DB::select("SELECT sum(qty_prepared) as qty ,actual_width,item_id,color  FROM ns_wms_cuting_integration WHERE planning_date='".$pcd."' and style='".$style."' AND article_no='".$articleno."' and po_buyer in ('".$reppobuy."') and part_no in('".$prepl."') group by actual_width,item_id,color order by actual_width desc");

            if (count($get)>0) {
              foreach ($get as $gt) {

                   $dt['width']=$gt->actual_width;
                   $dt['qty']=$gt->qty;
                   $dt['available']=$gt->qty;
                   $dt['item_id']=$gt->item_id;
                   $dt['color']=$gt->color;

                    $qwf[]=$dt;
               }
            }
        return $qwf;
    }

    // upload request marker manual

    public function uploadManual(){
        RatioDetailTemp::where('user_id', \Auth::user()->id)->where('deleted_at', null)->delete();
        return view('preparation/upload_permintaan_manual');
    }

   public function uploadxlsmanual(Request $request){
      $pcd = Carbon::createFromFormat('d F, Y', $request->date_cut)->format('Y-m-d');

      $results = array();
      if($request->hasFile('file_upload')){
          $extension = \File::extension($request->file_upload->getClientOriginalName());
          if ($extension == "xlsx" || $extension == "xls") {
              $path = $request->file_upload->getRealPath();
              $datax = \Excel::selectSheetsByIndex(0)->load($path)->get();
              // $datas = \Excel::selectSheetsByIndex(1)->load($path)->get();
          }
      }


        try {

            db::begintransaction();
              foreach ($datax as $dx) {
                  $queu = trim($dx->queu);
                  $style  = trim($dx->style);
                  $article = trim($dx->article);
                  $part = trim($dx->part);
                  $cut = trim($dx->cut);
                  $layer = trim($dx->layer);
                  $fabric_width = trim($dx->fabric_width);
                  $item_id = trim($dx->item_id);
                  $is_body = trim($dx->body);
                  $set_type = trim($dx->set);
                  $is_laporan_baru = trim($dx->laporan_baru);
                  $ratio = str_replace(" ","", $dx->ratio);

                    if($is_body == 1) {
                        $is_body = true;
                    } else {
                        $is_body = false;
                    }

                    if($is_laporan_baru == 1) {
                        $is_laporan_baru = 'editreport';
                    } else {
                        $is_laporan_baru = null;
                    }

                    if($set_type == '') {
                        $set_type = 1;
                    } else {
                        $get_type = DB::table('types')->where('type_name', $set_type)->first();
                        if($get_type == null) {
                            $set_type = 1;
                        } else {
                            $set_type = $get_type->id;
                        }
                    }

                  if ($queu!=''&&$style!=''&&$article!=''&&$part!=''&&$cut!=''&&$layer!=''&&$ratio!=''&&$fabric_width!=''&&$item_id!='') {
                        $qtyratio = $this->qty_ratio($ratio);

                        $queue_check = explode('+', $queu);

                        // jika queue lebih dari 1
                        if(count($queue_check) > 1) {
                            $id_plan_array = array();
                            $validate_check = 0;
                            foreach($queue_check as $queue) {
                                // $check_plan = $this->check_plan($queue,$style,$pcd,$article);

                                $check_plan =  DB::table('ns_detail_cutplan')
                                                    ->where('cutting_date',$pcd)
                                                    ->where('style',$style)
                                                    ->where('articleno',$article)
                                                    ->where('queu',$queue)
                                                    ->get();

                                if(count($check_plan) > 0) {
                                    $id_plan = $check_plan->first()->id_plan;
                                } else {
                                    $id_plan = null;
                                    $check_plan = null;
                                }

                                $id_plan_array[] = $id_plan;

                                $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                                if ($check_plan == null || $check_CutMark != null) {
                                    $validate_check++;
                                }
                            }

                            if($validate_check == 0) {

                                $newid = $this->id_marker($pcd);
                                $next_val = DB::select("select nextval('combine_part_id') as nxt");

                                $ratio_temp = explode(",",$ratio);
                                $ratio_temp = implode(', ', $ratio_temp);

                                $dt_cutmark = array(
                                    'barcode_id'=>$newid,
                                    'id_plan'=>$id_plan,
                                    'part_no'=>$part,
                                    'cut'=>$cut,
                                    'layer'=>$layer,
                                    'qty_ratio'=>$qtyratio,
                                    'fabric_width'=>$fabric_width,
                                    'item_id'=>$item_id,
                                    'created_at'=>carbon::now(),
                                    'updated_at'=>carbon::now(),
                                    'combine_id'=>$next_val[0]->nxt,
                                    'is_body'=>$is_body,
                                    'is_new'=>$is_laporan_baru,
                                    'layer_plan'=>$layer,
                                    'set_type'=>$set_type,
                                    'ratio_print'=>$ratio_temp,
                                    'created_by'=>\Auth::user()->id
                                );

                                DB::table('cutting_marker')->insert($dt_cutmark);

                                $opRatio = explode(",",$ratio);

                                foreach ($opRatio as $or) {
                                    $qr = explode("|",$or);

                                    $dt_ratio = array(
                                            'id_marker'=>$newid,
                                            'size'=>$qr[0],
                                            'ratio'=>(int)$qr[1],
                                            'created_at'=>carbon::now(),
                                            'updated_at'=>carbon::now()
                                    );

                                    RatioMarker::FirstOrCreate($dt_ratio);
                                }

                                foreach($id_plan_array as $id_plan) {
                                    CombinePart::FirstOrCreate([
                                        'plan_id' => $id_plan,
                                        'combine_id' => $next_val[0]->nxt,
                                        'deleted_at' => null
                                    ]);
                                }

                                CuttingMovement::firstOrCreate([
                                    'barcode_id' => $newid,
                                    'process_from' => null,
                                    'status_from' => null,
                                    'process_to' => 'preparation',
                                    'status_to' => 'onprogress',
                                    'is_canceled' => false,
                                    'user_id' => \Auth::user()->id,
                                    'description' => 'barcoding',
                                    'ip_address' => $this->getIPClient(),
                                    'barcode_type' => 'spreading',
                                    'deleted_at' => null
                                ]);
                            }

                        // jika queue cuma ada 1
                        } else {
                            // $check_plan = $this->check_plan($queue_check[0],$style,$pcd,$article);

                            $check_plan =  DB::table('ns_detail_cutplan')
                                                ->where('cutting_date',$pcd)
                                                ->where('style',$style)
                                                ->where('articleno',$article)
                                                ->where('queu',$queue_check[0])
                                                ->get();

                            if(count($check_plan) > 0) {
                                $id_plan = $check_plan->first()->id_plan;
                            } else {
                                $id_plan = null;
                                $check_plan = null;
                            }

                            $check_CutMark = $this->check_CutMark($id_plan,$part,$cut);

                            if ($check_plan!=null && $check_CutMark==null) {
                                $newid = $this->id_marker($pcd);

                                $ratio_temp = explode(",",$ratio);
                                $ratio_temp = implode(', ', $ratio_temp);

                                // $items = DB::table('ns_master_items')->where('item_id',$item_id)->first();
                                $dt_cutmark = array(
                                    'barcode_id'=>$newid,
                                    'id_plan'=>$id_plan,
                                    'part_no'=>$part,
                                    'cut'=>$cut,
                                    'layer'=>$layer,
                                    'layer_plan'=>$layer,
                                    'qty_ratio'=>$qtyratio,
                                    'fabric_width'=>$fabric_width,
                                    'item_id'=>$item_id,
                                    'created_at'=>carbon::now(),
                                    'updated_at'=>carbon::now(),
                                    'is_body'=>$is_body,
                                    'is_new'=>$is_laporan_baru,
                                    'set_type'=>$set_type,
                                    'ratio_print'=>$ratio_temp,
                                    'created_by'=>\Auth::user()->id
                                );

                                DB::table('cutting_marker')->insert($dt_cutmark);

                                $opRatio = explode(",",$ratio);

                                foreach ($opRatio as $or) {
                                    $qr = explode("|",$or);

                                    $dt_ratio = array(
                                        'id_marker'=>$newid,
                                        'size'=>$qr[0],
                                        'ratio'=>(int)$qr[1],
                                        'created_at'=>carbon::now(),
                                        'updated_at'=>carbon::now()
                                    );

                                   RatioMarker::FirstOrCreate($dt_ratio);
                                }

                                CuttingMovement::firstOrCreate([
                                    'barcode_id' => $newid,
                                    'process_from' => null,
                                    'status_from' => null,
                                    'process_to' => 'preparation',
                                    'status_to' => 'onprogress',
                                    'is_canceled' => false,
                                    'user_id' => \Auth::user()->id,
                                    'description' => 'barcoding',
                                    'ip_address' => $this->getIPClient(),
                                    'barcode_type' => 'spreading',
                                    'deleted_at' => null
                                ]);
                            }
                        }
                  }

              }



            db::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'upload request marker success'
                          ];
        } catch (Exception $er) {
              db::rollback();
                    $message = $er->getMessage();
                    ErrorHandler::db($message);

                    $data_response = [
                            'status' => 422,
                            'output' => 'upload request marker failed '.$message
                          ];
        }

        return response()->json(['data' => $data_response]);

   }


   public function getPO(Request $request){
         $data = DB::table('ns_detail_po_2')
                        ->where('cutting_date',$request->cutting_date)
                        ->where('style',$request->style)
                        ->where('articleno',$request->articleno)
                        ->where('size_category',$request->size_category)
                        ->where('queu',$request->queu)
                        ->where('top_bot',$request->top_bot);

         return DataTables::of($data)
                           ->addColumn('checkbox',function($data){
                              if ($data->queu==null) {
                                  return '<input type="checkbox" class="setpo" name="selector[]" id="Inputselector" value="'.$data->po_buyer.'" data-stat="'.$data->statistical_date.'" data-qty="'.$data->qty.'" >';
                              }else{
                                  return '';
                              }

                           })
                           ->editColumn('queu',function($data){
                                    return $data->queu;
                           })
                           ->rawColumns(['checkbox','queu'])
                           ->make(true);

   }#getPO

   public function partialIn(Request $request){

        $data = $request->data;
        $pcd = $request->date;
        $style = $request->style;
        $article = $request->article;
        $sizectg = $request->sizectg;
        $queue = $request->queue;
        $mo_created = $request->mo_created;
        $color = $request->color;
        $material = $request->material;
        $color_code = $request->color_code;
        $top_bot = $request->top_bot;
        $season = $request->season;
        $factory_id = Auth::user()->factory_id;
        $get_top_bot = $request->gtopbot;

        $cekqueu = CuttingPlan::where('cutting_date',$pcd)
                                 ->where('queu',$queue)
                                 ->where('factory_id',$factory_id)
                                 ->whereNull('deleted_at')->count();
          try {
                if ($cekqueu==0) {

                          switch ($request->gtopbot) {
                            case '2':
                              $set_tb = $request->top_bot;
                              break;

                            case '1':
                                $getTB= DB::table('data_cuttings')->select('style')
                                              ->where('cutting_date',$pcd)
                                              ->where('style','LIKE',$style.'%')
                                              ->where('articleno',$article)
                                              ->where('size_category',$sizectg)
                                              ->where('warehouse',$factory_id)
                                              ->groupBy('style')
                                              ->first();
                              $set_tb = substr($getTB->style, -3,3);
                              break;

                            default:
                              $set_tb = null;
                              break;
                          }



                          $incutplan = array(
                                'cutting_date'=>$pcd,
                                'style'=>$style,
                                'articleno'=>$article,
                                'size_category'=>$sizectg,
                                'queu'=>$queue,
                                'remark'=>'partial',
                                'mo_created_at'=>$mo_created,
                                'color'=>$color,
                                'material'=>$material,
                                'color_code'=>$color_code,
                                'user_id'=>Auth::user()->id,
                                'top_bot'=>$set_tb,
                                'season'=>$season,
                                'created_at'=>Carbon::now(),
                                'factory_id'=>$factory_id,
                                'is_combine' => false,
                                'is_header' => true
                             );
                        CuttingPlan::FirstOrCreate($incutplan);
                        $getidplan = CuttingPlan::where('cutting_date',$pcd)
                                                     ->where('style',$style)
                                                     ->where('articleno',$article)
                                                     ->where('size_category',$sizectg)
                                                     ->where('queu',$queue)
                                                     ->where('factory_id',$factory_id)
                                                     ->whereNull('deleted_at')
                                                     ->first();


                        foreach ($data as $dt) {

                            $gp = DB::table('ns_detail_po_2')
                                    ->where('cutting_date',$pcd)
                                    ->where('style',$style)
                                    ->where('articleno',$article)
                                    ->where('size_category',$sizectg)
                                    ->where('factory_id',$factory_id)
                                    ->where('po_buyer',$dt['pobuyer'])
                                    ->whereNull('queu')
                                    ->first();

                            $detplan = array(
                                  'id_plan'=>$getidplan->id,
                                  'po_buyer'=>$dt['pobuyer'],
                                  'statistical_date'=>$dt['stat_date'],
                                  'queu'=>$queue,
                                  'qty'=>$dt['qty'],
                                  'job'=>$gp->job_no,
                                  'destination'=>$gp->destination,
                                  'created_at'=>carbon::now()
                            );

                             DetailPlan::FirstOrCreate($detplan);
                             DB::table('data_cuttings')->where('cutting_date',$pcd)->where('warehouse',$factory_id)->where('top_bot',$top_bot)->where('po_buyer',$dt['pobuyer'])->update(['queu'=>$queue, 'plan_id'=>$getidplan->id]);


                        }

                        $data_response = [
                            'status' => 200,
                            'output' => 'Insert queue success !'
                          ];
                }else{
                    $data_response = [
                            'status' => 422,
                            'output' => 'Queue already axist'
                          ];
                }
          } catch (Exception $e) {
                db::rollback();
                    $message = $er->getMessage();
                    ErrorHandler::db($message);

                    $data_response = [
                            'status' => 422,
                            'output' => 'Insert queue failed'
                          ];
          }

        return response()->json(['data' => $data_response]);

   }#partialIn

   public function delpart(Request $request){
         $cekplan = CuttingPlan::where('cutting_date',$request->pcd)
                                 ->where('style',$request->style)
                                 ->where('articleno',$request->article)
                                 ->where('queu',$request->queu)
                                 ->where('factory_id',Auth::user()->factory_id)
                                 ->whereNull('deleted_at')
                                 ->first();

         if ($cekplan!=null) {
              DetailPlan::where('id_plan',$cekplan->id)->update(['deleted_at'=>Carbon::now()]);
              DB::table('data_cuttings')->where('po_buyer',$request->po_buyer)->update(['queu'=>null,'plan_id'=>null]);
              CuttingPlan::where('id',$cekplan->id)->update(['deleted_at'=>Carbon::now()]);

         }
   }#delpart

   public function allIn(Request $request){

    $factory_id = Auth::user()->factory_id;

        $cekplan = CuttingPlan::where('cutting_date',$request->cutting_date)
                                ->where('queu',$request->queu)
                                ->where('factory_id',$factory_id)
                                ->whereNull('deleted_at')
                                ->first();

          try {
                if (is_null($cekplan)) {

                  switch ($request->gtopbot) {
                    case '2':
                      $set_tb = $request->top_bot;
                      break;

                    case '1':
                        $getTB= DB::table('data_cuttings')->select('style')
                                      ->where('cutting_date',$request->cutting_date)
                                      ->where('style','LIKE',$request->style.'%')
                                      ->where('articleno',$request->articleno)
                                      ->where('size_category',$request->size_category)
                                      ->where('warehouse',$factory_id)
                                      ->groupBy('style')
                                      ->first();
                      $set_tb = substr($getTB->style, -3,3);
                      break;

                    default:
                      $set_tb = null;
                      break;
                  }


                  $inplan = array(
                      'cutting_date'=>$request->cutting_date,
                      'style'=>$request->style,
                      'articleno'=>$request->articleno,
                      'size_category'=>$request->size_category,
                      'queu'=>$request->queu,
                      'remark'=>'all',
                      'user_id'=>Auth::user()->id,
                      'mo_created_at'=>$request->mo_created,
                      'color'=>$request->color,
                      'material'=>$request->material,
                      'color_code'=>$request->color_code,
                      'top_bot'=>$set_tb,
                      'season'=>$request->season,
                      'created_at'=>Carbon::now(),
                      'factory_id'=>$factory_id,
                      'is_combine' => false,
                      'is_header' => true
                  );

                  CuttingPlan::FirstOrCreate($inplan);
                  $getplan = CuttingPlan::where('cutting_date',$request->cutting_date)->where('style',$request->style)
                                ->where('articleno',$request->articleno)->where('queu',$request->queu)->where('factory_id',$factory_id)
                                ->whereNull('deleted_at')->first();

                  $getPo = DB::table('ns_detail_po_2')
                              ->where('cutting_date',$request->cutting_date)
                              ->where('style',$request->style)
                              ->where('articleno',$request->articleno)
                              ->where('size_category',$request->size_category)
                              ->where('factory_id',$factory_id)
                              ->whereNull('queu')
                              ->get();

                  foreach ($getPo as $gp) {
                      $detplan = array(
                          "po_buyer"=>$gp->po_buyer,
                          "id_plan"=>$getplan->id,
                          "queu"=>$request->queu,
                          'statistical_date'=>$gp->statistical_date,
                          'qty'=>$gp->qty,
                          'job'=>$gp->job_no,
                          'destination'=>$gp->destination,
                          "created_at"=>carbon::now()
                      );



                      DetailPlan::FirstOrCreate($detplan);
                      DB::table('data_cuttings')
                              ->where('cutting_date',$request->cutting_date)
                              ->where('top_bot',$request->top_bot)
                              ->where('po_buyer',$gp->po_buyer)
                              ->where('warehouse',$factory_id)
                              ->update(['queu'=>$request->queu,'plan_id'=>$getplan->id]);
                  }




                  $this->set_info($request->cutting_date);

                  $data_response = [
                            'status' => 200,
                            'output' => 'Insert queue success !'
                          ];

             }else{
                $data_response = [
                            'status' => 422,
                            'output' => 'Queue already exists !!!'
                          ];
             }
          } catch (Exception $er) {
                  db::rollback();
                    $message = $er->getMessage();
                    ErrorHandler::db($message);

                    $data_response = [
                            'status' => 422,
                            'output' => 'Insert queue failed'
                          ];
          }

          return response()->json(['data' => $data_response]);

   }


   public function edQueu(Request $request){

    $factory_id = Auth::user()->factory_id;
    $id = $request->id;
    $date = $request->date;
    $queu = $request->queu;

        $cekplan = CuttingPlan::where('cutting_date',$date)
                                ->where('queu',$queu)
                                ->where('factory_id',$factory_id)
                                ->whereNull('deleted_at')
                                ->first();

          try {
                if (is_null($cekplan)) {
                  $edit = array(
                      'queu'=>$queu,
                      'plan_id'=>$id
                  );

                  CuttingPlan::where('id',$id)->whereNull('deleted_at')->update($edit);
                  DetailPlan::where('id_plan',$id)->whereNull('deleted_at')->update($edit);

                  $gpo = DetailPlan::where('id_plan',$id)->whereNull('deleted_at')->get();

                  foreach ($gpo as $gp) {
                    DB::table('data_cuttings')->where('cutting_date',$date)->where('po_buyer',$gp->po_buyer)->update($edit);
                  }

                  $data_response = [
                            'status' => 200,
                            'output' => 'Edit queue success !'
                          ];

             }else{
                $data_response = [
                            'status' => 422,
                            'output' => 'Queue already exists !!!'
                          ];
             }
          } catch (Exception $er) {
                  db::rollback();
                    $message = $er->getMessage();
                    ErrorHandler::db($message);

                    $data_response = [
                            'status' => 422,
                            'output' => 'Edit queue failed'
                          ];
          }

          return response()->json(['data' => $data_response]);

   }

   public function deletedAll(Request $request){

      $getplan = CuttingPlan::where('cutting_date',$request->cutting_date)
                              ->where('style',$request->style)
                              ->where('articleno',$request->articleno)
                              ->where('size_category',$request->size_category)
                              ->where('factory_id',Auth::user()->factory_id)
                              ->where('queu',$request->queu)
                              ->whereNull('deleted_at')->first();

            try {
              DetailPlan::where('id_plan',$getplan->id)->update(['deleted_at'=>carbon::now()]);
              DB::table('data_cuttings')->where('plan_id',$getplan->id)
                              ->update(['queu'=>null,'plan_id'=>null]);

              CuttingPlan::where('id',$getplan->id)->update(['deleted_at'=>carbon::now()]);

            } catch (Exception $ex) {
              DB::rollback();
              $message = $ex->getMessage();
                  \ErrorHandler($message);
            }
            // foreach ($getplan as $gp) {

            //     if ($gp->remark=='all') {
            //         DB::table('data_cuttings')
            //                 ->where('cutting_date',$request->cutting_date)
            //                 ->where('style',$request->style)
            //                 ->where('articleno',$request->articleno)
            //                 ->where('size_category',$request->size_category)
            //                 ->where('queu',$gp->queu)
            //                 ->update(['queu'=>null]);
            //         CuttingPlan::where('id',$gp->id)->update(['deleted_at'=>Carbon::now()]);
            //     }else{

            //         $getdetplan=DetailPlan::where('id_plan',$gp->id)->get();

            //         foreach ($getdetplan as $dp) {
            //             DB::table('data_cuttings')->where('po_buyer',$dp->po_buyer)->update(['queu'=>null]);
            //             DetailPlan::where('id',$dp->id)->update(['deleted_at'=>Carbon::now()]);
            //         }

            //          CuttingPlan::where('id',$gp->id)->update(['deleted_at'=>Carbon::now()]);
            //     }
            // }
   }

   private function set_info($pcd){
      // dd($pcd,$style,$articleno,$size_category);
        $chek = 0;
        $cekdatcut = DB::table('jaz_cutting_plan_new')
                          ->where('cutting_date',$pcd)
                          ->get();


        foreach ($cekdatcut as $dc) {
              $cekplan = CuttingPlan::where('cutting_date',$dc->cutting_date)
                                        ->where('style',$dc->style)
                                        ->where('articleno',$dc->articleno)
                                        ->where('size_category',$dc->size_category)
                                        ->where('factory_id',Auth::user()->factory_id)
                                        ->count();

              if ($cekplan==1) {
                  $chek = $chek;
              }else{
                  $chek++;
              }

        }

        if ($chek!=0) {
          InfoSync::FirstOrCreate(['type'=>"create",'cutting_date'=>$pcd,'style'=>"-",'articleno'=>"-",'documentno'=>"-",'po_buyer'=>"-",'desc'=>"Plan date ".$pcd." create",'created_at'=>Carbon::now(),'is_integrated'=>false]);
        }

   }


   public function splitTopBot(Request $request){
        $pcd = $request->pcd;
        $style = $request->style;
        $article = $request->article;
        $size_category = $request->size_category;
        $factory_id = Auth::user()->factory_id;

       try {
         DB::begintransaction();
              $getDC = DB::table('data_cuttings')->select('style')
                                      ->where('cutting_date',$pcd)
                                      ->where('style','LIKE',$style.'%')
                                      ->where('articleno',$article)
                                      ->where('size_category',$size_category)
                                      ->where('warehouse',$factory_id)
                                      ->groupBy('style')
                                      ->get();
                foreach ($getDC as $dc) {
                    $sub = substr($dc->style, -3,3);
                    if ($sub=='TOP') {
                        DB::table('data_cuttings')
                          ->where('cutting_date',$pcd)
                          ->where('style',$dc->style)
                          ->where('articleno',$article)
                          ->where('size_category',$size_category)
                          ->where('warehouse',$factory_id)
                          ->update(['top_bot'=>'TOP']);
                    }else{
                      DB::table('data_cuttings')
                          ->where('cutting_date',$pcd)
                          ->where('style',$dc->style)
                          ->where('articleno',$article)
                          ->where('size_category',$size_category)
                          ->where('warehouse',$factory_id)
                          ->update(['top_bot'=>'BOT']);
                    }
                }

                $data_response = [
                            'status' => 200,
                            'output' => 'Split Top and Bottom success'
                          ];
         DB::commit();
       } catch (Exception $e) {
            DB::rollback();
            $message = $ex->getMessage();
            \ErrorHandler($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Split Top and Bottom field'
                          ];
       }

       return response()->json(['data' => $data_response]);
   }

   public function margeTopBot(Request $request){

        $pcd = $request->pcd;
        $style = $request->style;
        $article = $request->article;
        $size_ctg = $request->size_category;
        $factory_id = Auth::user()->factory_id;
        try {
           DB::begintransaction();
                $getDC = DB::table('data_cuttings')->select('style')
                                        ->where('cutting_date',$pcd)
                                        ->where('style','LIKE',$style.'%')
                                        ->where('articleno',$article)
                                        ->where('size_category',$size_ctg)
                                        ->where('warehouse',$factory_id)
                                        ->groupBy('style')
                                        ->get();
                  foreach ($getDC as $dc) {
                      $sub = substr($dc->style, -3,3);
                      if ($sub=='TOP') {
                          DB::table('data_cuttings')
                            ->where('cutting_date',$pcd)
                            ->where('style',$dc->style)
                            ->where('articleno',$article)
                            ->where('size_category',$size_ctg)
                            ->where('warehouse',$factory_id)
                            ->update(['top_bot'=>null]);
                      }else{
                        DB::table('data_cuttings')
                            ->where('cutting_date',$pcd)
                            ->where('style',$dc->style)
                            ->where('articleno',$article)
                            ->where('size_category',$size_ctg)
                            ->where('warehouse',$factory_id)
                            ->update(['top_bot'=>null]);
                      }
                  }

                  $data_response = [
                              'status' => 200,
                              'output' => 'Marge Top and Bottom success'
                            ];
           DB::commit();
         } catch (Exception $e) {
              DB::rollback();
              $message = $ex->getMessage();
              \ErrorHandler($message);
              $data_response = [
                              'status' => 422,
                              'output' => 'Marge Top and Bottom field'
                            ];
         }

         return response()->json(['data' => $data_response]);

   }

    static function dailySync()
    {
        // set date range
        $from = date(Carbon::now()->subDays(2)->format('Y-m-d'));
        $to = date(Carbon::now()->addDays(3)->format('Y-m-d'));

        // set factory
        if(\Auth::user()->factory_id == 2) {
            $factory_id = '1000012';
        } elseif(\Auth::user()->factory_id == 1) {
            $factory_id = '1000005';
        }  else {
            $factory_id = '0000000';
        }

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')->table('jz_csi_details')
                                ->whereBetween('datestartschedule', [$from, $to])
                                ->where('m_warehouse_id', $factory_id)
                                ->get();

        // get data cutting
        $cdms_cutting_datas =  DataCutting::whereBetween('cutting_date', [$from, $to])->where('warehouse', \Auth::user()->factory_id)
                                    ->get();


        $is_exists = '';

        // start datebase excecution
        try
        {
            DB::beginTransaction();

            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data)
            {

                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                                        ->where('documentno', $erp_csi_data->documentno)
                                        ->where('style', $erp_csi_data->style)
                                        ->where('po_buyer', $erp_csi_data->po_buyer)
                                        ->where('articleno', $erp_csi_data->kst_articleno)
                                        ->where('size_finish_good', $erp_csi_data->size_fg)
                                        ->where('size_category', $size_category)
                                        ->where('part_no', $erp_csi_data->part_no)
                                        ->where('warehouse', $warehouse);

                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {

                    // check data if update data
                    if($erp_csi_data->qtyordered != $is_exists->ordered_qty) {

                        // set data to update
                        $data_for_update['ordered_qty'] = $erp_csi_data->qtyordered;
                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);

                        // update data cutting
                        $data_is_exists->update($data_for_update);
                    }
                    else{
                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'update is_recycle',
                            'factory_id' => $warehouse,
                        ]);

                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                        $data_is_exists->update($data_for_update);
                    }

                } else {

                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => $erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    InfoSync::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data)
            {
                $check_count_data = 0;

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();

                    // insert table info
                    InfoSync::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            return response()->json('Sync Success . . . ');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json('Sync Field . . . ');
        }
    }

    static function dailySync7()
    {
        // set date range
        $from = date(Carbon::now()->subDays(2)->format('Y-m-d'));
        $to = date(Carbon::now()->addDays(5)->format('Y-m-d'));

        // set factory
        if(\Auth::user()->factory_id == 2) {
            $factory_id = '1000012';
        } elseif(\Auth::user()->factory_id == 1) {
            $factory_id = '1000005';
        }  else {
            $factory_id = '0000000';
        }

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')->table('jz_csi_details')
                                ->whereBetween('datestartschedule', [$from, $to])
                                ->where('m_warehouse_id', $factory_id)
                                ->get();

        // get data cutting
        $cdms_cutting_datas =  DataCutting::whereBetween('cutting_date', [$from, $to])->where('warehouse', \Auth::user()->factory_id)
                                    ->get();


        $is_exists = '';

        // start datebase excecution
        try
        {
            DB::beginTransaction();

            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data)
            {

                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                                        ->where('documentno', $erp_csi_data->documentno)
                                        ->where('style', $erp_csi_data->style)
                                        ->where('po_buyer', $erp_csi_data->po_buyer)
                                        ->where('articleno', $erp_csi_data->kst_articleno)
                                        ->where('size_finish_good', $erp_csi_data->size_fg)
                                        ->where('size_category', $size_category)
                                        ->where('part_no', $erp_csi_data->part_no)
                                        ->where('warehouse', $warehouse);

                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {

                    // check data if update data
                    if($erp_csi_data->qtyordered != $is_exists->ordered_qty) {

                        // set data to update
                        $data_for_update['ordered_qty'] = $erp_csi_data->qtyordered;
                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);

                        // update data cutting
                        $data_is_exists->update($data_for_update);
                    }
                    else{
                        // insert table info status update
                        InfoSync::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'update is_recycle',
                            'factory_id' => $warehouse,
                        ]);

                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                        $data_is_exists->update($data_for_update);
                    }

                } else {

                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => $erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    InfoSync::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data)
            {
                $check_count_data = 0;

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();

                    // insert table info
                    InfoSync::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            return response()->json('Sync Success . . . ');
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json('Sync Field . . . ');
        }
    }


    public function fabricPrepared(){
        return view('preparation/fabric_prepared');
    }

    public function getFabricPrepared(Request $request){

          if (!is_null($request->pcd)) {
              $date    = Carbon::createFromFormat('d F, Y', $request->pcd)->format('Y-m-d');
              $factory = Auth::user()->factory_id;

              $data = DB::table('ns_report_fabric_whs')
                      ->where('planning_date',$date)
                      ->where('factory_id',$factory);
          }else{
              $data = array();
          }

          return DataTables::of($data)
                            ->make(true);
    }

    public function exportFabricPrepared(Request $request){

        $date         = Carbon::createFromFormat('d F, Y', $request->mpcd)->format('Y-m-d');
        $current_date = Carbon::now()->format('Y-m-d');
        $factory      = Auth::user()->factory_id;

        if (!is_null($request->mpcd)) {

              $data = DB::table('ns_report_fabric_whs')
                      ->where('planning_date',$date)
                      ->where('factory_id',$factory)
                      ->orderBy('style','asc')
                      ->get();

          $filename = 'Report_fabric_prepared_PCD_'.$date;

          return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','Style');
                    $sheet->setCellValue('C1','Article');
                    $sheet->setCellValue('D1','Part No.');
                    $sheet->setCellValue('E1','Width');
                    $sheet->setCellValue('F1','Qty(yds)');
                    $sheet->setCellValue('G1','Item ID');
                    $sheet->setCellValue('H1','Color');
                    $sheet->setCellValue('I1','PO Buyer');

                    $index =1;

                    foreach ($data as $key => $dt) {

                        $row = $index +1;
                        $sheet->setCellValue('A'.$row,$index);
                        $sheet->setCellValue('B'.$row,$dt->style);
                        $sheet->setCellValue('C'.$row,$dt->article_no);
                        $sheet->setCellValue('D'.$row,$dt->part_no);
                        $sheet->setCellValue('E'.$row,$dt->actual_width);
                        $sheet->setCellValue('F'.$row,$dt->qty);
                        $sheet->setCellValue('G'.$row,$dt->item_id);
                        $sheet->setCellValue('H'.$row,$dt->color);
                        $sheet->setCellValue('I'.$row,$dt->po_buyer);
                        $index++;
                    }
                });
                $excel->setActiveSheetIndex(0);
          })->export('xlsx');


          // =========================================
        }

    }

    public function shootFabricWidth(Request $request){
          $barcode_id = $request->barcode_id;
          $id_plan = $request->id_plan;
          $part_no = $request->part_no;

          $plan = CuttingPlan::where('id',$id_plan)->whereNull('deleted_at')->first();

          $width = DB::table('data_cuttings')->where('style',$plan->style)->where('articleno',$plan->articleno)->where('part_no',$part_no)->first();

          try {
            db::begintransaction();
                $dtupdate = array(
                  'fabric_width'=>$width->width_size,
                  'color'=>$width->color_name,
                  'bay_pass'=>true
                );

                DB::table('cutting_marker')->where('barcode_id',$barcode_id)->whereNull('deleted_at')->update($dtupdate);

                  $data_response = [
                            'status' => 200,
                            'output' => 'Adjust Fabric Width success !!!'
                          ];

            db::commit();
          } catch (Exception $ex) {
            db::rollback();
              $message = $ex->getMessage();
                      \ErrorHandler($message);

            $data_response = [
                                'status' => 422,
                                'output' => 'Adjust Fabric Width failed !!!'
                              ];
          }

          return response()->json(['data' => $data_response]);


    }

    public function downloadPrioritas(Request $request)
    {
        $this->validate($request, [
            'date_cutting_download' => 'required',
        ]);

        $cutting_date = $request->date_cutting_download;

        $data = DB::table('jaz_report_prioritas_cutting')->where('cutting_date', $cutting_date)->whereNotNull('queu')->where('deleted_at', null)->orderBy('queu', 'asc');

        if(count($data->get()) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Prioritas_Cutting_'.$cutting_date;

            $i = 1;

            $export = \Excel::create($filename, function($excel) use ($data, $i) {
                $excel->sheet('report_prioritas', function($sheet) use($data, $i) {
                    $sheet->appendRow(array(
                        'QUEUE',
                        'CUTTING DATE',
                        'STYLE',
                        'PO BUYER',
                        'ARTICLE',
                        'SIZE CATEGORY',
                        'COLOR',
                        'QTY'
                    ));
                    $data->chunk(100, function($rows) use ($sheet, $i)
                    {
                        foreach ($rows as $row)
                        {
                            if($row->top_bot != null) {
                                $style = $row->style.'-'.$row->top_bot;
                            } else {
                                $style = $row->style;
                            }
                            $data_excel = array(
                                $row->queu,
                                Carbon::parse($row->cutting_date)->format('d-m-Y'),
                                $style,
                                $row->po_buyer,
                                $row->articleno,
                                $row->size_category,
                                $row->color,
                                $row->qty
                            );

                            $sheet->appendRow($data_excel);
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }

    public function getSizeBalance(Request $request)
    {
        if(request()->ajax())
        {
            $planning_id = $request->planning_id;

            if($planning_id != NULL) {
                $part_no = CuttingMarker::select('part_no')->where('id_plan', $planning_id)->where('deleted_at', null)->groupBy('part_no')->orderBy('part_no')->pluck('part_no')->toArray();

                foreach($part_no as $aa) {

                    $data_temp = array();
                    $data_rasio_size = array();
                    $data_final = array();
                    $data = array();
                    $data_final = array();

                    $data_marker = CuttingMarker::where('id_plan', $planning_id)->where('deleted_at', null)->where('part_no', $aa)->get();

                    $data_per_size = DB::table('jaz_detail_size_per_po_2')->select(DB::raw("size_finish_good, sum(qty) as qty"))->where('plan_id', $planning_id)->groupBy('size_finish_good')->orderByRaw('LENGTH(size_finish_good) asc')
                    ->orderBy('size_finish_good', 'asc')->get();

                    $data_temp[0][0] = 'Size';
                    $data_temp[1][0] = 'Total Kebutuhan';
                    $data_temp[2][0] = 'Total Marker';

                    for($i1 = 0; $i1 < 17; $i1++) {
                        if(isset($data_per_size[$i1])) {
                            $data_temp[0][$i1 + 1] = $data_per_size[$i1]->size_finish_good;
                            $data_temp[1][$i1 + 1] = $data_per_size[$i1]->qty;
                        } else {
                            $data_temp[0][$i1 + 1] = null;
                            $data_temp[1][$i1 + 1] = null;
                        }
                    }

                    foreach($data_marker as $a) {
                        foreach($a->rasio_markers as $b) {
                            $data_rasio_size[] = [
                                'size' => $b->size,
                                'rasio' => $b->ratio * $a->layer,
                            ];
                        }
                    }

                    for($i2 = 1; $i2 < 17; $i2++) {
                        $jumlah_qty = 0;
                        foreach($data_rasio_size as $a) {
                            if($data_temp[0][$i2] != null) {
                                if($a['size'] == $data_temp[0][$i2]) {
                                    $jumlah_qty += $a['rasio'];
                                }
                            }
                        }
                        if($jumlah_qty == 0) {
                            $jumlah_qty = null;
                        }
                        $data_temp[2][$i2] = $jumlah_qty;
                    }

                    for($i3 = 0; $i3 < 3; $i3++) {
                        if($i3 == 2) {
                            for($i4 = 0; $i4 < 16; $i4++) {
                                if($i4 == 0) {
                                    $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                } else {
                                    if($data_temp[$i3][$i4] != null) {
                                        if($data_temp[$i3][$i4] > $data_temp[$i3-1][$i4]) {
                                            $data_temp[$i3][$i4] = '<div class="bg-danger">'.$data_temp[$i3][$i4].'</div>';
                                        } elseif($data_temp[$i3][$i4] < $data_temp[$i3-1][$i4]) {
                                            $data_temp[$i3][$i4] = '<div class="bg-orange-300">'.$data_temp[$i3][$i4].'</div>';
                                        } else {
                                            $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                        }
                                    } else {
                                        $data_temp[$i3][$i4] = $data_temp[$i3][$i4];
                                    }
                                }
                            }
                        } else {
                            for($i5 = 0; $i5 < 16; $i5++) {
                                $data_temp[$i3][$i5] = $data_temp[$i3][$i5];
                            }
                        }

                        $data[] = [
                            'column1' => $data_temp[$i3][0],
                            'column2' => $data_temp[$i3][1],
                            'column3' => $data_temp[$i3][2],
                            'column4' => $data_temp[$i3][3],
                            'column5' => $data_temp[$i3][4],
                            'column6' => $data_temp[$i3][5],
                            'column7' => $data_temp[$i3][6],
                            'column8' => $data_temp[$i3][7],
                            'column9' => $data_temp[$i3][8],
                            'column10' => $data_temp[$i3][9],
                            'column11' => $data_temp[$i3][10],
                            'column12' => $data_temp[$i3][11],
                            'column13' => $data_temp[$i3][12],
                            'column14' => $data_temp[$i3][13],
                            'column15' => $data_temp[$i3][14],
                            'column16' => $data_temp[$i3][15],
                        ];
                    }

                    $data_final[] = $aa;

                    $data_final[] = $data;

                    $data_final_fix[] = $data_final;
                }


            }
            return response()->json(['data' => $data_final_fix],200);
        }
    }

    public function getCheckFB($id)
    {
        $data = array();
        $part_no = array();
        $cutting_date_ = null;

        $get_plan = CuttingPlan::where('id', $id)->first();

        $get_data_temp = DB::table('jaz_detail_size_per_part_2')->select('part_no', 'cutting_date')->where('plan_id', $get_plan->id)->groupBy('part_no', 'cutting_date')->orderBy('part_no', 'asc')->get();

        foreach($get_data_temp as $aa) {
            $part_no[] = $aa->part_no;
            $cutting_date_ = $aa->cutting_date;
        }

        $cutting_date_ = explode(',', $cutting_date_);

        $po_buyer = $get_plan->po_details->pluck('po_buyer')->toArray();

        $style = $get_plan->top_bot != null ? $get_plan->style.'-'.$get_plan->top_bot : $get_plan->style;

        foreach($part_no as $a) {
            $data_temp = array();
            $data_lebar = array();
            $data_lebar_temp = array();

            $get_data_fb = DB::connection('wms_live')->table('integration_whs_to_cutting')->select(DB::raw("actual_width, part_no, sum(qty_prepared) as qty_prepared"))->whereIn('planning_date', $cutting_date_)->where('style', $style)->where('article_no', $get_plan->articleno)->whereIn('po_buyer', $po_buyer)->where('part_no', $a)->groupBy('part_no', 'actual_width')->get();

            $get_data_uom_csi = DB::table('data_cuttings')->where('plan_id', $get_plan->id)->where('part_no', $a)->first()->uom;
            $get_data_csi = DB::table('data_cuttings')->where('plan_id', $get_plan->id)->where('part_no', $a)->sum('fbc');
            if($get_data_uom_csi == 'M') {
                $get_data_csi = $get_data_csi * 1.0936;
            }

            foreach($get_data_fb as $c) {
                $data_lebar_temp = array();

                $data_lebar_temp = [
                    'width' => $c->actual_width,
                    'qty' => $c->qty_prepared,
                ];

                $data_lebar[] = $data_lebar_temp;
            }

            $blc_qty = $get_data_fb->sum('qty_prepared') - $get_data_csi;

            if($blc_qty > 0) {
                $blc_qty = '<div class="bg-success">'.$blc_qty.'</div>';
            } elseif($blc_qty < 0) {
                $blc_qty = '<div class="bg-danger">'.$blc_qty.'</div>';
            } else {
                $blc_qty = $blc_qty;
            }

            $data_qty = [
                'csi_qty' => $get_data_csi,
                'whs_qty' => $get_data_fb->sum('qty_prepared'),
                'blc_qty' => $blc_qty,
            ];

            $data_temp['part_no'] = $a;
            $data_temp['qty'] = $data_qty;
            $data_temp['lebar'] = $data_lebar;

            $data[] = $data_temp;
        }

        $obj = new StdClass();
        $obj->data = $data;

		return response()->json($obj,200);
    }

    public function getCheckAlokasi($id)
    {
        if(request()->ajax())
        {
            RatioDetailTemp::where('plan_id', $id)->where('user_id', \Auth::user()->id)->delete();

            $new_data = CuttingMarker::where('id_plan', $id)->where('deleted_at', null)->get();

            $last_po_buyer = '';

            // foreach marker
            foreach($new_data as $get_data_marker) {

                try {
                    DB::begintransaction();

                    $queue_list = array();
                    $plan_id_list = array();

                    if($get_data_marker->combine_id == null) {
                        $queue_list[] = $get_data_marker->cutting_plan->queu;
                        $plan_id_list[] = $get_data_marker->cutting_plan->id;
                    } else {
                        $plan_id_list = CombinePart::where('combine_id', $get_data_marker->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                        foreach($plan_id_list as $planid) {
                            $get_data_plan = CuttingPlan::where('id', $planid)->where('deleted_at', null)->first();
                            $queue_list[] = $get_data_plan->queu;
                        }
                    }

                    // get informasi part in marker
                    $part_no = explode('+', $get_data_marker->part_no);

                    // set value sisa menjadi 0
                    $sisa = 0;

                    // foreach ratio size per marker
                    foreach($get_data_marker->rasio_markers as $b) {

                        $qty_marker_size = $get_data_marker->layer * $b->ratio;

                        // get qty per po per part no per size -> planning
                        $get_list_po = DB::table('jaz_detail_size_per_part_2')->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))->whereIn('plan_id', $plan_id_list)->where('part_no', $part_no[0])->orderBy('deliv_date', 'asc')->orderBy('qty', 'asc')->groupBy('po_buyer','deliv_date','part_no')->get();

                        foreach($get_list_po as $c) {
                            if($qty_marker_size > 0) {
                                $get_po_plot = RatioDetailTemp::where('plan_id', $get_data_marker->cutting_plan->id)->where('size', $b->size)->where('part_no', $c->part_no)->where('po_buyer', $c->po_buyer)->where('deleted_at', null)->sum('qty');

                                $get_po_plan = DB::table('jaz_detail_size_per_part_2')->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))->whereIn('plan_id', $plan_id_list)->where('part_no', $part_no[0])->where('po_buyer', $c->po_buyer)->where('size_finish_good', $b->size)->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')->first();

                                if($get_po_plan != null) {
                                    if($get_po_plot < $get_po_plan->qty) {
                                        if(($get_po_plan->qty - $get_po_plot) < $qty_marker_size) {
                                            RatioDetailTemp::FirstOrCreate([
                                                'barcode_id' => $get_data_marker->barcode_id,
                                                'plan_id' => $get_data_marker->cutting_plan->id,
                                                'ratio_id' => $b->id,
                                                'po_buyer' => $c->po_buyer,
                                                'size' => $b->size,
                                                'qty' => $get_po_plan->qty - $get_po_plot,
                                                'is_sisa' => 'f',
                                                'part_no' => $c->part_no,
                                                'user_id' => \Auth::user()->id,
                                                'factory_id' => \Auth::user()->factory_id,
                                            ]);

                                            $qty_marker_size = $qty_marker_size - ($get_po_plan->qty - $get_po_plot);

                                            $last_ratio_id = $b->id;
                                            $last_po_buyer = $c->po_buyer;
                                            $last_size = $b->size;
                                            $last_part_no = $c->part_no;
                                        } else {
                                            RatioDetailTemp::FirstOrCreate([
                                                'barcode_id' => $get_data_marker->barcode_id,
                                                'plan_id' => $get_data_marker->cutting_plan->id,
                                                'ratio_id' => $b->id,
                                                'po_buyer' => $c->po_buyer,
                                                'size' => $b->size,
                                                'qty' => $qty_marker_size,
                                                'is_sisa' => 'f',
                                                'part_no' => $c->part_no,
                                                'user_id' => \Auth::user()->id,
                                                'factory_id' => \Auth::user()->factory_id,
                                            ]);

                                            $qty_marker_size = 0;
                                        }
                                    } else {
                                        $last_po_buyer = $c->po_buyer;
                                    }
                                }
                            } else {
                                break;
                            }
                        }

                        if($qty_marker_size > 0) {
                            RatioDetailTemp::FirstOrCreate([
                                'barcode_id' => $get_data_marker->barcode_id,
                                'plan_id' => $get_data_marker->cutting_plan->id,
                                'ratio_id' => $b->id,
                                'po_buyer' => $last_po_buyer,
                                'size' => $b->size,
                                'qty' => $qty_marker_size,
                                'is_sisa' => 't',
                                'part_no' => $part_no[0],
                                'user_id' => \Auth::user()->id,
                                'factory_id' => \Auth::user()->factory_id,
                            ]);
                        }
                    }

                    DB::commit();
                }
                catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            $return_data = array();
            $return_data_temp = array();

            $get_header = DB::table('jaz_ratio_header_temp')->where('plan_id', $id)->where('user_id', \Auth::user()->id)->get();
            $get_cut = CuttingMarker::select('cut')->where('id_plan', $id)->where('deleted_at', null)->groupBy('cut')->orderBy('cut', 'asc')->pluck('cut')->toArray();

            array_push($return_data_temp, 'PO Buyer', 'Part No', 'Size', 'Qty Order', 'Qty Total');

            foreach($get_cut as $cut) {
                $return_data_temp[] = 'Cut '.$cut;
            }

            $return_data_temp[] = 'Balance';

            $return_data[] = $return_data_temp;

            foreach($get_header as $header) {
                $return_data_temp = array();
                $return_data_cut_temp = array();
                $qty_total = 0;
                $detail_plan = CuttingPlan::where('id', $header->plan_id)->where('deleted_at', null)->first();
                $part_no = explode('+', $header->part_no);

                $qty_order = DB::table('jaz_detail_size_per_part_2')->where('plan_id', $detail_plan->id)->where('part_no', $part_no[0])->where('factory_id', $detail_plan->factory_id)->where('po_buyer', $header->po_buyer)->where('size_finish_good', $header->size)->first()->qty;

                array_push($return_data_temp, $header->po_buyer, $header->part_no, $header->size, $qty_order);
                foreach($get_cut as $cut) {
                    $get_detail = DB::table('jaz_ratio_detail_temp')->where('plan_id', $id)->where('user_id', \Auth::user()->id)->where('po_buyer', $header->po_buyer)->where('size', $header->size)->where('part_no', $header->part_no)->where('cut', $cut)->first();
                    if($get_detail != null) {
                        $return_data_cut_temp[] = $get_detail->qty;
                        $qty_total = $qty_total + $get_detail->qty;
                    } else {
                        $return_data_cut_temp[] = '';
                    }
                }
                $balance = $qty_total - $qty_order;
                $return_data_temp[] = $qty_total;
                $return_data_cut_temp[] = $balance;
                $return_result = array_merge($return_data_temp, $return_data_cut_temp);
                $return_data[] = $return_result;
            }

            RatioDetailTemp::where('plan_id', $id)->delete();

            return response()->json($return_data,200);
        }
    }

    public function deleteCheckAlokasi(Request $request)
    {
        if(request()->ajax())
        {
            RatioDetailTemp::where('plan_id', $request->id)->where('user_id', \Auth::user()->id)->where('deleted_at', null)->delete();
            return response()->json('Delete data berhasil',200);
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
