@extends('layouts.scan',['active' => 'dashboard-scan'])

@section('page-css')
<style>
	.center-row {
		text-align: center !important;
	}
	.bg-red {
		background-color: red !important;
		color: white !important;
	}
	.bg-yellow {
		background-color: yellow !important;
	}
	.bg-green {
		background-color: green !important;
		color: white !important;
	}
	.bg-blue {
		background-color: blue !important;
		color: white !important;
	}
	.add-margin-right {
		margin-right: 20px;
	}

    .white-text {
        color: #ffffff !important;
        font-size: 30px;
    }

    .btn-scan {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .locator-title {
        margin-top: 10px;
        margin-bottom: 5px;

        text-size-adjust: 100pt;
    }

    .title-distribusi {
        padding-top: 0px !important;
    }

    .login-text{
        line-height: 36px;
        vertical-align:center !important;
    }

    .btn-custom-header {
        width: 80px;
    }


</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title text-center title-distribusi">
            <h1 class="white-text"><i class="icon-grid5 position-left"></i> <span class="text-semibold">SCAN LOCATOR {{$locator->locator_name}}</span> <i class="icon-grid5 position-right"></i> </h1>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('distribusi.index') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            {{-- <li><a href="{{ route('distribusi.menu',Request::segment(2))}}"> Menu</a></li> --}}
            <li class="active">Menu</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<fieldset class="content-group">
    <div class="row text-center">
        <div class="col-md-4">
            <a href="{{ route('distribusi.index') }}" class="btn btn-sm btn-info btn-custom-header pull-left"> BACK </a>
        </div>
        <div class="col-md-4 login-text">
            <label class="control-label col-md-12 col-xs-12">Anda login sebagai, <strong>{{ Auth::user()->name }}<strong></label>
        </div>
        <div class="col-md-4">
            <a href="#"
            {{-- <a href="{{ route('logout') }}" --}}
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
            class="btn btn-sm btn-danger pull-right btn-custom-header">
            Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</fieldset>

<div class="panel panel-flat">
    <div class="panel-body">
        {!! Form::hidden('_locator_id', Request::segment(2),array('id' => '_locator_id')) !!}

        @if (Request::segment(2)=='16')
            <a href="{{ route('distribusi.checkOut',Request::segment(2))}}" data-id='1' class="btn-scan btn btn-sm bg-primary-600 col-md-12 col-xs-12"> CHECK OUT </a>
        @else
            @if(!$locator->is_cutting)
                <a href="{{ route('distribusi.checkIn',Request::segment(2))}}" data-id='1' class="btn-scan btn btn-sm bg-violet-600 col-md-12 col-xs-12"> CHECK IN </a>
            @endif

            @if(!$locator->is_setting)
                <a href="{{ route('distribusi.checkOut',Request::segment(2))}}" data-id='1' class="btn-scan btn btn-sm bg-primary-600 col-md-12 col-xs-12"> CHECK OUT </a>
            @endif
        @endif

        {{-- <a href="{{ route('distribusi.menu','1')}}" data-id='1' class="btn-scan btn btn-sm bg-violet-600 col-md-12 col-xs-12"> CHECK IN </a> --}}
        <a href="{{ route('distribusi.checkBundle',Request::segment(2))}}" data-id='1' class="btn-scan btn btn-sm bg-grey-600 col-md-12 col-xs-12"> CHECK BUNDLE </a>
        {{-- <a href="{{ route('distribusi.menu','1')}}" data-id='1' class="btn-scan btn btn-sm bg-grey-600 col-md-12 col-xs-12"> CARI KOMPONEN </a> --}}
        {{-- <a href="{{ route('distribusi.menu','1')}}" data-id='1' class="btn-scan btn btn-sm bg-info-600 col-md-12 col-xs-12"> BANTUAN </a> --}}
        {{-- <a href="{{ route('distribusi.index')}}" data-id='1' class="btn-scan btn btn-sm bg-orange-600 col-md-12 col-xs-12"> PINDAH PROSES </a> --}}
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/notif_mo_update.js') }}"></script>
<script src="{{ mix('js/dashboard_monitoring.js') }}"></script>
@endsection
