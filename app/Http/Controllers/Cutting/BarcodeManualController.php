<?php

namespace App\Http\Controllers\Cutting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use DataTables;
use Carbon\Carbon;
use App\Models\TempPrintTicket;
use Uuid;

class BarcodeManualController extends Controller
{
    public function index()
    {
        $barcode_marker = DB::table('bundle_header')
        ->select('barcode_marker')
        ->where('factory_id',\Auth::user()->factory_id)
        ->where('created_at','>',Carbon::now()->subDays(90))
        //->where('statistical_date','>',Carbon::now())
        ->distinct()
        ->get();

        $po_buyer = DB::table('bundle_header')
        ->select('poreference')
        ->where('factory_id',\Auth::user()->factory_id)
        ->where('created_at','>',Carbon::now()->subDays(90))
        //->where('statistical_date','>',Carbon::now()->subDays(90))
        ->distinct()
        ->get();

        return view('cutting.barcode.manual_barcode', compact('barcode_marker','po_buyer'));
    }

    public function getDataDetailbundle(Request $request){

        if ($request->ajax()){
            $barcode_marker = $request->barcode_marker;

            $data = DB::table('bundle_header as bh')
            ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
            ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
            ->where('bh.barcode_marker',$barcode_marker)
            ->get();

            return DataTables::of($data)
            ->addColumn('cut_sticker',function ($data)
            {
                return $data->cut_num.' | '.$data->start_no.'-'.$data->end_no;
            })
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->addColumn('selector', function($data) {
                return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'">';
            })
            ->rawColumns(['style','cut_sticker','selector'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }
    public function printPdf(Request $request)
    {        
        $data = json_decode($request->data);
        $barcode_id = array();
                for ($i=0; $i < count($data); $i++) {
                    if ($data[$i][1] == true) {
                        $bar['barcode_id'] = $data[$i][8];
                        $barcode_id[] = $bar;
                    }
                }
                
            $map = explode(',',implode(",",array_map(function($a) {return implode("~",$a);},$barcode_id)));
            $data_detail_bundle = DB::table('bundle_detail as bd')
            ->select(
                'bd.barcode_id',
                'bd.bundle_header_id',
                'bd.style_detail_id',
                'bd.komponen_name',
                'bd.start_no',
                'bd.end_no',
                'bd.qty',
                'bd.location',
                'bd.sds_barcode',
                'bd.is_recycle',
                'bh.season',
                'bh.style',
                'bh.set_type',
                'bh.poreference',
                'bh.article',
                'bh.cut_num',
                'bh.size',
                'bh.factory_id',
                'bh.cutting_date',
                'bh.color_name',
                'bh.cutter',
                'bh.note',
                'bh.admin',
                'bh.qc_operator',
                'bh.bundle_operator',
                'bh.box_lim',
                'bh.max_bundle',
                'bh.barcode_marker',
                'bh.job',
                'v_master_style.part',
                'v_master_style.process',
                'v_master_style.process_name',
                'v_master_style.type_id',
                'v_master_style.type_name',
                'master_factory.factory_name',
                'master_factory.factory_alias',
                'bh.admin_name',
                'bh.qc_operator_name',
                'bh.bundle_operator_name',
                'bh.statistical_date',
                'bh.created_at',
                'bh.destination'
            )
            ->join('bundle_header as bh','bh.id','=','bd.bundle_header_id')
            ->join('v_master_style', 'v_master_style.id', '=', 'bd.style_detail_id')
            ->join('master_factory', 'master_factory.id', '=', 'bh.factory_id')
            ->whereIn('bd.barcode_id', $map)
            ->get();
                $pdf = \PDF::loadView('/cutting/barcode/print/index_man',['data_detail_bundle' => $data_detail_bundle])->setPaper('a4');

                return $pdf->stream("ManualPrint".'pdf');
    }

    public function getStyle(Request $request)
    {
        $po_buyer = $request->po_buyer;
        $style = DB::table('bundle_header')
        ->selectRaw("(CASE WHEN (set_type = 'Non') THEN style ELSE concat(style,'-',set_type) END) as style")
            ->where('poreference',$po_buyer)
            ->distinct()
            ->get();
        return response()->json(['response' => $style],200);
    }

    public function getCutnum(Request $request)
    {
        $po_buyer = $request->po_buyer;
        $cut_num = DB::table('bundle_header')
            ->select('cut_num')
            ->where('poreference',$po_buyer)
            ->distinct()
            ->get();
        return response()->json(['response' => $cut_num],200);
    }

    public function getDetailbypobundle(Request $request){

        if ($request->ajax()){
            $po_buyer = $request->po_buyer;
            $style = $request->style;
            $cut_num = $request->cut_num;
            if(strpos($request->style,'-TOP') > 1){
                if($request->po_buyer != null && $request->style != null && $request->cut_num != null){
                $length_char = strlen($request->style) - (strlen($request->style)-strpos($request->style,'-'));
                    $style = substr($request->style,0,$length_char);
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->where('bh.style',$style)
                    ->where('bh.set_type','TOP')
                    ->get();
                }elseif($request->po_buyer != null && $request->style != null){
                    $length_char = strlen($request->style) - (strlen($request->style)-strpos($request->style,'-'));
                    $style = substr($request->style,0,$length_char);
                    
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->where('bh.style',$style)
                    ->where('bh.set_type','TOP')
                    ->get();
                }elseif($request->po_buyer != null && $request->cut_num != null){
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->where('bh.set_type','TOP')
                    ->get();
                }else{
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.set_type','TOP')
                    ->get();
                }
            }elseif(strpos($request->style,'-BOTTOM') > 1){
                if($request->po_buyer != null && $request->style != null && $request->cut_num != null){
                    $length_char = strlen($request->style) - (strlen($request->style)-strpos($request->style,'-'));
                    $style = substr($request->style,0,$length_char);
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->where('bh.style',$style)
                    ->where('bh.set_type','BOTTOM')
                    ->get();
                }elseif($request->po_buyer != null && $request->style != null){
                    $length_char = strlen($request->style) - (strlen($request->style)-strpos($request->style,'-'));
                    $style = substr($request->style,0,$length_char);
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.style',$style)
                    ->where('bh.set_type','BOTTOM')
                    ->get();
                }elseif($request->po_buyer != null && $request->cut_num != null){
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->where('bh.set_type','BOTTOM')
                    ->get();
                }else{
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.set_type','BOTTOM')
                    ->get();
                }
            }else{
                if($request->po_buyer != null && $request->style != null && $request->cut_num != null){
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.style',$style)
                    ->where('bh.cut_num',$cut_num)
                    ->get();
                }elseif($request->po_buyer != null && $request->style != null){
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.style',$style)
                    ->get();
                }elseif($request->po_buyer != null && $request->cut_num != null){
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->where('bh.cut_num',$cut_num)
                    ->get();
                }else{
                    $data = DB::table('bundle_header as bh')
                    ->select(DB::raw('bd.bundle_header_id,bh.season,bh.style,bh.set_type,bh.cutting_date,bh.poreference,bh.article,bh.cut_num,bh.size,bh.color_name,bh.job,bd.qty,bd.komponen_name,bd.barcode_id,bd.start_no,bd.end_no'))
                    ->leftJoin('bundle_detail as bd','bd.bundle_header_id','=','bh.id')
                    ->where('bh.factory_id',\Auth::user()->factory_id)
                    ->where('bh.poreference',$po_buyer)
                    ->get();
                }
            }

            return DataTables::of($data)
            ->addColumn('cut_sticker',function ($data)
            {
                return $data->cut_num.' | '.$data->start_no.'-'.$data->end_no;
            })
            ->addColumn('style',function ($data)
            {
                if($data->set_type == 'Non'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->addColumn('selector', function($data) {
                return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'">';
            })
            ->rawColumns(['style','cut_sticker','selector'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function addNewData(Request $request){
        if(request()->ajax()){
            $poreference        = trim($request['poreference']);
            $cut_num            = trim($request['cut_num']);
            $style_num          = trim($request['style_num']);
            $size               = trim($request['size']);
            $part_name          = trim($request['part_name']);
            $start_num          = trim($request['start_num']);
            $factory_id         = Auth::user()->factory_id;

            if($style_num != null && $cut_num == null){
                return response()->json('MASUKKAN NO CUTTING',422);
            }
            if($part_name != null && $cut_num == null){
                return response()->json('MASUKKAN NO CUTTING',422);
            }

            if(strpos($style_num,'-BOT') > 0 || strpos($style_num,'BOT') > 0 || strpos($style_num,'BOTTOM') > 0 || strpos($style_num,'-BOTTOM') > 0 && strpos($style_num,'-TOP') > 0 || strpos($style_num,'TOP') > 0){
                if(strpos($style_num,'-') > 0){
                    $style_temp = explode('-',$style_num);
                    if(count($style_temp) > 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
                        $style_arr = [];
                        for($i=1;$i < count($style_temp) - 1;$i++){
                            $style_arr[] = $style_temp[$i];
                        }
                        $style      = implode('-',$style_arr);
                        $pos_set    = count($style_temp) - 1;
                        $set_type   = $style_temp[$pos_set] == 'TOP' ? 'TOP' : 'BOTTOM';
                    }elseif(count($style_temp) > 1 && count($style_temp) <= 2 && in_array('TOP',$style_temp) || in_array('BOTTOM',$style_temp)){
                        $style      = $style_temp[1];
                        $set_type   = $style_temp[2] == 'TOP' ? 'TOP' : 'BOTTOM';
                    }elseif(count($style_temp) > 2 && !in_array('TOP',$style_temp) || !in_array('BOTTOM',$style_temp)){
                        $style_arr = [];
                        for($i=1;$i < count($style_temp); $i++){
                            $style_arr[] = $style_temp[$i];
                        }
                        $style = implode('-',$style_arr);
                        $set_type   = '0';
                    }else{
                        $style      = $style_temp[1];
                        $set_type   = '0';
                    }
                }else{
                    $len    = strlen($style_num);
                    if(strpos($style_num,'BOT') > 0){
                        $pos = strpos($style_num,'BOT');
                    }elseif(strpos($style_num,'BOTTOM') > 0){
                        $pos = strpos($style_num,'BOTTOM');
                    }elseif(strpos($style_num,'-BOT') > 0){
                        $pos = strpos($style_num,'-BOT');
                    }elseif(strpos($style_num,'-BOTTOM') > 0){
                        $pos = strpos($style_num,'-BOTTOM');
                    }else{
                        $pos = strpos($style_num,'TOP');
                    }
                    $sub = ($len - $pos)+1;
                    $set_type = substr($style_num,$pos,$sub);
                    $style = substr($style_num,0,$pos);
                }
            }else{
                $style = $style_num;
                $set_type = '0';
            }
            $data  = $request->all();
            $clauses = [
                'poreference'       => trim(strtoupper($data['poreference'])),
                'cut_num'           => trim(strtoupper($data['cut_num'])),
                'article'           => trim(strtoupper($data['article'])),
                'size'              => trim(strtoupper($data['size'])),
                'komponen'          => trim(strtoupper($data['part_name'])),
                'start_no'          => trim(strtoupper($data['start_num'])),
                'end_no'            => trim(strtoupper($data['end_num'])),
                'style'             => trim(strtoupper($style)),
            ];
            $param = array_filter($clauses);
            $x=[];
            $y=[];
            foreach($param as $p => $key){
                $x[] = $p;
                $y[] = $key;
            }
            $z1 = $x;
            $z2 = $y;
            $params = [];
            for($i=0;$i < count($z1);$i++){
                $params[] = $z1[$i].'='."'".$z2[$i]."'";
            }
            $where_clause   = implode(' and ',$params);
            $added_clauses = 'DETAIL '.implode(' DAN ',$params).' SUDAH DITAMBAHKAN!';
            
                // $existing_data = TempPrintTicket::where('poreference',$poreference)
                // ->where('style',$style)
                // ->where('set_type',$set_type)
                // ->where('cut_num',$cut_num)
                // ->where('komponen',trim($part_name))
                // ->get();
                $existing_data      = DB::select(DB::raw("SELECT * FROM temp_print_ticket WHERE $where_clause AND set_type='$set_type'"));
                if(count($existing_data) > 0){
                    return response()->json($added_clauses,422);
                }else{
                    // $check = DB::connection('sds_live')->table('dd_bundle_info_new')
                    // ->where('poreference',$poreference)
                    // ->where('style',$style)
                    // ->where('set_type',$set_type)
                    // ->where('cut_num',$cut_num)
                    // ->where('part_name',trim($part_name))
                    // ->where('processing_fact',\Auth::user()->factory_id)
                    // ->first();

                    $data2  = $request->all();
                    $clauses2 = [
                        'poreference'       => trim(strtoupper($data2['poreference'])),
                        'cut_num'           => trim(strtoupper($data2['cut_num'])),
                        'article'           => trim(strtoupper($data2['article'])),
                        'size'              => trim(strtoupper($data2['size'])),
                        'part_name'         => trim(strtoupper($data2['part_name'])),
                        'start_num'         => trim(strtoupper($data2['start_num'])),
                        'end_num'           => trim(strtoupper($data2['end_num'])),
                        'style'             => trim(strtoupper($style)),
                    ];
                    $param2 = array_filter($clauses2);
                    $x2=[];
                    $y2=[];
                    foreach($param2 as $p2 => $key2){
                        $x2[] = $p2;
                        $y2[] = $key2;
                    }
                    $z12 = $x2;
                    $z22 = $y2;
                    $params2 = [];
                    $params3 = [];
                    for($i=0;$i < count($z12);$i++){
                        $params2[] = 'bi.'.$z12[$i].'='."'".$z22[$i]."'";
                        $params3[] = $z12[$i].'='."'".$z22[$i]."'";
                    }
                    $where_clause2 = implode(' and ',$params2);
                    $where_clause3 = implode(' and ',$params3);
                    $nullable_data2 = 'DETAIL '.implode(' DAN ',$params2).' TIDAK DITEMUKAN!';

                    $check = DB::connection('sds_live')->select(DB::raw("SELECT * FROM dd_bundle_info_new WHERE $where_clause3 AND processing_fact='$factory_id'"));
                    if($check == null){
                        return response()->json($nullable_data2,422);
                    }
                    // $data = DB::connection('sds_live')
                    // ->table('bundle_info_new as bi')
                    // ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
                    // ->Join('art_desc_new as ad',function($j){
                    //     $j->on('ad.season','bi.season')
                    //     ->on('ad.style','bi.style')
                    //     ->on('ad.part_num','bi.part_num')
                    //     ->on('ad.part_name','bi.part_name');
                    // })
                    // ->where('bi.poreference',$poreference)
                    // ->where('bi.style',$style)
                    // ->where('ad.set_type',$set_type)
                    // ->where('bi.cut_num',$cut_num)
                    // ->where('bi.part_name',$part_name)
                    // ->where('bi.processing_fact',Auth::user()->factory_id)
                    // ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
                    // ->get();
                    $data = DB::connection('sds_live')->select(DB::raw("SELECT bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id FROM bundle_info_new bi JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name WHERE $where_clause2 AND processing_fact='$factory_id' AND ad.set_type='$set_type'"));
                }
            // }elseif($cut_num != null && $size != null){
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)
            //     ->where('cut_num',$cut_num)
            //     ->where('size',$size)
            //     ->get();
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' Size '.$size.' Sticker '.$start_num.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('cut_num',$cut_num)
            //         ->where('size',$size)
            //         ->where('processing_fact',\Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' Size '.$size.' Sticker '.$cut_num.' Tidak Ditemukan!',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.cut_num',$cut_num)
            //         ->where('bi.size',$size)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }elseif($part_name != null && $start_num != null){
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)
            //     ->where('start_no',$start_num)
            //     ->where('komponen',$part_name)
            //     ->get();
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' Komponen '.$part_name.' Sticker '.$start_num.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('part_name',$part_name)
            //         ->where('start_num',$start_num)
            //         ->where('processing_fact',\Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' Komponen '.$part_name.' Sticker '.$start_num.' Tidak Ditemukan!',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.part_name',$part_name)
            //         ->where('bi.start_num',$start_num)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }elseif($cut_num != null && $part_name != null){
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)->where('cut_num',$cut_num)->where('komponen',$part_name)->get();
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' CUT '.$cut_num.' Komponen '.$part_name.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('cut_num',$cut_num)
            //         ->where('part_name',$part_name)
            //         ->where('processing_fact',\Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' KOMPONEN '.$part_name.' CUT NUM '.$cut_num.' TIDAK DITEMUKAN',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.cut_num',$cut_num)
            //         ->where('bi.part_name',$part_name)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }elseif($cut_num != null && $style_num != null){
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)->where('cut_num',$cut_num)->where('style',$style)->where('set_type',$set_type)->get();
            //     $type_name = $set_type == 0 ? null : '-'.$set_type;
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' CUT '.$cut_num.' Style '.$style.$type_name.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('style',$style)
            //         ->where('set_type',$set_type)
            //         ->where('cut_num',$cut_num)
            //         ->where('processing_fact',Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' CUT NUM '.$cut_num.' Style '.$style.$type_name.' TIDAK DITEMUKAN',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.cut_num',$cut_num)
            //         ->where('bi.style',$style)
            //         ->where('ad.set_type',$set_type)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }elseif($cut_num != null){
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)->where('cut_num',$cut_num)->get();
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' CUT '.$cut_num.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('cut_num',$cut_num)
            //         ->where('processing_fact',\Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' CUT NUM '.$cut_num.' TIDAK DITEMUKAN',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.cut_num',$cut_num)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }else{
            //     $existing_data = TempPrintTicket::where('poreference',$poreference)->get();
            //     if(count($existing_data) > 0){
            //         return response()->json('PO '.$poreference.' Sudah Ditambahkan!',422);
            //     }else{
            //         $check = DB::connection('sds_live')->table('dd_bundle_info_new')
            //         ->where('poreference',$poreference)
            //         ->where('processing_fact',\Auth::user()->factory_id)
            //         ->first();
            //         if($check == null){
            //             return response()->json('DETAIL PO '.$poreference.' TIDAK DITEMUKAN',422);
            //         }
            //         $data = DB::connection('sds_live')
            //         ->table('bundle_info_new as bi')
            //         ->selectRaw('bi.barcode_id,bi.season,bi.style,ad.set_type,bi.poreference,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.qty,bi.start_num,bi.end_num,bi.note,bi.coll,bi.cut_id')
            //         ->selectRaw('bi.barcode_id,bi.season,')
            //         ->Join('art_desc_new as ad',function($j){
            //             $j->on('ad.season','bi.season')
            //             ->on('ad.style','bi.style')
            //             ->on('ad.part_num','bi.part_num')
            //             ->on('ad.part_name','bi.part_name');
            //         })
            //         ->where('bi.poreference',$poreference)
            //         ->where('bi.processing_fact',Auth::user()->factory_id)
            //         ->orderBy('bi.poreference','bi.cut_num','bi.size','bi.start_num','bi.part_num')
            //         ->get();
            //     }
            // }

            foreach($data as $d){
                $header_data = DB::connection('sds_live')
                ->table('cut_data_new')
                ->where('id',$d->cut_id)
                ->first();
                $csi_sds = DB::connection('sds_live')->table('ppcm_new')->where('poreference',$d->poreference)->first();
                $cdms_barcode = DB::table('bundle_detail')->where('sds_barcode',$d->barcode_id)->first();
                $is_recycle = DB::table('data_cuttings')
                ->where(function($w) use ($d) {
                    $w->where('po_buyer',$d->poreference)
                    ->orWhere('job_no','like','%'.$d->poreference.'%');
                })
                ->where('warehouse',Auth::user()->factory_id)
                ->whereNull('deleted_at')
                ->first();
                try{
                    DB::begintransaction();
                        TempPrintTicket::firstorCreate([
                            'barcode_id'    => $d->barcode_id,
                            'barcode_cdms'  => $cdms_barcode == null ? null : $cdms_barcode->barcode_id,
                            'season'        => $d->season,
                            'style'         => $d->style,
                            'set_type'      => $d->set_type,
                            'poreference'   => $d->poreference,
                            'article'       => $header_data->article,
                            'cut_num'       => $d->cut_num,
                            'size'          => $d->size,
                            'part'          => $d->part_num,
                            'komponen'      => $d->part_name,
                            'qty'           => $d->qty,
                            'start_no'      => $d->start_num,
                            'end_no'        => $d->end_num,
                            'box_lim'       => $header_data->box_lim,
                            'note'          => $d->note,
                            'job'           => $d->style.'::'.$d->poreference.'::'.$header_data->article,
                            'podd'          => $csi_sds->podd,
                            'ship_to'       => $csi_sds->ship_to,
                            'coll'          => $d->coll,
                            'bd_op'         => $header_data->bd_op,
                            'qc_op'         => $header_data->qc_op,
                            'cutter'        => $header_data->cutter,
                            'ed_pcd'        => $header_data->ed_pcd,
                            'add_note'      => $header_data->add_note,
                            'created_at'    => Carbon::now(),
                            'created_by'    => Auth::user()->id,
                            'factory_id'    => Auth::user()->factory_id,
                            'is_recycle'    => $is_recycle->is_recycle
                        ]);
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollback();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
            return response()->json('ok',200);
        }
    }

    public function getDataTicket(Request $request){
        if(request()->ajax()) 
        {
            $data = TempPrintTicket::where('factory_id',Auth::user()->factory_id)
                    ->where('created_by',Auth::user()->id)
                    ->orderBy('style','ASC')
                    ->orderBy('set_type','ASC')
                    ->orderBy('cut_num','ASC')
                    ->orderBy('size','ASC')
                    ->orderBy('start_no','ASC')
                    ->orderBy('komponen','ASC')
                    ->get();

            return DataTables::of($data)
            ->editColumn('style',function($data){
                if($data->set_type == '0'){
                    return $data->style;
                }else{
                    return $data->style.'-'.$data->set_type;
                }
            })
            ->editColumn('cut_sticker',function($data){
                return $data->cut_num.' | '.$data->start_no.'-'.$data->end_no;
            })
            ->editColumn('komponen',function($data){
                return $data->part.' | '.$data->komponen;
            })
            ->addColumn('action', function($data){
                if($data->id != null){
                    return '<a href="#" data-id="'.$data->id.'" class="ignore-click delete_b btn btn-danger btn-raised btn-xs"><i class="icon-trash"></i></a>';
                }
            })
            ->rawColumns(['action','style','cut_sticker','komponen'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function deleteBarcode(Request $request){
        $id = $request->id;
        try{
            DB::begintransaction();
                TempPrintTicket::where('id', $id)->delete();
            DB::commit();
        }catch (Exception $e) {
            DB::rollback();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('ROW DELETED',200);
    }

    public function deleteAllDetail(Request $request){  
        $user = $request['user'];
        $check_data = TempPrintTicket::where('created_by',$user)->get();
        if(count($check_data) > 0){
            try{
                DB::begintransaction();
                    TempPrintTicket::where('created_by', $user)->delete();
                DB::commit();
            }catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            return response()->json('DATA DELETED',200);
        }else{
            return response()->json('Detail Ticket Belum Ditambahkan',422);
        }
    }

    public function printTicketAll($ticket_box,$user,$factory){
        $data = DB::table('temp_print_ticket')
        ->where('created_by',$user)
        ->where('factory_id',$factory)
        ->orderBy('poreference','ASC')
        ->orderBy('season','ASC')
        ->orderBy('cut_num','ASC')
        ->orderBy('style','ASC')
        ->orderBy('set_type','ASC')
        ->orderBy('article','ASC')
        ->orderBy('size','ASC')
        ->orderBy('start_no','ASC')
        ->orderBy('komponen','ASC')
        ->get();        
        if(count($data) > 0){
            if($ticket_box == '1'){
                $checklist = $this->checklistBox($data);
                return view('cutting.barcode.print.index_man_com',compact('data','checklist'));
            }else{
                return view('cutting.barcode.print.index_man_com',compact('data'));
            }
        }else{
            return response()->json('Detail Tiket Belum Ditambahkan!',422);
        }
    }

    public function sessionDestroy($user){
        //DELETE TEMPORARY DATA
        TempPrintTicket::where('created_by',$user)->delete();

        $barcode_marker = DB::table('bundle_header')
        ->select('barcode_marker')
        ->where('factory_id',\Auth::user()->factory_id)
        ->where('created_at','>',Carbon::now()->subDays(90))
        ->distinct()
        ->get();

        $po_buyer = DB::table('bundle_header')
        ->select('poreference')
        ->where('factory_id',\Auth::user()->factory_id)
        ->where('created_at','>',Carbon::now()->subDays(90))
        ->distinct()
        ->get();

        return view('cutting.barcode.manual_barcode', compact('barcode_marker','po_buyer'));
    }

    private function checklistBox($data){
        $cut_season=trim($data[0]->season);
        $cut_style=trim($data[0]->style);
        $cut_cut_num=$data[0]->cut_num;
        $lim = ($data[0]->box_lim == '' ||  $data[0]->box_lim == '') ? 9999999999 : $data[0]->box_lim;
        $c_art='';
        $c_po='';
        $c_style='';
        $c_ssn='';
        $c_cut_num=-999;
        $c_size='';
        $c_start=-999;
        $sum=0;
        $comp='';
        $t_num=0;
        $box_ticket=array();
        foreach($data as $box){
            $style=trim($box->style).trim($box->set_type);
            $po=trim($box->poreference);
            $size=trim($box->size);
            $comp=trim($box->part).' - '.trim($box->komponen);
            if(($box->season==$c_ssn)&&($box->article==$c_art)&&($po==$c_po)&&($style==$c_style)&&($box->cut_num==$c_cut_num)&&($size==$c_size)){
                if(($sum+($box->qty)>$lim)&&($box->start_no!=$c_start)){
                    $t_num+=1;
                    $sum=$box->qty;
                    $c_start=$box->start_no;
                    $box_ticket[$t_num][$comp]=$box;
                }else{
                    if($box->start_no==$c_start){
                        if(isset($box_ticket[$t_num][$comp])){
                            $box_ticket[$t_num][$comp]->qty=$sum;
                            $box_ticket[$t_num][$comp]->end_num=$box->end_num;
                        }else{
                            $box_ticket[$t_num][$comp]=$box;
                        }
                    }else{
                        $c_start=$box->start_no;
                        $sum+=$box->qty;
                        if(isset($box_ticket[$t_num][$comp])){
                            $box_ticket[$t_num][$comp]->qty=$sum;
                            $box_ticket[$t_num][$comp]->end_num=$box->end_num;
                        }else{
                            $box_ticket[$t_num][$comp]=$box;
                        }
                    }
                }
            }else{
                $t_num+=1;
                $c_po=$po;
                $c_art=$box->article;
                $c_ssn=$box->season;
                $c_style=$style;
                $c_cut_num=$box->cut_num;
                $c_size=$size;
                $c_start=$box->start_no;
                $sum=$box->qty;
                $box_ticket[$t_num][$comp]=$box;
            }
        }
        return $box_ticket;
    }
}
