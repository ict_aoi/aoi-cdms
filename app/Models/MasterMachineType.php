<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMachineType extends Model
{
	public $incrementing = true;
	protected $guarded = ['id'];
	protected $table = 'master_machine_type';
	protected $fillable = ['id','machine_type','created_at','created_by','deleted_at','deleted_by','updated_at','updated_by'];
}
