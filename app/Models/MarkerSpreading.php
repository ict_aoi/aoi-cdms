<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
class MarkerSpreading extends Model
{
    use Uuids;
    public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'marker_spreading';
	protected $fillable = ['id_marker','actual_length_qc','actual_width_qc','spreading_high_qc','remark_qc','created_at','updated_at','deleted_at'];
}
