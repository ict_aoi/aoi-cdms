$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    var table = $('#planningTable').DataTable({
        ajax: {
            type: 'get',
            url: '/fabric-need-info/data-planning',
            data: {
                cutting_date: $('#cutting_date').val(),
                factory: $("input[name='factory']:checked").val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            // {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'queu', name: 'queu',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'poreference', name: 'poreference',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'size_category', name: 'size_category',searchable:true,orderable:true},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:false,orderable:false},
            {data: 'last_updated', name: 'last_updated',searchable:false,orderable:false},
            {data: 'detail', name: 'detail',searchable:false,orderable:false},
        ],
    });

    $('#planningTable').on('click','.ignore-click', function() {
        return false;
    });

    $('#form_filter_date').submit(function(event){
        event.preventDefault();
        var cutting_date = $('#cutting_date').val();
        var factory = $("input[name='factory']:checked").val();

        if(!cutting_date){
            $("#alert_warning").trigger("click", 'Tanggal cutting wajib diisi!');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url : '/fabric-need-info/data-planning',
            data: {
                cutting_date: cutting_date,
                factory: factory,
            },
            beforeSend: function() {
                $('#planningTable').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('#planningTable').unblock();

                table.clear();
                table.rows.add(response['data']);
                table.draw();
            },
            error: function(response) {
                $('#planningTable').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#detailModal').on('hidden.bs.modal', function(){
        $('#form_filter_date').submit();
    });

    $('#detailDetailModal').on('hidden.bs.modal', function(){
        var planning_id_detail = $('#planning_id_detail').val();
        $('#form_filter_date').submit();
        $('#planning_id').val(planning_id_detail);
        $('#detailModal').modal();
        get_data_modal_table()
    });

    $('#btn_download_data').on('click', function() {
        var planning_id = $('#planning_id').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "post",
            url: $('#generateDownloadLink').attr('href'),
            data: {
                planning_id: planning_id,
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
                window.open(response.download_link, '_blank');
            },
            error: function(response) {
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });
});

function detailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            $('#planning_id').val(response.id);
            $('#style_detail_view').text(response.style);
            $('#articleno_detail_view').text(response.articleno);
            $('#cutting_date_detail_view').text(response.cutting_date);
            $('#po_detail_view').text(response.poreference);
            $('#detailModal').modal();
            get_data_modal_table();
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function detailDetailModal(url) {
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
            if(response.user == null) {
                $('#planning_id_detail').val(response.id_plan);
                $('#part_no_detail').val(response.part_no);
                $('#color_detail').val(response.color);
                $('#style_detail_detail_view').text(response.style);
                $('#articleno_detail_detail_view').text(response.articleno);
                $('#cutting_date_detail_detail_view').text(response.cutting_date);
                $('#part_no_detail_detail_view').text(response.part_no);
                $('#detailModal').modal('toggle');
                $('#detailDetailModal').modal('toggle');
                get_data_over_consum_table();
                get_data_modal_detail_table();
                get_data_lebar_fb_table();
                get_data_size_balance_table();
            } else {
                $('#planning_id').val(response.id_plan);
                $('#style_detail_view').text(response.style);
                $('#articleno_detail_view').text(response.articleno);
                $('#cutting_date_detail_view').text(response.cutting_date);
                $('#detailModal').modal();
                get_data_modal_table();

                $("#alert_warning").trigger("click", 'Halaman ini sedang di edit oleh '+ response.user);
                return false;
            }
        },
        error: function (response) {
            $.unblockUI();
            console.log(response);
        }
    });
}

function get_data_modal_table() {
    var table1 = $('#detailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/fabric-need-info/data-marker',
            data: {
                planning_id: $('#planning_id').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'part_no', name: 'part_no', searchable:true, visible:true, orderable:false},
            {data: 'total_cutting', name: 'total_cutting', searchable:false, visible:true, orderable:false},
            {data: 'status', name: 'status', searchable:false, visible:true, orderable:false},
            {data: 'detail', name: 'detail', searchable:false, visible:true, orderable:false},
        ],
    });

    return table1;
}

function get_data_modal_detail_table() {
    var table1 = $('#detailDetailTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/fabric-need-info/data-detail-marker',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: [0],
                className: 'dt_col_hide',
            },
        ],
        columns: [
            {data: 'id_row', name: 'id_row', searchable: false},
            {data: 'cut', name: 'cut', searchable: true, visible:true, orderable:false},
            {data: 'barcode_marker', name: 'barcode_marker', searchable: true, visible:true, orderable:false},
            {data: 'color', name: 'color', searchable:true, visible:true, orderable:false},
            {data: 'rasio', name: 'rasio', searchable:true, visible:true, orderable:false},
            {data: 'layer', name: 'layer', searchable:true, visible:true, orderable:false},
            {data: 'lebar_fb', name: 'lebar_fb', searchable:true, visible:true, orderable:false},
            {data: 'lebar_marker', name: 'lebar_marker', searchable:true, visible:true, orderable:false},
            {data: 'perimeter', name: 'perimeter', searchable:true, visible:true, orderable:false},
            {data: 'actual_marker', name: 'actual_marker', searchable:true, visible:true, orderable:false},
            {data: 'need_total', name: 'need_total', searchable:true, visible:true, orderable:false},
        ],
    });

    return table1;
}

function get_data_lebar_fb_table() {
    var table2 = $('#lebarFBTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/fabric-need-info/data-lebar-fabrik',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'lebar_fb', name: 'lebar_fb', searchable:true, visible:true, orderable:false},
            {data: 'alokasi_fb', name: 'alokasi_fb', searchable:true, visible:true, orderable:false},
            {data: 'need_total', name: 'need_total', searchable:true, visible:true, orderable:false},
        ],
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column(2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column(2).footer() ).html(
                total +' <strong>(Total Kebutuhan)</strong>'
            );
        }
    });

    return table2;
}

function get_data_size_balance_table() {
    var table3 = $('#sizeBalanceTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/fabric-need-info/data-size-balance',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center',
            },
        ],
        columns: [
            {data: 'column1', name: 'column1', searchable:false, visible:true, orderable:false},
            {data: 'column2', name: 'column2', searchable:false, visible:true, orderable:false},
            {data: 'column3', name: 'column3', searchable:false, visible:true, orderable:false},
            {data: 'column4', name: 'column4', searchable:false, visible:true, orderable:false},
            {data: 'column5', name: 'column5', searchable:false, visible:true, orderable:false},
            {data: 'column6', name: 'column6', searchable:false, visible:true, orderable:false},
            {data: 'column7', name: 'column7', searchable:false, visible:true, orderable:false},
            {data: 'column8', name: 'column8', searchable:false, visible:true, orderable:false},
            {data: 'column9', name: 'column9', searchable:false, visible:true, orderable:false},
            {data: 'column10', name: 'column10', searchable:false, visible:true, orderable:false},
            {data: 'column11', name: 'column11', searchable:false, visible:true, orderable:false},
            {data: 'column12', name: 'column12', searchable:false, visible:true, orderable:false},
            {data: 'column13', name: 'column13', searchable:false, visible:true, orderable:false},
            {data: 'column14', name: 'column14', searchable:false, visible:true, orderable:false},
            {data: 'column15', name: 'column15', searchable:false, visible:true, orderable:false},
            {data: 'column16', name: 'column16', searchable:false, visible:true, orderable:false},
        ],
    });

    return table3;
}

function get_data_over_consum_table() {
    var table4 = $('#overConsumTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/fabric-need-info/data-over-consum',
            data: {
                planning_id: $('#planning_id_detail').val(),
                part_no: $('#part_no_detail').val(),
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData < 0 ) {
                        $(td).css({'color': 'white', 'backgroundColor': 'red'})
                    }
                }
            },
        ],
        columns: [
            {data: 'qty_csi', name: 'qty_csi', searchable:true, visible:true, orderable:false},
            {data: 'qty_need', name: 'qty_need', searchable:true, visible:true, orderable:false},
            {data: 'over_consum', name: 'over_consum', searchable:true, visible:true, orderable:false},
        ],
    });

    return table4;
}
