<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportRollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_roll', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('barcode', 30);
            $table->string('no_roll', 10);
            $table->string('material');
            $table->string('color');
            $table->string('actual_lot', 30);
            $table->double('actual_width', 15, 8);
            $table->double('qty_prepared', 15, 8);
            $table->timestamps();
            $table->timestamp('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_roll');
    }
}
