<div id="insertRoleModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => route('role.store'),
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'insert_role'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							@include('form.text', [
								'field' => 'name',
								'label' => 'Nama',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'name'
								]
							])

							@include('form.textarea', [
								'field' => 'description',
								'label' => 'Description',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'description',
									'rows' => 5,
									'style' => 'resize: none;'
								]
							])

							@include('form.select', [
								'field' => 'permission',
								'label' => 'Permission',
								'options' => [
									'' => '-- Pilih Permission --',
								]+$permissions,
								'class' => 'select-search',
								'attributes' => [
									'id' => 'select_permission'
								]
							])
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h6 class="panel-title text-semibold">Role Permission &nbsp; <span class="label label-info heading-text">Mapping Role with Permission Role.</span></h6>
								</div>

								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Permission</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="role_permission"></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>
				{!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>