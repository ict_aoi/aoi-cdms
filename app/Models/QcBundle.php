<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class QcBundle extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];
	// protected $table = 'cutting_marker';

    protected $fillable = ['barcode_id', 'bundle_table_id', 'status', 'created_by', 'deleted_at', 'created_at', 'updated_at','defect','locator_id','subcont_id','factory_id','total_cek','operator_name'];
}
