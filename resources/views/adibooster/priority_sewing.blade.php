@extends('adibooster.index')
@section('page-css')
<style type="text/css">
.header{
    font-family:arial;
    font-size:  50px;
}
.text_header{
    font-weight: bold;
    text-align: center;
}
.time_update{
    text-align: center;
    font-weight: bold;
    text-align: center;
    font-size: 25px;
}
.content_header_1{
    text-align: left;
    font-weight: bold;
    font-size:18px;
    padding-left: 70px;
}
.content_header_2{
    text-align: right;
    font-weight: bold;
    font-size:18px;
    padding-right: 70px;
}
.table_1{
    background-color: orange;
    opacity: 0.8;
    color:white;
    font-size:20px;
    text-align:center;
    font-weight:bold;
    border:2.6px solid white;
}
.table_content_3{
    margin-top:10px;
    width:100%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}
.table_content_4{
    margin-top:0px;
    width:100%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}
.text_content{
    font-size:20px;
    font-weight:bold;
    border:3px solid white;
}
.text_content2{
    font-size:17px;
    font-weight:bold;
    border:1px solid white;
}
.text_content3{
    font-size:17px;
    font-weight:bold;
    border:1px solid white;
    background-color: #00FFFF;
}
.filled_output{
    background-color:grey;
    opacity: 0.5;
    border:2.6px solid white;
}
.filled_output2{
    font-size:18px;
    color:white,
    text-align:center;
    font-weight:bold;
}
.table-responsive{
    height:240px; 
    width:100%;
    overflow-y: hidden;
    margin-left: auto;
    margin-right: auto;
}
</style>
@endsection
@section('page-content')
<meta http-equiv="refresh" content="300"> <!-- Refresh every 15 minutes -->
<table style="width: 100%;margin-top:10px">
    <thead class="bg-blue-700">
        <tr class="header">
            <th class="text_header" colspan="6">DASHBOARD PRIORITAS AOI {{$factory_id}}</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr class="bg-blue-700">
            <td colspan="6" class="time_update">PLAN SEWING: {{\Carbon\Carbon::now()->format('d M Y')}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="bg-blue-700">
            <td colspan="6" class="pull-right" style="font-size:20px;font-weight:bold;margin-right:10px">UPDATED AT : {{\Carbon\Carbon::now('GMT+7')->format('H:i')}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
<input type="hidden" name="factory_id" id="factory_id" value="{{$factory_id}}">
<input type="hidden" id="start_sewing" class="btn btn-default" value="{{$today}}" readonly>
<table class="" width="100%">
    <tr class="bg-blue text_content">
        <td style="text-align:center;font-size:15px" width="15%">DETAIL</td>
        <td style="text-align:center;font-size:15px" width="8%">Cutting</td>
        <td style="text-align:center;font-size:15px" width="8.1%">PPA 1</td>
        <td style="text-align:center;font-size:15px" width="8.1%">PPA 2</td>
        <td style="text-align:center;font-size:15px" width="8%">Fuse</td>
        <td style="text-align:center;font-size:15px" width="8.1%">Artwork</td>
        <td style="text-align:center;font-size:15px" width="8%">Auto</td>
        <td style="text-align:center;font-size:15px" width="8%">Pad Print</td>
        <td style="text-align:center;font-size:15px" width="8%">HE</td>
        <td style="text-align:center;font-size:15px" width="8%">Setting</td>
        <td style="text-align:center;font-size:15px" width="8%">Supply</td>
    </tr>
</table>
<table width="100%">
    <tr>
        <div id="news-container">
        </div>
    </tr>
</table>
@endsection
@section('page-js')
<script type="text/javascript">
$(document).ready( function ()
{
    var factory_id = $('#factory_id').val();
    var start_sewing    = $('#start_sewing').val();

    // window.onload = function (){
    //     console.log(factory_id,start_sewing);
    // }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'get',
        url: '/adibooster/get-data-priority/'+factory_id,
        data: {
            start_sewing: start_sewing,
            factory_id: factory_id
        },
        success: function( response ) {
            console.log(response.count>10);
            $('#news-container' ).html(response.text);
            if(response.count>10){
                $(function() {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 8000,
                        animation: 'fade',
                        mousePause: false,
                        showItems: 8
                    });
                });
            }
            else{
                var show = response.count;
                $(function() {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 8000,
                        animation: 'fade',
                        showItems: show-1,
                    });
                });
            }
        }
    });
});
(function (a) {
    a.fn.vTicker = function (b) {
        var c = {
            speed: 700,
            pause: 8000,
            showItems: 3,
            animation: "",
            mousePause: true,
            isPaused: false,
            direction: "up",
            height: 0
        };
        var b = a.extend(c, b);
        moveUp = function (g, d, e) {
            if (e.isPaused) {
                return
            }
            var f = g.children("ul");
            var h = f.children("li:first").clone(true);
            if (e.height > 0) {
                d = f.children("li:first").height()
            }
            f.animate({
                top: "-=" + d + "px"
            }, e.speed, function () {
                a(this).children("li:first").remove();
                a(this).css("top", "0px")
            });
            if (e.animation == "fade") {
                f.children("li:first").fadeOut(e.speed);
                if (e.height == 0) {
                    f.children("li:eq(" + e.showItems + ")").hide().fadeIn(e.speed)
                }
            }
            h.appendTo(f)
        };
        moveDown = function (g, d, e) {
            if (e.isPaused) {
                return
            }
            var f = g.children("ul");
            var h = f.children("li:last").clone(true);
            if (e.height > 0) {
                d = f.children("li:first").height()
            }
            f.css("top", "-" + d + "px").prepend(h);
            f.animate({
                top: 0
            }, e.speed, function () {
                a(this).children("li:last").remove()
            });
            if (e.animation == "fade") {
                if (e.height == 0) {
                    f.children("li:eq(" + e.showItems + ")").fadeOut(e.speed)
                }
                f.children("li:first").hide().fadeIn(e.speed)
            }
        };
        return this.each(function () {
            var f = a(this);
            var e = 0;
            f.css({
                overflow: "hidden",
                position: "relative"
            }).children("ul").css({
                position: "absolute",
                margin: 0,
                padding: 0
            }).children("li").css({
                margin: 0,
                padding: 0
            });
            if (b.height == 0) {
                f.children("ul").children("li").each(function () {
                    if (a(this).height() > e) {
                        e = a(this).height()
                    }
                });
                f.children("ul").children("li").each(function () {
                    a(this).height(e)
                });
                f.height(e * b.showItems)
            } else {
                f.height(b.height)
            }
            var d = setInterval(function () {
                if (b.direction == "up") {
                    moveUp(f, e, b)
                } else {
                    moveDown(f, e, b)
                }
            }, b.pause);
            if (b.mousePause) {
                f.bind("mouseenter", function () {
                    b.isPaused = true
                }).bind("mouseleave", function () {
                    b.isPaused = false
                })
            }
        })
    }
})(jQuery);
</script>
@endsection