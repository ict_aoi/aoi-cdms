<?php

namespace App\Http\Controllers\PolaMarker;

use DB;
use Uuid;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Data\DataCuttingDev;
use App\Models\CuttingPlan;
use App\Models\DetailPlan;
use App\Models\Factory;
use App\Http\Controllers\Controller;

class RequestRasioController extends Controller
{
    public function indexDownload()
    {
        $factorys = Factory::where('deleted_at', null)
            ->where('status', 'internal')
            ->orderBy('factory_alias', 'asc')
            ->get();

        return view('pola_marker.request_rasio.index', compact('factorys'));
    }

    public function download(Request $request)
    {
        $this->validate($request, [
            'cutting_date' => 'required',
        ]);

        $cutting_date = $request->cutting_date;

        $factory = isset($request->factory) ? $request->factory : \Auth::user()->factory_id;

        if ($factory > 0) {
            $data = CuttingPlan::where('cutting_date', $cutting_date)
                ->where('is_header', true)
                ->where('factory_id', $factory)
                ->where('deleted_at', null)
                ->orderBy('queu', 'asc');

        } else {
            $data = CuttingPlan::where('cutting_date', $cutting_date)
                ->where('is_header', true)
                ->where('deleted_at', null)
                ->orderBy('factory_id', 'asc')
                ->orderBy('queu', 'asc');
        }

        if (count($data->get()) < 1) {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            if ($factory > 0) {
                $filename = 'AOI_' . $factory . '_Excel_SSP_' . $cutting_date;
                $sheet_name = 'AOI_' . $factory;
            } else {
                $filename = 'AOI_ALL_Excel_SSP_' . $cutting_date;
                $sheet_name = 'AOI_ALL';
            }

            $i = 1;

            $export = \Excel::create($filename, function ($excel) use ($data, $factory, $i, $sheet_name) {
                $excel->sheet($sheet_name, function ($sheet) use ($data, $factory, $i) {
                    $sheet->appendRow(array(
                        'order_tp', 'Customer', 'LC Date', 'Season', 'order_no', 'style_no', 'article_no', 'article_name', 'Cutting Date', 'Garment Type', 'Unit', 'order_qty', 'size_cnt',
                        'ts_01', 'qty_01',
                        'ts_02', 'qty_02',
                        'ts_03', 'qty_03',
                        'ts_04', 'qty_04',
                        'ts_05', 'qty_05',
                        'ts_06', 'qty_06',
                        'ts_07', 'qty_07',
                        'ts_08', 'qty_08',
                        'ts_09', 'qty_09',
                        'ts_10', 'qty_10',
                        'ts_11', 'qty_11',
                        'ts_12', 'qty_12',
                        'ts_13', 'qty_13',
                        'ts_14', 'qty_14',
                        'ts_15', 'qty_15',
                        'ts_16', 'qty_16',
                        'ts_17', 'qty_17',
                        'ts_18', 'qty_18',
                        'ts_19', 'qty_19',
                        'ts_20', 'qty_20',
                        'ts_21', 'qty_21',
                        'ts_22', 'qty_22',
                        'ts_23', 'qty_23',
                        'ts_24', 'qty_24',
                        'ts_25', 'qty_25',
                        'ts_26', 'qty_26',
                        'part_no', 'YY MO', 'usage_', 'matr_tp', 'matr_cd', 'matr_color', 'Combine', 'Width', 'Season2', 'FabQty', 'Max lay', 'Min lay', 'Max Length', 'End loss', 'piping', 'tgl info', 'jam info', 'actual width', 'actual qty', 'keterangan'
                    ));
                    $data->chunk(100, function ($rows) use ($sheet, $factory, $i) {
                        foreach ($rows as $row) {
                            $size_category_tambahan = '';
                            $remark_data = '';

                            if($row->is_combine) {
                                $header_id = CuttingPlan::where('header_id', $row->id)
                                    ->whereNull('deleted_at')
                                    ->pluck('id')
                                    ->toArray();

                                $data_detail = DataCuttingDev::where('plan_id', $row->id)->first();

                                if ($data_detail != null) {
                                    $query_data_detail_size = DB::table('jaz_detail_size_per_po_2')
                                        ->select(DB::raw("size_finish_good, sum(qty) as qty"))
                                        ->where('qty','>','0')
                                        ->whereIn('plan_id', $header_id)
                                        ->orderByRaw('LENGTH(size_finish_good) asc')
                                        ->orderBy('size_finish_good', 'asc')
                                        ->groupBy('size_finish_good');
    
                                    $count_plan = CuttingPlan::where('cutting_date', $row->cutting_date)
                                        ->where('style', $row->style)
                                        ->where('articleno', $row->articleno)
                                        ->where('size_category', $row->size_category)
                                        ->where('deleted_at', null)
                                        ->get();
    
                                    $return_size = '';
    
                                    if ($row->size_category == 'J') {
                                        $return_size = 'J';
                                    } elseif ($row->size_category == 'A') {
                                        $return_size = 'A';
                                    }
    
                                    if (count($count_plan) > 1) {
                                        $return_articleno = $row->articleno . $return_size . 'QQ' . $row->queu;
                                    } else {
                                        $return_articleno = $row->articleno . $return_size;
                                    }
    
                                    $total_qty = $query_data_detail_size->get()->sum('qty');
                                    $data_detail_size = $query_data_detail_size->get();
    
                                    $data_excel = array(
                                        'INLINE',
                                        $data_detail->customer,
                                        Carbon::parse($data_detail->lc_date)->format('j-M-y'),
                                        $data_detail->season,
                                        $data_detail->style . '-' . $data_detail->po_buyer . '-' . $data_detail->articleno,
                                        $row->style,
                                        $return_articleno,
                                        '',
                                        Carbon::parse($data_detail->cutting_date)->format('j-M-y'),
                                        '',
                                        'PCS',
                                        $total_qty,
                                        ''
                                    );
    
                                    foreach ($data_detail_size as $sz) {
                                        $data_excel[] = '="'.$sz->size_finish_good.'"';
                                        $data_excel[] = $sz->qty;
                                    }
    
                                    $sheet->appendRow($data_excel);
                                }
                            } else {
                                $data_detail = DataCuttingDev::where('plan_id', $row->id)->first();

                                if ($data_detail != null) {
                                    $query_data_detail_size = DB::table('jaz_detail_size_per_po_2')
                                        ->select(DB::raw("size_finish_good, sum(qty) as qty"))
                                        ->where('qty','>','0')
                                        ->where('plan_id', $row->id)
                                        ->orderByRaw('LENGTH(size_finish_good) asc')
                                        ->orderBy('size_finish_good', 'asc')
                                        ->groupBy('size_finish_good');
    
                                    $count_plan = CuttingPlan::where('cutting_date', $row->cutting_date)
                                        ->where('style', $row->style)
                                        ->where('articleno', $row->articleno)
                                        ->where('size_category', $row->size_category)
                                        ->where('deleted_at', null)
                                        ->get();
    
                                    $return_size = '';
    
                                    if ($row->size_category == 'J') {
                                        $return_size = 'J';
                                    } elseif ($row->size_category == 'A') {
                                        $return_size = 'A';
                                    }
    
                                    if (count($count_plan) > 1) {
                                        $return_articleno = $row->articleno . $return_size . 'QQ' . $row->queu;
                                    } else {
                                        $return_articleno = $row->articleno . $return_size;
                                    }
    
                                    $total_qty = $query_data_detail_size->get()->sum('qty');
                                    $data_detail_size = $query_data_detail_size->get();
    
                                    $data_excel = array(
                                        'INLINE',
                                        $data_detail->customer,
                                        Carbon::parse($data_detail->lc_date)->format('j-M-y'),
                                        $data_detail->season,
                                        $data_detail->style . '-' . $data_detail->po_buyer . '-' . $data_detail->articleno,
                                        $row->style,
                                        $return_articleno,
                                        '',
                                        Carbon::parse($data_detail->cutting_date)->format('j-M-y'),
                                        '',
                                        'PCS',
                                        $total_qty,
                                        ''
                                    );
    
                                    foreach ($data_detail_size as $sz) {
                                        $data_excel[] = '="'.$sz->size_finish_good.'"';
                                        $data_excel[] = $sz->qty;
                                    }
                                    $sheet->appendRow($data_excel);
                                }
                            }
                        }
                    });
                });
            })->download('xlsx');

            return response()->json(200);
        }
    }
}
