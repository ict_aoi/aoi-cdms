<?php

namespace App\Http\Controllers\Spreading;

use DB;
use StdClass;
use Carbon\Carbon;
use App\Models\RasioMarker;
use App\Models\CuttingMarker;
use App\Models\MarkerDetailPO;
use App\Models\LogSystem;
use App\Models\CuttingMovement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LaporanBaruController extends Controller
{
    public function index()
    {
        return view('request_marker.laporan_baru.index');
    }

    public function dataMarker(Request $request)
    {
        $cutting_date = $request->cutting_date;

        if(request()->ajax()) 
        {
            if($cutting_date != null) {

                if(\Auth::user()->factory_id > 0) {
                    $data = DB::table('cutting_marker')->join('cutting_plans2', 'cutting_plans2.id', '=', 'cutting_marker.id_plan')->select('cutting_marker.*', 'cutting_plans2.cutting_date', 'cutting_plans2.factory_id', 'cutting_plans2.top_bot', 'cutting_plans2.style', 'cutting_plans2.articleno', 'cutting_plans2.size_category')->where('cutting_plans2.factory_id', \Auth::user()->factory_id)->where(DB::raw("cutting_marker.created_at::date"), $cutting_date)->whereNotNull('is_new')->get();
                } else {
                    $data = DB::table('cutting_marker')->join('cutting_plans2', 'cutting_plans2.id', '=', 'cutting_marker.id_plan')->select('cutting_marker.*', 'cutting_plans2.cutting_date', 'cutting_plans2.factory_id', 'cutting_plans2.top_bot', 'cutting_plans2.style', 'cutting_plans2.articleno', 'cutting_plans2.size_category')->where(DB::raw("cutting_marker.created_at::date"), $cutting_date)->whereNotNull('is_new')->get();
                }


                return datatables()->of($data)
                ->addColumn('plan', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-M');
                })
                ->addColumn('style', function($data) {
                    return $data->top_bot != null ? $data->style.'-'.$data->top_bot : $data->style;
                })
                ->addColumn('articleno', function($data) {
                    return $data->articleno;
                })
                ->addColumn('size_category', function($data) {
                    return $data->size_category == 'A' ? 'Asian' : $data->size_category == 'J' ? 'Japan' : 'Inter';
                })
                ->editColumn('download_at', function($data) {
                    if($data->download_at == null) {
                        return '<span class="label label-default label-icon"><i class="icon-cross3"></i></span>';
                    } else {
                        if($data->deleted_at == null) {
                            return '<span class="label label-success label-icon"><i class="icon-checkmark2"></i></span>';
                        } else {
                            return '<span class="label label-default label-icon"><i class="icon-cross3"></i></span>';
                        }
                    }
                })
                ->addColumn('selector', function($data) {
                    if($data->deleted_at == null) {
                        if($data->marker_length != null && $data->perimeter != null) {
                            return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'">';
                        } else {
                            return '<input type="checkbox" class="styled" name="selector[]" id="Inputselector" data-id="'.$data->barcode_id.'" disabled="disabled">';
                        }
                    } else {
                        return 'Deleted';
                    }
                })
                ->addColumn('detail', function($data) {
                    return view('request_marker.laporan_baru._action', [
                        'model'      => $data,
                        'detail'     => route('laporanBaru.detailLaporanBaru',$data->barcode_id)
                    ]);
                })
                ->rawColumns(['download_at','selector','detail'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function printId(Request $request)
    {
        if(request()->ajax()) 
        {
            $data = $request->data;
            $print_type = $request->print_type;
            $cutting_date = $request->cutting_date;

            $new_data = array();
            $data_result = array();

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    if($data[$i][10] == 'true') {
                        $new_data[] = $data[$i][2];
                    }
                }
            }

            $last_po_buyer = '';

            // foreach marker
            foreach($new_data as $a) {

                // get data detail marker
                $get_data_marker = CuttingMarker::where('barcode_id', $a)->where('deleted_at', null)->where('download_at', null)->whereNotNull('is_new')->first();
                
                // jika marker ada
                if($get_data_marker != null) {
                    
                    if($get_data_marker->is_new == 'editreport') {
    
                        try {
                            DB::begintransaction();
    
                            $queue_list = array();
    
                            if($get_data_marker->combine_id == null) {
                                $queue_list[] = $get_data_marker->cutting_plan->queu;
                            } else {
                                $plan_id_list = CombinePart::where('combine_id', $get_data_marker->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                                foreach($plan_id_list as $planid) {
                                    $get_data_plan = CuttingPlan::where('id', $planid)->where('deleted_at', null)->first();
                                    $queue_list[] = $get_data_plan->queu;
                                }
                            }
    
                            // get informasi part in marker
                            $part_no = explode('+', $get_data_marker->part_no);
    
                            // set value sisa menjadi 0
                            $sisa = 0;
    
                            // foreach ratio size per marker
                            foreach($get_data_marker->rasio_markers as $b) {
    
                                $qty_marker_size = $get_data_marker->layer * $b->ratio;
                                
                                // get qty per po per part no per size -> planning
                                $get_list_po = DB::table('jaz_detail_size_per_part')->select(DB::raw("po_buyer, part_no, sum(qty) as qty, deliv_date"))->where('cutting_date', $get_data_marker->cutting_plan->cutting_date)->whereIn('queu', $queue_list)->where('part_no', $part_no[0])->where('factory_id', $get_data_marker->cutting_plan->factory_id)->orderBy('deliv_date', 'asc')->orderBy('qty', 'asc')->groupBy('po_buyer','deliv_date','part_no')->get();
    
                                foreach($get_list_po as $c) {
                                    if($qty_marker_size > 0) {
                                        $get_po_plot = MarkerDetailPO::where('plan_id', $get_data_marker->cutting_plan->id)->where('size', $b->size)->where('part_no', $c->part_no)->where('po_buyer', $c->po_buyer)->where('deleted_at', null)->sum('qty');
            
                                        $get_po_plan = DB::table('jaz_detail_size_per_part')->select(DB::raw("po_buyer, part_no, size_finish_good as size, sum(qty) as qty, deliv_date"))->where('cutting_date', $get_data_marker->cutting_plan->cutting_date)->whereIn('queu', $queue_list)->where('part_no', $part_no[0])->where('po_buyer', $c->po_buyer)->where('factory_id', $get_data_marker->cutting_plan->factory_id)->where('size_finish_good', $b->size)->groupBy('po_buyer','deliv_date', 'size_finish_good', 'part_no')->first();
            
                                        if($get_po_plan != null) {
                                            if($get_po_plot < $get_po_plan->qty) {
                                                if(($get_po_plan->qty - $get_po_plot) < $qty_marker_size) {
                                                    MarkerDetailPO::FirstOrCreate([
                                                        'barcode_id' => $a,
                                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                                        'ratio_id' => $b->id,
                                                        'po_buyer' => $c->po_buyer,
                                                        'size' => $b->size,
                                                        'qty' => $get_po_plan->qty - $get_po_plot,
                                                        'is_sisa' => 'f',
                                                        'part_no' => $c->part_no,
                                                    ]);
            
                                                    $qty_marker_size = $qty_marker_size - ($get_po_plan->qty - $get_po_plot);
    
                                                    $last_ratio_id = $b->id;
                                                    $last_po_buyer = $c->po_buyer;
                                                    $last_size = $b->size;
                                                    $last_part_no = $c->part_no;
                                                } else {
                                                    MarkerDetailPO::FirstOrCreate([
                                                        'barcode_id' => $a,
                                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                                        'ratio_id' => $b->id,
                                                        'po_buyer' => $c->po_buyer,
                                                        'size' => $b->size,
                                                        'qty' => $qty_marker_size,
                                                        'is_sisa' => 'f',
                                                        'part_no' => $c->part_no,
                                                    ]);
            
                                                    $qty_marker_size = 0;
                                                }
                                            } else {
                                                $last_po_buyer = $c->po_buyer;
                                            }
                                        }
                                    } else {
                                        break;
                                    }
                                }
    
                                if($qty_marker_size > 0) {
                                    MarkerDetailPO::FirstOrCreate([
                                        'barcode_id' => $a,
                                        'plan_id' => $get_data_marker->cutting_plan->id,
                                        'ratio_id' => $b->id,
                                        'po_buyer' => $last_po_buyer,
                                        'size' => $b->size,
                                        'qty' => $qty_marker_size,
                                        'is_sisa' => 't',
                                        'part_no' => $part_no[0],
                                    ]);
                                }
                            }
                            
                            DB::commit();
                        }
                        catch (Exception $e) {
                            DB::rollback();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }

            if(count($new_data) > 0) {
                return response()->json(['data' => $new_data, 'status' => 'isset', 'cutting_date' => $cutting_date, 'print_type' => $print_type]);
            } else {
                return response()->json(['data' => $new_data, 'status' => 'null', 'cutting_date' => $cutting_date, 'print_type' => $print_type]);
            }
        }
    }

    public function printIdPdf(Request $request)
    {
        $print_type = $request->print_type_;
        $data = json_decode($request->list_id_marker);
        
        $barcode_ids = array();

        $data_marker_query = CuttingMarker::whereIn('barcode_id', $data)->orderBy('part_no', 'asc');
        
        $data_marker_all = $data_marker_query->orderBy('cut', 'asc')->get();
        
        $data_marker['data_marker_all'] = $data_marker_all;

        if($print_type == 'double') {
            foreach($data_marker_all as $data_marker) {
                $barcode_ids[] = $data_marker->barcode_id;
                $barcode_ids[] = $data_marker->barcode_id;
            }
        }

        foreach($data_marker_all as $marker) {
            CuttingMovement::firstOrCreate([
                'barcode_id' => $marker->barcode_id,
                'process_from' => 'preparation',
                'status_from' => 'onprogress',
                'process_to' => 'preparation',
                'status_to' => 'completed',
                'is_canceled' => false,
                'user_id' => \Auth::user()->id,
                'description' => 'printing',
                'ip_address' => $this->getIPClient(),
                'barcode_type' => 'spreading',
                'deleted_at' => null
            ]);
        }

        $data_marker['barcode_ids'] = $barcode_ids;
        $data_marker['print_type'] = $print_type;

        $data_marker_query->update(['download_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        
        // return view('print.identitas_marker', $data_marker);

        $pdf = \PDF::loadView('print.identitas_marker', $data_marker)->setPaper('a4', 'landscape');
        return $pdf->stream('identitas_marker_'.$data[0].'-'.end($data).'.pdf');
    }

    public function detailLaporanBaru($id)
    {
        $data = CuttingMarker::where('barcode_id', $id)->where('deleted_at', null)->first();

        $ratio = 0;
        foreach($data->rasio_markers as $a) {
            $ratio = $ratio + $a->ratio;
        }
        $total_garment = $ratio * $data->layer;

        $part_no = explode('+', preg_replace('/\s+/', '', $data->part_no));

        $po_buyer = DB::table('ratio_detail')->select('po_buyer')->where('plan_id', $data->id_plan)->where('barcode_id', $data->barcode_id)->groupBy('po_buyer')->pluck('po_buyer')->toArray();

        $po_buyer = implode(', ', $po_buyer);

        $get_data_cutting = DB::table('data_cuttings')->where('plan_id', $data->cutting_plan->id)->whereIn('part_no', $part_no)->orderBy('statistical_date', 'asc')->first();

        $ratio_size = '';
        foreach($data->rasio_markers as $aa) {
            $ratio_size .= $aa->size.'/'.$aa->ratio.', ';
        }

        $barcode_check = array();
        array_push($barcode_check, $id);

        $notes = CuttingMovement::where('description', 'LIKE', '%'.$id.'%')->where('process_to', 'spreading')->where('status_to', 'completed')->whereNull('deleted_at')->whereIn('barcode_id', $barcode_check)->orderBy('created_at', 'desc')->first();

        if($notes == null) {
            $notes = null;
        } else {
            $get_barcode_lama = str_replace(' pecah marker '.$id, '', $notes->description);
            $data_lama = CuttingMarker::where('barcode_id', $get_barcode_lama)->where('deleted_at', null)->first();

            if($data_lama == null) {
                $notes = null;
            } else {
                $notes = '(PECAH DARI MARKER '.$get_barcode_lama.' CUT '.$data_lama->cut.')';
            }
        }

        $obj = new StdClass();
        $obj->barcode_id = $data->barcode_id;
        $obj->cutting_date = Carbon::parse($data->cutting_plan->cutting_date)->format('d-m-Y');
        $obj->style = $data->cutting_plan->style;
        $obj->articleno = $data->cutting_plan->articleno;
        $obj->cut = $data->cut;
        $obj->marker_length = $data->marker_length;
        $obj->total_length = $data->marker_length + 0.75;
        $obj->marker_width = $data->fabric_width - 0.5;
        $obj->layer = $data->layer;
        $obj->deliv = Carbon::parse($get_data_cutting->statistical_date)->format('d-m-Y');
        $obj->item_code = $get_data_cutting->material;
        $obj->color_code = substr($get_data_cutting->color_code_raw_material, 0, 20);
        $obj->color_name = substr($data->cutting_plan->color, 0, 15);
        $obj->marker_pcs = $ratio;
        $obj->part_no = $data->part_no;
        $obj->total_garment = $total_garment;
        $obj->po_buyer = $po_buyer;
        $obj->ratio_size = $ratio_size;
        $obj->notes = $notes;
		
		return response()->json($obj,200);
    }

    public function changeRatio(Request $request)
    {
        if(request()->ajax()) 
        {
            $cutting_date = $request->cutting_date;
            $data = $request->data;
            $new_data = array();

            if(count($data) > 0){
                for($i = 0; $i < count($data); $i++) {
                    if($data[$i][10] != 'deleted') {
                        if($data[$i][10] == 'true') {
                            $new_data[] = $data[$i][2];
                        }
                    }
                }
            }

            try {
                DB::begintransaction();

                CuttingMarker::whereIn('barcode_id', $new_data)->update([
                    'deleted_at' => Carbon::now()
                ]);

                RasioMarker::whereIn('id_marker', $new_data)->update([
                    'deleted_at' => Carbon::now()
                ]);
                
                MarkerDetailPO::whereIn('barcode_id', $new_data)->update([
                    'deleted_at' => Carbon::now()
                ]);

                CuttingMovement::whereIn('barcode_id', $new_data)->whereNotIn('process_to', ['preparation'])->whereNotIn('status_to', ['onprogress'])->update([
                    'deleted_at' => Carbon::now()
                ]);

                DB::commit();
            }
            catch (Exception $e) {
                DB::rollback();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $response = [
                'cutting_date' => $cutting_date,
                'data' => $new_data
            ];

            return response()->json($response,200);
        }
    }

    private function getIPClient() {
        $request = $request ?? request();
        $xForwardedFor = $request->header('x-forwarded-for');
        if (empty($xForwardedFor)) {
            $ip = $request->ip();
        } else {
            $ips = is_array($xForwardedFor) ? $xForwardedFor : explode(', ', $xForwardedFor);
            $ip = $ips[0];
        }

        return $ip;
    }
}
