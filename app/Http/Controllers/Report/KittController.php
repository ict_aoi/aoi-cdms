<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Auth;
use Excel;

class KittController extends Controller
{
    // PPA 2 & PPA 1
    public function ppa2()
    {
        $master_seasons = DB::table('master_seasons')
                            ->whereNull('deleted_at')
                            ->orderBy('season_name')
                            ->get();

        return view('report.kitt.ppa2', compact('master_seasons'));
    }

    public function exportPpa2(Request $request)
    {
        if ($request->tab_active == 'ppa2') {
            $data_non_ppa2 = DB::table('v_rnd_detail_process')
                                ->select(
                                        DB::raw('
                                                v_rnd_detail_process.style,
                                                v_rnd_detail_process.season,
                                                0::text AS detail_process_name,
                                                0 AS process_id,
                                                0::text AS gsd_name,
                                                v_rnd_detail_process.type_id,
                                                v_rnd_detail_process.type_name,
                                                0 AS id_column_report,
                                                0::varchar AS column_name,
                                                0 AS id_detail_process,
                                                0 AS detail_process_id,
                                                0 AS id_column_report2,
                                                0::varchar AS column_name2,
                                                0 AS total,
                                                0::varchar AS remark_rnd,
                                                0 AS gasa,
                                                0 AS lc,
                                                0 AS ts,
                                                0 AS komponen,
                                                0::varchar AS komponen_name,
                                                v_rnd_detail_process.season_name,
                                                0::varchar AS product_type,
                                                master_list_style.product_category'
                                        )
                                )
                                ->join('master_list_style', function($join){
                                    $join->on('master_list_style.style_standart', '=', 'v_rnd_detail_process.style');
                                    $join->on('master_list_style.season', '=', 'v_rnd_detail_process.season');
                                    $join->on('master_list_style.set_type', '=', 'v_rnd_detail_process.type_name');
                                })
                                ->where('v_rnd_detail_process.process_id', '!=', \Config::get('constants.PPA2_TYPE'))
                                ->whereNotIn('v_rnd_detail_process.style', function($query) {
                                    $query->select('v_rnd_detail_process.style')
                                          ->from('v_rnd_detail_process')
                                          ->join('master_list_style', function($join){
                                                $join->on('master_list_style.style_standart', '=', 'v_rnd_detail_process.style');
                                                $join->on('master_list_style.season', '=', 'v_rnd_detail_process.season');
                                                $join->on('master_list_style.set_type', '=', 'v_rnd_detail_process.type_name');
                                            })
                                            ->where('v_rnd_detail_process.process_id', \Config::get('constants.PPA2_TYPE'));
                                });

                if($request->radio_status=='season'){
                    $data_non_ppa2 = $data_non_ppa2->where('v_rnd_detail_process.season', $request->season);
                }
                                
                $data_non_ppa2 = $data_non_ppa2->groupBy([
                                                    'v_rnd_detail_process.style', 
                                                    'v_rnd_detail_process.season',
                                                    'v_rnd_detail_process.type_id', 
                                                    'v_rnd_detail_process.type_name', 
                                                    'v_rnd_detail_process.season_name', 
                                                    'master_list_style.product_category'
                                                ]);

            $data = DB::table('v_rnd_detail_process')
                    ->select(
                            'v_rnd_detail_process.style',
                            'v_rnd_detail_process.season',
                            'v_rnd_detail_process.detail_process_name',
                            'v_rnd_detail_process.process_id',
                            'v_rnd_detail_process.gsd_name',
                            'v_rnd_detail_process.type_id',
                            'v_rnd_detail_process.type_name',
                            'v_rnd_detail_process.id_column_report',
                            'v_rnd_detail_process.column_name',
                            'v_rnd_detail_process.id_detail_process',
                            'v_rnd_detail_process.detail_process_id',
                            'v_rnd_detail_process.id_column_report2',
                            'v_rnd_detail_process.column_name2',
                            'v_rnd_detail_process.total',
                            'v_rnd_detail_process.remark_rnd',
                            'v_rnd_detail_process.gasa',
                            'v_rnd_detail_process.lc',
                            'v_rnd_detail_process.ts',
                            'v_rnd_detail_process.komponen',
                            'v_rnd_detail_process.komponen_name',
                            'v_rnd_detail_process.season_name',
                            'master_list_style.product_type',
                            'master_list_style.product_category'
                    )
                    ->join('master_list_style', function($join){
                        $join->on('master_list_style.style_standart', '=', 'v_rnd_detail_process.style');
                        $join->on('master_list_style.season', '=', 'v_rnd_detail_process.season');
                        $join->on('master_list_style.set_type', '=', 'v_rnd_detail_process.type_name');
                    })
                    ->where('v_rnd_detail_process.process_id', \Config::get('constants.PPA2_TYPE'))
                    ->groupBy([
                        'v_rnd_detail_process.style',
                            'v_rnd_detail_process.season',
                            'v_rnd_detail_process.detail_process_name',
                            'v_rnd_detail_process.process_id',
                            'v_rnd_detail_process.gsd_name',
                            'v_rnd_detail_process.type_id',
                            'v_rnd_detail_process.type_name',
                            'v_rnd_detail_process.id_column_report',
                            'v_rnd_detail_process.column_name',
                            'v_rnd_detail_process.id_detail_process',
                            'v_rnd_detail_process.detail_process_id',
                            'v_rnd_detail_process.id_column_report2',
                            'v_rnd_detail_process.column_name2',
                            'v_rnd_detail_process.total',
                            'v_rnd_detail_process.remark_rnd',
                            'v_rnd_detail_process.gasa',
                            'v_rnd_detail_process.lc',
                            'v_rnd_detail_process.ts',
                            'v_rnd_detail_process.komponen',
                            'v_rnd_detail_process.komponen_name',
                            'v_rnd_detail_process.season_name',
                            'master_list_style.product_type',
                            'master_list_style.product_category'
                    ])
                    ->union($data_non_ppa2);

            if($request->radio_status=='season'){
                $data=$data->where('v_rnd_detail_process.season', $request->season);
            }

            $data = $data->orderBy('komponen_name');

            $filename = 'repor_kitt_ppa2_'.Carbon::now()->format('d_m_Y');
            $i=1;
            return Excel::create($filename, function($excel) use ($data, $i) {
                    $excel->sheet('report', function($sheet) use($data, $i) {
                        $sheet->appendRow(array(
                            'Season', 'Product Category', 'Style', 'Category #', 'Category', 'Panel', 'Process', 'GSD', 'TS GSD', 'Note'
                        ));

                        foreach ($data->get() as $row)
                        {
                            $style = $row->style;
                            $cat1 = 'PPA 2';
                            $cat2 = 'PPA';
                            $komponen_name = $row->komponen_name;
                            $detail_process_name = $row->detail_process_name;
                            $gsd_name = $row->gsd_name;
                            $lc = $row->lc;

                            if ($row->type_id > 1) {
                                $style = $row->style.' '.$row->type_name;
                            }
                            if ($row->process_id == '0') {
                                $cat1 = 'NO PPA 2';
                                $cat2 = 'NO';
                                $komponen_name = '';
                                $detail_process_name = '';
                                $gsd_name = '';
                                $lc = '';
                            }
                            $sheet->appendRow(array(
                            $row->season_name, $row->product_category, $style, $cat1, $cat2, $komponen_name, $detail_process_name, $gsd_name, $lc, '' 
                            ));
                        }
                    });
            })->download('xlsx');

        }elseif ($request->tab_active == 'ppa1') {
            $ppa1_type = \Config::get('constants.KITT_PPA1_TYPE');

            $data = DB::table('v_rnd_detail_process')
                    ->select(
                            'v_rnd_detail_process.season_name',
                            'v_rnd_detail_process.style',
                            'v_rnd_detail_process.process_name',
                            'v_rnd_detail_process.detail_process_name',
                            'v_rnd_detail_process.gsd_name',
                            'v_rnd_detail_process.lc',        
                            'master_list_style.product_category'
                    )
                    ->join('master_list_style', function($join){
                        $join->on('master_list_style.style_standart', '=', 'v_rnd_detail_process.style');
                        $join->on('master_list_style.season', '=', 'v_rnd_detail_process.season');
                        $join->on('master_list_style.set_type', '=', 'v_rnd_detail_process.type_name');
                    })
                    ->whereIn('v_rnd_detail_process.process_id', $ppa1_type)
                    ->groupBy(['v_rnd_detail_process.season_name',
                    'v_rnd_detail_process.style',
                    'v_rnd_detail_process.process_name',
                    'v_rnd_detail_process.detail_process_name',
                    'v_rnd_detail_process.gsd_name', 
                    'v_rnd_detail_process.lc',        
                    'master_list_style.product_category']);

            //$data = $data->orderBy('v_rnd_detail_process.komponen_name');

            $filename = 'repor_kitt_ppa_'.Carbon::now()->format('d_m_Y');
            $i=1;
            return Excel::create($filename, function($excel) use ($data, $i) {
                    $excel->sheet('report', function($sheet) use($data, $i) {
                        $sheet->appendRow(array(
                            'Season', 'Product Category', 'Style', 'Category', 'Process', 'GSD', 'TS GSD', 'Note'
                        ));

                        foreach ($data->get() as $row)
                        {
                            $sheet->appendRow(array(
                            $row->season_name, $row->product_category, $row->style, $row->process_name, $row->detail_process_name, $row->gsd_name, $row->lc, '' 
                            ));
                        }
                    });
            })->download('xlsx');
        }
        
    }
}
