<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RatioDetailTemp extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'ratio_detail_temp';
    protected $fillable = ['barcode_id','plan_id','ratio_id','po_buyer','size','qty','deleted_at','is_sisa','part_no','user_id'];
    
    public function rasio_marker()
    {
        return $this->belongsTo('App\Models\RasioMarker', 'ratio_id', 'id');
    }
}
