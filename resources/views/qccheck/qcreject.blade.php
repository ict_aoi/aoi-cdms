@extends('layouts.app',['active' => 'qc_reject'])

@section('page-css')
    <style>
        th, td { white-space: nowrap; }
        div.dataTables_wrapper {
            width: 100%;
            margin: 0 auto;
        }
    </style>
@endsection


@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ganti Reject</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Ganti Reject</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
    <div class="panel-heading">
        <h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        @include('form.select', [
            'field'     => 'select_filter_reject',
            'label'     => 'Status',
            'mandatory' => '*Required',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
            'options'   => [
                'belum'   => 'Outstanding (Belum Diganti Reject)',
                'sudah'   => 'Sudah Diganti Reject, Belum Approve',
                'approve' => 'Sudah Approve',
            ],
            'class'      => 'select-search',
            'attributes' => [
                'id' => 'select_filter_reject'
            ]
        ])

        @include('form.select', [
            'field'     => 'select_filter_meja',
            'label'     => 'Meja',
            'mandatory' => '*Required',
            'label_col' => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
            'options' => [
                '' => '-- All Meja --',
            ]+$qc_table,
            'class'      => 'select-search',
            'attributes' => [
                'id' => 'select_filter_meja'
            ]
        ])

        <button type="button" class="btn btn-info col-md-12" id="btn-filter">Filter</button>
    </div>
</div>

<div class="panel panel-default border-grey">
    <div class="panel-heading">
        <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
        <div class="heading-elements">
            <div class="heading-btn">
                {!! Form::hidden('list_ganti_reject','', array('id' => 'list_ganti_reject')) !!}
                <button type="button" id="btn-ganti-reject" class="btn btn-success"><span class="icon-checkmark"></span> Ganti Reject</button>
                {{-- <button type="button" id="btn_check_all" class="btn btn-yellow-cancel">Check All <i class="icon-checkbox-checked position-left"></i></button>
                <button type="button" id="btn_uncheck_all" class="btn btn-default">Uncheck All <i class=" icon-checkbox-unchecked position-left"></i></button> --}}
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
        	<table class="table datatable-basic" id="table-list" style="width:100%">
        		<thead>
                    <tr>
                        <th>Barcode Bundle</th>
                        <th>Cut Num</th>
                        <th>Meja Bundle</th>
                        <th>Total</th>
                        <th>Style</th>
                        <th>Article</th>
                        <th>PO Buyer</th>
                        <th>Size</th>
                        <th>Part - Komponen</th>
                        <th>Tanggal Defect</th>
                        <th>QC Bundle</th>
                        <th>Tanggal Ganti Reject</th>
                        <th>QC Reject</th>
                        <th>Tanggal Confirm Defect</th>
                        <th>QC Bundle</th>
                        <th>Action</th>
                    </tr>
        		</thead>
        	</table>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/fixed_columns.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        dom: 'Bfrtip',
        scrollX: true,
        // scrollY: "300px",
        // scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 1,
            rightColumns: 1,
        },
        paging: false,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('Qccheck.getQcReject') }}",
            data: function(d) {
                 return $.extend({}, d, {
                     "filter" : $('#select_filter_reject').val(),
                     "filter_meja" : $('#select_filter_meja').val(),
                 });
            }
        },
        columns: [
            {data: 'barcode_id', name: 'barcode_id'},
            {data: 'cut_num', name: 'cut_num'},
            {data: 'name', name: 'name'},
            {data: 'defect', name: 'defect'},
            {data: 'style', name: 'style'},
            {data: 'article', name: 'article'},
            {data: 'poreference', name: 'poreference'},
            {data: 'size', name: 'size'},
            {data: 'component_name', name: 'component_name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'user_qc', name: 'user_qc'},
            {data: 'repaired_at', name: 'repaired_at'},
            {data: 'user_reject', name: 'user_reject'},
            {data: 'confirmed_at', name: 'confirmed_at'},
            {data: 'user_approve', name: 'user_approve'},
            {data: 'action', name: 'action', searchable:false, sortable:false, orderable:false}
        ]
    });

    var dtable = $('#table-list').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();


    $('#btn-filter').on('click',function(){
        dtable.draw();
    });

    $('#btn-ganti-reject').on('click',function(){

        selectedApproval();
        var data = $('#list_ganti_reject').val();

        if(data.length<=2){
            $("#alert_warning").trigger("click", 'Pilih Numbering Terlebih Dahulu!');
            return false;
        }

        bootbox.confirm("Apakah anda yakin akan mengirim ganti reject atas bundle ini?", function (result) {
            if(result){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url : "{{ route('Qccheck.gantiReject') }}",
                    data:{id:data},
                    beforeSend:function(){
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        var data_response = response.data;
                        if (data_response.status == 200) {
                            $("#alert_success").trigger("click", data_response.output);
                            var dtable = $('#table-list').dataTable().api();
                            dtable.draw();
                        }else{
                            $("#alert_warning").trigger("click",data_response.output);
                        }
                    },
                    error: function (response) {
                        $.unblockUI();
                        $("#alert_warning").trigger("click",'Save failed, table required');
                    }
                });

            }
        });
    });
});

function selectedApproval()
{
	var list_ganti_reject = new Array();
	$("input[type=checkbox]:checked").each(function (){
		list_ganti_reject.push(this.id);
	});
	$('#list_ganti_reject').val(JSON.stringify(list_ganti_reject));
}




</script>
@endsection
