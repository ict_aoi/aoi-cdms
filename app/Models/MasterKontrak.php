<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterKontrak extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_kk';
	protected $fillable = ['no_kk','subcont_id','factory_id','user_id','total','created_at','updated_at','deleted_at'];
}
