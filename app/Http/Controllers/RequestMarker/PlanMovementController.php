<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use StdClass;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Factory;
use App\Models\CuttingPlan;
use App\Models\CuttingMarker;
use App\Models\DetailPlan;
use Carbon\Carbon;

class PlanMovementController extends Controller
{
    public function index()
    {
        $factorys = Factory::where('deleted_at', null)->where('status', 'internal')->orderBy('factory_alias', 'asc')->get();
        return view('request_marker.plan_movement.index');
    }

    public function dataPlanning(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id != 0) {
                    $data = CuttingPlan::where('cutting_date', $cutting_date)->where('factory_id', \Auth::user()->factory_id)->where('deleted_at', null)->orderBy('queu', 'asc');
                } else {
                    $data = CuttingPlan::where('cutting_date', $cutting_date)->where('deleted_at', null)->orderBy('queu', 'asc');
                }

                return datatables()->of($data)
                ->editColumn('articleno', function($data) {
                    if ($data->remark == 'partial') return $data->articleno.' ('.$data->queu.')';
                    else return $data->articleno;
                })
                ->addColumn('po', function($data) {
                    return implode(', ', $data->po_details->pluck('po_buyer')->toArray());
                })
                ->addColumn('status', function($data) {
                    $po_array     = $data->po_details->pluck('po_buyer')->toArray();
                    $check_plan   = DB::table('data_cuttings')->where('cutting_date', $data->cutting_date)->where('style', 'like', '%'.$data->style.'%')->where('articleno', $data->articleno)->whereIn('po_buyer', $po_array)->where('queu', $data->queu)->get();
                    if (count($check_plan) < 1) {
                        $cutting_date = DB::table('data_cuttings')->where('style', 'like', '%'.$data->style.'%')->where('articleno', $data->articleno)->whereIn('po_buyer', $po_array)->distinct('cutting_date')->pluck('cutting_date')->toArray();
                        return '<span class="badge bg-danger">Geser Plan</span><p> ke tanggal '. implode(', ', $cutting_date).'</p>';
                    } else {
                        return null;
                    }
                })
                ->editColumn('size_category', function($data) {
                    if ($data->size_category == 'J') return 'Japan';
                    elseif ($data->size_category == 'A') return 'Asia';
                    else return 'Inter';
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.plan_movement._action', [
                        'model'      => $data,
                        'detail'     => route('planMovement.dataDetail',$data->id)
                    ]);
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns(['action'])
                ->make(true);
            }
        }
    }

    public function dataDetail($id)
    {
        $data = CuttingPlan::where('id', $id)->where('deleted_at', null)->get()->first();
        $obj = new StdClass();
        $obj->id = $id;
        $obj->cutting_date = $data->cutting_date;
        $obj->style = $data->style;
        $obj->articleno = $data->articleno;
        $obj->size_category = $data->size_category == 'J' ? 'Japan' : $data->size_category == 'A' ? 'Asian' : 'International';
        $po_view = implode(', ', $data->po_details->pluck('po_buyer')->toArray());
        $obj->po_view = $po_view;

		return response()->json($obj,200);
    }

    public function movePlan(Request $request)
    {
        if(request()->ajax())
        {
            $movement_date = $request->movement_date;
            $movement_queu = $request->movement_queu;
            $id_plan = $request->id_plan;
            $respons = [];

            if($movement_date != NULL && $id_plan != NULL) {
                $get_plan = CuttingPlan::where('id', $id_plan)->where('deleted_at', null)->first();
                if($get_plan != null) {
                    $get_po = $get_plan->po_details->pluck('po_buyer')->toArray();
                    $count_check = 0;
                    $note_check = '';
                    foreach($get_po as $po) {
                        $check_data = DB::table('data_cuttings')->where('cutting_date', $movement_date)->where('style','like','%'.$get_plan->style.'%')->where('articleno', $get_plan->articleno)->where('size_category', $get_plan->size_category)->where('warehouse', $get_plan->factory_id)->where('po_buyer', $po)->get();

                        if(count($check_data) < 1) {
                            $count_check++;
                            $note_check .= $po.'/';
                        }
                    }

                    if($count_check > 0) {
                        $respons['status'] = 'failed';
                        $respons['message'] = 'Style '.$get_plan->style.' Article '.$get_plan->articleno.' PO '.$note_check.' ditemukan untuk plan '.$movement_date;
                    } else {
                        $check_queue = CuttingPlan::where('cutting_date', $movement_date)->where('deleted_at', null)->where('queu', $movement_queu)->where('factory_id', $get_plan->factory_id)->get();

                        if(count($check_queue) > 0) {
                            $respons['status'] = 'failed';
                            $respons['message'] = 'Queue '.$movement_queu.' sudah ada pada plan '.$movement_date.'!';
                        } else {

                            DB::table('data_cuttings')->where('cutting_date', $movement_date)->where('style','like','%'.$get_plan->style.'%')->where('articleno', $get_plan->articleno)->where('size_category', $get_plan->size_category)->where('warehouse', $get_plan->factory_id)->whereIn('po_buyer', $get_po)->update(['queu' => $movement_queu, 'plan_id' => $id_plan]);

                            CuttingPlan::where('id', $id_plan)->where('deleted_at', null)->first()->update(['cutting_date' => $movement_date, 'queu' => $movement_queu]);

                            $respons['status'] = 'success';
                            $respons['message'] = 'Plan berhasil dipindah!';
                        }
                    }
                } else {
                    $respons['status'] = 'failed';
                    $respons['message'] = 'Plan tidak ditemukan!';
                }
            } else {
                $respons['status'] = 'failed';
                $respons['message'] = 'Data plan atau geser planning kosong!';
            }
            return response()->json($respons,200);
        }
    }

    public function deletePlan(Request $request)
    {
        if(request()->ajax())
        {
            $id_plan = $request->id_plan;
            $respons = [];

            if($id_plan != NULL) {
                $delete_plan = CuttingPlan::where('id', $id_plan)->where('deleted_at', null)->first();
                if($delete_plan != null) {

                    DB::transaction(function () use ($id_plan, $delete_plan) {
                        $delete_plan->deleted_at = Carbon::now();
                        $delete_plan->save();
                        DetailPlan::where('id_plan', $id_plan)->where('deleted_at', null)->update(['deleted_at' => Carbon::now()]);
                    });

                    $respons['status'] = 'success';
                    $respons['message'] = 'Plan berhasil dihapus!';
                } else {
                    $respons['status'] = 'failed';
                    $respons['message'] = 'Data plan tidak ditemukan!!';
                }
            } else {
                $respons['status'] = 'failed';
                $respons['message'] = 'Terjadi kesalahan, silakan kontak ICT!!';
            }
            return response()->json($respons,200);
        }
    }
}
