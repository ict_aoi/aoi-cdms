<div class="row" id="notif_view">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title">Pemberitahuan Perubahan Planning<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<a href="{{ route('notif.data') }}" id="link-notif-data"></a>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse" class="" id="notif_detail"></a></li>
						<li><a data-action="close"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body" style="display: none;">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-flat">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<strong><p id="day1_label">Tidak Ada Data<a class="heading-elements-toggle"><i class="icon-more"></i></a></p></strong>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse" class=""></a></li>
										</ul>
									</div>
								</div>

								<div class="panel-body" style="display: none;" id="notif_day1">
									<div class="row">
										<div class="col-md-12">
											<div class="content-group">
												<div class="list-group no-border">
													<div id="day1_data">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel panel-flat">
								<div class="panel-heading">
									<strong><p id="day2_label">Tidak Ada Data<a class="heading-elements-toggle"><i class="icon-more"></i></a></p></strong>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse" class=""></a></li>
										</ul>
									</div>
								</div>

								<div class="panel-body" style="display: none;" id="notif_day2">
									<div class="row">
										<div class="col-md-12">
											<div class="content-group">
												<div class="list-group no-border">
													<div id="day2_data">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel panel-flat">
								<div class="panel-heading">
									<strong><p id="day3_label">Tidak Ada Data<a class="heading-elements-toggle"><i class="icon-more"></i></a></p></strong>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse" class=""></a></li>
										</ul>
									</div>
								</div>

								<div class="panel-body" style="display: none;" id="notif_day3">
									<div class="row">
										<div class="col-md-12">
											<div class="content-group">
												<div class="list-group no-border">
													<div id="day3_data">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel panel-flat">
								<div class="panel-heading">
									<strong><p id="day4_label">Tidak Ada Data<a class="heading-elements-toggle"><i class="icon-more"></i></a></p></strong>
									<div class="heading-elements">
										<ul class="icons-list">
											<li><a data-action="collapse" class=""></a></li>
										</ul>
									</div>
								</div>

								<div class="panel-body" style="display: none;" id="notif_day4">
									<div class="row">
										<div class="col-md-12">
											<div class="content-group">
												<div class="list-group no-border">
													<div id="day4_data">

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>