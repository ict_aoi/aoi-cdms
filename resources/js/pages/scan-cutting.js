
var barcode_list = [];
var marker_list  = [];
var scan_id_list = [];
var myNik;

$(document).ready( function () {

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $.extend( $.fn.dataTable.defaults, {
        stateSave: true,
        autoWidth: false,
        autoLength: false,
        processing: true,
        // serverSide: true, //sync
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        }
    });

    $('#table_cutting_check').on('click', function() {
        $('#tableModal').modal();
        get_data_cutting_table();
    });

    $('#tableTable').on('click', '#select_cutting_table', function() {

        var id_cutting_table = $(this).data('id');
        var barcode_cutting_table = $(this).data('barcode');
        var name_cutting_table = $(this).data('name');
        barcode_for_table(id_cutting_table);
        $('#tableModal').modal('hide');
        list_scan_id = [];
    });

    $('.input-new').keypress(function(event){
        if(event.which == 13) {
            event.preventDefault();

            var input_barcode = $(this).val();
            var id_scan = $('#id_scan').val();
            var barcode_check = input_barcode.substr(0, 3);
            var state = $('#state').val();



            // console.log(barcode_check == 'MK-');
            // console.log(input_barcode);
            // console.log(barcode_check);

            if(input_barcode.length == 9) {
                var table_barcode = $('#id_cutting_table').val();
                if(state == 'progress') {
                    $("#alert_warning").trigger("click", 'Sedang Proses Cutting, Tidak scan selain barcode marker!');
                    $('#marker_check').val('');
                    return false;
                }
                if(!table_barcode){
                    $("#alert_warning").trigger("click", 'Scan Barcode Meja Cutting Terlebih Dahulu!');
                    $('#marker_check').val('');
                    return false;
                }
                barcode_for_operator(input_barcode, id_scan);
                return false
            } else {
                if(barcode_check == 'MJ-') {
                    if(state == 'progress') {
                        $("#alert_warning").trigger("click", 'Sedang Proses Cutting, Tidak scan selain barcode marker!');
                        $('#marker_check').val('');
                        return false;
                    }
                    barcode_for_table(input_barcode);
                    return false
                } else if(barcode_check == 'MK-') {
                    var table_barcode = $('#id_cutting_table').val();
                    var cutter_barcode = $('#cutter_check').val();

                    if(!table_barcode){
                        $("#alert_warning").trigger("click", 'Scan Barcode Meja Cutting Terlebih Dahulu!');
                        $('#marker_check').val('');
                        return false;
                    }

                    if(state == 'progress') {
                        var barcode_marker = $('#barcode_marker').val();
                        if(barcode_marker != input_barcode) {
                            $("#alert_warning").trigger("click", 'Sedang Proses Cutting, Scan dengan barcode marker yang sama!');
                            $('#marker_check').val('');
                            return false;
                        }
                       check_marker(input_barcode, id_scan, state);
                        return false;
                    } else {
                        check_marker(input_barcode, id_scan, state);
                        return false;
                    }
                } else {
                    $("#alert_warning").trigger("click", 'Barcode Tidak Dapat Dikenali!');
                    $('#marker_check').val('');
                    return false;
                }
            }
        }

        console.log(marker_list);
    });

    $('#btn-cut-start').click(function(event){

        var id_scan       = $('#id_scan').val();
        var state         = $('#state').val();

        console.log(marker_list);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/scan-cutting/start-cutting',
            data: {
                id_scan: id_scan,
                state: state,
                list_marker: marker_list
            },
            beforeSend: function() {
                $('.panel').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.panel').unblock();
                appendMarker(response.marker);
                list_scan_id=[];
                $('#marker_check').val('');
                $('#marker_check').attr('readonly', true);
                $('#btn-cut-start').addClass('hidden');
                $('#btn-cut-end').removeClass('hidden');
            },
            error: function(response) {
                $('#loading_view').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#btn-cut-end').click(function(event){

        var id_scan       = $('#id_scan').val();
        var state         = $('#state').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/scan-cutting/end-cutting',
            data: {
                id_scan: id_scan,
                state: state,
                list_marker: marker_list
            },
            beforeSend: function() {
                $('.panel').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.panel').unblock();
                // appendMarker(response.marker);

                $('#btn-cut-start').removeClass('hidden');
                $('#btn-cut-end').addClass('hidden');
                $('#downtimeModal').modal();
                appendLastData(response.marker);
                scan_id_list = response.list_scan_id;
                marker_list = [];
                $('#marker_check').val('');
                $('#marker-view').html('');
                $('#barcode_marker').val('');
                $('#table_cutting_check').val('');
                $('#barcode_cutting_table').val('');
                $('#id_cutting_table').val('');
                $('#state').val('');
                $('#marker_check').attr('readonly', false);
            },
            error: function(response) {
                $('#loading_view').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#tableModal').on('hidden.bs.modal', function(){
        $('#marker_check').focus();
    });

    $(document).on('click', '.delete-operator', function() {
        var id_scan_operator = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/scan-cutting/delete-operator',
            data: {
                id_scan_operator: id_scan_operator,
            },
            beforeSend: function() {
                $('.panel').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.panel').unblock();
                if(response.status == 200) {
                    append_cutter(response.operator);
                    $('#marker_check').val('');
                    $('#marker_check').focus();
                } else {
                    append_cutter(response.operator);
                    $('#marker_check').val('');
                    $('#marker_check').focus();
                    $("#alert_warning").trigger("click", 'Barcode Meja Tidak Ditemukan!');
                }
            },
            error: function(response) {
                $('#loading_view').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $(document).on('click', '#btn-delete-marker', function () {
        var scan_id    = $(this).data('id');
        var barcode_id = $(this).data('barcode');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/scan-cutting/delete-marker',
            data: {
                scan_id: scan_id,
                barcode_id: barcode_id,
            },
            beforeSend: function() {
                $('.panel').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.panel').unblock();
                // append_cutter();
                appendMarker(response.marker);
                marker_list = JSON.parse(response.list_marker);
                // $('#marker-view').html('');
                // $('#barcode_marker').val('');
                // $('#table_cutting_check').val('');
                // $('#barcode_cutting_table').val('');
                // $('#id_cutting_table').val('');
                // $('#state').val('');
                // console.log(response);
            },
            error: function(response) {
                $('.panel').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });
    $(document).on('click', '#btn-cancel-cutting', function() {
        var id_scan = $(this).data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'post',
            url: '/scan-cutting/cancel-cutting',
            data: {
                id_scan: id_scan,
            },
            beforeSend: function() {
                $('.panel').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response) {
                $('.panel').unblock();
                append_cutter(null);
                $('#marker-view').html('');
                $('#barcode_marker').val('');
                $('#table_cutting_check').val('');
                $('#barcode_cutting_table').val('');
                $('#id_cutting_table').val('');
                $('#state').val('');
                console.log(response);
            },
            error: function(response) {
                $('.panel').unblock();
                $.unblockUI();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#btn_downtime_submit').on('click', function() {
        var downtime = $('#downtime').val();
        var keterangan = $('#keterangan').val();
        // var id_scan = $('#id_scan').val();
        var list_scan_id = scan_id_list;
        var downtime_category = $('#downtime_category').val();

        validation_check_callback(downtime_category, downtime, keterangan, id_scan, function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'post',
                url: '/scan-cutting/update-downtime',
                data: {
                    downtime: downtime,
                    keterangan: keterangan,
                    list_scan_id: scan_id_list,
                    downtime_category: downtime_category,
                },
                beforeSend: function() {
                    $('.panel').block({
                        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: '10px 15px',
                            color: '#fff',
                            width: 'auto',
                            '-webkit-border-radius': 2,
                            '-moz-border-radius': 2,
                            backgroundColor: '#333'
                        }
                    });
                },
                success: function(response) {
                    $('.panel').unblock();
                    $('#downtime_category').each(function(i, v) {
                        $(this).children('option').removeAttr('selected');
                    });

                    $('#select2-downtime_category-container').text('Pilih a downtime...');
                    $('#select2-downtime_category-container').removeAttr('title');
                    $('#select2-downtime_category-container').attr('title', 'Pilih a downtime...');

                    $("#alert_success").trigger("click", response.message);
                    $('#downtime').val('');
                    $('#keterangan').val('');
                    $('#id_scan').val('');
                    $('#downtimeModal').modal('hide');
                    // scan_id_list = [];
                },
                error: function(response) {
                    $('.panel').unblock();
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                }
            });
        });
    });
});

function barcode_for_table(input_barcode) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/scan-cutting/check-table',
        data: {
            table_check: input_barcode,
        },
        beforeSend: function() {
            $('.panel').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('.panel').unblock();
            if(response.status == 200) {
                $('#table_cutting_check').val(response.data.table_name);
                $('#barcode_cutting_table').val(response.data.id_table);
                $('#id_cutting_table').val(response.data.id_table);
                $('#id_scan').val(response.header.id);
                $('#state').val(response.header.state);
                setMyNik(response.mynik, function() {
                    append_cutter(response.operator);
                });
                if(response.header.state == 'progress') {

                    // $('#btn-cut-start').addClass('')
                    $('#btn-cut-start').addClass('hidden');
                    $('#btn-cut-end').removeClass('hidden');
                }
                    // console.log(response.list_marker);
                    // console.log(JSON.parse(response.list_marker));
                appendMarker(response.marker);
                marker_list = JSON.parse(response.list_marker);
                // marker_list = response.list_marker;
                    // $('#barcode_marker').val(response.marker.barcode_id);
                // } else {
                //     $('#marker-view').html('');
                // }
                $('#marker_check').val('');
                $('#marker_check').focus();
            } else {
                append_cutter(null);
                $('#table_cutting_check').val('');
                $('#marker_check').val('');
                $('#marker_check').focus();
                $("#alert_warning").trigger("click", 'Barcode Meja Tidak Ditemukan!');
            }
        },
        error: function(response) {
            $('#loading_view').unblock();
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
        }
    });
}

function barcode_for_operator(input_barcode, id_scan) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/scan-cutting/check-operator',
        data: {
            operator_check: input_barcode,
            id_scan: id_scan,
        },
        beforeSend: function() {
            $('.panel').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('.panel').unblock();
            if(response.status == 200) {
                append_cutter(response.operator);
                $('#marker_check').val('');
                $('#marker_check').focus();
            } else {
                $('#marker_check').val('');
                $('#marker_check').focus();
                $("#alert_warning").trigger("click", response.message);
            }
        },
        error: function(response) {
            $('#loading_view').unblock();
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.message);
        }
    });
}

function check_marker(input_barcode, id_scan, state) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/scan-cutting/check-marker',
        data: {
            marker_check: input_barcode,
            id_scan: id_scan,
            state: state,
        },
        beforeSend: function() {
            $('.panel').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('.panel').unblock();
            if(response.status == 200) {
                // if(response.header.state == 'done') {
                //     append_cutter(null);
                //     $('#marker-view').html('');
                //     $('#barcode_marker').val('');
                //     $('#table_cutting_check').val('');
                //     $('#barcode_cutting_table').val('');
                //     $('#id_cutting_table').val('');
                //     $('#state').val('');
                //     $('#downtimeModal').modal();
                //     appendLastData(response.marker)
                // } else {
                    // append_cutter(response.operator, state);
                    appendMarker(response.marker);
                    marker_list = JSON.parse(response.list_marker);
                    // console.log(marker_list);
                    // console.log(response.list_marker);
                    // $('#barcode_marker').val(response.marker.barcode_id);
                    // $('#state').val(response.header.state);
                // }
                $('#marker_check').val('');
                $('#marker_check').focus();
            } else {
                $('#marker_check').val('');
                $('#marker_check').focus();
                $("#alert_warning").trigger("click", response.message);
            }
        },
        error: function(response) {
            $('#panel').unblock();
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
        }
    });
}

function barcode_for_marker(input_barcode, id_scan, state) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'post',
        url: '/scan-cutting/save-cutting',
        data: {
            marker_check: input_barcode,
            id_scan: id_scan,
            state: state,
        },
        beforeSend: function() {
            $('.panel').block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333'
                }
            });
        },
        success: function(response) {
            $('.panel').unblock();
            if(response.status == 200) {
                if(response.header.state == 'done') {
                    append_cutter(null);
                    $('#marker-view').html('');
                    $('#barcode_marker').val('');
                    $('#table_cutting_check').val('');
                    $('#barcode_cutting_table').val('');
                    $('#id_cutting_table').val('');
                    $('#state').val('');
                    $('#downtimeModal').modal();
                    appendLastData(response.marker)
                } else {
                    append_cutter(response.operator, state);
                    appendMarker(response.marker);
                    $('#barcode_marker').val(response.marker.barcode_id);
                    $('#state').val(response.header.state);
                }
                $('#marker_check').val('');
                $('#marker_check').focus();
            } else {
                $('#marker_check').val('');
                $('#marker_check').focus();
                $("#alert_warning").trigger("click", response.message);
            }
        },
        error: function(response) {
            $('#panel').unblock();
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
        }
    });
}

function get_data_cutting_table() {
    var table = $('#tableTable').DataTable({
        destroy: true,
        bFilter: false,
        paging: false,
        ordering: false,
        info: false,
        ajax: {
            url: '/actual-spreading/data-cutting-table',
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
            {
                targets: 2,
                className: 'text-center',
            }
        ],
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'table_name', name: 'table_name', searchable:true, visible:true, orderable:false},
            {data: 'action', name: 'action', searchable:false, orderable:false},
        ],
    });

    $('#tableTable').on('click','.ignore-click', function() {
        return false;
    });
}

function append_cutter(data, state) {
    $('#operator-view').html('');

    if($.isArray(data)) {
        if(data.length > 0) {
            $.each(data, function(index, value) {
                var urutan = index + 1;
                if(state == 'progress') {
                    if(myNik == value.nik) {
                        $('#operator-view').append(
                            $('<div>').addClass('row').append(
                                $('<div>').addClass('col-md-8').append(
                                    $('<input>').attr('type', 'text').addClass('form-control check-operator').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).val(value.name+' ('+value.nik+')')
                                )
                            ).append(
                                $('<div>').addClass('col-md-3').append(
                                    $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Barcode Cutter '+urutan).attr('readonly', true)
                                )
                            ).append(
                                $('<div>').addClass('col-md-1').append(
                                    $('<button>').attr('type', 'button').addClass('btn btn-danger btn-sm disabled').attr('data-id', value.id).append(
                                        $('<i>').addClass('icon-trash')
                                    )
                                )
                            )
                        );
                    } else {
                        $('#operator-view').append(
                            $('<div>').addClass('row').append(
                                $('<div>').addClass('col-md-8').append(
                                    $('<input>').attr('type', 'text').addClass('form-control check-operator').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).val(value.name+' ('+value.nik+')')
                                )
                            ).append(
                                $('<div>').addClass('col-md-3').append(
                                    $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Barcode Cutter '+urutan).attr('readonly', true)
                                )
                            ).append(
                                $('<div>').addClass('col-md-1').append(
                                    $('<button>').attr('type', 'button').addClass('btn btn-danger btn-sm delete-operator').attr('data-id', value.id).append(
                                        $('<i>').addClass('icon-trash')
                                    )
                                )
                            )
                        );
                    }
                } else {
                    if(myNik == value.nik) {
                        $('#operator-view').append(
                            $('<div>').addClass('row').append(
                                $('<div>').addClass('col-md-8').append(
                                    $('<input>').attr('type', 'text').addClass('form-control check-operator').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).val(value.name+' ('+value.nik+')')
                                )
                            ).append(
                                $('<div>').addClass('col-md-3').append(
                                    $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Operator '+urutan).attr('readonly', true)
                                )
                            ).append(
                                $('<div>').addClass('col-md-1').append(
                                    $('<button>').attr('type', 'button').addClass('btn btn-danger btn-sm disabled').attr('data-id', value.id).append(
                                        $('<i>').addClass('icon-trash')
                                    )
                                )
                            )
                        );
                    } else {
                        $('#operator-view').append(
                            $('<div>').addClass('row').append(
                                $('<div>').addClass('col-md-8').append(
                                    $('<input>').attr('type', 'text').addClass('form-control check-operator').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).val(value.name+' ('+value.nik+')')
                                )
                            ).append(
                                $('<div>').addClass('col-md-3').append(
                                    $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Operator '+urutan).attr('readonly', true)
                                )
                            ).append(
                                $('<div>').addClass('col-md-1').append(
                                    $('<button>').attr('type', 'button').addClass('btn btn-danger btn-sm delete-operator').attr('data-id', value.id).append(
                                        $('<i>').addClass('icon-trash')
                                    )
                                )
                            )
                        );
                    }
                }
            });
        } else {
            $('#operator-view').append(
                $('<div>').addClass('row').append(
                    $('<div>').addClass('col-md-8').append(
                        $('<input>').attr('type', 'text').addClass('form-control').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).attr('placeholder', '#Scan Barcode ID Cutter').css('opacity', '1')
                    )
                ).append(
                    $('<div>').addClass('col-md-4').append(
                        $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Operator').attr('readonly', true)
                    )
                )
            );
        }
    } else {
        $('#operator-view').append(
            $('<div>').addClass('row').append(
                $('<div>').addClass('col-md-8').append(
                    $('<input>').attr('type', 'text').addClass('form-control').attr('id', 'check_cutter').attr('name', 'check_cutter').attr('readonly', true).attr('placeholder', '#Scan Barcode ID Cutter').css('opacity', '1')
                )
            ).append(
                $('<div>').addClass('col-md-4').append(
                    $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Operator').attr('readonly', true)
                )
            )
        );
    }

}

function appendMarker(data) {
    console.log(data);
    // data = JSON.parse(data);
    $('#marker-view').html('');
    // if(data) {
    //     $('#marker-view').append(
    //         $('<div>').addClass('row').append(
    //             $('<div>').addClass('col-md-9').append(
    //                 $('<input>').attr('type', 'text').addClass('form-control bg-info').attr('id', 'marker_view').attr('name', 'marker_view').val(data.barcode_id+' [ '+data.style+' | '+data.articleno+' | '+data.size_category+' | Cut '+data.cut+' | Part '+data.part_no+' | Layer '+data.layer+' | Total Garment '+data.total+' ]').attr('readonly', true)
    //             )
    //         ).append(
    //             $('<div>').addClass('col-md-3').append(
    //                 $('<a>').addClass('btn btn-warning btn-sm pull-right').text('Cancel Cutting').attr('id', 'btn-delete-marker').attr('data-id', data.scan_id)
    //             )
    //         )
    //     ).append(
    //         $('<br>')
    //     ).append(
    //         $('<div>').addClass('row').append(
    //             $('<div>').addClass('col-md-6').append(
    //                 $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Ratio: '+data.ratio_print).attr('readonly', true)
    //             )
    //         // ).append(
    //         //     $('<div>').addClass('col-md-3').append(
    //         //         $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Status: '+data.state).attr('readonly', true)
    //         //     )
    //         ).append(
    //             $('<div>').addClass('col-md-3').append(
    //                 $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Panjang Marker: '+data.marker_length).attr('readonly', true)
    //             )
    //         ).append(
    //             $('<div>').addClass('col-md-3').append(
    //                 $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Perimeter: '+data.perimeter).attr('readonly', true)
    //             )
    //         )
    //     );
    // }
    if(Array.isArray(data)) {
        if(data.length > 0) {
            $.each(data, function(index, value) {

                if(value.state == 'progress') {
                    $('#marker-view').append(
                        $('<div>').addClass('row').append(
                            $('<div>').addClass('col-md-9').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').attr('id', 'marker_view').attr('name', 'marker_view').val(value.barcode_id+' [ '+value.style+' | '+value.articleno+' | '+value.size_category+' | Cut '+value.cut+' | Part '+value.part_no+' | Layer '+value.layer+' | Total Garment '+value.total+' ]').attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<a>').addClass('btn btn-warning btn-sm pull-right disabled').text('Hapus Marker').attr('id', 'btn-delete-marker').attr('data-id', value.scan_id).attr('data-barcode', value.barcode_id)
                            )
                        )
                    ).append(
                        $('<br>')
                    ).append(
                        $('<div>').addClass('row').append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Ratio: '+value.ratio_print).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Start at: '+value.start_at).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Panjang Marker: '+value.marker_length).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Perimeter: '+value.perimeter).attr('readonly', true)
                            )
                        )
                    );
                }
                else{
                    $('#marker-view').append(
                        $('<div>').addClass('row').append(
                            $('<div>').addClass('col-md-9').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').attr('id', 'marker_view').attr('name', 'marker_view').val(value.barcode_id+' [ '+value.style+' | '+value.articleno+' | '+value.size_category+' | Cut '+value.cut+' | Part '+value.part_no+' | Layer '+value.layer+' | Total Garment '+value.total+' ]').attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<a>').addClass('btn btn-warning btn-sm pull-right').text('Hapus Marker').attr('id', 'btn-delete-marker').attr('data-id', value.scan_id).attr('data-barcode', value.barcode_id)
                            )
                        )
                    ).append(
                        $('<br>')
                    ).append(
                        $('<div>').addClass('row').append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Ratio: '+value.ratio_print).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Start at: '+value.start_at).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Panjang Marker: '+value.marker_length).attr('readonly', true)
                            )
                        ).append(
                            $('<div>').addClass('col-md-3').append(
                                $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Perimeter: '+value.perimeter).attr('readonly', true)
                            )
                        )
                    );
                }
            });
        } else {
            $('#marker-view').html('');
            // $('#marker-view').append(
            //     $('<div>').addClass('row').append(
            //         $('<div>').addClass('col-md-8').append(
            //             $('<input>').attr('type', 'text').addClass('form-control input-new').attr('id', 'marker_check').attr('name', 'marker_check').attr('readonly', true).attr('placeholder', '#Scan Barcode Marker').css('opacity', '1')
            //         )
            //     ).append(
            //         $('<div>').addClass('col-md-4').append(
            //             $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Barcode Marker').attr('readonly', true)
            //         )
            //     )
            // );
        }
    } else {
        $('#marker-view').html('');
        // $('#marker-view').append(
        //     $('<div>').addClass('row').append(
        //         $('<div>').addClass('col-md-8').append(
        //             $('<input>').attr('type', 'text').addClass('form-control input-new').attr('id', 'marker_check').attr('name', 'marker_check').attr('readonly', true).attr('placeholder', '#Scan Barcode Barcode Marker').css('opacity', '1')
        //         )
        //     ).append(
        //         $('<div>').addClass('col-md-4').append(
        //             $('<input>').attr('type', 'text').addClass('form-control bg-info').val('Barcode Marker').attr('readonly', true)
        //         )
        //     )
        // );
    }
}

function appendLastData(data) {
    if(Array.isArray(data)) {
        if(data.length > 0) {
            $.each(data, function(index, value) {
                $('#lastActivityTable > tbody').prepend(
                    $('<tr>').append(
                        $('<td>').text(value.barcode_id)
                    ).append(
                        $('<td>').text(value.style+' | '+value.articleno+' | '+value.size_category+' | Cut '+value.cut+' | Part '+value.part_no)
                    ).append(
                        $('<td>').text(value.table_name)
                    ).append(
                        $('<td>').text(value.start_at)
                    ).append(
                        $('<td>').text(value.end_at)
                    ).append(
                        $('<td>').append(
                            $('<span>').addClass('badge bg-success').text('Done')
                        )
                    )
                );
            });
        }
    }

}

function setMyNik(nik, callback) {
    myNik = nik;
    callback(nik);
}

function isNumberDot(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
        var regex = /^[0-9]*$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function validation_check_callback(downtime_category, downtime, keterangan, id_scan, callback_function) {
    if(keterangan) {
        if(!downtime) {
            $("#alert_warning").trigger("click", "waktu downtime tidak boleh kosong!");
            return false;
        }

        if(!downtime_category) {
            $("#alert_warning").trigger("click", "kategori downtime tidak boleh kosong!");
            return false;
        }
    }

    if(downtime) {
        if(!downtime_category) {
            $("#alert_warning").trigger("click", "kategori downtime tidak boleh kosong!");
            return false;
        }
    }

    callback_function(downtime_category, downtime, keterangan, id_scan);
}
