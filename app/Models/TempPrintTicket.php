<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TempPrintTicket extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'temp_print_ticket';
	protected $fillable = ['id','barcode_id','season','style','set_type','poreference','article','cut_num','size','part','komponen','qty','start_no','end_no','box_lim','note','job','podd','ship_to','coll','bd_op','qc_op','cutter','ed_pcd','add_note','created_by','factory_id','barcode_cdms'];
    protected $dates        = ['created_at','updated_at'];
}
