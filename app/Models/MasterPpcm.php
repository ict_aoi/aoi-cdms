<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterPpcm extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'master_ppcm';
	protected $dates = ['created_at', 'updated_at'];
	protected $fillable = ['area_id','factory_id','set_type','season','articleno','qty_order','qty_split','qty_actual','start_date','end_date','created_at','updated_at','deleted_at','po_buyer','style','user_id','area_name'];
}
