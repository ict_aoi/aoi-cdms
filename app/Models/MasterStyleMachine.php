<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class MasterStyleMachine extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $fillable = ['style','season_id','product_type_id','name','inisial','code','user_id','qty','created_at','updated_at','deleted_at'];
}
