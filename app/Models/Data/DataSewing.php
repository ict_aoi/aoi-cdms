<?php

namespace App\Models\Data;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DataSewing extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $table        = 'data_sewings';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $fillable     = ['documentno','season','style','po_buyer','job_no','customer','destination','ordered_qty','material','product_category','cons','fbc','uom','start_sewing','is_piping','c_order_id','warehouse_id','warehouse','factory_id','cust_no','statistical_date','lc_date','upc','code_raw_material','desc_raw_material','color_code_material','width_size','desc_produksi','mo_updated_at','ts_lc','articleno','m_product_id','is_recycle','deleted_at','size_finish_good'];
}
