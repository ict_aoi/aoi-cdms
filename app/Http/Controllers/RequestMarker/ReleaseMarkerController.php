<?php

namespace App\Http\Controllers\RequestMarker;

use DB;
use Carbon\Carbon;
use StdClass;
use App\Models\MarkerReleaseHeader;
use App\Models\MarkerReleaseDetail;
use App\Models\CuttingPlan;
use App\Models\CombinePart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReleaseMarkerController extends Controller
{
    public function index()
    {
        return view('request_marker.release_marker.index');
    }

    public function getData(Request $request)
    {
        if(request()->ajax())
        {
            $cutting_date = $request->cutting_date;
            if($cutting_date != NULL) {

                if(\Auth::user()->factory_id > 0) {
                    $data = MarkerReleaseHeader::where('cutting_date', $cutting_date)->where('factory_id', \Auth::user()->factory_id)->where('deleted_at', null);
                } else {
                    $data = MarkerReleaseHeader::where('cutting_date', $cutting_date)->where('deleted_at', null);
                }

                return datatables()->of($data)
                ->editColumn('cutting_date', function($data) {
                    return Carbon::parse($data->cutting_date)->format('d-m-Y');
                })
                ->editColumn('factory_id', function($data) {
                    return $data->factory->factory_alias;
                })
                ->editColumn('lc_date', function($data) {
                    return Carbon::parse($data->lc_date)->format('d-m-Y');
                })
                ->addColumn('po_buyer', function($data) {
                    if($data->combine_id == null) {
                        return implode(', ', $data->cutting_plan->po_details->pluck('po_buyer')->toArray());
                    } else {
                        $po_list = array();
                        $plan_id_list = CombinePart::where('combine_id', $data->combine_id)->where('deleted_at', null)->pluck('plan_id')->toArray();
                        foreach($plan_id_list as $plan_id) {
                            $get_po_list = CuttingPlan::where('id', $plan_id)->where('deleted_at', null)->first()->po_details->pluck('po_buyer')->toArray();
                            $po_list = array_merge($po_list, $get_po_list);
                        }
                        return implode(', ', $po_list);
                    }
                })
                ->editColumn('item_id', function($data) {
                    if ($data->item_id == null) {
                        return null;
                    } else {
                        return DB::connection('wms_live')->table('items')->where('item_id', $data->item_id)->first()->item_code;
                    }
                })
                ->addColumn('balance', function($data) {
                    if($data->detail_approve->count() > 0) {
                        return $data->detail->first()->balance;
                    } else {
                        if(is_null($data->detail->first())){
                            return null;
                        }else{
                            return $data->detail->first()->balance;
                        }
                    }
                })
                ->addColumn('action', function($data) {
                    return view('request_marker.release_marker._action', [
                        'model'      => $data,
                        'detail'     => route('ReleaseMarker.getDetail',$data->id)
                    ]);
                })
                ->rawColumns(['action'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    public function getDetail($id)
    {
        $data = MarkerReleaseHeader::where('id', $id)->where('deleted_at', null)->first();
        if ($data->item_id == null) {
            $item_code = null;
        } else {
            $item_code = DB::connection('wms_live')->table('items')->where('item_id', $data->item_id)->first()->item_code;
        }
        if($data->detail_approve->count() > 0) {
            $balance = null;
        } else {
            $balance = $data->detail->first()->balance;
        }
        $obj = new StdClass();
        $obj->id = $id;
        $obj->style = $data->style;
        $obj->pcd = Carbon::parse($data->cutting_date)->format('d-m-Y');
        $obj->lc = Carbon::parse($data->lc_date)->format('d-m-Y');
        $obj->factory = $data->factory->factory_alias;
        $obj->part_no = $data->part_no;
        $obj->po_buyer = implode(', ', $data->cutting_plan->po_details->pluck('po_buyer')->toArray());
        $obj->item = $item_code;
        $obj->balance = $balance;

		return response()->json($obj,200);
    }

    public function getDetailTable(Request $request)
    {
        if(request()->ajax())
        {
            $release_id = $request->release_id;
            if($release_id != NULL) {

                $data = MarkerReleaseDetail::where('header_id', $release_id)->where('deleted_at', null)->orderBy('created_at', 'asc');

                return datatables()->of($data)
                ->editColumn('created_at', function($data) {
                    return Carbon::parse($data->created_at)->diffForHumans();
                })
                ->editColumn('release_by', function($data) {
                    return $data->user_release->name;
                })
                ->addColumn('persen', function($data) {
                    return 100 - ($data->actual_marker_prod / $data->supply_whs * 100);
                })
                ->editColumn('confirm_by', function($data) {
                    if($data->confirm_by == null) {
                        return '<span class="badge badge-warning">Belum Approve</span>';
                    } else {
                        return $data->user_confirm->name;
                    }
                })
                ->editColumn('confirm_at', function($data) {
                    if($data->confirm_at == null) {
                        return null;
                    } else {
                        return Carbon::parse($data->confirm_at)->diffForHumans();
                    }
                })
                ->rawColumns(['confirm_by'])
                ->make(true);
            } else {
                $data = array();
                return datatables()->of($data)
                ->rawColumns([])
                ->make(true);
            }
        }
    }

    static function checkFabricPrepare()
    {
        $tgl  = Carbon::now()->subDays(2)->format('Y-m-d');
        $data = DB::select(db::raw("SELECT * from jaz_balance_marker where balance < 0
                and (persen_prepare is null or persen_prepare < 90)
                and date(cutting_date) >= '$tgl'
                ORDER BY factory_id, style, articleno, po_buyer, part_no"));

        foreach($data as $d){
            $po = explode(',', $d->po_buyer);

            // $qty_prepared = DB::connection('wms_live')
            //     ->table('integration_whs_to_cutting')
            //     ->where('style',$d->style)
            //     ->where('planning_date', $d->cutting_date)
            //     ->where('article_no',$d->articleno)
            //     ->where('part_no',$d->part_no)
            //     ->whereIn('po_buyer',$po)
            //     ->sum('qty_prepared');

            $style = $d->style;
            $cutting_date = $d->cutting_date;
            $article = $d->articleno;
            $part = $d->part_no;
            $po = "'".implode("','",$po)."'";

            $qty_prepared = DB::connection('wms_live_new')->select(DB::raw("SELECT sum(qty_prepared) as qty_prepared FROM integration_to_cutting_by_planning_f('$cutting_date') WHERE style='$style' AND article_no ='$article' AND po_buyer IN($po) AND part_no = $part"));

            if($d->supply_whs == 0){
                $percentage = 0;
            }
            else{
                $percentage = ($qty_prepared[0]->qty_prepared/$d->supply_whs)*100;
            }

            MarkerReleaseDetail::where('id', $d->marker_release_detail_id)
            ->update([
                'persen_prepare' => round($percentage,2)
            ]);

        }
    }
}
