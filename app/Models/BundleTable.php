<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class BundleTable extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
    protected $table = 'bundle_table';

    protected $fillable = ['id_table', 'name', 'factory_id', 'created_at', 'updated_at', 'deleted_at', 'created_by','updated_by'];

    public function distribusi()
    {
        return $this->hasMany('App\Models\DistribusiMovement','bundle_table','id');
    }
}
