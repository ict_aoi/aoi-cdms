@extends('layouts.app',['active' => 'manual_create'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Manual Create Ticket</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Manual Create Ticket</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

<div class="panel panel-flat data_detail">
    <form class="form-horizontal" action="{{ route('manual_create.addDatasize') }}" method="POST"  id="temp_dt_bd" enctype="multipart/form-data">
        @csrf
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">INPUT DATA BUNDLE</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                <input type="hidden" name="user_id" id="user_id" value="{{\Auth::user()->nik}}" readonly>
                <input type="hidden" name="is_detail" id="is_detail" value="{{$is_details}}" readonly>
                <input type="hidden" name="is_head" id="is_head" readonly>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group col-md-12">
                                <div class="col-md-12">
                                    <legend class="text-semibold"><i class="icon-design position-left"></i>DATA BUNDLE</legend>
                                </div>
                                <div class="col-md-12">
                                    {{-- <select data-placeholder="Pilih Nama Style" class="form-control select-search" name="style" id="style" required>
                                        <option value=""></option>
                                        @foreach($style as $st)
                                        <option value="{{$st->season.'-'.$st->style}}">{{$st->season.'-'.$st->style}}</option>
                                        @endforeach
                                    </select> --}}
                                    <select data-placeholder="Select a Style..." name="style" id="style" class="form-control select-search">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="cut_num" id="cut_num" placeholder="Nomor Cutting">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="coll" id="coll" placeholder="Warna Fabric">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="bd_op" id="bd_op" placeholder="Nama Operator Bundling">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="qc_op" id="qc_op" placeholder="Nama Operator QC">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="machine" id="machine" placeholder="Mesin Cutter">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold number_only" style="background-color : #d1d1d1;padding-left:5px" name="q_bd" id="q_bd" placeholder="MAX QTY / BUNDLE">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <select data-placeholder="Pilih Size" class="form-control select-search" name="size" id="size">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control text-bold number_only" style="background-color : #d1d1d1;padding-left:5px" name="q_box" id="q_box" placeholder="MAX QTY / BOX">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-12">
                                    <input type="text" value="{{'AOI '.\Auth::user()->factory_id}}" class="form-control text-bold text-uppercase" style="background-color : #d1d1d1;padding-left:5px" name="note" id="note" placeholder="CATATAN TAMBAHAN">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-eye position-left"></i>PREVIEW</legend>
                            <div class="table-responsive">
                                <table class="table table-basic table-condensed table-bordered table-striped table-hover" id="list_size">
                                    <thead class="bg-teal">
                                        <tr>
                                            <th>SIZE</th>
                                            <th style="width:10%">ACTION</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-left">
                        <button type="submit" class="btn btn-primary">ADD<i class="icon-plus2 position-right"></i></button>
                        <lable type="button" name="create" id="create" class="btn btn-success bt_create hidden">CREATE<i class="icon-qrcode position-right"></i></lable>
                        <lable type="button" name="cancel_head" id="cancel_head" class="btn btn-danger bt_cancel hidden">CANCEL<i class="icon-trash position-right"></i></lable>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="panel panel-flat hidden data_head">
    <form class="form-horizontal" action="{{route('manual_create.addDetailsize')}}" method="POST"  id="reg_order_size" enctype="multipart/form-data">
        @csrf
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">URUTAN PO STYLE {{strtoupper($style_head)}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <fieldset>
                <div class="panel-body">
                    <div class="row">
                    <input type="hidden" name="user_id" id="user_id" value="{{\Auth::user()->nik}}" readonly>
                    <input type="hidden" name="style" id="style" value="{{$style_head}}" readonly>
                    <input type="hidden" name="cut_num" id="cut_num" value="{{$cut_num}}" readonly>
                        <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <legend class="text-semibold" style="padding-top:20px"><i class="icon-certificate position-left"></i>DETAIL</legend>
                                        </div>
                                        <div class="col-md-6">
                                            <span>Queue</span><input type="text" class="form-control text-bold text-uppercase text-center" style="background-color : #d1d1d1;padding-left:5px;padding-bottom: 0px;border-bottom-width: 1px;height: 30px;padding-top: 0px;width: 219px;" value="{{$que_no}}" name="order" id="order" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <select data-placeholder="Pilih PO Buyer" class="form-control select-search" name="po_buyer" id="po_buyer">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select data-placeholder="Pilih Article" class="form-control select-search" name="article" id="article">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            
                        </div>
                        <div class="col-md-6">
                            <legend class="text-semibold"><i class="icon-design position-left"></i>FILL QTY SIZE</legend>
                            <div class="table-responsive form-group">
                                <table class="table table-condensed table-bordered table-striped" id="list_size">
                                    <thead>
                                        <tr style="font-size:12px">
                                            <th style="width: 30px;" class="bg-teal text-bold">SIZE</th>
                                            @php
                                            $i=0;
                                            @endphp
                                            @foreach($size_register as $sr)
                                                <?php $i++ ?>
                                                <th style="width: 50px;padding-left: 3px;padding-right: 3px;text-align:center"><input class="text-bold text-center" style="border:0px;width:35px" id="sz-{{$i}}" name="sz-{{$i}}" value="{{$sr->size}}" readonly></input></th>
                                            @endforeach
                                        </tr>
                                        <tr style="font-size:12px">
                                            <th style="width: 30px;" class="bg-teal text-bold">QTY</th>
                                            @for($i=0; $i < count($size_register); $i++)
                                                <th style="padding:3px;"><input type="text" class="form-control text-bold text-uppercase" id="{{'size_reg-'.$i}}" name="{{'size_reg-'.$i}}" style="background-color : #d1d1d1;text-align:center"></input></th>
                                            @endfor
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">ADD<i class="icon-plus2 position-right"></i></button>
                            <lable type="button" id="generate" class="btn btn-success hidden btn_generate">GENERATE<i class="icon-barcode2 position-right"></i></lable>
                            <lable type="button" id="cancel_detail" class="btn btn-danger">CANCEL<i class="icon-trash position-right"></i></lable>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed table-bordered table-striped table-hover" id="list_detail_size">
                        <thead class="bg-teal">
                            <tr>
                                <th>ORDER</th>
                                <th>STYLE / CUT</th>
                                <th>PO BUYER</th>
                                <th>SEASON</th>
                                <th>ARTICLE</th>
                                <th>CUTTING DATE</th>
                                <th>SIZE</th>
                                <th>QTY</th>
                                <th style="width:10%">ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed table-bordered table-striped table-hover" id="summary_size_qty">
                        <thead class="bg-grey-700">
                            <tr>
                                <th>SEASON</th>
                                <th>STYLE / CUT</th>
                                <th>ARTICLE</th>
                                <th>SIZE</th>
                                <th>QTY</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="{{ route('manual_create.getDataSize') }}" id="prev_data_size"></a>
<a href="{{ route('manual_create.summarySizeQty') }}" id="summary_size_qty_link"></a>
<a href="{{ route('manual_create.getDataDetail') }}" id="prev_data_detail"></a>
<a href="{{ route('manual_create.ajaxGetDataSize') }}" id="get_data_size"></a>
<a href="{{ route('manual_create.ajaxGetDataPobuyer') }}" id="get_data_po"></a>
<a href="{{ route('manual_create.ajaxGetDataArt') }}" id="get_data_article"></a>
<a href="{{ route('manual_create.createHead') }}" id="create_head"></a>
<a href="{{ route('manual_create.generateBarcode') }}" id="generate_barcode"></a>
<a href="{{ route('manual_create.deleteHeader') }}" id="delete_header"></a>
<a href="{{ route('manual_create.deleteDetail') }}" id="delete_detail"></a>
<a href="{{ route('manual_create.updateQty') }}" id="update_qty_link"></a>
<a href="{{ route('manual_create.getDataStyle') }}" id="get_data_style"></a>
@endsection
@section('page-js')
<script type="text/javascript">
$('body').block({
    message: '<i class="icon-spinner4 spinner"></i>',
    overlayCSS: {
        backgroundColor: '#fff',
        opacity: 0.8,
        cursor: 'wait'
    },
    css: {
        border: 0,
        padding: 0,
        backgroundColor: 'none'
    }
});
$(document).ready(function(){
    $('.select-search').select2({
        allowClear: true
    });
    window.onload = function(){
        $.unblockUI();
        var url = "{{ route('manual_create.sessionEdit') }}";
        tempSessionUser(url);
        var urlReg = "{{ route('manual_create.checkRegSize') }}";
        checkRegsize(urlReg);
        var is_detail = $('#is_detail').val();
        if(is_detail == 1){
            $('.data_detail').addClass('hidden');
            $('.data_head').removeClass('hidden');
        }
    };
    var url = $('#prev_data_size').attr('href');
    var table = $('#list_size').DataTable({
        autoWidth: false,
        autoLength: false,
        paging: false,
        info: false,
        order:false,
        scrollCollapse: true,
        searching: false,
        ajax: url,
        ajax: {
            url: url,
        },
        columnDefs: [
            { className: 'text-center', targets: [0]},
        ],
        columns: [
            {data: 'size', name: 'size', orderable: true, searchable: true},//1
            {data: 'action', name: 'action', orderable: false, searchable: false}//23
        ]
    });

    var url = $('#prev_data_detail').attr('href');
    var table2 = $('#list_detail_size').DataTable({
        autoWidth: false,
        autoLength: false,
        paging: false,
        info: false,
        order:false,
        scrollCollapse: true,
        searching: false,
        ajax: url,
        ajax: {
            url: url,
        },
        columnDefs: [
            { className: 'text-center', targets: [0]},
        ],
        columns: [
            {data: 'que_no', name: 'que_no', orderable: true, searchable: true},//1
            {data: 'style', name: 'style', orderable: true, searchable: true},//1
            {data: 'poreference', name: 'poreference', orderable: true, searchable: true},//1
            {data: 'season', name: 'season', orderable: true, searchable: true},//1
            {data: 'article', name: 'article', orderable: true, searchable: true},//1
            {data: 'cutting_date', name: 'cutting_date', orderable: true, searchable: true},//1
            {data: 'size', name: 'size', orderable: true, searchable: true},//1
            {data: 'qty', name: 'qty', orderable: true, searchable: true},//1
            {data: 'action', name: 'action', orderable: false, searchable: false}//23
        ]
    });

    var url = $('#summary_size_qty_link').attr('href');
    var table3 = $('#summary_size_qty').DataTable({
        autoWidth: false,
        autoLength: false,
        paging: false,
        info: false,
        order:false,
        scrollCollapse: true,
        searching: false,
        ajax: url,
        ajax: {
            url: url,
        },
        columnDefs: [
            { className: 'text-center', targets: [0]},
        ],
        columns: [
            {data: 'season', name: 'season', orderable: true, searchable: true},//1
            {data: 'style', name: 'style', orderable: true, searchable: true},//1
            {data: 'article', name: 'article', orderable: true, searchable: true},//1
            {data: 'size', name: 'size', orderable: true, searchable: true},//1
            {data: 'qty', name: 'qty', orderable: true, searchable: true},//1
        ]
    });

    $('#temp_dt_bd').submit(function(event) {
        event.preventDefault();
        var size = $('#size').val();
        if(size == '' || size == null){
            myalert('error','Harap Pilih Size Dahulu');
            return false;
        }
        $('#style').removeAttr('disabled');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#temp_dt_bd').attr('action'),
            data: $('#temp_dt_bd').serialize(),
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function(response) {
                $('#style').attr('disabled',true);
                $('#cut_num').attr('readonly',true);
                $('#coll').attr('readonly',true);
                $('#bd_op').attr('readonly',true);
                $('#qc_op').attr('readonly',true);
                $('#machine').attr('readonly',true);
                $('#q_bd').attr('readonly',true);
                $('#q_box').attr('readonly',true);
                $('#note').attr('readonly',true);
                $('#size').val('').change();
                if($('.bt_create').hasClass('hidden')){
                    $('.bt_create').removeClass('hidden');
                }
                if($('.bt_cancel').hasClass('hidden')){
                    $('.bt_cancel').removeClass('hidden');
                }
                table.ajax.reload();
            },
            error: function(response) {
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#style').attr('disabled',true);
            }
        })
    });

    $('#reg_order_size').submit(function(event) {
        var po_buyer = $('#po_buyer').val();
        var article = $('#article').val();
        if(po_buyer == '' || po_buyer == null){
            alert2('error','PO Buyer Belum Dipilih!')
            return false;
        }
        if(article == '' || article == null){
            alert2('error','Article Belum Dipilih!')
            return false;
        }
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",
            url : $('#reg_order_size').attr('action'),
            data: $('#reg_order_size').serialize(),
            beforeSend: function () {
                $('#reg_order_size').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Processing...</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },
            success: function() {
                $('#reg_order_size').unblock();
                document.getElementById("reg_order_size").reset();
                $('#po_buyer').val('').change();
                $('#article').val('').change();
                var urlQ = "{{ route('manual_create.updateQue') }}";
                updateQue(urlQ);
                table2.ajax.reload();
                table3.ajax.reload();
                $('.btn_generate').removeClass('hidden');
            },
            error: function(response) {
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                $('#reg_order_size').unblock();
            }
        })
    });

    $("#list_size").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this Size ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "manual-creation/delete/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        alert2('success','Deleted Successfully');
                        table.ajax.reload();
                    },
                    error: function() {
                        alert2('error','Cannot Delete This Size');
                    }
                });
            }
        });
    });

    $("#list_detail_size").on("click", ".delete", function() {
        event.preventDefault();
        var id = $(this).data('id');
        var token = $(this).data("token");
        bootbox.confirm("Are you sure delete this detail ?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: "manual-creation/delete-detail/"+id,
                    type: "GET",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $(".loader-area").unblock();
                    },
                    success: function () {
                        alert2('success','Deleted Successfully');
                        table2.ajax.reload();
                        var urlQ = "{{ route('manual_create.updateQue') }}";
                        updateQue(urlQ);
                    },
                    error: function() {
                        alert2('error','Cannot Delete This Size');
                    }
                });
            }
        });
    });

    $('#list_detail_size').on('keypress', '.qty_unfilled', function(e) {
        if(e.which == 13){
            var id = $(this).data('id');
            var qty = $(this).val();
            var reg = new RegExp(/[a-zA-Z,()!~`@#$^&*.=+;:'"<>|?]/g);
            if(reg.test($(this).val())){
                myalert('error','Invalid');
                $(this).val('');
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: $('#update_qty_link').attr('href'),
                data: {
                    id:id,
                    qty:qty
                },
                success: function(response){
                    $('#qty_' + id).html(response);
                    alert2('success','QTY Updated!');
                    table2.ajax.reload();
                },
                error: function(response) {
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    table2.ajax.reload();
                }
            });
        }
    });

    $('.number_only').on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^0-9]/g,''));
        if(event.which == 44){
            return false;
        }
        if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57  )){
            event.preventDefault();
        }
    });

    $('#size').select2({
        placeholder: "Pilih Size",
        minimumInputLength: 2,
        ajax: {
            url: $('#get_data_size').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    $('#po_buyer').select2({
        placeholder: "Pilih PO Buyer",
        minimumInputLength: 5,
        ajax: {
            url: $('#get_data_po').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    $('#article').select2({
        placeholder: "Pilih Article",
        minimumInputLength: 3,
        ajax: {
            url: $('#get_data_article').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    $('#create').on('click',function(){
        var user_id = $('#user_id').val();
        $('#style').removeAttr('disabled');
        var style = $('#style').val();
        var cut_num = $('#cut_num').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#create_head').attr('href'),
            data: {
                user_id:user_id,
                style:style,
                cut_num:cut_num
            },
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $('.loader-area').unblock();
            },                
            success: function(){
                $('#is_detail').val(1);
                $('.data_detail').addClass('hidden');
                $('.data_head').removeClass('hidden');
                location.reload();
            },
            error: function(response) {
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#generate').on('click',function(){
        var user_id = $('#user_id').val();
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: $('#generate_barcode').attr('href'),
            data: {
                user_id:user_id,
            },
            beforeSend: function() {
                $('#reg_order_size').block({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; Generating Barcode</span>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        width: 'auto',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333'
                    }
                });
            },
            success: function(response){
                $('#reg_order_size').unblock();
                alert2('success','Barcode Generated!');
                window.location.href = "/manual-creation/";
            },
            error: function(response) {
                $('#reg_order_size').unblock();
                if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            }
        });
    });

    $('#cancel_detail').on('click',function(){
        var user_id = $('#user_id').val();
        bootbox.confirm("Are you Cancel All Registered Size?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: $('#delete_detail').attr('href'),
                    data: {
                        user_id:user_id,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $('.loader-area').unblock();
                    },                
                    success: function(response){
                        $('.data_detail').removeClass('hidden');
                        $('.data_head').addClass('hidden');
                        $('#is_detail').val('');
                        var url = "{{ route('manual_create.sessionEdit') }}";
                        tempSessionUser(url);
                        var urlQ = "{{ route('manual_create.updateQue') }}";
                        updateQue(urlQ);
                        table.ajax.reload();
                        location.reload();
                    },
                    error: function(response) {
                        alert2('error','TERJADI KESALAHAN HARAP HUBUNGI ICT!');
                    }
                });
            }
        });
    });

    $('#cancel_head').on('click',function(){
        var user_id = $('#user_id').val();
        bootbox.confirm("Are you Cancel All Registered Size?", function (result) {
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: $('#delete_header').attr('href'),
                    data: {
                        user_id:user_id,
                    },
                    beforeSend: function () {
                        $('.loader-area').block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    },
                    complete: function () {
                        $('.loader-area').unblock();
                    },                
                    success: function(response){
                        alert2('success','All Registered Size was Deleted!')
                        $('#style').removeAttr('disabled');
                        $('#cut_num').removeAttr('readonly');
                        $('#coll').removeAttr('readonly');
                        $('#bd_op').removeAttr('readonly');
                        $('#qc_op').removeAttr('readonly');
                        $('#machine').removeAttr('readonly');
                        $('#q_bd').removeAttr('readonly');
                        $('#q_box').removeAttr('readonly');
                        $('#note').removeAttr('readonly');

                        $('#style').val('').change();
                        $('#cut_num').val('');
                        $('#coll').val('');
                        $('#bd_op').val('');
                        $('#qc_op').val('');
                        $('#machine').val('');
                        $('#q_bd').val('');
                        $('#q_box').val('');
                        table.ajax.reload();
                    },
                    error: function(response) {
                        alert2('error','TERJADI KESALAHAN HARAP HUBUNGI ICT!');
                    }
                });
            }
        });
    });

    $('#style').select2({
        placeholder: "PILIH STYLE",
        minimumInputLength: 3,
        ajax: {
            url: $('#get_data_style').attr('href'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });
});

function alert2(type, text) {
    var color = '#66BB6A';
    var title = 'Good Job!';
    if(type == 'error') {
        color = '#EF5350';
        title = 'Oops...';
    }
    swal({
        title: title,
        text: text,
        confirmButtonColor: color,
        type: type,
        timer:1000,
    });
}
function tempSessionUser(url){
    var user_id = $('#user_id').val();
    $.ajax({
        type: "get",
        url: url,
        data:{
            user_id:user_id
        },
        success: function(response){
            console.log(Object.keys(response).length);
            if(Object.keys(response).length > 0){
                var style = $('#style').val(response.season+'-'+response.style).change();
                $('#cut_num').val(response.cut_num);
                $('#coll').val(response.coll);
                $('#bd_op').val(response.bd_op);
                $('#qc_op').val(response.qc_op);
                $('#machine').val(response.cutter);
                $('#q_bd').val(response.bd_lim);
                $('#q_box').val(response.box_lim);
                $('#note').val(response.add_note);
                $('#size').val('').change();
                $('#style').attr('disabled',true);
                $('#cut_num').attr('readonly',true);
                $('#coll').attr('readonly',true);
                $('#bd_op').attr('readonly',true);
                $('#qc_op').attr('readonly',true);
                $('#machine').attr('readonly',true);
                $('#q_bd').attr('readonly',true);
                $('#q_box').attr('readonly',true);
                $('#note').attr('readonly',true);
                if(style != ''){
                    if($('.bt_create').hasClass('hidden')){
                        $('.bt_create').removeClass('hidden');
                    }
                    if($('.bt_cancel').hasClass('hidden')){
                        $('.bt_cancel').removeClass('hidden');
                    }
                }
            }else{
                document.getElementById("temp_dt_bd").reset();
                $('.bt_create').addClass('hidden');
                $('.bt_cancel').addClass('hidden');
            }
        },
    })
}
function checkRegsize(urlReg){
    var user_id = $('#user_id').val();
    $.ajax({
        type: "get",
        url: urlReg,
        data:{
            user_id:user_id
        },
        success: function(response){
            if(response > 0){
                if($('.btn_generate').hasClass('hidden')){
                    $('.btn_generate').removeClass('hidden');
                }
            }
        },
    })
}
function updateQue(urlQ){
    var user_id = $('#user_id').val();
    var po_buyer = $('#po_buyer').val();
    $.ajax({
        type: "get",
        url: urlQ,
        data:{
            user_id:user_id,
        },
        success: function(response){
            if(response == 0){
                $('#order').val(parseInt(1));
            }else{
                var last_que = response.que_no;
                var poreference = response.poreference;
                if(last_que && po_buyer != poreference && po_buyer != '' || po_buyer != null){
                    $('#order').val(parseInt(last_que)+1);
                }
            }
        }
    })
}
</script>
@endsection




