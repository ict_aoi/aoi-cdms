@extends('layouts.app',['active' => 'data_sewing'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">MO Sewing AOI {{\Auth::user()->factory_id}}</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Manufacturing Order Sewing</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row form-group " id="filter_by_lcdate">
            @csrf
            <label><b>Choose Date</b></label>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                <input type="text" class="form-control pickadate" name="start_sewing" id="start_sewing" name="start_sewing" placeholder="Masukkan Tanggal Sewing">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary margin-kanan" id="btn-filter">Pilih<i class="icon-select2 position-right"></i></button>
                    <button type="button" class="btn btn-success" name="sync_mo_sewing" id="sync_mo_sewing">UPDATE MO<i class="icon-sync position-right"></i></label>
                </div>
            </div>
            <div class="input-group-btn" style="margin-top:10px;padding-top:10px">
                <button type="button" class="btn bg-info-700" name="sync_mo_sewing3" id="sync_mo_sewing3"><b>UPDATE MO <u>+</u> 3</b><i class="icon-calendar2 position-right"></i></label>
                <button type="button" class="btn bg-warning-700" name="sync_mo_sewing5" id="sync_mo_sewing5"><b>UPDATE MO <u>+</u> 7</b><i class="icon-calendar2 position-right"></i></label>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed table-hover table-striped" id="table-list">
                <thead class="bg-teal">
                    <tr>
                        <th>#</th>
                        <th>LINE</th>
                        <th>SEASON</th>
                        <th>STYLE</th>
                        <th>ARTICLE</th>
                        <th>PO BUYER</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
<div id="modalDetail"  data-backdrop="static" data-keyboard="false" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <input type="hidden" id="line" name="line">
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed table-striped table-hover" id="table-detail">
                        <thead class="bg-grey">
                            <tr>
                            <th>SEASON</th>
                            <th>STYLE</th>
                            <th>ARTICLE</th>
                            <th>PO BUYER</th>
                            <th>SIZE</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-body" id="sizeBalance">
            </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
@endsection
<a href="{{ route('data_sewing.updateMosewing') }}" id="sync_mo_sewing_link"></a>
<a href="{{ route('data_sewing.updateMosewing3') }}" id="sync_mo_sewing_link3"></a>
<a href="{{ route('data_sewing.updateMosewing5') }}" id="sync_mo_sewing_link5"></a>
@section('page-js')
<script>
$(document).ready(function(){

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    var pcd =$('#start_sewing').val();
    var url = "{{route('data_sewing.getDataMoSewing')}}";
    var tableL = $('#table-list').DataTable({
        processing: true,
        serverSide: true,
        deferRender:true,
        pageLength:30,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
                return $.extend({}, d, {
                    "start_sewing"         : $('#start_sewing').val(),
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = tableL.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(5).css('max-width','80px');
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'desc_produksi', name: 'desc_produksi', searchable:true, orderable:true},//0
            {data: 'season', name: 'season', searchable:true, orderable:true},//1
            {data: 'style', name: 'style'},//2
            {data: 'articleno', name: 'articleno'},//3
            {data: 'po_buyer', name: 'po_buyer'},//4
            {data: 'action', name:'action', searchable:false, orderable:false}//5
        ]
    });

    var tableL = $('#table-list').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                tableL.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                tableL.search("").draw();
            }
            return;
    });

    tableL.on('preDraw',function(){
        Pace.start();
        $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    })
    .on('draw.dt',function(){
        Pace.stop();
        $.unblockUI();
    });

    $('#btn-filter').click(function(){
        event.preventDefault();
        var start_sewing    = $('#start_sewing').val();

        if (start_sewing=='') {
            $("#alert_warning").trigger("click", 'Tanggal tidak boleh kosong');
            return false;
        }else{
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'GET',
                url : url,
                data:{start_sewing:start_sewing},
                beforeSend:function(){
                    loading_process();
                },
                success:function(response){
                    $('#table-list').unblock();
                    tableL.clear();
                    tableL.draw();
                },
                error:function(response){
                    myalert('error','eror');
                }
            });
        }
    });

    $("#table-list").on("click", ".detail", function() {
        event.preventDefault();
        var line_id = $(this).data('id');        
        $.ajax({
            url: "/data-sewing/detail-line/"+line_id,
            type: "GET",
            data: {
                "line_id": line_id,
            },
            beforeSend: function () {
                $('.loader-area').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $(".loader-area").unblock();
            },
            success: function (response) {
                console.log(response);
                $('#modalDetail').modal();
                $('#line').val(response);
                get_data_modal_table();
            },
            error: function() {
                myalert('error','TERJADI KESALAHAN HARAP HUBUNGI ICT!');
            }
        });
    });

    $('#modalDetail').on('hidden.bs.modal', function(){
        tableL.ajax.reload();
    });

    $('#sync_mo_sewing').on('click', function() {
        var start_sewing = $('#start_sewing').val();
        console.log(start_sewing);

        if(start_sewing == '') {
            $("#alert_warning").trigger("click", 'Tanggal Sewing Tidak Boleh Kosong!');
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'post',
                url: $('#sync_mo_sewing_link').attr('href'),
                data: {
                    start_sewing: start_sewing,
                },
                beforeSend:function(){
                $('#filter_by_lcdate').block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });
                },
                complete: function () {
                    $("#filter_by_lcdate").unblock();
                },
                success:function(response){
                    $("#filter_by_lcdate").unblock();
                    myalert('success','Manufacturing Order Updated!');
                },
                eror:function(response){
                    $("#filter_by_lcdate").unblock();
                    myalert('error','Terjadi Kesalahan Harap Hubungi ICT!');
                }
            });
        }
    });

    $('#sync_mo_sewing3').on('click', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url: $('#sync_mo_sewing_link3').attr('href'),
            beforeSend:function(){
            $('#filter_by_lcdate').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $("#filter_by_lcdate").unblock();
            },
            success:function(response){
                $("#filter_by_lcdate").unblock();
                myalert('success','Manufacturing Order Updated!');
            },
            eror:function(response){
                $("#filter_by_lcdate").unblock();
                myalert('error','Terjadi Kesalahan Harap Hubungi ICT!');
            }
        });
    });

    $('#sync_mo_sewing5').on('click', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'post',
            url: $('#sync_mo_sewing_link5').attr('href'),
            beforeSend:function(){
            $('#filter_by_lcdate').block({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            },
            complete: function () {
                $("#filter_by_lcdate").unblock();
            },
            success:function(response){
                $("#filter_by_lcdate").unblock();
                myalert('success','Manufacturing Order Updated!');
            },
            eror:function(response){
                $("#filter_by_lcdate").unblock();
                myalert('error','Terjadi Kesalahan Harap Hubungi ICT!');
            }
        });
    });
});
function get_data_modal_table() {
    var line_id         = $('#line').val();
    var start_sewing    = $('#start_sewing').val();
    var table1 = $('#table-detail').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        deferRender:true,
        pageLength:30,
        dom: '<"datatable-header"fBl><t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            url: '/data-sewing/get-data-detail',
            data: {
                line_id: line_id,
                start_sewing:start_sewing
            },
        },
        columnDefs: [
            {
                className: 'dt-center'
            },
        ],
        columns: [
            {data: 'season', name: 'season', searchable:true, orderable:true},//1
            {data: 'style', name: 'style'},//2
            {data: 'articleno', name: 'articleno'},//3
            {data: 'po_buyer', name: 'po_buyer'},//4
            {data: 'size_finish_good', name: 'size_finish_good'},//4
        ],
    });
    return table1;
}
</script>
@endsection
