<?php

namespace App\Http\Controllers\Report;

use DB;
use DataTables;
use Carbon\Carbon;
use Excel;
use App\Models\DistribusiMovement;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DistribusiMovementReportController extends Controller
{
    public function indexBundleMovement()
    {
        $po_buyer = DB::table('bundle_header')
        ->select(DB::raw('poreference'))
        ->where('factory_id',\Auth::user()->factory_id)
        ->whereDate('created_at','>',Carbon::now()->subDays(150)->format('Y-m-d'))
        ->distinct('poreference')
        ->get();
        // $cut_num = DB::table('bundle_header')->select('cut_num')->where('factory_id',\Auth::user()->factory_id)->where('statistical_date','>',Carbon::now()->format('Y-m-d'))->distinct()->orderBy('cut_num')->get();
        $locators = DB::table('master_locator')->whereNull('deleted_at')->orderBy('locator_name')->get();
        return view('report.distribusi_movement.bundle_movement.index', compact('locators','po_buyer'));
    }

    public function downloadBundleMovement(Request $request)
    {
        $locator = $request->locator;
        $factory_id = \Auth::user()->factory_id;
        // $data = DistribusiMovement::whereIn('locator_to', $locator)->whereNull('deleted_at')->orderBy('created_at','desc');
        $data = DB::select(DB::raw("SELECT z.barcode_id,bd.sds_barcode,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,bd.komponen_name,bh.cut_num,CONCAT(bd.start_no,'-',bd.end_no) AS sticker,bd.qty,z.pic_in,z.in_at,z.out_at,z.pic_out,
        CASE WHEN z.out_at IS NULL THEN true END AS is_wip,bd.current_locator_id FROM(SELECT 
        y.barcode_id,MAX(y.in_at) as in_at,MAX(y.pic_in) AS pic_in,MAX(y.out_at) AS out_at,MAX(y.pic_out) AS pic_out
        FROM
        (SELECT x.barcode_id,
        CASE WHEN x.status_to = 'in' THEN x.created_at END AS in_at,
        CASE WHEN x.status_to = 'in' THEN x.name END AS pic_in,
        CASE WHEN x.status_to = 'out' THEN x.created_at END AS out_at,
        CASE WHEN x.status_to = 'out' THEN x.name END AS pic_out
        FROM(SELECT 
        dm.barcode_id,dm.status_to,dm.description,u.name,dm.created_at
        FROM distribusi_movements dm 
        JOIN users u ON u.id = dm.user_id
        WHERE dm.locator_to='$locator'
        ORDER BY dm.barcode_id)x)y
        GROUP BY y.barcode_id)z
        JOIN bundle_detail bd ON bd.barcode_id = z.barcode_id
        JOIN bundle_header bh ON bh.id = bd.bundle_header_id
        JOIN v_master_style  vms ON vms.id = bd.style_detail_id
        WHERE bh.factory_id='$factory_id' AND bd.current_locator_id='$locator'
        UNION 
        SELECT z.barcode_id,sdm.sds_barcode,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,sdm.komponen_name,shm.cut_num,CONCAT(sdm.start_no,'-',sdm.end_no) AS sticker,sdm.qty,z.pic_in,z.in_at,z.out_at,z.pic_out,
        CASE WHEN z.out_at IS NULL THEN true END AS is_wip,sdm.current_location_id as current_locator_id
        FROM(SELECT y.barcode_id,MAX(y.in_at) AS in_at,MAX(y.pic_in) AS pic_in,MAX(y.out_at) AS out_at,MAX(y.pic_out) AS pic_out
        FROM(SELECT x.barcode_id,
        CASE WHEN x.status_to = 'in' THEN x.created_at end as in_at,
        CASE WHEN x.status_to = 'in' THEN x.name end as pic_in,
        CASE WHEN x.status_to = 'out' THEN x.created_at end as out_at,
        CASE WHEN x.status_to = 'out' THEN x.name end as pic_out
        FROM(SELECT dm.barcode_id,dm.status_to,dm.description,u.name,dm.created_at
        FROM distribusi_movements dm 
        JOIN users u on u.id = dm.user_id
        WHERE dm.locator_to='$locator'
        ORDER BY dm.barcode_id)x)y
        GROUP BY y.barcode_id)z
        JOIN sds_detail_movements sdm on sdm.sds_barcode = z.barcode_id
        JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
        JOIN v_master_style  vms on vms.id = sdm.style_id
        WHERE shm.factory_id='$factory_id' and sdm.current_location_id='$locator' ORDER BY poreference,style,type_name,cut_num"));

        $locator_detail = DB::table('master_locator')->where('id', $locator)->first();

        if(count($data) < 1){
            return response()->json('Data tidak ditemukan!', 422);
        }else{
            $filename = "Report_WIP_".$locator_detail->locator_name.'_'.Carbon::now()->format('Y-m-d').'AOI '.\Auth::user()->factory_id;
            return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'J' => '@',
                        'K' => '@',
                        'L' => '@',
                        'M' => '@',
                        'N' => '@',
                        'O' => '@',
                        'P' => '@',
                        'Q' => '@',
                        'R' => '@',
                        'R' => '@',
                    ));
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','BARCODE_ID');
                    $sheet->setCellValue('C1','SDS BARCODE');
                    $sheet->setCellValue('D1','PO BUYER');
                    $sheet->setCellValue('E1','SEASON');
                    $sheet->setCellValue('F1','STYLE');
                    $sheet->setCellValue('G1','ARTICLE');
                    $sheet->setCellValue('H1','SET TYPE');
                    $sheet->setCellValue('I1','KOMPONEN');
                    $sheet->setCellValue('J1','CUT');
                    $sheet->setCellValue('K1','SIZE');
                    $sheet->setCellValue('L1','STICKER');
                    $sheet->setCellValue('M1','QTY');
                    $sheet->setCellValue('N1','LOCATOR');
                    $sheet->setCellValue('O1','is WIP');
                    $sheet->setCellValue('P1','SCAN IN AT');
                    $sheet->setCellValue('Q1','PIC IN');
                    $sheet->setCellValue('R1','SCAN OUT AT');
                    $sheet->setCellValue('S1','PIC OUT');

                    $i = 1;
                    foreach ($data as $key => $vl){
                        $rw = $i+1;
                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,"'".$vl->barcode_id);
                        $sheet->setCellValue('C'.$rw,$vl->sds_barcode);
                        $sheet->setCellValue('D'.$rw,$vl->poreference);
                        $sheet->setCellValue('E'.$rw,$vl->season);
                        $sheet->setCellValue('F'.$rw,$vl->style);
                        $sheet->setCellValue('G'.$rw,$vl->article);
                        $sheet->setCellValue('H'.$rw,$vl->type_name);
                        $sheet->setCellValue('I'.$rw,$vl->komponen_name);
                        $sheet->setCellValue('J'.$rw,$vl->cut_num);
                        $sheet->setCellValue('K'.$rw,$vl->size);
                        $sheet->setCellValue('L'.$rw,$vl->sticker);
                        $sheet->setCellValue('M'.$rw,$vl->qty);
                        $locator = DB::table('master_locator')->where('id',$vl->current_locator_id)->first();
                        $sheet->setCellValue('N'.$rw,$locator->locator_name);
                        $sheet->setCellValue('O'.$rw,$vl->is_wip);
                        $sheet->setCellValue('P'.$rw,$vl->in_at);
                        $sheet->setCellValue('Q'.$rw,$vl->pic_in);
                        $sheet->setCellValue('R'.$rw,$vl->out_at);
                        $sheet->setCellValue('S'.$rw,$vl->pic_out);
                        $i++;
                    }
                });
                $excel->setActiveSheetIndex(0);
            })->export('xlsx');
        }
    }

    public function indexBundleOutput()
    {
        $locators = DB::table('master_locator')->whereNull('deleted_at')->orderBy('locator_name')->get();
        return view('report.distribusi_movement.bundle_output.index', compact('locators'));
    }

    public function downloadBundleOutput(Request $request)
    {
        $locator    = $request->locator;
        $state      = 'out';
        $scan_date  = explode('-', preg_replace('/\s+/', '', $request->scan_date));
        $from       = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[0].' 07:00:00')->format('Y-m-d H:i:s');
        $to         = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');
        $factory_id = Auth::user()->factory_id;

        $converter  = $this->convertLocator($locator,$state);
        $out_column = $converter['out_column'];
        $pic        = $converter['pic_column'];
        $process    = $converter['process'];

        if(in_array($locator,['10','11','16'])){
            $cdms_barcode = DB::table('distribusi_movements as dm')
            ->select('bd.sds_barcode')
            ->join('bundle_detail as bd','bd.barcode_id','dm.barcode_id')
            ->whereBetween('dm.created_at',[$from,$to])
            ->where('dm.locator_to',$locator);

            $sds_barcode = DB::table('distribusi_movements as dm')
            ->select('sdm.sds_barcode')
            ->join('sds_detail_movements as sdm','sdm.sds_barcode','dm.barcode_id')
            ->whereBetween('dm.created_at',[$from,$to])
            ->where('dm.locator_to',$locator)
            ->union($cdms_barcode)->pluck('sds_barcode')->toArray();

            dd($sds_barcode);
            // $data = DB::connection('sds_live')->select(DB::raw("SELECT z.poreference,z.season,z.style,z.article,z.cut_num,z.size,
            // SUM(z.qty) as qty,
            // MAX(z.$out_column) as last_scan,
            // MAX(mu.name) as $pic
            // FROM(SELECT DISTINCT y.poreference,y.season,y.style,y.article,y.cut_num,y.size,y.start_num,y.end_num,y.qty,y.$out_column,y.$pic
            // FROM(SELECT x.barcode_id,x.poreference,x.season,x.style,x.article,x.cut_num,x.size,x.part_num,x.part_name,x.start_num,x.end_num,
            // FIRST_VALUE(x.qty) OVER(PARTITION BY
            // x.poreference,x.season,x.style,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty) qty,
            // FIRST_VALUE(x.$out_column) OVER(PARTITION BY
            // x.poreference,x.season,x.style,x.article,x.cut_num,x.size,x.start_num,
            // x.end_num ORDER BY x.$out_column DESC NULLS LAST
            // ) $out_column,
            // FIRST_VALUE(x.$pic) OVER(PARTITION BY
            // x.poreference,x.season,x.style,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.$pic DESC NULLS LAST) $pic
            // FROM(SELECT 
            // bi.barcode_id,bi.poreference,bi.season,
            // CASE WHEN ad.set_type = '0' THEN bi.style ELSE CONCAT(bi.style,'-',ad.set_type) END AS style,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
            // CASE WHEN bi.$out_column IS NULL THEN 0 ELSE bi.qty END AS qty,bi.$out_column,bi.$pic
            // FROM bundle_info_new bi
            // JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            // WHERE (bi.season,bi.poreference,bi.style,bi.cut_num,bi.size,bi.start_num,bi.end_num) in
            // (SELECT DISTINCT bi.season,bi.poreference,bi.style,bi.cut_num,bi.size,bi.start_num,bi.end_num
            // FROM bundle_info_new bi
            // WHERE bi.$out_column BETWEEN '$from' AND '$to' AND bi.processing_fact='$factory_id'))x)y
            // WHERE y.$out_column BETWEEN '$from' AND '$to')z
            // JOIN m_user mu on mu.nik = z.$pic
            // GROUP BY z.poreference,z.season,z.style,z.article,z.cut_num,z.size"));
        }else{
            // $data = DB::connection('sds_live')->select(DB::raw("SELECT z.season,z.style,z.poreference,z.article,z.cut_num,z.size,
            // SUM(z.qty) as qty,
            // MAX(z.$out_column) as last_scan,
            // MAX(mu.name) as $pic
            // FROM(SELECT DISTINCT y.season,y.style,y.poreference,y.article,y.cut_num,y.size,y.start_num,y.end_num,y.qty,y.$out_column,y.$pic
            // FROM(SELECT x.barcode_id,x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,x.part_num,x.part_name,
            // FIRST_VALUE(x.qty) OVER(PARTITION BY x.poreference,x.season,x.style,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty)qty,
            // FIRST_VALUE(x.$out_column) OVER(PARTITION BY x.poreference,x.season,x.style,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.$out_column DESC NULLS LAST
            // )$out_column,
            // FIRST_VALUE(x.$pic) OVER(PARTITION BY x.poreference,x.season,x.style,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.$pic DESC NULLS LAST)$pic
            // FROM(SELECT bi.barcode_id,bi.season,
            // CASE WHEN ad.set_type = '0' THEN bi.style ELSE CONCAT(bi.style,'-',ad.set_type) end as style,bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
            // CASE WHEN bi.$out_column IS NULL THEN 0 ELSE bi.qty END AS qty,
            // bi.$out_column,bi.$pic
            // FROM bundle_info_new bi
            // JOIN art_desc_new ad on ad.season = bi.season AND ad.style = bi.style AND ad.part_num = bi.part_num AND ad.part_name = bi.part_name
            // where (bi.season,bi.style,bi.poreference,bi.article,bi.cut_num,bi.size,bi.start_num,bi.end_num)in(SELECT DISTINCT bi.season,bi.style,bi.poreference,bi.article,bi.cut_num,bi.size,bi.start_num,bi.end_num
            // FROM bundle_info_new bi
            // WHERE bi.$out_column BETWEEN '$from' AND '$to' AND bi.processing_fact='$factory_id')
            // AND ad.bonding IS TRUE
            // ORDER BY bi.poreference,bi.cut_num,bi.size,bi.start_num)x)y
            // WHERE y.$out_column BETWEEN '$from' AND '$to')z
            // JOIN m_user mu ON mu.nik = z.$pic
            // GROUP BY z.season,z.style,z.poreference,z.article,z.cut_num,z.size"));
            $data   = DB::connection('sds_live')->select(DB::raw("SELECT y.season,y.style,y.poreference,y.article,y.cut_num,y.size,
            SUM(y.qty) as qty,
            MAX(y.$out_column) as last_scan,
            MAX(mu.name) as $pic
            FROM(SELECT DISTINCT x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num,
            FIRST_VALUE(x.qty) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty)qty,
            FIRST_VALUE(x.$out_column) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty DESC)$out_column,
            FIRST_VALUE(x.$pic) OVER(PARTITION BY x.season,x.style,x.poreference,x.article,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.qty DESC)$pic
            FROM(SELECT bi.barcode_id,bi.season,
            CASE WHEN ad.set_type = '0' THEN bi.style ELSE CONCAT(bi.style,'-',ad.set_type) END AS style,bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,
            CASE WHEN bi.$out_column <= '$to'::TIMESTAMP THEN bi.qty ELSE 0 END AS qty,
            bi.$out_column,
            bi.$pic
            FROM 
            bundle_info_new bi 
            JOIN art_desc_new ad ON ad.season = bi.season AND ad.style = bi.style AND ad.part_num = bi.part_num AND ad.part_name = bi.part_name
            WHERE (bi.season,bi.poreference,bi.style,bi.article,bi.cut_num,bi.size) 
            IN(SELECT DISTINCT bi.season,bi.poreference,bi.style,bi.article,bi.cut_num,bi.size
            FROM bundle_info_new bi
            JOIN art_desc_new ad ON ad.season = bi.season AND ad.style = bi.style AND ad.part_num = bi.part_num and ad.part_name = bi.part_name
            WHERE bi.$out_column BETWEEN '$from' AND '$to' and bi.processing_fact='$factory_id' and ad.$process IS TRUE)
            AND bi.processing_fact='$factory_id' AND ad.$process IS TRUE
            ORDER BY bi.poreference,bi.style,bi.cut_num,bi.size,bi.start_num)x)y
            JOIN m_user mu on mu.nik = y.$pic
            GROUP BY y.season,y.style,y.poreference,y.article,y.cut_num,y.size"));
        }

        $locator_detail = DB::table('master_locator')->where('id', $locator)->first();

        if(count($data) < 1){
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Laporan_Bundle_Output_'.$locator_detail->locator_name.'_'.$state.'_'.$from.'-'.$to;
            return Excel::create($filename,function($excel) use($data,$state,$process,$pic){
                    $excel->sheet('active',function($sheet) use ($data,$state,$process,$pic){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'J' => 'DD-MM-YYYY hh:mm:ss'
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','SEASON');
                        $sheet->setCellValue('C1','STYLE');
                        $sheet->setCellValue('D1','ARTICLE');
                        $sheet->setCellValue('E1','PO BUYER');
                        $sheet->setCellValue('F1','SIZE');
                        $sheet->setCellValue('G1','CUT NUM');
                        $sheet->setCellValue('H1','OUTPUT');
                        $sheet->setCellValue('I1','PIC');
                        $sheet->setCellValue('J1','LAST SCAN');
                        $sheet->setCellValue('K1','LOCATOR');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->season);
                            $sheet->setCellValue('C'.$rw,$vl->style);
                            $sheet->setCellValue('D'.$rw,$vl->article);
                            $sheet->setCellValue('E'.$rw,$vl->poreference);
                            $sheet->setCellValue('F'.$rw,'="'.$vl->size.'"');
                            $sheet->setCellValue('G'.$rw,$vl->cut_num);
                            $sheet->setCellValue('H'.$rw,$vl->qty);
                            $sheet->setCellValue('I'.$rw,$vl->$pic);
                            $sheet->setCellValue('J'.$rw,$vl->last_scan);
                            $sheet->setCellValue('K'.$rw,$state.'_'.$process);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }

    public function downloadBundleOutputDaily(Request $request)
    {
        $locator    = $request->locator_2;
        $state      = 'out';
        $scan_date  = explode('-', preg_replace('/\s+/', '', $request->scan_date_2));
        $from       = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[0].' 07:00:00')->format('Y-m-d H:i:s');
        $to         = Carbon::createFromFormat('m/d/Y H:i:s', $scan_date[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');
        $factory_id = Auth::user()->factory_id;

        $converter  = $this->convertLocator($locator,$state);
        $out_column = $converter['out_column'];
        $pic        = $converter['pic_column'];
        $process    = $converter['process'];
        // $data = DB::table('jaz_report_dist_output_cutting')->whereBetween('scan_time_max', [$from, $to])->where('factory_id', \Auth::user()->factory_id)->whereNotIn('supplied_check', array('0'))->orderBy('scan_time_max','asc');
        if(in_array($locator,['10','11','16'])){
            $data = DB::connection('sds_live')->select(DB::raw("SELECT
            y.barcode_id,y.season,y.style,y.poreference,y.article,y.cut_num,y.size,y.part_num,y.part_name,y.start_num,y.end_num,y.qty,y.distrib_received,y.pic
            FROM(SELECT *,
            FIRST_VALUE(x.$out_column) OVER(PARTITION BY
            x.poreference,x.season,x.style,x.size,x.cut_num,x.start_num,
            x.end_num ORDER BY x.$out_column DESC NULLS LAST
            ) $out_column
            FROM(SELECT bi.barcode_id,bi.season,
            CASE WHEN ad.set_type = '0' THEN bi.style ELSE concat(bi.style,'-',ad.set_type) END AS style,bi.poreference,bi.article,bi.cut_num,bi.size,bi.part_num,bi.part_name,bi.start_num,bi.end_num,bi.qty,bi.$out_column,mu.name as pic
            FROM bundle_info_new bi
            LEFT JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            LEFT JOIN m_user mu on mu.nik = bi.$pic
            where (bi.poreference,bi.season,bi.style,bi.article,bi.cut_num,bi.size,bi.start_num,bi.end_num) in
            (SELECT poreference,season,style,article,cut_num,size,start_num,end_num
            FROM bundle_info_new WHERE $out_column BETWEEN '$from' AND '$to' AND processing_fact='$factory_id')
            ORDER BY bi.poreference,bi.cut_num,bi.size,bi.start_num,bi.end_num)x)y
            where y.$out_column BETWEEN '$from' AND '$to'"));
        }else{
            $data = DB::connection('sds_live')->select(DB::raw("SELECT y.barcode_id,y.season,y.style,y.poreference,y.cut_num,y.size,y.part_num,y.part_name,y.start_num,y.end_num,y.qty,y.$out_column,y.$pic
            FROM(SELECT x.barcode_id,x.season,x.style,x.poreference,x.cut_num,x.size,x.part_num,x.part_name,x.start_num,x.end_num,x.qty,x.$out_column,x.$pic,
            FIRST_VALUE(x.$out_column) OVER(PARTITION BY x.season,x.style,x.poreference,x.cut_num,x.size,x.start_num,x.end_num ORDER BY x.$out_column DESC NULLS LAST)temp_$out_column
            FROM(SELECT bi.barcode_id,bi.season,
            CASE WHEN ad.set_type = '0' THEN bi.style ELSE CONCAT(bi.style,'-',ad.set_type) END AS style,bi.poreference,bi.cut_num,bi.size,bi.start_num,bi.end_num,bi.part_num,bi.part_name,bi.qty,bi.$out_column,mu.name as $pic
            FROM bundle_info_new bi
            LEFT JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            LEFT JOIN m_user mu on mu.nik = bi.$pic
            WHERE (bi.poreference,bi.style,bi.season,bi.article,bi.size,bi.cut_num,bi.start_num,bi.end_num) in(SELECT DISTINCT poreference,style,season,article,size,cut_num,start_num,end_num
            FROM bundle_info_new where $out_column BETWEEN '$from' AND '$to' AND processing_fact='$factory_id')
            and ad.$process IS TRUE)x)y
            WHERE y.temp_$out_column BETWEEN '$from' AND '$to'"));
        }

        $locator_detail = DB::table('master_locator')->where('id', $locator)->first();

        if(count($data) < 1)
        {
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = 'Uncomplete_Scan_Process_'.$locator_detail->locator_name.'_'.$state.'_'.$from.'-'.$to;
            return Excel::create($filename,function($excel) use($data,$state,$process,$pic,$out_column){
                    $excel->sheet('active',function($sheet) use ($data,$state,$process,$pic,$out_column){
                        $sheet->setColumnFormat(array(
                            'B' => '@',
                            'E' => '@',
                            'H' => '@',
                            'J' => '@',
                            'L' => 'DD:MM:YYYY hh:mm:ss'
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','BARCODE');
                        $sheet->setCellValue('C1','SEASON');
                        $sheet->setCellValue('D1','STYLE');
                        $sheet->setCellValue('E1','PO BUYER');
                        $sheet->setCellValue('F1','CUT NUM');
                        $sheet->setCellValue('G1','SIZE');
                        $sheet->setCellValue('H1','PART');
                        $sheet->setCellValue('I1','KOMPONEN');
                        $sheet->setCellValue('J1','STICKER');
                        $sheet->setCellValue('K1','QTY');
                        $sheet->setCellValue('L1','SCAN AT');
                        $sheet->setCellValue('M1','PIC');
                        $sheet->setCellValue('N1','PROSES');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->barcode_id);
                            $sheet->setCellValue('C'.$rw,$vl->season);
                            $sheet->setCellValue('D'.$rw,$vl->style);
                            $sheet->setCellValue('E'.$rw,$vl->poreference);
                            $sheet->setCellValue('F'.$rw,$vl->cut_num);
                            $sheet->setCellValue('G'.$rw,'="'.$vl->size.'"');
                            $sheet->setCellValue('H'.$rw,$vl->part_num);
                            $sheet->setCellValue('I'.$rw,$vl->part_name);
                            $sheet->setCellValue('J'.$rw,$vl->start_num.'-'.$vl->end_num);
                            $sheet->setCellValue('K'.$rw,$vl->qty);
                            $sheet->setCellValue('L'.$rw,$vl->$out_column);
                            $sheet->setCellValue('M'.$rw,$vl->$pic);
                            $sheet->setCellValue('N'.$rw,$state.'_'.$process);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }

    public function getDataDetailmovement(Request $request){

        if ($request->ajax()){
            $poreference = $request->po_buyer;
            $factory = \Auth::user()->factory_id;
            $cut_num = $request->cut_num;
            $article_num = $request->article_num;
            if($poreference != null && $cut_num != null && $article_num != null){
                $data = DB::connection('sds_live')->select(DB::RAW("SELECT
                bi.barcode_id,
                bi.season,
                bi.style,
                ad.set_type,
                bi.poreference,
                bi.article,
                bi.cut_num,
                bi.size,
                bi.part_num,
                bi.part_name,
                bi.note,
                bi.start_num,
                bi.end_num,
                bi.qty,
                bi.location,
                bi.processing_fact,
                bi.setting,
                bi.supplied_to
                FROM bundle_info_new bi
                JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
                WHERE bi.poreference = '$poreference' AND bi.cut_num='$cut_num' and bi.article = '$article_num' AND bi.processing_fact='$factory' ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"));
            }elseif($poreference != null && $article_num != null && $cut_num == null){
                $data = DB::connection('sds_live')->select(DB::RAW("SELECT
                bi.barcode_id,
                bi.season,
                bi.style,
                ad.set_type,
                bi.poreference,
                bi.article,
                bi.cut_num,
                bi.size,
                bi.part_num,
                bi.part_name,
                bi.note,
                bi.start_num,
                bi.end_num,
                bi.qty,
                bi.location,
                bi.processing_fact,
                bi.setting,
                bi.supplied_to
                FROM bundle_info_new bi
                JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
                WHERE bi.poreference = '$poreference' AND bi.article = '$article_num' AND bi.processing_fact='$factory' ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"));
            }elseif($poreference != null && $article_num == null && $cut_num != null){
                $data = DB::connection('sds_live')->select(DB::RAW("SELECT
                bi.barcode_id,
                bi.season,
                bi.style,
                ad.set_type,
                bi.poreference,
                bi.article,
                bi.cut_num,
                bi.size,
                bi.part_num,
                bi.part_name,
                bi.note,
                bi.start_num,
                bi.end_num,
                bi.qty,
                bi.location,
                bi.processing_fact,
                bi.setting,
                bi.supplied_to
                FROM bundle_info_new bi
                JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
                WHERE bi.poreference = '$poreference' AND bi.cut_num = '$cut_num' AND bi.processing_fact='$factory' ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"));
            }elseif($poreference != null && $article_num == null && $cut_num == null){
                $data = DB::connection('sds_live')->select(DB::RAW("SELECT
                bi.barcode_id,
                bi.season,
                bi.style,
                ad.set_type,
                bi.poreference,
                bi.article,
                bi.cut_num,
                bi.size,
                bi.part_num,
                bi.part_name,
                bi.note,
                bi.start_num,
                bi.end_num,
                bi.qty,
                bi.location,
                bi.processing_fact,
                bi.setting,
                bi.supplied_to
                FROM bundle_info_new bi
                JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
                WHERE bi.poreference = '$poreference' AND bi.processing_fact='$factory' ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"));
            }else{
                $data = [];
            }
            return DataTables::of($data)
            ->addColumn('is_supplied',function ($data)
            {
                $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$data->barcode_id)->first();
                $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$data->barcode_id)->first();
                if($cdms_ver != null && $sds_ver == null){
                    $is_supplied = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                    if($is_supplied != null){
                        return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                    }else{
                        return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                    }
                }elseif($cdms_ver == null && $sds_ver != null){
                    $is_supplied = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','16')->where('status_to','out')->first();
                    if($is_supplied != null){
                        return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                    }else{
                        return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                    }
                }elseif($data->supplied_to != null){
                    return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                }else{
                    return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                }
            })
            ->addColumn('cut_sticker',function ($data)
            {
                return $data->cut_num.' | '.$data->start_num.'-'.$data->end_num;
            })
            ->addColumn('style',function ($data)
            {
                if($data->set_type == '0'){
                    $style = $data->style;
                }else{
                    $style = $data->style.'-'.$data->set_type;
                }
                return $style;
            })
            ->addColumn('current_description',function ($data)
            {
                $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$data->barcode_id)->first();
                $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$data->barcode_id)->first();
                // return $data->location;
                $check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$data->barcode_id)->first();
                if($data->location == 'GEN'){
                    return 'WIP CUT';
                }else{
                    if($cdms_ver != null && $sds_ver == null){
                        return $cdms_ver->current_description;
                    }elseif($cdms_ver == null && $sds_ver != null){
                        return $sds_ver->current_description;
                    }else{
                        if(strpos($data->location,'LOC') > 0 || strpos($data->location,'L.') > 0){
                            return 'in SETTING';
                        }elseif(strpos($data->location,'WP') > 0){
                            if($check->bobok_out != null){
                                return 'out AUTO';
                            }else{
                                return 'in AUTO';
                            }
                        }elseif(strpos($data->location,'FUSE') > 0){
                            if($check->fuse_out != null){
                                return 'out FUSE';
                            }else{
                                return 'in FUSE';
                            }
                        }elseif(strpos($data->location,'PAD') > 0){
                            if($check->pad_out != null){
                                return 'out PAD';
                            }else{
                                return 'in PAD';
                            }
                        }elseif(strpos($data->location,'HE') > 0){
                            if($check->he_out != null){
                                return 'out HE';
                            }else{
                                return 'in HE';
                            }
                        }elseif(strpos($data->location,'ART') > 0){
                            if($check->artwork_out != null){
                                return 'out ARTWORK';
                            }else{
                                return 'in ARTWORK';
                            }
                        }else{
                            return $data->supplied_to;
                        }
                    }
                }
            })
            ->addColumn('line',function ($data)
            {
                $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$data->barcode_id)->first();
                $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$data->barcode_id)->first();
                if($cdms_ver != null && $sds_ver == null){
                    if($cdms_ver->current_description == 'in SETTING'){
                        $line = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','11')->first();
                        $line = $line == null ? '#CHECK' : $line->loc_dist;
                    }elseif($cdms_ver->current_description == 'out SUPPLY'){
                        $line = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','16')->first();
                        $line = $line == null ? '#CHECK' : $line->loc_dist;

                    }else{
                        $line =  '-';
                    }
                }elseif($cdms_ver == null && $sds_ver != null){
                    if($sds_ver->current_description == 'in SETTING'){
                        $line = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','11')->first();
                        $line = $line == null ? '#CHECK' : $line->loc_dist;
                    }elseif($sds_ver->current_description == 'out SUPPLY'){
                        $line = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','16')->first();
                        $line = $line == null ? '#CHECK' : $line->loc_dist;
                    }else{
                        $line =  '-';
                    }
                }else{
                    $line = '-';
                }
                return $line;
            })
            ->addColumn('user_scan',function($data){
                $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$data->barcode_id)->first();
                $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$data->barcode_id)->first();
                if($cdms_ver != null && $sds_ver == null){
                    if($cdms_ver->current_user_id != null){
                        $users = User::where('id',$cdms_ver->current_user_id)->first();
                        $user =  strtoupper($users->name);
                    }else{
                        $user =  '';
                    }
                }elseif($cdms_ver == null && $sds_ver != null){
                    if($sds_ver->current_user_id != null){
                        $users = User::where('id',$sds_ver->current_user_id)->first();
                        $user =  strtoupper($users->name);
                    }else{
                        $user =  '';
                    }
                }else{
                    $user = '';
                }
                return $user;
            })
            ->editColumn('updated_movement_at',function($data){
                $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$data->barcode_id)->first();
                $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$data->barcode_id)->first();
                if($cdms_ver != null && $sds_ver == null){
                    if($cdms_ver->updated_movement_at != null){
                        $date =  Carbon::createFromFormat('Y-m-d H:i:s',$cdms_ver->updated_movement_at);
                        setlocale(LC_ALL, 'IND');
                        return $date->formatLocalized('%A, %d %B %Y %H:%M:%S');
                    }else{
                        return '-';
                    }
                }elseif($cdms_ver == null && $sds_ver != null){
                    if($sds_ver->updated_movement_at != null){
                        $date =  Carbon::createFromFormat('Y-m-d H:i:s',$sds_ver->updated_movement_at);
                        setlocale(LC_ALL, 'IND');
                        return $date->formatLocalized('%A, %d %B %Y %H:%M:%S');
                    }else{
                        return '-';
                    }
                }else{
                    return '-';
                }
                // // $movement_date  = $data->updated_movement_at === null ? null : $data->updated_movement_at;
                // // $date           = Carbon::createfromformat('Y-m-d H:i:s',$movement_date);
                // // return $this->convertHari($date);
                // return 'updated';
            })
            ->addColumn('scan_setting',function($data){
                if($data->setting != null){
                    $date = Carbon::createFromFormat('Y-m-d H:i:s',$data->setting);
                    return $this->convertHari($date);
                }else{
                    return '-';
                }
                // $scan_set = DistribusiMovement::where('barcode_id',$data->barcode_id)->where('locator_to','11')->OrderBy('created_at','DESC')->first();
                // $date = $scan_set == null ? null : $scan_set->created_at;
                // return $this->convertHari($date);
            })
            ->editColumn('komponen_name',function($data){
                if($data->note == null || $data->note == ''){
                    return $data->part_num.' - '.$data->part_name;
                }else{
                    return $data->part_num.' - '.$data->part_name.' '.'<b class="text-danger"><i>'.trim($data->note).'</i></b>';
                }
            })
            ->addColumn('sewing_confirm',function($data){
                if($data->supplied_to == null){
                    return '<td class="text-center"><span class="icon icon-spam text-danger"></span></td>';
                }else{
                    return '<td class="text-center"><span class="icon icon-checkmark text-success"></span></td>';
                }
            })
            ->rawColumns(['style','factory_id','user_scan','current_description','is_supplied','scan_setting','updated_movement_at','komponen_name','sewing_confirm'])
            ->make(true);
        }else {
            $data = array();
            return DataTables::of($data)
            ->make(true);
        }
    }

    public function exportExcel(Request $request){
        $poreference = $request->po_buyer;
        $cut_num    = $request->cut_num;
        $factory_id = Auth::user()->factory_id;

        if($poreference != null && $cut_num != null){
            $filename = $poreference.'_Cut_'.$cut_num;
            $data = DB::connection('sds_live')->select(DB::RAW("SELECT
            bi.barcode_id,
            bi.season,
            bi.style,
            ad.set_type,
            bi.poreference,
            bi.article,
            bi.cut_num,
            bi.size,
            bi.part_num,
            bi.part_name,
            bi.note,
            bi.start_num,
            bi.end_num,
            bi.qty,
            bi.location,
            bi.processing_fact,
            bi.setting,
            bi.supplied_to
            FROM bundle_info_new bi
            JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            WHERE bi.poreference = ? AND bi.cut_num=? AND bi.processing_fact=? ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"),[$poreference,$cut_num,$factory_id]);
        }elseif($poreference != null){
            $filename = $poreference;
            $data = DB::connection('sds_live')->select(DB::RAW("SELECT
            bi.barcode_id,
            bi.season,
            bi.style,
            ad.set_type,
            bi.poreference,
            bi.article,
            bi.cut_num,
            bi.size,
            bi.part_num,
            bi.part_name,
            bi.note,
            bi.start_num,
            bi.end_num,
            bi.qty,
            bi.location,
            bi.processing_fact,
            bi.setting,
            bi.supplied_to
            FROM bundle_info_new bi
            JOIN art_desc_new ad on ad.season = bi.season and ad.style = bi.style and ad.part_num = bi.part_num and ad.part_name = bi.part_name
            WHERE bi.poreference = ? AND bi.processing_fact=? ORDER BY bi.style,ad.set_type,bi.cut_num,bi.size,bi.start_num"),[$poreference,$factory_id]);
        }else{
            $data = [];
        }
        if(count($data) == 0){
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Bundle_Locator_".$filename;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                            'H' => '@',
                            'I' => '@',
                            'J' => '@',
                            'K' => '@',
                            'L' => '@',
                            'M' => '@',
                            'N' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','DISTRIBUSI');
                        $sheet->setCellValue('C1','SEWING');
                        $sheet->setCellValue('D1','SCAN SETTING');
                        $sheet->setCellValue('E1','LAST UPDATE');
                        $sheet->setCellValue('F1','LAST LOCATOR');
                        $sheet->setCellValue('G1','STYLE');
                        $sheet->setCellValue('H1','ARTICLE');
                        $sheet->setCellValue('I1','CUT|STICKER');
                        $sheet->setCellValue('J1','SIZE');
                        $sheet->setCellValue('K1','KOMPONEN');
                        $sheet->setCellValue('L1','QTY');
                        $sheet->setCellValue('M1','DETAIL LOCATOR');
                        $sheet->setCellValue('N1','NAMA');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $cdms_ver = DB::table('bundle_detail')->where('sds_barcode',$vl->barcode_id)->first();
                            $sds_ver = DB::table('sds_detail_movements')->where('sds_barcode',$vl->barcode_id)->first();
                            if($cdms_ver != null && $sds_ver == null){
                                $is_supplied = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','16')->where('status_to','out')->first();
                                if($is_supplied != null){
                                    $is_supplied = 'SUPPLY';
                                }else{
                                    $is_supplied = '-';
                                }
                                ///
                                $last_update = $cdms_ver->updated_movement_at == null ? '-' : $cdms_ver->updated_movement_at;
                                ///
                                if($cdms_ver->current_description == 'in SETTING'){
                                    $line = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','11')->first();
                                    $line = $line == null ? '#CHECK' : $line->loc_dist;
                                }elseif($cdms_ver->current_description == 'out SUPPLY'){
                                    $line = DB::table('distribusi_movements')->where('barcode_id',$cdms_ver->barcode_id)->where('locator_to','16')->first();
                                    $line = $line == null ? '#CHECK' : $line->loc_dist;
            
                                }else{
                                    $line =  '-';
                                }
                                ///
                                if($cdms_ver->current_user_id != null){
                                    $users = User::where('id',$cdms_ver->current_user_id)->first();
                                    $user =  strtoupper($users->name);
                                }else{
                                    $user =  '';
                                }
                            }elseif($cdms_ver == null && $sds_ver != null){
                                $is_supplied = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','16')->where('status_to','out')->first();
                                if($is_supplied != null){
                                    $is_supplied = 'SUPPLY';
                                }else{
                                    $is_supplied = '-';
                                }

                                $last_update = $sds_ver->updated_movement_at == null ? '-' : $sds_ver->updated_movement_at;

                                if($sds_ver->current_description == 'in SETTING'){
                                    $line = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','11')->first();
                                    $line = $line == null ? '#CHECK' : $line->loc_dist;
                                }elseif($sds_ver->current_description == 'out SUPPLY'){
                                    $line = DB::table('distribusi_movements')->where('barcode_id',$sds_ver->sds_barcode)->where('locator_to','16')->first();
                                    $line = $line == null ? '#CHECK' : $line->loc_dist;
                                }else{
                                    $line =  '-';
                                }

                                if($sds_ver->current_user_id != null){
                                    $users = User::where('id',$sds_ver->current_user_id)->first();
                                    $user =  strtoupper($users->name);
                                }else{
                                    $user =  '';
                                }
                            }elseif($vl->supplied_to != null){
                                $is_supplied = 'SUPPLY';
                                $last_update = '-';
                                $line = '-';
                            }else{
                                $last_update = '-';
                                $is_supplied = '-';
                                $line = '-';
                            }

                            if($vl->supplied_to != null){
                                $sewing = $vl->supplied_to;
                            }else{
                                $sewing = '-';
                            }
                            if($vl->note == null || $vl->note == ''){
                                $komponen_name =  $vl->part_num.' - '.$vl->part_name;
                            }else{
                                $komponen_name =  $vl->part_num.' - '.$vl->part_name.'('.trim($vl->note).')';
                            }
                            $check = DB::connection('sds_live')->table('bundle_info_new')->where('barcode_id',$vl->barcode_id)->first();
                            if($vl->location == 'GEN'){
                                $last_locator =  'WIP CUT';
                            }else{
                                if($cdms_ver != null && $sds_ver == null){
                                    $last_locator =  $cdms_ver->current_description;
                                }elseif($cdms_ver == null && $sds_ver != null){
                                    $last_locator =  $sds_ver->current_description;
                                }else{
                                    if(strpos($vl->location,'LOC') > 0 || strpos($vl->location,'L.') > 0){
                                        $last_locator =  'in SETTING';
                                    }elseif(strpos($vl->location,'WP') > 0){
                                        if($check->bobok_out != null){
                                            $last_locator =  'out AUTO';
                                        }else{
                                            $last_locator =  'in AUTO';
                                        }
                                    }elseif(strpos($vl->location,'FUSE') > 0){
                                        if($check->fuse_out != null){
                                            $last_locator =  'out FUSE';
                                        }else{
                                            $last_locator =  'in FUSE';
                                        }
                                    }elseif(strpos($vl->location,'PAD') > 0){
                                        if($check->pad_out != null){
                                            $last_locator =  'out PAD';
                                        }else{
                                            $last_locator =  'in PAD';
                                        }
                                    }elseif(strpos($vl->location,'HE') > 0){
                                        if($check->he_out != null){
                                            $last_locator =  'out HE';
                                        }else{
                                            $last_locator =  'in HE';
                                        }
                                    }elseif(strpos($vl->location,'ART') > 0){
                                        if($check->artwork_out != null){
                                            $last_locator =  'out ARTWORK';
                                        }else{
                                            $last_locator =  'in ARTWORK';
                                        }
                                    }else{
                                        $last_locator =  $vl->supplied_to;
                                    }
                                }
                            }
                            $style_set = $vl->set_type == '0' ? $vl->style : $vl->style.'-'.$vl->set_type;
                            $sticker = $vl->cut_num.' | '.$vl->start_num.'-'.$vl->end_num;
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$is_supplied);
                            $sheet->setCellValue('C'.$rw,$sewing);
                            $sheet->setCellValue('D'.$rw,$vl->setting == null ? '-' : $vl->setting);
                            $sheet->setCellValue('E'.$rw,$last_update);
                            $sheet->setCellValue('F'.$rw,$last_locator);
                            $sheet->setCellValue('G'.$rw,$style_set);
                            $sheet->setCellValue('H'.$rw,$vl->article);
                            $sheet->setCellValue('I'.$rw,$sticker);
                            $sheet->setCellValue('J'.$rw,$vl->size);
                            $sheet->setCellValue('K'.$rw,$komponen_name);
                            $sheet->setCellValue('L'.$rw,$vl->qty);
                            $sheet->setCellValue('M'.$rw,$line);
                            $sheet->setCellValue('N'.$rw,$user);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }

    public function getStyle(Request $request)
    {
        $style = DB::table('data_cuttings')
        ->select('style')
        ->where('po_buyer',$request->po_buyer)
        ->distinct()
        ->get();
        return response()->json(['response' => $style],200);
    }
    public function getCutnum(Request $request){
        $cut_num = DB::table('bundle_header')
        ->select('cut_num')
        ->where('poreference',$request->po_buyer)
        ->distinct('cut_num')
        ->orderby('cut_num')
        ->get();
        return response()->json(['response' => $cut_num],200);
    }

    public function downloadSupplymovement(Request $request)
    {
        $scan_supply = explode('-', preg_replace('/\s+/', '', $request->scan_supply));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $scan_supply[0].' 06:00:00')->format('Y-m-d H:i:s');
        
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $scan_supply[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');
        $factory_id = \Auth::user()->factory_id;

        $data = DB::SELECT(DB::RAW("SELECT bd.barcode_id,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,bh.cut_num,bd.start_no,bd.end_no,bd.komponen_name,bd.qty,dm.loc_dist,
        DATE(dm.created_at) AS supply_date,
        u.name AS pic_supply
        FROM
        bundle_detail bd
        JOIN bundle_header bh ON bh.id = bd.bundle_header_id
        JOIN v_master_style vms ON vms.id = bd.style_detail_id
        JOIN distribusi_movements dm ON dm.barcode_id = bd.barcode_id
        JOIN users u ON u.id = dm.user_id
        WHERE dm.created_at BETWEEN '$from' and '$to' and locator_to='16' and bh.factory_id='$factory_id'
        UNION
        SELECT sdm.sds_barcode,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,shm.cut_num,sdm.start_no,sdm.end_no,sdm.komponen_name,sdm.qty,dm.loc_dist,DATE(dm.created_at) AS supply_date,
        u.name AS pic_supply
        FROM
        sds_detail_movements sdm
        JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
        JOIN v_master_style vms on vms.id = sdm.style_id
        JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
        JOIN users u on u.id = dm.user_id
        WHERE dm.created_at BETWEEN '$from' and '$to' and locator_to='16' and shm.factory_id='$factory_id'"));

        if($data == null){
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $filename = "Supply_Movement".$from." sampai ".$to;
            return Excel::create($filename,function($excel) use($data){
                    $excel->sheet('active',function($sheet) use ($data){
                        $sheet->setColumnFormat(array(
                            'A' => '@',
                            'B' => '@',
                            'C' => '@',
                            'D' => '@',
                            'E' => '@',
                            'F' => '@',
                            'G' => '@',
                            'H' => '@',
                            'I' => '@',
                            'J' => '@',
                            'K' => '@',
                            'L' => '@',
                            'M' => '@',
                            'N' => '@',
                        ));
                        $sheet->setCellValue('A1','NO');
                        $sheet->setCellValue('B1','LINE');
                        $sheet->setCellValue('C1','BARCODE ID');
                        $sheet->setCellValue('D1','PO BUYER');
                        $sheet->setCellValue('E1','SEASON');
                        $sheet->setCellValue('F1','STYLE');
                        $sheet->setCellValue('G1','ARTICLE');
                        $sheet->setCellValue('H1','CUT');
                        $sheet->setCellValue('I1','SIZE');
                        $sheet->setCellValue('J1','KOMPONEN');
                        $sheet->setCellValue('K1','QTY');
                        $sheet->setCellValue('L1','STICKER');
                        $sheet->setCellValue('M1','TANGGAL SUPPLY');
                        $sheet->setCellValue('N1','NAMA');

                        $i = 1;
                        foreach ($data as $key => $vl) {
                            $style_set = $vl->type_name == 'Non' ? $vl->style : $vl->style.'-'.$vl->type_name;
                            $stickers = $vl->start_no.'-'.$vl->end_no;
                            $rw = $i+1;
                            $sheet->setCellValue('A'.$rw,$i);
                            $sheet->setCellValue('B'.$rw,$vl->loc_dist);
                            $sheet->setCellValue('C'.$rw,"'".$vl->barcode_id);
                            $sheet->setCellValue('D'.$rw,$vl->poreference);
                            $sheet->setCellValue('E'.$rw,$vl->season);
                            $sheet->setCellValue('F'.$rw,$style_set);
                            $sheet->setCellValue('G'.$rw,$vl->article);
                            $sheet->setCellValue('H'.$rw,$vl->cut_num);
                            $sheet->setCellValue('I'.$rw,$vl->size);
                            $sheet->setCellValue('J'.$rw,$vl->komponen_name);
                            $sheet->setCellValue('K'.$rw,$vl->qty);
                            $sheet->setCellValue('L'.$rw,$stickers);
                            $sheet->setCellValue('M'.$rw,$vl->supply_date);
                            $sheet->setCellValue('N'.$rw,$vl->pic_supply);
                            $i++;
                        }
                    });
                    $excel->setActiveSheetIndex(0);
                })->export('xlsx');
        }
    }
    public function downloadSupplyset(Request $request)
    {
        $supply_date = explode('-', preg_replace('/\s+/', '', $request->supply_date));
        $from = Carbon::createFromFormat('m/d/Y H:i:s', $supply_date[0].' 06:00:00')->format('Y-m-d H:i:s');
        $to = Carbon::createFromFormat('m/d/Y H:i:s', $supply_date[1].' 06:59:59')->addDays(1)->format('Y-m-d H:i:s');
        $factory_id = \Auth::user()->factory_id;

        $data = DB::SELECT(DB::RAW("SELECT y.poreference,y.season,y.style,y.type_name,y.article,y.cut_num,y.size,y.start_no,y.end_no,sum(y.qty) qty_supply,y.loc_dist AS line_supply,y.supply_date,y.pic_supply
        FROM(SELECT x.poreference,x.season,x.style,x.type_name,x.article,x.size,x.cut_num,x.start_no,x.end_no,x.qty,x.loc_dist,x.supply_date,x.pic_supply
        FROM(SELECT bd.barcode_id,bh.poreference,bh.season,bh.style,vms.type_name,bh.article,bh.size,bh.cut_num,bd.start_no,bd.end_no,bd.komponen_name,bd.qty,dm.loc_dist,DATE(dm.created_at) AS supply_date,u.name AS pic_supply
        FROM
        bundle_detail bd
        JOIN bundle_header bh on bh.id = bd.bundle_header_id
        JOIN v_master_style vms on vms.id = bd.style_detail_id
        JOIN distribusi_movements dm on dm.barcode_id = bd.barcode_id
        JOIN users u on u.id = dm.user_id
        WHERE dm.created_at BETWEEN '$from' and  '$to' and locator_to='16' and bh.factory_id='$factory_id'
        UNION
        SELECT sdm.sds_barcode,shm.poreference,shm.season,shm.style,vms.type_name,shm.article,shm.size,shm.cut_num,sdm.start_no,sdm.end_no,sdm.komponen_name,sdm.qty,dm.loc_dist,
        DATE(dm.created_at) as supply_date,
        u.name AS pic_supply
        FROM
        sds_detail_movements sdm
        JOIN sds_header_movements shm on shm.id = sdm.sds_header_id
        JOIN v_master_style vms on vms.id = sdm.style_id
        JOIN distribusi_movements dm on dm.barcode_id = sdm.sds_barcode
        JOIN users u on u.id = dm.user_id
        WHERE dm.created_at BETWEEN '$from' and '$to' and locator_to='16' and shm.factory_id='$factory_id')x
        GROUP BY
        x.poreference,x.season,x.style,x.type_name,x.article,x.size,x.cut_num,x.start_no,x.end_no,x.qty,x.loc_dist,x.supply_date,x.pic_supply)y
        GROUP BY y.poreference,y.cut_num,y.size,y.start_no,y.end_no,y.season,y.style,y.type_name,y.article,y.loc_dist,y.supply_date,y.pic_supply"));
        

        if($data == null){
            return response()->json('Data tidak ditemukan.', 422);
        } else {
            $supply_date_name = explode('-', preg_replace('/\s+/', '', $request->supply_date));
            $from_name = Carbon::createFromFormat('m/d/Y', $supply_date[0])->format('Y-m-d');
            $to_name = Carbon::createFromFormat('m/d/Y', $supply_date[1])->format('Y-m-d');
            $filename = "Laporan_Supply_Set".$from_name." sampai ".$to_name;
            return Excel::create($filename,function($excel) use($data){
                $excel->sheet('active',function($sheet) use ($data){
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'J' => '@',
                        'K' => '@',
                        'L' => '@',
                    ));
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','LINE');
                    $sheet->setCellValue('C1','PO BUYER');
                    $sheet->setCellValue('D1','SEASON');
                    $sheet->setCellValue('E1','STYLE');
                    $sheet->setCellValue('F1','ARTICLE');
                    $sheet->setCellValue('G1','CUT NUM');
                    $sheet->setCellValue('H1','SIZE');
                    $sheet->setCellValue('I1','STICKER');
                    $sheet->setCellValue('J1','QTY SUPPLY');
                    $sheet->setCellValue('K1','SUPPLY DATE');
                    $sheet->setCellValue('L1','PIC');
                    $i = 1;
                    foreach ($data as $key => $vl) {
                        $style_set = $vl->type_name == 'Non' ? $vl->style : $vl->style.'-'.$vl->type_name;
                        $stickers = $vl->start_no.'-'.$vl->end_no;
                        $rw = $i+1;
                        $sheet->setCellValue('A'.$rw,$i);
                        $sheet->setCellValue('B'.$rw,$vl->line_supply);
                        $sheet->setCellValue('C'.$rw,$vl->poreference);
                        $sheet->setCellValue('D'.$rw,$vl->season);
                        $sheet->setCellValue('E'.$rw,$style_set);
                        $sheet->setCellValue('F'.$rw,$vl->article);
                        $sheet->setCellValue('G'.$rw,$vl->cut_num);
                        $sheet->setCellValue('H'.$rw,$vl->size);
                        $sheet->setCellValue('I'.$rw,$stickers);
                        $sheet->setCellValue('J'.$rw,$vl->qty_supply);
                        $sheet->setCellValue('K'.$rw,$vl->supply_date);
                        $sheet->setCellValue('L'.$rw,$vl->pic_supply);
                        $i++;
                    }
                });
                $excel->setActiveSheetIndex(0);
            })->export('xlsx');
        }
    }

    private function convertHari($date){
        if($date == null || $date == ''){
            return '-';
        }else{
            setlocale(LC_ALL, 'IND');
            return $date->formatLocalized('%A, %d %B %Y %H:%M:%S');
        }
    }

    private function convertLocator($locator)
    {
        // id base database master_locator
        switch ($locator) {
            case 1:
                $process    = 'fuse';
                $out_column = 'fuse_out';
                $pic_column = 'fuse_out_pic';
                break;
            case 2:
                $process    = 'he';
                $out_column = 'he_out';
                $pic_column = 'he_out_pic';
                break;
            case 3:
                $process    = 'artwork';
                $out_column = 'artwork_out';
                $pic_column = 'artwork_out_pic';
                break;
            case 4:
                $process    = 'pad';
                $out_column = 'pad_out';
                $pic_column = 'pad_out_pic';
                break;
            case 5:
                $process    = 'bobok';
                $out_column = 'bobok_out';
                $pic_column = 'bobok_out_pic';
                break;
            case 6:
                $process    = 'ppa';
                $out_column = 'ppa_out';
                $pic_column = 'ppa_out_pic';
                break;
            case 8:
                $process    = 'bonding';
                $out_column = 'bonding_out';
                $pic_column = 'bonding_out_pic';
                break;
            case 10:
                $process    = 'cutting';
                $out_column = 'distrib_received';
                $pic_column = 'bd_pic';
                break;
            case 11:
                $process    = 'setting';
                $out_column = 'setting';
                $pic_column = 'setting_pic';
                break;
            case 16:
                //SEWING VERSION
                $process    = 'supply';
                $out_column = 'supply';
                $pic_column = 'supplied_to';
                break;
            default:
                $process    = null;
                $out_column = null;
                $pic_column = null;
        }
        $convert_locator = [
            'process'          => $process,
            'out_column'       => $out_column,
            'pic_column'       => $pic_column
        ];

        return $convert_locator;
    }
}
